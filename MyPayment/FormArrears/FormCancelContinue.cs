﻿using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormCancelContinue : Form
    {
        ResultCostTDebt resul = new ResultCostTDebt();
        ArrearsController arr = new ArrearsController();
        GeneralService genService = new GeneralService(true);
        ClassLogsService meaLog = new ClassLogsService("Cancel", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("Cancel", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        public FormCancelContinue()
        {
            InitializeComponent();
        }
        private void ButtonOk_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (DataGridViewDetail.Rows.Count > 0)
                {
                    List<ClassConnectMeter> lstConnect = new List<ClassConnectMeter>();                   
                    bool status = false;
                    int i = 0;
                    var loopTo = DataGridViewDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewRow rownew = DataGridViewDetail.Rows[i];
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        string ca = rownew.Cells["ColCa"].EditedFormattedValue.ToString();                     
                        bool isChecked = (bool)checkCell.EditedFormattedValue;
                        if (isChecked)
                        {
                            ClassConnectMeter cancel = new ClassConnectMeter();
                            status = true;
                            cancel.ca =Convert.ToInt64(ca);
                            lstConnect.Add(cancel);
                        }
                    }
                    if (status)
                    {
                        if (MessageBox.Show("คุณต้องการต่อกลับค่าดำเนินการงดจ่ายไฟ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;
                        else
                        {
                            FormCheckUser checkUser = new FormCheckUser("ยืนยันการต่อกลับค่าดำเนินการงดจ่ายไฟ");
                            checkUser.ShowDialog();
                            checkUser.Dispose();
                            if (GlobalClass.StatusForm == 1)
                            {
                                if (GlobalClass.AuthorizeLevel == 2)
                                {
                                    if (lstConnect.Count > 0)
                                    {
                                        string str = "";
                                        foreach (var item in lstConnect)
                                        {
                                            string strJSON = null;
                                            ClassConnectMeter Cancel = new ClassConnectMeter();
                                            Cancel.ca = item.ca;
                                            Cancel.userId = GlobalClass.UserId.ToString();
                                            string result = JsonConvert.SerializeObject(Cancel);
                                            strJSON = ClassReadAPI.PostDataAPI("connectMeter", result, GlobalClass.Timer);
                                            ClassUser user = ClassReadAPI.ReadToObject(strJSON);
                                            if (user.result_code == "SUCCESS")
                                            {
                                                str = item.ca + " : SUCCESS" + Environment.NewLine;
                                                meaLog.WriteData("SUCCESS :: CA : " + item.ca.ToString() + " |  User ID : " + GlobalClass.UserIdCancel, method, LogLevel.Debug);
                                            }
                                            else
                                            {
                                                str = item.ca + " : ERROR" + Environment.NewLine;
                                                meaLogError.WriteData("Fail :: CA : " + item.ca.ToString() + " |  User ID : " + GlobalClass.UserIdCancel, method, LogLevel.Debug);
                                            }                                           
                                        }
                                        MessageBox.Show(str, "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        
                                    }
                                    this.Close();
                                }
                                else
                                    MessageBox.Show("User ที่สามารถต่อกลับค่าดำเนินการงดจ่ายไฟได้ต้องเป็น Level 2 เท่านั้น", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }                              
                        }
                    }
                }
                else
                    MessageBox.Show("กรุณาเลือกข้อมูลที่ต้องการต่อกลับค่าดำเนินการงดจ่ายไฟ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("ไม่สามารถต่อกลับค่าดำเนินการงดจ่ายไฟการ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            CheckBoxSelectAll.Checked = false;
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void LoadData()
        {
            resul = arr.LoadDataContinue();
            DataGridViewDetail.DataSource = resul;
        }
        private void CheckBoxSelectAll_Click(object sender, EventArgs e)
        {
            int i = 0;
            var loopTo = DataGridViewDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CheckBoxSelectAll.Checked == true)
                    DataGridViewDetail.Rows[i].Cells["ColCheckBox"].Value = true;
                else
                    DataGridViewDetail.Rows[i].Cells["ColCheckBox"].Value = false;
            }
        }
        private void DataGridViewDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                int i = 0;
                int countCheck = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (DataGridViewDetail.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        if (newBool)
                        {
                            DataGridViewDetail.Rows[i].Cells[0].Value = true;
                            countCheck++;
                            if (countCheck == DataGridViewDetail.Rows.Count)
                                CheckBoxSelectAll.Checked = true;
                        }
                        else
                        {
                            DataGridViewDetail.Rows[i].Cells[0].Value = false;
                            CheckBoxSelectAll.Checked = false;
                            if (countCheck == 0)
                                countCheck = 0;
                        }
                    }
                }
            }
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            ResultInquiryDebt resul = new ResultInquiryDebt();
            string strBarcade = txtBarcode.Text.Trim();
            ClassUser clsUser = new ClassUser();
            clsUser.debtType = "DIS";
            clsUser.ca = strBarcade;
            resul = arr.SelectInquiryDIS(clsUser, strBarcade);

            if (resul.ResultMessage != null)
                MessageBox.Show(resul.ResultMessage.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
                DataGridViewDetail.DataSource = resul.OutputDIS;
        }
        private void FormCancelContinue_Load(object sender, EventArgs e)
        {
            SetControl();
        }
        public void SetControl()
        {
            DataGridViewDetail.AutoGenerateColumns = false;
            DataGridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;
        }
        private void txtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ButtonSearch_Click(sender,e);
            }
        }
        private void btUnconnect_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (DataGridViewDetail.Rows.Count > 0)
                {
                    List<ClassCancelContinue> lstUnConnect = new List<ClassCancelContinue>();
                    bool status = false;
                    int i = 0;
                    var loopTo = DataGridViewDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewRow rownew = DataGridViewDetail.Rows[i];
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        string debtId = rownew.Cells["ColDebtId"].EditedFormattedValue.ToString();
                        string ca = rownew.Cells["ColCa"].EditedFormattedValue.ToString();
                        bool isChecked = (bool)checkCell.EditedFormattedValue;
                        if (isChecked)
                        {
                            ClassCancelContinue cancel = new ClassCancelContinue();
                            status = true;
                            cancel.debtId = debtId;
                            cancel.ca = ca;
                            lstUnConnect.Add(cancel);
                        }
                    }
                    if (status)
                    {
                        if (MessageBox.Show("คุณต้องการยกเลิกต่อกลับค่าดำเนินการงดจ่ายไฟ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;
                        else
                        {
                            FormCheckUser checkUser = new FormCheckUser("ยืนยันการยกเลิกต่อกลับค่าดำเนินการงดจ่ายไฟ");
                            checkUser.ShowDialog();
                            checkUser.Dispose();
                            if (GlobalClass.StatusForm == 1)
                            {
                                if (GlobalClass.AuthorizeLevel == 2)
                                {
                                    if (lstUnConnect.Count > 0)
                                    {
                                        string str = "";
                                        foreach (var item in lstUnConnect)
                                        {
                                            string strJSON = null;
                                            ClassCancelContinue UnCancel = new ClassCancelContinue();
                                            UnCancel.debtId = item.debtId;
                                            UnCancel.userId = GlobalClass.UserId;
                                            UnCancel.authorizedCancelBy = GlobalClass.UserName;
                                            UnCancel.ca = item.ca;
                                            string result = JsonConvert.SerializeObject(UnCancel);
                                            strJSON = ClassReadAPI.PostDataAPI("UnconnectMeter", result, GlobalClass.Timer);
                                            ClassUser user = ClassReadAPI.ReadToObject(strJSON);
                                            if (user.result_code == "SUCCESS")
                                            {
                                                str = item.debtId + " : SUCCESS" + Environment.NewLine;
                                                meaLog.WriteData("SUCCESS :: Debt ID : " + item.debtId.ToString() + " |  User ID : " + GlobalClass.UserIdCancel, method, LogLevel.Debug);
                                            }
                                            else
                                            {
                                                str = item.debtId + " : ERROR" + Environment.NewLine;
                                                meaLogError.WriteData("Fail :: Debt ID : " + item.debtId.ToString() + " |  User ID : " + GlobalClass.UserIdCancel, method, LogLevel.Debug);
                                            }
                                        }
                                        MessageBox.Show(str, "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    }
                                    this.Close();
                                }
                                else
                                    MessageBox.Show("User ที่สามารถยกเลิกต่อกลับค่าดำเนินการงดจ่ายไฟได้ต้องเป็น Level 2 เท่านั้น", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                    }
                }
                else
                    MessageBox.Show("กรุณาเลือกข้อมูลที่ต้องการยกเลิกต่อกลับค่าดำเนินการงดจ่ายไฟ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("ไม่สามารถยกเลิกต่อกลับค่าดำเนินการงดจ่ายไฟการ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            CheckBoxSelectAll.Checked = false;
        }
    }
}
