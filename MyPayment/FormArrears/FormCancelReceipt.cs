﻿using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.FormArrears
{
    public partial class FormCancelReceipt : Form
    {
        ResultDisplayPaytment resul = new ResultDisplayPaytment();
        GeneralService genService = new GeneralService(true);
        ClassLogsService meaLog = new ClassLogsService("Cancel", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("Cancel", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        public FormCancelReceipt()
        {
            InitializeComponent();
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void ButtonOk_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (DataGridViewDetail.Rows.Count > 0)
                {
                    List<ClasscancelReceipt> lstReceip = new List<ClasscancelReceipt>();
                    ClasscancelReceipt Receip = new ClasscancelReceipt();
                    bool status = false;
                    int i = 0;
                    string[] str = new string[DataGridViewDetail.Rows.Count];
                    var loopTo = DataGridViewDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewRow rownew = DataGridViewDetail.Rows[i];
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckbox"];
                        string receipt = rownew.Cells["ColReceiptNo"].EditedFormattedValue.ToString();
                        bool isChecked = (bool)checkCell.EditedFormattedValue;
                        if (isChecked)
                        {
                            status = true;
                            str[i] = receipt;
                        }
                    }
                    if (status)
                    {
                        if (MessageBox.Show("คุณต้องการยกเลิกใบเสร็จรับเงิน", "ยกเลิก", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;
                        else
                        {
                            FormCheckUser checkUser = new FormCheckUser("ยืนยันการยกเลิกใบเสร็จรับเงิน");
                            checkUser.ShowDialog();
                            checkUser.Dispose();
                            if (GlobalClass.StatusForm == 1)
                            {
                                if (GlobalClass.AuthorizeLevel == 2)
                                {
                                    string strJSON = null;
                                    Receip.receiptNoList = str;
                                    Receip.userId = GlobalClass.UserId;
                                    Receip.approveEmpId = GlobalClass.UserIdCancel;
                                    Receip.remark = GlobalClass.Remark;
                                    string result = JsonConvert.SerializeObject(Receip);
                                    strJSON = ClassReadAPI.PostDataAPI("processCancelReceipt", result, GlobalClass.Timer);
                                    ClassUser clsUser = ClassReadAPI.ReadToObject(strJSON);
                                    if (clsUser.result_code == "SUCCESS")
                                    {
                                        string ReceiptNo = "";
                                        if (clsUser.successReceiptList != null)
                                        {
                                            for (int j = 0; j < clsUser.successReceiptList.Count(); j++)
                                            {
                                                ReceiptNo += clsUser.successReceiptList[j].ToString() + " : SUCCESS" + Environment.NewLine;
                                            }
                                            //MessageBox.Show("ยกเลิกใบเสร็จรับเงินเรียบร้อยแล้ว", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        }
                                        if (clsUser.failureReceiptList != null)
                                        {
                                            for (int j = 0; j < clsUser.failureReceiptList.Count(); j++)
                                            {
                                                ReceiptNo += clsUser.failureReceiptList[j].ToString() + " : ERROR" + Environment.NewLine;
                                            }
                                            //MessageBox.Show("ReceiptNo " + ReceiptNo + " ไม่สามารถยกเลิกใบเสร็จรับเงินได้ ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        }
                                        MessageBox.Show(ReceiptNo, "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    }
                                    //this.Close();
                                }
                            }
                        }
                        DataGridViewDetail.DataSource = null;
                    }
                    else
                        MessageBox.Show("กรุณาเลือกข้อมูลที่ต้องการยกเลิกใบเสร็จรับเงิน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("ไม่สามารถยกเลิกใบเสร็จได้","Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            CheckBoxSelectAll.Checked = false;
        }
        private void FormCancelReceipt_Load(object sender, EventArgs e)
        {
            SetControl();
        }
        public void SetControl()
        {
            DataGridViewDetail.AutoGenerateColumns = false;
            DataGridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ArrearsController arr = new ArrearsController();
                ClassDisplayPaytment cls = new ClassDisplayPaytment();
                cls.paymentNo = TextboxPaymentNo.Text;
                cls.receiptNo = TextBoxReceiptNo.Text;
                resul = arr.GetDisplayPaytment(cls);
                if (resul.Output.Count > 0)
                {
                    DataGridViewDetail.DataSource = resul.Output;
                    ButtonOk.Focus();
                }
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            ClearData();
        }
        private void CheckBoxSelectAll_Click(object sender, EventArgs e)
        {
            int i = 0;
            var loopTo = DataGridViewDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CheckBoxSelectAll.Checked == true)
                    DataGridViewDetail.Rows[i].Cells["ColCheckBox"].Value = true;
                else
                    DataGridViewDetail.Rows[i].Cells["ColCheckBox"].Value = false;
            }
        }
        private void DataGridViewDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                int i = 0;
                int countCheck = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (DataGridViewDetail.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        if (newBool)
                        {
                            DataGridViewDetail.Rows[i].Cells[0].Value = true;
                            countCheck++;
                            if (countCheck == DataGridViewDetail.Rows.Count)
                                CheckBoxSelectAll.Checked = true;
                        }
                        else
                        {
                            DataGridViewDetail.Rows[i].Cells[0].Value = false;
                            CheckBoxSelectAll.Checked = false;
                            if (countCheck == 0)
                                countCheck = 0;
                        }
                    }
                }
            }
        }
        public void ClearData()
        {
            TextboxPaymentNo.Text = string.Empty;
            TextBoxReceiptNo.Text = string.Empty;
        }
        private void TextboxPaymentNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                ButtonSearch_Click(null, null);
        }
        private void TextBoxReceiptNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                ButtonSearch_Click(null, null);
        }
    }
}
