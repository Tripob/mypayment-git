﻿namespace MyPayment.FormArrears
{
    partial class FormCancelContinue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCancelContinue));
            this.DataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.ColCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColCusName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCusId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDebtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btUnconnect = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonOk = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridViewDetail
            // 
            this.DataGridViewDetail.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.DataGridViewDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewDetail.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCheckBox,
            this.ColCusName,
            this.ColCusId,
            this.ColCa,
            this.ColUi,
            this.ColDebtId});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.DefaultCellStyle = dataGridViewCellStyle7;
            this.DataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridViewDetail.Location = new System.Drawing.Point(0, 64);
            this.DataGridViewDetail.Name = "DataGridViewDetail";
            this.DataGridViewDetail.RowHeadersVisible = false;
            this.DataGridViewDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DataGridViewDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewDetail.Size = new System.Drawing.Size(1012, 578);
            this.DataGridViewDetail.TabIndex = 3;
            this.DataGridViewDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewDetail_CellContentClick);
            // 
            // ColCheckBox
            // 
            this.ColCheckBox.DataPropertyName = "SelectCheck";
            this.ColCheckBox.Frozen = true;
            this.ColCheckBox.HeaderText = "";
            this.ColCheckBox.Name = "ColCheckBox";
            this.ColCheckBox.Width = 50;
            // 
            // ColCusName
            // 
            this.ColCusName.DataPropertyName = "cusName";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColCusName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColCusName.HeaderText = "ชื่อลูกค้า";
            this.ColCusName.Name = "ColCusName";
            this.ColCusName.Width = 250;
            // 
            // ColCusId
            // 
            this.ColCusId.DataPropertyName = "cusId";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColCusId.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColCusId.HeaderText = "รหัสลูกค้า";
            this.ColCusId.Name = "ColCusId";
            this.ColCusId.Width = 230;
            // 
            // ColCa
            // 
            this.ColCa.DataPropertyName = "ca";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColCa.DefaultCellStyle = dataGridViewCellStyle5;
            this.ColCa.HeaderText = "บัญชีแสดงสัญญา";
            this.ColCa.Name = "ColCa";
            this.ColCa.Width = 230;
            // 
            // ColUi
            // 
            this.ColUi.DataPropertyName = "ui";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColUi.DefaultCellStyle = dataGridViewCellStyle6;
            this.ColUi.HeaderText = "รหัสเครื่องวัดฯ";
            this.ColUi.Name = "ColUi";
            this.ColUi.Width = 235;
            // 
            // ColDebtId
            // 
            this.ColDebtId.DataPropertyName = "debtId";
            this.ColDebtId.HeaderText = "Debt ID";
            this.ColDebtId.Name = "ColDebtId";
            this.ColDebtId.Visible = false;
            // 
            // CheckBoxSelectAll
            // 
            this.CheckBoxSelectAll.AutoSize = true;
            this.CheckBoxSelectAll.Location = new System.Drawing.Point(19, 74);
            this.CheckBoxSelectAll.Name = "CheckBoxSelectAll";
            this.CheckBoxSelectAll.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelectAll.TabIndex = 4;
            this.CheckBoxSelectAll.UseVisualStyleBackColor = true;
            this.CheckBoxSelectAll.Click += new System.EventHandler(this.CheckBoxSelectAll_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonSearch);
            this.groupBox2.Controls.Add(this.txtBarcode);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1012, 64);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.txtBarcode.Location = new System.Drawing.Point(135, 19);
            this.txtBarcode.Margin = new System.Windows.Forms.Padding(2);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(330, 31);
            this.txtBarcode.TabIndex = 4;
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 28);
            this.label1.TabIndex = 6;
            this.label1.Text = "บัญชีแสดงสัญญา";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btUnconnect);
            this.groupBox1.Controls.Add(this.ButtonOk);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 642);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1012, 60);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // btUnconnect
            // 
            this.btUnconnect.BackColor = System.Drawing.Color.Transparent;
            this.btUnconnect.BorderColor = System.Drawing.Color.Transparent;
            this.btUnconnect.BorderWidth = 1;
            this.btUnconnect.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btUnconnect.ButtonText = "ไม่ต่อกลับ";
            this.btUnconnect.CausesValidation = false;
            this.btUnconnect.EndColor = System.Drawing.Color.DarkGray;
            this.btUnconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUnconnect.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btUnconnect.ForeColor = System.Drawing.Color.Black;
            this.btUnconnect.GradientAngle = 90;
            this.btUnconnect.Image = global::MyPayment.Properties.Resources.ok2;
            this.btUnconnect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btUnconnect.Location = new System.Drawing.Point(873, 14);
            this.btUnconnect.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btUnconnect.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btUnconnect.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btUnconnect.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btUnconnect.Name = "btUnconnect";
            this.btUnconnect.ShowButtontext = true;
            this.btUnconnect.Size = new System.Drawing.Size(127, 40);
            this.btUnconnect.StartColor = System.Drawing.Color.LightGray;
            this.btUnconnect.TabIndex = 3;
            this.btUnconnect.TextLocation_X = 45;
            this.btUnconnect.TextLocation_Y = 5;
            this.btUnconnect.Transparent1 = 80;
            this.btUnconnect.Transparent2 = 120;
            this.btUnconnect.UseVisualStyleBackColor = true;
            this.btUnconnect.Click += new System.EventHandler(this.btUnconnect_Click);
            // 
            // ButtonOk
            // 
            this.ButtonOk.BackColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderWidth = 1;
            this.ButtonOk.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonOk.ButtonText = "ต่อกลับ";
            this.ButtonOk.CausesValidation = false;
            this.ButtonOk.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonOk.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonOk.ForeColor = System.Drawing.Color.Black;
            this.ButtonOk.GradientAngle = 90;
            this.ButtonOk.Image = global::MyPayment.Properties.Resources.ok;
            this.ButtonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonOk.Location = new System.Drawing.Point(740, 14);
            this.ButtonOk.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonOk.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonOk.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonOk.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonOk.Name = "ButtonOk";
            this.ButtonOk.ShowButtontext = true;
            this.ButtonOk.Size = new System.Drawing.Size(127, 40);
            this.ButtonOk.StartColor = System.Drawing.Color.LightGray;
            this.ButtonOk.TabIndex = 2;
            this.ButtonOk.TextLocation_X = 45;
            this.ButtonOk.TextLocation_Y = 5;
            this.ButtonOk.Transparent1 = 80;
            this.ButtonOk.Transparent2 = 120;
            this.ButtonOk.UseVisualStyleBackColor = true;
            this.ButtonOk.Click += new System.EventHandler(this.ButtonOk_Click);
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "ค้นหา";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = global::MyPayment.Properties.Resources.zoom;
            this.ButtonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSearch.Location = new System.Drawing.Point(492, 14);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(127, 40);
            this.ButtonSearch.StartColor = System.Drawing.Color.LightGray;
            this.ButtonSearch.TabIndex = 5;
            this.ButtonSearch.TextLocation_X = 45;
            this.ButtonSearch.TextLocation_Y = 5;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // FormCancelContinue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CheckBoxSelectAll);
            this.Controls.Add(this.DataGridViewDetail);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormCancelContinue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ยกเลิกค่างดจ่ายไฟฟ้า";
            this.Load += new System.EventHandler(this.FormCancelContinue_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Custom_Controls_in_CS.ButtonZ ButtonOk;
        private System.Windows.Forms.DataGridView DataGridViewDetail;
        private System.Windows.Forms.CheckBox CheckBoxSelectAll;
        private System.Windows.Forms.GroupBox groupBox2;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCusName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCusId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUi;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDebtId;
        private Custom_Controls_in_CS.ButtonZ btUnconnect;
    }
}