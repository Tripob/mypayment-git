﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.FormArrears
{
    public partial class FormSearchData : Form
    {
        ResultPayment resul = new ResultPayment();
        GeneralService genService = new GeneralService(true);
        ClassLogsService meaLog = new ClassLogsService("Cancel", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("Cancel", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        ArrearsController arr = new ArrearsController();
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        List<inquiryGroupDebtBeanList> _lstinquiry = new List<inquiryGroupDebtBeanList>();
        public FormSearchData()
        {
            InitializeComponent();
        }
        private void FormCancelReceipt_Load(object sender, EventArgs e)
        {
            SetControl();
        }
        public void SetControl()
        {
            DataGridViewDetail.AutoGenerateColumns = false;
            DataGridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (TextboxCA.Text == "" && TextBoxUI.Text =="")
                    MessageBox.Show("กรุณากรอกเงื่อนไขในการค้นหา", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    ArrearsController arr = new ArrearsController();
                    inquiryInfoBeanList cls = new inquiryInfoBeanList();
                    cls.ca = TextboxCA.Text;
                    cls.ui = TextBoxUI.Text;
                    resul = arr.SelectDataRemittance(cls);
                    if (resul.Output.Count > 0)
                        DataGridViewDetail.DataSource = resul.Output;
                    else
                        MessageBox.Show("ไม่มีข้อมูลในการค้นหา", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }             
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("เกิดข้อผิดพลาดในการค้นหา", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            ClearData();
        }
        public void ClearData()
        {
            TextboxCA.Text = string.Empty;
            TextBoxUI.Text = string.Empty;
        }
        private void TextboxPaymentNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                ButtonSearch_Click(null, null);
        }
        private void TextBoxReceiptNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                ButtonSearch_Click(null, null);
        }
        private void ButtonClear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            if (!SendDataAddPrintData())
            {
                this.DataGridViewDetail.DataSource = null;
                this.DataGridViewDetail.Refresh();
            }
        }
        public bool SendDataAddPrintData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool status = false;
            try
            {
                if (resul.Output.Count > 0)
                {
                    string Path = "";
                    List<paymentDisplayTransctionResponseDtoList> _lstDisplay = new List<paymentDisplayTransctionResponseDtoList>();
                    foreach (paymentDisplayTransctionResponseDtoList item in resul.Output)
                    {
                        DateTime dt = DateTime.Now;
                        DateTime dtNow = DateTime.Now;
                        paymentDisplayTransctionResponseDtoList models = new paymentDisplayTransctionResponseDtoList();
                        string str = arr.PrintDate(dt.Month);
                        if (dt.Year < 2500)
                            dtNow = dt.AddYears(543);
                        models.strDate = "วันที่ " + dt.Day.ToString() + " " + str + " " + dtNow.Year.ToString() + " เวลา : " + dt.Hour.ToString() + ":" + dt.Minute.ToString();
                        models.paymentNo = item.paymentNo;
                        models.receiptNo = item.receiptNo;
                        models.paymentChannelDesc = item.paymentChannelDesc;
                        models.distShortDesc = item.distShortDesc;
                        models.paymentTypeDesc = item.paymentTypeDesc;
                        models.paymentDateStr = item.paymentDateStr;
                        models.debtName = item.debtName;
                        models.receiptDtlName = item.receiptDtlName;
                        models.receiptDtlAmount = item.receiptDtlAmount;
                        models.receiptDtlVat = item.receiptDtlVat;
                        models.receiptDtlTotalAmount = item.receiptDtlTotalAmount;
                        models.cashierName = item.cashierName;
                        models.cashierNo = item.cashierNo;
                        models.totalAmount += item.receiptDtlAmount + item.receiptDtlVat;
                        _lstDisplay.Add(models);
                    }
                    Path = ReportPath + "\\" + "Check_Payment_Report.rpt";
                    ReportDocument cryRpt = new ReportDocument();
                    cryRpt.Load(Path);
                    cryRpt.Database.Tables[0].SetDataSource(_lstDisplay);
                    CrystalReportViewer crystalReportViewer1 = new CrystalReportViewer();
                    crystalReportViewer1.ReportSource = cryRpt;
                    crystalReportViewer1.Refresh();
                    cryRpt.PrintToPrinter(1, true, 0, 0);
                    status = true;
                }
            }
            catch (Exception ex)
            {
                meaLogError.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
            return status;
        }
    }
}
