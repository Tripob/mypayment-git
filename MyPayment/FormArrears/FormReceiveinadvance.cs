﻿using MyPayment.Controllers;
using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormReceiveinadvance : Form
    {
        ClassLogsService ReceiveMoneyLog = new ClassLogsService("ReceiveMoney", "DataLog");
        ClassLogsService ReceiveMoneyEventLog = new ClassLogsService("ReceiveMoney", "EventLog");
        ClassLogsService ReceiveMoneyErrorLog = new ClassLogsService("ReceiveMoney", "ErrorLog");
        ArrearsController arr = new ArrearsController();

        public string ca;
        public ResultInquiryDebt resul = new ResultInquiryDebt();
        public FormReceiveinadvance()
        {
            InitializeComponent();
        }
        private void ButtonOk_Click(object sender, EventArgs e)
        {
            if (TextboxAmount.Text != "" && TextboxAmount.Text != "0")
            {
                if (Convert.ToDecimal(labelTotalAmount.Text) > 0)
                {
                    AddReceiveinadvance();
                    GlobalClass.StatusAdvance = 1;
                    this.Close();
                }
                else
                    MessageBox.Show("กรุณากรอกจำนวนเงิน ให้ถูกต้อง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
                MessageBox.Show("กรุณากรอกจำนวนเงิน ที่ต้องการรับเงินล่วงหน้า", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        public void AddReceiveinadvance()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                arr.AddReceiveinadvance(Convert.ToDecimal(labelTotalAmount.Text), resul, ca);
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void TextboxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                ButtonOk.Focus();
        }
        private void TextboxAmount_TextChanged(object sender, EventArgs e)
        {
            TextBox T = (TextBox)sender;
            if (System.Text.RegularExpressions.Regex.IsMatch(T.Text, "^[0-9]"))
                Summount();
            decimal amount = Convert.ToDecimal(TextboxAmount.Text);
            TextboxAmount.Text = amount.ToString("#,###,###,##0.00");
            int len = TextboxAmount.Text.ToString().Length;
            T.SelectionStart = len - 3;
        }
        public void Summount()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double totalAmount = 0;
                double amount = 0;
                double payment = 0;
                if (TextboxAmount.Text != "")
                    amount = Convert.ToDouble(TextboxAmount.Text);
                if (LabelPayment.Text != "0.00")
                    payment = Convert.ToDouble(LabelPayment.Text);

                totalAmount = amount - payment;
                labelTotalAmount.Text = totalAmount.ToString("#,###,###,##0.00");
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            GlobalClass.StatusAdvance = 0;
            this.Close();
        }
    }
}
