﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using GinkoSolutions;
using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormReceiveMoney : Form
    {
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ClassLogsService ReportErrorLog = new ClassLogsService("ReportErrorLog", "ErrorLog");
        ClassLogsService ReceiveMoneyLog = new ClassLogsService("ReceiveMoney", "DataLog");
        ClassLogsService ReceiveMoneyEventLog = new ClassLogsService("ReceiveMoney", "EventLog");
        ClassLogsService ReceiveMoneyErrorLog = new ClassLogsService("ReceiveMoney", "ErrorLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        public CultureInfo usCulture = new CultureInfo("en-US");
        public CultureInfo _usCultureTh = new CultureInfo("th-TH");
        public string CustomerName;
        public double Countamount;
        public double CountamountAll;
        ResultCostTDebt resul = new ResultCostTDebt();
        ArrearsController arr = new ArrearsController();
        List<ClassCheque> _cheque = new List<ClassCheque>();
        List<ClassListGridview> _lstGrid = new List<ClassListGridview>();
        public byte Key_Error;
        public string qNo;
        public bool _StatusSendData;
        public string _RealChange;
        ResultProcessPayment _Paymentout = new ResultProcessPayment();
        Thread threadData;
        Thread threadBar;
        int _Type;
        decimal _amount = 0;
        decimal _amountCheque = 0;
        public FormReceiveMoney(int type)
        {
            _Type = type;
            InitializeComponent();
        }
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (!CheckStatus())
                {
                    panel3.Enabled = false;
                    panel6.Enabled = false;
                    double notenoughamount = Convert.ToDouble(label25.Text);
                    decimal amountReceived = Convert.ToDecimal(LabelAmountReceived.Text);
                    if (notenoughamount >= 0 && amountReceived > 0)
                    {
                        if (TextAmountCreditCard.Text != "")
                        {
                            if (CheckTextCreditCardNew())
                            {
                                panel3.Enabled = true;
                                panel6.Enabled = true;
                                MessageBox.Show("กรุณากรอกข้อมูลบัตรเครดิตให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return;
                            }
                        }
                        ThreadStart();
                    }
                    else
                    {
                        panel3.Enabled = true;
                        panel6.Enabled = true;
                        MessageBox.Show("จำนวนเงินที่รับน้อยกว่าจำนวนเงินที่ต้องชำระ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                    MessageBox.Show("ไม่สามารถรับชำระได้เนื่องจากมีรายการหักบัญชีธนาคาร/บัตรเครดิต", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                panel3.Enabled = true;
                panel6.Enabled = true;
                ReceiveMoneyErrorLog.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public bool CheckTextCreditCardNew()
        {
            bool tf = false;
            if (ComboBoxType.SelectedIndex == 0)
                tf = true;
            if (TextAmountCreditCard.Text == "")
                tf = true;
            if (TextBoxCardNo.Text == "")
                tf = true;
            if (TextBoxCardName.Text == "")
                tf = true;
            if (txtDateEnd.Text.Trim() == "/")
                tf = true;
            return tf;
        }
        private void ClearDataCard()
        {
            ComboBoxType.SelectedIndex = 0;
            TextAmountCreditCard.Text = string.Empty;
            TextBoxCardNo.Text = string.Empty;
            TextBoxCardName.Text = string.Empty;
            txtDateEnd.Text = string.Empty;
            ComboBoxCa.SelectedIndex = 0;
        }
        public bool CheckTextCreditCard()
        {
            bool tf = false;
            ComboBoxType.Invoke(new MethodInvoker(delegate { if (ComboBoxType.SelectedIndex == 0) tf = true; }));
            TextAmountCreditCard.Invoke(new MethodInvoker(delegate { if (TextAmountCreditCard.Text == "") tf = true; }));
            TextBoxCardNo.Invoke(new MethodInvoker(delegate { if (TextBoxCardNo.Text == "") tf = true; }));
            TextBoxCardName.Invoke(new MethodInvoker(delegate { if (TextBoxCardName.Text == "") tf = true; }));
            txtDateEnd.Invoke(new MethodInvoker(delegate { if (txtDateEnd.Text.Trim() == "/") tf = true; }));
            return tf;
        }
        public void SelectPayment()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                DeleteListInfo();
                GetDataReportController getdata = new GetDataReportController();
                MyPaymentList _mypay = new MyPaymentList();
                List<paymentList> _paylst = new List<paymentList>();
                List<paymentDetailList> _paydetaillst = new List<paymentDetailList>();
                List<paymentMethodList> _paymethodlst = new List<paymentMethodList>();
                List<receiptList> receiptlst = new List<receiptList>();
                List<receiptDetailDtoList> receiptdetaillst = new List<receiptDetailDtoList>();
                List<ClassOTH> _water = new List<ClassOTH>();
                paymentList payWater = new paymentList();
                paymentList pay = new paymentList();
                decimal AmountCreditTotal = 0;
                decimal AmountCheqTotal = 0;
                decimal AmountTotal = 0;
                decimal AmountCredit = 0;
                decimal amount = 0;
                decimal TotalAmountCheq = 0;
                decimal TotalAmount = 0;
                string chequeNo = "";
                if (TextAmountCreditCard.Text != "")
                    TextAmountCreditCard.Invoke(new MethodInvoker(delegate { AmountCredit = Convert.ToDecimal(TextAmountCreditCard.Text); }));
                if (labelAmountTotalCheq.Text != "")
                    TotalAmountCheq = Convert.ToDecimal(labelAmountTotalCheq.Text);
                if (TextAmount.Text != "")
                    TotalAmount = Convert.ToDecimal(TextAmount.Text);
                #region ค่าไฟ               
                //paymentList ELE
                DateTime dueDate;
                pay.distId = GlobalClass.DistId;
                pay.cashierNo = Convert.ToInt32(GlobalClass.No);
                pay.cashierId = GlobalClass.UserId;
                pay.paymentTotalAmount = Convert.ToDouble(LabelAmount.Text.Trim());
                pay.paymentReceive = Convert.ToDouble(LabelAmountReceived.Text.Trim());
                pay.paymentChange = Convert.ToDouble(_RealChange);
                pay.paymentRealChange = Convert.ToDouble(LabelAmountChange.Text.Trim());
                dueDate = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                pay.paymentDate = dueDate.ToString("yyyy-MM-dd");
                pay.paymentChannel = 1;//Fix
                pay.payeeFlag = "N";
                pay.subDistId = GlobalClass.SubDistId;
                pay.userId = GlobalClass.UserId;
                pay.qNo = (qNo != null) ? qNo : null;
                pay.paymentServer = "ONLINE";
                #region NonMapCheque
                bool _Status = false;
                var lstInfo = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).OrderByDescending(c => c.StatusOrderBy).ToList();
                foreach (var item in lstInfo)
                {
                    List<ClassCheque> _lstCh = _cheque.Where(c => c.ChStatus == true).ToList();
                    if (_cheque.Count > 0)
                    {
                        foreach (var itemCh in _lstCh)
                        {
                            decimal total = item.EleTotalAmount + Convert.ToDecimal(item.Defaultpenalty);
                            decimal Colamount = Convert.ToDecimal(itemCh.Chamount);
                            ClassCheque cheque = _lstCh.Find(c => c.Chno.Equals(itemCh.Chno, StringComparison.CurrentCultureIgnoreCase));
                            if (total == Colamount)
                            {
                                inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(c => c.ca.Equals(item.ca, StringComparison.CurrentCultureIgnoreCase));
                                if (userInfo != null)
                                {
                                    if (userInfo.ChequeNo == null)
                                    {
                                        if (cheque != null)
                                            cheque.ChStatus = false;
                                        _Status = true;
                                        userInfo.StatusOrderBy = true;
                                        userInfo.ChequeNo = itemCh.Chno;
                                    }
                                }
                            }
                            if (cheque != null)
                                cheque.StatusCheque = true;
                        }
                    }
                }
                #endregion
                List<inquiryInfoBeanList> _lstCost = new List<inquiryInfoBeanList>();
                if (_Status)
                    _lstCost = GlobalClass.LstInfo.Where(c => c.Status == true).OrderByDescending(g => g.StatusOrderBy).ToList();
                else
                    _lstCost = GlobalClass.LstInfo.Where(c => c.Status == true).OrderByDescending(g => g.TotalAmount).ToList();
                foreach (var itemCost in _lstCost)
                {
                    List<inquiryGroupDebtBeanList> lstCostDetail = new List<inquiryGroupDebtBeanList>();
                    pay.collId = itemCost.collId;
                    if (itemCost.collId != null)
                        lstCostDetail = itemCost.inquiryGroupDebtBeanList.Where(c => c.status == true).OrderByDescending(c => c.StatusOrderBy).ToList();
                    else
                        lstCostDetail = itemCost.inquiryGroupDebtBeanList.Where(c => c.status == true && c.ca == itemCost.ca).OrderByDescending(c => c.StatusOrderBy).ToList();
                    int CountRow = GridDetail.Rows.Count - 1;
                    string ChequeNo = "";
                    foreach (var itemList in lstCostDetail)
                    {
                        decimal AmountBalance = 0;
                        bool status = false;
                        decimal paymentOld = 0;
                        decimal CountTotalAmount = 0;
                        List<paymentDetailSubList> _paySublst = new List<paymentDetailSubList>();
                        //paymentList==>paymentDetailList
                        paymentDetailList paydetail = new paymentDetailList();
                        paydetail.debtIdSet = itemList.debtIdSet;//Convert.ToInt32(itemList.debtIdSet);
                        paydetail.amount = Math.Round(itemList.debtBalance - Convert.ToDecimal(itemList.vatBalance), 2);
                        paydetail.percentVat = Convert.ToInt32(itemList.percentVat);
                        paydetail.vat = Convert.ToDecimal(itemList.vatBalance);
                        paydetail.totalAmount = Math.Round(Convert.ToDecimal(itemList.debtBalance), 2);
                        paydetail.intAmount = Math.Round(itemList.Defaultpenalty, 2);
                        paydetail.intDay = itemList.countDay;
                        paydetail.debtType = itemList.debtType;
                        paydetail.mwaTaxId = itemList.mwaTaxId;
                        paydetail.customerCode = itemList.customerCode;
                        paydetail.billDueDate = itemList.billDueDate;
                        paydetail.billNumber = itemList.billNumber;
                        paydetail.meaUnit = itemList.unit;
                        paydetail.check = itemList.check;
                        paydetail.custId = itemList.custId;
                        paydetail.ca = itemList.ca;
                        paydetail.ui = itemList.ui;
                        paydetail.price = itemList.price;
                        paydetail.itemId = itemList.itemId;
                        paydetail.payeeName = itemCost.payeeEleName;
                        paydetail.payeeAddress = itemCost.payeeEleAddress;
                        paydetail.accountCode = itemList.accountCode;
                        paydetail.unitCode = itemList.unitCode;
                        paydetail.itemDesc1 = itemList.itemDesc1;
                        paydetail.itemDesc2 = itemList.itemDesc2;
                        paydetail.itemDesc3 = itemList.itemDesc3;
                        paydetail.profitCenter = itemList.profitCenter;
                        paydetail.businessTypeId = itemList.businessTypeId;
                        paydetail.insDebtInsHdrId = itemList.insDebtInsHdrId;
                        CountTotalAmount = Convert.ToDecimal(itemList.debtBalance) + Convert.ToDecimal(itemList.Defaultpenalty);
                        if (AmountCredit > 0)
                        {
                            int index = 0;
                            ComboBoxCa.Invoke(new MethodInvoker(delegate { if (ComboBoxCa.SelectedIndex == 0) index = 1; }));
                            if (index == 1)
                            {
                                paymentDetailSubList paySub = new paymentDetailSubList();
                                paySub.paymentMethodId = 3;
                                decimal countTotal = 0;
                                countTotal = Convert.ToDecimal(itemList.debtBalance);
                                paySub.totalAmount = Math.Round(countTotal, 2);
                                paySub.cardNo = TextBoxCardNo.Text.Trim();
                                _paySublst.Add(paySub);
                                AmountCredit = AmountCredit - Convert.ToDecimal(itemList.debtBalance);
                                status = true;
                                AmountCreditTotal += Convert.ToDecimal(itemList.debtBalance);
                                if (AmountCredit <= 0)
                                {
                                    TotalAmount = Convert.ToDecimal(itemList.debtBalance) - countTotal;
                                    paymentOld = TotalAmount;
                                    status = false;
                                    AmountCredit = 0;
                                    AmountCreditTotal += TotalAmount;
                                }
                            }
                            else
                            {
                                string id = "";
                                ComboBoxCa.Invoke(new MethodInvoker(delegate { id = ComboBoxCa.Text.ToString(); }));
                                if (itemList.ca == id)
                                {
                                    paymentDetailSubList paySub = new paymentDetailSubList();
                                    paySub.paymentMethodId = 3;
                                    decimal countTotal = 0;
                                    countTotal = Convert.ToDecimal(itemList.debtBalance);
                                    paySub.totalAmount = Math.Round(countTotal, 2);
                                    paySub.cardNo = TextBoxCardNo.Text.Trim();
                                    _paySublst.Add(paySub);
                                    AmountCredit = AmountCredit - Convert.ToDecimal(itemList.debtBalance);
                                    status = true;
                                    AmountCreditTotal += Convert.ToDecimal(itemList.debtBalance);
                                    if (AmountCredit < 0)
                                    {
                                        TotalAmount = Convert.ToDecimal(itemList.debtBalance) - countTotal;
                                        paymentOld = TotalAmount;
                                        status = false;
                                        AmountCredit = 0;
                                        AmountCreditTotal += TotalAmount;
                                    }
                                }
                            }
                        }
                        if (TotalAmountCheq > 0 && TotalAmount >= 0 && AmountCredit >= 0)
                        {
                            if (GlobalClass.LstSub != null)
                            {
                                List<ClassPaymentCheque> _lstCheq = GlobalClass.LstSub.Where(c => c.StatusCheque == true && c.DebtId == itemList.debtId).ToList();
                                if (_lstCheq.Count > 0)
                                {
                                    foreach (ClassPaymentCheque item in _lstCheq)
                                    {
                                        AmountBalance += Convert.ToDecimal(item.Payed);
                                        paymentDetailSubList paySub = new paymentDetailSubList();
                                        paySub.paymentMethodId = 2;
                                        paySub.chequeNo = item.ChequeNo;
                                        paySub.totalAmount = Math.Round(item.Payed, 2);
                                        _paySublst.Add(paySub);
                                        status = true;
                                        ChequeNo = item.ChequeNo;
                                        TotalAmountCheq = Math.Round(TotalAmountCheq, 2) - Math.Round(item.Payed, 2);
                                    }
                                    decimal sumAmount = Convert.ToDecimal(itemList.debtBalance) - AmountBalance;
                                    if (sumAmount > 0)
                                    {
                                        int count = 0;
                                        int i = 0;
                                        decimal amountTotal = 0;
                                        List<ClassCheque> _lstCh = new List<ClassCheque>();
                                        for (i = 0; i <= count; i++)
                                        {
                                            _lstCh = _cheque.Where(c => c.StatusCheque == true && c.Balance > 0).OrderByDescending(c => c.ChStatus).ToList();
                                            foreach (ClassCheque item in _lstCh)
                                            {
                                                amountTotal += item.Balance;
                                                if (amount == 0)
                                                {
                                                    ClassCheque cheque = new ClassCheque();
                                                    cheque = _lstCh.Find(c => c.Chno.Equals(item.Chno, StringComparison.CurrentCultureIgnoreCase));
                                                    if (cheque != null)
                                                        cheque.StatusCheque = false;
                                                    amount = Convert.ToDecimal(cheque.Balance);
                                                    chequeNo = cheque.Chno;
                                                }
                                            }
                                            decimal debtBalance = 0;
                                            decimal countTotal = 0;
                                            if (paymentOld != 0)
                                                countTotal = paymentOld;
                                            else
                                                countTotal = Math.Round(sumAmount, 2);
                                            paymentDetailSubList paySub = new paymentDetailSubList();
                                            paySub.paymentMethodId = 2;
                                            paySub.chequeNo = chequeNo;
                                            if (countTotal > amount)
                                            {
                                                debtBalance = Math.Round(amount, 2);
                                                if (paymentOld != 0)
                                                    paymentOld = Math.Round(paymentOld, 2) - Math.Round(amount, 2);
                                                else
                                                    paymentOld = Math.Round(sumAmount, 2) - Math.Round(amount, 2);
                                                if (_lstCh.Count != 0)
                                                    TotalAmountCheq = amountTotal;
                                                decimal sumTotalAmount = amountTotal - amount;
                                                if (sumTotalAmount > 0)
                                                {
                                                    count++;
                                                    amount = 0;
                                                }
                                            }
                                            else
                                            {
                                                if (paymentOld != 0)
                                                {
                                                    debtBalance = Math.Round(paymentOld, 2);
                                                    amount = Math.Round(amount, 2) - Math.Round(paymentOld, 2);
                                                }
                                                else
                                                {
                                                    debtBalance = sumAmount;
                                                    amount = Math.Round(amount, 2) - Math.Round(sumAmount, 2);
                                                }
                                            }
                                            var lstPaysub = _paySublst.Find(c => c.chequeNo.Equals(chequeNo, StringComparison.CurrentCultureIgnoreCase));
                                            if (lstPaysub != null)
                                                lstPaysub.totalAmount = lstPaysub.totalAmount + Math.Round(debtBalance, 2);
                                            else
                                            {
                                                if (TotalAmountCheq > 0 && _lstCh.Count != 0)
                                                {
                                                    paySub.totalAmount = Math.Round(debtBalance, 2);
                                                    _paySublst.Add(paySub);
                                                }
                                            }
                                            TotalAmountCheq = Math.Round(TotalAmountCheq, 2) - Math.Round(debtBalance, 2);
                                            status = true;
                                            AmountCheqTotal += countTotal;

                                        }
                                        if (TotalAmountCheq < 0 || paymentOld > 0)
                                        {
                                            paymentDetailSubList paySub = new paymentDetailSubList();
                                            paySub.paymentMethodId = 1;
                                            paySub.totalAmount = Math.Round(sumAmount - amount, 2);
                                            _paySublst.Add(paySub);
                                            TotalAmount = Math.Round(TotalAmount, 2) - (sumAmount - amount);
                                            AmountTotal += sumAmount - Math.Round(amount, 2);
                                        }
                                    }
                                }
                                else
                                {
                                    status = true;
                                    paymentOld = itemList.debtBalance + Math.Round(itemList.Defaultpenalty, 2);
                                    List<ClassCheque> _lstCh = _cheque.Where(c => c.StatusCheque == true && c.Balance > 0).OrderByDescending(c => c.ChStatus).ToList();

                                    if (_lstCh.Count != 0 && TotalAmountCheq > 0)
                                    {
                                        ClassCheque clsCheque = _lstCh.OrderBy(c => c.Chno).FirstOrDefault();
                                        ClassCheque classCheque = _lstCh.Find(item => item.Chno.Equals(clsCheque.Chno, StringComparison.CurrentCultureIgnoreCase));
                                        decimal debtBalance = itemList.debtBalance;
                                        decimal payment;
                                        if (TotalAmountCheq > Math.Round(debtBalance, 2) && classCheque.Balance > Math.Round(debtBalance, 2))
                                        {
                                            payment = debtBalance;
                                            paymentOld = 0;
                                        }
                                        else
                                        {
                                            payment = classCheque.Balance;
                                            paymentOld = debtBalance - classCheque.Balance;
                                        }
                                        if (classCheque != null)
                                        {
                                            ChequeNo = classCheque.Chno;
                                            classCheque.Balance = classCheque.Balance - Math.Round(payment, 2);
                                        }
                                        paymentDetailSubList paySub = new paymentDetailSubList();
                                        paySub.paymentMethodId = 2;
                                        paySub.chequeNo = ChequeNo;
                                        paySub.totalAmount = Math.Round(payment, 2);
                                        _paySublst.Add(paySub);
                                        TotalAmountCheq = Math.Round(TotalAmountCheq, 2) - Math.Round(payment, 2);

                                    }
                                    if (TotalAmountCheq < 0 || paymentOld > 0)
                                    {
                                        paymentDetailSubList paySub = new paymentDetailSubList();
                                        paySub.paymentMethodId = 1;
                                        paySub.totalAmount = Math.Round(paymentOld, 2);
                                        _paySublst.Add(paySub);
                                        TotalAmount = Math.Round(TotalAmount, 2) - paymentOld;
                                        AmountTotal += Math.Round(paymentOld, 2);
                                    }
                                }
                            }
                            else
                            {
                                int count = 0;
                                int i = 0;
                                for (i = 0; i <= count; i++)
                                {
                                    List<ClassCheque> _lstCh = _cheque.Where(c => c.StatusCheque == true).OrderByDescending(c => c.ChStatus).ToList();
                                    foreach (var item in _lstCh)
                                    {
                                        if (amount == 0)
                                        {
                                            ClassCheque cheque = new ClassCheque();
                                            if (itemCost.StatusOrderBy)
                                                cheque = _lstCh.Find(c => c.Chno.Equals(itemCost.ChequeNo, StringComparison.CurrentCultureIgnoreCase));
                                            else
                                                cheque = _lstCh.Find(c => c.Chno.Equals(item.Chno, StringComparison.CurrentCultureIgnoreCase));

                                            if (cheque != null)
                                                cheque.StatusCheque = false;

                                            amount = Convert.ToDecimal(cheque.Chamount);
                                            chequeNo = cheque.Chno;
                                        }
                                    }
                                    decimal debtBalance = 0;
                                    paymentDetailSubList paySub = new paymentDetailSubList();
                                    paySub.paymentMethodId = 2;
                                    paySub.chequeNo = chequeNo;

                                    decimal countTotal = 0;
                                    if (paymentOld != 0)
                                        countTotal = paymentOld;
                                    else
                                        countTotal = CountTotalAmount;//Convert.ToDouble(itemList.debtBalance);
                                    if (countTotal > amount)
                                    {
                                        debtBalance = Math.Round(amount, 2);
                                        if (paymentOld != 0)
                                            paymentOld = Math.Round(paymentOld, 2) - Math.Round(amount, 2);
                                        else
                                            paymentOld = CountTotalAmount - Math.Round(amount, 2);// Math.Round(Convert.ToDouble(itemList.debtBalance), 2) - Math.Round(amount, 2);

                                        decimal sumAmount = TotalAmountCheq - amount;
                                        if (sumAmount > 0)//(TotalAmountCheq > countTotal)
                                        {
                                            count++;
                                            amount = 0;
                                        }
                                    }
                                    else
                                    {
                                        if (paymentOld != 0)
                                        {
                                            debtBalance = Math.Round(paymentOld, 2);
                                            amount = Math.Round(amount, 2) - Math.Round(paymentOld, 2);
                                        }
                                        else
                                        {
                                            debtBalance = CountTotalAmount;//Convert.ToDouble(itemList.debtBalance);
                                            amount = Math.Round(amount, 2) - CountTotalAmount;//Math.Round(Convert.ToDouble(itemList.debtBalance), 2);
                                        }
                                    }
                                    paySub.totalAmount = Math.Round(debtBalance, 2);
                                    _paySublst.Add(paySub);
                                    TotalAmountCheq = Math.Round(TotalAmountCheq, 2) - Math.Round(debtBalance, 2);
                                    status = true;
                                    AmountCheqTotal += countTotal;
                                }
                                if (TotalAmountCheq < 0 || paymentOld > 0)
                                {
                                    paymentDetailSubList paySub = new paymentDetailSubList();
                                    paySub.paymentMethodId = 1;
                                    paySub.totalAmount = Math.Round(paymentOld, 2);//Math.Round(Convert.ToDouble(itemList.debtBalance) - amount, 2);
                                    _paySublst.Add(paySub);
                                    TotalAmount = Math.Round(TotalAmount, 2) - paymentOld;//(Convert.ToDouble(itemList.debtBalance) - amount);
                                    AmountTotal += Math.Round(paymentOld, 2);// Convert.ToDouble(itemList.debtBalance) - Math.Round(amount, 2);
                                }
                            }
                        }
                        if (TotalAmount > 0 && status == false)
                        {
                            paymentDetailSubList paySub = new paymentDetailSubList();
                            paySub.paymentMethodId = 1;
                            paySub.totalAmount = CountTotalAmount;// Math.Round(Convert.ToDouble(itemList.debtBalance) + itemList.Defaultpenalty, 2);
                            _paySublst.Add(paySub);
                            TotalAmount = Math.Round(TotalAmount, 2) - CountTotalAmount;//(Convert.ToDouble(itemList.debtBalance) + itemList.Defaultpenalty);
                            AmountTotal += CountTotalAmount;//Convert.ToDouble(itemList.debtBalance);
                        }
                        paydetail.paymentDetailSubList = _paySublst;
                        _paydetaillst.Add(paydetail);
                    }
                }
                pay.paymentDetailList = _paydetaillst;//.OrderBy(c => c.debtId).ToList();
                decimal AmountCreditDetail = 0;
                decimal TotalAmountCheqDetail = 0;
                decimal TotalAmountDetail = 0;
                if (TextAmountCreditCard.Text != "")
                    AmountCreditDetail = Convert.ToDecimal(TextAmountCreditCard.Text);
                if (labelAmountTotalCheq.Text != "")
                    TotalAmountCheqDetail = Convert.ToDecimal(labelAmountTotalCheq.Text);
                if (TextAmount.Text != "")
                    TotalAmountDetail = Convert.ToDecimal(TextAmount.Text);
                if (AmountCreditDetail > 0)
                {
                    paymentMethodList paymethod = new paymentMethodList();
                    paymethod.paymentMethodId = 3;
                    paymethod.cardNo = TextBoxCardNo.Text;
                    paymethod.cardAmount = Math.Round(AmountCreditTotal, 2);
                    paymethod.cardOwner = TextBoxCardName.Text;
                    paymethod.cardStartDate = dueDate.ToString("yyyy-MM-dd");
                    string date = DateTime.Now.Day.ToString() + "/" + txtDateEnd.Text;
                    string dt = DateTime.Parse(date).ToString("yyyy-MM-dd", usCulture);
                    paymethod.cardEndDate = dt;
                    _paymethodlst.Add(paymethod);
                }
                if (TotalAmountCheqDetail > 0)
                {
                    int i = 0;
                    var loopTo = GridDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewTextBoxCell ChequeNoCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNo"];
                        DataGridViewTextBoxCell ChequeNumberCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNumber"];
                        DataGridViewTextBoxCell PayeeNameCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colPayeeName"];
                        DataGridViewTextBoxCell DateCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colDateNew"];
                        DataGridViewTextBoxCell AmountCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colAmount"];
                        DataGridViewTextBoxCell TelCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colTel"];
                        paymentMethodList paymethod = new paymentMethodList();
                        paymethod.paymentMethodId = 2;
                        paymethod.bankBranch = ChequeNoCell.EditedFormattedValue.ToString();
                        paymethod.chequeNo = ChequeNumberCell.EditedFormattedValue.ToString();
                        paymethod.chequeOwner = PayeeNameCell.EditedFormattedValue.ToString();
                        string date = DateCell.EditedFormattedValue.ToString();
                        DateTime dateTime = DateTime.Parse(date);
                        DateTime dt = Convert.ToDateTime(dateTime.ToString("yyyy-MM-dd", usCulture));
                        paymethod.chequeDate = dt.ToString("yyyy-MM-dd");
                        decimal amountCell = Convert.ToDecimal(AmountCell.EditedFormattedValue.ToString());
                        paymethod.chequeAmount = amountCell;
                        TotalAmountCheqDetail = Math.Round(TotalAmountCheqDetail, 2) - Math.Round(amountCell, 2);
                        paymethod.chequeBalance = (TotalAmountCheqDetail < 10) ? TotalAmountCheqDetail : 0;
                        paymethod.cashierCheque = "N";
                        paymethod.chequeApproveBy = null;
                        paymethod.chequeTel = TelCell.EditedFormattedValue.ToString();
                        _paymethodlst.Add(paymethod);
                    }
                }
                if (TotalAmountDetail > 0)
                {
                    paymentMethodList paymethod = new paymentMethodList();
                    paymethod.paymentMethodId = 1;
                    paymethod.cashTotalAmount = Math.Round(AmountTotal, 2);
                    paymethod.cashReceive = Math.Round(Convert.ToDecimal(TextAmount.Text), 2);//Math.Round(TotalAmount, 2);
                    _paymethodlst.Add(paymethod);
                }
                pay.paymentMethodList = _paymethodlst;
                //receipt 
                foreach (var itemRe in GlobalClass.LstInfo)
                {
                    var lst = itemRe.inquiryGroupDebtBeanList.Where(c => c.status == true && c.ca == itemRe.ca).OrderBy(g => g.ca).ToList();
                    var countType = lst.GroupBy(c => c.debtType).Select(g => new { DebtType = g.FirstOrDefault().debtType, Ca = g.FirstOrDefault().ca }).ToList();
                    foreach (var item in countType)
                    {
                        var lstCost = lst.Where(c => c.ca == item.Ca && c.debtType == item.DebtType).ToList();
                        List<receiptDetailDtoList> _lstreceipt = new List<receiptDetailDtoList>();
                        //paymentList==>receiptList
                        receiptList receipt = new receiptList();
                        receipt.receiptTypeId = 1;
                        receipt.receiptDebtType = item.DebtType;
                        receipt.receiptDate = dueDate.ToString("yyyy-MM-dd");
                        receipt.receiptPayer = itemRe.payeeEleName;
                        receipt.receiptPayerAddress = itemRe.payeeEleAddress;
                        receipt.receiptPayerTaxid = itemRe.payeeSerTax20;
                        receipt.receiptPayerBranch = itemRe.payeeEleTaxBranch;
                        receipt.distId = GlobalClass.DistId.ToString();
                        receipt.distName = GlobalClass.DistName;
                        receipt.receiptCustId = itemRe.custId;
                        receipt.receiptCustName = itemRe.Title + " " + itemRe.FirstName + " " + itemRe.LastName;
                        receipt.receiptReqAddress = itemRe.coAddress;
                        receipt.receiptCa = itemRe.ca;
                        receipt.receiptUi = itemRe.ui;
                        receipt.receiptAmount = lstCost.Sum(itemTotal => Convert.ToDouble(itemTotal.debtBalance) - Convert.ToDouble(itemTotal.vatBalance)).ToString();
                        receipt.receiptPercentVat = "7";
                        receipt.receiptVat = lstCost.Sum(itemTotal => Convert.ToDouble(itemTotal.vatBalance)).ToString();
                        receipt.receiptTotalAmount = lstCost.Sum(itemTotal => Convert.ToDouble(itemTotal.debtBalance)).ToString();
                        double Defaultpenalty = lstCost.Sum(itemTotal => Convert.ToDouble(itemTotal.Defaultpenalty));
                        receipt.receiptIntAmount = Defaultpenalty;
                        receipt.receiptIntTotalAmount = Math.Round(lstCost.Sum(itemTotal => Convert.ToDouble(itemTotal.debtBalance)), 2) + Math.Round(Defaultpenalty, 2);
                        receipt.receiptPayerFlag = itemRe.payeeFlag;
                        receipt.countCa = null;
                        receipt.countInvoice = lstCost.Count();
                        foreach (var itemRelst in lstCost)
                        {
                            receiptDetailDtoList receiptdetail = new receiptDetailDtoList();
                            //receiptList==>receiptDetailDtoList
                            receiptdetail.receiptDtlNo = null;
                            receiptdetail.receiptDtlName = itemRelst.reqDesc;
                            receiptdetail.receiptDtlAmount = Math.Round(Convert.ToDouble(itemRelst.debtBalance) - Convert.ToDouble(itemRelst.vatBalance), 2);
                            receiptdetail.receiptDtlVat = Convert.ToDouble(itemRelst.vatBalance);
                            receiptdetail.receiptDtlTotalAmount = Math.Round(Convert.ToDecimal(itemRelst.debtBalance), 2);
                            receiptdetail.lastBillingDate = itemRelst.schdate.ToString();
                            receiptdetail.receiptDtlUnit = itemRelst.kwh;
                            receiptdetail.intAmount = itemRelst.calTotalInt.ToString();
                            receiptdetail.intDay = itemRelst.calTotalIntDay;
                            receiptdetail.ft = itemRelst.ft;
                            Int64[][] num = new Int64[1][];
                            for (int i = 0; i < 1; i++)
                            {
                                num[i] = itemRelst.debtIdSet;
                            }
                            receiptdetail.debtIdSetList = num;
                            _lstreceipt.Add(receiptdetail);
                            //receiptdetaillst.Add(receiptdetail);
                        }
                        receipt.receiptDetailDtoList = _lstreceipt;// receiptdetaillst;
                        receiptlst.Add(receipt);
                    }
                }
                pay.receiptList = receiptlst;
                _paylst.Add(pay);
                _mypay.paymentList = _paylst;
                #endregion         
                _Paymentout = getdata.ReturnProcessPayment(_mypay);
                if (_Paymentout.OutputProcess.result_message == "SUCCESS")
                    _StatusSendData = true;
                else
                    _StatusSendData = false;
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("การส่งข้อมูลผิดพลาด", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                SetProgressBar(false);
                panel3.Enabled = false;
                panel6.Enabled = false;
                panel3.Invoke(new MethodInvoker(delegate { panel3.Enabled = true; }));
                panel6.Invoke(new MethodInvoker(delegate { panel6.Enabled = true; }));
            }
        }
        public bool UnconnectMeter()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                string strJSON = null;
                foreach (var item in GlobalClass.Cancel)
                {
                    ClassCancelContinue Cancel = new ClassCancelContinue();
                    Cancel.debtId = item.debtId;
                    Cancel.userId = item.userId;
                    string result = JsonConvert.SerializeObject(Cancel);
                    strJSON = ClassReadAPI.PostDataAPI("UnconnectMeter", result, GlobalClass.Timer);
                    meaLog.WriteData("UnconnectMeter :: Success :: Debt ID : " + item.debtId.ToString() + " |  User ID : " + GlobalClass.UserId, method, LogLevel.Debug);
                }
                GlobalClass.Cancel = null;
            }
            catch (Exception ex)
            {
                tf = false;
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return tf;
        }
        public bool ConnectMeter()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                string strJSON = null;
                foreach (var item in GlobalClass.ConnectMeter)
                {
                    ClassConnectMeter Cancel = new ClassConnectMeter();
                    Cancel.ca = item.ca;
                    Cancel.userId = item.userId;
                    string result = JsonConvert.SerializeObject(Cancel);
                    strJSON = ClassReadAPI.PostDataAPI("connectMeter", result, GlobalClass.Timer);
                    meaLog.WriteData("ConnectMeter :: Success :: CA : " + item.ca.ToString() + " |  User ID : " + GlobalClass.UserId, method, LogLevel.Debug);
                }
                GlobalClass.ConnectMeter = null;
            }
            catch (Exception ex)
            {
                tf = false;
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return tf;
        }
        public bool UpdateElectricityPenalty()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                string strJSON = null;
                foreach (var item in GlobalClass.CancelElectricity)
                {
                    ClassElectricityPenalty Cancel = new ClassElectricityPenalty();
                    Cancel.debtId = item.debtId;
                    Cancel.userId = item.userId;
                    Cancel.approveEmpId = item.approveEmpId;
                    string result = JsonConvert.SerializeObject(Cancel);
                    strJSON = ClassReadAPI.PostDataAPI("cancelInterest", result, GlobalClass.Timer);
                    meaLog.WriteData("CancelInterest :: Success :: Debt ID : " + item.debtId.ToString() + " |  User ID : " + GlobalClass.UserId, method, LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                tf = false;
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return tf;
        }
        public bool CancelProcessingFee()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                string strJSON = null;
                foreach (var item in GlobalClass.CancelProcessing)
                {
                    ClassCancelProcessingFee Cancel = new ClassCancelProcessingFee();
                    Cancel.debtId = item.debtId;
                    Cancel.userId = item.userId;
                    string result = JsonConvert.SerializeObject(Cancel);
                    strJSON = ClassReadAPI.PostDataAPI("cancelUnwireFee", result, GlobalClass.Timer);
                    meaLog.WriteData("CancelProcessing :: Success :: Debt ID : " + item.debtId.ToString() + " |  User ID : " + GlobalClass.UserId, method, LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                tf = false;
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return tf;
        }
        private void FormReceiveMoney_Load(object sender, EventArgs e)
        {
            TextAmount.Focus();
            LabelAmount.Text = Countamount.ToString("#,###,###,##0.00");
            SetControl();
            ComboBoxType.SelectedIndex = 0;
            TextBoxPayeeName.Text = CustomerName;
            CountItemReport();

        }
        public bool CheckStatus()
        {
            bool tf = false;
            inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.StatusLockDirectDebit == true).FirstOrDefault();
            if (info != null)
            {
                if (info.inquiryGroupDebtBeanList.Count > 1)
                {
                    inquiryGroupDebtBeanList detail = info.inquiryGroupDebtBeanList.Where(c => c.SelectCheck && c.debtLock != "Y").FirstOrDefault();
                    if (detail == null)
                        tf = true;
                }
                else
                {
                    inquiryInfoBeanList checkStatus = GlobalClass.LstInfo.Where(c => c.StatusLockDirectDebit == true).FirstOrDefault();
                    if (checkStatus != null)
                        tf = true;
                }
            }
            return tf;
        }
        public void CountItemReport()
        {
           if(GlobalClass.LstCost != null && GlobalClass.LstCost.Count != 0)
            {
                var LstCost = GlobalClass.LstCost.Where(c => c.status == true).GroupBy(g => new { g.debtType, g.infoCa }).ToList();
                label24.Text = LstCost.Count.ToString();
            }             
        }
        public void LoadDataCa()
        {
            try
            {
                List<inquiryInfoBeanList> UserInfo = GlobalClass.LstInfo.OrderBy(c => c.ca).ToList();
                List<inquiryInfoBeanList> _lstInfo = new List<inquiryInfoBeanList>();
                int i = 0;
                foreach (var item in UserInfo)
                {
                    if (i == 0)
                    {
                        inquiryInfoBeanList userinfo = new inquiryInfoBeanList();
                        userinfo.ca = "กรุณาเลือกบัญชีแสดงสัญญา";
                        _lstInfo.Add(userinfo);
                    }
                    inquiryInfoBeanList info = new inquiryInfoBeanList();
                    info.ca = item.ca;
                    _lstInfo.Add(info);
                    i++;
                }
                ComboBoxCa.DataSource = _lstInfo;
                ComboBoxCa.DisplayMember = "ca";
                ComboBoxCa.SelectedIndex = 0;
            }
            catch (Exception)
            {

            }
        }
        public void SetControl()
        {
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            GlobalClass.StatusFormMoney = 0;
            this.Hide();
        }
        public void CheckType()
        {
            if (GlobalClass.LstCost.Count > 0)
            {
                foreach (var item in GlobalClass.LstCost)
                {
                    inquiryGroupDebtBeanList lst = new inquiryGroupDebtBeanList();
                    if (item.debtType == "DIS")
                    {
                        //typeAmount
                    }
                }
            }
        }
        public bool CheckCheque()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                inquiryInfoBeanList lstInfo = GlobalClass.LstInfo.Where(c => c.SelectCheck == true && c.lockCheque != "" && c.lockCheque != null).FirstOrDefault();
                if (lstInfo != null && CheckCh.Checked == false)
                {
                    MessageBox.Show("บัญชีแสดงสัญญา " + lstInfo.ca + " ห้ามรับเช็ค", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    tf = true;
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return tf;
        }
        private void TextBoxB1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else
                e.Handled = true;
            if (e.KeyChar.Equals(Keys.Tab) || cInt == 13)
            {
                if (TextBoxChequeNo.Text.Length >= 7 && TextBoxChequeNo.Text.Length <= 9)
                    TextBoxBankBranch.Focus();
                else
                {
                    MsgBox.Show(this, "เลขที่เช็คต้องอยู่ระหว่าง 7 ถึง 9 หลัก", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    TextBoxChequeNo.Focus();
                }
            }
        }
        private void TextBoxNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                int cInt = Convert.ToInt32(e.KeyChar);
                if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                    e.Handled = false;
                else
                    e.Handled = true;

                if (cInt == 13)
                {
                    string name = "";
                    if (TextBoxBankBranch.Text != "")
                    {
                        name = arr.GetBankDesc(TextBoxBankBranch.Text);
                        if (name == null)
                        {
                            MsgBox.Show(this, "ไม่มีรหัสธนาคาร " + TextBoxBankBranch.Text + " ในฐานข้อมูล", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                            TextBoxBankBranch.Text = string.Empty;
                            TextBoxBankBranch.Focus();
                        }
                        else
                            DateTimePickerCheque.Focus();
                    }
                    else
                        TextBoxBankBranch.Focus();
                    TextBoxName.Text = name;
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MsgBox.Show(this, "ไม่สามารถค้นหาธนาคารได้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
            }
        }
        private void TextBoxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else
                e.Handled = true;
        }
        private void ComboBoxContract_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == Convert.ToChar(Keys.Enter))
                SendKeys.Send("{TAB}");
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            //int i;
            //if (GridDetail.RowCount == 0)
            //    return;
            DialogResult dialogResult = MessageBox.Show("ยืนยันการลบเลิกรายการ", "ลบรายการ", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;
            else
            {
                int rowIndex = GridDetail.CurrentCell.RowIndex;
                GridDetail.Rows.RemoveAt(rowIndex);
                //RemoveMapCheque();              
                //CalAmountCheque();
                ClearData();
                TextBoxChequeNo.Focus();
            }
        }
        private void ClearData()
        {
            TextAmountCheque.Text = "";
            TextBoxChequeNo.Text = "";
            DateTimePickerCheque.Text = DateTime.Now.ToString("dd/MM/yyyy").Trim();
            TextBoxName.Text = "";
            //TextBoxPayeeName.Text = "";
            TextBoxTel.Text = "";
            TextBoxBankBranch.Text = "";
            CheckCh.Checked = false;
            //TextBoxChequeNo.Enabled = true;
        }
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                decimal amount_New = TextAmountCheque.Text != "" ? Convert.ToDecimal(TextAmountCheque.Text) : 0;
                if (amount_New > 0)
                {
                    if (_Type == 1)
                    {
                        if (CheckCheque())
                            return;
                    }
                    if (CheckTextEmpty())
                        MsgBox.Show(this, "กรุณากรอกข้อมูลให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    else
                    {
                        if (CheckData())
                            MsgBox.Show(this, "ข้อมูลเลขที่เช็คและรหัสธนาคาร/สาขา ไม่สามารถซ้ำกันได้ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                        else
                        {
                            bool status = false;
                            double AmountCredit = 0;
                            double TotalAmountCheq = 0;
                            double TotalAmount = 0;
                            if (TextAmountCreditCard.Text != "")
                                AmountCredit = Convert.ToDouble(TextAmountCreditCard.Text);
                            if (labelAmountTotalCheq.Text != "")
                                TotalAmountCheq = Convert.ToDouble(labelAmountTotalCheq.Text);
                            if (TextAmount.Text != "")
                                TotalAmount = Convert.ToDouble(TextAmount.Text);

                            if (this.GridDetail.SelectedRows.Count > 0)
                            {
                                if (TotalAmount == 0 && AmountCredit == 0 && TotalAmountCheq > 0)
                                {
                                    CountAmountCheq();
                                    double amount = TextAmountCheque.Text != "" ? Convert.ToDouble(TextAmountCheque.Text) : 0;
                                    double amountSum = (TotalAmountCheq + amount) - Convert.ToDouble(LabelAmount.Text);
                                    if (amountSum > 10)
                                        status = true;
                                }
                            }
                            else
                            {
                                if (TotalAmount == 0 && AmountCredit == 0 && Convert.ToDouble(TextAmountCheque.Text) > 0)
                                {
                                    _amount = Convert.ToDecimal(TextAmountCheque.Text);
                                    double amountSum = Convert.ToDouble(TextAmountCheque.Text) - Convert.ToDouble(LabelAmount.Text);
                                    if (amountSum > 10)
                                        status = true;
                                }
                            }
                            if (status)
                            {
                                MsgBox.Show(this, "มูลค่าเช็คที่ชำระเกินจากจำนวนเงินที่ต้องชำระมากกว่า 10 บาทไม่สามารถรับได้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                                return;
                            }
                            else
                            {
                                AddChequeToList();
                                CountAmountCheq();
                                Summount();
                                ClearData();
                                if (labelAmountTotalCheq.Text != "0.00")
                                {
                                    ClearDataCard();
                                    groupBox7.Enabled = false;
                                }
                                else
                                    groupBox7.Enabled = true;
                                GridDetail.FirstDisplayedScrollingRowIndex = GridDetail.RowCount - 1;
                            }
                        }
                    }
                }
                else
                    MsgBox.Show(this, "ไม่สามารถเพิ่มเช็คที่ยอดเท่ากับ 0 ได้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            decimal amountNew = Convert.ToDecimal(label25.Text);
            if (amountNew < 0)
                TextAmount.Focus();
        }
        public void Summount(int? status = 0)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double totalAmount = 0;
                double amount = 0;
                double amountCheque = 0;
                double amountCreditCard = 0;
                double sumAmount = 0;
                if (TextAmount.Text != "")
                    amount = Convert.ToDouble(TextAmount.Text);
                if (labelAmountTotalCheq.Text != "0.00")
                    amountCheque = Convert.ToDouble(labelAmountTotalCheq.Text);
                if (TextAmountCreditCard.Text != "")
                    amountCreditCard = Convert.ToDouble(TextAmountCreditCard.Text);

                totalAmount = amount + amountCheque + amountCreditCard;
                LabelAmountReceived.Text = totalAmount.ToString("#,###,###,##0.00");
                if (totalAmount > 0)
                    sumAmount = totalAmount - Convert.ToDouble(LabelAmount.Text.Trim());
                if (sumAmount < 0)
                {
                    if (sumAmount > -0.12)
                    {
                        label25.Text = "0.00";
                        LabelAmountChange.Text = "00";
                        string[] lblamount = LabelAmount.Text.Split('.');
                        labelFraction.Text = "0." + lblamount[1].ToString();
                    }
                    else
                    {
                        label25.Text = sumAmount.ToString("#,###,###,##0.00");
                        LabelAmountChange.Text = "00";
                    }
                }
                else
                {
                    double NewAmount;
                    string Satang;
                    var AmountTotal = sumAmount.ToString("#,###,###,##0.00");
                    _RealChange = AmountTotal;
                    String[] datas = AmountTotal.Split('.');
                    Satang = arr.Fraction(Convert.ToInt32(datas[1].ToString()));
                    NewAmount = Convert.ToDouble(datas[0].ToString()) + Convert.ToDouble(Satang);
                    LabelAmountChange.Text = NewAmount.ToString("#,###,###,##0.00");
                    label25.Text = "0.00";
                    string[] splitSatang = Satang.ToString().Split('.');
                    if (splitSatang.Count() > 1)
                    {
                        decimal fraction = Convert.ToDecimal("0." + datas[1]) - Convert.ToDecimal(Satang);
                        var _amount = Math.Abs(fraction);
                        labelFraction.Text = _amount.ToString();
                    }
                    else
                    {
                        if (Satang != "0")
                        {
                            var SatangNew = 100 - Convert.ToDecimal(datas[1].ToString());
                            if (SatangNew < 10)
                                labelFraction.Text = "0.0" + SatangNew;
                            else
                                labelFraction.Text = "0." + SatangNew;
                        }
                        else if (Satang == "0")
                        {
                            string[] lblamount = _RealChange.Split('.');
                            labelFraction.Text = "0." + lblamount[1];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public void SummountCheque()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double totalAmount = 0;
                double amount = 0;
                double amountCheque = 0;
                double amountCreditCard = 0;
                double sumAmount = 0;
                if (TextAmount.Text != "")
                    amount = Convert.ToDouble(TextAmount.Text);
                if (labelAmountTotalCheq.Text != "0.00" || TextAmountCheque.Text != "")
                {
                    amountCheque = Convert.ToDouble(labelAmountTotalCheq.Text) + Convert.ToDouble(TextAmountCheque.Text);
                }
                if (TextAmountCreditCard.Text != "")
                    amountCreditCard = Convert.ToDouble(TextAmountCreditCard.Text);

                totalAmount = amount + amountCheque + amountCreditCard;
                LabelAmountReceived.Text = totalAmount.ToString("#,###,###,##0.00");
                if (totalAmount > 0)
                    sumAmount = totalAmount - Convert.ToDouble(LabelAmount.Text.Trim());
                if (sumAmount < 0)
                {
                    if (sumAmount > -0.12)
                    {
                        label25.Text = "0.00";
                        LabelAmountChange.Text = "00";
                        string[] lblamount = LabelAmount.Text.Split('.');
                        labelFraction.Text = "0." + lblamount[1].ToString();
                    }
                    else
                    {
                        label25.Text = sumAmount.ToString("#,###,###,##0.00");
                        LabelAmountChange.Text = "00";
                    }
                }
                else
                {
                    double NewAmount;
                    string Satang;
                    var AmountTotal = sumAmount.ToString("#,###,###,##0.00");
                    _RealChange = AmountTotal;
                    String[] datas = AmountTotal.Split('.');
                    Satang = arr.Fraction(Convert.ToInt32(datas[1].ToString()));
                    NewAmount = Convert.ToDouble(datas[0].ToString()) + Convert.ToDouble(Satang);
                    LabelAmountChange.Text = NewAmount.ToString("#,###,###,##0.00");
                    label25.Text = "0.00";
                    string[] splitSatang = Satang.ToString().Split('.');
                    if (splitSatang.Count() > 1)
                    {
                        decimal fraction = Convert.ToDecimal("0." + datas[1]) - Convert.ToDecimal(Satang);
                        var _amount = Math.Abs(fraction);
                        labelFraction.Text = _amount.ToString();
                    }
                    else
                    {
                        if (Satang != "0")
                        {
                            var SatangNew = 100 - Convert.ToDecimal(datas[1].ToString());
                            if (SatangNew < 10)
                                labelFraction.Text = "0.0" + SatangNew;
                            else
                                labelFraction.Text = "0." + SatangNew;
                        }
                        else if (Satang == "0")
                        {
                            string[] lblamount = _RealChange.Split('.');
                            labelFraction.Text = "0." + lblamount[1];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public void CountAmountCheq()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double Amount = 0;
                int i = 0;
                var loopTo = GridDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    Image image = Properties.Resources.Recycle;
                    GridDetail.Rows[i].Cells["ColDelete"].Value = image;
                    Image Edit = Properties.Resources.icons8_edit_32;
                    GridDetail.Rows[i].Cells["ColEdit"].Value = Edit;
                    Amount += Convert.ToDouble(GridDetail.Rows[i].Cells["colAmount"].Value);
                }
                labelAmountTotalCheq.Text = Amount.ToString("#,###,###,##0.00");
                labelAmountCheq.Text = Amount.ToString("#,###,##0.00");
                _amount = Convert.ToDecimal(TextAmountCheque.Text);
                TextAmountCheque.Text = string.Empty;
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private bool CheckTextEmpty()
        {
            bool status = false;
            if (TextBoxChequeNo.Text == string.Empty)
                status = true;
            if (TextBoxBankBranch.Text == string.Empty)
                status = true;
            if (TextBoxName.Text == string.Empty)
                status = true;
            if (DateTimePickerCheque.Text == string.Empty)
                status = true;
            if (TextAmountCheque.Text == string.Empty)
                status = true;
            if (TextBoxPayeeName.Text == string.Empty)
                status = true;
            if (TextBoxTel.Text == string.Empty)
                status = true;
            return status;
        }
        private bool CheckData()
        {
            bool tf;
            var checkData = _cheque.Where(c => c.Chno == TextBoxChequeNo.Text).FirstOrDefault();
            if (checkData != null)
                tf = true;
            else
                tf = false;
            return tf;
        }
        public void AddChequeToList()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (_cheque != null && _cheque.Count > 0)
                {
                    int i = 1;
                    List<ClassCheque> _lst = new List<ClassCheque>();
                    foreach (var item in _cheque)
                    {
                        ClassCheque cheque = new ClassCheque();
                        cheque.Chno = item.Chno;
                        cheque.Bankno = item.Bankno;
                        cheque.Bankname = item.Bankname;
                        cheque.Chdate = item.Chdate;
                        cheque.Chamount = item.Chamount;
                        cheque.Chname = item.Chname;
                        cheque.Tel = item.Tel;
                        cheque.Chcheck = item.Chcheck;
                        cheque.ChStatus = true;
                        cheque.Status = item.Status;
                        cheque.ChdateNew = item.ChdateNew;
                        cheque.No = i.ToString();
                        i++;
                        _lst.Add(cheque);
                    }
                    ClassCheque chequeNew = new ClassCheque();
                    chequeNew.Chno = TextBoxChequeNo.Text.Trim();
                    chequeNew.Bankno = TextBoxBankBranch.Text.Trim();
                    chequeNew.Bankname = TextBoxName.Text.Trim();
                    chequeNew.Chdate = DateTimePickerCheque.Text.ToString();
                    chequeNew.ChdateNew = DateTimePickerCheque.Value.Date.ToString("dd/MM/yyyy");
                    chequeNew.Chamount = _amount;//.ToString("#,###,###,##0.00"); //TextAmountCheque.Text.Trim();
                    chequeNew.Chname = TextBoxPayeeName.Text.Trim();
                    string tel = txtTel.Text == "" ? TextBoxTel.Text.Replace("-", "").Trim() : TextBoxTel.Text.Replace("-", "").Trim() + "#" + txtTel.Text.Trim();
                    chequeNew.Tel = tel;
                    chequeNew.Chcheck = CheckCh.Checked;
                    chequeNew.ChStatus = true;
                    chequeNew.No = i.ToString();
                    _lst.Add(chequeNew);
                    _cheque = _lst;
                }
                else
                {
                    ClassCheque cheque = new ClassCheque();
                    cheque.Chno = TextBoxChequeNo.Text.Trim();
                    cheque.Bankno = TextBoxBankBranch.Text.Trim();
                    cheque.Bankname = TextBoxName.Text.Trim();
                    cheque.Chdate = DateTimePickerCheque.Text.ToString();
                    cheque.ChdateNew = DateTimePickerCheque.Value.Date.ToString("dd/MM/yyyy");
                    cheque.Chamount = _amount;//.ToString();// TextAmountCheque.Text.Trim();
                    cheque.Chname = TextBoxPayeeName.Text.Trim();
                    string tel = txtTel.Text == "" ? TextBoxTel.Text.Replace("-", "").Trim() : TextBoxTel.Text.Replace("-", "").Trim() + "#" + txtTel.Text.Trim();
                    cheque.Tel = tel;
                    cheque.Chcheck = CheckCh.Checked;
                    cheque.ChStatus = true;
                    cheque.No = "1";
                    _cheque.Add(cheque);
                    lblNo.Text = "1";
                }
                this.GridDetail.DataSource = _cheque;
                GlobalClass.LstCheque = _cheque;
                foreach (var item in GlobalClass.LstCheque)
                {
                    CountamountAll += Convert.ToDouble(item.Chamount);
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void GridDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                FormMapCheque detail = new FormMapCheque();
                detail.labelNumber.Text = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                detail.labelNo.Text = GridDetail.CurrentRow.Cells["ColNo"].Value.ToString();
                detail.labelName.Text = GridDetail.CurrentRow.Cells["ColName"].Value.ToString();
                detail.labelDate.Text = GridDetail.CurrentRow.Cells["ColDate"].Value.ToString();
                string strbalance = GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString();
                decimal balance = Convert.ToDecimal(strbalance);
                detail.labelAmount.Text = balance.ToString("#,###,###,##0.00");
                detail.labelPayeeName.Text = GridDetail.CurrentRow.Cells["ColPayeeName"].Value.ToString();
                detail.labelTel.Text = GridDetail.CurrentRow.Cells["ColTel"].Value.ToString();
                detail.CheckCh.Checked = Convert.ToBoolean(GridDetail.CurrentRow.Cells["colchCash"].Value.ToString());
                detail.labelBalance.Text = (GridDetail.CurrentRow.Cells["ColBalance"].Value.ToString() != "") ? GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString() : GridDetail.CurrentRow.Cells["ColBalance"].Value.ToString();
                detail.ShowDialog();
                detail.Dispose();
                LoadData();
                _amountCheque = balance;
            }
            else if (e.ColumnIndex == 1)
            {
                if (MessageBox.Show("คุณต้องการยกเลิกรายการนี้หรือไม่ ", "ยกเลิกเช็ค", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                else
                {
                    if (_cheque != null)
                    {
                        string no = GridDetail.CurrentRow.Cells["ColNo"].Value.ToString();
                        ClassCheque cheque = _cheque.Where(c => c.No == no && c.Status == true).FirstOrDefault();
                        if (cheque == null)
                        {

                        }
                        int rowIndex = GridDetail.CurrentCell.RowIndex;
                        _cheque.RemoveAt(rowIndex);
                        this.GridDetail.DataSource = null;

                        if (_cheque.Count > 0)
                            this.GridDetail.DataSource = _cheque;

                        CountAmountCheq();
                        Summount();
                        ClearData();
                        this.GridDetail.Update();
                        this.GridDetail.Refresh();
                        ButtonAdd.Enabled = true;
                    }
                    if (this.GridDetail.RowCount > 0)
                        groupBox7.Enabled = false;
                    else
                        groupBox7.Enabled = true;
                }
            }
            else if (e.ColumnIndex == 2)
            {
                //if (Convert.ToDecimal(labelAmountCheq.Text) >= 0)
                //{
                //    labelAmountCheq.Text = (Convert.ToDecimal(labelAmountCheq.Text) - Convert.ToDecimal(GridDetail.CurrentRow.Cells["ColAmount"].Value)).ToString();
                //    labelAmountTotalCheq.Text = (Convert.ToDecimal(labelAmountTotalCheq.Text) - Convert.ToDecimal(GridDetail.CurrentRow.Cells["ColAmount"].Value)).ToString();
                //}
                TextBoxChequeNo.Text = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                TextBoxBankBranch.Text = GridDetail.CurrentRow.Cells["ColNo"].Value.ToString();
                TextBoxName.Text = GridDetail.CurrentRow.Cells["ColName"].Value.ToString();
                DateTimePickerCheque.Text = GridDetail.CurrentRow.Cells["colDateNew"].Value.ToString();
                TextAmountCheque.Text = GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString();
                TextBoxPayeeName.Text = GridDetail.CurrentRow.Cells["ColPayeeName"].Value.ToString();
                if (GridDetail.CurrentRow.Cells["ColTel"].Value.ToString().Length > 10)
                {
                    string[] strTel = GridDetail.CurrentRow.Cells["ColTel"].Value.ToString().Split('#');
                    TextBoxTel.Text = strTel[0].ToString();
                    txtTel.Text = strTel[1].ToString();
                }
                else
                    TextBoxTel.Text = GridDetail.CurrentRow.Cells["ColTel"].Value.ToString();
                CheckCh.Checked = Convert.ToBoolean(GridDetail.CurrentRow.Cells["colchCash"].Value.ToString());
                lblNo.Text = GridDetail.CurrentRow.Cells["ColNoId"].Value.ToString();
                ButtonUpate.Enabled = true;
                ButtonAdd.Enabled = false;
            }
            else
            {
                ButtonUpate.Enabled = false;
                ButtonAdd.Enabled = true;
            }
        }
        public void LoadData()
        {
            if (GlobalClass.LstCheque != null)
                GridDetail.DataSource = GlobalClass.LstCheque;
        }
        private void TextAmount_TextChanged(object sender, EventArgs e)
        {
            TextBox T = (TextBox)sender;
            try
            {
                if (T.Text != string.Empty)
                {
                    ClearDataCard();
                    groupBox7.Enabled = false;
                }
                else
                    groupBox7.Enabled = true;

                double checkType = double.Parse(T.Text);
                string[] SplitData = T.Text.Split('.');
                if (SplitData.Count() == 1)
                {
                    if (Regex.IsMatch(T.Text, "^[0-9]"))
                        Summount();
                }
                else
                {
                    for (int i = 0; i < SplitData.Count(); i++)
                    {
                        if (i != 0 && SplitData[i] != "")
                        {
                            if (int.Parse(SplitData[i].ToString()) == 25 || int.Parse(SplitData[i].ToString()) == 50 || int.Parse(SplitData[i].ToString()) == 75)
                            {
                                if (Regex.IsMatch(T.Text, "^[0-9]"))
                                    Summount();
                            }
                            else
                            {
                                if (SplitData[1].Length == 2)
                                {
                                    int CursorIndex = T.SelectionStart - 1;
                                    T.Text = T.Text.Remove(CursorIndex, 1);
                                    T.SelectionStart = CursorIndex;
                                    T.SelectionLength = 0;
                                    MessageBox.Show("จำนวนเงินสตางค์ ต้องเป็น 25,50 หรือ 75 เท่านั้น");
                                }
                            }
                        }
                    }
                }
                if (T.Text == "" && TextAmountCreditCard.Text == "" && labelAmountTotalCheq.Text == "0.00")
                    Summount();
            }
            catch (Exception)
            {
                try
                {
                    int CursorIndex = T.SelectionStart - 1;
                    T.Text = T.Text.Remove(CursorIndex, 1);
                    T.SelectionStart = CursorIndex;
                    T.SelectionLength = 0;
                }
                catch (Exception)
                {
                    if (T.Text == "")
                    {
                        label25.Text = "00";
                        LabelAmountReceived.Text = "00";
                        LabelAmountChange.Text = "00";
                    }
                }
            }
        }
        public void SetDataListGridview()
        {
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                ClassListGridview grid = new ClassListGridview();
                DataGridViewTextBoxCell amountChequeCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colAmount"];
                DataGridViewTextBoxCell ChequeNoCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNumber"];
                grid.ChNo = ChequeNoCell.EditedFormattedValue.ToString();
                grid.ChAmount = amountChequeCell.EditedFormattedValue.ToString();
                _lstGrid.Add(grid);
            }
        }
        private void ComboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxType.SelectedIndex != 0)
            {
                TextAmountCreditCard.Text = LabelAmount.Text;
                LoadDataCa();
                Summount();
                ComboBoxCa.Focus();
            }
            if (ComboBoxType.SelectedIndex == 0)
            {
                TextAmountCreditCard.Text = string.Empty;
                ComboBoxCa.SelectedIndex = 0;
            }
            TextAmount.Text = string.Empty;
        }
        private void ComboBoxCa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxCa.SelectedIndex == 0)
            {
                if (ComboBoxType.SelectedIndex != 0)
                    TextAmountCreditCard.Text = LabelAmount.Text;
            }
            else
            {
                inquiryInfoBeanList UserInfo = GlobalClass.LstInfo.Where(c => c.ca == ComboBoxCa.Text).FirstOrDefault();
                if (UserInfo.EleTotalAmount.ToString() != "0")
                {
                    decimal totalAmount = UserInfo.EleTotalAmount + Convert.ToDecimal(UserInfo.Defaultpenalty);
                    TextAmountCreditCard.Text = Math.Round(totalAmount, 2).ToString("#,###,###,##0.00");
                }
                TextBoxCardNo.Focus();
            }
        }
        private void TextAmountCreditCard_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(TextAmountCreditCard.Text, "^[0-9]"))
                Summount();
        }
        private void ButtonApprove_Click(object sender, EventArgs e)
        {

        }
        private void GridDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }
        private void ButtonUpate_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (!CheckCh.Checked)
                {
                    if (CheckCheque())
                        return;
                }
                if (CheckTextEmpty())
                    MsgBox.Show(this, "กรุณากรอกข้อมูลให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                else
                {
                    bool status = false;
                    double AmountCredit = 0;
                    double TotalAmountCheq = 0;
                    double TotalAmount = 0;
                    if (TextAmountCreditCard.Text != "")
                        AmountCredit = Convert.ToDouble(TextAmountCreditCard.Text);
                    if (labelAmountTotalCheq.Text != "")
                        TotalAmountCheq = Convert.ToDouble(labelAmountTotalCheq.Text);
                    if (TextAmount.Text != "")
                        TotalAmount = Convert.ToDouble(TextAmount.Text);
                    if (this.GridDetail.SelectedRows.Count > 0)
                    {
                        if (TotalAmount == 0 && AmountCredit == 0 && TotalAmountCheq > 0)
                        {
                            decimal amountSum;
                            decimal amount = _cheque.Where(c => c.No != lblNo.Text.ToString()).Sum(c => Convert.ToDecimal(c.Chamount));
                            if (amount == 0)
                                amountSum = Convert.ToDecimal(TextAmountCheque.Text) - Convert.ToDecimal(LabelAmount.Text);
                            else
                                amountSum = (amount + Convert.ToDecimal(TextAmountCheque.Text)) - Convert.ToDecimal(LabelAmount.Text);

                            if (amountSum > 10)
                                status = true;
                        }
                    }
                    else
                    {
                        if (TotalAmount == 0 && AmountCredit == 0 && Convert.ToDouble(TextAmountCheque.Text) > 0)
                        {
                            double amountSum = Convert.ToDouble(TextAmountCheque.Text) - Convert.ToDouble(LabelAmount.Text);
                            if (amountSum > 10)
                                status = true;
                        }
                    }
                    if (status)
                    {
                        MsgBox.Show(this, "มูลค่าเช็คที่ชำระเกินจากจำนวนเงินที่ต้องชำระมากกว่า 10 บาทไม่สามารถรับได้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                        TextAmountCheque.Text = _amountCheque.ToString("#,###,###,###.##");
                        return;
                    }
                    else
                    {
                        ClassCheque cheque = _cheque.Find(item => item.No.Equals(lblNo.Text.ToString(), StringComparison.CurrentCultureIgnoreCase));
                        if (cheque != null)
                        {
                            cheque.Bankno = TextBoxBankBranch.Text;
                            cheque.Bankname = TextBoxName.Text;
                            cheque.Chdate = DateTimePickerCheque.Text;
                            cheque.Chamount = Convert.ToDecimal(TextAmountCheque.Text);
                            cheque.Chname = TextBoxPayeeName.Text;
                            cheque.ChdateNew = DateTimePickerCheque.Value.Date.ToString("dd/MM/yyyy");
                            cheque.Tel = TextBoxTel.Text.Replace("-", "").Trim();
                            cheque.Chcheck = CheckCh.Checked;
                            cheque.Chno = TextBoxChequeNo.Text;
                        }
                        this.GridDetail.DataSource = null;
                        this.GridDetail.DataSource = _cheque;
                        CountAmountCheq();
                        this.GridDetail.Update();
                        this.GridDetail.Refresh();
                        ButtonUpate.Enabled = false;
                        ButtonAdd.Enabled = true;
                        Summount();
                        ClearData();
                    }
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void TextBoxPayeeName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //int cInt = Convert.ToInt32(e.KeyChar);
            //if (cInt == 13)
            //    TextBoxTel.Focus();
        }
        private void DateTimePickerCheque_ValueChanged(object sender, EventArgs e)
        {

        }
        public void SetProgressBar(bool status)
        {
            if (status == true)
            {
                pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Location = new Point(361, 163); }));
                pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Size = new Size(330, 293); }));
                pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.BackColor = Color.FromArgb(125, Color.WhiteSmoke); }));
            }
            else
            {
                pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Location = new Point(212, 0); }));
                pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Size = new Size(0, 0); }));
            }
            //panel3.Invoke(new MethodInvoker(delegate { panel3.Enabled = !status; }));
            //panel6.Invoke(new MethodInvoker(delegate { panel6.Enabled = !status; }));
            pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Visible = status; }));
        }
        public void ThreadStart()
        {
            threadBar = new Thread(new ThreadStart(LodaProgressBar));
            threadData = new Thread(new ThreadStart(SendDataAndPrintData));
            threadData.Start();
            threadBar.Start();
        }
        public void LodaProgressBar()
        {
            SetProgressBar(true);
        }
        public void SendDataAndPrintData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (GlobalClass.CancelElectricity != null)
                    UpdateElectricityPenalty();
                if (GlobalClass.CancelProcessing != null)
                    CancelProcessingFee();
                SelectPayment();
                if (GlobalClass.ConnectMeter != null)
                    ConnectMeter();
                if (GlobalClass.Cancel != null)
                    UnconnectMeter();

                string ReceiptNo = "";
                if (_StatusSendData)
                {
                    int countReport = 0;
                    if (_Paymentout.OutputProcess.paymentResponseDtoList != null)
                    {
                        string str1 = "ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์เมื่อเรียกเก็บเงินตามเช็คได้แล้ว";
                        string str2 = "This receipt is not valid until the cheque is honored by the bank.";
                        string str3 = "";//"เอกสารนี้ได้จัดทำและส่งข้อมูลให้แก่กรมสรรพากรด้วยวิธีการทางอิเล็กทรอนิกส์";
                        string str4 = "เลขบัตรเครดิต (Credit Card No.) ";
                        foreach (var item in _Paymentout.OutputProcess.paymentResponseDtoList)
                        {
                            countReport = item.payTReceiptList.Count;
                            int ca = 1;
                            var lstOrderBy = item.payTReceiptList.OrderBy(c => c.receiptCa);
                            foreach (var itemlist in lstOrderBy)
                            {
                                int statusVat = 0;
                                string Path = "";
                                string receiptDtlName = "";
                                List<ClassAmount> _lstamount = new List<ClassAmount>();
                                List<ClassModelsReport> _lstModels = new List<ClassModelsReport>();
                                List<ClassModelsReportDetail> _lstDetail = new List<ClassModelsReportDetail>();
                                ReceiptNo += itemlist.receiptNo + Environment.NewLine;
                                bool status = false;
                                ClassAmount lstamount = new ClassAmount();
                                decimal TotalAmount = 0;
                                ClassModelsReport report = new ClassModelsReport();
                                DateTime dt = DateTime.Now;
                                DateTime dtNow = DateTime.Now;
                                string str = arr.PrintDate(dt.Month);
                                if (dt.Year < 2500)
                                    dtNow = dt.AddYears(543);
                                report.strDate = dt.Day.ToString() + " " + str + " " + dtNow.Year.ToString();
                                report.No = itemlist.receiptNo != null ? itemlist.receiptNo : "";
                                report.DistName = itemlist.distName != null ? itemlist.distName : "";
                                report.CustomerName = itemlist.receiptPayer != null ? itemlist.receiptPayer : "";
                                report.CustomerTaxId = itemlist.receiptPayerTaxid != null ? itemlist.receiptPayerTaxid : "";
                                report.CustomerTaxBranch = itemlist.receiptPayerBranch != null ? itemlist.receiptPayerBranch : "-";
                                report.CustomerAddress = itemlist.receiptPayerAddress != null ? itemlist.receiptPayerAddress : "";
                                report.Name = itemlist.receiptCustName != null ? itemlist.receiptCustName : "";
                                report.CoAddress = itemlist.receiptReqAddress != null ? itemlist.receiptReqAddress : "";
                                report.UI = itemlist.receiptUi != null ? itemlist.receiptUi.ToString() : "";
                                report.CA = itemlist.receiptCa != null ? itemlist.receiptCa.ToString() : "";
                                report.EmpId = itemlist.cashierId != null ? itemlist.cashierId : "";
                                report.EmpName = itemlist.cashierName != null ? itemlist.cashierName : "";
                                report.PayNo = itemlist.receiptRefNo != null ? itemlist.receiptRefNo.ToString() : "";
                                report.ReceiptBranch = "";
                                report.CusType = "";
                                report.countCa = itemlist.countCa;
                                report.countInv = itemlist.countInv;
                                if (itemlist.debtType == "MWA")
                                    report.AccountNo = itemlist.mwaAccountNo;
                                foreach (var itemlistDetail in itemlist.responseReceiptDtlBeanList.OrderByDescending(c => c.lastBillingDate))
                                {
                                    ClassModelsReportDetail detail = new ClassModelsReportDetail();
                                    int i = 0;
                                    int year = 0;
                                    int day = 0;
                                    int mouth = 0;
                                    DateTime date = new DateTime();
                                    detail.invNo = itemlistDetail.receiptDtlNo;
                                    detail.amount = itemlistDetail.receiptDtlAmount;
                                    detail.vatBalance = itemlistDetail.receiptDtlVat != null ? Convert.ToDouble(itemlistDetail.receiptDtlVat).ToString("#,###,###,##0.00") : "";
                                    detail.debtBalance = Convert.ToDouble(itemlistDetail.receiptDtlTotalAmount).ToString("#,###,###,##0.00");
                                    if (itemlist.debtType == "ELE" && itemlist.receiptTypeId != 3)
                                    {
                                        if (itemlistDetail.lastBillingDate != null && itemlistDetail.lastBillingDate != "")
                                        {
                                            String[] dataDate = itemlistDetail.lastBillingDate.Split('-');
                                            foreach (var itemDate in dataDate)
                                            {
                                                if (i == 0)
                                                    year = int.Parse(itemDate);
                                                else if (i == 1)
                                                    mouth = int.Parse(itemDate);
                                                else
                                                    day = int.Parse(itemDate);
                                                i++;
                                            }
                                            if (year < 2500)
                                                year = year + 543;
                                            date = new DateTime(year, mouth, day);
                                            //if (date.Year < 2500)
                                            //    date.AddYears(543);
                                        }
                                        if (itemlistDetail.lastBillingDate != "" && itemlistDetail.lastBillingDate != null)
                                            detail.schdate = date.ToString("dd/MM/yyyy");
                                    }
                                    if (itemlist.debtType == "GUA")
                                        detail.invNo = itemlistDetail.receiptDtlNo;
                                    //if (itemlist.debtType == "ADV")
                                    //    detail.amount = itemlistDetail.receiptDtlAmount;
                                    if (itemlist.debtType == "MWA")
                                        detail.invNo = itemlistDetail.mwaInvoiceNo;
                                    detail.totalkWh = Convert.ToDouble(itemlistDetail.receiptDtlUnit).ToString("#,###,###,###");
                                    detail.countDay = itemlistDetail.intDay.Value;
                                    detail.Defaultpenalty = itemlistDetail.intAmount;
                                    detail.ftRate = itemlistDetail.ft != null ? itemlistDetail.ft.ToString() : "";
                                    detail.DetailDesc = itemlistDetail.receiptDtlName;
                                    detail.Branch = itemlistDetail.mwaBranch;
                                    receiptDtlName += "  " + itemlistDetail.receiptDtlName;
                                    if (itemlist.debtType == "OTH")
                                    {
                                        //detail.invNo = itemlistDetail.receiptDtlNo;
                                        report.CA = ca++.ToString();
                                        detail.Price = itemlistDetail.itemPriceWithVat != null ? itemlistDetail.itemPriceWithVat.Value : 0;
                                        if (itemlistDetail.receiptDtlVat != null && itemlistDetail.receiptDtlVat != 0)
                                            statusVat = 0;
                                        else
                                            statusVat = 1;
                                    }
                                    ///Report CreditNote
                                    detail.RefReceiptId = itemlistDetail.refReceiptId != null ? itemlistDetail.refReceiptId : "";
                                    detail.RefReceiptNo = itemlistDetail.refReceiptNo != null ? itemlistDetail.refReceiptNo : "";
                                    detail.RefReceiptAmount = itemlistDetail.refReceiptAmount != null ? itemlistDetail.refReceiptAmount.Value : 0;
                                    detail.RefOldReceiptAmount = itemlistDetail.refOldReceiptAmount != null ? itemlistDetail.refOldReceiptAmount.Value : 0;
                                    detail.RefReceiptSumAmount = itemlistDetail.refReceiptSumAmount != null ? itemlistDetail.refReceiptSumAmount.Value : 0;
                                    detail.RefReceiptVat = itemlistDetail.refReceiptVat != null ? itemlistDetail.refReceiptVat.Value : 0;
                                    detail.RefReceiptTotalAmount = itemlistDetail.refReceiptTotalAmount != null ? itemlistDetail.refReceiptTotalAmount.Value : 0;
                                    detail.RefRemark = itemlistDetail.refRemark != null ? itemlistDetail.refRemark : "";
                                    ///
                                    _lstDetail.Add(detail);
                                }
                                report.SumAmount = itemlist.receiptAmount.Value;
                                report.SumVatAmount = itemlist.receiptVat.Value;
                                report.SumTotal = itemlist.receiptTotalAmount.Value;
                                report.SumPenaltyCharge = itemlist.receiptIntAmount.Value;
                                TotalAmount = itemlist.receiptTotalAmount.Value + itemlist.receiptIntAmount.Value;
                                report.SumTotalAmount = TotalAmount;
                                report.strAmount = "(" + arr.ThaiBaht(TotalAmount) + ")";
                                if (itemlist.paymentTypeId == 2 || itemlist.paymentTypeId == 3)
                                    status = true;
                                if (itemlist.paymentTypeId == 4)
                                    lstamount.str4 = str4 + "  " + itemlist.cardNo;
                                #region ELE
                                if (itemlist.debtType == "ELE")
                                {
                                    if (itemlist.installmentsFlag == "N")
                                    {
                                        if (itemlist.receiptTypeId == 3)
                                            Path = ReportPath + "\\" + "CreditNote_Reciept.rpt";
                                        else
                                            Path = ReportPath + "\\" + "Elect_DocReciept.rpt";
                                    }
                                    else
                                    {
                                        lstamount.str3 = "**** ผ่อนชำระ" + receiptDtlName + " ****";
                                        Path = ReportPath + "\\" + "Elect_DocReciept_Ins.rpt";
                                    }

                                    if (itemlist.partialPayment == "Y" && itemlist.installmentsFlag == "N")
                                        lstamount.str3 = "**ชำระเงินบางส่วน**";
                                }
                                #endregion
                                #region DIS
                                if (itemlist.debtType == "DIS")
                                {
                                    Path = ReportPath + "\\" + "DIS_DocReciept.rpt";
                                }
                                #endregion
                                #region SER
                                if (itemlist.debtType == "SER")
                                    Path = ReportPath + "\\" + "SER_DocReciept.rpt";
                                #endregion
                                #region GUA 
                                if (itemlist.debtType == "GUA")
                                    Path = ReportPath + "\\" + "GUA_DocReciept.rpt";
                                #endregion
                                #region CHQ
                                if (itemlist.debtType == "CHQ")
                                    Path = ReportPath + "\\" + "CHQ_DocReciept.rpt";
                                #endregion
                                #region MWA
                                if (itemlist.debtType == "MWA")
                                    Path = ReportPath + "\\" + "Water_DocReciept.rpt";
                                #endregion
                                #region ADV
                                if (itemlist.debtType == "ADV")
                                    Path = ReportPath + "\\" + "ADV_DocReciept.rpt";
                                #endregion
                                #region COL
                                if (itemlist.debtType == "COL")
                                    Path = ReportPath + "\\" + "COL_DocReciept.rpt";
                                #endregion
                                #region OTH
                                if (itemlist.debtType == "OTH" || itemlist.debtType == "LEG")
                                {
                                    if (statusVat == 0)
                                        Path = ReportPath + "\\" + "OthTax_DocReciept_New.rpt";
                                    else
                                        Path = ReportPath + "\\" + "OthTax_DocReciept_New_Nonvat.rpt";
                                }
                                #endregion
                                #region BAD
                                if (itemlist.debtType == "BAD")
                                    Path = ReportPath + "\\" + "BAD_DocReciept.rpt";
                                #endregion
                                _lstModels.Add(report);
                                lstamount.TotalAmount = TotalAmount;
                                if (status)
                                {
                                    lstamount.str1 = str1;
                                    lstamount.str2 = str2;
                                }
                                _lstamount.Add(lstamount);
                                ReportDocument cryRpt = new ReportDocument();
                                cryRpt.Load(Path);
                                cryRpt.Database.Tables[0].SetDataSource(_lstModels);
                                cryRpt.Database.Tables[1].SetDataSource(_lstamount);
                                cryRpt.Database.Tables[2].SetDataSource(_lstDetail);
                                CrystalReportViewer crystalReportViewer1 = new CrystalReportViewer();
                                crystalReportViewer1.ReportSource = cryRpt;
                                crystalReportViewer1.Refresh();
                                cryRpt.PrintToPrinter(1, true, 0, 0);
                            }
                        }
                    }
                    SetProgressBar(false);
                    this.Invoke(new MethodInvoker(delegate { MsgBox.Show(this, ReceiptNo, "เลขที่ Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 26, FontStyle.Bold), Color.Black, countReport); }));
                    GlobalClass.StatusFormMoney = 1;
                    GlobalClass.StatusForm = null;
                    this.Invoke(new MethodInvoker(delegate { this.Close(); }));
                }
            }
            catch (Exception ex)
            {
                SetProgressBar(false);
                ReportErrorLog.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void txtDateEnd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void txtDateEnd_TextChanged(object sender, EventArgs e)
        {
            MaskedTextBox T = (MaskedTextBox)sender;
            try
            {
                string[] SplitData = T.Text.Split('/');
                if (int.Parse(SplitData[0].ToString().Trim()) > 12)
                {
                    T.Text = T.Text.Remove(0, 2);
                    MessageBox.Show("จำนวนเดือนไม่ถูกต้อง");
                    MaskFormat oldFormat = T.TextMaskFormat;
                    T.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
                    string fullText = T.Text;
                    T.TextMaskFormat = oldFormat;
                    int index = fullText.IndexOf(T.PromptChar);
                    if (index > -1)
                        BeginInvoke(new Action(() => T.Select(index, 0)));
                    return;
                }
                if (SplitData[1].ToString().Length == 4)
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                    DateTime dt = Convert.ToDateTime("01/" + SplitData[0].ToString() + "/" + SplitData[1].ToString());
                    if (dt.Year < DateTime.Now.Year)
                        MessageBox.Show("บัตรนี้หมดอายุแล้วไม่สามารถรับได้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            catch (Exception) { }
        }
        private void TextAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                decimal amountNew = Convert.ToDecimal(label25.Text);
                decimal amount_New = Convert.ToDecimal(LabelAmountReceived.Text);
                decimal amount_Ole = Convert.ToDecimal(LabelAmount.Text);
                if (LabelAmountReceived.Text == "00.0" || amountNew < 0 || amount_Ole > amount_New)
                    TextBoxChequeNo.Focus();
                else
                    ButtonConfirm.Focus();
            }
        }
        public void DeleteListInfo()
        {
            #region deleteDataList
            List<inquiryInfoBeanList> resulDelete = GlobalClass.LstInfo.Where(c => c.inquiryGroupDebtBeanList == null || c.inquiryGroupDebtBeanList.Count == 0).ToList();
            foreach (var itemDelete in resulDelete)
            {
                GlobalClass.LstInfo.Remove(itemDelete);
            }
            #endregion
        }
        private void DateTimePickerCheque_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (e.KeyChar.Equals(Keys.Tab) || cInt == 13)
                TextAmountCheque.Focus();
        }
        private void TextBoxTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (e.KeyChar.Equals(Keys.Tab) || cInt == 13)
            {
                if (TextBoxTel.Text.Replace("-", "").Trim().Length == 10)
                    txtTel.Focus();
                else
                {
                    MessageBox.Show("กรุณากรอกเบอร์โทรให้ครบ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    TextBoxTel.Focus();
                }
            }
        }
        private void TextBoxBankBranch_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                if (TextBoxBankBranch.Text != "")
                {
                    string name = "";
                    name = arr.GetBankDesc(TextBoxBankBranch.Text);
                    if (name == null)
                    {
                        MsgBox.Show(this, "ไม่มีรหัสธนาคาร " + TextBoxBankBranch.Text + " ในฐานข้อมูล", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                        SetTabIndex(TextBoxBankBranch);
                    }
                    else
                        SetTabIndex(null, 1);
                    TextBoxName.Text = name;
                }
            }
        }
        private void TextBoxChequeNo_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                if (TextBoxChequeNo.Text.Length >= 7 && TextBoxChequeNo.Text.Length <= 9)
                    SetTabIndex(TextBoxBankBranch);
                else
                {
                    MsgBox.Show(this, "เลขที่เช็คต้องอยู่ระหว่าง 7 ถึง 9 หลัก", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true,1);
                    SetTabIndex(TextBoxChequeNo);
                    TextBoxChequeNo.Select();
                }
            }
        }
        private void txtTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                CheckCh.Focus();
        }
        private void TextAmountCheque_TextChanged(object sender, EventArgs e)
        {
            TextBox T = (TextBox)sender;
            if (T.Text != "")
            {
                string[] amountS = T.Text.Split('.');
                if (amountS.Count() == 2)
                {
                    if (amountS[1].ToString() == "")
                        return;
                }
                decimal amount = Convert.ToDecimal(T.Text);
                TextAmountCheque.Text = amount.ToString("#,###,###,###.##");
                int len = T.ToString().Length;
                T.SelectionStart = len;
            }

            //TextBox T = (TextBox)sender;
            //try
            //{
            //    if (TextAmountCreditCard.Text == "" && TextAmount.Text == "")
            //        SummountCheque();
            //}
            //catch (Exception)
            //{
            //    try
            //    {
            //        int CursorIndex = T.SelectionStart - 1;
            //        T.Text = T.Text.Remove(CursorIndex, 1);
            //        T.SelectionStart = CursorIndex;
            //        T.SelectionLength = 0;
            //    }
            //    catch (Exception)
            //    {
            //        if (T.Text == "")
            //        {
            //            label25.Text = "00";
            //            LabelAmountReceived.Text = "00";
            //            LabelAmountChange.Text = "00";
            //        }
            //    }
            //}
        }
        private void CheckCh_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (ButtonAdd.Enabled == true)
                ButtonAdd.Focus();
            else
                ButtonUpate.Focus();
        }
        private void TextBoxCardNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else
                e.Handled = true;          
        }
        private void TextBoxCardNo_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                if (TextBoxCardNo.Text.Length == 16)
                {
                    SetTabIndex(TextBoxCardName);
                    TextBoxCardName.Focus();
                }
                else
                {
                    MsgBox.Show(this, "เลขที่บัตรเครดิตไม่ครบ 16 ตัวอักษร", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    SetTabIndex(TextBoxCardNo);
                    TextBoxCardNo.Focus();
                }
            }
        }
        private void DateTimePickerCheque_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
                CheckChequeText();
        }
        private void DateTimePickerCheque_CloseUp(object sender, EventArgs e)
        {
            CheckChequeText();
        }
        public void CheckChequeText()
        {
            DateTime dt = DateTimePickerCheque.Value;
            DateTime now = DateTime.Now;
            var numOfDay = now - dt;
            if (numOfDay.Days > 7)
            {
                DialogResultNew dialog = MsgBox.Show(this, "คุณต้องการรับเช็คย้อนหลัง จากวันที่ปัจจุบันเกิน 7 วัน ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);              
                if (dialog.Result == DialogResult.Yes)
                {
                    FormCheckUser checkUser = new FormCheckUser("ยืนยันการรับเช็คย้อนหลัง");
                    checkUser.ShowDialog();
                    checkUser.Dispose();
                    if (GlobalClass.AuthorizeLevel == 2)
                    {
                        SetTabIndex(TextAmountCheque);
                        TextAmountCheque.Focus();
                    }
                    else
                    {
                        if (GlobalClass.StatusForm != 0)
                            MsgBox.Show(this, "User ที่สามารถรับเช็คย้อนหลัง จากวันที่ปัจจุบันได้เกิน 7 วันได้ต้องเป็น Level 2 เท่านั้น", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);              
                        SetTabIndex(null, 1);
                        DateTimePickerCheque.Focus();
                        DateTimePickerCheque.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        return;
                    }
                }
                else if (dialog.Result == DialogResult.No)
                {
                    SetTabIndex(null, 1);
                    DateTimePickerCheque.Focus();
                    DateTimePickerCheque.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    return;
                }
            }
            if (dt > now)
            {
                SetTabIndex(null, 1);
                DateTimePickerCheque.Focus();
                DateTimePickerCheque.Text = DateTime.Now.ToString("dd/MM/yyyy");
                MsgBox.Show(this, "ไม่สามารถรับเช็คที่วันที่มากกว่าวันปัจจุบันได้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
            }
            else
            {
                SetTabIndex(TextAmountCheque);
                TextAmountCheque.Focus();
            }
        }
        private void SetTabIndex(TextBox txt = null, int? status = 0, Button bt = null)
        {
            TextAmount.TabStop = false;
            groupBox6.TabStop = false;
            TextBoxChequeNo.TabStop = false;
            TextBoxBankBranch.TabStop = false;
            DateTimePickerCheque.TabStop = false;
            TextAmountCheque.TabStop = false;
            TextBoxPayeeName.TabStop = false;
            TextBoxTel.TabStop = false;
            txtTel.TabStop = false;
            CheckCh.TabStop = false;
            ButtonAdd.TabStop = false;
            ButtonUpate.TabStop = false;
            ///
            groupBox7.TabStop = false;
            ComboBoxType.TabStop = false;
            ComboBoxCa.TabStop = false;
            TextAmountCreditCard.TabStop = false;
            TextBoxCardNo.TabStop = false;
            TextBoxCardName.TabStop = false;
            txtDateEnd.TabStop = false;
            ButtonApprove.TabStop = false;
            ButtonConfirm.TabStop = false;
            ButtonCancel.TabStop = false;
            if (status == 0)
            {
                txt.TabStop = true;
                txt.TabIndex = 1;
            }
            else if (status == 1)
            {
                DateTimePickerCheque.TabStop = true;
                DateTimePickerCheque.TabIndex = 1;
            }
            else if (status == 2)
            {
                ButtonConfirm.TabStop = true;
                ButtonConfirm.TabIndex = 1;
                ButtonCancel.TabStop = true;
            }
            else if (status == 3)
            {
                TextBoxTel.TabStop = true;
                TextBoxTel.TabIndex = 1;
            }
            else if (status == 4)
            {
                CheckCh.TabStop = true;
                CheckCh.TabIndex = 1;
            }
            else if (status == 5)
            {
                if (ButtonAdd.Enabled == true)
                {
                    bt.TabStop = true;
                    bt.TabIndex = 1;
                }
                else
                {
                    bt.TabStop = true;
                    bt.TabIndex = 1;
                }
            }
            else if (status == 6)
            {
                ComboBoxType.TabStop = true;
                ComboBoxType.TabIndex = 1;
            }
            else if (status == 7)
            {
                ComboBoxCa.TabStop = true;
                ComboBoxCa.TabIndex = 1;
            }
            else if(status == 8)
            {
                txtDateEnd.TabStop = true;
                txtDateEnd.TabIndex = 1;
            }
            else
            {
                ButtonApprove.TabStop = true;
                ButtonApprove.TabIndex = 1;
            }
        }
        private void TextAmount_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                decimal amountNew = Convert.ToDecimal(label25.Text);
                decimal amount_New = Convert.ToDecimal(LabelAmountReceived.Text);
                decimal amount_Ole = Convert.ToDecimal(LabelAmount.Text);
                if (LabelAmountReceived.Text == "00.0" || amountNew < 0 || amount_Ole > amount_New)
                    SetTabIndex(TextBoxChequeNo);
                else
                    SetTabIndex(null, 2);
            }
        }
        private void TextAmountCheque_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                if (TextAmountCheque.Text != "")
                {
                    SetTabIndex(TextBoxPayeeName);
                    TextBoxPayeeName.Focus();
                }
            }
        }
        private void TextBoxPayeeName_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                if (TextBoxPayeeName.Text != "")
                {
                    SetTabIndex(null, 3);
                    TextBoxTel.Focus();
                }
            }
        }
        private void TextBoxTel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                SetTabIndex(txtTel);
                txtTel.Focus();
            }
        }
        private void txtTel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                SetTabIndex(null, 4);
                CheckCh.Focus();
            }
        }
        private void CheckCh_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
                SetTabIndex(null, 5, ButtonAdd);
            if (e.KeyCode == Keys.Space)
            {
                if (CheckCh.Checked)
                    CheckCh.Checked = false;
                else
                    CheckCh.Checked = true;
            }
        }
        private void ComboBoxType_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                SetTabIndex(null, 7, null);
                ComboBoxCa.Focus();
            }
        }
        private void ComboBoxCa_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                SetTabIndex(TextAmountCreditCard);
                TextAmountCreditCard.Focus();
            }           
        }
        private void TextAmountCreditCard_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                if (TextAmountCreditCard.Text != "")
                {
                    SetTabIndex(TextBoxCardNo);
                    TextBoxCardNo.Focus();
                }
            }
        }
        private void TextBoxCardName_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                if (TextBoxCardName.Text != "")
                {
                    SetTabIndex(null,8, null);
                    txtDateEnd.Focus();
                }
                else
                {
                    SetTabIndex(TextBoxCardName);
                    TextBoxCardName.Focus();
                }
            }
        }
        private void ButtonApprove_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            SetTabIndex(null, 2, null);
        }
        private void txtDateEnd_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                if (TextBoxCardName.Text != "")
                {
                    SetTabIndex(null, 9, null);
                    ButtonApprove.Focus();
                }
                else
                {
                    SetTabIndex(null, 8, null);
                    txtDateEnd.Focus();
                }
            }
        }
    }
}
