﻿namespace MyPayment.FormArrears
{
    partial class FormMapCheque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMapCheque));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Label13 = new System.Windows.Forms.Label();
            this.GridDetail = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelTel = new System.Windows.Forms.Label();
            this.labelBalance = new System.Windows.Forms.Label();
            this.labelPayeeName = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelNo = new System.Windows.Forms.Label();
            this.labelNumber = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.CheckCh = new System.Windows.Forms.CheckBox();
            this.lblB6 = new System.Windows.Forms.Label();
            this.lblB3 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.lblB5 = new System.Windows.Forms.Label();
            this.lblB2 = new System.Windows.Forms.Label();
            this.lblB1 = new System.Windows.Forms.Label();
            this.ButtonConfirm = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.ColCheckbox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColCa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColReqNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColInterest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPayed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPayedNew = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColdebtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Label13);
            this.groupBox1.Controls.Add(this.GridDetail);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(959, 458);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label13.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Label13.Location = new System.Drawing.Point(9, -4);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(189, 28);
            this.Label13.TabIndex = 63;
            this.Label13.Text = "เลือกบัญชีแสดงสัญญาที่จ่าย";
            // 
            // GridDetail
            // 
            this.GridDetail.AllowUserToAddRows = false;
            this.GridDetail.BackgroundColor = System.Drawing.Color.White;
            this.GridDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCheckbox,
            this.ColCa,
            this.ColUi,
            this.ColReqNo,
            this.ColType,
            this.ColInterest,
            this.ColAmount,
            this.colTotalAmount,
            this.ColBalance,
            this.ColPayed,
            this.colPayedNew,
            this.ColdebtId});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridDetail.DefaultCellStyle = dataGridViewCellStyle3;
            this.GridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDetail.Location = new System.Drawing.Point(3, 183);
            this.GridDetail.MultiSelect = false;
            this.GridDetail.Name = "GridDetail";
            this.GridDetail.RowHeadersVisible = false;
            this.GridDetail.RowHeadersWidth = 10;
            this.GridDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridDetail.Size = new System.Drawing.Size(953, 272);
            this.GridDetail.TabIndex = 62;
            this.GridDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridDetail_CellContentClick);
            this.GridDetail.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.GridDetail_DataError);
            this.GridDetail.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GridDetail_EditingControlShowing);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.labelTel);
            this.panel1.Controls.Add(this.labelBalance);
            this.panel1.Controls.Add(this.labelPayeeName);
            this.panel1.Controls.Add(this.labelAmount);
            this.panel1.Controls.Add(this.labelDate);
            this.panel1.Controls.Add(this.labelName);
            this.panel1.Controls.Add(this.labelNo);
            this.panel1.Controls.Add(this.labelNumber);
            this.panel1.Controls.Add(this.Label2);
            this.panel1.Controls.Add(this.CheckCh);
            this.panel1.Controls.Add(this.lblB6);
            this.panel1.Controls.Add(this.lblB3);
            this.panel1.Controls.Add(this.Label1);
            this.panel1.Controls.Add(this.lblB5);
            this.panel1.Controls.Add(this.lblB2);
            this.panel1.Controls.Add(this.lblB1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(953, 152);
            this.panel1.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(531, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 26);
            this.label11.TabIndex = 1000000033;
            this.label11.Text = "บาท";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(531, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 26);
            this.label10.TabIndex = 1000000032;
            this.label10.Text = "บาท";
            // 
            // labelTel
            // 
            this.labelTel.BackColor = System.Drawing.Color.White;
            this.labelTel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTel.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTel.ForeColor = System.Drawing.Color.Blue;
            this.labelTel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTel.Location = new System.Drawing.Point(88, 114);
            this.labelTel.Name = "labelTel";
            this.labelTel.Size = new System.Drawing.Size(196, 33);
            this.labelTel.TabIndex = 1000000031;
            this.labelTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelBalance
            // 
            this.labelBalance.BackColor = System.Drawing.Color.White;
            this.labelBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBalance.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBalance.ForeColor = System.Drawing.Color.Blue;
            this.labelBalance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelBalance.Location = new System.Drawing.Point(369, 77);
            this.labelBalance.Name = "labelBalance";
            this.labelBalance.Size = new System.Drawing.Size(161, 33);
            this.labelBalance.TabIndex = 1000000030;
            this.labelBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPayeeName
            // 
            this.labelPayeeName.BackColor = System.Drawing.Color.White;
            this.labelPayeeName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelPayeeName.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPayeeName.ForeColor = System.Drawing.Color.Blue;
            this.labelPayeeName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPayeeName.Location = new System.Drawing.Point(88, 77);
            this.labelPayeeName.Name = "labelPayeeName";
            this.labelPayeeName.Size = new System.Drawing.Size(196, 33);
            this.labelPayeeName.TabIndex = 1000000029;
            this.labelPayeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAmount
            // 
            this.labelAmount.BackColor = System.Drawing.Color.White;
            this.labelAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmount.ForeColor = System.Drawing.Color.Blue;
            this.labelAmount.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelAmount.Location = new System.Drawing.Point(369, 41);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(161, 33);
            this.labelAmount.TabIndex = 1000000028;
            this.labelAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelDate
            // 
            this.labelDate.BackColor = System.Drawing.Color.White;
            this.labelDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelDate.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.ForeColor = System.Drawing.Color.Blue;
            this.labelDate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelDate.Location = new System.Drawing.Point(123, 41);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(161, 33);
            this.labelDate.TabIndex = 1000000027;
            this.labelDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelName
            // 
            this.labelName.BackColor = System.Drawing.Color.White;
            this.labelName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelName.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.ForeColor = System.Drawing.Color.Blue;
            this.labelName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelName.Location = new System.Drawing.Point(536, 4);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(412, 33);
            this.labelName.TabIndex = 1000000026;
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelNo
            // 
            this.labelNo.BackColor = System.Drawing.Color.White;
            this.labelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNo.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNo.ForeColor = System.Drawing.Color.Blue;
            this.labelNo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNo.Location = new System.Drawing.Point(412, 4);
            this.labelNo.Name = "labelNo";
            this.labelNo.Size = new System.Drawing.Size(118, 33);
            this.labelNo.TabIndex = 1000000025;
            this.labelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelNumber
            // 
            this.labelNumber.BackColor = System.Drawing.Color.White;
            this.labelNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber.ForeColor = System.Drawing.Color.Blue;
            this.labelNumber.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNumber.Location = new System.Drawing.Point(88, 4);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(196, 33);
            this.labelNumber.TabIndex = 1000000024;
            this.labelNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label2.Location = new System.Drawing.Point(290, 80);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(59, 26);
            this.Label2.TabIndex = 85;
            this.Label2.Text = "คงเหลือ";
            // 
            // CheckCh
            // 
            this.CheckCh.AutoSize = true;
            this.CheckCh.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CheckCh.Location = new System.Drawing.Point(295, 115);
            this.CheckCh.Name = "CheckCh";
            this.CheckCh.Size = new System.Drawing.Size(115, 30);
            this.CheckCh.TabIndex = 81;
            this.CheckCh.Text = "แคชเชียร์เช็ค";
            this.CheckCh.UseVisualStyleBackColor = true;
            // 
            // lblB6
            // 
            this.lblB6.AutoSize = true;
            this.lblB6.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB6.Location = new System.Drawing.Point(13, 117);
            this.lblB6.Name = "lblB6";
            this.lblB6.Size = new System.Drawing.Size(65, 26);
            this.lblB6.TabIndex = 80;
            this.lblB6.Text = "เบอร์โทร";
            // 
            // lblB3
            // 
            this.lblB3.AutoSize = true;
            this.lblB3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB3.Location = new System.Drawing.Point(290, 7);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(120, 26);
            this.lblB3.TabIndex = 79;
            this.lblB3.Text = "รหัสธนาคาร/สาขา";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label1.Location = new System.Drawing.Point(290, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(73, 26);
            this.Label1.TabIndex = 78;
            this.Label1.Text = "จำนวนเงิน";
            // 
            // lblB5
            // 
            this.lblB5.AutoSize = true;
            this.lblB5.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB5.Location = new System.Drawing.Point(13, 80);
            this.lblB5.Name = "lblB5";
            this.lblB5.Size = new System.Drawing.Size(76, 26);
            this.lblB5.TabIndex = 76;
            this.lblB5.Text = "ชื่อผู้สั่งจ่าย";
            // 
            // lblB2
            // 
            this.lblB2.AutoSize = true;
            this.lblB2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB2.Location = new System.Drawing.Point(13, 44);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(114, 26);
            this.lblB2.TabIndex = 73;
            this.lblB2.Text = "วันที่ในเช็ค(พ.ศ.)";
            this.lblB2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblB1
            // 
            this.lblB1.AutoSize = true;
            this.lblB1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB1.Location = new System.Drawing.Point(13, 7);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(69, 26);
            this.lblB1.TabIndex = 68;
            this.lblB1.Text = "เลขที่เช็ค";
            // 
            // ButtonConfirm
            // 
            this.ButtonConfirm.BackColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderWidth = 1;
            this.ButtonConfirm.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonConfirm.ButtonText = "ยืนยัน";
            this.ButtonConfirm.CausesValidation = false;
            this.ButtonConfirm.EndColor = System.Drawing.Color.Silver;
            this.ButtonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonConfirm.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonConfirm.ForeColor = System.Drawing.Color.Black;
            this.ButtonConfirm.GradientAngle = 90;
            this.ButtonConfirm.Image = global::MyPayment.Properties.Resources.ok1;
            this.ButtonConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonConfirm.Location = new System.Drawing.Point(782, 477);
            this.ButtonConfirm.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonConfirm.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonConfirm.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonConfirm.Name = "ButtonConfirm";
            this.ButtonConfirm.ShowButtontext = true;
            this.ButtonConfirm.Size = new System.Drawing.Size(90, 28);
            this.ButtonConfirm.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.TabIndex = 2;
            this.ButtonConfirm.TextLocation_X = 35;
            this.ButtonConfirm.TextLocation_Y = 1;
            this.ButtonConfirm.Transparent1 = 80;
            this.ButtonConfirm.Transparent2 = 120;
            this.ButtonConfirm.UseVisualStyleBackColor = true;
            this.ButtonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.Silver;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close2;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(879, 477);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(90, 28);
            this.ButtonCancel.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.TabIndex = 3;
            this.ButtonCancel.TextLocation_X = 35;
            this.ButtonCancel.TextLocation_Y = 1;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // ColCheckbox
            // 
            this.ColCheckbox.DataPropertyName = "status";
            this.ColCheckbox.Frozen = true;
            this.ColCheckbox.HeaderText = "";
            this.ColCheckbox.Name = "ColCheckbox";
            this.ColCheckbox.Width = 30;
            // 
            // ColCa
            // 
            this.ColCa.DataPropertyName = "ca";
            this.ColCa.HeaderText = "เลขที่สัญญา";
            this.ColCa.Name = "ColCa";
            this.ColCa.ReadOnly = true;
            this.ColCa.Width = 120;
            // 
            // ColUi
            // 
            this.ColUi.DataPropertyName = "ui";
            this.ColUi.HeaderText = "เลขที่เครื่องวัดฯ";
            this.ColUi.Name = "ColUi";
            this.ColUi.ReadOnly = true;
            this.ColUi.Width = 150;
            // 
            // ColReqNo
            // 
            this.ColReqNo.DataPropertyName = "reqNo";
            dataGridViewCellStyle1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ColReqNo.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColReqNo.HeaderText = "เลขที่รับเรื่อง";
            this.ColReqNo.Name = "ColReqNo";
            this.ColReqNo.ReadOnly = true;
            this.ColReqNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColReqNo.Width = 120;
            // 
            // ColType
            // 
            this.ColType.DataPropertyName = "debtType";
            this.ColType.HeaderText = "ประเภท";
            this.ColType.Name = "ColType";
            this.ColType.ReadOnly = true;
            this.ColType.Width = 90;
            // 
            // ColInterest
            // 
            this.ColInterest.DataPropertyName = "Defaultpenalty";
            this.ColInterest.HeaderText = "เบี้ยปรับ";
            this.ColInterest.Name = "ColInterest";
            this.ColInterest.ReadOnly = true;
            this.ColInterest.Width = 120;
            // 
            // ColAmount
            // 
            this.ColAmount.DataPropertyName = "debtBalance";
            this.ColAmount.HeaderText = "ยอดเงิน";
            this.ColAmount.Name = "ColAmount";
            this.ColAmount.ReadOnly = true;
            this.ColAmount.Width = 120;
            // 
            // colTotalAmount
            // 
            this.colTotalAmount.DataPropertyName = "totalAmount";
            this.colTotalAmount.HeaderText = "ยอดเงินรวม";
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.Width = 120;
            // 
            // ColBalance
            // 
            this.ColBalance.DataPropertyName = "AmountBalance";
            this.ColBalance.HeaderText = "ยอดคงเหลือ";
            this.ColBalance.Name = "ColBalance";
            this.ColBalance.ReadOnly = true;
            this.ColBalance.Width = 120;
            // 
            // ColPayed
            // 
            this.ColPayed.DataPropertyName = "Payed";
            dataGridViewCellStyle2.Format = "#,###,###,##0.00";
            this.ColPayed.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColPayed.HeaderText = "ยอดแบ่งให้";
            this.ColPayed.Name = "ColPayed";
            this.ColPayed.ReadOnly = true;
            this.ColPayed.Width = 120;
            // 
            // colPayedNew
            // 
            this.colPayedNew.DataPropertyName = "PayedNew";
            this.colPayedNew.HeaderText = "PayedNew";
            this.colPayedNew.Name = "colPayedNew";
            this.colPayedNew.ReadOnly = true;
            this.colPayedNew.Visible = false;
            // 
            // ColdebtId
            // 
            this.ColdebtId.DataPropertyName = "debtId";
            this.ColdebtId.HeaderText = "DebtId";
            this.ColdebtId.Name = "ColdebtId";
            this.ColdebtId.ReadOnly = true;
            this.ColdebtId.Visible = false;
            // 
            // FormMapCheque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 513);
            this.Controls.Add(this.ButtonConfirm);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMapCheque";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "เลือกบัญชีแสดงสัญญาที่จ่าย";
            this.Load += new System.EventHandler(this.FormMapCheque_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label Label13;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.CheckBox CheckCh;
        internal System.Windows.Forms.Label lblB6;
        internal System.Windows.Forms.Label lblB3;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label lblB5;
        internal System.Windows.Forms.Label lblB2;
        internal System.Windows.Forms.Label lblB1;
        private Custom_Controls_in_CS.ButtonZ ButtonConfirm;
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        internal System.Windows.Forms.Label labelAmount;
        internal System.Windows.Forms.Label labelDate;
        internal System.Windows.Forms.Label labelName;
        internal System.Windows.Forms.Label labelNo;
        internal System.Windows.Forms.Label labelNumber;
        internal System.Windows.Forms.Label labelTel;
        internal System.Windows.Forms.Label labelBalance;
        internal System.Windows.Forms.Label labelPayeeName;
        internal System.Windows.Forms.DataGridView GridDetail;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColCheckbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUi;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColReqNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColInterest;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTotalAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPayed;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPayedNew;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColdebtId;
    }
}