﻿namespace MyPayment.FormArrears
{
    partial class FormProcessingFeeElectricity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProcessingFeeElectricity));
            this.ButtonNo = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonYes = new Custom_Controls_in_CS.ButtonZ();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ButtonNo
            // 
            this.ButtonNo.BackColor = System.Drawing.Color.Transparent;
            this.ButtonNo.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonNo.BorderWidth = 1;
            this.ButtonNo.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonNo.ButtonText = "ไม่";
            this.ButtonNo.CausesValidation = false;
            this.ButtonNo.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonNo.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonNo.ForeColor = System.Drawing.Color.Black;
            this.ButtonNo.GradientAngle = 90;
            this.ButtonNo.Image = global::MyPayment.Properties.Resources.close1;
            this.ButtonNo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonNo.Location = new System.Drawing.Point(184, 109);
            this.ButtonNo.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonNo.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonNo.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonNo.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonNo.Name = "ButtonNo";
            this.ButtonNo.ShowButtontext = true;
            this.ButtonNo.Size = new System.Drawing.Size(127, 40);
            this.ButtonNo.StartColor = System.Drawing.Color.LightGray;
            this.ButtonNo.TabIndex = 1000000025;
            this.ButtonNo.TextLocation_X = 60;
            this.ButtonNo.TextLocation_Y = 5;
            this.ButtonNo.Transparent1 = 80;
            this.ButtonNo.Transparent2 = 120;
            this.ButtonNo.UseVisualStyleBackColor = true;
            this.ButtonNo.Click += new System.EventHandler(this.ButtonNo_Click);
            // 
            // ButtonYes
            // 
            this.ButtonYes.BackColor = System.Drawing.Color.Transparent;
            this.ButtonYes.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonYes.BorderWidth = 1;
            this.ButtonYes.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonYes.ButtonText = "ใช่";
            this.ButtonYes.CausesValidation = false;
            this.ButtonYes.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonYes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonYes.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonYes.ForeColor = System.Drawing.Color.Black;
            this.ButtonYes.GradientAngle = 90;
            this.ButtonYes.Image = global::MyPayment.Properties.Resources.ok;
            this.ButtonYes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonYes.Location = new System.Drawing.Point(41, 109);
            this.ButtonYes.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonYes.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonYes.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonYes.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonYes.Name = "ButtonYes";
            this.ButtonYes.ShowButtontext = true;
            this.ButtonYes.Size = new System.Drawing.Size(127, 40);
            this.ButtonYes.StartColor = System.Drawing.Color.LightGray;
            this.ButtonYes.TabIndex = 1000000024;
            this.ButtonYes.TextLocation_X = 60;
            this.ButtonYes.TextLocation_Y = 5;
            this.ButtonYes.Transparent1 = 80;
            this.ButtonYes.Transparent2 = 120;
            this.ButtonYes.UseVisualStyleBackColor = true;
            this.ButtonYes.Click += new System.EventHandler(this.ButtonYes_Click);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(389, 106);
            this.label3.TabIndex = 1000000026;
            this.label3.Text = "รบกวนสอบถามลูกค้าว่าต้องการต่อกลับใช่หรือไม่\r\nกด ใช่ เพื่อดำเนินขอต่อกลับ\r\nกด ไม่" +
    " หากไม่ต้องการขอต่อกลับ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormProcessingFeeElectricity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 159);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ButtonNo);
            this.Controls.Add(this.ButtonYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormProcessingFeeElectricity";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ค่าดำเนินการต่อไฟ";
            this.ResumeLayout(false);

        }

        #endregion

        private Custom_Controls_in_CS.ButtonZ ButtonNo;
        private Custom_Controls_in_CS.ButtonZ ButtonYes;
        internal System.Windows.Forms.Label label3;
    }
}