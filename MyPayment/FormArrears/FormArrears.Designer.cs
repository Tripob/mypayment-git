﻿using System;
using System.Windows.Forms;

namespace MyPayment.FormArrears
{
    partial class FormArrears
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                this.Invoke(new MethodInvoker(delegate { base.Dispose(disposing); }));               
            }
            catch (Exception)
            {
                //this.Invoke(new MethodInvoker(delegate { Close(); }));
            }         
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormArrears));
            this.panelMain = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblStatusD = new System.Windows.Forms.Label();
            this.lblStatusC = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.GroupBoxDetail = new System.Windows.Forms.GroupBox();
            this.pnProgressBar = new System.Windows.Forms.Panel();
            this.ptMascot = new System.Windows.Forms.PictureBox();
            this.ProgressBar = new CircularProgressBar.CircularProgressBar();
            this.PanelCancelWait = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblReceiptName = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.CheckBoxSelect = new System.Windows.Forms.CheckBox();
            this.labelBill = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelBranch = new System.Windows.Forms.Label();
            this.labelTaxId = new System.Windows.Forms.Label();
            this.labelCa = new System.Windows.Forms.Label();
            this.labelUi = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.labelFk = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.GridDetail = new System.Windows.Forms.DataGridView();
            this.LabelTmp_Time = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.comboBoxDetail = new System.Windows.Forms.ComboBox();
            this.TextBoxSearch = new System.Windows.Forms.TextBox();
            this.txtCount = new System.Windows.Forms.TextBox();
            this.CheckBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.DataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.ColCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColCA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColckCancel = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColTotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSumTotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPenaltyAll = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCusName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCusTaxId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCusTaxBranch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCusAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCustomerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTax20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTaxBranch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.lblCountList = new System.Windows.Forms.Label();
            this.lblAmountPayment = new System.Windows.Forms.Label();
            this.lblCountListDetail = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblAmountDetail = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCaDetail = new System.Windows.Forms.Label();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label32 = new System.Windows.Forms.Label();
            this.Label29 = new System.Windows.Forms.Label();
            this.Label31 = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblCountcapayment = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCountCa = new System.Windows.Forms.Label();
            this.lblCountCreditNote = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.Label39 = new System.Windows.Forms.Label();
            this.Label38 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtFine = new System.Windows.Forms.Label();
            this.txtTotalSum = new System.Windows.Forms.Label();
            this.Label36 = new System.Windows.Forms.Label();
            this.Label37 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.Label();
            this.Label35 = new System.Windows.Forms.Label();
            this.txtVat = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.PanelEmployee = new System.Windows.Forms.Panel();
            this.groupBoxEmployee = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelStationNo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelEmployeeName = new System.Windows.Forms.Label();
            this.LabelEmployeeCode = new System.Windows.Forms.Label();
            this.LabelFkName = new System.Windows.Forms.Label();
            this.PanelHead = new System.Windows.Forms.Panel();
            this.PanelNoneQ = new System.Windows.Forms.Panel();
            this.ComboBoxDocType = new System.Windows.Forms.ComboBox();
            this.TextBoxBarcode = new System.Windows.Forms.TextBox();
            this.PanelHaveQ = new System.Windows.Forms.Panel();
            this.LabelNoQ = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.ButtonRec = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonCancelWait = new Custom_Controls_in_CS.ButtonZ();
            this.btCancelFine = new Custom_Controls_in_CS.ButtonZ();
            this.buttonZ1 = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonAdvance = new Custom_Controls_in_CS.ButtonZ();
            this.btOK = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonDelete = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonClear = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonCallQNone = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonCallQ = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonRefreshQ = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonCancelQ = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonResetQ = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonAdd = new Custom_Controls_in_CS.ButtonZ();
            this.clientSocket2 = new MyPayment.Models.ClientSocket(this.components);
            this.clientSocket1 = new MyPayment.Models.ClientSocket(this.components);
            this.ColCheckBoxDetail = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colCADetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReqNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmountDetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPreviousDueDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldebtBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPayment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colcountDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDefaultpenalty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCkStatus = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colVatDetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDebtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColdebtIdSet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColdebtType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNewDuedate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDebtLock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInsDebtInsHdrId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInstallmentsFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelMain.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.GroupBoxDetail.SuspendLayout();
            this.pnProgressBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptMascot)).BeginInit();
            this.PanelCancelWait.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).BeginInit();
            this.panel2.SuspendLayout();
            this.Panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.PanelEmployee.SuspendLayout();
            this.groupBoxEmployee.SuspendLayout();
            this.PanelHead.SuspendLayout();
            this.PanelNoneQ.SuspendLayout();
            this.PanelHaveQ.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.panel3);
            this.panelMain.Controls.Add(this.panel1);
            this.panelMain.Controls.Add(this.PanelEmployee);
            this.panelMain.Controls.Add(this.PanelHead);
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1009, 704);
            this.panelMain.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblStatusD);
            this.panel3.Controls.Add(this.lblStatusC);
            this.panel3.Controls.Add(this.ButtonRec);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 664);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1009, 40);
            this.panel3.TabIndex = 8;
            // 
            // lblStatusD
            // 
            this.lblStatusD.BackColor = System.Drawing.Color.LightGray;
            this.lblStatusD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatusD.Font = new System.Drawing.Font("TH Sarabun New", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblStatusD.ForeColor = System.Drawing.Color.Red;
            this.lblStatusD.Location = new System.Drawing.Point(198, 0);
            this.lblStatusD.Name = "lblStatusD";
            this.lblStatusD.Size = new System.Drawing.Size(331, 40);
            this.lblStatusD.TabIndex = 1000000044;
            this.lblStatusD.Text = "*หักบัญชีธนาคาร/บัตรเครดิต*";
            this.lblStatusD.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblStatusD.Visible = false;
            // 
            // lblStatusC
            // 
            this.lblStatusC.BackColor = System.Drawing.Color.LightGray;
            this.lblStatusC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatusC.Font = new System.Drawing.Font("TH Sarabun New", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblStatusC.ForeColor = System.Drawing.Color.Red;
            this.lblStatusC.Location = new System.Drawing.Point(583, 0);
            this.lblStatusC.Name = "lblStatusC";
            this.lblStatusC.Size = new System.Drawing.Size(331, 40);
            this.lblStatusC.TabIndex = 1000000043;
            this.lblStatusC.Text = "*ไม่อนุญาตให้จ่ายชำระด้วยเช็ค*";
            this.lblStatusC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblStatusC.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.GroupBoxDetail);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 85);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1009, 579);
            this.panel1.TabIndex = 6;
            // 
            // GroupBoxDetail
            // 
            this.GroupBoxDetail.Controls.Add(this.pnProgressBar);
            this.GroupBoxDetail.Controls.Add(this.PanelCancelWait);
            this.GroupBoxDetail.Controls.Add(this.panel6);
            this.GroupBoxDetail.Controls.Add(this.panel5);
            this.GroupBoxDetail.Controls.Add(this.panel2);
            this.GroupBoxDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupBoxDetail.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.GroupBoxDetail.Location = new System.Drawing.Point(0, 0);
            this.GroupBoxDetail.Name = "GroupBoxDetail";
            this.GroupBoxDetail.Size = new System.Drawing.Size(1009, 579);
            this.GroupBoxDetail.TabIndex = 4;
            this.GroupBoxDetail.TabStop = false;
            this.GroupBoxDetail.Text = "รายละเอียดหนี้ค้างชำระ";
            // 
            // pnProgressBar
            // 
            this.pnProgressBar.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnProgressBar.Controls.Add(this.ptMascot);
            this.pnProgressBar.Controls.Add(this.ProgressBar);
            this.pnProgressBar.Location = new System.Drawing.Point(203, 3);
            this.pnProgressBar.Name = "pnProgressBar";
            this.pnProgressBar.Size = new System.Drawing.Size(25, 21);
            this.pnProgressBar.TabIndex = 131;
            this.pnProgressBar.Visible = false;
            // 
            // ptMascot
            // 
            this.ptMascot.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ptMascot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ptMascot.Image = global::MyPayment.Properties.Resources._14_02;
            this.ptMascot.Location = new System.Drawing.Point(65, 52);
            this.ptMascot.Name = "ptMascot";
            this.ptMascot.Size = new System.Drawing.Size(200, 185);
            this.ptMascot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptMascot.TabIndex = 135;
            this.ptMascot.TabStop = false;
            // 
            // ProgressBar
            // 
            this.ProgressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.ProgressBar.AnimationSpeed = 500;
            this.ProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.ProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold);
            this.ProgressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ProgressBar.InnerColor = System.Drawing.SystemColors.ScrollBar;
            this.ProgressBar.InnerMargin = 2;
            this.ProgressBar.InnerWidth = -1;
            this.ProgressBar.Location = new System.Drawing.Point(0, 0);
            this.ProgressBar.MarqueeAnimationSpeed = 2000;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.OuterColor = System.Drawing.Color.Gray;
            this.ProgressBar.OuterMargin = -25;
            this.ProgressBar.OuterWidth = 26;
            this.ProgressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ProgressBar.ProgressWidth = 10;
            this.ProgressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.ProgressBar.Size = new System.Drawing.Size(25, 21);
            this.ProgressBar.StartAngle = 270;
            this.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.ProgressBar.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.ProgressBar.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.ProgressBar.SubscriptText = ".23";
            this.ProgressBar.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.ProgressBar.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.ProgressBar.SuperscriptText = "°C";
            this.ProgressBar.TabIndex = 132;
            this.ProgressBar.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.ProgressBar.Value = 68;
            // 
            // PanelCancelWait
            // 
            this.PanelCancelWait.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.PanelCancelWait.Controls.Add(this.pictureBox4);
            this.PanelCancelWait.Controls.Add(this.pictureBox3);
            this.PanelCancelWait.Controls.Add(this.ButtonCancelWait);
            this.PanelCancelWait.Controls.Add(this.Label12);
            this.PanelCancelWait.Controls.Add(this.pictureBox1);
            this.PanelCancelWait.Location = new System.Drawing.Point(164, 5);
            this.PanelCancelWait.Name = "PanelCancelWait";
            this.PanelCancelWait.Size = new System.Drawing.Size(32, 19);
            this.PanelCancelWait.TabIndex = 128;
            this.PanelCancelWait.Visible = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::MyPayment.Properties.Resources.mascot7_1;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(451, 56);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(75, 198);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 1000000042;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::MyPayment.Properties.Resources.mascot6_1;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(84, 56);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(75, 198);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 1000000041;
            this.pictureBox3.TabStop = false;
            // 
            // Label12
            // 
            this.Label12.Font = new System.Drawing.Font("TH Sarabun New", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label12.ForeColor = System.Drawing.Color.Red;
            this.Label12.Location = new System.Drawing.Point(-3, 258);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(616, 72);
            this.Label12.TabIndex = 2;
            this.Label12.Text = "*** รอคิวถัดไปเพื่อทำงานต่อ ***";
            this.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::MyPayment.Properties.Resources.MEA_Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(177, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(251, 198);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btCancelFine);
            this.panel6.Controls.Add(this.lblReceiptName);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.CheckBoxSelect);
            this.panel6.Controls.Add(this.buttonZ1);
            this.panel6.Controls.Add(this.ButtonAdvance);
            this.panel6.Controls.Add(this.labelBill);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.labelBranch);
            this.panel6.Controls.Add(this.labelTaxId);
            this.panel6.Controls.Add(this.labelCa);
            this.panel6.Controls.Add(this.labelUi);
            this.panel6.Controls.Add(this.labelAddress);
            this.panel6.Controls.Add(this.labelFk);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.Label15);
            this.panel6.Controls.Add(this.Label11);
            this.panel6.Controls.Add(this.Label9);
            this.panel6.Controls.Add(this.GridDetail);
            this.panel6.Controls.Add(this.LabelTmp_Time);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Location = new System.Drawing.Point(176, 25);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(833, 412);
            this.panel6.TabIndex = 130;
            // 
            // lblReceiptName
            // 
            this.lblReceiptName.BackColor = System.Drawing.Color.White;
            this.lblReceiptName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblReceiptName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiptName.Location = new System.Drawing.Point(111, 69);
            this.lblReceiptName.Name = "lblReceiptName";
            this.lblReceiptName.Size = new System.Drawing.Size(289, 21);
            this.lblReceiptName.TabIndex = 1000000049;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(4, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 26);
            this.label14.TabIndex = 1000000048;
            this.label14.Text = "ชื่อผู้ชำระอื่นๆ";
            // 
            // CheckBoxSelect
            // 
            this.CheckBoxSelect.AutoSize = true;
            this.CheckBoxSelect.Location = new System.Drawing.Point(4, 112);
            this.CheckBoxSelect.Name = "CheckBoxSelect";
            this.CheckBoxSelect.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelect.TabIndex = 1000000047;
            this.CheckBoxSelect.UseVisualStyleBackColor = true;
            this.CheckBoxSelect.Click += new System.EventHandler(this.CheckBoxSelect_Click);
            // 
            // labelBill
            // 
            this.labelBill.BackColor = System.Drawing.Color.White;
            this.labelBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBill.Location = new System.Drawing.Point(111, 46);
            this.labelBill.Name = "labelBill";
            this.labelBill.Size = new System.Drawing.Size(126, 21);
            this.labelBill.TabIndex = 1000000044;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(22, 43);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 26);
            this.label16.TabIndex = 1000000043;
            this.label16.Text = "รหัสบิลก้อน";
            // 
            // labelBranch
            // 
            this.labelBranch.BackColor = System.Drawing.Color.White;
            this.labelBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBranch.Location = new System.Drawing.Point(480, 46);
            this.labelBranch.Name = "labelBranch";
            this.labelBranch.Size = new System.Drawing.Size(103, 21);
            this.labelBranch.TabIndex = 1000000042;
            // 
            // labelTaxId
            // 
            this.labelTaxId.BackColor = System.Drawing.Color.White;
            this.labelTaxId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTaxId.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTaxId.Location = new System.Drawing.Point(295, 46);
            this.labelTaxId.Name = "labelTaxId";
            this.labelTaxId.Size = new System.Drawing.Size(118, 21);
            this.labelTaxId.TabIndex = 1000000040;
            // 
            // labelCa
            // 
            this.labelCa.BackColor = System.Drawing.Color.White;
            this.labelCa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCa.Location = new System.Drawing.Point(722, 1);
            this.labelCa.Name = "labelCa";
            this.labelCa.Size = new System.Drawing.Size(104, 21);
            this.labelCa.TabIndex = 1000000038;
            // 
            // labelUi
            // 
            this.labelUi.BackColor = System.Drawing.Color.White;
            this.labelUi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelUi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUi.Location = new System.Drawing.Point(722, 23);
            this.labelUi.Name = "labelUi";
            this.labelUi.Size = new System.Drawing.Size(104, 21);
            this.labelUi.TabIndex = 1000000036;
            // 
            // labelAddress
            // 
            this.labelAddress.BackColor = System.Drawing.Color.White;
            this.labelAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddress.Location = new System.Drawing.Point(111, 23);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(534, 21);
            this.labelAddress.TabIndex = 1000000035;
            // 
            // labelFk
            // 
            this.labelFk.BackColor = System.Drawing.Color.White;
            this.labelFk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelFk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFk.Location = new System.Drawing.Point(111, 1);
            this.labelFk.Name = "labelFk";
            this.labelFk.Size = new System.Drawing.Size(534, 21);
            this.labelFk.TabIndex = 1000000034;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(645, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 26);
            this.label5.TabIndex = 1000000033;
            this.label5.Text = "เครื่องวัดฯ";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label15.Location = new System.Drawing.Point(691, 0);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(29, 26);
            this.Label15.TabIndex = 1000000031;
            this.Label15.Text = "CA";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label11.Location = new System.Drawing.Point(1, 20);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(100, 26);
            this.Label11.TabIndex = 1000000030;
            this.Label11.Text = "สถานที่ใช้ไฟฟ้า";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label9.Location = new System.Drawing.Point(14, -1);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(87, 26);
            this.Label9.TabIndex = 1000000029;
            this.Label9.Text = "ชื่อผู้ใช้ไฟฟ้า";
            // 
            // GridDetail
            // 
            this.GridDetail.AllowUserToAddRows = false;
            this.GridDetail.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GridDetail.BackgroundColor = System.Drawing.Color.White;
            this.GridDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCheckBoxDetail,
            this.colCADetail,
            this.col3,
            this.colReqNo,
            this.col7,
            this.colAmountDetail,
            this.col6,
            this.colPreviousDueDate,
            this.coldebtBalance,
            this.colPayment,
            this.col11,
            this.col5,
            this.col4,
            this.col12,
            this.colcountDay,
            this.colDefaultpenalty,
            this.colCkStatus,
            this.colVatDetail,
            this.dataGridViewTextBoxColumn3,
            this.col15,
            this.col26,
            this.ColDebtId,
            this.ColdebtIdSet,
            this.ColdebtType,
            this.colNewDuedate,
            this.colDebtLock,
            this.colInsDebtInsHdrId,
            this.colInstallmentsFlag});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridDetail.DefaultCellStyle = dataGridViewCellStyle18;
            this.GridDetail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GridDetail.Location = new System.Drawing.Point(0, 101);
            this.GridDetail.Name = "GridDetail";
            this.GridDetail.RowHeadersVisible = false;
            this.GridDetail.RowHeadersWidth = 10;
            this.GridDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridDetail.Size = new System.Drawing.Size(831, 309);
            this.GridDetail.TabIndex = 2;
            this.GridDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridDetail_CellClick);
            this.GridDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridDetail_CellContentClick);
            this.GridDetail.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GridDetail_ColumnHeaderMouseClick);
            this.GridDetail.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.GridDetail_DataError);
            this.GridDetail.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GridDetail_EditingControlShowing);
            // 
            // LabelTmp_Time
            // 
            this.LabelTmp_Time.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTmp_Time.ForeColor = System.Drawing.Color.Red;
            this.LabelTmp_Time.Location = new System.Drawing.Point(587, 45);
            this.LabelTmp_Time.Name = "LabelTmp_Time";
            this.LabelTmp_Time.Size = new System.Drawing.Size(124, 23);
            this.LabelTmp_Time.TabIndex = 1000000046;
            this.LabelTmp_Time.Text = "ไฟชั่วคราว ถึง 01/02/2553 ";
            this.LabelTmp_Time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LabelTmp_Time.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(419, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 26);
            this.label10.TabIndex = 1000000041;
            this.label10.Text = "สาขาที่";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(236, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 28);
            this.label7.TabIndex = 1000000039;
            this.label7.Text = "TAX ID";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btOK);
            this.panel5.Controls.Add(this.ButtonSearch);
            this.panel5.Controls.Add(this.comboBoxDetail);
            this.panel5.Controls.Add(this.TextBoxSearch);
            this.panel5.Controls.Add(this.txtCount);
            this.panel5.Controls.Add(this.ButtonDelete);
            this.panel5.Controls.Add(this.CheckBoxSelectAll);
            this.panel5.Controls.Add(this.ButtonClear);
            this.panel5.Controls.Add(this.DataGridViewDetail);
            this.panel5.Location = new System.Drawing.Point(7, 25);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(167, 411);
            this.panel5.TabIndex = 129;
            // 
            // comboBoxDetail
            // 
            this.comboBoxDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDetail.FormattingEnabled = true;
            this.comboBoxDetail.Items.AddRange(new object[] {
            "ปกติ",
            "วันครบกำหนด",
            "เบี้ยปรับ"});
            this.comboBoxDetail.Location = new System.Drawing.Point(3, 3);
            this.comboBoxDetail.Name = "comboBoxDetail";
            this.comboBoxDetail.Size = new System.Drawing.Size(105, 28);
            this.comboBoxDetail.TabIndex = 1000000013;
            this.comboBoxDetail.SelectedIndexChanged += new System.EventHandler(this.comboBoxDetail_SelectedIndexChanged);
            // 
            // TextBoxSearch
            // 
            this.TextBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxSearch.Location = new System.Drawing.Point(3, 72);
            this.TextBoxSearch.MaxLength = 9;
            this.TextBoxSearch.Name = "TextBoxSearch";
            this.TextBoxSearch.Size = new System.Drawing.Size(114, 26);
            this.TextBoxSearch.TabIndex = 2;
            // 
            // txtCount
            // 
            this.txtCount.Enabled = false;
            this.txtCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCount.Location = new System.Drawing.Point(113, 4);
            this.txtCount.MaxLength = 9;
            this.txtCount.Name = "txtCount";
            this.txtCount.Size = new System.Drawing.Size(49, 26);
            this.txtCount.TabIndex = 70;
            this.txtCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCount_KeyPress);
            // 
            // CheckBoxSelectAll
            // 
            this.CheckBoxSelectAll.AutoSize = true;
            this.CheckBoxSelectAll.Location = new System.Drawing.Point(8, 112);
            this.CheckBoxSelectAll.Name = "CheckBoxSelectAll";
            this.CheckBoxSelectAll.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelectAll.TabIndex = 0;
            this.CheckBoxSelectAll.UseVisualStyleBackColor = true;
            this.CheckBoxSelectAll.Click += new System.EventHandler(this.CheckBoxSelectAll_Click);
            this.CheckBoxSelectAll.MouseHover += new System.EventHandler(this.CheckBoxSelectAll_MouseHover);
            // 
            // DataGridViewDetail
            // 
            this.DataGridViewDetail.AllowUserToAddRows = false;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DataGridViewDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.DataGridViewDetail.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCheckBox,
            this.ColCA,
            this.colInv,
            this.ColUI,
            this.ColCustomerName,
            this.ColAddress,
            this.ColAmount,
            this.ColckCancel,
            this.ColTotalAmount,
            this.ColSumTotalAmount,
            this.ColPenaltyAll,
            this.ColVat,
            this.ColType,
            this.ColCusName,
            this.ColCusTaxId,
            this.ColCusTaxBranch,
            this.ColCusAddress,
            this.ColCustomerId,
            this.collId,
            this.ColTax20,
            this.ColTaxBranch,
            this.colStatus});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.DefaultCellStyle = dataGridViewCellStyle23;
            this.DataGridViewDetail.Location = new System.Drawing.Point(3, 101);
            this.DataGridViewDetail.Name = "DataGridViewDetail";
            this.DataGridViewDetail.RowHeadersVisible = false;
            this.DataGridViewDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DataGridViewDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewDetail.Size = new System.Drawing.Size(159, 309);
            this.DataGridViewDetail.TabIndex = 1;
            this.DataGridViewDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewDetail_CellClick);
            this.DataGridViewDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewDetail_CellContentClick);
            // 
            // ColCheckBox
            // 
            this.ColCheckBox.DataPropertyName = "SelectCheck";
            this.ColCheckBox.Frozen = true;
            this.ColCheckBox.HeaderText = "";
            this.ColCheckBox.Name = "ColCheckBox";
            this.ColCheckBox.Width = 20;
            // 
            // ColCA
            // 
            this.ColCA.DataPropertyName = "ca";
            this.ColCA.HeaderText = "CA/INV NO.";
            this.ColCA.Name = "ColCA";
            this.ColCA.ReadOnly = true;
            this.ColCA.Width = 136;
            // 
            // colInv
            // 
            this.colInv.HeaderText = "เลขที่ใบแจ้งบิลรวม";
            this.colInv.Name = "colInv";
            this.colInv.Visible = false;
            // 
            // ColUI
            // 
            this.ColUI.DataPropertyName = "ui";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColUI.DefaultCellStyle = dataGridViewCellStyle20;
            this.ColUI.HeaderText = "รหัสเครื่องวัด";
            this.ColUI.Name = "ColUI";
            this.ColUI.Visible = false;
            this.ColUI.Width = 120;
            // 
            // ColCustomerName
            // 
            this.ColCustomerName.DataPropertyName = "FirstName";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColCustomerName.DefaultCellStyle = dataGridViewCellStyle21;
            this.ColCustomerName.HeaderText = "ชื่อลูกค้า";
            this.ColCustomerName.Name = "ColCustomerName";
            this.ColCustomerName.Visible = false;
            this.ColCustomerName.Width = 200;
            // 
            // ColAddress
            // 
            this.ColAddress.DataPropertyName = "coAddress";
            this.ColAddress.HeaderText = "สถานที่ใช้ไฟฟ้า";
            this.ColAddress.Name = "ColAddress";
            this.ColAddress.Visible = false;
            this.ColAddress.Width = 190;
            // 
            // ColAmount
            // 
            this.ColAmount.DataPropertyName = "EleInvCount";
            this.ColAmount.HeaderText = "จำนวนฉบับ";
            this.ColAmount.Name = "ColAmount";
            this.ColAmount.Visible = false;
            this.ColAmount.Width = 150;
            // 
            // ColckCancel
            // 
            this.ColckCancel.DataPropertyName = "debtType";
            this.ColckCancel.HeaderText = "ค่างดจ่ายไฟ";
            this.ColckCancel.Name = "ColckCancel";
            this.ColckCancel.Visible = false;
            // 
            // ColTotalAmount
            // 
            this.ColTotalAmount.DataPropertyName = "EleTotalAmount";
            this.ColTotalAmount.HeaderText = "รวมค่าไฟฟ้า(บาท)";
            this.ColTotalAmount.Name = "ColTotalAmount";
            this.ColTotalAmount.Visible = false;
            this.ColTotalAmount.Width = 200;
            // 
            // ColSumTotalAmount
            // 
            this.ColSumTotalAmount.DataPropertyName = "TotalAmount";
            this.ColSumTotalAmount.HeaderText = "รวมเงินทั้งหมด";
            this.ColSumTotalAmount.Name = "ColSumTotalAmount";
            this.ColSumTotalAmount.Visible = false;
            // 
            // ColPenaltyAll
            // 
            this.ColPenaltyAll.DataPropertyName = "Defaultpenalty";
            this.ColPenaltyAll.HeaderText = "เบี้ยปรับทั้งหมด";
            this.ColPenaltyAll.Name = "ColPenaltyAll";
            this.ColPenaltyAll.Visible = false;
            this.ColPenaltyAll.Width = 150;
            // 
            // ColVat
            // 
            this.ColVat.DataPropertyName = "EleTotalVat";
            this.ColVat.HeaderText = "VAT";
            this.ColVat.Name = "ColVat";
            this.ColVat.Visible = false;
            // 
            // ColType
            // 
            this.ColType.DataPropertyName = "custType";
            this.ColType.HeaderText = "Type";
            this.ColType.Name = "ColType";
            this.ColType.Visible = false;
            // 
            // ColCusName
            // 
            this.ColCusName.DataPropertyName = "payeeEleName";
            this.ColCusName.HeaderText = "ผู้ชำระเงิน";
            this.ColCusName.Name = "ColCusName";
            this.ColCusName.Visible = false;
            // 
            // ColCusTaxId
            // 
            this.ColCusTaxId.DataPropertyName = "payeeEleTax20";
            this.ColCusTaxId.HeaderText = "TaxId";
            this.ColCusTaxId.Name = "ColCusTaxId";
            this.ColCusTaxId.Visible = false;
            // 
            // ColCusTaxBranch
            // 
            this.ColCusTaxBranch.DataPropertyName = "payeeEleTaxBranch";
            this.ColCusTaxBranch.HeaderText = "TaxBranch";
            this.ColCusTaxBranch.Name = "ColCusTaxBranch";
            this.ColCusTaxBranch.Visible = false;
            // 
            // ColCusAddress
            // 
            this.ColCusAddress.DataPropertyName = "payeeEleAddress";
            this.ColCusAddress.HeaderText = "CusAddress";
            this.ColCusAddress.Name = "ColCusAddress";
            this.ColCusAddress.Visible = false;
            // 
            // ColCustomerId
            // 
            this.ColCustomerId.DataPropertyName = "custId";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColCustomerId.DefaultCellStyle = dataGridViewCellStyle22;
            this.ColCustomerId.HeaderText = "รหัสลูกค้า";
            this.ColCustomerId.Name = "ColCustomerId";
            this.ColCustomerId.Visible = false;
            this.ColCustomerId.Width = 110;
            // 
            // collId
            // 
            this.collId.DataPropertyName = "collId";
            this.collId.HeaderText = "collId";
            this.collId.Name = "collId";
            this.collId.Visible = false;
            // 
            // ColTax20
            // 
            this.ColTax20.DataPropertyName = "Tax20";
            this.ColTax20.HeaderText = "Tax20";
            this.ColTax20.Name = "ColTax20";
            this.ColTax20.Visible = false;
            // 
            // ColTaxBranch
            // 
            this.ColTaxBranch.DataPropertyName = "TaxBranch";
            this.ColTaxBranch.HeaderText = "TaxBranch";
            this.ColTaxBranch.Name = "ColTaxBranch";
            this.ColTaxBranch.Visible = false;
            // 
            // colStatus
            // 
            this.colStatus.DataPropertyName = "StatuslockCheque";
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Panel4);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.Label39);
            this.panel2.Controls.Add(this.Label38);
            this.panel2.Controls.Add(this.Label22);
            this.panel2.Controls.Add(this.Label13);
            this.panel2.Controls.Add(this.txtFine);
            this.panel2.Controls.Add(this.txtTotalSum);
            this.panel2.Controls.Add(this.Label36);
            this.panel2.Controls.Add(this.Label37);
            this.panel2.Controls.Add(this.txtTotal);
            this.panel2.Controls.Add(this.Label35);
            this.panel2.Controls.Add(this.txtVat);
            this.panel2.Controls.Add(this.txtAmount);
            this.panel2.Controls.Add(this.Label34);
            this.panel2.Controls.Add(this.Label33);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 436);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1003, 140);
            this.panel2.TabIndex = 5;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel4.Controls.Add(this.lblCountList);
            this.Panel4.Controls.Add(this.lblAmountPayment);
            this.Panel4.Controls.Add(this.lblCountListDetail);
            this.Panel4.Controls.Add(this.label18);
            this.Panel4.Controls.Add(this.lblAmountDetail);
            this.Panel4.Controls.Add(this.label8);
            this.Panel4.Controls.Add(this.lblCaDetail);
            this.Panel4.Controls.Add(this.Label26);
            this.Panel4.Controls.Add(this.Label32);
            this.Panel4.Controls.Add(this.Label29);
            this.Panel4.Controls.Add(this.Label31);
            this.Panel4.Controls.Add(this.Label30);
            this.Panel4.Location = new System.Drawing.Point(254, 7);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(454, 77);
            this.Panel4.TabIndex = 127;
            // 
            // lblCountList
            // 
            this.lblCountList.BackColor = System.Drawing.Color.White;
            this.lblCountList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCountList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblCountList.Location = new System.Drawing.Point(344, 40);
            this.lblCountList.Name = "lblCountList";
            this.lblCountList.Size = new System.Drawing.Size(55, 25);
            this.lblCountList.TabIndex = 110;
            this.lblCountList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAmountPayment
            // 
            this.lblAmountPayment.BackColor = System.Drawing.Color.White;
            this.lblAmountPayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAmountPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblAmountPayment.Location = new System.Drawing.Point(175, 38);
            this.lblAmountPayment.Name = "lblAmountPayment";
            this.lblAmountPayment.Size = new System.Drawing.Size(134, 25);
            this.lblAmountPayment.TabIndex = 101;
            this.lblAmountPayment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCountListDetail
            // 
            this.lblCountListDetail.BackColor = System.Drawing.Color.White;
            this.lblCountListDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCountListDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblCountListDetail.Location = new System.Drawing.Point(344, 7);
            this.lblCountListDetail.Name = "lblCountListDetail";
            this.lblCountListDetail.Size = new System.Drawing.Size(55, 25);
            this.lblCountListDetail.TabIndex = 109;
            this.lblCountListDetail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(401, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 26);
            this.label18.TabIndex = 107;
            this.label18.Text = "รายการ";
            // 
            // lblAmountDetail
            // 
            this.lblAmountDetail.BackColor = System.Drawing.Color.White;
            this.lblAmountDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAmountDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblAmountDetail.Location = new System.Drawing.Point(175, 7);
            this.lblAmountDetail.Name = "lblAmountDetail";
            this.lblAmountDetail.Size = new System.Drawing.Size(135, 25);
            this.lblAmountDetail.TabIndex = 106;
            this.lblAmountDetail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(307, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 26);
            this.label8.TabIndex = 105;
            this.label8.Text = "บาท";
            // 
            // lblCaDetail
            // 
            this.lblCaDetail.BackColor = System.Drawing.Color.White;
            this.lblCaDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCaDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblCaDetail.Location = new System.Drawing.Point(27, 7);
            this.lblCaDetail.Name = "lblCaDetail";
            this.lblCaDetail.Size = new System.Drawing.Size(100, 25);
            this.lblCaDetail.TabIndex = 104;
            this.lblCaDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label26.ForeColor = System.Drawing.Color.Blue;
            this.Label26.Location = new System.Drawing.Point(401, 41);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(56, 26);
            this.Label26.TabIndex = 93;
            this.Label26.Text = "รายการ";
            // 
            // Label32
            // 
            this.Label32.AutoSize = true;
            this.Label32.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label32.ForeColor = System.Drawing.Color.Blue;
            this.Label32.Location = new System.Drawing.Point(134, 37);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(44, 26);
            this.Label32.TabIndex = 96;
            this.Label32.Text = "ชำระ";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label29.ForeColor = System.Drawing.Color.Blue;
            this.Label29.Location = new System.Drawing.Point(126, 6);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(52, 26);
            this.Label29.TabIndex = 99;
            this.Label29.Text = "จำนวน";
            // 
            // Label31
            // 
            this.Label31.AutoSize = true;
            this.Label31.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label31.ForeColor = System.Drawing.Color.Blue;
            this.Label31.Location = new System.Drawing.Point(307, 37);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(36, 26);
            this.Label31.TabIndex = 97;
            this.Label31.Text = "บาท";
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label30.ForeColor = System.Drawing.Color.Blue;
            this.Label30.Location = new System.Drawing.Point(-1, 6);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(29, 26);
            this.Label30.TabIndex = 98;
            this.Label30.Text = "CA";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gainsboro;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.lblCountcapayment);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.lblCountCa);
            this.panel7.Controls.Add(this.lblCountCreditNote);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Controls.Add(this.label28);
            this.panel7.Controls.Add(this.label41);
            this.panel7.Location = new System.Drawing.Point(8, 7);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(244, 77);
            this.panel7.TabIndex = 128;
            // 
            // lblCountcapayment
            // 
            this.lblCountcapayment.BackColor = System.Drawing.Color.White;
            this.lblCountcapayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCountcapayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblCountcapayment.Location = new System.Drawing.Point(48, 40);
            this.lblCountcapayment.Name = "lblCountcapayment";
            this.lblCountcapayment.Size = new System.Drawing.Size(51, 25);
            this.lblCountcapayment.TabIndex = 108;
            this.lblCountcapayment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(99, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 26);
            this.label1.TabIndex = 107;
            this.label1.Text = "CA";
            // 
            // lblCountCa
            // 
            this.lblCountCa.BackColor = System.Drawing.Color.White;
            this.lblCountCa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCountCa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblCountCa.Location = new System.Drawing.Point(48, 7);
            this.lblCountCa.Name = "lblCountCa";
            this.lblCountCa.Size = new System.Drawing.Size(51, 25);
            this.lblCountCa.TabIndex = 106;
            this.lblCountCa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCountCreditNote
            // 
            this.lblCountCreditNote.BackColor = System.Drawing.Color.White;
            this.lblCountCreditNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCountCreditNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblCountCreditNote.Location = new System.Drawing.Point(179, 7);
            this.lblCountCreditNote.Name = "lblCountCreditNote";
            this.lblCountCreditNote.Size = new System.Drawing.Size(60, 25);
            this.lblCountCreditNote.TabIndex = 104;
            this.lblCountCreditNote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.Location = new System.Drawing.Point(-1, 39);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 26);
            this.label24.TabIndex = 96;
            this.label24.Text = "ชำระ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(-1, 6);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 26);
            this.label25.TabIndex = 99;
            this.label25.Text = "จำนวน";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label28.ForeColor = System.Drawing.Color.Blue;
            this.label28.Location = new System.Drawing.Point(127, 6);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 26);
            this.label28.TabIndex = 97;
            this.label28.Text = "หนี้ค้าง";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label41.ForeColor = System.Drawing.Color.Blue;
            this.label41.Location = new System.Drawing.Point(98, 6);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(29, 26);
            this.label41.TabIndex = 98;
            this.label41.Text = "CA";
            // 
            // Label39
            // 
            this.Label39.AutoSize = true;
            this.Label39.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label39.Location = new System.Drawing.Point(962, 109);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(36, 26);
            this.Label39.TabIndex = 124;
            this.Label39.Text = "บาท";
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label38.Location = new System.Drawing.Point(962, 76);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(36, 26);
            this.Label38.TabIndex = 123;
            this.Label38.Text = "บาท";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label22.Location = new System.Drawing.Point(961, 43);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(36, 26);
            this.Label22.TabIndex = 122;
            this.Label22.Text = "บาท";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label13.Location = new System.Drawing.Point(961, 10);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(36, 26);
            this.Label13.TabIndex = 121;
            this.Label13.Text = "บาท";
            // 
            // txtFine
            // 
            this.txtFine.BackColor = System.Drawing.Color.White;
            this.txtFine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtFine.Location = new System.Drawing.Point(817, 109);
            this.txtFine.Name = "txtFine";
            this.txtFine.Size = new System.Drawing.Size(143, 25);
            this.txtFine.TabIndex = 120;
            this.txtFine.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalSum
            // 
            this.txtTotalSum.BackColor = System.Drawing.Color.White;
            this.txtTotalSum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTotalSum.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalSum.Location = new System.Drawing.Point(183, 89);
            this.txtTotalSum.Name = "txtTotalSum";
            this.txtTotalSum.Size = new System.Drawing.Size(448, 39);
            this.txtTotalSum.TabIndex = 103;
            this.txtTotalSum.Text = "0.00";
            this.txtTotalSum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label36
            // 
            this.Label36.AutoSize = true;
            this.Label36.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label36.Location = new System.Drawing.Point(659, 109);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(152, 26);
            this.Label36.TabIndex = 119;
            this.Label36.Text = "เบี้ยปรับและค่าเสียหายฯ";
            this.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.BackColor = System.Drawing.Color.Transparent;
            this.Label37.Font = new System.Drawing.Font("TH Sarabun New", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label37.ForeColor = System.Drawing.Color.Blue;
            this.Label37.Location = new System.Drawing.Point(10, 79);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(173, 57);
            this.Label37.TabIndex = 102;
            this.Label37.Text = "รวมรับชำระ";
            this.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.White;
            this.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotal.Location = new System.Drawing.Point(817, 76);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(143, 25);
            this.txtTotal.TabIndex = 118;
            this.txtTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label35
            // 
            this.Label35.AutoSize = true;
            this.Label35.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label35.Location = new System.Drawing.Point(774, 76);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(37, 26);
            this.Label35.TabIndex = 117;
            this.Label35.Text = "รวม";
            this.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVat
            // 
            this.txtVat.BackColor = System.Drawing.Color.White;
            this.txtVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtVat.Location = new System.Drawing.Point(817, 43);
            this.txtVat.Name = "txtVat";
            this.txtVat.Size = new System.Drawing.Size(143, 25);
            this.txtVat.TabIndex = 116;
            this.txtVat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.Color.White;
            this.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtAmount.Location = new System.Drawing.Point(817, 10);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(143, 25);
            this.txtAmount.TabIndex = 115;
            this.txtAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label34
            // 
            this.Label34.AutoSize = true;
            this.Label34.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label34.Location = new System.Drawing.Point(753, 10);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(58, 26);
            this.Label34.TabIndex = 114;
            this.Label34.Text = "รวมเงิน";
            this.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label33
            // 
            this.Label33.AutoSize = true;
            this.Label33.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label33.Location = new System.Drawing.Point(717, 43);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(94, 26);
            this.Label33.TabIndex = 113;
            this.Label33.Text = "ภาษีมูลค่าเพิ่ม";
            this.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanelEmployee
            // 
            this.PanelEmployee.Controls.Add(this.groupBoxEmployee);
            this.PanelEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelEmployee.Location = new System.Drawing.Point(0, 39);
            this.PanelEmployee.Name = "PanelEmployee";
            this.PanelEmployee.Size = new System.Drawing.Size(1009, 46);
            this.PanelEmployee.TabIndex = 4;
            // 
            // groupBoxEmployee
            // 
            this.groupBoxEmployee.Controls.Add(this.label3);
            this.groupBoxEmployee.Controls.Add(this.label2);
            this.groupBoxEmployee.Controls.Add(this.LabelStationNo);
            this.groupBoxEmployee.Controls.Add(this.label4);
            this.groupBoxEmployee.Controls.Add(this.LabelEmployeeName);
            this.groupBoxEmployee.Controls.Add(this.LabelEmployeeCode);
            this.groupBoxEmployee.Controls.Add(this.LabelFkName);
            this.groupBoxEmployee.Location = new System.Drawing.Point(4, -4);
            this.groupBoxEmployee.Name = "groupBoxEmployee";
            this.groupBoxEmployee.Size = new System.Drawing.Size(1005, 46);
            this.groupBoxEmployee.TabIndex = 1000000022;
            this.groupBoxEmployee.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(361, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 26);
            this.label3.TabIndex = 1000000009;
            this.label3.Text = "รหัสพนง.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(6, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 26);
            this.label2.TabIndex = 1000000008;
            this.label2.Text = "ฟข.";
            // 
            // LabelStationNo
            // 
            this.LabelStationNo.BackColor = System.Drawing.Color.White;
            this.LabelStationNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelStationNo.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStationNo.Location = new System.Drawing.Point(925, 12);
            this.LabelStationNo.Name = "LabelStationNo";
            this.LabelStationNo.Size = new System.Drawing.Size(74, 25);
            this.LabelStationNo.TabIndex = 1000000020;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(854, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 26);
            this.label4.TabIndex = 1000000010;
            this.label4.Text = "ช่องชำระ";
            // 
            // LabelEmployeeName
            // 
            this.LabelEmployeeName.BackColor = System.Drawing.Color.White;
            this.LabelEmployeeName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelEmployeeName.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEmployeeName.Location = new System.Drawing.Point(542, 12);
            this.LabelEmployeeName.Name = "LabelEmployeeName";
            this.LabelEmployeeName.Size = new System.Drawing.Size(306, 25);
            this.LabelEmployeeName.TabIndex = 1000000019;
            this.LabelEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelEmployeeCode
            // 
            this.LabelEmployeeCode.BackColor = System.Drawing.Color.White;
            this.LabelEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelEmployeeCode.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEmployeeCode.Location = new System.Drawing.Point(429, 12);
            this.LabelEmployeeCode.Name = "LabelEmployeeCode";
            this.LabelEmployeeCode.Size = new System.Drawing.Size(110, 25);
            this.LabelEmployeeCode.TabIndex = 1000000018;
            // 
            // LabelFkName
            // 
            this.LabelFkName.BackColor = System.Drawing.Color.White;
            this.LabelFkName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelFkName.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelFkName.Location = new System.Drawing.Point(47, 12);
            this.LabelFkName.Name = "LabelFkName";
            this.LabelFkName.Size = new System.Drawing.Size(310, 25);
            this.LabelFkName.TabIndex = 1000000017;
            // 
            // PanelHead
            // 
            this.PanelHead.Controls.Add(this.PanelNoneQ);
            this.PanelHead.Controls.Add(this.PanelHaveQ);
            this.PanelHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelHead.Location = new System.Drawing.Point(0, 0);
            this.PanelHead.Name = "PanelHead";
            this.PanelHead.Size = new System.Drawing.Size(1009, 39);
            this.PanelHead.TabIndex = 3;
            // 
            // PanelNoneQ
            // 
            this.PanelNoneQ.Controls.Add(this.ButtonCallQNone);
            this.PanelNoneQ.Controls.Add(this.ComboBoxDocType);
            this.PanelNoneQ.Controls.Add(this.TextBoxBarcode);
            this.PanelNoneQ.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelNoneQ.Location = new System.Drawing.Point(0, 35);
            this.PanelNoneQ.Name = "PanelNoneQ";
            this.PanelNoneQ.Size = new System.Drawing.Size(1009, 39);
            this.PanelNoneQ.TabIndex = 1000000014;
            this.PanelNoneQ.Visible = false;
            // 
            // ComboBoxDocType
            // 
            this.ComboBoxDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxDocType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxDocType.FormattingEnabled = true;
            this.ComboBoxDocType.Items.AddRange(new object[] {
            "บัญชีแสดงสัญญา",
            "เลขที่เครื่องวัดฯ",
            "เลขที่ใบแจ้งหนี้ฯ",
            "รหัสก้อนงาน",
            "เลขที่ใบแจ้งหนี้บิลรวม",
            "เลขใบรับเรื่อง",
            "ค่าน้ำประปา",
            "หมายเลขคิว"});
            this.ComboBoxDocType.Location = new System.Drawing.Point(10, 6);
            this.ComboBoxDocType.Name = "ComboBoxDocType";
            this.ComboBoxDocType.Size = new System.Drawing.Size(213, 28);
            this.ComboBoxDocType.TabIndex = 1;
            this.ComboBoxDocType.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDocType_SelectedIndexChanged);
            // 
            // TextBoxBarcode
            // 
            this.TextBoxBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.TextBoxBarcode.ForeColor = System.Drawing.Color.Blue;
            this.TextBoxBarcode.Location = new System.Drawing.Point(238, 7);
            this.TextBoxBarcode.Name = "TextBoxBarcode";
            this.TextBoxBarcode.Size = new System.Drawing.Size(406, 26);
            this.TextBoxBarcode.TabIndex = 0;
            this.TextBoxBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxBarcode_KeyDown);
            this.TextBoxBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxBarcode_KeyPress);
            this.TextBoxBarcode.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.TextBoxBarcode_PreviewKeyDown);
            // 
            // PanelHaveQ
            // 
            this.PanelHaveQ.Controls.Add(this.ButtonCallQ);
            this.PanelHaveQ.Controls.Add(this.ButtonRefreshQ);
            this.PanelHaveQ.Controls.Add(this.ButtonCancelQ);
            this.PanelHaveQ.Controls.Add(this.ButtonResetQ);
            this.PanelHaveQ.Controls.Add(this.ButtonAdd);
            this.PanelHaveQ.Controls.Add(this.LabelNoQ);
            this.PanelHaveQ.Controls.Add(this.label6);
            this.PanelHaveQ.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelHaveQ.Location = new System.Drawing.Point(0, 0);
            this.PanelHaveQ.Name = "PanelHaveQ";
            this.PanelHaveQ.Size = new System.Drawing.Size(1009, 35);
            this.PanelHaveQ.TabIndex = 1000000015;
            this.PanelHaveQ.Visible = false;
            // 
            // LabelNoQ
            // 
            this.LabelNoQ.BackColor = System.Drawing.Color.White;
            this.LabelNoQ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelNoQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNoQ.ForeColor = System.Drawing.Color.Red;
            this.LabelNoQ.Location = new System.Drawing.Point(198, 4);
            this.LabelNoQ.Name = "LabelNoQ";
            this.LabelNoQ.Size = new System.Drawing.Size(124, 28);
            this.LabelNoQ.TabIndex = 1000000017;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(132, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 27);
            this.label6.TabIndex = 1000000006;
            this.label6.Text = "เลขที่คิว";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Interval = 1000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // ButtonRec
            // 
            this.ButtonRec.BackColor = System.Drawing.Color.Transparent;
            this.ButtonRec.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonRec.BorderWidth = 1;
            this.ButtonRec.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonRec.ButtonText = "รับเงิน";
            this.ButtonRec.CausesValidation = false;
            this.ButtonRec.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonRec.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonRec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonRec.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonRec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.ButtonRec.GradientAngle = 90;
            this.ButtonRec.Image = global::MyPayment.Properties.Resources.icons8_paper_money_26;
            this.ButtonRec.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonRec.Location = new System.Drawing.Point(920, 3);
            this.ButtonRec.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonRec.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonRec.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonRec.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonRec.Name = "ButtonRec";
            this.ButtonRec.ShowButtontext = true;
            this.ButtonRec.Size = new System.Drawing.Size(83, 35);
            this.ButtonRec.StartColor = System.Drawing.Color.LightGray;
            this.ButtonRec.TabIndex = 1;
            this.ButtonRec.TextLocation_X = 35;
            this.ButtonRec.TextLocation_Y = 6;
            this.ButtonRec.Transparent1 = 80;
            this.ButtonRec.Transparent2 = 120;
            this.ButtonRec.UseVisualStyleBackColor = true;
            this.ButtonRec.Click += new System.EventHandler(this.ButtonRec_Click);
            // 
            // ButtonCancelWait
            // 
            this.ButtonCancelWait.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancelWait.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancelWait.BorderWidth = 1;
            this.ButtonCancelWait.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancelWait.ButtonText = "ยกเลิกการรอคิว";
            this.ButtonCancelWait.CausesValidation = false;
            this.ButtonCancelWait.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCancelWait.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancelWait.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonCancelWait.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancelWait.GradientAngle = 90;
            this.ButtonCancelWait.Image = global::MyPayment.Properties.Resources.close2;
            this.ButtonCancelWait.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancelWait.Location = new System.Drawing.Point(245, 327);
            this.ButtonCancelWait.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancelWait.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancelWait.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancelWait.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancelWait.Name = "ButtonCancelWait";
            this.ButtonCancelWait.ShowButtontext = true;
            this.ButtonCancelWait.Size = new System.Drawing.Size(130, 40);
            this.ButtonCancelWait.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCancelWait.TabIndex = 1000000040;
            this.ButtonCancelWait.TextLocation_X = 31;
            this.ButtonCancelWait.TextLocation_Y = 8;
            this.ButtonCancelWait.Transparent1 = 80;
            this.ButtonCancelWait.Transparent2 = 120;
            this.ButtonCancelWait.UseVisualStyleBackColor = true;
            this.ButtonCancelWait.Click += new System.EventHandler(this.ButtonCancelWait_Click);
            // 
            // btCancelFine
            // 
            this.btCancelFine.BackColor = System.Drawing.Color.Transparent;
            this.btCancelFine.BorderColor = System.Drawing.Color.Transparent;
            this.btCancelFine.BorderWidth = 1;
            this.btCancelFine.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btCancelFine.ButtonText = "ยกเลิกเบี้ยปรับ";
            this.btCancelFine.CausesValidation = false;
            this.btCancelFine.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btCancelFine.EndColor = System.Drawing.Color.DarkGray;
            this.btCancelFine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCancelFine.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold);
            this.btCancelFine.ForeColor = System.Drawing.Color.Black;
            this.btCancelFine.GradientAngle = 90;
            this.btCancelFine.Image = global::MyPayment.Properties.Resources.close3;
            this.btCancelFine.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCancelFine.Location = new System.Drawing.Point(406, 69);
            this.btCancelFine.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btCancelFine.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btCancelFine.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btCancelFine.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btCancelFine.Name = "btCancelFine";
            this.btCancelFine.ShowButtontext = true;
            this.btCancelFine.Size = new System.Drawing.Size(110, 28);
            this.btCancelFine.StartColor = System.Drawing.Color.LightGray;
            this.btCancelFine.TabIndex = 1000000050;
            this.btCancelFine.TextLocation_X = 30;
            this.btCancelFine.TextLocation_Y = 3;
            this.btCancelFine.Transparent1 = 80;
            this.btCancelFine.Transparent2 = 120;
            this.btCancelFine.UseVisualStyleBackColor = true;
            this.btCancelFine.Click += new System.EventHandler(this.btCancelFine_Click);
            // 
            // buttonZ1
            // 
            this.buttonZ1.BackColor = System.Drawing.Color.Transparent;
            this.buttonZ1.BorderColor = System.Drawing.Color.Transparent;
            this.buttonZ1.BorderWidth = 1;
            this.buttonZ1.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.buttonZ1.ButtonText = "ยกเลิกค่าดำเนินการงดจ่ายไฟ";
            this.buttonZ1.CausesValidation = false;
            this.buttonZ1.Cursor = System.Windows.Forms.Cursors.No;
            this.buttonZ1.Enabled = false;
            this.buttonZ1.EndColor = System.Drawing.Color.DarkGray;
            this.buttonZ1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonZ1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold);
            this.buttonZ1.ForeColor = System.Drawing.Color.Black;
            this.buttonZ1.GradientAngle = 90;
            this.buttonZ1.Image = global::MyPayment.Properties.Resources.close3;
            this.buttonZ1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonZ1.Location = new System.Drawing.Point(518, 69);
            this.buttonZ1.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.buttonZ1.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonZ1.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.buttonZ1.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.buttonZ1.Name = "buttonZ1";
            this.buttonZ1.ShowButtontext = true;
            this.buttonZ1.Size = new System.Drawing.Size(184, 28);
            this.buttonZ1.StartColor = System.Drawing.Color.LightGray;
            this.buttonZ1.TabIndex = 1000000045;
            this.buttonZ1.TextLocation_X = 30;
            this.buttonZ1.TextLocation_Y = 3;
            this.buttonZ1.Transparent1 = 80;
            this.buttonZ1.Transparent2 = 120;
            this.buttonZ1.UseVisualStyleBackColor = true;
            this.buttonZ1.Click += new System.EventHandler(this.buttonZ1_Click);
            // 
            // ButtonAdvance
            // 
            this.ButtonAdvance.BackColor = System.Drawing.Color.Transparent;
            this.ButtonAdvance.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonAdvance.BorderWidth = 1;
            this.ButtonAdvance.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonAdvance.ButtonText = "รับเงินล่วงหน้า";
            this.ButtonAdvance.CausesValidation = false;
            this.ButtonAdvance.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAdvance.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonAdvance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdvance.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAdvance.ForeColor = System.Drawing.Color.Black;
            this.ButtonAdvance.GradientAngle = 90;
            this.ButtonAdvance.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage;
            this.ButtonAdvance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonAdvance.Location = new System.Drawing.Point(704, 69);
            this.ButtonAdvance.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonAdvance.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonAdvance.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonAdvance.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonAdvance.Name = "ButtonAdvance";
            this.ButtonAdvance.ShowButtontext = true;
            this.ButtonAdvance.Size = new System.Drawing.Size(122, 28);
            this.ButtonAdvance.StartColor = System.Drawing.Color.LightGray;
            this.ButtonAdvance.TabIndex = 1000000042;
            this.ButtonAdvance.TextLocation_X = 32;
            this.ButtonAdvance.TextLocation_Y = 3;
            this.ButtonAdvance.Transparent1 = 80;
            this.ButtonAdvance.Transparent2 = 120;
            this.ButtonAdvance.UseVisualStyleBackColor = true;
            this.ButtonAdvance.Click += new System.EventHandler(this.ButtonAdvance_Click);
            // 
            // btOK
            // 
            this.btOK.BackColor = System.Drawing.Color.Transparent;
            this.btOK.BorderColor = System.Drawing.Color.Transparent;
            this.btOK.BorderWidth = 1;
            this.btOK.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btOK.ButtonText = "";
            this.btOK.CausesValidation = false;
            this.btOK.Enabled = false;
            this.btOK.EndColor = System.Drawing.Color.Silver;
            this.btOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btOK.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btOK.ForeColor = System.Drawing.Color.Black;
            this.btOK.GradientAngle = 90;
            this.btOK.Image = global::MyPayment.Properties.Resources.ok1;
            this.btOK.Location = new System.Drawing.Point(113, 38);
            this.btOK.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btOK.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btOK.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btOK.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btOK.Name = "btOK";
            this.btOK.ShowButtontext = true;
            this.btOK.Size = new System.Drawing.Size(49, 28);
            this.btOK.StartColor = System.Drawing.Color.Gainsboro;
            this.btOK.TabIndex = 1000000014;
            this.btOK.TextLocation_X = 15;
            this.btOK.TextLocation_Y = 14;
            this.btOK.Transparent1 = 80;
            this.btOK.Transparent2 = 120;
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.Silver;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = global::MyPayment.Properties.Resources.zoom1;
            this.ButtonSearch.Location = new System.Drawing.Point(123, 72);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(39, 26);
            this.ButtonSearch.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.TabIndex = 70;
            this.ButtonSearch.TextLocation_X = 12;
            this.ButtonSearch.TextLocation_Y = 13;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            this.ButtonSearch.MouseHover += new System.EventHandler(this.ButtonSearch_MouseHover);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.BackColor = System.Drawing.Color.Transparent;
            this.ButtonDelete.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonDelete.BorderWidth = 1;
            this.ButtonDelete.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonDelete.ButtonText = "";
            this.ButtonDelete.CausesValidation = false;
            this.ButtonDelete.Enabled = false;
            this.ButtonDelete.EndColor = System.Drawing.Color.Silver;
            this.ButtonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonDelete.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonDelete.ForeColor = System.Drawing.Color.Black;
            this.ButtonDelete.GradientAngle = 90;
            this.ButtonDelete.Image = global::MyPayment.Properties.Resources.Recycle;
            this.ButtonDelete.Location = new System.Drawing.Point(59, 38);
            this.ButtonDelete.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonDelete.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonDelete.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonDelete.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.ShowButtontext = true;
            this.ButtonDelete.Size = new System.Drawing.Size(49, 28);
            this.ButtonDelete.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonDelete.TabIndex = 69;
            this.ButtonDelete.TextLocation_X = 15;
            this.ButtonDelete.TextLocation_Y = 14;
            this.ButtonDelete.Transparent1 = 80;
            this.ButtonDelete.Transparent2 = 120;
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            this.ButtonDelete.MouseHover += new System.EventHandler(this.ButtonDelete_MouseHover);
            // 
            // ButtonClear
            // 
            this.ButtonClear.BackColor = System.Drawing.Color.Transparent;
            this.ButtonClear.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonClear.BorderWidth = 1;
            this.ButtonClear.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonClear.ButtonText = "";
            this.ButtonClear.CausesValidation = false;
            this.ButtonClear.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonClear.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonClear.ForeColor = System.Drawing.Color.Black;
            this.ButtonClear.GradientAngle = 90;
            this.ButtonClear.Image = global::MyPayment.Properties.Resources.refresh;
            this.ButtonClear.Location = new System.Drawing.Point(4, 38);
            this.ButtonClear.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonClear.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonClear.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonClear.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.ShowButtontext = true;
            this.ButtonClear.Size = new System.Drawing.Size(49, 28);
            this.ButtonClear.StartColor = System.Drawing.Color.LightGray;
            this.ButtonClear.TabIndex = 68;
            this.ButtonClear.TextLocation_X = 15;
            this.ButtonClear.TextLocation_Y = 14;
            this.ButtonClear.Transparent1 = 80;
            this.ButtonClear.Transparent2 = 120;
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            this.ButtonClear.MouseHover += new System.EventHandler(this.ButtonClear_MouseHover);
            // 
            // ButtonCallQNone
            // 
            this.ButtonCallQNone.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCallQNone.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCallQNone.BorderWidth = 1;
            this.ButtonCallQNone.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCallQNone.ButtonText = "เรียกคิว";
            this.ButtonCallQNone.CausesValidation = false;
            this.ButtonCallQNone.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCallQNone.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCallQNone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCallQNone.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonCallQNone.ForeColor = System.Drawing.Color.Black;
            this.ButtonCallQNone.GradientAngle = 90;
            this.ButtonCallQNone.Image = global::MyPayment.Properties.Resources.user1;
            this.ButtonCallQNone.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCallQNone.Location = new System.Drawing.Point(668, 7);
            this.ButtonCallQNone.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCallQNone.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCallQNone.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCallQNone.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCallQNone.Name = "ButtonCallQNone";
            this.ButtonCallQNone.ShowButtontext = true;
            this.ButtonCallQNone.Size = new System.Drawing.Size(106, 28);
            this.ButtonCallQNone.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCallQNone.TabIndex = 1000000042;
            this.ButtonCallQNone.TextLocation_X = 35;
            this.ButtonCallQNone.TextLocation_Y = 1;
            this.ButtonCallQNone.Transparent1 = 80;
            this.ButtonCallQNone.Transparent2 = 120;
            this.ButtonCallQNone.UseVisualStyleBackColor = true;
            this.ButtonCallQNone.Click += new System.EventHandler(this.ButtonCallQNone_Click);
            // 
            // ButtonCallQ
            // 
            this.ButtonCallQ.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCallQ.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCallQ.BorderWidth = 1;
            this.ButtonCallQ.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCallQ.ButtonText = "เรียกคิว";
            this.ButtonCallQ.CausesValidation = false;
            this.ButtonCallQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCallQ.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCallQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCallQ.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonCallQ.ForeColor = System.Drawing.Color.Black;
            this.ButtonCallQ.GradientAngle = 90;
            this.ButtonCallQ.Image = global::MyPayment.Properties.Resources.user1;
            this.ButtonCallQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCallQ.Location = new System.Drawing.Point(554, 4);
            this.ButtonCallQ.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCallQ.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCallQ.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCallQ.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCallQ.Name = "ButtonCallQ";
            this.ButtonCallQ.ShowButtontext = true;
            this.ButtonCallQ.Size = new System.Drawing.Size(106, 28);
            this.ButtonCallQ.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCallQ.TabIndex = 1000000041;
            this.ButtonCallQ.TextLocation_X = 35;
            this.ButtonCallQ.TextLocation_Y = 1;
            this.ButtonCallQ.Transparent1 = 80;
            this.ButtonCallQ.Transparent2 = 120;
            this.ButtonCallQ.UseVisualStyleBackColor = true;
            this.ButtonCallQ.Click += new System.EventHandler(this.ButtonCallQ_Click);
            // 
            // ButtonRefreshQ
            // 
            this.ButtonRefreshQ.BackColor = System.Drawing.Color.Transparent;
            this.ButtonRefreshQ.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonRefreshQ.BorderWidth = 1;
            this.ButtonRefreshQ.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonRefreshQ.ButtonText = "เรียกซ้ำ";
            this.ButtonRefreshQ.CausesValidation = false;
            this.ButtonRefreshQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonRefreshQ.Enabled = false;
            this.ButtonRefreshQ.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonRefreshQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonRefreshQ.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonRefreshQ.ForeColor = System.Drawing.Color.Black;
            this.ButtonRefreshQ.GradientAngle = 90;
            this.ButtonRefreshQ.Image = global::MyPayment.Properties.Resources.refresh;
            this.ButtonRefreshQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonRefreshQ.Location = new System.Drawing.Point(668, 4);
            this.ButtonRefreshQ.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonRefreshQ.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonRefreshQ.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonRefreshQ.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonRefreshQ.Name = "ButtonRefreshQ";
            this.ButtonRefreshQ.ShowButtontext = true;
            this.ButtonRefreshQ.Size = new System.Drawing.Size(106, 28);
            this.ButtonRefreshQ.StartColor = System.Drawing.Color.LightGray;
            this.ButtonRefreshQ.TabIndex = 1000000040;
            this.ButtonRefreshQ.TextLocation_X = 35;
            this.ButtonRefreshQ.TextLocation_Y = 1;
            this.ButtonRefreshQ.Transparent1 = 80;
            this.ButtonRefreshQ.Transparent2 = 120;
            this.ButtonRefreshQ.UseVisualStyleBackColor = true;
            this.ButtonRefreshQ.Click += new System.EventHandler(this.ButtonRefreshQ_Click);
            // 
            // ButtonCancelQ
            // 
            this.ButtonCancelQ.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancelQ.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancelQ.BorderWidth = 1;
            this.ButtonCancelQ.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancelQ.ButtonText = "ยกเลิกคิว";
            this.ButtonCancelQ.CausesValidation = false;
            this.ButtonCancelQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCancelQ.Enabled = false;
            this.ButtonCancelQ.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCancelQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancelQ.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonCancelQ.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancelQ.GradientAngle = 90;
            this.ButtonCancelQ.Image = global::MyPayment.Properties.Resources.close2;
            this.ButtonCancelQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancelQ.Location = new System.Drawing.Point(782, 4);
            this.ButtonCancelQ.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancelQ.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancelQ.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancelQ.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancelQ.Name = "ButtonCancelQ";
            this.ButtonCancelQ.ShowButtontext = true;
            this.ButtonCancelQ.Size = new System.Drawing.Size(106, 28);
            this.ButtonCancelQ.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCancelQ.TabIndex = 1000000039;
            this.ButtonCancelQ.TextLocation_X = 35;
            this.ButtonCancelQ.TextLocation_Y = 1;
            this.ButtonCancelQ.Transparent1 = 80;
            this.ButtonCancelQ.Transparent2 = 120;
            this.ButtonCancelQ.UseVisualStyleBackColor = true;
            this.ButtonCancelQ.Click += new System.EventHandler(this.ButtonCancelQ_Click);
            // 
            // ButtonResetQ
            // 
            this.ButtonResetQ.BackColor = System.Drawing.Color.Transparent;
            this.ButtonResetQ.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonResetQ.BorderWidth = 1;
            this.ButtonResetQ.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonResetQ.ButtonText = "หยุดพัก";
            this.ButtonResetQ.CausesValidation = false;
            this.ButtonResetQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonResetQ.Enabled = false;
            this.ButtonResetQ.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonResetQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonResetQ.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonResetQ.ForeColor = System.Drawing.Color.Black;
            this.ButtonResetQ.GradientAngle = 90;
            this.ButtonResetQ.Image = global::MyPayment.Properties.Resources.pause;
            this.ButtonResetQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonResetQ.Location = new System.Drawing.Point(896, 4);
            this.ButtonResetQ.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonResetQ.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonResetQ.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonResetQ.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonResetQ.Name = "ButtonResetQ";
            this.ButtonResetQ.ShowButtontext = true;
            this.ButtonResetQ.Size = new System.Drawing.Size(106, 28);
            this.ButtonResetQ.StartColor = System.Drawing.Color.LightGray;
            this.ButtonResetQ.TabIndex = 1000000038;
            this.ButtonResetQ.TextLocation_X = 35;
            this.ButtonResetQ.TextLocation_Y = 1;
            this.ButtonResetQ.Transparent1 = 80;
            this.ButtonResetQ.Transparent2 = 120;
            this.ButtonResetQ.UseVisualStyleBackColor = true;
            this.ButtonResetQ.Click += new System.EventHandler(this.ButtonResetQ_Click);
            // 
            // ButtonAdd
            // 
            this.ButtonAdd.BackColor = System.Drawing.Color.Transparent;
            this.ButtonAdd.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonAdd.BorderWidth = 1;
            this.ButtonAdd.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonAdd.ButtonText = "เพิ่ม";
            this.ButtonAdd.CausesValidation = false;
            this.ButtonAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAdd.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdd.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonAdd.ForeColor = System.Drawing.Color.Black;
            this.ButtonAdd.GradientAngle = 90;
            this.ButtonAdd.Image = global::MyPayment.Properties.Resources.add;
            this.ButtonAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonAdd.Location = new System.Drawing.Point(10, 4);
            this.ButtonAdd.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonAdd.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonAdd.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonAdd.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonAdd.Name = "ButtonAdd";
            this.ButtonAdd.ShowButtontext = true;
            this.ButtonAdd.Size = new System.Drawing.Size(99, 28);
            this.ButtonAdd.StartColor = System.Drawing.Color.LightGray;
            this.ButtonAdd.TabIndex = 1000000037;
            this.ButtonAdd.TextLocation_X = 35;
            this.ButtonAdd.TextLocation_Y = 1;
            this.ButtonAdd.Transparent1 = 80;
            this.ButtonAdd.Transparent2 = 120;
            this.ButtonAdd.UseVisualStyleBackColor = true;
            this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // clientSocket2
            // 
            this.clientSocket2.EOLChar = '\n';
            this.clientSocket2.LineMode = true;
            this.clientSocket2.PacketSize = 10024;
            this.clientSocket2.SynchronizingObject = this;
            this.clientSocket2.Receive += new MyPayment.Models.ClientSocket.ReceiveEventHandler(this.clientSocket2_Receive);
            // 
            // clientSocket1
            // 
            this.clientSocket1.EOLChar = '\n';
            this.clientSocket1.LineMode = true;
            this.clientSocket1.PacketSize = 10024;
            this.clientSocket1.SynchronizingObject = this;
            this.clientSocket1.Receive += new MyPayment.Models.ClientSocket.ReceiveEventHandler(this.clientSocket_Receive);
            // 
            // ColCheckBoxDetail
            // 
            this.ColCheckBoxDetail.DataPropertyName = "SelectCheck";
            this.ColCheckBoxDetail.Frozen = true;
            this.ColCheckBoxDetail.HeaderText = "";
            this.ColCheckBoxDetail.Name = "ColCheckBoxDetail";
            this.ColCheckBoxDetail.Width = 20;
            // 
            // colCADetail
            // 
            this.colCADetail.DataPropertyName = "CA";
            this.colCADetail.HeaderText = "บัญชีแสดงสัญญา";
            this.colCADetail.Name = "colCADetail";
            this.colCADetail.ReadOnly = true;
            this.colCADetail.Visible = false;
            this.colCADetail.Width = 136;
            // 
            // col3
            // 
            this.col3.DataPropertyName = "schdate";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            this.col3.DefaultCellStyle = dataGridViewCellStyle2;
            this.col3.HeaderText = "วันที่จดครั้งหลัง";
            this.col3.Name = "col3";
            this.col3.ReadOnly = true;
            this.col3.Width = 140;
            // 
            // colReqNo
            // 
            this.colReqNo.DataPropertyName = "reqNo";
            this.colReqNo.HeaderText = "เลขที่ใบรับเรื่อง";
            this.colReqNo.Name = "colReqNo";
            this.colReqNo.ReadOnly = true;
            this.colReqNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.colReqNo.Visible = false;
            this.colReqNo.Width = 160;
            // 
            // col7
            // 
            this.col7.DataPropertyName = "dueDate";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "dd/MM/yyyy";
            this.col7.DefaultCellStyle = dataGridViewCellStyle3;
            this.col7.FillWeight = 80F;
            this.col7.HeaderText = "วันครบกำหนด";
            this.col7.Name = "col7";
            this.col7.ReadOnly = true;
            this.col7.Width = 150;
            // 
            // colAmountDetail
            // 
            this.colAmountDetail.DataPropertyName = "amountNew";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,###,###,##0.00";
            dataGridViewCellStyle4.NullValue = null;
            this.colAmountDetail.DefaultCellStyle = dataGridViewCellStyle4;
            this.colAmountDetail.HeaderText = "จำนวนเงิน";
            this.colAmountDetail.Name = "colAmountDetail";
            this.colAmountDetail.ReadOnly = true;
            // 
            // col6
            // 
            this.col6.DataPropertyName = "debtDate";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "dd/MM/yyyy";
            dataGridViewCellStyle5.NullValue = null;
            this.col6.DefaultCellStyle = dataGridViewCellStyle5;
            this.col6.HeaderText = "วันแจ้งหนี้";
            this.col6.Name = "col6";
            this.col6.ReadOnly = true;
            // 
            // colPreviousDueDate
            // 
            this.colPreviousDueDate.DataPropertyName = "previousDueDate";
            this.colPreviousDueDate.HeaderText = "วันครบกำหนดเดิม";
            this.colPreviousDueDate.Name = "colPreviousDueDate";
            this.colPreviousDueDate.ReadOnly = true;
            this.colPreviousDueDate.Width = 150;
            // 
            // coldebtBalance
            // 
            this.coldebtBalance.DataPropertyName = "debtBalance";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,###,###,##0.00";
            dataGridViewCellStyle6.NullValue = null;
            this.coldebtBalance.DefaultCellStyle = dataGridViewCellStyle6;
            this.coldebtBalance.HeaderText = "เงินที่ชำระ";
            this.coldebtBalance.Name = "coldebtBalance";
            this.coldebtBalance.ReadOnly = true;
            this.coldebtBalance.Visible = false;
            this.coldebtBalance.Width = 130;
            // 
            // colPayment
            // 
            this.colPayment.DataPropertyName = "Payment";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "#,###,###,##0.00";
            this.colPayment.DefaultCellStyle = dataGridViewCellStyle7;
            this.colPayment.HeaderText = "เงินที่ชำระจริง";
            this.colPayment.Name = "colPayment";
            this.colPayment.ToolTipText = "ดับเบิ้ลคลิกเพื่อแก้จำนวนเงิน";
            this.colPayment.Width = 150;
            // 
            // col11
            // 
            this.col11.DataPropertyName = "debtDesc";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.Format = "#,###,###,##0.00";
            this.col11.DefaultCellStyle = dataGridViewCellStyle8;
            this.col11.HeaderText = "ประเภท";
            this.col11.Name = "col11";
            this.col11.ReadOnly = true;
            this.col11.Width = 120;
            // 
            // col5
            // 
            this.col5.DataPropertyName = "invNo";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.col5.DefaultCellStyle = dataGridViewCellStyle9;
            this.col5.HeaderText = "เลขที่เอกสาร";
            this.col5.Name = "col5";
            this.col5.ReadOnly = true;
            this.col5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.col5.Width = 140;
            // 
            // col4
            // 
            this.col4.DataPropertyName = "totalkWh";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "#,###,###,##0.0000";
            dataGridViewCellStyle10.NullValue = null;
            this.col4.DefaultCellStyle = dataGridViewCellStyle10;
            this.col4.HeaderText = "หน่วย";
            this.col4.Name = "col4";
            this.col4.ReadOnly = true;
            this.col4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.col4.Width = 60;
            // 
            // col12
            // 
            this.col12.HeaderText = "ด/บ";
            this.col12.Name = "col12";
            this.col12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.col12.Visible = false;
            this.col12.Width = 60;
            // 
            // colcountDay
            // 
            this.colcountDay.DataPropertyName = "countDay";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "#,###,###,##0";
            this.colcountDay.DefaultCellStyle = dataGridViewCellStyle11;
            this.colcountDay.HeaderText = "จำนวนวันคิดเบี้ยปรับ";
            this.colcountDay.Name = "colcountDay";
            this.colcountDay.ReadOnly = true;
            this.colcountDay.Width = 180;
            // 
            // colDefaultpenalty
            // 
            this.colDefaultpenalty.DataPropertyName = "Defaultpenalty";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "#,###,###,##0.00";
            this.colDefaultpenalty.DefaultCellStyle = dataGridViewCellStyle12;
            this.colDefaultpenalty.HeaderText = "เบี้ยปรับผิดนัด";
            this.colDefaultpenalty.Name = "colDefaultpenalty";
            this.colDefaultpenalty.ReadOnly = true;
            this.colDefaultpenalty.Width = 150;
            // 
            // colCkStatus
            // 
            this.colCkStatus.DataPropertyName = "StatusDefaultpenalty";
            this.colCkStatus.HeaderText = "ยกเลิกเบี้ยปรับ";
            this.colCkStatus.Name = "colCkStatus";
            this.colCkStatus.Width = 150;
            // 
            // colVatDetail
            // 
            this.colVatDetail.DataPropertyName = "vatBalance";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "#,###,###,##0.00";
            dataGridViewCellStyle13.NullValue = null;
            this.colVatDetail.DefaultCellStyle = dataGridViewCellStyle13;
            this.colVatDetail.HeaderText = "ภาษี";
            this.colVatDetail.Name = "colVatDetail";
            this.colVatDetail.ReadOnly = true;
            this.colVatDetail.Width = 80;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "lockBill";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn3.HeaderText = "คำอธิบาย";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // col15
            // 
            this.col15.DataPropertyName = "ftRate";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "#,###,###,##0.0000";
            this.col15.DefaultCellStyle = dataGridViewCellStyle15;
            this.col15.HeaderText = "FT";
            this.col15.Name = "col15";
            this.col15.ReadOnly = true;
            this.col15.Width = 50;
            // 
            // col26
            // 
            this.col26.DataPropertyName = "debtRemark";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.col26.DefaultCellStyle = dataGridViewCellStyle16;
            this.col26.HeaderText = "เหตุผลในการล็อคการติดตามหนี้";
            this.col26.Name = "col26";
            this.col26.ReadOnly = true;
            this.col26.Width = 250;
            // 
            // ColDebtId
            // 
            this.ColDebtId.DataPropertyName = "debtId";
            this.ColDebtId.HeaderText = "debtId";
            this.ColDebtId.Name = "ColDebtId";
            this.ColDebtId.ReadOnly = true;
            this.ColDebtId.Visible = false;
            // 
            // ColdebtIdSet
            // 
            this.ColdebtIdSet.DataPropertyName = "distIdNew";
            this.ColdebtIdSet.HeaderText = "debtIdSet";
            this.ColdebtIdSet.Name = "ColdebtIdSet";
            this.ColdebtIdSet.ReadOnly = true;
            this.ColdebtIdSet.Visible = false;
            // 
            // ColdebtType
            // 
            this.ColdebtType.DataPropertyName = "debtType";
            this.ColdebtType.HeaderText = "debtType";
            this.ColdebtType.Name = "ColdebtType";
            this.ColdebtType.Visible = false;
            // 
            // colNewDuedate
            // 
            this.colNewDuedate.DataPropertyName = "invNewDueDate";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colNewDuedate.DefaultCellStyle = dataGridViewCellStyle17;
            this.colNewDuedate.HeaderText = "วันครบกำหนดผัดชำระ";
            this.colNewDuedate.Name = "colNewDuedate";
            this.colNewDuedate.ReadOnly = true;
            this.colNewDuedate.Visible = false;
            this.colNewDuedate.Width = 170;
            // 
            // colDebtLock
            // 
            this.colDebtLock.DataPropertyName = "debtLock";
            this.colDebtLock.HeaderText = "debtLock";
            this.colDebtLock.Name = "colDebtLock";
            this.colDebtLock.Visible = false;
            // 
            // colInsDebtInsHdrId
            // 
            this.colInsDebtInsHdrId.DataPropertyName = "insDebtInsHdrId";
            this.colInsDebtInsHdrId.HeaderText = "insDebtInsHdrId";
            this.colInsDebtInsHdrId.Name = "colInsDebtInsHdrId";
            this.colInsDebtInsHdrId.Visible = false;
            // 
            // colInstallmentsFlag
            // 
            this.colInstallmentsFlag.DataPropertyName = "installmentsFlag";
            this.colInstallmentsFlag.HeaderText = "installmentsFlag";
            this.colInstallmentsFlag.Name = "colInstallmentsFlag";
            this.colInstallmentsFlag.Visible = false;
            // 
            // FormArrears
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.panelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormArrears";
            this.Text = "FormArrearsNew4";
            this.Load += new System.EventHandler(this.FormArrearsNew_Load);
            this.panelMain.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.GroupBoxDetail.ResumeLayout(false);
            this.pnProgressBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptMascot)).EndInit();
            this.PanelCancelWait.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Panel4.ResumeLayout(false);
            this.Panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.PanelEmployee.ResumeLayout(false);
            this.groupBoxEmployee.ResumeLayout(false);
            this.groupBoxEmployee.PerformLayout();
            this.PanelHead.ResumeLayout(false);
            this.PanelNoneQ.ResumeLayout(false);
            this.PanelNoneQ.PerformLayout();
            this.PanelHaveQ.ResumeLayout(false);
            this.PanelHaveQ.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel PanelHead;
        private System.Windows.Forms.Panel PanelNoneQ;
        private Custom_Controls_in_CS.ButtonZ ButtonCallQNone;
        private System.Windows.Forms.ComboBox ComboBoxDocType;
        private System.Windows.Forms.TextBox TextBoxBarcode;
        private System.Windows.Forms.Panel PanelHaveQ;
        private Custom_Controls_in_CS.ButtonZ ButtonCallQ;
        private Custom_Controls_in_CS.ButtonZ ButtonRefreshQ;
        private Custom_Controls_in_CS.ButtonZ ButtonCancelQ;
        private Custom_Controls_in_CS.ButtonZ ButtonResetQ;
        private Custom_Controls_in_CS.ButtonZ ButtonAdd;
        internal System.Windows.Forms.Label LabelNoQ;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel PanelEmployee;
        private System.Windows.Forms.GroupBox groupBoxEmployee;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label LabelStationNo;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label LabelEmployeeName;
        internal System.Windows.Forms.Label LabelEmployeeCode;
        internal System.Windows.Forms.Label LabelFkName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox GroupBoxDetail;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Custom_Controls_in_CS.ButtonZ ButtonCancelWait;
        internal System.Windows.Forms.Label Label12;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox CheckBoxSelectAll;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label32;
        internal System.Windows.Forms.Label Label29;
        //internal System.Windows.Forms.Label lblCountDetail;
        internal System.Windows.Forms.Label Label31;
        internal System.Windows.Forms.Label lblAmountPayment;
        internal System.Windows.Forms.Label Label39;
        internal System.Windows.Forms.Label Label38;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label txtFine;
        internal System.Windows.Forms.Label txtTotalSum;
        internal System.Windows.Forms.Label Label36;
        internal System.Windows.Forms.Label txtTotal;
        internal System.Windows.Forms.Label Label35;
        internal System.Windows.Forms.Label txtAmount;
        internal System.Windows.Forms.Label Label34;
        internal System.Windows.Forms.Label Label33;
        internal System.Windows.Forms.Panel panel3;
        private Custom_Controls_in_CS.ButtonZ ButtonRec;
        private Custom_Controls_in_CS.ButtonZ ButtonAdvance;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private Models.ClientSocket clientSocket2;
        private Models.ClientSocket clientSocket1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private Custom_Controls_in_CS.ButtonZ ButtonDelete;
        private Custom_Controls_in_CS.ButtonZ ButtonClear;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private System.Windows.Forms.TextBox TextBoxSearch;
        private System.Windows.Forms.DataGridView GridDetail;
        internal System.Windows.Forms.Label labelCa;
        internal System.Windows.Forms.Label labelUi;
        internal System.Windows.Forms.Label labelAddress;
        internal System.Windows.Forms.Label labelFk;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label9;
        private System.Windows.Forms.DataGridView DataGridViewDetail;
        internal System.Windows.Forms.Label labelTaxId;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label labelBill;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label labelBranch;
        internal System.Windows.Forms.Label label10;
        private Custom_Controls_in_CS.ButtonZ buttonZ1;
        private System.Windows.Forms.Label LabelTmp_Time;
        internal System.Windows.Forms.Label lblStatusC;
        internal System.Windows.Forms.Label lblStatusD;
        private System.Windows.Forms.CheckBox CheckBoxSelect;
        private System.Windows.Forms.TextBox txtCount;
        private System.Windows.Forms.ComboBox comboBoxDetail;
        private Custom_Controls_in_CS.ButtonZ btOK;
        internal System.Windows.Forms.Label lblAmountDetail;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label lblCaDetail;
        internal System.Windows.Forms.Label Label30;
        internal System.Windows.Forms.Label Label37;
        internal System.Windows.Forms.Label lblReceiptName;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label txtVat;
        private System.Windows.Forms.Panel PanelCancelWait;
        private System.Windows.Forms.Timer timer3;
        private Custom_Controls_in_CS.ButtonZ btCancelFine;
        internal System.Windows.Forms.Label lblCountListDetail;
        internal System.Windows.Forms.Label label18;
        private Panel pnProgressBar;
        private PictureBox ptMascot;
        private CircularProgressBar.CircularProgressBar ProgressBar;
        private DataGridViewCheckBoxColumn ColCheckBox;
        private DataGridViewTextBoxColumn ColCA;
        private DataGridViewTextBoxColumn colInv;
        private DataGridViewTextBoxColumn ColUI;
        private DataGridViewTextBoxColumn ColCustomerName;
        private DataGridViewTextBoxColumn ColAddress;
        private DataGridViewTextBoxColumn ColAmount;
        private DataGridViewCheckBoxColumn ColckCancel;
        private DataGridViewTextBoxColumn ColTotalAmount;
        private DataGridViewTextBoxColumn ColSumTotalAmount;
        private DataGridViewTextBoxColumn ColPenaltyAll;
        private DataGridViewTextBoxColumn ColVat;
        private DataGridViewTextBoxColumn ColType;
        private DataGridViewTextBoxColumn ColCusName;
        private DataGridViewTextBoxColumn ColCusTaxId;
        private DataGridViewTextBoxColumn ColCusTaxBranch;
        private DataGridViewTextBoxColumn ColCusAddress;
        private DataGridViewTextBoxColumn ColCustomerId;
        private DataGridViewTextBoxColumn collId;
        private DataGridViewTextBoxColumn ColTax20;
        private DataGridViewTextBoxColumn ColTaxBranch;
        private DataGridViewTextBoxColumn colStatus;
        internal Panel panel7;
        internal Label label1;
        internal Label lblCountCa;
        internal Label lblCountCreditNote;
        internal Label label24;
        internal Label label25;
        internal Label label28;
        //internal Label lblCountDetail;
        internal Label label41;
        internal Label lblCountList;
        internal Label lblCountcapayment;
        private DataGridViewCheckBoxColumn ColCheckBoxDetail;
        private DataGridViewTextBoxColumn colCADetail;
        private DataGridViewTextBoxColumn col3;
        private DataGridViewTextBoxColumn colReqNo;
        private DataGridViewTextBoxColumn col7;
        private DataGridViewTextBoxColumn colAmountDetail;
        private DataGridViewTextBoxColumn col6;
        private DataGridViewTextBoxColumn colPreviousDueDate;
        private DataGridViewTextBoxColumn coldebtBalance;
        private DataGridViewTextBoxColumn colPayment;
        private DataGridViewTextBoxColumn col11;
        private DataGridViewTextBoxColumn col5;
        private DataGridViewTextBoxColumn col4;
        private DataGridViewCheckBoxColumn col12;
        private DataGridViewTextBoxColumn colcountDay;
        private DataGridViewTextBoxColumn colDefaultpenalty;
        private DataGridViewCheckBoxColumn colCkStatus;
        private DataGridViewTextBoxColumn colVatDetail;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn col15;
        private DataGridViewTextBoxColumn col26;
        private DataGridViewTextBoxColumn ColDebtId;
        private DataGridViewTextBoxColumn ColdebtIdSet;
        private DataGridViewTextBoxColumn ColdebtType;
        private DataGridViewTextBoxColumn colNewDuedate;
        private DataGridViewTextBoxColumn colDebtLock;
        private DataGridViewTextBoxColumn colInsDebtInsHdrId;
        private DataGridViewTextBoxColumn colInstallmentsFlag;
    }
}