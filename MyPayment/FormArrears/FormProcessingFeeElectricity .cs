﻿using GinkoSolutions;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormProcessingFeeElectricity : Form
    {
        ClassLogsService meaLog = new ClassLogsService("Cancel", "DataLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        public Int64[] _debtId;
        public string _ca;
        public string _status;
        public FormProcessingFeeElectricity(Int64[] debtId, string ca, string status)
        {
            _debtId = debtId;
            _ca = ca;
            _status = status;
            InitializeComponent();
        }
        private void ButtonNo_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (_status != "")
                {
                    List<ClassCancelContinue> _lstCancel = new List<ClassCancelContinue>();
                    if (GlobalClass.Cancel == null)
                        GlobalClass.Cancel = new List<ClassCancelContinue>();
                    ClassCancelContinue Cancel = new ClassCancelContinue();
                    Cancel.debtId = _debtId[0].ToString();
                    Cancel.userId = GlobalClass.UserId;
                    Cancel.authorizedCancelBy = GlobalClass.UserName;
                    Cancel.ca = _ca;
                    _lstCancel.Add(Cancel);
                    GlobalClass.Cancel.Add(Cancel);
                }
                else
                    MsgBox.Show(this, "ตัดไฟเกิน 6 เดือน กรุณาติดต่อ บร.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);               
                this.Close();
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
              
            }
        }
        private void ButtonYes_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (_status != "")
                {
                    List<ClassConnectMeter> _lstCancel = new List<ClassConnectMeter>();
                    if (GlobalClass.ConnectMeter == null)
                        GlobalClass.ConnectMeter = new List<ClassConnectMeter>();
                    ClassConnectMeter Cancel = new ClassConnectMeter();
                    Cancel.userId = GlobalClass.UserId.ToString();
                    Cancel.ca = Convert.ToInt64(_ca);
                    _lstCancel.Add(Cancel);
                    GlobalClass.ConnectMeter.Add(Cancel);
                }
                else
                    MsgBox.Show(this, "ตัดไฟเกิน 6 เดือน กรุณาติดต่อ บร.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);              
                this.Close();
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
