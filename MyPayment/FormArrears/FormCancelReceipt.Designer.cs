﻿namespace MyPayment.FormArrears
{
    partial class FormCancelReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCancelReceipt));
            this.TextboxPaymentNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TextBoxReceiptNo = new System.Windows.Forms.TextBox();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CheckBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.DataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.ColCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColPaymentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColReceiptNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCusName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCusId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ButtonOk = new Custom_Controls_in_CS.ButtonZ();
            this.pnProgressBar = new System.Windows.Forms.Panel();
            this.ptMascot = new System.Windows.Forms.PictureBox();
            this.ProgressBar = new CircularProgressBar.CircularProgressBar();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).BeginInit();
            this.panel2.SuspendLayout();
            this.pnProgressBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptMascot)).BeginInit();
            this.SuspendLayout();
            // 
            // TextboxPaymentNo
            // 
            this.TextboxPaymentNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.TextboxPaymentNo.Location = new System.Drawing.Point(11, 40);
            this.TextboxPaymentNo.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxPaymentNo.Name = "TextboxPaymentNo";
            this.TextboxPaymentNo.Size = new System.Drawing.Size(330, 31);
            this.TextboxPaymentNo.TabIndex = 0;
            this.TextboxPaymentNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxPaymentNo_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 28);
            this.label1.TabIndex = 3;
            this.label1.Text = "Payment Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(363, 16);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 28);
            this.label2.TabIndex = 4;
            this.label2.Text = "เลขที่ใบเสร็จ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TextBoxReceiptNo);
            this.groupBox1.Controls.Add(this.ButtonSearch);
            this.groupBox1.Controls.Add(this.TextboxPaymentNo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1012, 86);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // TextBoxReceiptNo
            // 
            this.TextBoxReceiptNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.TextBoxReceiptNo.Location = new System.Drawing.Point(363, 40);
            this.TextBoxReceiptNo.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxReceiptNo.Name = "TextBoxReceiptNo";
            this.TextBoxReceiptNo.Size = new System.Drawing.Size(330, 31);
            this.TextBoxReceiptNo.TabIndex = 1;
            this.TextBoxReceiptNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxReceiptNo_KeyPress);
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "ค้นหา";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = global::MyPayment.Properties.Resources.zoom;
            this.ButtonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSearch.Location = new System.Drawing.Point(722, 35);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(127, 40);
            this.ButtonSearch.StartColor = System.Drawing.Color.LightGray;
            this.ButtonSearch.TabIndex = 2;
            this.ButtonSearch.TextLocation_X = 45;
            this.ButtonSearch.TextLocation_Y = 5;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CheckBoxSelectAll);
            this.panel1.Controls.Add(this.DataGridViewDetail);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 86);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 564);
            this.panel1.TabIndex = 14;
            // 
            // CheckBoxSelectAll
            // 
            this.CheckBoxSelectAll.AutoSize = true;
            this.CheckBoxSelectAll.Location = new System.Drawing.Point(8, 11);
            this.CheckBoxSelectAll.Name = "CheckBoxSelectAll";
            this.CheckBoxSelectAll.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelectAll.TabIndex = 3;
            this.CheckBoxSelectAll.UseVisualStyleBackColor = true;
            this.CheckBoxSelectAll.Click += new System.EventHandler(this.CheckBoxSelectAll_Click);
            // 
            // DataGridViewDetail
            // 
            this.DataGridViewDetail.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.DataGridViewDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewDetail.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCheckBox,
            this.ColPaymentId,
            this.ColReceiptNo,
            this.ColCusName,
            this.ColCusId,
            this.ColCa,
            this.ColUi,
            this.ColAmount});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.DefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewDetail.Location = new System.Drawing.Point(0, 0);
            this.DataGridViewDetail.Name = "DataGridViewDetail";
            this.DataGridViewDetail.RowHeadersVisible = false;
            this.DataGridViewDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DataGridViewDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewDetail.Size = new System.Drawing.Size(1012, 564);
            this.DataGridViewDetail.TabIndex = 0;
            this.DataGridViewDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewDetail_CellContentClick);
            // 
            // ColCheckBox
            // 
            this.ColCheckBox.DataPropertyName = "SelectCheck";
            this.ColCheckBox.Frozen = true;
            this.ColCheckBox.HeaderText = "";
            this.ColCheckBox.Name = "ColCheckBox";
            this.ColCheckBox.Width = 30;
            // 
            // ColPaymentId
            // 
            this.ColPaymentId.DataPropertyName = "paymentNo";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColPaymentId.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColPaymentId.HeaderText = "เลขที่รายการรับชำระ";
            this.ColPaymentId.Name = "ColPaymentId";
            this.ColPaymentId.Width = 170;
            // 
            // ColReceiptNo
            // 
            this.ColReceiptNo.DataPropertyName = "receiptNo";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColReceiptNo.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColReceiptNo.HeaderText = "เลขที่ใบเสร็จ";
            this.ColReceiptNo.Name = "ColReceiptNo";
            this.ColReceiptNo.Width = 200;
            // 
            // ColCusName
            // 
            this.ColCusName.DataPropertyName = "payerName";
            this.ColCusName.HeaderText = "ชื่อลูกค้า";
            this.ColCusName.Name = "ColCusName";
            this.ColCusName.Width = 190;
            // 
            // ColCusId
            // 
            this.ColCusId.DataPropertyName = "custId";
            this.ColCusId.HeaderText = "รหัสลูกค้า";
            this.ColCusId.Name = "ColCusId";
            this.ColCusId.Width = 220;
            // 
            // ColCa
            // 
            this.ColCa.DataPropertyName = "ca";
            this.ColCa.HeaderText = "บัญชีแสดงสัญญา";
            this.ColCa.Name = "ColCa";
            this.ColCa.Width = 170;
            // 
            // ColUi
            // 
            this.ColUi.DataPropertyName = "ui";
            this.ColUi.HeaderText = "รหัสเครื่องวัดฯ";
            this.ColUi.Name = "ColUi";
            this.ColUi.Width = 150;
            // 
            // ColAmount
            // 
            this.ColAmount.DataPropertyName = "totalReceipt";
            this.ColAmount.HeaderText = "จำนวนเงินที่ชำระ";
            this.ColAmount.Name = "ColAmount";
            this.ColAmount.Width = 190;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ButtonOk);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 650);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1012, 55);
            this.panel2.TabIndex = 1;
            // 
            // ButtonOk
            // 
            this.ButtonOk.BackColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderWidth = 1;
            this.ButtonOk.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonOk.ButtonText = "ตกลง";
            this.ButtonOk.CausesValidation = false;
            this.ButtonOk.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonOk.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonOk.ForeColor = System.Drawing.Color.Black;
            this.ButtonOk.GradientAngle = 90;
            this.ButtonOk.Image = global::MyPayment.Properties.Resources.ok;
            this.ButtonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonOk.Location = new System.Drawing.Point(882, 6);
            this.ButtonOk.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonOk.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonOk.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonOk.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonOk.Name = "ButtonOk";
            this.ButtonOk.ShowButtontext = true;
            this.ButtonOk.Size = new System.Drawing.Size(127, 40);
            this.ButtonOk.StartColor = System.Drawing.Color.LightGray;
            this.ButtonOk.TabIndex = 0;
            this.ButtonOk.TextLocation_X = 45;
            this.ButtonOk.TextLocation_Y = 5;
            this.ButtonOk.Transparent1 = 80;
            this.ButtonOk.Transparent2 = 120;
            this.ButtonOk.UseVisualStyleBackColor = true;
            this.ButtonOk.Click += new System.EventHandler(this.ButtonOk_Click);
            // 
            // pnProgressBar
            // 
            this.pnProgressBar.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnProgressBar.Controls.Add(this.ptMascot);
            this.pnProgressBar.Controls.Add(this.ProgressBar);
            this.pnProgressBar.Enabled = false;
            this.pnProgressBar.Location = new System.Drawing.Point(985, 0);
            this.pnProgressBar.Name = "pnProgressBar";
            this.pnProgressBar.Size = new System.Drawing.Size(25, 21);
            this.pnProgressBar.TabIndex = 5;
            this.pnProgressBar.Visible = false;
            // 
            // ptMascot
            // 
            this.ptMascot.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ptMascot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ptMascot.Image = global::MyPayment.Properties.Resources._14_02;
            this.ptMascot.Location = new System.Drawing.Point(65, 52);
            this.ptMascot.Name = "ptMascot";
            this.ptMascot.Size = new System.Drawing.Size(200, 185);
            this.ptMascot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptMascot.TabIndex = 135;
            this.ptMascot.TabStop = false;
            // 
            // ProgressBar
            // 
            this.ProgressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.ProgressBar.AnimationSpeed = 500;
            this.ProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.ProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold);
            this.ProgressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ProgressBar.InnerColor = System.Drawing.SystemColors.ScrollBar;
            this.ProgressBar.InnerMargin = 2;
            this.ProgressBar.InnerWidth = -1;
            this.ProgressBar.Location = new System.Drawing.Point(0, 0);
            this.ProgressBar.MarqueeAnimationSpeed = 2000;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.OuterColor = System.Drawing.Color.Gray;
            this.ProgressBar.OuterMargin = -25;
            this.ProgressBar.OuterWidth = 26;
            this.ProgressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ProgressBar.ProgressWidth = 10;
            this.ProgressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.ProgressBar.Size = new System.Drawing.Size(25, 21);
            this.ProgressBar.StartAngle = 270;
            this.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.ProgressBar.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.ProgressBar.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.ProgressBar.SubscriptText = ".23";
            this.ProgressBar.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.ProgressBar.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.ProgressBar.SuperscriptText = "°C";
            this.ProgressBar.TabIndex = 132;
            this.ProgressBar.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.ProgressBar.Value = 68;
            // 
            // FormCancelReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.pnProgressBar);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCancelReceipt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ยกเลิกใบเสร็จ";
            this.Load += new System.EventHandler(this.FormCancelReceipt_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).EndInit();
            this.panel2.ResumeLayout(false);
            this.pnProgressBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptMascot)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TextboxPaymentNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private Custom_Controls_in_CS.ButtonZ ButtonOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView DataGridViewDetail;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private System.Windows.Forms.TextBox TextBoxReceiptNo;
        private System.Windows.Forms.CheckBox CheckBoxSelectAll;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPaymentId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColReceiptNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCusName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCusId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUi;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAmount;
        private System.Windows.Forms.Panel pnProgressBar;
        private System.Windows.Forms.PictureBox ptMascot;
        private CircularProgressBar.CircularProgressBar ProgressBar;
    }
}