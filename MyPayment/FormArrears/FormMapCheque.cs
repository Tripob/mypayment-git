﻿using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormMapCheque : Form
    {
        public CultureInfo usCulture = new CultureInfo("en-US");
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        Control cnt;
        List<inquiryGroupDebtBeanList> _Itemlst = new List<inquiryGroupDebtBeanList>();
        public FormMapCheque()
        {
            InitializeComponent();
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        public void SetControl()
        {
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                bool status = false;
                int statusCheck = 0;
                int i = 0;
                var loopTo = GridDetail.Rows.Count - 1;
                List<ClassPaymentCheque> _payment = new List<ClassPaymentCheque>();
                if (GlobalClass.LstSub == null)
                    GlobalClass.LstSub = new List<ClassPaymentCheque>();

                for (i = 0; i <= loopTo; i++)
                {
                    DataGridViewRow rownew = GridDetail.Rows[i];
                    string ChequeNo = labelNumber.Text.Trim();
                    string debtId = rownew.Cells["ColdebtId"].EditedFormattedValue.ToString();
                    //decimal amount =  Convert.ToDecimal(rownew.Cells["ColAmount"].EditedFormattedValue.ToString());
                    decimal payed = Convert.ToDecimal(rownew.Cells["ColPayed"].EditedFormattedValue.ToString());
                    decimal balance = Convert.ToDecimal(rownew.Cells["colTotalAmount"].EditedFormattedValue.ToString());
                    string ca = rownew.Cells["ColCa"].EditedFormattedValue.ToString();
                    decimal Sumamount = Convert.ToDecimal(rownew.Cells["ColBalance"].EditedFormattedValue.ToString());
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckbox"];
                    bool isChecked = (bool)checkCell.EditedFormattedValue;
                    inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == ca).FirstOrDefault();
                    inquiryGroupDebtBeanList costTDebt = info.inquiryGroupDebtBeanList.Find(item => item.debtId.Equals(debtId, StringComparison.CurrentCultureIgnoreCase));
                    ClassPaymentCheque subPayment = new ClassPaymentCheque();
                    if (isChecked)
                    {
                        statusCheck = 1;
                        decimal Payed = 0;
                        subPayment = GlobalClass.LstSub.Find(item => item.DebtId.Equals(debtId, StringComparison.CurrentCultureIgnoreCase) && item.ChequeNo.Equals(ChequeNo, StringComparison.CurrentCultureIgnoreCase));
                        if (subPayment != null)
                        {
                            subPayment.ChequeNo = labelNumber.Text;
                            Payed = Convert.ToDecimal(subPayment.Payed);
                            decimal sumAmount = Convert.ToDecimal(payed) - Convert.ToDecimal(subPayment.Payed);
                            subPayment.AmountBalance = subPayment.AmountBalance - Math.Abs(sumAmount);
                            subPayment.StatusCheque = true;
                            subPayment.Payed = Convert.ToDecimal(payed);
                            subPayment.Amount = Convert.ToDecimal(balance);
                        }
                        else
                        {
                            ClassPaymentCheque payment = new ClassPaymentCheque();
                            payment.Ca = ca;
                            payment.DebtId = debtId;
                            payment.ChequeNo = labelNumber.Text;
                            payment.AmountBalance = Convert.ToDecimal(balance) - Convert.ToDecimal(payed);
                            payment.Amount = Convert.ToDecimal(balance);
                            payment.Payed = Convert.ToDecimal(payed);
                            payment.StatusCheque = true;
                            _payment.Add(payment);
                            GlobalClass.LstSub.Add(payment);
                        }
                        inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(item => item.ca.Equals(ca, StringComparison.CurrentCultureIgnoreCase));
                        if (userInfo != null)
                            userInfo.StatusOrderBy = true;
                        if (costTDebt != null)
                            costTDebt.StatusOrderBy = true;
                        if (costTDebt != null)
                        {
                            if (payed != 0)
                            {
                                costTDebt.Payed = payed;//decimal.Parse(debtId);
                                costTDebt.ChequeNo = labelNumber.Text;
                                if (payed == balance)
                                    costTDebt.StatusCheq = true;
                                else
                                    costTDebt.StatusCheq = false;

                                costTDebt.StatusSelectCheq = true;
                                userInfo.SelectCheck = true;
                                status = true;
                            }
                            decimal Sumsmount = 0;
                            if (costTDebt.AmountBalance == "0")
                                Sumsmount = Convert.ToDecimal(balance) - Convert.ToDecimal(payed);
                            else
                            {
                                decimal sumAmount = 0;
                                if (subPayment != null)
                                    sumAmount = Convert.ToDecimal(payed) - Convert.ToDecimal(Payed);
                                else
                                    sumAmount = Convert.ToDecimal(payed);
                                Sumsmount = Convert.ToDecimal(costTDebt.AmountBalance) - Math.Abs(sumAmount);
                            }

                            costTDebt.AmountBalance = Math.Abs(Sumsmount).ToString("#,###,###,##0.00");
                        }
                    }
                    else
                    {
                        if (GlobalClass.LstSub.Count > 0 && statusCheck == 0)
                        {
                            decimal countPayed = costTDebt.Payed - payed;
                            costTDebt.StatusOrderBy = false;
                            costTDebt.StatusCheq = false;
                            costTDebt.StatusSelectCheq = false;
                            costTDebt.AmountBalance = Math.Abs(Sumamount + payed).ToString("#,###,###,##0.00");
                            costTDebt.Payed = countPayed < 0 ? 0 : countPayed;//decimal.Parse(debtId);
                            costTDebt.ChequeNo = null;
                            subPayment = GlobalClass.LstSub.Find(item => item.DebtId.Equals(debtId, StringComparison.CurrentCultureIgnoreCase) && item.ChequeNo.Equals(ChequeNo, StringComparison.CurrentCultureIgnoreCase));
                            subPayment.StatusCheque = false;
                            subPayment.Payed = countPayed;
                            List<ClassPaymentCheque> resulDelete = GlobalClass.LstSub.Where(c => c.Payed == 0).ToList();
                            foreach (var itemDelete in resulDelete)
                            {
                                GlobalClass.LstSub.Remove(itemDelete);
                            }
                            if (GlobalClass.LstSub.Count == 0)
                                GlobalClass.LstSub = null;
                        }
                    }
                }
                ClassCheque cheque = GlobalClass.LstCheque.Find(item => item.Chno.Equals(labelNumber.Text.ToString(), StringComparison.CurrentCultureIgnoreCase));
                if (cheque != null)
                {
                    cheque.Balance = Convert.ToDecimal(labelBalance.Text);
                    cheque.Status = status;
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormMapCheque | Function :: ButtonConfirm()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
            this.Close();
        }
        public void LoadData()
        {
            List<inquiryGroupDebtBeanList> lst = GlobalClass.LstCost.Where(c => c.status == true).OrderBy(g => g.ca).ToList();
            List<inquiryGroupDebtBeanList> GetCheqeNo = lst.Where(c => c.ChequeNo == labelNumber.Text).OrderBy(g => g.ca).ToList();
            if (GetCheqeNo.Count > 0)
                lst = lst.Where(c => c.ChequeNo == labelNumber.Text || c.StatusCheq == false).OrderBy(g => g.ca).ToList();
            else
                lst = lst.Where(c => c.StatusCheq == false && c.AmountBalance != "0.00").OrderBy(g => g.ca).ToList();
            List<inquiryGroupDebtBeanList> _lst = new List<inquiryGroupDebtBeanList>();
            decimal sumPayed = 0;
            foreach (var item in lst)
            {
                inquiryGroupDebtBeanList cost = new inquiryGroupDebtBeanList();
                cost.status = item.StatusSelectCheq;
                cost.debtId = item.debtId;
                cost.ca = item.ca;
                cost.ui = item.ui;
                cost.reqNo = item.reqNo;
                cost.debtType = item.debtType;
                cost.debtBalance = Math.Round(item.debtBalance, 2);
                cost.Defaultpenalty = item.Defaultpenalty;
                cost.totalAmount = Math.Round(item.debtBalance + item.Defaultpenalty, 2).ToString();
                cost.Payed = item.debtBalance;
                cost.PayedNew = item.debtBalance;
                cost.AmountBalance = item.debtBalance.ToString("#,###,###,##0.00");
                if (GlobalClass.LstSub != null)
                {
                    List<ClassPaymentCheque> _lstSub = GlobalClass.LstSub.Where(c => c.DebtId == item.debtId).ToList();
                    if (_lstSub.Count != 0)
                    {
                        ClassPaymentCheque lstSub = _lstSub.Where(c => c.ChequeNo == labelNumber.Text).FirstOrDefault();
                        cost.AmountBalance = Convert.ToDouble(item.AmountBalance).ToString("#,###,###,##0.00");
                        cost.Payed = Convert.ToDecimal(item.AmountBalance);
                        cost.PayedNew = Convert.ToDecimal(item.AmountBalance);
                        if (lstSub != null)
                        {
                            cost.status = item.StatusSelectCheq;
                            cost.Payed = lstSub.Payed;
                            sumPayed += lstSub.Payed;
                            cost.PayedNew = lstSub.Payed;
                            //cost.AmountBalance = Convert.ToDouble(lstSub.AmountBalance).ToString("#,###,###,##0.00");
                        }
                        else
                            cost.status = false;
                    }
                }
                _lst.Add(cost);
            }
            sumPayed = Convert.ToDecimal(labelAmount.Text) - sumPayed;
            labelBalance.Text = sumPayed.ToString("#,###,###,##0.00");
            GridDetail.DataSource = _lst;
            int i = 0;
            foreach (var item in _lst)
            {
                if (item.status == true)
                    GridDetail.Rows[i].Cells[9].ReadOnly = false;
                else
                    GridDetail.Rows[i].Cells[9].ReadOnly = true;
                i++;
            }
            _Itemlst = _lst;
        }
        private void FormMapCheque_Load(object sender, EventArgs e)
        {
            SetControl();
            LoadData();
        }
        private void GridDetail_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.TextChanged += new EventHandler(tb_TextChanged);
            cnt = e.Control;
            cnt.TextChanged += tb_TextChanged;
        }
        void tb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (cnt.Text != string.Empty)
                {
                    var isNumeric = decimal.TryParse(cnt.Text, out decimal n);
                    if (isNumeric)
                    {
                        int i = 0;
                        decimal balance = 0;
                        var loopTo = GridDetail.Rows.Count - 1;
                        for (i = 0; i <= loopTo; i++)
                        {
                            DataGridViewRow rownew = GridDetail.Rows[i];
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBox"];
                            bool isChecked = (bool)checkCell.EditedFormattedValue;
                            if (isChecked)
                            {
                                if (Convert.ToDouble(rownew.Cells["ColPayed"].EditedFormattedValue.ToString()) > Convert.ToDouble(rownew.Cells["colTotalAmount"].EditedFormattedValue.ToString()) &&
                                    Convert.ToDouble(rownew.Cells["ColAmount"].EditedFormattedValue.ToString()) != Convert.ToDouble(rownew.Cells["ColPayed"].EditedFormattedValue.ToString()))
                                {
                                    MessageBox.Show("ยอดเงินเกินจำนวน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    cnt.Text = string.Empty;
                                    cnt.Focus();
                                    return;
                                }
                                if (rownew.Cells["ColPayed"].EditedFormattedValue.ToString() != "")
                                    balance += Convert.ToDecimal(rownew.Cells["ColPayed"].EditedFormattedValue);
                            }
                        }
                        decimal CountAmount = Convert.ToDecimal(labelAmount.Text) - balance;
                        if (CountAmount < 0)
                        {
                            MessageBox.Show("ยอดเงินเกินจำนวนยอดเงินคงเหลือ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            cnt.Text = string.Empty;
                            cnt.Focus();
                            return;
                        }
                        else
                            labelBalance.Text = CountAmount.ToString("#,###,###,##0.00");
                    }
                    else
                        return;
                }
                else
                {
                    DataGridViewCell cell = GridDetail.CurrentCell;
                    decimal balance = Convert.ToDecimal(GridDetail.Rows[cell.RowIndex].Cells["colTotalAmount"].Value);
                    decimal amount = Convert.ToDecimal(GridDetail.Rows[cell.RowIndex].Cells["ColAmount"].Value);
                    decimal payed = Convert.ToDecimal(GridDetail.Rows[cell.RowIndex].Cells["colPayedNew"].Value);
                    if (balance != payed)
                    {
                        decimal sumamount = balance + payed;
                        GridDetail.Rows[cell.RowIndex].Cells["colTotalAmount"].Value = sumamount;
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        private void GridDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                decimal CountAmount = 0;
                DataGridViewCheckBoxCell ckCell = (DataGridViewCheckBoxCell)GridDetail.Rows[e.RowIndex].Cells["ColCheckBox"];
                decimal balance = Convert.ToDecimal(GridDetail.Rows[e.RowIndex].Cells["ColPayed"].EditedFormattedValue.ToString());
                string debtId = GridDetail.Rows[e.RowIndex].Cells["ColdebtId"].EditedFormattedValue.ToString();
                string ca = GridDetail.Rows[e.RowIndex].Cells["ColCa"].EditedFormattedValue.ToString();
                inquiryGroupDebtBeanList Itemlst = _Itemlst.Find(item => item.debtId.Equals(debtId, StringComparison.CurrentCultureIgnoreCase));
                bool newBool = (bool)ckCell.EditedFormattedValue;
                bool status = false;
                if (newBool)
                {
                    if (balance > Convert.ToDecimal(labelBalance.Text))
                        GridDetail.Rows[e.RowIndex].Cells[9].Value = Convert.ToDecimal(labelBalance.Text);
                    CountAmount = Convert.ToDecimal(labelBalance.Text) - balance;
                    GridDetail.Rows[e.RowIndex].Cells[9].ReadOnly = false;
                    //decimal sumamount = Convert.ToDecimal(GridDetail.Rows[e.RowIndex].Cells[6].Value) - Convert.ToDecimal(GridDetail.Rows[e.RowIndex].Cells[8].Value);
                    //GridDetail.Rows[e.RowIndex].Cells[7].Value = sumamount.ToString();
                    status = true;
                }
                else
                {
                    status = false;
                    CountAmount = Convert.ToDecimal(labelAmount.Text) - balance;
                    GridDetail.Rows[e.RowIndex].Cells[9].ReadOnly = true;
                    //GridDetail.Rows[e.RowIndex].Cells[7].Value = GridDetail.Rows[e.RowIndex].Cells[6].Value;                   
                }
                if (Itemlst != null)
                    Itemlst.status = status;
                decimal amount = _Itemlst.Where(c => c.status == true).Sum(c => c.Payed);
                amount = Convert.ToDecimal(labelAmount.Text) - amount;
                labelBalance.Text = amount.ToString("#,###,###,##0.00");
                GridDetail.DataSource = _Itemlst;

                //int i = 0;
                //var loopTo = GridDetail.Rows.Count - 1;
                //for (i = 0; i <= loopTo; i++)
                //{
                //    if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                //    {
                //        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBox"];
                //        DataGridViewTextBoxCell txtAmount = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["ColPayed"];
                //        bool isChecked = (bool)checkCell.EditedFormattedValue;
                //        if (isChecked)
                //        {
                //            if (GridDetail.Rows[i].Cells["ColPayed"].EditedFormattedValue.ToString() != "")
                //                balance += Convert.ToDouble(GridDetail.Rows[i].Cells["ColPayed"].EditedFormattedValue);

                //            double CountAmount = Convert.ToDouble(labelAmount.Text) - balance;
                //            labelBalance.Text = (CountAmount).ToString("#,###,###,##0.00");
                //        }
                //        else
                //        {

                //        }
                //    }
                //}
            }
        }
        private void GridDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (GridDetail.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == DBNull.Value)
                e.Cancel = true;
        }
    }
}
