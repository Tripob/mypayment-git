﻿using GinkoSolutions;
using MyPayment.Controllers;
using MyPayment.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormArrears : Form
    {
        System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
        #region Global Variable
        GeneralService genService = new GeneralService(true);
        public bool q_count = false;
        ClassLogsService LogQueue = new ClassLogsService("Queue", "DataLog");
        ClassLogsService LogQueueError = new ClassLogsService("Queue", "ErrorLog");
        public string strUserCode = "1506013";
        public string oldSendMessage;
        public string strIPAddress = GlobalClass.IpConfig;//ConfigurationManager.AppSettings["IPAddress"].ToString();
        public bool isRefreshData;
        public string retMessage;
        public ArrayList arrData;
        public string oldData;
        public int printErrorAmount;
        public int SleepTime;
        public int TotalPrint;
        public string QNo = "000";
        public bool _status = false;
        public bool StatusPanel;
        public int BarcodeCount = 0;
        public bool status = false;
        public bool _statusCkFin = true;
        private int _rowIndex = 0;
        private string _custtype;
        ArrearsController arrears = new ArrearsController();
        ResultInquiryDebt resul = new ResultInquiryDebt();
        List<result> _resulQRCode = new List<result>();
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ClassLogsService LogPayment = new ClassLogsService("Payment", "DataLog");
        ClassLogsService LogEventPayment = new ClassLogsService("Payment", "EventLog");
        ClassLogsService LogErrorPayment = new ClassLogsService("Payment", "ErrorLog");
        Control cnt;
        public CultureInfo usCulture = new CultureInfo("en-US");
        public CultureInfo _usCultureTh = new CultureInfo("th-TH");
        Thread threadBar;
        Thread threadData;
        private string _barcode;
        private int _selectIndex;
        public bool _statusAdvance = true;
        private int _previousIndex;
        private bool _sortDirection;
        #endregion
        public FormArrears()
        {
            InitializeComponent();
        }
        public void SetControl()
        {
            DataGridViewDetail.AutoGenerateColumns = false;
            DataGridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void InitialData()
        {
            lblCaDetail.Text = "";
            txtAmount.Text = "0.00";
            txtVat.Text = "0.00";
            txtFine.Text = "0.00";
            txtTotal.Text = "0.00";
            txtTotalSum.Text = "0.00";
            txtTotalSum.Tag = "0.00";
            lblAmountPayment.Text = "0.00";
            lblAmountDetail.Text = "0";
            lblCountCa.Text = "0";
            lblCountcapayment.Text = "0";
            lblCountCreditNote.Text = "0";
            lblCountListDetail.Text = "0";
            lblCountList.Text = "0";
        }
        private void FormArrearsNew_Load(object sender, EventArgs e)
        {
            LabelFkName.Text = GlobalClass.Dist;
            LabelEmployeeCode.Text = GlobalClass.UserId.ToString();
            LabelEmployeeName.Text = GlobalClass.UserName;
            LabelStationNo.Text = Convert.ToDecimal(GlobalClass.No).ToString("00");
            ComboBoxDocType.SelectedIndex = 0;
            comboBoxDetail.SelectedIndex = 0;
            SetPanel(true);
            GlobalClass.LstInfo = null;
            GlobalClass.LstCost = null;
            SetControl();
            InitialData();
        }
        public void SetPanel(bool status)
        {
            if (status)
            {
                StatusPanel = true;
                PanelNoneQ.Visible = true;
                PanelHaveQ.Visible = false;
                TextBoxBarcode.Text = string.Empty;
                ComboBoxDocType.SelectedIndex = 0;
            }
            else
            {
                StatusPanel = false;
                PanelHaveQ.Visible = true;
                PanelNoneQ.Visible = false;
                ButtonResetQ.Enabled = true;
            }
        }
        private void ButtonRec_Click(object sender, EventArgs e)
        {
            if (lblAmountPayment.Text.Trim() != "0.00")
            {
                if (!CheckStatusConnectMeter())
                {
                    bool tf = CheckStatusConnectMeter();
                    if (_statusCkFin)
                    {
                        if (!_statusAdvance)
                        {
                            List<inquiryInfoBeanList> lstInfo = GlobalClass.LstInfo.Where(c => c.statusPartialpayment == true).ToList();
                            if (lstInfo.Count > 0)
                            {
                                int count = 0;
                                foreach (var item in lstInfo)
                                {
                                    if (item.inquiryGroupDebtBeanList != null)
                                    {
                                        inquiryGroupDebtBeanList inquiry = item.inquiryGroupDebtBeanList.Where(c => c.Payment < c.EleTotalAmountOld).FirstOrDefault();
                                        if (inquiry != null)
                                            count++;
                                    }
                                }
                                if (count > 0)
                                    _statusAdvance = false;
                                else
                                    _statusAdvance = true;
                            }
                        }
                        if (_statusAdvance)
                        {
                            string cusName = "";
                            int i = 0;
                            List<ClassModelsReport> _list = new List<ClassModelsReport>();
                            foreach (DataGridViewRow row in DataGridViewDetail.Rows)
                            {
                                if (Convert.ToBoolean(row.Cells["ColCheckBox"].EditedFormattedValue))
                                {
                                    if (i == 0)
                                        cusName = (row.Cells["ColCusName"].Value != null) ? row.Cells["ColCusName"].Value.ToString() : "";
                                    i++;
                                }
                            }
                            FormReceiveMoney receive = new FormReceiveMoney(1);
                            receive.Countamount = (txtTotalSum.Text.Trim() != "") ? Convert.ToDouble(txtTotalSum.Text.Trim()) : 0;
                            receive.CustomerName = cusName;
                            receive.ShowDialog();
                            receive.Dispose();
                            if (GlobalClass.StatusFormMoney == 1)
                            {
                                GlobalClass.PayMent = null;
                                GlobalClass.LstInfo = null;
                                ClearData();
                                RefreshGridView();
                                if (!StatusPanel)
                                {
                                    SetButtonQ(true);
                                    SetPanel(false);
                                    Thread.Sleep(5000);
                                    ButtonCallQNone_Click(sender, e);
                                }
                                else
                                    TextBoxBarcode.Focus();
                            }
                            GlobalClass.StatusFormMoney = null;
                            _statusCkFin = true;
                            _statusAdvance = true;
                        }
                        else
                        {
                            DialogResultNew dialog = MsgBox.Show(this, "คุณต้องการรับเงินบางส่วน", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                            if (dialog.Result == DialogResult.No)
                            {
                                arrears.UpdateUserInfoNew();
                                LoadDataDetail(1, 0);
                                RefreshGridView();
                                _statusAdvance = true;
                                return;
                            }
                            else
                            {
                                FormCheckUser checkUser = new FormCheckUser("ยืนยันการรับเงินบางส่วน");
                                checkUser.ShowDialog();
                                checkUser.Dispose();
                                if (GlobalClass.AuthorizeLevel == 2 && GlobalClass.StatusForm == 1)
                                {
                                    string cusName = "";
                                    int i = 0;
                                    List<ClassModelsReport> _list = new List<ClassModelsReport>();
                                    foreach (DataGridViewRow row in DataGridViewDetail.Rows)
                                    {
                                        if (Convert.ToBoolean(row.Cells["ColCheckBox"].EditedFormattedValue))
                                        {
                                            if (i == 0)
                                                cusName = (row.Cells["ColCusName"].Value != null) ? row.Cells["ColCusName"].Value.ToString() : "";
                                            i++;
                                        }
                                    }
                                    FormReceiveMoney receive = new FormReceiveMoney(1);
                                    receive.Countamount = (txtTotalSum.Text.Trim() != "") ? Convert.ToDouble(txtTotalSum.Text.Trim()) : 0;
                                    receive.CustomerName = cusName;
                                    receive.ShowDialog();
                                    receive.Dispose();

                                    if (GlobalClass.StatusFormMoney == 1)
                                    {
                                        GlobalClass.PayMent = null;
                                        GlobalClass.LstInfo = null;
                                        ClearData();
                                        RefreshGridView();
                                        if (!StatusPanel)
                                        {
                                            SetButtonQ(true);
                                            SetPanel(false);
                                            Thread.Sleep(5000);
                                            ButtonCallQNone_Click(sender, e);
                                        }
                                        else
                                            TextBoxBarcode.Focus();
                                    }
                                    GlobalClass.StatusFormMoney = null;
                                    _statusAdvance = true;
                                }
                                else
                                {
                                    arrears.UpdateUserInfoNew();
                                    RefreshGridView();
                                    _statusAdvance = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        MsgBox.Show(this, "กรุณายืนยันการยกเลิกเบี้ยปรับ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                        btCancelFine.Focus();
                    }
                }
                else
                    MsgBox.Show(this, "ระบบไม่สร้างคำร้องต่อกลับเนื่องจากยังมีหนี้ค้างคงเหลือ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 20, FontStyle.Bold), Color.Black, true);
            }
            else
                MsgBox.Show(this, "กรุณาเลือกข้อมูลที่ต้องการชำระ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true,1);
        }
        private bool CheckStatusConnectMeter()
        {
            bool tf = false;
            if (GlobalClass.ConnectMeter != null && GlobalClass.ConnectMeter.Count > 0)
            {
                List<inquiryInfoBeanList> lstInfo = GlobalClass.LstInfo.Where(c => c.SelectCheck).ToList();
                if (lstInfo.Count > 0)
                {
                    foreach (var iteminfo in lstInfo)
                    {
                        if (iteminfo.inquiryGroupDebtBeanList != null && iteminfo.inquiryGroupDebtBeanList.Count > 0)
                        {
                            List<inquiryGroupDebtBeanList> inquiry = iteminfo.inquiryGroupDebtBeanList.Where(c => c.SelectCheck && c.debtType == "DIS").ToList();
                            if (inquiry.Count > 0)
                            {
                                foreach (var item in iteminfo.inquiryGroupDebtBeanList.Where(c => c.SelectCheck == false && c.debtType != "DIS"))
                                {
                                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                                    DateTime dtNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                                    DateTime dt = Convert.ToDateTime(item.dueDate);
                                    if (dtNow > dt)
                                        return true;
                                }
                            }
                        }
                    }
                }
            }
            return tf;
        }
        public void ClearData()
        {
            lblAmountPayment.Text = "0.00";
            lblAmountDetail.Text = "0.00";
            lblCountCa.Text = "0";
            lblCountcapayment.Text = "0";
            lblCountCreditNote.Text = "0";
            lblCountListDetail.Text = "0";
            lblCountList.Text = "0";
            txtAmount.Text = "0.00";
            txtVat.Text = "0.00";
            txtFine.Text = "0.00";
            txtTotal.Text = "0.00";
            txtTotalSum.Text = "0.00";
            GlobalClass.LstCheque = null;
            GlobalClass.LstSub = null;
            resul.Output = null;
            resul.OutputTDebt = null;
            resul.OutputSumlist = null;
            GlobalClass.LstInfo = null;
            GlobalClass.LstCost = null;
            GridDetail.DataSource = null;
            DataGridViewDetail.DataSource = null;
            labelFk.Text = string.Empty;
            labelCa.Text = string.Empty;
            labelAddress.Text = string.Empty;
            labelUi.Text = string.Empty;
            labelBranch.Text = string.Empty;
            labelTaxId.Text = string.Empty;
            labelBill.Text = string.Empty;
            lblReceiptName.Text = string.Empty;
            CheckBoxSelectAll.Checked = false;
            CheckBoxSelect.Checked = false;
            comboBoxDetail.SelectedIndex = 0;
            txtCount.Text = string.Empty;
            lblCaDetail.Text = string.Empty;
            lblStatusC.Visible = false;
            lblStatusD.Visible = false;
            LabelTmp_Time.Visible = false;
            GlobalClass.StatusForm = null;
            GlobalClass.StatusFormMoney = null;
            GlobalClass.AuthorizeLevel = 0;
            GlobalClass.CancelElectricity = null;
            GlobalClass.Cancel = null;
            GlobalClass.CancelProcessing = null;
            ButtonDelete.Enabled = false;
            btOK.Enabled = false;
            _rowIndex = 0;
        }
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            SetButtonQ(false);
            SetPanel(true);
            ButtonCallQNone.Visible = false;
            StatusPanel = false;
        }
        private void ButtonCallQ_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ButtonCallQ.Enabled = false;
                if (GlobalClass.IpServerQ == "")
                {
                    MsgBox.Show(this, "เครื่องคอมพิวเตอร์นี้ไม่ได้ถูกติดตั้งให้ต่อระบบคิวได้", "",MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true, 1);
                    //MessageBox.Show("เครื่องคอมพิวเตอร์นี้ไม่ได้ถูกติดตั้งให้ต่อระบบคิวได้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                SetProgressBar(true);
                if (!clientSocket1.IsConnected())
                {
                    LogQueue.WriteData("Connect Queue", method, LogLevel.Debug);
                    ConnectQ();
                    LogQueue.WriteData("Connect Success", method, LogLevel.Debug);
                    SetPanel(false);
                }

                //GlobalClass.UserId = Convert.ToInt32(226173);
                //GlobalClass.No = "09";
                string strSum = "";
                strSum = arrears.MyXor((char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01");
                oldSendMessage = (char)1 + strIPAddress + (char)2 + (char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01" + (char)(3) + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)4;
                LogQueue.WriteData("Send Message :: " + oldSendMessage, method, LogLevel.Debug);
                clientSocket1.Send(oldSendMessage);
            }
            catch (Exception ex)
            {
                ButtonCallQ.Enabled = true;
                SetPanel(true);
                SetProgressBar(false);
                LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message, "IP Server : " + "QServer" + " Port : " + "QPort", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }
        public void SetProgressBar(bool status)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (status == true)
                {
                    //Helper.BlendPictures(this.circularProgressBar1,this.ptMascot);
                    panelMain.Invoke(new MethodInvoker(delegate { panelMain.Enabled = false; }));
                    pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Location = new Point(377, 84); }));
                    pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Size = new Size(330, 293); }));
                    pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.BackColor = Color.FromArgb(125, Color.WhiteSmoke); }));
                }
                else
                {
                    panelMain.Invoke(new MethodInvoker(delegate { panelMain.Enabled = true; }));
                    pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Location = new Point(212, 0); }));
                    pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Size = new Size(0, 0); }));
                }
                pnProgressBar.Invoke(new MethodInvoker(delegate { pnProgressBar.Visible = status; }));
            }
            catch (Exception ex)
            {
                LogErrorPayment.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public void ConnectQ()
        {
            string host = GlobalClass.IpServerQ;//ConfigurationManager.AppSettings["HostQueue"].ToString();
            int port = int.Parse(GlobalClass.PortQ);//int.Parse(ConfigurationManager.AppSettings["Port"].ToString());
            clientSocket1.Connect(host, port);
        }
        private ArrayList getData(string data)
        {
            ArrayList retArr = new ArrayList();
            if (data == ((char)1).ToString())
                data = data.Substring(1);
            int intSTX = data.IndexOf("2");
            if (intSTX > -1)
                data = data.Substring((intSTX + 1));

            string[] tmpData = data.Split(' ');
            tmpData = tmpData[0].Split('~');
            string[] lines = Regex.Split(data, @"\W+");
            LabelNoQ.Invoke(new MethodInvoker(delegate { LabelNoQ.Text = tmpData[4].ToString(); }));
            LabelNoQ.Invoke(new MethodInvoker(delegate { LabelNoQ.Refresh(); }));
            LabelNoQ.Invoke(new MethodInvoker(delegate { this.Refresh(); }));

            if (tmpData[4] == "0000" || tmpData[4] == "000")
            {
                timer2.Enabled = true;
                SetPanelCancelWait(true);
                ButtonCancelQ.Invoke(new MethodInvoker(delegate { ButtonCancelQ.Enabled = false; }));
                ButtonRefreshQ.Invoke(new MethodInvoker(delegate { ButtonRefreshQ.Enabled = false; }));
                ButtonResetQ.Invoke(new MethodInvoker(delegate { ButtonResetQ.Enabled = false; }));
                PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Enabled = true; }));
                return retArr;
            }
            else
                SetPanelCancelWait(false);

            if (int.Parse(lines[7]) > 0)
            {
                int i = 0;
                for (i = 0; (i <= (int.Parse(lines[7]) - 1)); i++)
                {
                    String[] realData = new String[2];
                    realData[0] = lines[8 + i * 2];
                    realData[1] = lines[9 + i * 2];
                    retArr.Add(realData);
                }
            }
            return retArr;
        }
        public void SetPanelCancelWait(bool status)
        {
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Visible = status; }));
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Location = new Point(247, 65); }));
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Size = new Size(609, 386); }));
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.BackColor = Color.FromArgb(125, Color.WhiteSmoke); }));
        }
        private void ButtonRefreshQ_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (!clientSocket1.IsConnected())
                    ConnectQ();
                if (ButtonResetQ.Enabled == true)
                {
                    string strSum = arrears.MyXor((char)82 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
                    string sendData = (char)1 + strIPAddress + (char)2 + (char)82 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim() + (char)3 + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)4;
                    oldSendMessage = sendData;
                    LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                    clientSocket1.Send(sendData);
                }
                else
                {
                    // ClientSocket1.Send("04|" + txtEmpCode.Text.Trim + "|" + txtStationNo.Text.Trim + "|" + txtListNum1.Text)
                }
            }
            catch (Exception ex)
            {
                LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                //LogQueue.WriteData("Sleep " + (SleepTime * (1000 * TotalPrint)).ToString(), method, LogLevel.Debug);
                Thread.Sleep(SleepTime * (1000 * TotalPrint));
                //LogQueue.WriteData("Wake up", method, LogLevel.Debug);
            }
            catch (Exception)
            {
            }
            string strSum = arrears.MyXor((char)(85) + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
            if (printErrorAmount <= 0)
            {
                string sendData = ((char)(1) + strIPAddress + (char)(2) + (char)(85) + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~"
                            + LabelNoQ.Text.Trim() + (char)(3) + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)(4));
                oldSendMessage = sendData;
                //LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                clientSocket1.Send(sendData);
                timer1.Enabled = false;
            }
            timer1.Enabled = false;
        }
        public void GetDataToGridview()//string id, int selectIndex
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            string strBarcade = "";
            try
            {
                ThreadStart();
                string id = _barcode;
                int selectIndex = _selectIndex;
                ClassUser clsUser = new ClassUser();
                ArrearsController arr = new ArrearsController();
                if (selectIndex == 0)
                {
                    clsUser.ca = id;
                    strBarcade = " CA :: " + id;
                }
                else if (selectIndex == 1)
                {
                    clsUser.ui = TextBoxBarcode.Text.Trim();
                    strBarcade = " UI :: " + TextBoxBarcode.Text.Trim();
                }
                else if (selectIndex == 2)
                {
                    clsUser.invNo = TextBoxBarcode.Text.Trim();
                    strBarcade = " Inv No :: " + TextBoxBarcode.Text.Trim();

                }
                else if (selectIndex == 3)
                {
                    clsUser.collId = TextBoxBarcode.Text.Trim();
                    strBarcade = " CollId :: " + TextBoxBarcode.Text.Trim();
                }
                else if (selectIndex == 4)
                {
                    clsUser.collInvNo = TextBoxBarcode.Text.Trim();
                    strBarcade = " CollInvNo :: " + TextBoxBarcode.Text.Trim();
                }
                else if (selectIndex == 5)
                {
                    clsUser.reqNo = TextBoxBarcode.Text.Trim();
                    strBarcade = " ReqNo :: " + TextBoxBarcode.Text.Trim();
                }
                else
                {
                    clsUser.QNo = TextBoxBarcode.Text.Trim();
                    strBarcade = " Q :: " + TextBoxBarcode.Text.Trim();
                }
                resul = arr.SelectProcessInquiry(clsUser, resul, strBarcade, 0);
                #region
                if (GlobalClass.Cancel != null)
                {
                    foreach (var item in GlobalClass.Cancel)
                    {
                        inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == item.ca).FirstOrDefault();
                        inquiryGroupDebtBeanList costTDebt = info.inquiryGroupDebtBeanList.Find(c => c.debtId.Equals(item.debtId[0].ToString(), StringComparison.CurrentCultureIgnoreCase));
                        if (costTDebt != null)
                            costTDebt.statusCancel = false;
                    }
                }
                #endregion
                if (resul.Output != null && resul.Output.Count != 0)
                    tf = true;
                else
                    tf = false;

                if (resul.ResultMessage != null)
                {
                    foreach (var item in resul.Output)
                    {
                        if (item.inquiryGroupDebtBeanList == null || item.inquiryGroupDebtBeanList.Count == 0)
                        {
                            labelFk.Invoke(new MethodInvoker(delegate { labelFk.Text = item.FirstName; }));
                            labelCa.Invoke(new MethodInvoker(delegate { labelCa.Text = item.ca; }));
                            labelAddress.Invoke(new MethodInvoker(delegate { labelAddress.Text = item.coAddress; }));
                            labelUi.Invoke(new MethodInvoker(delegate { labelUi.Text = item.ui; }));
                            labelBranch.Invoke(new MethodInvoker(delegate { labelBranch.Text = item.TaxBranch; }));
                            labelTaxId.Invoke(new MethodInvoker(delegate { labelTaxId.Text = item.Tax20; }));
                            labelBill.Invoke(new MethodInvoker(delegate { labelBill.Text = item.collId; }));
                            lblReceiptName.Invoke(new MethodInvoker(delegate { lblReceiptName.Text = item.payeeEleName; }));
                            GridDetail.Invoke(new MethodInvoker(delegate { this.GridDetail.DataSource = null; }));
                        }
                    }
                    if (resul.Output.Count > 1)
                    {
                        DataGridViewDetail.Invoke(new MethodInvoker(delegate { this.DataGridViewDetail.DataSource = null; }));
                        //lblCountCa.Invoke(new MethodInvoker(delegate { lblCountCa.Text = resul.Output.Count.ToString(); }));
                        SetControl();
                        DataGridViewDetail.Invoke(new MethodInvoker(delegate { this.DataGridViewDetail.DataSource = resul.Output; }));
                        DataGridViewDetail.Invoke(new MethodInvoker(delegate { this.DataGridViewDetail.Refresh(); }));
                    }
                    SetProgressBar(false);
                    if (resul.ResultCode == "0000")
                        MsgBox.Show(this, resul.ResultMessage.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    else if (resul.ResultCode == null)
                    {
                        string str = "";
                        string value = "";
                        this.Invoke(new MethodInvoker(delegate { value = ComboBoxDocType.SelectedItem.ToString(); }));
                        str = "เลข " + value + " " + TextBoxBarcode.Text.Trim() + " ไม่ถูกต้อง";
                        MsgBox.Show(this, str, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    }
                    else
                    {
                        string str = "";
                        string value = "";
                        this.Invoke(new MethodInvoker(delegate { value = ComboBoxDocType.SelectedItem.ToString(); }));
                        str = "ค้นหาด้วย " + value + " " + TextBoxBarcode.Text.Trim() + " ไม่มีหนี้ค้าง " + Environment.NewLine + "( " + resul.ResultMessage.ToString() + " )";
                        MsgBox.Show(this, str, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    }
                    tf = false;
                }
                else
                {
                    SetControl();
                    DataGridViewDetail.Invoke(new MethodInvoker(delegate { this.DataGridViewDetail.DataSource = resul.Output; }));
                    DataGridViewDetail.Invoke(new MethodInvoker(delegate { this.DataGridViewDetail.Refresh(); }));
                    decimal Total = 0;
                    decimal Amount = 0;
                    decimal TotalVat = 0;
                    decimal Fine = 0;
                    int i = 0;
                    var loopTo = DataGridViewDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        if (newBool)
                            DataGridViewDetail.Rows[i].Cells[0].Value = true;
                        else
                            DataGridViewDetail.Rows[i].Cells[0].Value = false;
                        string ca = DataGridViewDetail.Rows[i].Cells["ColCA"].Value.ToString();
                        inquiryInfoBeanList info = GlobalClass.LstInfo.Find(c => c.ca.Equals(ca, StringComparison.CurrentCultureIgnoreCase));
                        if (info != null)
                            info.SelectCheck = newBool;

                        Amount = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => c.EleTotalAmount);
                        TotalVat = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => Convert.ToDecimal(c.EleTotalVat));
                        Fine = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => Convert.ToDecimal(c.Defaultpenalty));
                        Total = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => Convert.ToDecimal(c.TotalAmount));

                        if (Convert.ToBoolean(DataGridViewDetail.Rows[i].Cells["ColckCancel"].Value) == true)
                            DataGridViewDetail.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 154, 154);
                        if (Convert.ToBoolean(DataGridViewDetail.Rows[i].Cells["colStatus"].Value) == true)
                        {
                            DataGridViewDetail.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(205, 120, 91);
                            lblStatusC.Invoke(new MethodInvoker(delegate { lblStatusC.Visible = true; }));
                        }
                        else
                            lblStatusC.Invoke(new MethodInvoker(delegate { lblStatusC.Visible = false; }));
                    }
                    int countSelect = GlobalClass.LstInfo.Count(c => c.SelectCheck);
                    int rowCount = GlobalClass.LstInfo.Count();
                    if (countSelect == rowCount)
                        CheckBoxSelectAll.Invoke(new MethodInvoker(delegate { CheckBoxSelectAll.Checked = true; }));
                    else
                        CheckBoxSelectAll.Invoke(new MethodInvoker(delegate { CheckBoxSelectAll.Checked = false; }));

                    if (rowCount > 0)
                        ButtonDelete.Invoke(new MethodInvoker(delegate { ButtonDelete.Enabled = true; }));
                    txtAmount.Invoke(new MethodInvoker(delegate { txtAmount.Text = (Amount - TotalVat).ToString("#,###,###,##0.00"); }));
                    txtVat.Invoke(new MethodInvoker(delegate { txtVat.Text = TotalVat.ToString("#,###,##0.00"); }));
                    txtTotal.Invoke(new MethodInvoker(delegate { txtTotal.Text = (Amount).ToString("#,###,###,##0.00"); }));
                    txtFine.Invoke(new MethodInvoker(delegate { txtFine.Text = Fine.ToString("#,###,###,##0.00"); }));
                    txtTotalSum.Invoke(new MethodInvoker(delegate { txtTotalSum.Text = (Amount + Fine).ToString("#,###,###,##0.00"); }));
                }
                if (resul.Output.Count > 0)
                    ShowCountAll(resul.Output);
            }
            catch (Exception ex)
            {
                SetProgressBar(false);
                LogErrorPayment.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MsgBox.Show(this, "เกิดข้อผิดพลาดในการค้นหาข้อมูลหนี้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
            }
            if (!tf)
            {
                this.Invoke(new MethodInvoker(delegate { TextBoxBarcode.Text = ""; }));
                return;
            }
            else
            {
                TextBoxBarcode.Invoke(new MethodInvoker(delegate { TextBoxBarcode.Text = string.Empty; }));
                TextBoxBarcode.Invoke(new MethodInvoker(delegate { TextBoxBarcode.Focus(); }));
                int row = DataGridViewDetail.Rows.Count;
                if (row > 0)
                {
                    DataGridViewDetail.Rows[0].Selected = false;
                    DataGridViewDetail.Rows[row - 1].Selected = true;
                    LoadDataDetail(0, 1);
                }
            }
        }
        public void ShowCountAll(List<inquiryInfoBeanList> r)
        {
            lblCountCa.Invoke(new MethodInvoker(delegate { lblCountCa.Text = r.Count.ToString(); }));
            if (GlobalClass.LstInfo != null)
            {
                int CountCreditNote = GlobalClass.LstInfo.Count(c => c.inquiryGroupDebtBeanList != null && c.inquiryGroupDebtBeanList.Count != 0);
                lblCountCreditNote.Invoke(new MethodInvoker(delegate { lblCountCreditNote.Text = CountCreditNote.ToString(); }));
                int Countcapayment = GlobalClass.LstInfo.Count(c => c.inquiryGroupDebtBeanList != null && c.inquiryGroupDebtBeanList.Count != 0 && c.SelectCheck);
                lblCountcapayment.Invoke(new MethodInvoker(delegate { lblCountcapayment.Text = Countcapayment.ToString(); }));
            }
        }
        public void ShowCountAllDetail(List<inquiryGroupDebtBeanList> r, int? status = 0)
        {
            lblCountListDetail.Invoke(new MethodInvoker(delegate { lblCountListDetail.Text = r.Count.ToString(); }));
            decimal payment = r.Sum(c => (c.Payment != 0) ? c.Payment : Convert.ToDecimal(c.debtBalance));
            lblAmountDetail.Invoke(new MethodInvoker(delegate { lblAmountDetail.Text = payment.ToString("#,###,###,##0.00"); }));
            decimal paymentNew = r.Where(c => c.SelectCheck == true).Sum(c => (c.Payment != 0) ? c.Payment : Convert.ToDecimal(c.debtBalance));
            lblAmountPayment.Invoke(new MethodInvoker(delegate { lblAmountPayment.Text = paymentNew.ToString("#,###,###,##0.00"); }));
            int Countcapayment = r.Count(c => c.SelectCheck);
            lblCountList.Invoke(new MethodInvoker(delegate { lblCountList.Text = Countcapayment.ToString(); }));
        }
        public void ThreadStart()
        {
            threadBar = new Thread(new ThreadStart(LodaProgressBar));
            threadBar.Start();
        }
        public void LodaProgressBar()
        {
            SetProgressBar(true);
        }
        public void SelectCheckbox()
        {
            bool status = CheckBoxSelectAll.Checked;
            decimal Total = 0;
            decimal Amount = 0;
            decimal TotalVat = 0;
            decimal Fine = 0;
            int i = 0;
            var loopTo = DataGridViewDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CheckBoxSelectAll.Checked)
                    DataGridViewDetail.Rows[i].Cells[0].Value = true;
                else
                    DataGridViewDetail.Rows[i].Cells[0].Value = false;
                string ca = DataGridViewDetail.Rows[i].Cells["ColCA"].Value.ToString();
                inquiryInfoBeanList info = GlobalClass.LstInfo.Find(item => item.ca.Equals(ca, StringComparison.CurrentCultureIgnoreCase));
                if (info != null)
                {
                    if (info.inquiryGroupDebtBeanList != null && info.inquiryGroupDebtBeanList.Count != 0)
                    {
                        foreach (var item in info.inquiryGroupDebtBeanList)
                        {
                            inquiryGroupDebtBeanList inquiry = GlobalClass.LstCost.Find(detail => detail.debtId.Equals(item.debtId, StringComparison.CurrentCultureIgnoreCase));
                            if (inquiry != null)
                                inquiry.status = CheckBoxSelectAll.Checked;

                            if (item.debtLock == "Y")
                            {
                                item.SelectCheck = false;
                                status = false;
                            }
                            else
                            {
                                item.SelectCheck = CheckBoxSelectAll.Checked;
                                status = CheckBoxSelectAll.Checked;
                            }
                            item.status = CheckBoxSelectAll.Checked;
                        }
                    }
                    info.StatusLockDirectDebit = status;
                    CheckBoxSelect.Checked = status;
                }
                Amount = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => c.EleTotalAmount);
                TotalVat = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => Convert.ToDecimal(c.EleTotalVat));
                Fine = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => Convert.ToDecimal(c.Defaultpenalty));
                Total = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => Convert.ToDecimal(c.TotalAmount));
            }
            txtAmount.Text = (Amount - TotalVat).ToString("#,###,###,##0.00");
            txtVat.Text = TotalVat.ToString("#,###,##0.00");
            txtTotal.Text = Amount.ToString("#,###,###,##0.00");
            txtFine.Text = Fine.ToString("#,###,###,##0.00");
            txtTotalSum.Text = (Amount + Fine).ToString("#,###,###,##0.00");
            LoadDataDetail(0, 0);
            ShowCountAll(GlobalClass.LstInfo);
        }
        private void ButtonCancelQ_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if ((sender as Button).Text.ToString() != "ยกเลิกการรอคิว")
                {
                    DialogResultNew dialog = MsgBox.Show(this, "ต้องการยกเลิกคิวนี้หรือไม่", "ยกเลิกคิว", MessageBoxButtons.YesNo, MessageBoxIcon.Question, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    if(dialog.Result == DialogResult.No)// if (MessageBox.Show("ต้องการยกเลิกคิวนี้หรือไม่ ", "ยกเลิกคิว", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    else
                        q_count = false;
                }
                if (ButtonResetQ.Enabled)
                {
                    string strSum = arrears.MyXor((char)67 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
                    string sendData = (char)1 + strIPAddress + (char)2 + (char)67 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim() + (char)3 + "" + (char)(Convert.ToInt32(strSum)) + "" + Convert.ToChar(4);
                    oldSendMessage = sendData;
                    LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                    clientSocket1.Send(sendData);
                }
                else
                {
                    string strSum = arrears.MyXor((char)67 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
                    string sendData = (char)1 + strIPAddress + (char)2 + (char)67 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim() + (char)3 + "" + (char)(Convert.ToInt32(strSum)) + "" + Convert.ToChar(4);
                    oldSendMessage = sendData;
                    LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                    clientSocket1.Send(sendData);
                }
                ClearData();
            }
            catch (Exception ex)
            {
                LogQueueError.WriteData(("Error Message :: " + ex.ToString()), method, LogLevel.Debug);
                //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonResetQ_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ButtonResetQ.Enabled = false;
                string strSum = arrears.MyXor((char)80 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim());
                string sendData = (char)1 + strIPAddress + (char)2 + (char)80 + "~0~" + strUserCode + "~" + LabelStationNo.Text.Trim() + "~" + LabelNoQ.Text.Trim() + (char)3 + "" + (char)(Convert.ToInt32(strSum)) + "" + Convert.ToChar(4);
                oldSendMessage = sendData;
                LogQueue.WriteData(("Send Message :: " + oldSendMessage), method, LogLevel.Debug);
                clientSocket1.Send(sendData);
                ButtonRefreshQ.Enabled = false;
                ButtonCancelQ.Enabled = false;
                ButtonCallQ.Enabled = true;
                SetPanel(true);
                ButtonCallQNone.Visible = true;
            }
            catch (Exception ex)
            {
                LogQueueError.WriteData(("Error Message :: " + ex.ToString()), method, LogLevel.Debug);
                //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonCancelWait_Click(object sender, EventArgs e)
        {
            this.ButtonResetQ_Click(sender, e);
            this.ButtonCancelQ_Click(sender, e);
            PanelCancelWait.Invoke(new MethodInvoker(delegate { PanelCancelWait.Visible = false; }));
        }
        private void ButtonCallQNone_Click(object sender, EventArgs e)
        {
            //System.Reflection.MethodBase method;
            //method = System.Reflection.MethodBase.GetCurrentMethod();
            //SetPanelCancelWait(true);
            //try
            //{
            //    if (GlobalClass.IpServerQ == "")
            //    {
            //        MessageBox.Show("เครื่องคอมพิวเตอร์นี้ไม่ได้ถูกติดตั้งให้ต่อระบบคิวได้", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //        return;
            //    }
            //    ButtonCallQ.Enabled = false;
            //    if (!clientSocket1.IsConnected())
            //    {
            //        LogQueue.WriteData("Connect Queue", method, LogLevel.Debug);
            //        ConnectQ();
            //        LogQueue.WriteData("Connect Success", method, LogLevel.Debug);
            //        SetPanel(false);
            //    }
            //    string strSum = "";
            //    strSum = arrears.MyXor((char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01");
            //    oldSendMessage = (char)1 + strIPAddress + (char)2 + (char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01" + (char)(3) + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)4;
            //    LogQueue.WriteData("Send Message :: " + oldSendMessage, method, LogLevel.Debug);
            //    clientSocket1.Send(oldSendMessage);
            //}
            //catch (Exception ex)
            //{
            //    ButtonCallQ.Enabled = true;
            //    SetPanel(true);
            //    SetProgressBar(false);
            //    LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            //    MessageBox.Show(ex.Message, "IP Server : " + "QServer" + " Port : " + "QPort", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //}
        }
        private void clientSocket_Receive(string receiveData)
        {
            if (receiveData.ToString().Substring(0, 1) != "")
            {
                isRefreshData = true;
                return;
            }
            retMessage = receiveData;
            oldData = retMessage;
            arrData = getData(retMessage);
            if (StatusPanel)
                ComboBoxDocType.Invoke(new MethodInvoker(delegate { ComboBoxDocType.SelectedIndex = 1; }));

            foreach (var item in arrData)
            {
                _barcode = ((string[])item)[1].ToString();
                _selectIndex = 0;
                GetDataToGridview();
            }
            if (this.LabelNoQ.Text == "000" || LabelNoQ.Text == "0000")
            {
                ButtonRefreshQ.Invoke(new MethodInvoker(delegate { ButtonRefreshQ.Enabled = false; }));
                ButtonCancelQ.Invoke(new MethodInvoker(delegate { ButtonCancelQ.Enabled = false; }));
                ButtonResetQ.Invoke(new MethodInvoker(delegate { ButtonResetQ.Enabled = false; }));
                SetPanelCancelWait(true);
                SetProgressBar(false);
                clientSocket1.Close();
                QNo = "0000";
            }
            else
            {
                ButtonRefreshQ.Invoke(new MethodInvoker(delegate { ButtonRefreshQ.Enabled = true; }));
                ButtonResetQ.Invoke(new MethodInvoker(delegate { ButtonResetQ.Enabled = true; }));
                ButtonResetQ.Invoke(new MethodInvoker(delegate { SetPanel(false); }));
                ButtonCancelQ.Invoke(new MethodInvoker(delegate { ButtonCancelQ.Enabled = true; }));
                QNo = LabelNoQ.Text;
                ButtonCallQ.Invoke(new MethodInvoker(delegate { ButtonCallQ.Enabled = false; }));
                SetProgressBar(false);
            }
            isRefreshData = true;
        }
        public void SetButtonQ(bool status)
        {
            ButtonCallQ.Enabled = status;
            ButtonRefreshQ.Enabled = status;
            ButtonResetQ.Enabled = status;
            ButtonCancelQ.Enabled = status;
            LabelNoQ.Text = "0000";
        }
        private void clientSocket2_Receive(string receiveData)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            //LogQueue.WriteData("Receive :: " + receiveData, method, LogLevel.Debug);
            string retMessage = receiveData;
            oldData = retMessage;
            arrData = GetDataCallQ(retMessage);
            foreach (var item in arrData)
            {
                _barcode = ((string[])item)[1].ToString();
                _selectIndex = 0;
                GetDataToGridview();
            }
        }
        public void CallQ()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                string host = GlobalClass.IpServerQ;
                int port = int.Parse(GlobalClass.PortQ);
                if (!clientSocket2.IsConnected())
                {
                    LogQueue.WriteData("Connect Queue", method, LogLevel.Debug);
                    clientSocket2.Connect(host, port);
                    LogQueue.WriteData("Connect Success", method, LogLevel.Debug);
                }
                string strSum = "";
                strSum = arrears.MyXor((char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01");
                oldSendMessage = (char)1 + strIPAddress + (char)2 + (char)78 + "~0~" + GlobalClass.UserId.ToString() + "~" + GlobalClass.No + "~01" + (char)(3) + "" + (char)(Convert.ToInt32(strSum)) + "" + (char)4;
                LogQueue.WriteData("Send Message :: " + oldSendMessage, method, LogLevel.Debug);
                clientSocket2.Send(oldSendMessage);
            }
            catch (Exception ex)
            {
                LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message, "IP Server : " + "QServer" + " Port : " + "QPort", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public ArrayList GetDataCallQ(string data)
        {
            ArrayList array = new ArrayList();
            if (data == ((char)1).ToString())
                data = data.Substring(1);
            int intSTX = data.IndexOf("2");
            if (intSTX > -1)
                data = data.Substring((intSTX + 1));

            string[] tmpData = data.Split(' ');
            tmpData = tmpData[0].Split('~');
            string[] lines = Regex.Split(data, @"\W+");
            if (tmpData[4].ToString() != "0000")
            {
                LabelNoQ.Invoke(new MethodInvoker(delegate { LabelNoQ.Text = tmpData[4].ToString(); }));
                LabelNoQ.Invoke(new MethodInvoker(delegate { LabelNoQ.Refresh(); }));
                LabelNoQ.Invoke(new MethodInvoker(delegate { this.Refresh(); }));
                timer2.Enabled = false;
                clientSocket2.Close();
                EnabledButton(true);
                ButtonCallQ.Invoke(new MethodInvoker(delegate { ButtonCallQ.Enabled = false; }));
            }
            if (int.Parse(lines[7]) > 0)
            {
                SetPanelCancelWait(false);
                int i = 0;
                for (i = 0; (i <= (int.Parse(lines[7]) - 1)); i++)
                {
                    String[] realData = new String[2];
                    realData[0] = lines[8 + i * 2];
                    realData[1] = lines[9 + i * 2];
                    array.Add(realData);
                }
            }
            return array;
        }
        public void EnabledButton(bool status)
        {
            ButtonRefreshQ.Invoke(new MethodInvoker(delegate { ButtonRefreshQ.Enabled = status; }));
            ButtonCancelQ.Invoke(new MethodInvoker(delegate { ButtonCancelQ.Enabled = status; }));
            ButtonResetQ.Invoke(new MethodInvoker(delegate { ButtonResetQ.Enabled = status; }));
            ButtonCallQ.Invoke(new MethodInvoker(delegate { ButtonCallQ.Enabled = status; }));
        }
        private void timer2_Tick(object sender, EventArgs e)
        {
            CallQ();
        }
        public void searchCa(string userId)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            //LogQueue = new ClassLogsService("Search");
        }
        private void TextBoxBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void DataGridViewDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    string ca = DataGridViewDetail.Rows[e.RowIndex].Cells["ColCA"].Value.ToString();
                    inquiryInfoBeanList info = GlobalClass.LstInfo.Find(itemdetail => itemdetail.ca.Equals(ca, StringComparison.CurrentCultureIgnoreCase));
                    if (info != null)
                    {
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[e.RowIndex].Cells["ColCheckBox"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        info.Status = newBool;
                        info.SelectCheck = newBool;
                        info.StatusLockDirectDebit = newBool;
                        int countRow = 0;
                        if (info.inquiryGroupDebtBeanList.Count > 0 && info.inquiryGroupDebtBeanList != null)
                        {
                            foreach (var item in info.inquiryGroupDebtBeanList)
                            {
                                inquiryGroupDebtBeanList lstCost = GlobalClass.LstCost.Find(itemdetail => itemdetail.debtId.Equals(item.debtId, StringComparison.CurrentCultureIgnoreCase));
                                if (lstCost != null)
                                {
                                    lstCost.status = newBool;
                                    if (lstCost.debtLock == "Y")
                                        item.SelectCheck = false;
                                    else
                                        item.SelectCheck = newBool;
                                }
                                if (newBool)
                                    countRow++;
                            }
                        }
                        if (countRow > 0)
                            CheckBoxSelect.Checked = true;
                        else
                            CheckBoxSelect.Checked = false;
                        LoadDataDetail(0, e.RowIndex);

                        if (newBool)
                            DataGridViewDetail.Rows[e.RowIndex].Cells[0].Value = true;
                        else
                            DataGridViewDetail.Rows[e.RowIndex].Cells[0].Value = false;
                        var count = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).ToList();
                        var countNew = GlobalClass.LstInfo.ToList();
                        if (count.Count == countNew.Count)
                            CheckBoxSelectAll.Checked = true;
                        else
                            CheckBoxSelectAll.Checked = false;

                        decimal TotalVat = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => c.EleTotalVat);
                        decimal Fine = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => Convert.ToDecimal(c.Defaultpenalty));
                        decimal Amount = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => c.EleTotalAmount);
                        decimal Total = GlobalClass.LstInfo.Where(c => c.SelectCheck).Sum(c => c.TotalAmount);
                        txtAmount.Text = (Amount - TotalVat).ToString("#,###,###,##0.00");
                        txtVat.Text = TotalVat.ToString("#,###,##0.00");
                        txtTotal.Text = Amount.ToString("#,###,###,##0.00");
                        txtFine.Text = Fine.ToString("#,###,###,##0.00");
                        txtTotalSum.Text = (Amount + Fine).ToString("#,###,###,##0.00");
                        ShowCountAll(GlobalClass.LstInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormArrearsNew | Function :: DataGridViewDetail_CellContentDoubleClick()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void CheckBoxSelectAll_Click(object sender, EventArgs e)
        {
            SelectCheckbox();
        }
        private void CheckBoxSelectAll_MouseHover(object sender, EventArgs e)
        {
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(this.CheckBoxSelectAll, "เลือกรายการทั้งหมด");
        }
        private void ComboBoxDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxBarcode.Text = string.Empty;
            TextBoxBarcode.Focus();
        }
        public void RefreshGridView()
        {
            List<inquiryInfoBeanList> _list = new List<inquiryInfoBeanList>();
            if (GlobalClass.LstInfo != null)
            {
                foreach (var Item in GlobalClass.LstInfo)
                {
                    inquiryInfoBeanList models = new inquiryInfoBeanList();
                    models.custId = Item.custId;
                    models.ca = Item.ca;
                    models.ui = Item.ui;
                    models.FirstName = Item.FirstName;
                    models.coAddress = Item.coAddress;
                    models.custType = Item.custType;
                    models.payeeEleName = Item.payeeEleName;
                    models.payeeEleTax20 = Item.payeeEleTax20;
                    models.payeeEleAddress = Item.payeeEleAddress;
                    models.payeeEleTaxBranch = Item.payeeEleTaxBranch;
                    models.lockCheque = Item.lockCheque;
                    if (labelCa.Text == Item.ca)
                    {
                        models.EleTotalAmount = Item.EleTotalAmount;
                        models.EleTotalVat = Item.EleTotalVat;
                        models.Defaultpenalty = Item.Defaultpenalty;
                        models.TotalAmount = Item.TotalAmount;
                        if (!Item.Status)
                            models.SelectCheck = false;
                        else
                            models.SelectCheck = true;
                    }
                    else
                    {
                        models.EleTotalAmount = Item.EleTotalAmount;
                        models.EleTotalVat = Item.EleTotalVat;
                        models.Defaultpenalty = Item.Defaultpenalty;
                        models.EleInvCount = Item.EleInvCount;
                        models.TotalAmount = Item.TotalAmount;
                        if (!Item.Status)
                            models.SelectCheck = false;
                        else
                            models.SelectCheck = true;
                    }
                    models.lockBill = Item.lockBill;
                    models.debtType = Item.debtType;
                    _list.Add(models);
                }
                this.DataGridViewDetail.DataSource = _list;
                CountAmount();
                foreach (DataGridViewRow row in DataGridViewDetail.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["ColckCancel"].Value) == true)
                        row.DefaultCellStyle.BackColor = Color.FromArgb(255, 154, 154);
                }
            }
            else
            {
                this.DataGridViewDetail.DataSource = null;
                this.DataGridViewDetail.Refresh();
                this.GridDetail.ScrollBars = ScrollBars.None;
                this.GridDetail.DataSource = null;
                this.GridDetail.ScrollBars = ScrollBars.Both;
                this.GridDetail.Refresh();
            }
        }
        public void CountAmount()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (GlobalClass.LstInfo != null)
                {
                    decimal _defaultpenalty = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).Sum(c => Convert.ToDecimal(c.Defaultpenalty));
                    decimal _vat = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).Sum(c => c.EleTotalVat);
                    decimal _payment = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).Sum(c => c.EleTotalAmount);
                    var count = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).ToList();
                    var countNew = GlobalClass.LstInfo.ToList();
                    if (count.Count == countNew.Count)
                        CheckBoxSelectAll.Checked = true;
                    else
                        CheckBoxSelectAll.Checked = false;
                    decimal _amountDetail = GlobalClass.LstInfo.Where(c => c.ca == labelCa.Text).Sum(c => c.TotalAmount);
                    decimal _amountl = GlobalClass.LstInfo.Where(c => c.ca == labelCa.Text).Sum(c => Convert.ToDecimal(c.Defaultpenalty));
                    //lblAmountPayment.Text = count.Count.ToString();
                    txtAmount.Text = (_payment - _vat).ToString("#,###,###,##0.00");
                    txtVat.Text = _vat.ToString("#,###,##0.00");
                    txtTotal.Text = _payment.ToString("#,###,###,##0.00");
                    txtFine.Text = _defaultpenalty.ToString("#,###,###,##0.00");
                    txtTotalSum.Text = (_payment + _defaultpenalty).ToString("#,###,###,##0.00");
                    ShowCountAll(GlobalClass.LstInfo);
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void TextBoxBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                System.Reflection.MethodBase method;
                method = System.Reflection.MethodBase.GetCurrentMethod();
                try
                {
                    buttonZ1.Enabled = false;
                    buttonZ1.Cursor = Cursors.No;
                    if (ComboBoxDocType.Text == "บัญชีแสดงสัญญา")
                        ComboBoxDocType.SelectedIndex = 0;
                    int select = ComboBoxDocType.SelectedIndex;
                    if (select == 6)
                        GetDataWaterbill();
                    else
                    {
                        _selectIndex = select;
                        if (select == 0)
                        {
                            int lengthBarcode = TextBoxBarcode.Text.Trim().Length;
                            if (lengthBarcode == 9 || lengthBarcode == 8)
                            {
                                timer3.Enabled = false;
                                this.Invoke(new MethodInvoker(delegate { _barcode = TextBoxBarcode.Text.Trim(); }));
                                threadData = new Thread(new ThreadStart(GetDataToGridview));
                                threadData.Start();
                            }
                            else if (lengthBarcode > 55)
                            {
                                timer3.Enabled = false;
                                string strData = TextBoxBarcode.Text.Substring(16, 9);
                                _barcode = strData;
                                threadData = new Thread(new ThreadStart(GetDataToGridview));
                                threadData.Start();
                            }
                            else
                                timer3.Enabled = true;
                        }
                        else
                        {
                            this.Invoke(new MethodInvoker(delegate { _barcode = TextBoxBarcode.Text.Trim(); }));
                            threadData = new Thread(new ThreadStart(GetDataToGridview));
                            threadData.Start();
                        }
                        if (!StatusPanel)
                            SetPanel(false);
                    }
                }
                catch (Exception ex)
                {
                    SetProgressBar(false);
                    meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                    //this.Invoke(new MethodInvoker(delegate { MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }));
                }
            }
        }
        private void ButtonAdvance_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double payment = 0;
                if (labelCa.Text == "")
                {
                    MsgBox.Show(this, "กรุณาเลือก CA ที่ต้องการรับเงินล่วงหน้า", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    //MessageBox.Show("กรุณาเลือก CA ที่ต้องการรับเงินล่วงหน้า", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (lblAmountPayment.Text == "0.00")
                    payment = 0;
                else
                    payment = Convert.ToDouble(lblAmountPayment.Text);
                FormReceiveinadvance advance = new FormReceiveinadvance();
                advance.LabelPayment.Text = payment.ToString("#,###,###,##0.00");
                advance.ca = labelCa.Text;
                advance.resul = resul;
                advance.ShowDialog();
                advance.Dispose();
                if (GlobalClass.StatusAdvance == 1)
                {
                    resul.Output = GlobalClass.LstInfo;
                    LoadData(labelCa.Text);
                    RefreshGridView();
                }
                TextBoxBarcode.Focus();
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonClear_MouseHover(object sender, EventArgs e)
        {
            ToolTip1.SetToolTip(this.ButtonClear, "เคลียร์ข้อมูล");
        }
        private void ButtonDelete_MouseHover(object sender, EventArgs e)
        {
            ToolTip1.SetToolTip(this.ButtonDelete, "ลบข้อมูล");
        }
        private void ButtonSearch_MouseHover(object sender, EventArgs e)
        {
            ToolTip1.SetToolTip(this.ButtonSearch, "ค้นหาข้อมูล");
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                int i = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    //if (DataGridViewDetail.CurrentCell is DataGridViewCheckBoxCell)
                    //{
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                    {
                        string ca = DataGridViewDetail.Rows[i].Cells["ColCA"].Value.ToString();
                        var itemToRemove = GlobalClass.LstInfo.SingleOrDefault(r => r.ca == ca);
                        if (itemToRemove != null)
                        {
                            GlobalClass.LstInfo.Remove(itemToRemove);
                            meaLog.WriteData("CancelInterest :: Success :: CA : " + ca + " |  User ID : " + GlobalClass.UserId, method, LogLevel.Debug);
                        }
                    }
                    //}
                }
                resul.Output = GlobalClass.LstInfo;
                if (GlobalClass.LstInfo.Count <= 0)
                    ClearData();
                else
                {
                    this.DataGridViewDetail.DataSource = null;
                    this.GridDetail.DataSource = null;
                    this.DataGridViewDetail.DataSource = GlobalClass.LstInfo;
                    DataGridViewDetail.Rows[0].Selected = true;
                    LoadDataDetail(0, 0);
                    CheckBoxSelectAll.Checked = true;
                    SelectCheckbox();
                    _rowIndex = 0;
                    CheckBoxSelect.Checked = true;
                    CheckBoxSelect_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MsgBox.Show(this, "เกิดข้อผิดพลาดในการลบข้อมูล", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (DataGridViewDetail.Rows.Count > 0)
                {
                    int index = 0;
                    List<inquiryInfoBeanList> lst = new List<inquiryInfoBeanList>();
                    if (TextBoxSearch.Text != "")
                    {
                        inquiryInfoBeanList agedTwenty = resul.Output.Where<inquiryInfoBeanList>(x => x.ca == TextBoxSearch.Text).Single<inquiryInfoBeanList>();
                        index = resul.Output.IndexOf(agedTwenty);
                        lst = resul.Output.Where(c => c.ca == TextBoxSearch.Text).ToList();
                    }
                    if (lst.Count > 0)
                    {
                        DataGridViewDetail.DataSource = resul.Output;
                        DataGridViewDetail.ClearSelection();
                        DataGridViewDetail.CurrentCell = null;
                        DataGridViewDetail.Rows[index].Selected = true;
                        LoadDataDetail(2, index);
                    }
                    else
                        MsgBox.Show(this, "ไม่มีข้อมูลในระบบ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true,1);
                    //MessageBox.Show("ไม่มีข้อมูลในระบบ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    TextBoxSearch.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                TextBoxSearch.Text = string.Empty;
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MsgBox.Show(this, "เกิดข้อผิดพลาดในการค้นหาข้อมูล", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true, 1);
                //MessageBox.Show("ไม่มีข้อมูลในระบบ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void LoadData(string ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                bool status = false;
                ResultCostTDebt resulDebt = new ResultCostTDebt();
                inquiryInfoBeanList inquiry = GlobalClass.LstInfo.Where(c => c.ca == ca).FirstOrDefault();
                GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Columns[3].Visible = false; }));
                if (inquiry.collInvNo != null && inquiry.collInvNo != "")
                    GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Columns[1].Visible = true; }));
                else
                    GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Columns[1].Visible = false; }));
                resulDebt = arrears.SelectDataInquiryDebtDetail(resul, ca);
                GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.DataSource = resulDebt.Output; }));
                int countDetail = 0;
                int row = 0;
                int countCheck = 0;
                bool statusType = false;
                if (resulDebt.Output != null)
                {
                    countDetail = resulDebt.Output.Count();
                    foreach (var item in resulDebt.Output)
                    {
                        if (item.debtLock == "Y")
                        {
                            GridDetail.Rows[row].DefaultCellStyle.BackColor = Color.FromArgb(255, 154, 154);
                            GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Rows[row].Cells["ColCheckBoxDetail"].ReadOnly = true; }));
                        }
                        if (item.reqNo != "" && item.reqNo != null && row == 0)
                            GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Columns[3].Visible = true; }));

                        if (item.StatusLockDirectDebit)
                        {
                            //if (item.SelectCheck)
                            //{
                            inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(c => c.ca.Equals(item.ca, StringComparison.CurrentCultureIgnoreCase));
                            if (userInfo != null)
                                userInfo.StatusLockDirectDebit = true;
                            status = item.StatusLockDirectDebit;
                            //}
                            GridDetail.Rows[row].DefaultCellStyle.BackColor = Color.FromArgb(255, 154, 154);
                        }
                        if (item.debtType == "DIS")
                        {
                            if (item.SelectCheck == false && item.debtLock == "Y")
                                GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Rows[row].Cells["ColCheckBoxDetail"].ReadOnly = true; }));
                            statusType = true;
                        }
                        DateTime dateTime = DateTime.Now;
                        if (item.dueDate != null && item.dueDate != "")
                        {
                            DateTime DateTime = DateTime.Parse(item.dueDate);
                            if (DateTime <= dateTime)
                                GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Rows[row].Cells["colPayment"].ReadOnly = false; }));
                            else
                                GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Rows[row].Cells["colPayment"].ReadOnly = true; }));
                        }
                        else
                            GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Rows[row].Cells["colPayment"].ReadOnly = true; }));

                        if (item.debtType == "GUA" || item.installmentsFlag == "N")
                            GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Rows[row].Cells["colPayment"].ReadOnly = false; }));
                        else
                            GridDetail.Invoke(new MethodInvoker(delegate { GridDetail.Rows[row].Cells["colPayment"].ReadOnly = true; }));

                        if (item.SelectCheck)
                            countCheck++;
                        row++;
                    }
                    if (countCheck == resulDebt.Output.Count())
                        CheckBoxSelect.Invoke(new MethodInvoker(delegate { CheckBoxSelect.Checked = true; }));
                    else
                        CheckBoxSelect.Invoke(new MethodInvoker(delegate { CheckBoxSelect.Checked = false; }));
                }
                else
                    CheckBoxSelect.Invoke(new MethodInvoker(delegate { CheckBoxSelect.Checked = false; }));

                buttonZ1.Invoke(new MethodInvoker(delegate { buttonZ1.Enabled = statusType; }));
                if (statusType)
                    buttonZ1.Invoke(new MethodInvoker(delegate { buttonZ1.Cursor = Cursors.Hand; }));
                else
                    buttonZ1.Invoke(new MethodInvoker(delegate { buttonZ1.Cursor = Cursors.No; }));

                lblStatusD.Invoke(new MethodInvoker(delegate { lblStatusD.Visible = status; }));
                lblCountListDetail.Invoke(new MethodInvoker(delegate { lblCountListDetail.Text = countDetail.ToString(); }));

                ShowCountAllDetail(resulDebt.Output, 1);
            }
            catch (Exception ex)
            {
                SetProgressBar(false);
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                this.Invoke(new MethodInvoker(delegate { MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }));
            }
        }
        public void CheckStatus()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double Amount = 0;
                double TotalVat = 0;
                double TotalAmount = 0;
                double Fine = 0;
                int i = 0;
                int countCheck = 0;
                var loopTo = GridDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        DataGridViewRow rownew = GridDetail.Rows[i];
                        rownew.Cells["ColCheckBoxDetail"].Value = !Convert.ToBoolean(rownew.Cells["ColCheckBoxDetail"].EditedFormattedValue);
                        bool newBool = (bool)rownew.Cells[0].Value;
                        bool originalBool = !newBool;
                        if (originalBool)
                        {
                            Fine += Convert.ToDouble(GridDetail.Rows[i].Cells["colDefaultpenalty"].Value);
                            Amount += Convert.ToDouble(GridDetail.Rows[i].Cells["colAmountDetail"].Value);
                            TotalVat += Convert.ToDouble(GridDetail.Rows[i].Cells["colVatDetail"].Value);
                            TotalAmount += Convert.ToDouble(GridDetail.Rows[i].Cells["colPayment"].Value);
                            GridDetail.Rows[i].Cells[0].Value = true;
                            countCheck++;
                            if (countCheck == GridDetail.Rows.Count)
                                CheckBoxSelectAll.Checked = true;
                        }
                        else
                        {
                            GridDetail.Rows[i].Cells[0].Value = false;
                            CheckBoxSelectAll.Checked = false;
                            if (countCheck == 0)
                                countCheck = 0;
                        }
                    }
                }
                //lblAmountPayment.Text = countCheck.ToString();
                txtAmount.Text = (TotalAmount - TotalVat).ToString("#,###,###,##0.00");
                txtVat.Text = TotalVat.ToString("#,###,##0.00");
                txtTotal.Text = TotalAmount.ToString("#,###,###,##0.00");
                txtFine.Text = Fine.ToString("#,###,###,##0.00");
                txtTotalSum.Text = (Amount + Fine).ToString("#,###,###,##0.00");
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonClear_Click(object sender, EventArgs e)
        {
            ClearData();
            TextBoxBarcode.Text = string.Empty;
            TextBoxBarcode.Focus();
        }
        public void SelectCheckboxDetail()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                int i = 0;
                bool status = false;
                inquiryInfoBeanList Info = GlobalClass.LstInfo.Where(c => c.ca == labelCa.Text).FirstOrDefault();
                var loop = GridDetail.Rows.Count - 1;
                for (int j = 0; j <= loop; j++)
                {
                    string DebtLock = GridDetail.Rows[j].Cells["colDebtLock"].Value.ToString();
                    var Iddebt = GridDetail.Rows[j].Cells["ColDebtId"].Value.ToString();
                    inquiryGroupDebtBeanList costTDebt = Info.inquiryGroupDebtBeanList.Find(itemDetail => itemDetail.debtId.ToString().Equals(Iddebt, StringComparison.CurrentCultureIgnoreCase));
                    if (CheckBoxSelect.Checked == true && DebtLock != "Y")
                    {
                        GridDetail.Rows[j].Cells[0].Value = true;
                        costTDebt.status = CheckBoxSelect.Checked;
                        costTDebt.SelectCheck = CheckBoxSelect.Checked;
                        i++;
                    }
                    else
                        GridDetail.Rows[j].Cells[0].Value = false;
                }
                if (i > 0)
                    status = true;
                else
                    CheckBoxSelect.Checked = false;

                if (Info != null)
                {
                    decimal _vat = Info.inquiryGroupDebtBeanList.Where(c => c.status == true).Sum(c => Convert.ToDecimal(c.vatBalance));
                    decimal _defaultpenalty = Info.inquiryGroupDebtBeanList.Where(c => c.status == true).Sum(c => c.Defaultpenalty);
                    decimal _payment = Info.inquiryGroupDebtBeanList.Where(c => c.status == true).Sum(c => (c.Payment != 0) ? c.Payment : Convert.ToDecimal(c.debtBalance));
                    arrears.UpdateStatusUserInfo(labelCa.Text, status, _payment, _vat, _defaultpenalty.ToString());
                    if (GridDetail.Rows.Count > 0)
                    {
                        DataGridViewDetail.Rows[0].Selected = false;
                        DataGridViewDetail.Rows[_rowIndex].Selected = true;
                    }
                }


                //Info.SelectCheck = status;
                ShowCountAllDetail(Info.inquiryGroupDebtBeanList.ToList());
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void GridDetail_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.TextChanged += new EventHandler(tb_TextChanged);
            cnt = e.Control;
            cnt.TextChanged += tb_TextChanged;
        }
        void tb_TextChanged(object sender, EventArgs e)
        {
            if (cnt.Text != string.Empty)
            {
                var isNumeric = decimal.TryParse(cnt.Text, out decimal n);
                if (isNumeric)
                {
                    DataGridViewCell cell = GridDetail.CurrentCell;
                    decimal balance = 0;
                    inquiryGroupDebtBeanList _costTDebt = new inquiryGroupDebtBeanList();
                    DataGridViewCheckBoxCell check = (DataGridViewCheckBoxCell)GridDetail.Rows[cell.RowIndex].Cells["ColCheckBoxDetail"];
                    bool isCheck = (bool)check.EditedFormattedValue;
                    if (isCheck)
                    {
                        _statusAdvance = false;
                        string hdrId = "";
                        string IdDebt = "";
                        IdDebt = GridDetail.Rows[cell.RowIndex].Cells["ColDebtId"].Value.ToString();
                        _costTDebt = arrears.UpdateStatus(IdDebt, true, Convert.ToDecimal(cnt.Text), labelCa.Text, hdrId);
                        GridDetail.Rows[cell.RowIndex].Cells["coldebtBalance"].Value = Convert.ToDecimal(cnt.Text);
                        GridDetail.Rows[cell.RowIndex].Cells["colVatDetail"].Value = _costTDebt.vatBalance;
                        balance += Convert.ToDecimal(GridDetail.Rows[cell.RowIndex].Cells["colAmountDetail"].EditedFormattedValue);

                        decimal CountAmount = balance - Convert.ToDecimal(cnt.Text);
                        if (CountAmount < 0)
                        {
                            MsgBox.Show(this, "ยอดเงินเกินจำนวนที่ต้องชำระ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true, 1);
                            //MessageBox.Show("ยอดเงินเกินจำนวนที่ต้องชำระ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            cnt.Text = balance.ToString();
                            cnt.Focus();
                            return;
                        }
                        inquiryInfoBeanList Info = GlobalClass.LstInfo.Where(c => c.ca == labelCa.Text).FirstOrDefault();
                        inquiryGroupDebtBeanList costTDebt = Info.inquiryGroupDebtBeanList.Find(itemDetail => itemDetail.debtId.ToString().Equals(IdDebt, StringComparison.CurrentCultureIgnoreCase));
                        costTDebt.vatBalance = _costTDebt.vatBalance;
                        costTDebt.debtBalance = Convert.ToDecimal(cnt.Text);
                        if (Info != null)
                        {
                            decimal _vat = Info.inquiryGroupDebtBeanList.Where(c => c.status == true).Sum(c => Convert.ToDecimal(c.vatBalance));
                            decimal _defaultpenalty = Info.inquiryGroupDebtBeanList.Where(c => c.status == true).Sum(c => c.Defaultpenalty);
                            decimal _payment = Info.inquiryGroupDebtBeanList.Where(c => c.status == true).Sum(c => (c.Payment != 0) ? c.Payment : Convert.ToDecimal(c.debtBalance));
                            arrears.UpdateStatusUserInfo(labelCa.Text, true, _payment, _vat, _defaultpenalty.ToString());
                            RefreshGridView();
                            lblAmountPayment.Text = (_payment + _defaultpenalty).ToString("#,###,###,##0.00");
                            DataGridViewDetail.Rows[0].Selected = false;
                            DataGridViewDetail.Rows[_rowIndex].Selected = true;
                        }
                    }
                }
                else
                    return;
            }
        }
        public void SumAmount(int status, int rows)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double Amount = 0;
                double Fine = 0;
                if (status == 0)
                {
                    lblCaDetail.Invoke(new MethodInvoker(delegate { lblCaDetail.Text = DataGridViewDetail.Rows[rows].Cells["ColCA"].Value.ToString(); }));
                    //Amount += Convert.ToDouble(DataGridViewDetail.Rows[rows].Cells["ColSumTotalAmount"].Value);
                    //Fine += Convert.ToDouble(DataGridViewDetail.Rows[rows].Cells["ColPenaltyAll"].Value);
                }
                else
                {
                    lblCaDetail.Invoke(new MethodInvoker(delegate { lblCaDetail.Text = DataGridViewDetail.CurrentRow.Cells["ColCA"].Value.ToString(); }));
                    //Amount += Convert.ToDouble(DataGridViewDetail.CurrentRow.Cells["ColSumTotalAmount"].Value);
                    //Fine += Convert.ToDouble(DataGridViewDetail.CurrentRow.Cells["ColPenaltyAll"].Value);
                }
                //lblAmountPayment.Invoke(new MethodInvoker(delegate { lblAmountPayment.Text = (Amount + Fine).ToString("#,###,###,##0.00"); }));
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void SumAmountDetail()
        {
            double payment = 0;
            double fine = 0;
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                DataGridViewRow rownew = GridDetail.Rows[i];
                DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBoxDetail"];
                bool isChecked = (bool)checkCell.EditedFormattedValue;
                if (isChecked)
                {
                    if (GridDetail.Rows[i].Cells["colDefaultpenalty"].Value.ToString() != "")
                        fine += Convert.ToDouble(GridDetail.Rows[i].Cells["colDefaultpenalty"].Value);
                    if (GridDetail.Rows[i].Cells["colPayment"].Value.ToString() != "")
                        payment += Convert.ToDouble(GridDetail.Rows[i].Cells["coldebtBalance"].Value);
                }
            }
            //lblAmountPayment.Text = (payment + fine).ToString("#,###,###,##0.00");
        }
        private void GridDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    inquiryInfoBeanList Info = GlobalClass.LstInfo.Where(c => c.ca == labelCa.Text).FirstOrDefault();
                    var Iddebt = GridDetail.Rows[e.RowIndex].Cells["ColDebtId"].Value.ToString();
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[e.RowIndex].Cells["ColCheckBoxDetail"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                        GridDetail.Rows[e.RowIndex].Cells[0].Value = true;
                    else
                        GridDetail.Rows[e.RowIndex].Cells[0].Value = false;
                    inquiryGroupDebtBeanList costTDebt = Info.inquiryGroupDebtBeanList.Find(itemDetail => itemDetail.debtId.ToString().Equals(Iddebt, StringComparison.CurrentCultureIgnoreCase));
                    costTDebt.status = newBool;
                    costTDebt.SelectCheck = newBool;
                    if (Info != null)
                    {
                        decimal _vat = Info.inquiryGroupDebtBeanList.Where(c => c.status && c.debtLock != "Y").Sum(c => Convert.ToDecimal(c.vatBalance));
                        decimal _defaultpenalty = Info.inquiryGroupDebtBeanList.Where(c => c.status && c.debtLock != "Y").Sum(c => c.Defaultpenalty);
                        decimal _payment = Info.inquiryGroupDebtBeanList.Where(c => c.status && c.debtLock != "Y").Sum(c => (c.Payment != 0) ? c.Payment : Convert.ToDecimal(c.debtBalance));
                        var count = Info.inquiryGroupDebtBeanList.Where(c => c.status && c.debtLock != "Y").ToList();
                        var countNew = Info.inquiryGroupDebtBeanList.ToList();
                        if (count.Count == countNew.Count)
                            CheckBoxSelect.Checked = true;
                        else
                            CheckBoxSelect.Checked = false;
                        if (count.Count <= 0)
                            arrears.UpdateStatusUserInfo(labelCa.Text, false, 0, _vat, _defaultpenalty.ToString());
                        else
                            arrears.UpdateStatusUserInfo(labelCa.Text, true, _payment, _vat, _defaultpenalty.ToString());
                        RefreshGridView();
                        DataGridViewDetail.Rows[0].Selected = false;
                        DataGridViewDetail.Rows[_rowIndex].Selected = true;
                    }
                    ShowCountAllDetail(Info.inquiryGroupDebtBeanList.ToList());
                }
                if (e.ColumnIndex == 16)
                {
                    string IdDebt = "";
                    IdDebt = GridDetail.Rows[e.RowIndex].Cells["ColDebtId"].Value.ToString();
                    DataGridViewCheckBoxCell check = (DataGridViewCheckBoxCell)GridDetail.Rows[e.RowIndex].Cells["colCkStatus"];
                    bool isCheck = (bool)check.EditedFormattedValue;
                    decimal penalty = Convert.ToDecimal(GridDetail.Rows[e.RowIndex].Cells["colDefaultpenalty"].Value.ToString());
                    if (!isCheck)
                    {
                        DeleteElectricityPenalty(IdDebt);
                        arrears.UpdateStatusDefaultpenalty(false, IdDebt, lblCaDetail.Text);
                        LoadData(labelCa.Text);
                        RefreshGridView();
                        _statusCkFin = true;
                    }
                    else
                        _statusCkFin = false;
                    //////////////////////
                    if (GlobalClass.ClsDataGridView == null)
                        GlobalClass.ClsDataGridView = new List<ClassDataGridView>();
                    ClassDataGridView clsData = GlobalClass.ClsDataGridView.Where(c => c.IdDebt == IdDebt).FirstOrDefault();
                    if (clsData != null)
                    {
                        clsData.Status = isCheck;
                        clsData.IdDebt = IdDebt;
                        clsData.Penalty = penalty;
                    }
                    else
                    {
                        ClassDataGridView item = new ClassDataGridView();
                        item.Status = isCheck;
                        item.IdDebt = IdDebt;
                        item.Penalty = penalty;
                        GlobalClass.ClsDataGridView.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void ElectricityPenalty(string idDebt, bool status)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ResultCancelElectricity result = new ResultCancelElectricity();
                result.Cancel = new List<ClassElectricityPenalty>();
                if (GlobalClass.CancelElectricity != null && GlobalClass.CancelElectricity.Count != 0)
                {
                    foreach (var item in GlobalClass.CancelElectricity)
                    {
                        ClassElectricityPenalty Cancel = new ClassElectricityPenalty();
                        Cancel.ca = item.ca;
                        Cancel.userId = item.userId;
                        Cancel.debtId = item.debtId;
                        Cancel.StatusDefaultpenalty = item.StatusDefaultpenalty;
                        Cancel.approveEmpId = item.approveEmpId;
                        result.Cancel.Add(Cancel);
                    }
                    ClassElectricityPenalty CancelDetail = new ClassElectricityPenalty();
                    CancelDetail.ca = labelCa.Text;
                    CancelDetail.userId = GlobalClass.UserId;
                    CancelDetail.debtId = idDebt;
                    CancelDetail.approveEmpId = GlobalClass.UserId;
                    CancelDetail.StatusDefaultpenalty = status;
                    result.Cancel.Add(CancelDetail);
                }
                else
                {
                    ClassElectricityPenalty Cancel = new ClassElectricityPenalty();
                    Cancel.ca = labelCa.Text;
                    Cancel.userId = GlobalClass.UserId;
                    Cancel.debtId = idDebt;
                    Cancel.StatusDefaultpenalty = status;
                    Cancel.approveEmpId = GlobalClass.UserId;
                    result.Cancel.Add(Cancel);
                }
                GlobalClass.CancelElectricity = result.Cancel;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public bool DeleteElectricityPenalty(string idDebt)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                if (GlobalClass.CancelElectricity != null)
                {
                    ClassElectricityPenalty resulRemove = GlobalClass.CancelElectricity.Where(c => c.debtId == idDebt).FirstOrDefault();
                    if (resulRemove != null)
                    {
                        GlobalClass.CancelElectricity.Remove(resulRemove);
                        tf = true;
                    }
                }
            }
            catch (Exception ex)
            {
                tf = false;
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return tf;
        }
        private void CheckBoxSelect_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                SelectCheckboxDetail();
                RefreshGridView();
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void comboBoxDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDetail.SelectedIndex == 1)
            {
                txtCount.Enabled = true;
                btOK.Enabled = true;
            }
            else if (comboBoxDetail.SelectedIndex == 2)
            {
                txtCount.Enabled = false;
                btOK.Enabled = true;
            }
            else
            {
                txtCount.Enabled = false;
                btOK.Enabled = false;
            }
        }
        private void btOK_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                List<inquiryInfoBeanList> lstInfo = new List<inquiryInfoBeanList>();
                int i = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    inquiryInfoBeanList info = new inquiryInfoBeanList();
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                    {
                        info.ca = DataGridViewDetail.Rows[i].Cells["ColCA"].Value.ToString();
                        lstInfo.Add(info);
                    }
                }
                arrears.UpdateCostTDebt(lstInfo, comboBoxDetail.SelectedIndex, txtCount.Text);
                int row = DataGridViewDetail.Rows.Count;
                DataGridViewDetail.Rows[0].Selected = true;
                RefreshGridView();
                LoadDataDetail(0, 0);
                txtCount.Text = string.Empty;
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void LoadDataDetail(int status, int rows)
        {
            string Custtype;
            string fk, ca, address, ui, branch, taxId, bill, receiptName;
            if (status == 0)
            {
                int row;
                if (rows == 0)
                    row = 0;
                else
                    row = DataGridViewDetail.Rows.Count - 1;
                rows = row;
                fk = (DataGridViewDetail.Rows[row].Cells["ColCustomerName"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColCustomerName"].Value.ToString() : "";
                ca = DataGridViewDetail.Rows[row].Cells["ColCA"].Value.ToString();
                address = (DataGridViewDetail.Rows[row].Cells["ColAddress"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColAddress"].Value.ToString() : "";
                ui = (DataGridViewDetail.Rows[row].Cells["ColUI"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColUI"].Value.ToString() : "";
                branch = (DataGridViewDetail.Rows[row].Cells["ColTaxBranch"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColTaxBranch"].Value.ToString() : "";
                taxId = (DataGridViewDetail.Rows[row].Cells["ColTax20"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColTax20"].Value.ToString() : "";
                bill = (DataGridViewDetail.Rows[row].Cells["collId"].Value != null) ? DataGridViewDetail.Rows[row].Cells["collId"].Value.ToString() : "";
                receiptName = (DataGridViewDetail.Rows[row].Cells["ColCusName"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColCusName"].Value.ToString() : "";
                labelFk.Invoke(new MethodInvoker(delegate { labelFk.Text = fk; }));
                labelCa.Invoke(new MethodInvoker(delegate { labelCa.Text = ca; }));
                labelAddress.Invoke(new MethodInvoker(delegate { labelAddress.Text = address; }));
                labelUi.Invoke(new MethodInvoker(delegate { labelUi.Text = ui; }));
                labelBranch.Invoke(new MethodInvoker(delegate { labelBranch.Text = branch; }));
                labelTaxId.Invoke(new MethodInvoker(delegate { labelTaxId.Text = taxId; }));
                labelBill.Invoke(new MethodInvoker(delegate { labelBill.Text = bill; }));
                Custtype = (DataGridViewDetail.Rows[row].Cells["ColType"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColType"].Value.ToString() : "";
                lblReceiptName.Invoke(new MethodInvoker(delegate { lblReceiptName.Text = receiptName; }));
                if (Convert.ToBoolean(DataGridViewDetail.Rows[row].Cells["colStatus"].Value) == true)
                    lblStatusC.Invoke(new MethodInvoker(delegate { lblStatusC.Visible = true; }));
                else
                    lblStatusC.Invoke(new MethodInvoker(delegate { lblStatusC.Visible = false; }));
            }
            else if (status == 1)
            {
                fk = (DataGridViewDetail.CurrentRow.Cells["ColCustomerName"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColCustomerName"].Value.ToString() : "";
                ca = (DataGridViewDetail.CurrentRow.Cells["ColCA"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColCA"].Value.ToString() : "";
                address = (DataGridViewDetail.CurrentRow.Cells["ColAddress"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColAddress"].Value.ToString() : "";
                ui = (DataGridViewDetail.CurrentRow.Cells["ColUI"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColUI"].Value.ToString() : "";
                branch = (DataGridViewDetail.CurrentRow.Cells["ColTaxBranch"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColTaxBranch"].Value.ToString() : "";
                taxId = (DataGridViewDetail.CurrentRow.Cells["ColTax20"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColTax20"].Value.ToString() : "";
                bill = (DataGridViewDetail.CurrentRow.Cells["collId"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["collId"].Value.ToString() : "";
                receiptName = (DataGridViewDetail.CurrentRow.Cells["ColCusName"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColCusName"].Value.ToString() : "";
                labelFk.Invoke(new MethodInvoker(delegate { labelFk.Text = fk; }));
                labelCa.Invoke(new MethodInvoker(delegate { labelCa.Text = ca; }));
                labelAddress.Invoke(new MethodInvoker(delegate { labelAddress.Text = address; }));
                labelUi.Invoke(new MethodInvoker(delegate { labelUi.Text = ui; }));
                labelBranch.Invoke(new MethodInvoker(delegate { labelBranch.Text = branch; }));
                labelTaxId.Invoke(new MethodInvoker(delegate { labelTaxId.Text = taxId; }));
                labelBill.Invoke(new MethodInvoker(delegate { labelBill.Text = bill; }));
                lblReceiptName.Invoke(new MethodInvoker(delegate { lblReceiptName.Text = receiptName; }));
                Custtype = (DataGridViewDetail.CurrentRow.Cells["ColType"].Value != null) ? DataGridViewDetail.CurrentRow.Cells["ColType"].Value.ToString() : "";
                if (Convert.ToBoolean(DataGridViewDetail.CurrentRow.Cells["colStatus"].Value) == true)
                    lblStatusC.Invoke(new MethodInvoker(delegate { lblStatusC.Visible = true; }));
                else
                    lblStatusC.Invoke(new MethodInvoker(delegate { lblStatusC.Visible = false; }));
            }
            else
            {
                int row;
                row = rows;
                fk = (DataGridViewDetail.Rows[row].Cells["ColCustomerName"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColCustomerName"].Value.ToString() : "";
                ca = DataGridViewDetail.Rows[row].Cells["ColCA"].Value.ToString();
                address = (DataGridViewDetail.Rows[row].Cells["ColAddress"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColAddress"].Value.ToString() : "";
                ui = (DataGridViewDetail.Rows[row].Cells["ColUI"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColUI"].Value.ToString() : "";
                branch = (DataGridViewDetail.Rows[row].Cells["ColTaxBranch"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColTaxBranch"].Value.ToString() : "";
                taxId = (DataGridViewDetail.Rows[row].Cells["ColTax20"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColTax20"].Value.ToString() : "";
                bill = (DataGridViewDetail.Rows[row].Cells["collId"].Value != null) ? DataGridViewDetail.Rows[row].Cells["collId"].Value.ToString() : "";
                receiptName = (DataGridViewDetail.Rows[row].Cells["ColCusName"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColCusName"].Value.ToString() : "";
                labelFk.Invoke(new MethodInvoker(delegate { labelFk.Text = fk; }));
                labelCa.Invoke(new MethodInvoker(delegate { labelCa.Text = ca; }));
                labelAddress.Invoke(new MethodInvoker(delegate { labelAddress.Text = address; }));
                labelUi.Invoke(new MethodInvoker(delegate { labelUi.Text = ui; }));
                labelBranch.Invoke(new MethodInvoker(delegate { labelBranch.Text = branch; }));
                labelTaxId.Invoke(new MethodInvoker(delegate { labelTaxId.Text = taxId; }));
                labelBill.Invoke(new MethodInvoker(delegate { labelBill.Text = bill; }));
                Custtype = (DataGridViewDetail.Rows[row].Cells["ColType"].Value != null) ? DataGridViewDetail.Rows[row].Cells["ColType"].Value.ToString() : "";
                lblReceiptName.Invoke(new MethodInvoker(delegate { lblReceiptName.Text = receiptName; }));
                if (Convert.ToBoolean(DataGridViewDetail.Rows[row].Cells["colStatus"].Value) == true)
                    lblStatusC.Invoke(new MethodInvoker(delegate { lblStatusC.Visible = true; }));
                else
                    lblStatusC.Invoke(new MethodInvoker(delegate { lblStatusC.Visible = false; }));
            }
            _custtype = Custtype;
            LoadData(labelCa.Text);
            SumAmount(status, rows);
            SetProgressBar(false);
            TextBoxBarcode.Invoke(new MethodInvoker(delegate { TextBoxBarcode.Focus(); }));
        }
        private void txtCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void DataGridViewDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                _rowIndex = e.RowIndex;
                if (e.ColumnIndex == 1)
                    LoadDataDetail(1, 0);
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormArrearsNew | Function :: DataGridViewDetail_CellContentDoubleClick()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void buttonZ1_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (GridDetail.Rows.Count > 0)
                {
                    if (MessageBox.Show("คุณต้องการยกเลิกค่าดำเนินการงดจ่ายไฟ", "ยกเลิก", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    FormCheckUser checkUser = new FormCheckUser("ยืนยันการยกเลิกค่าดำเนินการงดจ่ายไฟ");
                    checkUser.ShowDialog();
                    checkUser.Dispose();
                    if (GlobalClass.StatusForm == 1)
                    {
                        if (GlobalClass.AuthorizeLevel == 2)
                        {
                            decimal vat = 0;
                            decimal payment = 0;
                            decimal fine = 0;
                            decimal balance = 0;
                            string IdDebt = "";
                            string debtIdSet = "";
                            int i = 0;
                            int countCheck = 0;
                            string hdrId;
                            var loopTo = GridDetail.Rows.Count - 1;
                            for (i = 0; i <= loopTo; i++)
                            {
                                if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                                {
                                    string DebtType = GridDetail.Rows[i].Cells["ColdebtType"].Value.ToString();
                                    IdDebt = GridDetail.Rows[i].Cells["ColDebtId"].Value.ToString();
                                    debtIdSet = GridDetail.Rows[i].Cells["ColdebtIdSet"].Value.ToString();
                                    hdrId = GridDetail.Rows[i].Cells["colInsDebtInsHdrId"].Value.ToString();
                                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[i].Cells["ColCheckBoxDetail"];
                                    bool newBool = (bool)checkCell.EditedFormattedValue;
                                    if (newBool)
                                    {
                                        if (DebtType == "DIS")
                                        {
                                            if (GlobalClass.CancelProcessing == null)
                                                GlobalClass.CancelProcessing = new List<ClassCancelProcessingFee>();
                                            ClassCancelProcessingFee Cancel = new ClassCancelProcessingFee();
                                            Cancel.userId = GlobalClass.UserIdCancel;
                                            Cancel.debtId = debtIdSet;
                                            GlobalClass.CancelProcessing.Add(Cancel);
                                            GridDetail.Rows[i].Cells[0].Value = false;
                                            arrears.UpdateStatus(IdDebt, false, 0, labelCa.Text, hdrId);
                                        }
                                        else
                                        {
                                            if (GridDetail.Rows[i].Cells["colDefaultpenalty"].Value.ToString() != "")
                                                fine += Convert.ToDecimal(GridDetail.Rows[i].Cells["colDefaultpenalty"].Value);
                                            if (GridDetail.Rows[i].Cells["colVatDetail"].Value.ToString() != "")
                                                vat += Convert.ToDecimal(GridDetail.Rows[i].Cells["colVatDetail"].Value);
                                            if (GridDetail.Rows[i].Cells["colPayment"].Value.ToString() != "")
                                            {
                                                payment += Convert.ToDecimal(GridDetail.Rows[i].Cells["colPayment"].Value);
                                                balance = Convert.ToDecimal(GridDetail.Rows[i].Cells["colPayment"].Value.ToString());
                                            }
                                            arrears.UpdateStatus(IdDebt, true, balance, labelCa.Text, hdrId);
                                            countCheck++;
                                        }
                                    }
                                }
                            }
                            if (countCheck <= 0)
                                arrears.UpdateStatusUserInfo(labelCa.Text, false, 0, vat, fine.ToString());
                            else
                                arrears.UpdateStatusUserInfo(labelCa.Text, true, payment, vat, fine.ToString());
                            RefreshGridView();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MsgBox.Show(this, "เกิดข้อผิดพลาดการยกเลิกค่าดำเนินการงดจ่ายไฟ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void GetDataWaterbill()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                #region
                string TaxId, CheckType, CusCode = "", DueDate, BillNumber, Unit, AmountBill, mwaTaxId, customerCode, billDueDate, billNumber, petVat;
                string BarCode = TextBoxBarcode.Text;
                string BarCodeReplace = BarCode.Replace("◙", "");
                string barcodeTrim = BarCodeReplace.Replace(" ", "");
                CheckType = barcodeTrim.Substring(16, 1);
                if (CheckType != "7")
                {
                    DueDate = barcodeTrim.Substring(34, 6);
                    string year = DueDate.Substring(4);
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
                    DateTime dt = DateTime.Now;
                    DateTime dtNow = DateTime.Now;
                    if (dt.Year < 2500)
                        dtNow = dt.AddYears(543);
                    string newyear = dtNow.Year.ToString().Substring(0, 2);
                    string strDate = DueDate.Substring(0, 2) + "/" + DueDate.Substring(2, 2) + "/" + newyear + year;
                    DateTime DateNew = Convert.ToDateTime(strDate);
                    if (dt <= DateNew)
                    {
                        TaxId = barcodeTrim.Substring(1, 13);
                        mwaTaxId = barcodeTrim.Substring(1, 15);
                        customerCode = barcodeTrim.Substring(16, 18);
                        billDueDate = barcodeTrim.Substring(34, 15);
                        billNumber = barcodeTrim.Substring(49);
                        CusCode = barcodeTrim.Substring(18, 9);
                        BillNumber = barcodeTrim.Substring(27, 7);
                        petVat = barcodeTrim.Substring(40, 2);
                        Unit = barcodeTrim.Substring(42, 7);
                        AmountBill = barcodeTrim.Substring(49);
                        string perVat = "1." + petVat;
                        string Righ = SubstringRight(AmountBill, 2);
                        string Left = SubstringLeft(AmountBill, AmountBill.Length - 2);
                        AmountBill = Left + "." + Righ;
                        decimal vat = Convert.ToDecimal(AmountBill) - (Convert.ToDecimal(AmountBill) / Convert.ToDecimal(perVat));
                        ClassItemWater wt = new ClassItemWater();
                        wt.customerName = CusCode;
                        wt.taxId = TaxId;
                        wt.debtIdSet = new Int64[1] { 1 };
                        wt.Amount = Math.Round(Convert.ToDecimal(AmountBill) - vat, 2).ToString();
                        wt.vatType = petVat;
                        wt.VatAmount = Math.Round(vat, 2);
                        wt.totalAmount = Convert.ToDecimal(AmountBill);
                        wt.meaUnit = Unit;
                        wt.mwaTaxId = mwaTaxId;
                        wt.customerCode = customerCode;
                        wt.billDueDate = billDueDate;
                        wt.billNumber = billNumber;
                        wt.check = CheckType;
                        wt.dueDate = DueDate;
                        resul = arrears.addDataWater(wt);
                    }
                    else
                    {
                        MsgBox.Show(this, "ไม่สามารถทำการรับชำระได้ เนื่องจาก วันที่เกินกำหนด", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                        //MessageBox.Show("ไม่สามารถทำการรับชำระได้ เนื่องจาก วันที่เกินกำหนด", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    MsgBox.Show(this, "ไม่สามารถทำการรับชำระได้ เนื่องจาก ยังมีหนี้เก่าค้างชำระ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true);
                    //MessageBox.Show("ไม่สามารถทำการรับชำระได้ เนื่องจาก ยังมีหนี้เก่าค้างชำระ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                #endregion
                ArrearsController arr = new ArrearsController();
                TextBoxBarcode.Text = CusCode;

                //if (resul.Output != null && resul.Output.Count != 0)
                //    lblCountDetail.Text = resul.Output.Count.ToString();

                SetControl();
                this.DataGridViewDetail.DataSource = GlobalClass.LstInfo;
                double Amount = 0;
                double TotalVat = 0;
                double Fine = 0;
                int i = 0;
                int countCheck = 0;
                var loopTo = DataGridViewDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (Convert.ToBoolean(DataGridViewDetail.Rows[i].Cells["ColckCancel"].Value) == true)
                        DataGridViewDetail.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 154, 154);
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)DataGridViewDetail.Rows[i].Cells["ColCheckBox"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                    {
                        Amount += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColTotalAmount"].Value);
                        TotalVat += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColVat"].Value);
                        Fine += Convert.ToDouble(DataGridViewDetail.Rows[i].Cells["ColPenaltyAll"].Value);
                        DataGridViewDetail.Rows[i].Cells[0].Value = true;
                        countCheck++;
                    }
                    else
                        DataGridViewDetail.Rows[i].Cells[0].Value = false;
                }
                if (countCheck == DataGridViewDetail.Rows.Count)
                    CheckBoxSelectAll.Checked = true;
                else
                    CheckBoxSelectAll.Checked = false;
                //lblAmountPayment.Text = countCheck.ToString();
                txtAmount.Text = (Amount - TotalVat).ToString("#,###,###,##0.00");
                txtVat.Text = TotalVat.ToString("#,###,##0.00");
                txtTotal.Text = Amount.ToString("#,###,###,##0.00");
                txtFine.Text = Fine.ToString("#,###,###,##0.00");
                txtTotalSum.Text = (Amount + Fine).ToString("#,###,###,##0.00");
                ButtonRec.Focus();
                int row = DataGridViewDetail.Rows.Count;
                if (row > 0)
                {
                    DataGridViewDetail.Rows[0].Selected = false;
                    DataGridViewDetail.Rows[row - 1].Selected = true;
                    LoadDataDetail(0, 1);
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormArrearsNew | Function :: DataGridViewDetail_CellContentDoubleClick()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public static string SubstringLeft(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }
        public static string SubstringRight(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }
        private void timer3_Tick(object sender, EventArgs e)
        {
            string str = "";
            string value = "";
            this.Invoke(new MethodInvoker(delegate { value = ComboBoxDocType.SelectedItem.ToString(); }));
            str = "เลข " + value + " " + TextBoxBarcode.Text.Trim() + " ไม่ถูกต้อง";
            timer3.Enabled = false;
            TextBoxBarcode.Invoke(new MethodInvoker(delegate { TextBoxBarcode.Text = string.Empty; }));
            TextBoxBarcode.Invoke(new MethodInvoker(delegate { TextBoxBarcode.Focus(); }));
            //MsgBox.Show(this, str, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true); 
            this.Invoke(new MethodInvoker(delegate { MessageBox.Show(str, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }));
        }
        public void DeleteListInfo()
        {
            #region deleteDataList
            List<inquiryInfoBeanList> resulDelete = GlobalClass.LstInfo.Where(c => c.inquiryGroupDebtBeanList == null || c.inquiryGroupDebtBeanList.Count == 0).ToList();
            foreach (var itemDelete in resulDelete)
            {
                GlobalClass.LstInfo.Remove(itemDelete);
            }
            #endregion
        }
        private void GridDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }
        private void btCancelFine_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (GridDetail.Rows.Count > 0)
                {
                    bool statusCk = false;
                    int j = 0;
                    var loopJ = GridDetail.Rows.Count - 1;
                    for (j = 0; j <= loopJ; j++)
                    {
                        if (GridDetail.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridDetail.Rows[j].Cells["colCkStatus"];
                            bool isCheck = (bool)checkCell.EditedFormattedValue;
                            decimal penalty = Convert.ToDecimal(GridDetail.Rows[j].Cells["colDefaultpenalty"].Value.ToString());
                            if (isCheck && penalty > 0)
                                statusCk = true;
                        }
                    }
                    if (statusCk)
                    {
                        DialogResultNew dialog = MsgBox.Show(this, "คุณต้องการยกเลิกเบี้ยปรับ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true,1);
                        if(dialog.Result == DialogResult.No)//if (MessageBox.Show("คุณต้องการยกเลิกเบี้ยปรับ", "ยกเลิก", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;
                        FormCheckUser checkUser = new FormCheckUser("ยืนยันการยกเลิกเบี้ยปรับ");
                        checkUser.ShowDialog();
                        checkUser.Dispose();
                        if (GlobalClass.StatusForm == 1)
                        {
                            if (GlobalClass.AuthorizeLevel == 2)
                            {
                                if (GlobalClass.ClsDataGridView.Count > 0)
                                {
                                    foreach (ClassDataGridView item in GlobalClass.ClsDataGridView)
                                    {
                                        if (item.Status)
                                        {
                                            if (item.Penalty > 0)
                                            {
                                                ElectricityPenalty(item.IdDebt, item.Status);
                                                arrears.UpdateStatusDefaultpenalty(true, item.IdDebt, lblCaDetail.Text);
                                                LoadData(labelCa.Text);
                                                RefreshGridView();
                                            }
                                        }
                                    }
                                }
                                _statusCkFin = true;
                            }
                        }
                    }
                    else
                        MsgBox.Show(this, "ไม่มีรายการที่คิดเบี้ยปรับ", "", MessageBoxButtons.OK, MessageBoxIcon.Warning, true, new Font("TH Sarabun New", 22, FontStyle.Bold), Color.Black, true,1);
                    //MessageBox.Show("ไม่มีรายการที่คิดเบี้ยปรับ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void GridDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (GridDetail.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == DBNull.Value)
                e.Cancel = true;
        }
        private void TextBoxBarcode_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                System.Reflection.MethodBase method;
                method = System.Reflection.MethodBase.GetCurrentMethod();
                try
                {
                    buttonZ1.Enabled = false;
                    buttonZ1.Cursor = Cursors.No;
                    if (ComboBoxDocType.Text == "บัญชีแสดงสัญญา")
                        ComboBoxDocType.SelectedIndex = 0;
                    int select = ComboBoxDocType.SelectedIndex;
                    if (select == 6)
                        GetDataWaterbill();
                    else
                    {
                        _selectIndex = select;
                        if (select == 0)
                        {
                            int lengthBarcode = TextBoxBarcode.Text.Trim().Length;
                            if (lengthBarcode == 9 || lengthBarcode == 8)
                            {
                                timer3.Enabled = false;
                                this.Invoke(new MethodInvoker(delegate { _barcode = TextBoxBarcode.Text.Trim(); }));
                                threadData = new Thread(new ThreadStart(GetDataToGridview));
                                threadData.Start();
                            }
                            else if (lengthBarcode > 55)
                            {
                                timer3.Enabled = false;
                                string strData = TextBoxBarcode.Text.Substring(16, 9);
                                _barcode = strData;
                                threadData = new Thread(new ThreadStart(GetDataToGridview));
                                threadData.Start();
                            }
                            else
                                timer3.Enabled = true;
                        }
                        else
                        {
                            this.Invoke(new MethodInvoker(delegate { _barcode = TextBoxBarcode.Text.Trim(); }));
                            threadData = new Thread(new ThreadStart(GetDataToGridview));
                            threadData.Start();
                        }
                        if (!StatusPanel)
                            SetPanel(false);
                    }
                }
                catch (Exception ex)
                {
                    SetProgressBar(false);
                    meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                }
            }
        }
        private void GridDetail_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == _previousIndex)
                _sortDirection ^= true; // toggle direction
            GridDetail.DataSource = SortData(
                (List<inquiryGroupDebtBeanList>)GridDetail.DataSource, GridDetail.Columns[e.ColumnIndex].DataPropertyName, _sortDirection);
            _previousIndex = e.ColumnIndex;
        }
        public List<inquiryGroupDebtBeanList> SortData(List<inquiryGroupDebtBeanList> list, string column, bool ascending)
        {
            return ascending ?
                list.OrderBy(_ => _.GetType().GetProperty(column).GetValue(_, null)).ToList() :
                list.OrderByDescending(_ => _.GetType().GetProperty(column).GetValue(_, null)).ToList();
        }
    }
}
