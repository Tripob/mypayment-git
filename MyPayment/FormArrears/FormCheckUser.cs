﻿using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using static MyPayment.Models.ClassLogsService;
using MyPayment.Controllers;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.FormArrears
{
    public partial class FormCheckUser : Form
    {
        ClassLogsService meaLog = new ClassLogsService("EventLog", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("EventLog", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("ErrorLog", "ErrorLog");
        ArrearsController arrears = new ArrearsController();
        private string strTemp;
        public FormCheckUser(string name="ยืนยัน")
        {
            InitializeComponent();
            this.Text = name;
        }
        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (TextboxIdUser.Text == string.Empty || TextboxPass.Text == string.Empty || TextBoxRemark.Text == string.Empty)
                {
                    string mes;
                    if (TextboxIdUser.Text == string.Empty)
                    {
                        TextboxIdUser.Focus();
                        mes = "กรุณากรอก รหัสผู้ใช้งาน";
                    }
                    else if(TextboxPass.Text == string.Empty)
                    {
                        TextboxPass.Focus();
                        mes = "กรุณากรอก รหัสผ่าน";
                    }
                    else
                    {
                        TextBoxRemark.Focus();
                        mes = "กรุณากรอก หมายเหตุ";
                    }
                    MessageBox.Show(mes, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    GlobalClass.Url = ConfigurationManager.AppSettings["UAT"].ToString();
                    ClassLogin clsuserNew = new ClassLogin();
                    clsuserNew.empId = TextboxIdUser.Text.Trim();
                    clsuserNew.password = TextboxPass.Text.Trim();
                    clsuserNew.ipAddress = GlobalClass.IpConfig;
                    ClassUser clsLogin = arrears.loadDataLogin(clsuserNew, "loginPrivilege");
                    strTemp = " | " + TextboxIdUser.Text;
                    if (clsLogin.result_code == "SUCCESS")
                    {
                        if (clsLogin.authorizeLevel == 2)
                        {
                            GlobalClass.AuthorizeLevel = clsLogin.authorizeLevel;
                            GlobalClass.UserIdCancel = Convert.ToInt32(clsLogin.empId);
                            GlobalClass.CanceluserId = Convert.ToInt32(clsLogin.userId);
                            GlobalClass.Remark = TextBoxRemark.Text.Trim() != "" ? TextBoxRemark.Text.Trim() : "";
                            GlobalClass.StatusForm = 1;
                            meaLog.WriteData("Success ==> User :: " + strTemp, method, LogLevel.Debug);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("User ที่สามารถยกเลิกใบเสร็จรับเงินได้ต้องเป็น Level 2 เท่านั้น", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            ClearData();
                            TextboxIdUser.Focus();
                        }
                    }
                    else
                    {
                        meaLogEvent.WriteDataEvent("Fail ==> User :: " + strTemp + " | Message :: " + clsLogin.result_message, method, LogLevel.Debug);
                        MessageBox.Show(clsLogin.result_message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.ToString(), method, LogLevel.Error);
                MessageBox.Show("เกิดข้อผิดพลาดในการยืนยันข้อมูล ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                ClearData();
            }            
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            GlobalClass.StatusForm = 0;
            this.Close();
        }
        private void TextboxIdUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                if (TextboxIdUser.Text == string.Empty)
                    TextboxIdUser.Focus();
                else
                    TextboxPass.Focus();
                if (TextboxPass.Text != string.Empty && TextboxIdUser.Text != string.Empty)
                    ButtonLogin_Click(sender, e);
            }
        }
        private void TextboxPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                if (TextboxPass.Text == string.Empty)
                    TextboxPass.Focus();
                if (TextboxPass.Text != string.Empty && TextboxIdUser.Text != string.Empty)
                    ButtonLogin_Click(sender, e);
            }
        }
        public void ClearData()
        {
            TextboxIdUser.Text = string.Empty;
            TextboxPass.Text = string.Empty;
            TextBoxRemark.Text = string.Empty;
        }      
    }
}
