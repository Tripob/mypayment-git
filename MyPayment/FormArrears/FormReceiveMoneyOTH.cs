﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormArrears
{
    public partial class FormReceiveMoneyOTH : Form
    {
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ClassLogsService ReportErrorLog = new ClassLogsService("ReportErrorLog", "ErrorLog");
        ClassLogsService ReceiveMoneyLog = new ClassLogsService("ReceiveMoney", "DataLog");
        ClassLogsService ReceiveMoneyEventLog = new ClassLogsService("ReceiveMoney", "EventLog");
        ClassLogsService ReceiveMoneyErrorLog = new ClassLogsService("ReceiveMoney", "ErrorLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        public CultureInfo usCulture = new CultureInfo("en-US");
        public CultureInfo _usCultureTh = new CultureInfo("th-TH");
        public string CustomerName;
        public double Countamount;
        public double CountamountAll;
        ResultCostTDebt resul = new ResultCostTDebt();
        ArrearsController arr = new ArrearsController();
        List<ClassCheque> _cheque = new List<ClassCheque>();
        List<ClassListGridview> _lstGrid = new List<ClassListGridview>();
        public byte Key_Error;
        public string qNo;
        public bool _StatusSendData;
        public string GB_StrResult = "";
        public FormReceiveMoneyOTH()
        {
            InitializeComponent();
        }
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double notenoughamount = Convert.ToDouble(label25.Text);
                if (notenoughamount >= 0)
                {
                    SelectPayment();

                    ///-----        
                    ///
                   // _StatusSendData = true;
                    ///------------------------

                    if (_StatusSendData)
                    {



                        /*************************************************************************/
                       // string strResult = "{\"result_code\":\"SUCCESS\",\"result_message\":\"SUCCESS\",\"version_control\":\"0.2\",\"paymentResponseDtoList\":[{\"paymentId\":16008619,\"payTReceiptList\":[{\"receiptId\":16012101,\"receiptNo\":\"M3116305000073\",\"receiptTypeId\":1,\"receiptDebtType\":1,\"receiptDate\":\"2020-05-12\",\"receiptRefNo\":\"606305000080\",\"receiptPayer\":\"\u0e19\u0e32\u0e07\u0e2a\u0e32\u0e27 \u0e44\u0e0a\u0e22\u0e40\u0e0a\u0e29\u0e10\u0e4c \u0e40\u0e15\u0e0a\u0e30\u0e20\u0e31\u0e17\u0e23\u0e23\u0e38\u0e48\u0e07\u0e40\u0e23\u0e37\u0e2d\u0e07\",\"receiptPayerAddress\":\"25-55 \u0e21.\u0e0a\u0e31\u0e22\u0e18\u0e19\u0e32\u0e2f \u0e16.\u0e09\u0e25\u0e2d\u0e07\u0e01\u0e23\u0e38\u0e07 \u0e41\u0e02\u0e27\u0e07\u0e25\u0e33\u0e1c\u0e31\u0e01\u0e0a\u0e35  \u0e40\u0e02\u0e15\u0e2b\u0e19\u0e2d\u0e07\u0e08\u0e2d\u0e01 \u0e08.\u0e01\u0e23\u0e38\u0e07\u0e40\u0e17\u0e1e\u0e21\u0e2b\u0e32\u0e19\u0e04\u0e23 10530\",\"receiptPayerTaxid\":null,\"receiptPayerBranch\":null,\"distId\":6001,\"distName\":\"\u0e40\u0e02\u0e15\u0e25\u0e32\u0e14\u0e01\u0e23\u0e30\u0e1a\u0e31\u0e07 \u0e2a\u0e32\u0e02\u0e32\u0e17\u0e35\u0e48 00031\",\"receiptCustId\":100173992,\"receiptCustName\":\"\u0e19\u0e32\u0e07\u0e2a\u0e32\u0e27 \u0e44\u0e0a\u0e22\u0e40\u0e0a\u0e29\u0e10\u0e4c \u0e40\u0e15\u0e0a\u0e30\u0e20\u0e31\u0e17\u0e23\u0e23\u0e38\u0e48\u0e07\u0e40\u0e23\u0e37\u0e2d\u0e07\",\"receiptCustTaxid\":null,\"receiptCustBranch\":null,\"receiptReqAddress\":\"25-55 \u0e21.\u0e0a\u0e31\u0e22\u0e18\u0e19\u0e32\u0e2f \u0e16.\u0e09\u0e25\u0e2d\u0e07\u0e01\u0e23\u0e38\u0e07 \u0e41\u0e02\u0e27\u0e07\u0e25\u0e33\u0e1c\u0e31\u0e01\u0e0a\u0e35  \u0e40\u0e02\u0e15\u0e2b\u0e19\u0e2d\u0e07\u0e08\u0e2d\u0e01 \u0e08.\u0e01\u0e23\u0e38\u0e07\u0e40\u0e17\u0e1e\u0e21\u0e2b\u0e32\u0e19\u0e04\u0e23 10530\",\"receiptCa\":111018145,\"receiptUi\":47120873,\"receiptAmount\":2201.87,\"receiptVat\":154.13,\"receiptTotalAmount\":2356,\"receiptIntAmount\":0,\"receiptIntTotalAmount\":2356,\"cashierNo\":\"68\",\"cashierId\":\"97602202\",\"cashierName\":\"\u0e04\u0e38\u0e13\u0e40\u0e08\u0e49\u0e32\u0e2b\u0e19\u0e49\u0e32\u0e17\u0e35\u0e48 \u0e1a\u0e232 \u0e01\u0e25\u0e38\u0e48\u0e21\u0e07\u0e32\u0e19\u0e1e\u0e34\u0e08\u0e32\u0e23\u0e13\u0e32\u0e04\u0e33\u0e23\u0e49\u0e2d\u0e07\",\"receiptPercentVat\":7,\"subDistId\":6001,\"countCa\":null,\"collId\":null,\"countInv\":null,\"responseReceiptDtlBeanList\":[{\"ITEM_PRICE_WITH_VAT\":100,\"receiptDtlNo\":\"63300000014624\",\"receiptDtlName\":\"\u0e04\u0e48\u0e32\u0e1a\u0e23\u0e34\u0e01\u0e32\u0e23\u0e02\u0e2d\u0e43\u0e0a\u0e49\u0e44\u0e1f\u0e1f\u0e49\u0e32 \u0e02\u0e19\u0e32\u0e14\u0e40\u0e04\u0e23\u0e37\u0e48\u0e2d\u0e07\u0e27\u0e31\u0e14\u0e2f 30(100)A 230-400V 3P 4W\",\"receiptDtlAmount\":2201.87,\"receiptDtlVat\":154.13,\"percentVat\":null,\"receiptDtlTotalAmount\":2356,\"lastBillingDate\":\"\",\"receiptDtlUnit\":0,\"intAmount\":0,\"intDay\":0,\"ft\":null}],\"debtType\":\"SER\",\"paymentTypeId\":1},{\"receiptId\":16012102,\"receiptNo\":\"M316305000016\",\"receiptTypeId\":2,\"receiptDebtType\":2,\"receiptDate\":\"2020-05-12\",\"receiptRefNo\":\"606305000080\",\"receiptPayer\":\"\u0e19\u0e32\u0e07\u0e2a\u0e32\u0e27 \u0e44\u0e0a\u0e22\u0e40\u0e0a\u0e29\u0e10\u0e4c \u0e40\u0e15\u0e0a\u0e30\u0e20\u0e31\u0e17\u0e23\u0e23\u0e38\u0e48\u0e07\u0e40\u0e23\u0e37\u0e2d\u0e07\",\"receiptPayerAddress\":\"25-55 \u0e21.\u0e0a\u0e31\u0e22\u0e18\u0e19\u0e32\u0e2f \u0e16.\u0e09\u0e25\u0e2d\u0e07\u0e01\u0e23\u0e38\u0e07 \u0e41\u0e02\u0e27\u0e07\u0e25\u0e33\u0e1c\u0e31\u0e01\u0e0a\u0e35  \u0e40\u0e02\u0e15\u0e2b\u0e19\u0e2d\u0e07\u0e08\u0e2d\u0e01 \u0e08.\u0e01\u0e23\u0e38\u0e07\u0e40\u0e17\u0e1e\u0e21\u0e2b\u0e32\u0e19\u0e04\u0e23 10530\",\"receiptPayerTaxid\":null,\"receiptPayerBranch\":null,\"distId\":6001,\"distName\":\"\u0e40\u0e02\u0e15\u0e25\u0e32\u0e14\u0e01\u0e23\u0e30\u0e1a\u0e31\u0e07 \u0e2a\u0e32\u0e02\u0e32\u0e17\u0e35\u0e48 00031\",\"receiptCustId\":100173992,\"receiptCustName\":\"\u0e19\u0e32\u0e07\u0e2a\u0e32\u0e27 \u0e44\u0e0a\u0e22\u0e40\u0e0a\u0e29\u0e10\u0e4c \u0e40\u0e15\u0e0a\u0e30\u0e20\u0e31\u0e17\u0e23\u0e23\u0e38\u0e48\u0e07\u0e40\u0e23\u0e37\u0e2d\u0e07\",\"receiptCustTaxid\":null,\"receiptCustBranch\":null,\"receiptReqAddress\":\"25-55 \u0e21.\u0e0a\u0e31\u0e22\u0e18\u0e19\u0e32\u0e2f \u0e16.\u0e09\u0e25\u0e2d\u0e07\u0e01\u0e23\u0e38\u0e07 \u0e41\u0e02\u0e27\u0e07\u0e25\u0e33\u0e1c\u0e31\u0e01\u0e0a\u0e35  \u0e40\u0e02\u0e15\u0e2b\u0e19\u0e2d\u0e07\u0e08\u0e2d\u0e01 \u0e08.\u0e01\u0e23\u0e38\u0e07\u0e40\u0e17\u0e1e\u0e21\u0e2b\u0e32\u0e19\u0e04\u0e23 10530\",\"receiptCa\":111018145,\"receiptUi\":47120873,\"receiptAmount\":12000,\"receiptVat\":0,\"receiptTotalAmount\":12000,\"receiptIntAmount\":0,\"receiptIntTotalAmount\":12000,\"cashierNo\":\"68\",\"cashierId\":\"97602202\",\"cashierName\":\"\u0e04\u0e38\u0e13\u0e40\u0e08\u0e49\u0e32\u0e2b\u0e19\u0e49\u0e32\u0e17\u0e35\u0e48 \u0e1a\u0e232 \u0e01\u0e25\u0e38\u0e48\u0e21\u0e07\u0e32\u0e19\u0e1e\u0e34\u0e08\u0e32\u0e23\u0e13\u0e32\u0e04\u0e33\u0e23\u0e49\u0e2d\u0e07\",\"receiptPercentVat\":0,\"subDistId\":6001,\"countCa\":null,\"collId\":null,\"countInv\":null,\"responseReceiptDtlBeanList\":[{\"ITEM_PRICE_WITH_VAT\":100,\"receiptDtlNo\":\"2000006252\",\"receiptDtlName\":\"\u0e04\u0e48\u0e32\u0e2b\u0e25\u0e31\u0e01\u0e1b\u0e23\u0e30\u0e01\u0e31\u0e19 \",\"receiptDtlAmount\":12000,\"receiptDtlVat\":0,\"percentVat\":null,\"receiptDtlTotalAmount\":12000,\"lastBillingDate\":\"\",\"receiptDtlUnit\":0,\"intAmount\":0,\"intDay\":0,\"ft\":null}],\"debtType\":\"GUA\",\"paymentTypeId\":1}],\"subReqIdList\":[38045],\"payCustId\":100173992,\"userLoginNow\":97602202}]}";
                        string strResult = GB_StrResult;

                        ClassOutputProcessPayment Payment = JsonConvert.DeserializeObject<ClassOutputProcessPayment>(strResult);
                        /*************************************************************************/

                        ClassOtherReceipt objResult = new ClassOtherReceipt();
                        List<ClassOtherReceipt> lstObjResult = new List<ClassOtherReceipt>();


                        int index = 0;
                        foreach (var item in Payment.paymentResponseDtoList.FirstOrDefault().payTReceiptList)
                        {
                            objResult = new ClassOtherReceipt();
                            objResult.index = ++index;
                            objResult.receiptPayer = item.receiptPayer;
                            objResult.receiptPayerAddress = item.receiptPayerAddress;
                            objResult.distName = item.distName;
                            objResult.receiptNo = item.receiptNo;
                          
                            //  objResult.receiptPercentVat = item.receiptPercentVat == null ? 0 : (int) item.receiptPercentVat;
                            objResult.cashierId = item.cashierId;
                            objResult.cashierName = item.cashierName;
                            objResult.receiptRefNo = item.receiptRefNo;
                            objResult.str1 = "ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์เมื่อเรียกเก็บเงินตามเช็คได้แล้ว";
                            objResult.str2 = "This receipt is not valid until the cheque is honored by the bank";
                            objResult.str3 = "เอกสารนี้ได้จัดทำและส่งข้อมูลให้แก่กรมสรรพากรด้วยวิธีการทางอิเล็กทรอนิกส์";

                            var detail = item.responseReceiptDtlBeanList.FirstOrDefault();


                            objResult.intAmount = detail.intAmount;
                            objResult.receiptDtlNo = detail.receiptDtlNo;
                            objResult.receiptDtlName = detail.receiptDtlName;
                            objResult.receiptDtlTotalAmount = detail.receiptDtlTotalAmount;
                            objResult.receiptDtlUnit = detail.receiptDtlUnit == null ? 0 : (int) detail.receiptDtlUnit;
                            objResult.ITEM_PRICE_WITH_VAT = detail.itemPriceWithVat.Value;
                            // objResult.receiptDtlAmount = detail.receiptDtlAmount;
                            if (detail.receiptDtlVat == null || detail.receiptDtlVat == 0)
                            {
                                objResult.receiptPercentVat = 0;
                                objResult.receiptDtlAmount = detail.receiptDtlTotalAmount;
                            }
                            else
                            {
                                // objResult.receiptPercentVat = (int)item.receiptPercentVat;
                                objResult.receiptPercentVat = 1;
                                objResult.receiptDtlAmount = detail.receiptDtlAmount;
                            }

                            objResult.receiptDtlVat = (decimal)detail.receiptDtlVat;
                            objResult.strAmount = "(" + arr.ThaiBaht(detail.receiptDtlTotalAmount) + ")";

                            lstObjResult.Add(objResult);


                        }




                        string Path = "";
                        ReportDocument cryRpt = new ReportDocument();
                        CrystalDecisions.Shared.PageMargins objPageMargins;


                        Path = ReportPath + "\\" + "OthTax_DocReciept.rpt";
                        cryRpt = new ReportDocument();
                        cryRpt.Load(Path);
                        objPageMargins = cryRpt.PrintOptions.PageMargins;
                        objPageMargins.bottomMargin = 100;
                        objPageMargins.leftMargin = 100;
                        objPageMargins.rightMargin = 100;
                        objPageMargins.topMargin = 100;


                        cryRpt.SetDataSource(lstObjResult);

                        //  var LstShortBill = OFC.GetShortBill(GlobalClass.ReceiptNumber);
                        //  cryRpt.SetDataSource(lstObjResult);

                        //cryRpt.SetParameterValue("Param_Payee", GlobalClass.UserName);
                        //cryRpt.SetParameterValue("Param_BranchName", GlobalClass.BranchName);
                        //cryRpt.SetParameterValue("Param_UserId", GlobalClass.UserId);
                        //cryRpt.SetParameterValue("Param_ReceiptId", "Param_ReceiptId");
                        //cryRpt.SetParameterValue("Param_BranchId", GlobalClass.BranchId);

                        cryRpt.PrintOptions.ApplyPageMargins(objPageMargins);
                        cryRpt.PrintOptions.PrinterName = "Microsoft Print to PDF";
                        cryRpt.PrintToPrinter(1, false, 0, 0);


                        GlobalClass.StatusFormMoney = 1;
                        GlobalClass.StatusForm = null;
                   
                        this.Hide();

                        /*************************************************************************/
                    }
                    else
                    {
                        string strResult = GB_StrResult;

                        if (strResult.IndexOf("Exception : ") >= 0)
                        {
                            MessageBox.Show(strResult, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else
                        {
                            ClassOutputProcessPayment Payment = JsonConvert.DeserializeObject<ClassOutputProcessPayment>(strResult);

                            MessageBox.Show(Payment.result_message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }


                    }
                }
                else
                    MessageBox.Show("จำนวนเงินที่รับน้อยกว่าจำนวนเงินที่ต้องชำระ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                ReportErrorLog.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public bool CheckTextCreditCard()
        {
            bool tf = false;
            if (ComboBoxType.SelectedIndex == 0)
                tf = true;
            if (TextAmountCreditCard.Text == "")
                tf = true;
            if (TextBoxCardNo.Text == "")
                tf = true;
            if (TextBoxCardName.Text == "")
                tf = true;
            if (TextBoxEndDate.Text == "")
                tf = true;
            return tf;
        }
        public void SelectPayment()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                GetDataReportController getdata = new GetDataReportController();
                MyPaymentListOTH _mypay = new MyPaymentListOTH();
                List<paymentListOTH> _paylst = new List<paymentListOTH>();
                //List<paymentDetailList> _paydetaillst = new List<paymentDetailList>();
                List<paymentDetailListOTH> _paydetaillst = new List<paymentDetailListOTH>();
                List<paymentMethodList> _paymethodlst = new List<paymentMethodList>();
                List<receiptList> receiptlst = new List<receiptList>();
                List<receiptDetailDtoList> receiptdetaillst = new List<receiptDetailDtoList>();
                List<ClassOTH> _water = new List<ClassOTH>();
                paymentList payWater = new paymentList();
                paymentListOTH pay = new paymentListOTH();
                decimal AmountCreditTotal = 0;
                decimal AmountCheqTotal = 0;
                decimal AmountTotal = 0;
                decimal AmountCredit = 0;
                decimal amount = 0;
                decimal TotalAmountCheq = 0;
                decimal TotalAmount = 0;
                string chequeNo = "";
                if (TextAmountCreditCard.Text != "")
                {
                    if (CheckTextCreditCard())
                        return;
                    AmountCredit = Convert.ToDecimal(TextAmountCreditCard.Text);
                }
                if (labelAmountTotalCheq.Text != "")
                    TotalAmountCheq = Convert.ToDecimal(labelAmountTotalCheq.Text);
                if (TextAmount.Text != "")
                    TotalAmount = Convert.ToDecimal(TextAmount.Text);
                #region ค่าไฟ               
                //paymentList ELE
                DateTime dueDate;
                pay.distId = GlobalClass.DistId;
                pay.cashierNo = Convert.ToInt32(GlobalClass.No);
                pay.cashierId = GlobalClass.UserId;
                pay.paymentTotalAmount = Convert.ToDouble(LabelAmount.Text.Trim());
                pay.paymentReceive = Convert.ToDouble(LabelAmountReceived.Text.Trim());
                pay.paymentChange = Convert.ToDouble(LabelAmountReceived.Text.Trim()) - Convert.ToDouble(LabelAmount.Text.Trim());
                pay.paymentRealChange = Convert.ToDouble(LabelAmountChange.Text.Trim());
                dueDate = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                pay.paymentDate = dueDate.ToString("yyyy-MM-dd");
                pay.paymentChannel = 1;
                pay.payeeFlag = "N";
                pay.subDistId = GlobalClass.SubDistId;
                pay.userId = GlobalClass.UserId;
                pay.qNo = (qNo != null) ? qNo : null;

                #region NonMapCheque
                bool _Status = false;
                List<inquiryInfoBeanList> lstInfo = new List<inquiryInfoBeanList>();
                if (GlobalClass.LstInfo != null)
                {
                    lstInfo = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).OrderByDescending(c => c.StatusOrderBy).ToList();
                }

                foreach (var item in lstInfo)
                {
                    List<ClassCheque> _lstCh = _cheque.Where(c => c.ChStatus == true).ToList();
                    if (_cheque.Count > 0)
                    {
                        if (GlobalClass.LstSub != null)
                        {
                            decimal cheqAmount = 0;
                            var lstSub = GlobalClass.LstSub.Where(c => c.Ca == item.ca).ToList();
                            if (lstSub.Count > 0)
                            {
                                foreach (var lstitem in lstSub)
                                {
                                    cheqAmount += lstitem.Payed;
                                }
                            }
                            foreach (var itemCh in _lstCh)
                            {
                                decimal Colamount = Convert.ToDecimal(itemCh.Chamount);
                                ClassCheque cheque = _lstCh.Find(c => c.Chno.Equals(itemCh.Chno, StringComparison.CurrentCultureIgnoreCase));
                                if (cheqAmount == Colamount)
                                {
                                    inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(c => c.ca.Equals(item.ca, StringComparison.CurrentCultureIgnoreCase));
                                    if (userInfo != null)
                                    {
                                        if (userInfo.ChequeNo == null)
                                        {
                                            if (cheque != null)
                                                cheque.ChStatus = false;
                                            _Status = true;
                                            userInfo.StatusOrderBy = true;
                                            userInfo.ChequeNo = itemCh.Chno;
                                        }
                                    }
                                }
                                if (cheque != null)
                                    cheque.StatusCheque = true;
                            }
                        }
                        else
                        {
                            foreach (var itemCh in _lstCh)
                            {
                                decimal Colamount = Convert.ToDecimal(itemCh.Chamount);
                                ClassCheque cheque = _lstCh.Find(c => c.Chno.Equals(itemCh.Chno, StringComparison.CurrentCultureIgnoreCase));
                                if (item.TotalAmount == Colamount)
                                {
                                    inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(c => c.ca.Equals(item.ca, StringComparison.CurrentCultureIgnoreCase));
                                    if (userInfo != null)
                                    {
                                        if (userInfo.ChequeNo == null)
                                        {
                                            if (cheque != null)
                                                cheque.ChStatus = false;
                                            _Status = true;
                                            userInfo.StatusOrderBy = true;
                                            userInfo.ChequeNo = itemCh.Chno;
                                        }
                                    }
                                }
                                if (cheque != null)
                                    cheque.StatusCheque = true;
                            }
                        }
                    }
                }
                #endregion
                List<inquiryInfoBeanList> _lstCost = new List<inquiryInfoBeanList>();
                if (_Status)

                    _lstCost = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).OrderByDescending(g => g.StatusOrderBy).ToList();
                else

                    _lstCost = GlobalClass.LstInfo.Where(c => c.SelectCheck == true).OrderByDescending(g => g.TotalAmount).ToList();
                foreach (var itemCost in _lstCost)
                {
                    var LstCost = GlobalClass.LstCost.Where(c => c.status == true && c.ca == itemCost.ca).OrderByDescending(c => c.StatusOrderBy).ToList();
                    int CountRow = GridDetail.Rows.Count - 1;
                    int _statusAmount = 0;
                    string ChequeNo = "";
                    foreach (var itemList in LstCost)
                    {
                        decimal AmountBalance = 0;
                        bool status = false;
                        decimal paymentOld = 0;
                        inquiryGroupDebtBeanList costTD = LstCost.Find(c => c.debtType.Equals(itemList.debtType.ToString(), StringComparison.CurrentCultureIgnoreCase));
                        List<paymentDetailSubList> _paySublst = new List<paymentDetailSubList>();
                        //paymentList==>paymentDetailList
                        paymentDetailListOTH paydetail = new paymentDetailListOTH();
                        paydetail.price = itemList.price;
                        paydetail.meaUnit = itemList.unit;
                        paydetail.debtType = itemList.debtType;
                        paydetail.itemId = itemList.itemId;
                        paydetail.ui = itemList.ui;
                        //  paydetail.invHdrNo = itemList.invHdrNo;
                        paydetail.payeeName = itemCost.payeeEleName;
                        paydetail.payeeAddress = itemCost.payeeEleAddress;
                        paydetail.amount = itemList.amount;
                        paydetail.percentVat = Convert.ToInt32(double.Parse(itemList.percentVat));
                        paydetail.vat = Convert.ToDecimal(itemList.vat);
                        paydetail.totalAmount = Convert.ToDecimal(itemList.totalAmount);
                        paydetail.debtIdSet = itemList.debtIdSet;
                        paydetail.accountCode = itemList.accountCode;
                        paydetail.unitCode = itemList.unitCode;
                        paydetail.itemDesc1 = itemList.itemDesc1;
                        paydetail.itemDesc2 = itemList.itemDesc2;
                        paydetail.itemDesc3 = itemList.itemDesc3;


                        if (AmountCredit > 0)
                        {
                            if (ComboBoxCa.SelectedIndex == 0)
                            {
                                paymentDetailSubList paySub = new paymentDetailSubList();
                                paySub.paymentMethodId = 3;
                                decimal countTotal = 0;
                                countTotal = itemList.debtBalance;
                                paySub.totalAmount = Math.Round(countTotal, 2);
                                _paySublst.Add(paySub);
                                AmountCredit = AmountCredit - itemList.debtBalance;
                                status = true;
                                AmountCreditTotal += itemList.debtBalance;
                                if (AmountCredit <= 0)
                                {
                                    TotalAmount = itemList.debtBalance - countTotal;
                                    paymentOld = TotalAmount;
                                    status = false;
                                    AmountCredit = 0;
                                    AmountCreditTotal += TotalAmount;
                                }
                            }
                            else
                            {
                                string id = ComboBoxCa.Text.ToString();
                                if (itemList.ca == id)
                                {
                                    paymentDetailSubList paySub = new paymentDetailSubList();
                                    paySub.paymentMethodId = 3;
                                    decimal countTotal = 0;
                                    countTotal = itemList.debtBalance;
                                    paySub.totalAmount = Math.Round(countTotal, 2);
                                    _paySublst.Add(paySub);
                                    AmountCredit = AmountCredit - itemList.debtBalance;
                                    status = true;
                                    AmountCreditTotal += itemList.debtBalance;
                                    if (AmountCredit < 0)
                                    {
                                        TotalAmount = itemList.debtBalance - countTotal;
                                        paymentOld = TotalAmount;
                                        status = false;
                                        AmountCredit = 0;
                                        AmountCreditTotal += TotalAmount;
                                    }
                                }
                            }
                            _statusAmount = 1;
                        }
                        if (TotalAmountCheq > 0 && TotalAmount >= 0 && AmountCredit >= 0)
                        {
                            if (GlobalClass.LstSub != null)
                            {
                                List<ClassPaymentCheque> _lstCheq = GlobalClass.LstSub.Where(c => c.StatusCheque == true && c.DebtId == itemList.debtIdSet.ToString()).ToList();
                                if (_lstCheq.Count > 0)
                                {
                                    foreach (ClassPaymentCheque item in _lstCheq)
                                    {
                                        AmountBalance += item.Payed;
                                        paymentDetailSubList paySub = new paymentDetailSubList();
                                        paySub.paymentMethodId = 2;
                                        paySub.chequeNo = item.ChequeNo;
                                        paySub.totalAmount = Math.Round(item.Payed, 2);
                                        _paySublst.Add(paySub);
                                        status = true;
                                        TotalAmountCheq = Math.Round(TotalAmountCheq, 2) - Math.Round(item.Payed, 2);
                                        ChequeNo = item.ChequeNo; ;
                                    }
                                    decimal sumAmount = itemList.debtBalance - AmountBalance;
                                    if (sumAmount > 0)
                                    {
                                        int count = 0;
                                        int i = 0;
                                        decimal amountTotal = 0;
                                        for (i = 0; i <= count; i++)
                                        {
                                            List<ClassCheque> _lstCh = _cheque.Where(c => c.StatusCheque == true && c.Balance > 0).OrderByDescending(c => c.ChStatus).ToList();
                                            foreach (ClassCheque item in _lstCh)
                                            {
                                                amountTotal += item.Balance;
                                                if (amount == 0)
                                                {
                                                    ClassCheque cheque = new ClassCheque();
                                                    cheque = _lstCh.Find(c => c.Chno.Equals(item.Chno, StringComparison.CurrentCultureIgnoreCase));
                                                    if (cheque != null)
                                                        cheque.StatusCheque = false;
                                                    amount = cheque.Balance;
                                                    chequeNo = cheque.Chno;
                                                }
                                            }
                                            decimal debtBalance = 0;
                                            paymentDetailSubList paySub = new paymentDetailSubList();
                                            paySub.paymentMethodId = 2;
                                            paySub.chequeNo = chequeNo;
                                            decimal countTotal = 0;
                                            if (paymentOld != 0)
                                                countTotal = paymentOld;
                                            else
                                                countTotal = Math.Round(sumAmount, 2);
                                            if (countTotal > amount)
                                            {
                                                debtBalance = Math.Round(amount, 2);
                                                if (paymentOld != 0)
                                                    paymentOld = Math.Round(paymentOld, 2) - Math.Round(amount, 2);
                                                else
                                                    paymentOld = Math.Round(sumAmount, 2) - Math.Round(amount, 2);

                                                TotalAmountCheq = amountTotal;
                                                decimal sumTotalAmount = amountTotal - amount;
                                                if (sumTotalAmount > 0)
                                                {
                                                    count++;
                                                    amount = 0;
                                                }
                                            }
                                            else
                                            {
                                                if (paymentOld != 0)
                                                {
                                                    debtBalance = Math.Round(paymentOld, 2);
                                                    amount = Math.Round(amount, 2) - Math.Round(paymentOld, 2);
                                                }
                                                else
                                                {
                                                    debtBalance = sumAmount;
                                                    amount = Math.Round(amount, 2) - Math.Round(sumAmount, 2);
                                                }
                                            }
                                            var lstPaysub = _paySublst.Find(c => c.chequeNo.Equals(chequeNo, StringComparison.CurrentCultureIgnoreCase));
                                            if (lstPaysub != null)
                                                lstPaysub.totalAmount = lstPaysub.totalAmount + Math.Round(debtBalance, 2);
                                            else
                                            {
                                                paySub.totalAmount = Math.Round(debtBalance, 2);
                                                _paySublst.Add(paySub);
                                            }
                                            TotalAmountCheq = Math.Round(TotalAmountCheq, 2) - Math.Round(debtBalance, 2);
                                            status = true;
                                            AmountCheqTotal += countTotal;
                                        }
                                        if (TotalAmountCheq <= 0)
                                        {
                                            paymentDetailSubList paySub = new paymentDetailSubList();
                                            paySub.paymentMethodId = 1;
                                            paySub.totalAmount = Math.Round(sumAmount - amount, 2);
                                            _paySublst.Add(paySub);
                                            TotalAmount = Math.Round(TotalAmount, 2) - (sumAmount - amount);
                                            AmountTotal += sumAmount - Math.Round(amount, 2);
                                        }
                                    }
                                }
                                else
                                {
                                    status = true;
                                    paymentOld = itemList.debtBalance;
                                    if (TotalAmountCheq > 0)
                                    {
                                        decimal debtBalance =itemList.debtBalance;
                                        decimal payment;
                                        if (TotalAmountCheq > Math.Round(debtBalance, 2))
                                            payment = debtBalance;
                                        else
                                        {
                                            payment = TotalAmountCheq;
                                            paymentOld = debtBalance - TotalAmountCheq;
                                        }
                                        paymentDetailSubList paySub = new paymentDetailSubList();
                                        paySub.paymentMethodId = 2;
                                        paySub.chequeNo = ChequeNo;
                                        paySub.totalAmount = Math.Round(payment, 2);
                                        _paySublst.Add(paySub);
                                        TotalAmountCheq = Math.Round(TotalAmountCheq, 2) - Math.Round(payment, 2);
                                    }

                                    if (TotalAmountCheq <= 0)
                                    {
                                        paymentDetailSubList paySub = new paymentDetailSubList();
                                        paySub.paymentMethodId = 1;
                                        paySub.totalAmount = Math.Round(paymentOld, 2);
                                        _paySublst.Add(paySub);
                                        TotalAmount = Math.Round(TotalAmount, 2) - paymentOld;
                                        AmountTotal += Math.Round(paymentOld, 2);
                                    }
                                }
                            }
                            else
                            {
                                int count = 0;
                                int i = 0;
                                for (i = 0; i <= count; i++)
                                {
                                    List<ClassCheque> _lstCh = _cheque.Where(c => c.StatusCheque == true).OrderByDescending(c => c.ChStatus).ToList();
                                    foreach (var item in _lstCh)
                                    {
                                        if (amount == 0)
                                        {
                                            ClassCheque cheque = new ClassCheque();
                                            if (itemCost.StatusOrderBy)
                                                cheque = _lstCh.Find(c => c.Chno.Equals(itemCost.ChequeNo, StringComparison.CurrentCultureIgnoreCase));
                                            else
                                                cheque = _lstCh.Find(c => c.Chno.Equals(item.Chno, StringComparison.CurrentCultureIgnoreCase));

                                            if (cheque != null)
                                                cheque.StatusCheque = false;

                                            amount = Convert.ToDecimal(cheque.Chamount);
                                            chequeNo = cheque.Chno;
                                        }
                                    }
                                    decimal debtBalance = 0;
                                    paymentDetailSubList paySub = new paymentDetailSubList();
                                    paySub.paymentMethodId = 2;
                                    paySub.chequeNo = chequeNo;

                                    decimal countTotal = 0;
                                    if (paymentOld != 0)
                                        countTotal = paymentOld;
                                    else
                                        countTotal = itemList.debtBalance;
                                    if (countTotal > amount)
                                    {
                                        debtBalance = Math.Round(amount, 2);
                                        if (paymentOld != 0)
                                            paymentOld = Math.Round(paymentOld, 2) - Math.Round(amount, 2);
                                        else
                                            paymentOld = Math.Round(itemList.debtBalance, 2) - Math.Round(amount, 2);

                                        decimal sumAmount = TotalAmountCheq - amount;
                                        if (sumAmount > 0)//(TotalAmountCheq > countTotal)
                                        {
                                            count++;
                                            amount = 0;
                                        }
                                    }
                                    else
                                    {
                                        if (paymentOld != 0)
                                        {
                                            debtBalance = Math.Round(paymentOld, 2);
                                            amount = Math.Round(amount, 2) - Math.Round(paymentOld, 2);
                                        }
                                        else
                                        {
                                            debtBalance = itemList.debtBalance;
                                            amount = Math.Round(amount, 2) - Math.Round(itemList.debtBalance, 2);
                                        }
                                    }
                                    paySub.totalAmount = Math.Round(debtBalance, 2);
                                    _paySublst.Add(paySub);
                                    TotalAmountCheq = Math.Round(TotalAmountCheq, 2) - Math.Round(debtBalance, 2);
                                    status = true;
                                    AmountCheqTotal += countTotal;
                                }
                                if (TotalAmountCheq <= 0)
                                {
                                    paymentDetailSubList paySub = new paymentDetailSubList();
                                    paySub.paymentMethodId = 1;
                                    paySub.totalAmount = Math.Round(paymentOld, 2);//Math.Round(Convert.ToDouble(itemList.debtBalance) - amount, 2);
                                    _paySublst.Add(paySub);
                                    TotalAmount = Math.Round(TotalAmount, 2) - paymentOld;//(Convert.ToDouble(itemList.debtBalance) - amount);
                                    AmountTotal += Math.Round(paymentOld, 2);// Convert.ToDouble(itemList.debtBalance) - Math.Round(amount, 2);
                                }
                            }
                            _statusAmount = 2;
                        }
                        if (TotalAmount > 0 && status == false)
                        {
                            paymentDetailSubList paySub = new paymentDetailSubList();
                            paySub.paymentMethodId = 1;
                            paySub.totalAmount = Math.Round(itemList.debtBalance, 2);
                            _paySublst.Add(paySub);
                            TotalAmount = Math.Round(TotalAmount, 2) - itemList.debtBalance;
                            AmountTotal += itemList.debtBalance;
                            _statusAmount = 3;
                        }
                        if (costTD != null)
                            costTD.statusAmount = _statusAmount;
                        paydetail.paymentDetailSubList = _paySublst;
                        _paydetaillst.Add(paydetail);
                    }
                }
                pay.paymentDetailList = _paydetaillst;//.OrderBy(c => c.debtId).ToList();
                decimal AmountCreditDetail = 0;
                decimal TotalAmountCheqDetail = 0;
                decimal TotalAmountDetail = 0;
                if (TextAmountCreditCard.Text != "")
                    AmountCreditDetail = Convert.ToDecimal(TextAmountCreditCard.Text);
                if (labelAmountTotalCheq.Text != "")
                    TotalAmountCheqDetail = Convert.ToDecimal(labelAmountTotalCheq.Text);
                if (TextAmount.Text != "")
                    TotalAmountDetail = Convert.ToDecimal(TextAmount.Text);
                if (AmountCreditDetail > 0)
                {
                    paymentMethodList paymethod = new paymentMethodList();
                    paymethod.paymentMethodId = 3;
                    paymethod.cardNo = TextBoxCardNo.Text;
                    paymethod.cardAmount = Math.Round(AmountCreditTotal, 2);
                    paymethod.cardOwner = TextBoxCardName.Text;
                    paymethod.cardStartDate = dueDate.ToString("yyyy-MM-dd");
                    string date = DateTime.Now.Day.ToString() + "/" + TextBoxEndDate.Text;
                    DateTime dateTime = Convert.ToDateTime(date);
                    DateTime dt = Convert.ToDateTime(dateTime.ToString("yyyy-MM-dd", usCulture));
                    paymethod.cardEndDate = dt.ToString("yyyy-MM-dd");
                    _paymethodlst.Add(paymethod);
                }
                if (TotalAmountCheqDetail > 0)
                {
                    int i = 0;
                    var loopTo = GridDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewTextBoxCell ChequeNoCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNo"];
                        DataGridViewTextBoxCell ChequeNumberCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNumber"];
                        DataGridViewTextBoxCell PayeeNameCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colPayeeName"];
                        DataGridViewTextBoxCell DateCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colDate"];
                        DataGridViewTextBoxCell AmountCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colAmount"];
                        paymentMethodList paymethod = new paymentMethodList();
                        paymethod.paymentMethodId = 2;
                        paymethod.bankBranch = ChequeNoCell.EditedFormattedValue.ToString();
                        paymethod.chequeNo = ChequeNumberCell.EditedFormattedValue.ToString();
                        paymethod.chequeOwner = PayeeNameCell.EditedFormattedValue.ToString();
                        string date = DateCell.EditedFormattedValue.ToString();
                        DateTime dateTime = DateTime.Parse(date);
                        DateTime dt = Convert.ToDateTime(dateTime.ToString("yyyy-MM-dd", usCulture));
                        paymethod.chequeDate = dt.ToString("yyyy-MM-dd");
                        decimal amountCell = Convert.ToDecimal(AmountCell.EditedFormattedValue.ToString());
                        paymethod.chequeAmount = amountCell;
                        TotalAmountCheqDetail = Math.Round(TotalAmountCheqDetail, 2) - Math.Round(amountCell, 2);
                        paymethod.chequeBalance = (TotalAmountCheqDetail < 10) ? TotalAmountCheqDetail : 0;
                        paymethod.cashierCheque = "N";
                        paymethod.chequeApproveBy = null;
                        _paymethodlst.Add(paymethod);
                    }
                }
                if (TotalAmountDetail > 0)
                {
                    paymentMethodList paymethod = new paymentMethodList();
                    paymethod.paymentMethodId = 1;
                    paymethod.cashTotalAmount = Math.Round(AmountTotal, 2);
                    paymethod.cashReceive = Math.Round(TotalAmount, 2);
                    _paymethodlst.Add(paymethod);
                }
                pay.paymentMethodList = _paymethodlst;
                //receipt
                foreach (var itemRe in GlobalClass.LstInfo)
                {
                    var lst = GlobalClass.LstCost.Where(c => c.ca == itemRe.ca && c.status == true).ToList();
                    foreach (var item in lst)
                    {
                        List<receiptDetailDtoList> _lstreceipt = new List<receiptDetailDtoList>();
                        //paymentList==>receiptList
                        receiptList receipt = new receiptList();
                        receipt.receiptTypeId = 1;
                        receipt.receiptDebtType = item.debtType;
                        receipt.receiptDate = dueDate.ToString("yyyy-MM-dd");
                        receipt.receiptPayer = itemRe.payeeEleName;
                        receipt.receiptPayerAddress = itemRe.payeeEleAddress;
                        receipt.receiptPayerTaxid = itemRe.payeeSerTax20;
                        receipt.receiptPayerBranch = itemRe.payeeEleTaxBranch;
                        receipt.distId = GlobalClass.DistId.ToString();
                        receipt.distName = GlobalClass.DistName;
                        receipt.receiptCa = itemRe.ca;
                        receipt.receiptAmount = Math.Round(itemRe.EleTotalAmount, 2).ToString();
                        receipt.receiptPercentVat = "7";
                        receipt.receiptVat = Math.Round(itemRe.EleTotalVat, 2).ToString();
                        receipt.receiptTotalAmount = Math.Round(itemRe.TotalAmount, 2).ToString();
                        receipt.countInvoice = 1;

                        //receiptList==>receiptDetailDtoList
                        receiptDetailDtoList receiptdetail = new receiptDetailDtoList();
                        receiptdetail.receiptDtlNo = item.ca;
                        receiptdetail.receiptDtlName = item.reqDesc;
                        receiptdetail.receiptDtlAmount = Convert.ToDouble(Math.Round(itemRe.EleTotalAmount, 2));
                        receiptdetail.receiptDtlVat = Convert.ToDouble(Math.Round(itemRe.EleTotalVat, 2));
                        receiptdetail.receiptDtlTotalAmount = Math.Round(Math.Round(itemRe.TotalAmount, 2));
                        receiptdetail.debtIdSetList = new Int64[][] { item.debtIdSet };
                        _lstreceipt.Add(receiptdetail);
                        receipt.receiptDetailDtoList = _lstreceipt;
                        receiptlst.Add(receipt);
                    }
                }
                pay.receiptList = receiptlst;
                _paylst.Add(pay);
                _mypay.paymentList = _paylst;
                #endregion    
                GB_StrResult = getdata.GetPaymentOTH(_mypay);
                ClassOutputProcessPayment clsGetData = new ClassOutputProcessPayment();
                if (GB_StrResult.IndexOf("Exception : ") >= 0)
                {
                    _StatusSendData = false;
                }
                else
                {
                    clsGetData = JsonConvert.DeserializeObject<ClassOutputProcessPayment>(GB_StrResult);
                    if (clsGetData.result_code == "SUCCESS")
                    {
                        _StatusSendData = true;
                        //if (GlobalClass.PayMent == null)
                        //    GlobalClass.PayMent = new List<ClassPaymentResponse>();
                        //foreach (var item in clsGetData.paymentResponseList)
                        //{
                        //    ClassPaymentResponse payment = new ClassPaymentResponse();
                        //    payment.CA = item.receiptCa;
                        //    payment.ReceiptNo = item.receiptNo;
                        //    payment.PaymentId = item.paymentId;
                        //    payment.PaymentNo = item.paymentNo;
                        //    GlobalClass.PayMent.Add(payment);

                        //    List<inquiryGroupDebtBeanList> lstTD = GlobalClass.LstCost.Where(c => c.ca == item.receiptCa && c.statusCancel == true).ToList();
                        //    if (clsGetData.paymentResponseList.Count == 1)
                        //    {
                        //        foreach (var itemTD in lstTD)
                        //        {
                        //            //inquiryDebtBeanList costTD = lstTD.Find(c => c.debtIdSet.Equals(itemTD.debtIdSet, StringComparison.CurrentCultureIgnoreCase));
                        //            //if (costTD != null)
                        //            //{
                        //            //    costTD.No = (item.receiptNo != null) ? item.receiptNo : null;
                        //            //    costTD.paymentId = (item.paymentId != null) ? item.paymentId : null;
                        //            //    costTD.guaNo = (item.guaNo != null) ? item.guaNo : null;
                        //            //    costTD.paymentNo = (item.paymentNo != null) ? item.paymentNo : null;
                        //            //}
                        //        }
                        //    }
                        //    else
                        //    {
                        //        //foreach (var itemDetail in item.debtIds)
                        //        //{
                        //        //    inquiryDebtBeanList costTD = GlobalClass.LstCost.Find(c => c.debtIdSet.Equals(itemDetail.ToString(), StringComparison.CurrentCultureIgnoreCase));
                        //        //    if (costTD != null)
                        //        //    {
                        //        //        costTD.No = (item.receiptNo != null) ? item.receiptNo : null;
                        //        //        costTD.paymentId = (item.paymentId != null) ? item.paymentId : null;
                        //        //        costTD.guaNo = (item.guaNo != null) ? item.guaNo : null;
                        //        //        costTD.paymentNo = (item.paymentNo != null) ? item.paymentNo : null;
                        //        //    }
                        //        //}
                        //    }
                        //}
                        //foreach (var item in GlobalClass.PayMent)
                        //{
                        //    inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Where(c => c.ca == item.CA).FirstOrDefault();
                        //    ClassPaymentResponse payment = GlobalClass.PayMent.Find(c => c.PaymentId.Equals(item.PaymentId, StringComparison.CurrentCultureIgnoreCase));
                        //    if (payment != null && userInfo != null)
                        //    {
                        //        payment.CustomerName = userInfo.FirstName;
                        //        payment.CustomerId = userInfo.custId;
                        //        payment.UI = userInfo.ui;
                        //        payment.Amount = userInfo.TotalAmount;
                        //    }
                        //}
                    }
                    else
                        _StatusSendData = false;
                }

            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void FormReceiveMoney_Load(object sender, EventArgs e)
        {
            TextAmount.Focus();
            LabelAmount.Text = Countamount.ToString("#,###,###,##0.00");
            SetControl();
            ComboBoxType.SelectedIndex = 0;
            TextBoxPayeeName.Text = CustomerName;
        }
        public void LoadDataCa()
        {
            try
            {
                List<inquiryInfoBeanList> UserInfo = GlobalClass.LstInfo.OrderBy(c => c.ca).ToList();
                List<inquiryInfoBeanList> _lstInfo = new List<inquiryInfoBeanList>();
                int i = 0;
                foreach (var item in UserInfo)
                {
                    if (i == 0)
                    {
                        inquiryInfoBeanList userinfo = new inquiryInfoBeanList();
                        userinfo.ca = "กรุณาเลือกบัญชีแสดงสัญญา";
                        _lstInfo.Add(userinfo);
                    }
                    inquiryInfoBeanList info = new inquiryInfoBeanList();
                    info.ca = item.ca;
                    _lstInfo.Add(info);
                    i++;
                }
                ComboBoxCa.DataSource = _lstInfo;
                ComboBoxCa.DisplayMember = "ca";
                ComboBoxCa.SelectedIndex = 0;
            }
            catch (Exception ex)
            {

            }
        }
        public void SetControl()
        {
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            GlobalClass.StatusFormMoney = 0;
            this.Hide();
        }       
        public bool CheckCheque()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                // inquiryInfoBeanList lstInfo = GlobalClass.LstInfo.Where(c => c.SelectCheck == true && c.lockCheq != "").FirstOrDefault();
                inquiryInfoBeanList lstInfo = GlobalClass.LstInfo.Where(c => c.SelectCheck == true && c.lockCheque != null).FirstOrDefault();
                if (lstInfo != null && CheckCh.Checked == false)
                {
                    MessageBox.Show("บัญชีแสดงสัญญา " + lstInfo.ca + " ห้ามรับเช็ค", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    tf = true;
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return tf;
        }
        private void TextBoxB1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else
                e.Handled = true;
            if (e.KeyChar.Equals(Keys.Tab) || cInt == 13)
            {
                if (TextBoxChequeNo.Text.Length > 7 && TextBoxChequeNo.Text.Length <= 9)
                    TextBoxBankBranch.Focus();
                else
                {
                    MessageBox.Show("เลขที่เช็คต้องมากกว่า 7 ตัวอักษร และไม่เกิน 9 ตัวอักษร", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    TextBoxChequeNo.Focus();
                }
            }
        }
        private void TextBoxNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                int cInt = Convert.ToInt32(e.KeyChar);
                if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                    e.Handled = false;
                else
                    e.Handled = true;

                if (cInt == 13)
                {
                    string name = "";
                    if (TextBoxBankBranch.Text != "")
                        name = arr.GetBankDesc(TextBoxBankBranch.Text);
                    else
                    {
                        TextBoxBankBranch.Text = string.Empty;
                        TextBoxBankBranch.Focus();
                    }
                    TextBoxName.Text = name;
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void TextBoxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else if (cInt == Convert.ToChar(Keys.Enter))
                SendKeys.Send("{TAB}");
            else
                e.Handled = true;
        }
        private void ComboBoxContract_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == Convert.ToChar(Keys.Enter))
                SendKeys.Send("{TAB}");
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("ยืนยันการลบเลิกรายการ", "ลบรายการ", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;
            else
            {
                int rowIndex = GridDetail.CurrentCell.RowIndex;
                GridDetail.Rows.RemoveAt(rowIndex);                
                ClearData();
                TextBoxChequeNo.Focus();
            }
        }
        private void ClearData()
        {
            TextAmountCheque.Text = "";
            TextBoxChequeNo.Text = "";
            DateTimePickerCheque.Text = DateTime.Now.ToString("dd/MM/yyyy");
            TextBoxName.Text = "";
            TextBoxPayeeName.Text = "";
            TextBoxTel.Text = "";
            TextBoxBankBranch.Text = "";
            CheckCh.Checked = false;
        }
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (CheckCheque())
                    return;
                if (CheckTextEmpty())
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    if (CheckData())
                        MessageBox.Show("ข้อมูลเลขที่เช็คและรหัสธนาคาร/สาขา ไม่สามารถซ้ำกันได้ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    else
                    {
                        bool status = false;
                        double AmountCredit = 0;
                        double TotalAmountCheq = 0;
                        double TotalAmount = 0;
                        if (TextAmountCreditCard.Text != "")
                            AmountCredit = Convert.ToDouble(TextAmountCreditCard.Text);
                        if (labelAmountTotalCheq.Text != "")
                            TotalAmountCheq = Convert.ToDouble(labelAmountTotalCheq.Text);
                        if (TextAmount.Text != "")
                            TotalAmount = Convert.ToDouble(TextAmount.Text);

                        if (this.GridDetail.SelectedRows.Count > 0)
                        {
                            if (TotalAmount == 0 && AmountCredit == 0 && TotalAmountCheq > 0)
                            {
                                CountAmountCheq();
                                double amountSum = (TotalAmountCheq + Convert.ToDouble(TextAmountCheque.Text)) - Convert.ToDouble(LabelAmount.Text);
                                if (amountSum > 10)
                                    status = true;
                            }
                        }
                        else
                        {
                            if (TotalAmount == 0 && AmountCredit == 0 && Convert.ToDouble(TextAmountCheque.Text) > 0)
                            {
                                double amountSum = Convert.ToDouble(TextAmountCheque.Text) - Convert.ToDouble(LabelAmount.Text);
                                if (amountSum > 10)
                                    status = true;
                            }
                        }
                        if (status)
                        {
                            MessageBox.Show("มูลค่าเช็คที่ชำระเกินจากจำนวนเงินที่ต้องชำระมากกว่า 10 บาทไม่สามารถรับได้ ", "ชำระเกิน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        else
                        {
                            AddChequeToList();
                            CountAmountCheq();
                            Summount();
                            ClearData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void Summount()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double totalAmount = 0;
                double amount = 0;
                double amountCheque = 0;
                double amountCreditCard = 0;
                double sumAmount = 0;
                if (TextAmount.Text != "")
                    amount = Convert.ToDouble(TextAmount.Text);
                if (labelAmountTotalCheq.Text != "0.00")
                    amountCheque = Convert.ToDouble(labelAmountTotalCheq.Text);
                if (TextAmountCreditCard.Text != "")
                    amountCreditCard = Convert.ToDouble(TextAmountCreditCard.Text);

                totalAmount = amount + amountCheque + amountCreditCard;
                LabelAmountReceived.Text = totalAmount.ToString("#,###,###,##0.00");
                if (totalAmount > 0)
                    sumAmount = totalAmount - Convert.ToDouble(LabelAmount.Text.Trim());
                if (sumAmount < 0)
                {
                    label25.Text = sumAmount.ToString("#,###,###,##0.00");
                    LabelAmountChange.Text = "00";
                }
                else
                {
                    double NewAmount;
                    string Satang;
                    var AmountTotal = sumAmount.ToString("#,###,###,##0.00");
                    String[] datas = AmountTotal.Split('.');
                    Satang = arr.Fraction(Convert.ToInt32(datas[1].ToString()));
                    NewAmount = Convert.ToDouble(datas[0].ToString()) + Convert.ToDouble(Satang);
                    LabelAmountChange.Text = NewAmount.ToString("#,###,###,##0.00");
                    label25.Text = "0.00";
                    double Fraction = Convert.ToDouble(datas[1].ToString());
                    labelFraction.Text = "0." + Fraction.ToString("0.##");
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public void CountAmountCheq()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                double Amount = 0;
                int i = 0;
                var loopTo = GridDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    Image image = Properties.Resources.Recycle;
                    GridDetail.Rows[i].Cells["ColDelete"].Value = image;
                    Image Edit = Properties.Resources.icons8_edit_32;
                    GridDetail.Rows[i].Cells["ColEdit"].Value = Edit;
                    Amount += Convert.ToDouble(GridDetail.Rows[i].Cells["colAmount"].Value);
                }
                labelAmountTotalCheq.Text = Amount.ToString("#,###,###,##0.00");
                labelAmountCheq.Text = Amount.ToString("#,###,##0.00");
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private bool CheckTextEmpty()
        {
            bool status = false;
            if (TextBoxChequeNo.Text == string.Empty)
                status = true;
            if (TextBoxBankBranch.Text == string.Empty)
                status = true;
            if (TextBoxName.Text == string.Empty)
                status = true;
            if (DateTimePickerCheque.Text == string.Empty)
                status = true;
            if (TextAmountCheque.Text == string.Empty)
                status = true;
            if (TextBoxPayeeName.Text == string.Empty)
                status = true;
            if (TextBoxTel.Text == string.Empty)
                status = true;
            return status;
        }
        private bool CheckData()
        {
            bool tf;
            var checkData = _cheque.Where(c => c.Chno == TextBoxChequeNo.Text).FirstOrDefault();
            if (checkData != null)
                tf = true;
            else
                tf = false;
            return tf;
        }
        public void AddChequeToList()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (_cheque != null)
                {
                    int i = 1;
                    List<ClassCheque> _lst = new List<ClassCheque>();
                    foreach (var item in _cheque)
                    {
                        ClassCheque cheque = new ClassCheque();
                        cheque.Chno = item.Chno;
                        cheque.Bankno = item.Bankno;
                        cheque.Bankname = item.Bankname;
                        cheque.Chdate = item.Chdate;
                        cheque.Chamount = item.Chamount;
                        cheque.Chname = item.Chname;
                        cheque.Tel = item.Tel;
                        cheque.Chcheck = item.Chcheck;
                        cheque.ChStatus = true;
                        cheque.Status = item.Status;
                        cheque.No = i.ToString();
                        i++;
                        _lst.Add(cheque);
                    }
                    ClassCheque chequeNew = new ClassCheque();
                    chequeNew.Chno = TextBoxChequeNo.Text.Trim();
                    chequeNew.Bankno = TextBoxBankBranch.Text.Trim();
                    chequeNew.Bankname = TextBoxName.Text.Trim();
                    chequeNew.Chdate = DateTimePickerCheque.Text.ToString();
                    chequeNew.Chamount = Convert.ToDecimal(TextAmountCheque.Text.Trim());
                    chequeNew.Chname = TextBoxPayeeName.Text.Trim();
                    chequeNew.Tel = TextBoxTel.Text.Trim();
                    chequeNew.Chcheck = CheckCh.Checked;
                    chequeNew.ChStatus = true;
                    chequeNew.No = i.ToString();
                    _lst.Add(chequeNew);
                    _cheque = _lst;
                }
                else
                {
                    ClassCheque cheque = new ClassCheque();
                    cheque.Chno = TextBoxChequeNo.Text.Trim();
                    cheque.Bankno = TextBoxBankBranch.Text.Trim();
                    cheque.Bankname = TextBoxName.Text.Trim();
                    cheque.Chdate = DateTimePickerCheque.Text.ToString();
                    cheque.Chamount = Convert.ToDecimal(TextAmountCheque.Text.Trim());
                    cheque.Chname = TextBoxPayeeName.Text.Trim();
                    cheque.Tel = TextBoxTel.Text.Trim();
                    cheque.Chcheck = CheckCh.Checked;
                    cheque.ChStatus = true;
                    cheque.No = "1";
                    _cheque.Add(cheque);
                    lblNo.Text = "1";
                }
                this.GridDetail.DataSource = _cheque;
                GlobalClass.LstCheque = _cheque;
                foreach (var item in GlobalClass.LstCheque)
                {
                    CountamountAll += Convert.ToDouble(item.Chamount);
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void GridDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                FormMapCheque detail = new FormMapCheque();
                detail.labelNumber.Text = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                detail.labelNo.Text = GridDetail.CurrentRow.Cells["ColNo"].Value.ToString();
                detail.labelName.Text = GridDetail.CurrentRow.Cells["ColName"].Value.ToString();
                detail.labelDate.Text = GridDetail.CurrentRow.Cells["ColDate"].Value.ToString();
                detail.labelAmount.Text = GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString();
                detail.labelPayeeName.Text = GridDetail.CurrentRow.Cells["ColPayeeName"].Value.ToString();
                detail.labelTel.Text = GridDetail.CurrentRow.Cells["ColTel"].Value.ToString();
                detail.CheckCh.Checked = Convert.ToBoolean(GridDetail.CurrentRow.Cells["colchCash"].Value.ToString());
                detail.labelBalance.Text = (GridDetail.CurrentRow.Cells["ColBalance"].Value.ToString() != "") ? GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString() : GridDetail.CurrentRow.Cells["ColBalance"].Value.ToString();
                detail.ShowDialog();
                detail.Dispose();
                LoadData();
            }
            else if (e.ColumnIndex == 1)
            {
                if (MessageBox.Show("คุณต้องการยกเลิกรายการนี้หรือไม่ ", "ยกเลิกเช็ค", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                else
                {
                    if (_cheque != null)
                    {
                        int rowIndex = GridDetail.CurrentCell.RowIndex;
                        _cheque.RemoveAt(rowIndex);
                        this.GridDetail.DataSource = null;

                        if (_cheque.Count > 0)
                            this.GridDetail.DataSource = _cheque;

                        CountAmountCheq();
                        ClearData();
                        this.GridDetail.Update();
                        this.GridDetail.Refresh();
                    }
                }
            }
            else if (e.ColumnIndex == 2)
            {
                TextBoxChequeNo.Text = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                TextBoxBankBranch.Text = GridDetail.CurrentRow.Cells["ColNo"].Value.ToString();
                TextBoxName.Text = GridDetail.CurrentRow.Cells["ColName"].Value.ToString();
                DateTimePickerCheque.Text = GridDetail.CurrentRow.Cells["ColDate"].Value.ToString();
                TextAmountCheque.Text = GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString();
                TextBoxPayeeName.Text = GridDetail.CurrentRow.Cells["ColPayeeName"].Value.ToString();
                TextBoxTel.Text = GridDetail.CurrentRow.Cells["ColTel"].Value.ToString();
                CheckCh.Checked = Convert.ToBoolean(GridDetail.CurrentRow.Cells["colchCash"].Value.ToString());
                lblNo.Text = GridDetail.CurrentRow.Cells["ColNoId"].Value.ToString();
                ButtonUpate.Enabled = true;
                ButtonAdd.Enabled = false;
            }
            else
            {
                ButtonUpate.Enabled = false;
                ButtonAdd.Enabled = true;
            }
        }
        public void LoadData()
        {
            if (GlobalClass.LstCheque != null)
                GridDetail.DataSource = GlobalClass.LstCheque;
        }
        private void TextAmount_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(TextAmount.Text, "^[0-9]"))
                Summount();
            if (TextAmount.Text == "" && TextAmountCreditCard.Text == "" && labelAmountTotalCheq.Text == "0.00")
                Summount();
        }
        public void SetDataListGridview()
        {
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                ClassListGridview grid = new ClassListGridview();
                DataGridViewTextBoxCell amountChequeCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colAmount"];
                DataGridViewTextBoxCell ChequeNoCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNumber"];
                grid.ChNo = ChequeNoCell.EditedFormattedValue.ToString();
                grid.ChAmount = amountChequeCell.EditedFormattedValue.ToString();
                _lstGrid.Add(grid);
            }
        }
        private void ComboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxType.SelectedIndex != 0)
            {
                TextAmountCreditCard.Text = LabelAmount.Text;
                LoadDataCa();
            }
            if (ComboBoxType.SelectedIndex == 0)
            {
                TextAmountCreditCard.Text = string.Empty;
                ComboBoxCa.SelectedIndex = 0;
            }
            TextAmount.Text = string.Empty;
            Summount();
        }
        private void ComboBoxCa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxCa.SelectedIndex == 0)
            {
                if (ComboBoxType.SelectedIndex != 0)
                    TextAmountCreditCard.Text = LabelAmount.Text;
            }
            else
            {
                inquiryInfoBeanList UserInfo = GlobalClass.LstInfo.Where(c => c.ca == ComboBoxCa.Text).FirstOrDefault();
                if (UserInfo.EleTotalAmount.ToString() != "0")
                    TextAmountCreditCard.Text = UserInfo.EleTotalAmount.ToString();
                else
                    TextAmountCreditCard.Text = UserInfo.OtherTotalAmount;
            }
        }
        private void TextAmountCreditCard_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(TextAmountCreditCard.Text, "^[0-9]"))
                Summount();
        }
        private void ButtonApprove_Click(object sender, EventArgs e)
        {

        }
        private void GridDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }
        private void ButtonUpate_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (CheckTextEmpty())
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    bool status = false;
                    double AmountCredit = 0;
                    double TotalAmountCheq = 0;
                    double TotalAmount = 0;
                    if (TextAmountCreditCard.Text != "")
                        AmountCredit = Convert.ToDouble(TextAmountCreditCard.Text);
                    if (labelAmountTotalCheq.Text != "")
                        TotalAmountCheq = Convert.ToDouble(labelAmountTotalCheq.Text);
                    if (TextAmount.Text != "")
                        TotalAmount = Convert.ToDouble(TextAmount.Text);
                    if (this.GridDetail.SelectedRows.Count > 0)
                    {
                        if (TotalAmount == 0 && AmountCredit == 0 && TotalAmountCheq > 0)
                        {
                            CountAmountCheq();
                            double amountSum = (TotalAmountCheq + Convert.ToDouble(TextAmountCheque.Text)) - Convert.ToDouble(LabelAmount.Text);
                            if (amountSum > 10)
                                status = true;
                        }
                    }
                    else
                    {
                        if (TotalAmount == 0 && AmountCredit == 0 && Convert.ToDouble(TextAmountCheque.Text) > 0)
                        {
                            double amountSum = Convert.ToDouble(TextAmountCheque.Text) - Convert.ToDouble(LabelAmount.Text);
                            if (amountSum > 10)
                                status = true;
                        }
                    }
                    if (status)
                    {
                        MessageBox.Show("มูลค่าเช็คที่ชำระเกินจากจำนวนเงินที่ต้องชำระมากกว่า 10 บาทไม่สามารถรับได้ ", "ชำระเกิน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    else
                    {
                        ClassCheque cheque = _cheque.Find(item => item.No.Equals(lblNo.Text.ToString(), StringComparison.CurrentCultureIgnoreCase));
                        if (cheque != null)
                        {
                            cheque.Bankno = TextBoxBankBranch.Text;
                            cheque.Bankname = TextBoxName.Text;
                            cheque.Chdate = DateTimePickerCheque.Text;
                            cheque.Chamount = Convert.ToDecimal(TextAmountCheque.Text);
                            cheque.Chname = TextBoxPayeeName.Text;
                            cheque.Tel = TextBoxTel.Text;
                            cheque.Chcheck = CheckCh.Checked;
                            cheque.Chno = TextBoxChequeNo.Text;
                        }
                        this.GridDetail.DataSource = null;
                        this.GridDetail.DataSource = _cheque;
                        CountAmountCheq();
                        this.GridDetail.Update();
                        this.GridDetail.Refresh();
                        ButtonUpate.Enabled = false;
                        ButtonAdd.Enabled = true;
                        ClearData();
                    }
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void TextBoxPayeeName_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                TextBoxTel.Focus();
        }
        private void TextBoxTel_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(TextBoxTel.Text, "^[0-9]"))
            {
                string s = TextBoxTel.Text;
                if (s.Length == 10)
                {
                    double sAsD = double.Parse(s);
                    string tel = string.Format("{0:###-###-####}", sAsD).ToString();
                    TextBoxTel.Text = "0" + tel;
                }
                if (TextBoxTel.Text.Length > 1)
                    TextBoxTel.SelectionStart = TextBoxTel.Text.Length;
                TextBoxTel.SelectionLength = 0;

            }
        }
        private void TextBoxEndDate_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(TextBoxEndDate.Text, "^[0-9]"))
            {
                string s = TextBoxEndDate.Text;
                if (s.Length == 10)
                {
                    double sAsD = double.Parse(s);
                    string tel = string.Format("{0:##/####}", sAsD).ToString();
                    TextBoxEndDate.Text = "0" + tel;
                }
                if (TextBoxEndDate.Text.Length > 1)
                    TextBoxEndDate.SelectionStart = TextBoxEndDate.Text.Length;
                TextBoxEndDate.SelectionLength = 0;
            }
        }
        private void DateTimePickerCheque_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt = DateTimePickerCheque.Value;
            DateTime now = DateTime.Now;
            var numOfDay = now - dt;          
            if (numOfDay.Days > 7)
            {
                DialogResult dialogResult = MessageBox.Show("คุณต้องการรับเช็คย้อนหลัง จากวันที่ปัจจุบันได้เกิน 7 วัน ", "แจ้งเตือน", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    FormCheckUser checkUser = new FormCheckUser("ยืนยันการรับเช็คย้อนหลัง");
                    checkUser.ShowDialog();
                    checkUser.Dispose();
                    if (GlobalClass.AuthorizeLevel == 2)
                        TextAmountCheque.Focus();
                    else
                    {
                        if (GlobalClass.StatusForm != 0)
                            MessageBox.Show("User ที่สามารถรับเช็คย้อนหลัง จากวันที่ปัจจุบันได้เกิน 7 วันได้ต้องเป็น Level 2 เท่านั้น", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        DateTimePickerCheque.Focus();
                        DateTimePickerCheque.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        return;
                    }
                }
                else if (dialogResult == DialogResult.No)
                {
                    DateTimePickerCheque.Focus();
                    DateTimePickerCheque.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    return;
                }
            }
        }
    }
}
