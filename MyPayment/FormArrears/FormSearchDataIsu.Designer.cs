﻿namespace MyPayment.FormArrears
{
    partial class FormSearchDataIsu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSearchDataIsu));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TextboxCA = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ButtonClear = new Custom_Controls_in_CS.ButtonZ();
            this.TextBoxUI = new System.Windows.Forms.TextBox();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btPrint = new Custom_Controls_in_CS.ButtonZ();
            this.coldebtDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldebtType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldebtDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colpercentVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colvat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colamount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSumAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldistId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextboxCA
            // 
            this.TextboxCA.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.TextboxCA.Location = new System.Drawing.Point(11, 40);
            this.TextboxCA.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxCA.Name = "TextboxCA";
            this.TextboxCA.Size = new System.Drawing.Size(330, 31);
            this.TextboxCA.TabIndex = 0;
            this.TextboxCA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxPaymentNo_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 28);
            this.label1.TabIndex = 3;
            this.label1.Text = "บัญชีแสดงสัญญา";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(363, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 28);
            this.label2.TabIndex = 4;
            this.label2.Text = "เครื่องวัด";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ButtonClear);
            this.groupBox1.Controls.Add(this.TextBoxUI);
            this.groupBox1.Controls.Add(this.ButtonSearch);
            this.groupBox1.Controls.Add(this.TextboxCA);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1012, 86);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // ButtonClear
            // 
            this.ButtonClear.BackColor = System.Drawing.Color.Transparent;
            this.ButtonClear.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonClear.BorderWidth = 1;
            this.ButtonClear.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonClear.ButtonText = "เคลียร์";
            this.ButtonClear.CausesValidation = false;
            this.ButtonClear.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonClear.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold);
            this.ButtonClear.ForeColor = System.Drawing.Color.Black;
            this.ButtonClear.GradientAngle = 90;
            this.ButtonClear.Image = global::MyPayment.Properties.Resources.Cycle1;
            this.ButtonClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonClear.Location = new System.Drawing.Point(735, 31);
            this.ButtonClear.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonClear.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonClear.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonClear.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.ShowButtontext = true;
            this.ButtonClear.Size = new System.Drawing.Size(127, 40);
            this.ButtonClear.StartColor = System.Drawing.Color.LightGray;
            this.ButtonClear.TabIndex = 1000000040;
            this.ButtonClear.TextLocation_X = 41;
            this.ButtonClear.TextLocation_Y = 5;
            this.ButtonClear.Transparent1 = 80;
            this.ButtonClear.Transparent2 = 120;
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // TextBoxUI
            // 
            this.TextBoxUI.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.TextBoxUI.Location = new System.Drawing.Point(363, 40);
            this.TextBoxUI.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxUI.Name = "TextBoxUI";
            this.TextBoxUI.Size = new System.Drawing.Size(330, 31);
            this.TextBoxUI.TabIndex = 1;
            this.TextBoxUI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxReceiptNo_KeyPress);
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "ค้นหา";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = global::MyPayment.Properties.Resources.zoom;
            this.ButtonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSearch.Location = new System.Drawing.Point(873, 31);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(127, 40);
            this.ButtonSearch.StartColor = System.Drawing.Color.LightGray;
            this.ButtonSearch.TabIndex = 2;
            this.ButtonSearch.TextLocation_X = 45;
            this.ButtonSearch.TextLocation_Y = 5;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DataGridViewDetail);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 86);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 562);
            this.panel1.TabIndex = 14;
            // 
            // DataGridViewDetail
            // 
            this.DataGridViewDetail.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.DataGridViewDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewDetail.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coldebtDate,
            this.coldebtType,
            this.coldebtDesc,
            this.colpercentVat,
            this.colvat,
            this.colamount,
            this.colSumAmount,
            this.coldistId});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewDetail.DefaultCellStyle = dataGridViewCellStyle9;
            this.DataGridViewDetail.Location = new System.Drawing.Point(0, 0);
            this.DataGridViewDetail.Name = "DataGridViewDetail";
            this.DataGridViewDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DataGridViewDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewDetail.Size = new System.Drawing.Size(1012, 562);
            this.DataGridViewDetail.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btPrint);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 648);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1012, 57);
            this.panel2.TabIndex = 1;
            // 
            // btPrint
            // 
            this.btPrint.BackColor = System.Drawing.Color.Transparent;
            this.btPrint.BorderColor = System.Drawing.Color.Transparent;
            this.btPrint.BorderWidth = 1;
            this.btPrint.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btPrint.ButtonText = "พิมพ์";
            this.btPrint.CausesValidation = false;
            this.btPrint.EndColor = System.Drawing.Color.Silver;
            this.btPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPrint.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold);
            this.btPrint.ForeColor = System.Drawing.Color.Black;
            this.btPrint.GradientAngle = 90;
            this.btPrint.Image = ((System.Drawing.Image)(resources.GetObject("btPrint.Image")));
            this.btPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrint.Location = new System.Drawing.Point(877, 7);
            this.btPrint.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btPrint.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btPrint.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btPrint.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btPrint.Name = "btPrint";
            this.btPrint.ShowButtontext = true;
            this.btPrint.Size = new System.Drawing.Size(127, 40);
            this.btPrint.StartColor = System.Drawing.Color.Gainsboro;
            this.btPrint.TabIndex = 1000000046;
            this.btPrint.TextLocation_X = 50;
            this.btPrint.TextLocation_Y = 5;
            this.btPrint.Transparent1 = 80;
            this.btPrint.Transparent2 = 120;
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // coldebtDate
            // 
            this.coldebtDate.DataPropertyName = "dueDate";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.coldebtDate.DefaultCellStyle = dataGridViewCellStyle3;
            this.coldebtDate.HeaderText = "วันที่ครบกำหนด";
            this.coldebtDate.Name = "coldebtDate";
            this.coldebtDate.Width = 180;
            // 
            // coldebtType
            // 
            this.coldebtType.DataPropertyName = "debtType";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.coldebtType.DefaultCellStyle = dataGridViewCellStyle4;
            this.coldebtType.HeaderText = "ประเภทหนี้";
            this.coldebtType.Name = "coldebtType";
            this.coldebtType.Width = 180;
            // 
            // coldebtDesc
            // 
            this.coldebtDesc.DataPropertyName = "debtDesc";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.coldebtDesc.DefaultCellStyle = dataGridViewCellStyle5;
            this.coldebtDesc.HeaderText = "รายการ";
            this.coldebtDesc.Name = "coldebtDesc";
            this.coldebtDesc.Width = 270;
            // 
            // colpercentVat
            // 
            this.colpercentVat.DataPropertyName = "percentVat";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colpercentVat.DefaultCellStyle = dataGridViewCellStyle6;
            this.colpercentVat.HeaderText = "vat %";
            this.colpercentVat.Name = "colpercentVat";
            this.colpercentVat.Width = 95;
            // 
            // colvat
            // 
            this.colvat.DataPropertyName = "vat";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colvat.DefaultCellStyle = dataGridViewCellStyle7;
            this.colvat.HeaderText = "ภาษี";
            this.colvat.Name = "colvat";
            this.colvat.Width = 120;
            // 
            // colamount
            // 
            this.colamount.DataPropertyName = "amount";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.colamount.DefaultCellStyle = dataGridViewCellStyle8;
            this.colamount.HeaderText = "จำนวนเงิน";
            this.colamount.Name = "colamount";
            this.colamount.Width = 120;
            // 
            // colSumAmount
            // 
            this.colSumAmount.DataPropertyName = "debtBalance";
            this.colSumAmount.HeaderText = "จำนวนเงินรวม";
            this.colSumAmount.Name = "colSumAmount";
            this.colSumAmount.Width = 180;
            // 
            // coldistId
            // 
            this.coldistId.DataPropertyName = "distId";
            this.coldistId.HeaderText = "DistId";
            this.coldistId.Name = "coldistId";
            this.coldistId.Visible = false;
            this.coldistId.Width = 150;
            // 
            // FormSearchDataIsu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSearchDataIsu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ค้นหาข้อมูลหนี้";
            this.Load += new System.EventHandler(this.FormCancelReceipt_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDetail)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TextboxCA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView DataGridViewDetail;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private System.Windows.Forms.TextBox TextBoxUI;
        private Custom_Controls_in_CS.ButtonZ ButtonClear;
        private Custom_Controls_in_CS.ButtonZ btPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldebtDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldebtType;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldebtDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colpercentVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colvat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colamount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSumAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldistId;
    }
}