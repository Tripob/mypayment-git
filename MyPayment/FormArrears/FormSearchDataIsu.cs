﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.FormArrears
{
    public partial class FormSearchDataIsu : Form
    {
        ResultPayment resul = new ResultPayment();
        GeneralService genService = new GeneralService(true);
        ClassLogsService meaLog = new ClassLogsService("Cancel", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("Cancel", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("Cancel", "ErrorLog");
        ArrearsController arr = new ArrearsController();
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        List<inquiryGroupDebtBeanList> _lstinquiry = new List<inquiryGroupDebtBeanList>();
        Thread threadData;
        Thread threadBar;
        public FormSearchDataIsu()
        {
            InitializeComponent();
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void FormCancelReceipt_Load(object sender, EventArgs e)
        {
            SetControl();
        }
        public void SetControl()
        {
            DataGridViewDetail.AutoGenerateColumns = false;
            DataGridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (TextboxCA.Text == "" && TextBoxUI.Text == "")
                    MessageBox.Show("กรุณากรอกเงื่อนไขในการค้นหา", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    ResultInquiryDebt resul = new ResultInquiryDebt();
                    ClassUser model = new ClassUser();
                    model.ca = TextboxCA.Text;
                    model.ui = TextBoxUI.Text;
                    resul = arr.SelectProcessInquiry(model, resul, TextboxCA.Text + TextBoxUI.Text, 1);
                    if (resul.Output.Count > 0)
                    {
                        _lstinquiry = new List<inquiryGroupDebtBeanList>();
                        foreach (var item in resul.Output)
                        {
                            if (item.inquiryGroupDebtBeanList != null)
                            {
                                foreach (var itemDetail in item.inquiryGroupDebtBeanList)
                                {
                                    inquiryGroupDebtBeanList inquiry = new inquiryGroupDebtBeanList();
                                    inquiry.debtType = itemDetail.debtType;
                                    inquiry.distId = itemDetail.distId;
                                    inquiry.dueDate = (itemDetail.debtDate != null) ? Convert.ToDateTime(itemDetail.debtDate).ToString("dd/MM/yyyy") : "";
                                    inquiry.amount = itemDetail.amount;
                                    inquiry.vat = itemDetail.vat;
                                    inquiry.percentVat = itemDetail.percentVat;
                                    inquiry.debtDesc = itemDetail.debtDesc;
                                    inquiry.debtBalance = itemDetail.debtBalance;
                                    _lstinquiry.Add(inquiry);
                                }
                            }
                            else
                            {
                                MessageBox.Show("ไม่มีหนี้ค้างชำระ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                ClearData();
                                return;
                            }
                        }
                        DataGridViewDetail.DataSource = _lstinquiry;
                    }
                    else
                        MessageBox.Show("ไม่มีข้อมูลในการค้นหา", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }                
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("เกิดข้อผิดพลาดในการค้นหา", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            ClearData();
        }
        public void ClearData()
        {
            TextboxCA.Text = string.Empty;
            TextBoxUI.Text = string.Empty;
        }
        private void TextboxPaymentNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                ButtonSearch_Click(null, null);
        }
        private void TextBoxReceiptNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == 13)
                ButtonSearch_Click(null, null);
        }
        private void ButtonClear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            if (!SendDataAddPrintData())
            {
                this.DataGridViewDetail.DataSource = null;
                this.DataGridViewDetail.Refresh();
            }
        }
        public bool SendDataAddPrintData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool status = false;
            try
            {
                if (_lstinquiry.Count > 0)
                {
                    string Path = "";
                    List<ClassInformationReport> _lstDisplay = new List<ClassInformationReport>();
                    foreach (inquiryGroupDebtBeanList item in _lstinquiry)
                    {
                        DateTime dt = DateTime.Now;
                        DateTime dtNow = DateTime.Now;
                        int i = 0;
                        int year = 0;
                        int day = 0;
                        int mouth = 0;
                        DateTime date = new DateTime();
                        ClassInformationReport inquiry = new ClassInformationReport();
                        if (item.debtDate != null && item.debtDate != "")
                        {
                            String[] dataDate = item.debtDate.Split('-');
                            foreach (var itemDate in dataDate)
                            {
                                if (i == 0)
                                    year = int.Parse(itemDate);
                                else if (i == 1)
                                    mouth = int.Parse(itemDate);
                                else
                                    day = int.Parse(itemDate);
                                i++;
                            }
                            if (year < 2500)
                                year = year + 543;
                            date = new DateTime(year, mouth, day);
                            inquiry.debtDate = date.ToString("dd/MM/yyyy");
                        }
                        string str = arr.PrintDate(dt.Month);
                        if (dt.Year < 2500)
                            dtNow = dt.AddYears(543);
                        inquiry.strDate = "วันที่ " + dt.Day.ToString() + " " + str + " " + dtNow.Year.ToString() + " เวลา : " + dt.Hour.ToString() + ":" + dt.Minute.ToString();
                        inquiry.debtType = item.debtType;
                        inquiry.distId = item.distId;
                        inquiry.amount = item.amount;
                        inquiry.vat = item.vat;
                        inquiry.percentVat = int.Parse(item.percentVat);
                        inquiry.debtDesc = item.debtDesc;
                        inquiry.totalAmount = item.debtBalance;
                        _lstDisplay.Add(inquiry);
                    }
                    Path = ReportPath + "\\" + "Check_Information_Report.rpt";
                    ReportDocument cryRpt = new ReportDocument();
                    cryRpt.Load(Path);
                    cryRpt.Database.Tables[0].SetDataSource(_lstDisplay);
                    CrystalReportViewer crystalReportViewer1 = new CrystalReportViewer();
                    crystalReportViewer1.ReportSource = cryRpt;
                    crystalReportViewer1.Refresh();
                    cryRpt.PrintToPrinter(1, true, 0, 0);
                    status = true;
                }
            }
            catch (Exception ex)
            {
                meaLogError.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
            return status;
        }
    }
}
