﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Master;
using MyPayment.Models;


namespace MyPayment.FormReport
{
    public partial class FormChequeReport : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        public FormChequeReport()
        {
            InitializeComponent();
        }
        private void FormChequeReport_Load(object sender, EventArgs e)
        {
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Font = new Font("TH Sarabun New", 14.25F, FontStyle.Bold);
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            int Year = DateTimePicker.Value.Year;
            if (Year > 2500)
                Year = Year - 543;
            string strDate = Year.ToString() + "-" + DateTimePicker.Value.ToString("MM-dd");
            var obj = OFC.GetSummaryChequeReport(strDate);
            GridDetail.DataSource = obj;
            GlobalClass.LstSummaryChequeModel = obj;
        }
        private void btPrintReport_Click(object sender, EventArgs e)
        {
            GlobalClass.ReportName = "rptSummaryChequeReport.rpt";
            GlobalClass.ReportOfflineType = "CHEQUE";
            FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();
            //formm receive = new FormReceiveMoney();
            FVR.ShowDialog();
        }
    }
}
