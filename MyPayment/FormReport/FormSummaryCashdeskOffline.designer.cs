﻿namespace MyPayment.FormReport
{
    partial class FormSummaryCashdeskOffline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSummaryCashdeskOffline));
            this.GridDetail = new System.Windows.Forms.DataGridView();
            this.F1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.lblTotalContractAccount = new System.Windows.Forms.Label();
            this.lblTotalTaxInvoice = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btPrintReport = new Custom_Controls_in_CS.ButtonZ();
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GridDetail
            // 
            this.GridDetail.AllowUserToAddRows = false;
            this.GridDetail.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridDetail.ColumnHeadersHeight = 34;
            this.GridDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F1,
            this.Column2,
            this.Column3,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column13,
            this.Column14,
            this.Column15});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridDetail.DefaultCellStyle = dataGridViewCellStyle14;
            this.GridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDetail.Location = new System.Drawing.Point(0, 0);
            this.GridDetail.Margin = new System.Windows.Forms.Padding(5);
            this.GridDetail.Name = "GridDetail";
            this.GridDetail.RowHeadersWidth = 10;
            this.GridDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridDetail.Size = new System.Drawing.Size(1012, 586);
            this.GridDetail.TabIndex = 3;
            // 
            // F1
            // 
            this.F1.DataPropertyName = "RowNo";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = "0.0";
            this.F1.DefaultCellStyle = dataGridViewCellStyle2;
            this.F1.HeaderText = "ลำดับ";
            this.F1.Name = "F1";
            this.F1.Width = 60;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "CreateDate";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column2.HeaderText = "วันที่รับชำระ";
            this.Column2.Name = "Column2";
            this.Column2.Width = 120;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CreateTime";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column3.HeaderText = "เวลา";
            this.Column3.Name = "Column3";
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "UserCode";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column6.HeaderText = "รหัสพนักงาน";
            this.Column6.Name = "Column6";
            this.Column6.Width = 130;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "CA";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column7.HeaderText = "บัญชีแสดงสัญญา";
            this.Column7.Name = "Column7";
            this.Column7.Width = 150;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "InvoiceNumber";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column8.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column8.HeaderText = "เลขที่แสดงใบแจ้งหนี้";
            this.Column8.Name = "Column8";
            this.Column8.Width = 170;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "BillNumber";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column9.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column9.HeaderText = "เลขที่ใบเสร็จรับเงิน/ใบกำกับภาษีอย่างย่อ";
            this.Column9.Name = "Column9";
            this.Column9.Width = 300;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "TotalAmountIncVat";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = "0.0";
            this.Column10.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column10.HeaderText = "จำนวนเงิน(บาท)";
            this.Column10.Name = "Column10";
            this.Column10.Width = 150;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "Cash";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            dataGridViewCellStyle10.NullValue = null;
            this.Column11.DefaultCellStyle = dataGridViewCellStyle10;
            this.Column11.HeaderText = "เงินสด";
            this.Column11.Name = "Column11";
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "SumCheque";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            this.Column13.DefaultCellStyle = dataGridViewCellStyle11;
            this.Column13.HeaderText = "เช็ค";
            this.Column13.Name = "Column13";
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "ChequeChange";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            this.Column14.DefaultCellStyle = dataGridViewCellStyle12;
            this.Column14.HeaderText = "เงินทอนเช็ค";
            this.Column14.Name = "Column14";
            this.Column14.Width = 130;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "Tel";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Column15.DefaultCellStyle = dataGridViewCellStyle13;
            this.Column15.HeaderText = "หมายเลขติดต่อ";
            this.Column15.Name = "Column15";
            this.Column15.Width = 130;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(17, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "วัน/เดือน/ปี(พ.ศ.)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(14, 666);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 26);
            this.label4.TabIndex = 12;
            this.label4.Text = "จำนวนยอดรับชำระทั้งหมด";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(554, 666);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(196, 26);
            this.label5.TabIndex = 13;
            this.label5.Text = "จำนวนบัญชีแสดงสัญญาทั้งหมด";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.Location = new System.Drawing.Point(288, 666);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(177, 26);
            this.label6.TabIndex = 14;
            this.label6.Text = "จำนวนใบเสร็จรับเงินทั้งหมด";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblTotalAmount.Location = new System.Drawing.Point(192, 666);
            this.lblTotalAmount.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(20, 26);
            this.lblTotalAmount.TabIndex = 16;
            this.lblTotalAmount.Text = "X";
            // 
            // lblTotalContractAccount
            // 
            this.lblTotalContractAccount.AutoSize = true;
            this.lblTotalContractAccount.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblTotalContractAccount.Location = new System.Drawing.Point(772, 666);
            this.lblTotalContractAccount.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTotalContractAccount.Name = "lblTotalContractAccount";
            this.lblTotalContractAccount.Size = new System.Drawing.Size(20, 26);
            this.lblTotalContractAccount.TabIndex = 17;
            this.lblTotalContractAccount.Text = "X";
            // 
            // lblTotalTaxInvoice
            // 
            this.lblTotalTaxInvoice.AutoSize = true;
            this.lblTotalTaxInvoice.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblTotalTaxInvoice.Location = new System.Drawing.Point(506, 666);
            this.lblTotalTaxInvoice.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTotalTaxInvoice.Name = "lblTotalTaxInvoice";
            this.lblTotalTaxInvoice.Size = new System.Drawing.Size(20, 26);
            this.lblTotalTaxInvoice.TabIndex = 18;
            this.lblTotalTaxInvoice.Text = "X";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btPrintReport);
            this.groupBox1.Controls.Add(this.DateTimePicker);
            this.groupBox1.Controls.Add(this.ButtonSearch);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1012, 76);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            // 
            // btPrintReport
            // 
            this.btPrintReport.BackColor = System.Drawing.Color.Transparent;
            this.btPrintReport.BorderColor = System.Drawing.Color.Transparent;
            this.btPrintReport.BorderWidth = 1;
            this.btPrintReport.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btPrintReport.ButtonText = "แสดงรายงาน";
            this.btPrintReport.CausesValidation = false;
            this.btPrintReport.EndColor = System.Drawing.Color.Silver;
            this.btPrintReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPrintReport.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btPrintReport.ForeColor = System.Drawing.Color.Black;
            this.btPrintReport.GradientAngle = 90;
            this.btPrintReport.Image = global::MyPayment.Properties.Resources.iconfinder_print_59498;
            this.btPrintReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrintReport.Location = new System.Drawing.Point(469, 22);
            this.btPrintReport.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btPrintReport.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btPrintReport.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btPrintReport.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btPrintReport.Name = "btPrintReport";
            this.btPrintReport.ShowButtontext = true;
            this.btPrintReport.Size = new System.Drawing.Size(138, 45);
            this.btPrintReport.StartColor = System.Drawing.Color.Gainsboro;
            this.btPrintReport.TabIndex = 1000000056;
            this.btPrintReport.TextLocation_X = 45;
            this.btPrintReport.TextLocation_Y = 10;
            this.btPrintReport.Transparent1 = 80;
            this.btPrintReport.Transparent2 = 120;
            this.btPrintReport.UseVisualStyleBackColor = true;
            this.btPrintReport.Click += new System.EventHandler(this.btPrintReport_Click);
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker.Location = new System.Drawing.Point(144, 31);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(171, 27);
            this.DateTimePicker.TabIndex = 1000000055;
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "ค้นหา";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.Silver;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = global::MyPayment.Properties.Resources.zoom;
            this.ButtonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSearch.Location = new System.Drawing.Point(333, 22);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(130, 45);
            this.ButtonSearch.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.TabIndex = 1000000054;
            this.ButtonSearch.TextLocation_X = 55;
            this.ButtonSearch.TextLocation_Y = 10;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.GridDetail);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 586);
            this.panel1.TabIndex = 20;
            // 
            // FormSummaryCashdeskOffline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblTotalTaxInvoice);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblTotalContractAccount);
            this.Controls.Add(this.lblTotalAmount);
            this.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "FormSummaryCashdeskOffline";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "จำนวนใบเสร็จรับเงินทั้งหมด";
            this.Load += new System.EventHandler(this.FormSummaryCashdeskOffline_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView GridDetail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTotalAmount;
        private System.Windows.Forms.Label lblTotalContractAccount;
        private System.Windows.Forms.Label lblTotalTaxInvoice;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private Custom_Controls_in_CS.ButtonZ btPrintReport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
    }
}