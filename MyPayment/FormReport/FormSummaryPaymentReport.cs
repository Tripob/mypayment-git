﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Models;
using MyPayment.Master;

namespace MyPayment.FormReport
{
    public partial class FormSummaryPaymentReport : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        public static List< ItemSummaryPaymentModel> LstISPM = new List<ItemSummaryPaymentModel>();
        public FormSummaryPaymentReport()
        {
            InitializeComponent();
        }
        private void FormSummaryPaymentReport_Load(object sender, EventArgs e)
        {
            try
            {
                GridDetail.AutoGenerateColumns = false;
                GridDetail.ColumnHeadersDefaultCellStyle.Font = new Font("TH Sarabun New", 14.25F, FontStyle.Bold);
                ItemSummaryPaymentModel Rec = new ItemSummaryPaymentModel();
                Rec.Title = "";
                LstISPM.Add(Rec);
            }
            catch(Exception ex)
            {

            }
        }
        private void InitForm()
        {
            try
            {
                ItemSummaryPaymentModel Rec = new ItemSummaryPaymentModel();
                Rec.Title = "";
                LstISPM.Add(Rec);
            }
            catch (Exception ex)
            {

            }
        }
        private void btnCallDataReport_Click(object sender, EventArgs e)
        {
           
        }
        private void btnShowReport_Click(object sender, EventArgs e)
        {
           
        }

        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int Year = DateTimePicker.Value.Year;
                if (Year > 2500)
                {
                    Year = Year - 543;
                }
                string strDate = Year.ToString() + "-" + DateTimePicker.Value.ToString("MM-dd");
                // var obj = OFC.ReadAllOffLineFileFromCSV(strDate);
                var obj = OFC.GetSummaryPaymentReport(strDate);

                GridDetail.DataSource = obj.Display;
                GlobalClass.LstSummaryPaymentModel = obj.Report;
            }
            catch (Exception ex)
            {

            }
        }

        private void btPrintReport_Click(object sender, EventArgs e)
        {
            GlobalClass.ReportName = "rptSummaryPaymentReport.rpt";
            GlobalClass.ReportOfflineType = "SUMMARYPAYMENT";
            FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();
            //formm receive = new FormReceiveMoney();
            FVR.ShowDialog();
        }
    }
}
