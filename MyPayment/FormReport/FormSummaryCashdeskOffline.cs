﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Master;
using MyPayment.Models;
using System.Globalization;
using System.Threading;
using RestSharp;
using Newtonsoft.Json.Linq;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormReport
{
    public partial class FormSummaryCashdeskOffline : Form
    {
        ClassLogsService CashdeskOfflineLog = new ClassLogsService("CashdeskOffline", "DataLog");
        ClassLogsService CashdeskOfflineEventLog = new ClassLogsService("CashdeskOffline", "EventLog");
        ClassLogsService CashdeskOfflineErrorLog = new ClassLogsService("CashdeskOffline", "ErrorLog");
        private static OffLineClass OFC = new OffLineClass();
        public FormSummaryCashdeskOffline()
        {
            InitializeComponent();
        }
        private void FormSummaryCashdeskOffline_Load(object sender, EventArgs e)
        {
            GridDetail.AutoGenerateColumns = false;
            CultureInfo ci = new CultureInfo("th-TH");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            //GridDetail.ColumnHeadersDefaultCellStyle.Font = new Font("TH Sarabun New", 14.25F, FontStyle.Bold);
            lblTotalAmount.Text = "";
            lblTotalContractAccount.Text = "";
            lblTotalTaxInvoice.Text = "";
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
                string strDate = DateTimePicker.Value.Date.ToString("yyyy-MM-dd");
                List<ItemOfflineModel> obj = OFC.GetSummaryCashdeskOfflineReport(strDate);
                GridDetail.AutoGenerateColumns = false;
                GridDetail.DataSource = obj;
                if (obj.Count > 0)
                {
                    GlobalClass.LstOfflineModel = obj;
                    decimal TotalAmount = obj.Sum(m => decimal.Parse(m.TotalAmountIncVat.ToString()));// obj.Sum(m => decimal.Parse(m.Amount));
                    decimal TotalContractAccount = obj.GroupBy(m => m.CA).Count();//obj.GroupBy(m => m.ContractAccount).Count();
                    decimal TotalTaxInvoice = obj.GroupBy(m => m.BillNumber).Count(); ;//obj.GroupBy(m => m.TaxInvoice).Count();
                    lblTotalAmount.Text = string.Format("{0:n}", TotalAmount);
                    lblTotalContractAccount.Text = string.Format("{0:n0}", TotalContractAccount);
                    lblTotalTaxInvoice.Text = string.Format("{0:n0}", TotalTaxInvoice);
                    GlobalClass.TotalAmount = string.Format("{0:n}", TotalAmount);
                    GlobalClass.TotalContractAccount = string.Format("{0:n0}", TotalContractAccount);
                    GlobalClass.TotalTaxInvoice = string.Format("{0:n0}", TotalTaxInvoice);
                }
                else
                    MessageBox.Show("ไม่มีข้อมูลอยู่ในระบบ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                CashdeskOfflineErrorLog.WriteData("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void btPrintReport_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                GlobalClass.ReportName = "rptSummaryCashdeskOffline.rpt";
                GlobalClass.ReportOfflineType = "ALLOFFLINE";
                FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();
                FVR.ShowDialog();
            }
            catch (Exception ex)
            {
                CashdeskOfflineErrorLog.WriteData("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
           
        }
    }
}
