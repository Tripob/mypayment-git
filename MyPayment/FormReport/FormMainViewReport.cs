﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Models;
using System.Configuration;

namespace MyPayment.FormReport
{
    public partial class FormMainViewReport : Form
    {
        // private static string ReportPath = ConfigurationManager.AppSettings["ReportPath"].ToString();
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        public FormMainViewReport()
        {
            InitializeComponent();
        }

        private void FormMainViewReport_Load(object sender, EventArgs e)
        {
            // string ReportName = GlobalClass.ReportName;
            LoadReport();
        }
        private void LoadReport()
        {
            try
            {
                object obj = new object();
                string ReportName = GlobalClass.ReportName;
                string Path = ReportPath + "\\" + ReportName;

                ReportDocument cryRpt = new ReportDocument();
                cryRpt.Load(Path);
                if (GlobalClass.ReportOfflineType == "CHEQUE")
                {
                    obj = GlobalClass.LstSummaryChequeModel;
                    cryRpt.SetDataSource(obj);

                }
                else if (GlobalClass.ReportOfflineType == "ALLOFFLINE")
                {

                    //List<ItemCodeData> LstData = new List<ItemCodeData>();
                    //ItemCodeData objData = new ItemCodeData();
                    //objData.ContractAccount = "Test 1";
                    //objData.LastDayMemo = "LastDayMemo 1";
                    //objData.TaxInvoice = "TaxInvoice 1";

                    //LstData.Add(objData);
                    //objData = new ItemCodeData();
                    //objData.ContractAccount = "Test 1";
                    //objData.LastDayMemo = "LastDayMemo 2";
                    //objData.TaxInvoice = "TaxInvoice 2";
                    //LstData.Add(objData);


                    //objData = new ItemCodeData();
                    //objData.ContractAccount = "Test 2";
                    //objData.LastDayMemo = "xxxxx 1";
                    //objData.TaxInvoice = "yyyyyyy 1";
                    //LstData.Add(objData);

                    //objData = new ItemCodeData();
                    //objData.ContractAccount = "Test 2";
                    //objData.LastDayMemo = "xxxxx 2";
                    //objData.TaxInvoice = "yyyyyyy 2";
                    //LstData.Add(objData);


                    //objData = new ItemCodeData();
                    //objData.ContractAccount = "Test 2";
                    //objData.LastDayMemo = "xxxxx 3";
                    //objData.TaxInvoice = "yyyyyyy 3";
                    //LstData.Add(objData);

                    //cryRpt.SetDataSource(LstData);

                    /* ใช้งานจริง  ****************************************/

                    obj = GlobalClass.LstOfflineModel;
                    cryRpt.SetDataSource(obj);

                    cryRpt.SetParameterValue("Param_Branch",  GlobalClass.BranchName + " " + GlobalClass.DistName);
                    cryRpt.SetParameterValue("Param_CashierNo", GlobalClass.CashierNo);

                    cryRpt.SetParameterValue("Param_TotalAmount", GlobalClass.TotalAmount);
                    cryRpt.SetParameterValue("Param_TotalContractAccount", GlobalClass.TotalContractAccount);
                    cryRpt.SetParameterValue("Param_TotalTaxInvoice", GlobalClass.TotalTaxInvoice);



                    /*****************************************/


                }
                else if (GlobalClass.ReportOfflineType == "CANCEL")
                {
                    obj = GlobalClass.LstRevertTaxInvoiceModel;
                    cryRpt.SetDataSource(obj);


                }
                else if (GlobalClass.ReportOfflineType == "SUMMARYPAYMENT")
                {
                    obj = GlobalClass.LstSummaryPaymentModel;
                    cryRpt.SetDataSource(obj);
                }
                else if (GlobalClass.ReportOfflineType == "TAXINVOICE")
                {
                    cryRpt.SetParameterValue("Param_TaxId", "Param_TaxId");
                    cryRpt.SetParameterValue("Param_BranchName", "Param_BranchName");
                    cryRpt.SetParameterValue("Param_Amount", "Param_Amount");
                    cryRpt.SetParameterValue("Param_Vat", "Param_Vat");
                    cryRpt.SetParameterValue("Param_SumVatAmount", "Param_SumVatAmount");
                    cryRpt.SetParameterValue("Param_FineFlakedOut", "Param_FineFlakedOut");
                    cryRpt.SetParameterValue("Param_Total", "Param_Total");
                    cryRpt.SetParameterValue("Param_ContractAccount", "Param_ContractAccount");
                    cryRpt.SetParameterValue("Param_TaxInvoice", "Param_ContractAccount");
                    cryRpt.SetParameterValue("Param_TotalThai", "Param_TotalThai");
                    cryRpt.SetParameterValue("Param_ElectricBillUnit", "Param_ElectricBillUnit");
                    
                }






                crvMainReport.ReportSource = cryRpt;
               
                crvMainReport.Refresh();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                if(ex.InnerException != null)
                {
                    MessageBox.Show(ex.InnerException.Message);
                }
            }
        }

       
    }
}
