﻿namespace MyPayment.FormReport
{
    partial class FormMainViewReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crvMainReport = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crvMainReport
            // 
            this.crvMainReport.ActiveViewIndex = -1;
            this.crvMainReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvMainReport.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvMainReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvMainReport.Location = new System.Drawing.Point(0, 0);
            this.crvMainReport.Name = "crvMainReport";
            this.crvMainReport.Size = new System.Drawing.Size(800, 450);
            this.crvMainReport.TabIndex = 0;
            // 
            // FormMainViewReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.crvMainReport);
            this.Name = "FormMainViewReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMainViewReport";
            this.Load += new System.EventHandler(this.FormMainViewReport_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvMainReport;
    }
}