﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Master;
using MyPayment.Models;

namespace MyPayment.FormReport
{
    public partial class FormRevertReport : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        public FormRevertReport()
        {
            InitializeComponent();
        }
        private void FormRevertReport_Load(object sender, EventArgs e)
        {
            GridDetail.ColumnHeadersDefaultCellStyle.Font = new Font("TH Sarabun New", 14.25F, FontStyle.Bold);
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            int Year = DateTimePicker.Value.Year;
            if (Year > 2500)
                Year = Year - 543;
            string strDate = Year.ToString() + "-" + DateTimePicker.Value.ToString("MM-dd");
            var obj = OFC.GetRevertReport(strDate);
            GridDetail.DataSource = obj;
            GlobalClass.LstRevertTaxInvoiceModel = obj;
        }
        private void btPrintReport_Click(object sender, EventArgs e)
        {
            GlobalClass.ReportName = "rptRevertReport.rpt";
            GlobalClass.ReportOfflineType = "CANCEL";
            FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();
            //formm receive = new FormReceiveMoney();
            FVR.ShowDialog();
        }
    }
}
