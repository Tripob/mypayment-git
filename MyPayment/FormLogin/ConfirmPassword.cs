﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.FormLogin
{
    public partial class ConfirmPassword : Form
    {
        ArrearsController arrears = new ArrearsController();
        GeneralService genService = new GeneralService(true);
        ClassLogsService meaLog = new ClassLogsService("ChangePassword", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("ChangePassword", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("ChangePassword", "ErrorLog");
        private string strTemp;
        public ConfirmPassword()
        {
            InitializeComponent();
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (TextboxUserId.Text == string.Empty || TextboxPassOld.Text == string.Empty || TextboxPassNew.Text == string.Empty)
                {
                    string mes;
                    if (TextboxUserId.Text == string.Empty)
                    {
                        TextboxUserId.Focus();
                        mes = "กรุณากรอก รหัสพนักงาน";
                    }
                    else if (TextboxPassOld.Text == string.Empty)
                    {
                        TextboxPassOld.Focus();
                        mes = "กรุณากรอก รหัสผ่านเก่า";
                    }
                    else
                    {
                        TextboxPassNew.Focus();
                        mes = "กรุณากรอก รหัสผ่านใหม่";
                    }
                    MessageBox.Show(mes, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    GlobalClass.Url = ConfigurationManager.AppSettings["UAT"].ToString();
                    ClassUser clsuser = new ClassUser();
                    clsuser.empId = TextboxUserId.Text.Trim();
                    clsuser.oldPassword = TextboxPassOld.Text.Trim();
                    clsuser.password = TextboxPassNew.Text.Trim();
                    ClassUser clsLogin = arrears.ChangePassword(clsuser, "changePassword");

                    strTemp = TextboxUserId.Text;
                    if (clsLogin.result_code == "SUCCESS")
                    {
                        if (clsLogin.cashDeskIpFlag == "N")
                            MessageBox.Show("เครื่องนี้ไม่ใช่สำหรับรับชำระ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        else
                        {
                            LoginAdmin login = new LoginAdmin();
                            login.Show();
                            this.Hide();
                            meaLog.WriteData("Success ==> User :: " + strTemp, method, LogLevel.Debug);
                        }
                    }
                    else
                    {
                        meaLogEvent.WriteDataEvent("Fail ==> User :: " + strTemp + " | Message :: " + clsLogin.result_message, method, LogLevel.Debug);
                        MessageBox.Show(clsLogin.result_message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch (Exception ex)
            {
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            LoginAdmin login = new LoginAdmin();
            login.Show();
            this.Hide();
        }
        private void TextboxUserId_TextChanged(object sender, EventArgs e)
        {
            if (TextboxUserId.Text == "")
                LabelHolderId.Visible = true;
            else
                LabelHolderId.Visible = false;
        }
        private void TextboxPassOld_TextChanged(object sender, EventArgs e)
        {
            if (TextboxPassOld.Text == "")
                LabelHolderPassOld.Visible = true;
            else
                LabelHolderPassOld.Visible = false;
        }
        private void TextboxPassNew_TextChanged(object sender, EventArgs e)
        {
            if (TextboxPassNew.Text == "")
                LabelHolderPassNew.Visible = true;
            else
                LabelHolderPassNew.Visible = false;
        }
        private void LabelHolderId_Click(object sender, EventArgs e)
        {
            this.TextboxUserId.Focus();
        }
        private void LabelHolderPassOld_Click(object sender, EventArgs e)
        {
            this.TextboxPassOld.Focus();
        }
        private void LabelHolderPassNew_Click(object sender, EventArgs e)
        {
            this.TextboxPassNew.Focus();
        }
    }
}
