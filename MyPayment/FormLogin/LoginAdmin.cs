﻿using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using static MyPayment.Models.ClassLogsService;
using MyPayment.Controllers;
using static MyPayment.Models.ClassReadAPI;
using MyPayment.Master;

namespace MyPayment.FormLogin
{
    public partial class LoginAdmin : Form
    {
        private string strTemp;
        private static OffLineClass OFC = new OffLineClass();
        GeneralService genService = new GeneralService(true);
        ArrearsController arrears = new ArrearsController();
        ClassLogsService meaLog = new ClassLogsService("Loging", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("Loging", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("Loging", "ErrorLog");
        public LoginAdmin()
        {
            InitializeComponent();
        }
        private void linkLabelchangePass_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ConfirmPassword confirm = new ConfirmPassword();
            confirm.Show();
            this.Hide();
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            Environment.Exit(0);
        }
        private void LabelHolderId_Click(object sender, EventArgs e)
        {
            this.TextboxIdUser.Focus();
        }
        private void LabelHolderPass_Click(object sender, EventArgs e)
        {
            this.TextboxPass.Focus();
        }
        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                string loging = ConfigurationManager.AppSettings["StatusLoging"].ToString();
                string systems = "";
                if (radioButtonOnline.Checked)
                {
                    systems = "Online";
                    genService = new GeneralService(true);
                    GlobalClass.IsConnectOnline = true;
                }
                else
                {
                    systems = "Offline";
                    genService = new GeneralService(false);
                    GlobalClass.IsConnectOnline = false;
                }

                if (TextboxIdUser.Text == string.Empty || TextboxPass.Text == string.Empty)
                {
                    string mes;
                    if (TextboxIdUser.Text == string.Empty)
                    {
                        TextboxIdUser.Focus();
                        mes = "กรุณากรอก รหัสผู้ใช้งาน";
                    }
                    else
                    {
                        TextboxPass.Focus();
                        mes = "กรุณากรอก รหัสผ่าน";
                    }

                    if (loging == "1")
                    {
                        if (TextBoxIPAddess.Text == string.Empty)
                        {
                            TextBoxIPAddess.Focus();
                            mes = "กรุณากรอก IP Addess";
                        }
                    }
                    MessageBox.Show(mes, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    #region Online
                    if (radioButtonOnline.Checked)
                    {
                        string status = ConfigurationManager.AppSettings["StatusAdmin"].ToString();
                        string ip;
                        if (status == "0")
                            ip = ConfigurationManager.AppSettings["IPAddress"].ToString();
                        else
                            ip = arrears.GetIPAddress();
                        GlobalClass.Timer = Convert.ToDouble(ConfigurationManager.AppSettings["Timer"].ToString());
                        GlobalClass.Url = ConfigurationManager.AppSettings["UAT"].ToString();
                        GlobalClass.IpConfig = ip;
                        if (loging == "1")
                        {
                            if (comboBoxClient.SelectedIndex == 0)
                                GlobalClass.Url = ConfigurationManager.AppSettings["UAT"].ToString();
                            else
                                GlobalClass.Url = ConfigurationManager.AppSettings["Production"].ToString();
                            ip = TextBoxIPAddess.Text;
                        }
                        ClassLogin login = new ClassLogin();
                        login.empId = TextboxIdUser.Text.Trim();
                        login.password = TextboxPass.Text.Trim();
                        login.ipAddress = ip;
                        ClassUser clsLogin = arrears.loadDataLogin(login, "login");
                        strTemp = TextboxIdUser.Text;
                        if (clsLogin.result_code == "SUCCESS")
                        {
                            string qServerIp = "";
                            qServerIp = clsLogin.qServerIp != null ? clsLogin.qServerIp.Trim() : "";
                            if (qServerIp != null && qServerIp != "")
                            {
                                string[] Qserver = clsLogin.qServerIp.Split(':');
                                GlobalClass.IpServerQ = Qserver[0].ToString();
                                GlobalClass.PortQ = Qserver[1].ToString();
                            }
                            else
                                GlobalClass.IpServerQ = "";
                            GlobalClass.UserId = Convert.ToInt32(clsLogin.empId);
                            GlobalClass.UserName = clsLogin.userName;
                            GlobalClass.Dist = clsLogin.distId + "-" + clsLogin.distName;
                            GlobalClass.No = clsLogin.counterNo;
                            GlobalClass.DistName = clsLogin.distName + "  สาขา  " + clsLogin.receiptBranch;
                            GlobalClass.ReceiptBranch = clsLogin.receiptBranch;
                            GlobalClass.DistId = Convert.ToInt32(clsLogin.distId);
                            GlobalClass.SubDistId = clsLogin.subDistId;
                            GlobalClass.UserIdLogin = clsLogin.userId;
                            this.Hide();
                            #region setDataOffline
                            ItemUserOfflineModel objData = new ItemUserOfflineModel();
                            objData = OFC.GetUserOffline(TextboxIdUser.Text.Trim(), TextboxPass.Text.Trim());
                            if (objData == null)
                            {
                                objData = new ItemUserOfflineModel();
                                objData.UserCode = GlobalClass.UserId.ToString();
                                objData.UserName = GlobalClass.UserName;
                                objData.distId = Convert.ToInt32(clsLogin.distId);
                                objData.cashierNo = int.Parse(clsLogin.counterNo);
                                objData.Password = TextboxPass.Text.Trim();
                                objData.distName = clsLogin.distName;
                                objData.receiptBranch = clsLogin.receiptBranch;
                                OFC.InsertUserOffline(objData);
                            }
                            #endregion
                            Main main = new Main();
                            main.Show();
                            meaLog.WriteData("Success ==> System Connect :: " + systems + " | User :: " + strTemp, method, LogLevel.Debug);
                        }
                        else
                        {
                            if (clsLogin.result_code == "EL007")
                            {
                                TextboxIdUser.Text = string.Empty;
                                TextboxIdUser.Focus();
                            }
                            else if (clsLogin.result_code == "EL005" || clsLogin.result_code == "EL001")
                            {
                                TextboxPass.Text = string.Empty;
                                TextboxPass.Focus();
                            }
                            else
                            {
                                TextboxPass.Text = string.Empty;
                                TextboxIdUser.Text = string.Empty;
                                TextboxIdUser.Focus();
                            }

                            meaLogEvent.WriteDataEvent("Fail ==> System Connect :: " + systems + " | User :: " + strTemp + " | Message :: " + clsLogin.result_message, method, LogLevel.Debug);
                            MessageBox.Show(clsLogin.result_message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    #endregion

                    #region Offline
                    else if (radioButtonSemi.Checked)
                    {
                        bool isValid = OFC.LoginOffline(TextboxIdUser.Text.Trim(), TextboxPass.Text.Trim());
                        if (isValid)
                        {
                            GlobalClass.Url = ConfigurationManager.AppSettings["UAT"].ToString();
                            this.Hide();
                            Main main = new Main();
                            main.Show();
                            meaLog.WriteData("Success ==> System Connect :: " + systems + " | User :: " + strTemp, method, LogLevel.Debug);
                        }
                    }
                    #endregion     
                }
            }
            catch (Exception ex)
            {
                TextboxPass.Text = string.Empty;
                TextboxIdUser.Text = string.Empty;
                TextboxIdUser.Focus();
                meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("เกิดข้อผิดพลาดในการเข้าใช้งานระบบ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void TextboxIdUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                if (TextboxIdUser.Text == string.Empty)
                    TextboxIdUser.Focus();
                else if (TextboxIdUser.Text != string.Empty && TextboxPass.Text != string.Empty)
                    ButtonLogin_Click(sender, e);
                else
                    TextboxPass.Focus();
            }
        }
        private void TextboxPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                if (TextboxPass.Text == string.Empty)
                    TextboxPass.Focus();
                else
                {
                    string loging = ConfigurationManager.AppSettings["StatusLoging"].ToString();
                    if (loging == "0")
                    {
                        if (TextboxPass.Text != string.Empty && TextboxIdUser.Text != string.Empty)
                            ButtonLogin_Click(sender, e);
                    }
                    else
                        TextBoxIPAddess.Focus();
                }
            }
        }
        private void TextboxPass_TextChanged(object sender, EventArgs e)
        {
            if (TextboxPass.Text == "")
                LabelHolderPass.Visible = true;
            else
                LabelHolderPass.Visible = false;
        }
        private void TextboxIdUser_TextChanged(object sender, EventArgs e)
        {
            if (TextboxIdUser.Text == "")
                LabelHolderId.Visible = true;
            else
                LabelHolderId.Visible = false;
        }
        private void LabelHolderIPAddess_Click(object sender, EventArgs e)
        {
            this.TextBoxIPAddess.Focus();
        }
        private void TextBoxIPAddess_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                if (TextBoxIPAddess.Text == string.Empty)
                    TextBoxIPAddess.Focus();
                else
                    comboBoxClient.Focus();
            }
        }
        private void comboBoxClient_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void TextBoxIPAddess_TextChanged(object sender, EventArgs e)
        {
            if (TextBoxIPAddess.Text == "")
                LabelHolderIPAddess.Visible = true;
            else
                LabelHolderIPAddess.Visible = false;
        }
        private void LoginAdmin_Load(object sender, EventArgs e)
        {
            lblVersion.Text = Application.ProductVersion;
            string loging = ConfigurationManager.AppSettings["StatusLoging"].ToString();
            if (loging == "0")
                this.Size = new Size(550, 302);
            else
                this.Size = new Size(550, 424);
            radioButtonOnline.Checked = true;
            comboBoxClient.SelectedIndex = 0;

            //var capsLockIsOn = InputLanguage.CurrentInputLanguage;
            //string lan = capsLockIsOn.Culture.Name;
            //if (lan == "th-TH")
            //    lblLanguage.Text = "ไทย";
            //else
            //    lblLanguage.Text = "ENG";
        }
    }
}
