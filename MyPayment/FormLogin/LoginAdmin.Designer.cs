﻿namespace MyPayment.FormLogin
{
    partial class LoginAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginAdmin));
            this.comboBoxClient = new System.Windows.Forms.ComboBox();
            this.labelIPAddress = new System.Windows.Forms.Label();
            this.labelClient = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radioButtonSemi = new System.Windows.Forms.RadioButton();
            this.radioButtonOnline = new System.Windows.Forms.RadioButton();
            this.labelSystemConnect = new System.Windows.Forms.Label();
            this.GroupBoxLogin = new System.Windows.Forms.GroupBox();
            this.LabelHolderPass = new System.Windows.Forms.Label();
            this.TextboxPass = new System.Windows.Forms.TextBox();
            this.LabelHolderId = new System.Windows.Forms.Label();
            this.TextboxIdUser = new System.Windows.Forms.TextBox();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonLogin = new Custom_Controls_in_CS.ButtonZ();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabelchangePass = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelHolderIPAddess = new System.Windows.Forms.Label();
            this.TextBoxIPAddess = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblLanguage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.GroupBoxLogin.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxClient
            // 
            this.comboBoxClient.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.comboBoxClient.FormattingEnabled = true;
            this.comboBoxClient.Items.AddRange(new object[] {
            "UAT",
            "Production"});
            this.comboBoxClient.Location = new System.Drawing.Point(238, 64);
            this.comboBoxClient.Name = "comboBoxClient";
            this.comboBoxClient.Size = new System.Drawing.Size(270, 37);
            this.comboBoxClient.TabIndex = 1;
            this.comboBoxClient.SelectedIndexChanged += new System.EventHandler(this.comboBoxClient_SelectedIndexChanged);
            // 
            // labelIPAddress
            // 
            this.labelIPAddress.AutoSize = true;
            this.labelIPAddress.BackColor = System.Drawing.Color.Transparent;
            this.labelIPAddress.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelIPAddress.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labelIPAddress.Location = new System.Drawing.Point(128, 19);
            this.labelIPAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIPAddress.Name = "labelIPAddress";
            this.labelIPAddress.Size = new System.Drawing.Size(94, 32);
            this.labelIPAddress.TabIndex = 2;
            this.labelIPAddress.Text = "IP Address";
            // 
            // labelClient
            // 
            this.labelClient.AutoSize = true;
            this.labelClient.BackColor = System.Drawing.Color.Transparent;
            this.labelClient.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labelClient.Location = new System.Drawing.Point(162, 66);
            this.labelClient.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(60, 32);
            this.labelClient.TabIndex = 3;
            this.labelClient.Text = "Client";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MyPayment.Properties.Resources.MEA_new_colorcenter;
            this.pictureBox1.Location = new System.Drawing.Point(8, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(214, 175);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // radioButtonSemi
            // 
            this.radioButtonSemi.AutoSize = true;
            this.radioButtonSemi.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButtonSemi.Location = new System.Drawing.Point(329, 206);
            this.radioButtonSemi.Name = "radioButtonSemi";
            this.radioButtonSemi.Size = new System.Drawing.Size(81, 36);
            this.radioButtonSemi.TabIndex = 5;
            this.radioButtonSemi.TabStop = true;
            this.radioButtonSemi.Text = "Offline";
            this.radioButtonSemi.UseVisualStyleBackColor = true;
            // 
            // radioButtonOnline
            // 
            this.radioButtonOnline.AutoSize = true;
            this.radioButtonOnline.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButtonOnline.Location = new System.Drawing.Point(238, 206);
            this.radioButtonOnline.Name = "radioButtonOnline";
            this.radioButtonOnline.Size = new System.Drawing.Size(80, 36);
            this.radioButtonOnline.TabIndex = 4;
            this.radioButtonOnline.TabStop = true;
            this.radioButtonOnline.Text = "Online";
            this.radioButtonOnline.UseVisualStyleBackColor = true;
            // 
            // labelSystemConnect
            // 
            this.labelSystemConnect.AutoSize = true;
            this.labelSystemConnect.BackColor = System.Drawing.Color.Transparent;
            this.labelSystemConnect.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSystemConnect.Location = new System.Drawing.Point(82, 208);
            this.labelSystemConnect.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSystemConnect.Name = "labelSystemConnect";
            this.labelSystemConnect.Size = new System.Drawing.Size(140, 32);
            this.labelSystemConnect.TabIndex = 17;
            this.labelSystemConnect.Text = "System Connect";
            // 
            // GroupBoxLogin
            // 
            this.GroupBoxLogin.Controls.Add(this.lblLanguage);
            this.GroupBoxLogin.Controls.Add(this.LabelHolderPass);
            this.GroupBoxLogin.Controls.Add(this.TextboxPass);
            this.GroupBoxLogin.Controls.Add(this.LabelHolderId);
            this.GroupBoxLogin.Controls.Add(this.TextboxIdUser);
            this.GroupBoxLogin.Controls.Add(this.ButtonCancel);
            this.GroupBoxLogin.Controls.Add(this.ButtonLogin);
            this.GroupBoxLogin.Controls.Add(this.label1);
            this.GroupBoxLogin.Controls.Add(this.radioButtonSemi);
            this.GroupBoxLogin.Controls.Add(this.linkLabelchangePass);
            this.GroupBoxLogin.Controls.Add(this.radioButtonOnline);
            this.GroupBoxLogin.Controls.Add(this.pictureBox1);
            this.GroupBoxLogin.Controls.Add(this.labelSystemConnect);
            this.GroupBoxLogin.Location = new System.Drawing.Point(8, 11);
            this.GroupBoxLogin.Name = "GroupBoxLogin";
            this.GroupBoxLogin.Size = new System.Drawing.Size(521, 250);
            this.GroupBoxLogin.TabIndex = 0;
            this.GroupBoxLogin.TabStop = false;
            // 
            // LabelHolderPass
            // 
            this.LabelHolderPass.AutoSize = true;
            this.LabelHolderPass.BackColor = System.Drawing.Color.White;
            this.LabelHolderPass.Font = new System.Drawing.Font("TH Sarabun New", 17.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LabelHolderPass.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.LabelHolderPass.Location = new System.Drawing.Point(249, 103);
            this.LabelHolderPass.Name = "LabelHolderPass";
            this.LabelHolderPass.Size = new System.Drawing.Size(140, 31);
            this.LabelHolderPass.TabIndex = 23;
            this.LabelHolderPass.Text = "กรุณากรอก รหัสผ่าน";
            this.LabelHolderPass.Click += new System.EventHandler(this.LabelHolderPass_Click);
            // 
            // TextboxPass
            // 
            this.TextboxPass.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.TextboxPass.Location = new System.Drawing.Point(238, 98);
            this.TextboxPass.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxPass.Name = "TextboxPass";
            this.TextboxPass.PasswordChar = '*';
            this.TextboxPass.Size = new System.Drawing.Size(270, 37);
            this.TextboxPass.TabIndex = 1;
            this.TextboxPass.TextChanged += new System.EventHandler(this.TextboxPass_TextChanged);
            this.TextboxPass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxPass_KeyPress);
            // 
            // LabelHolderId
            // 
            this.LabelHolderId.AutoSize = true;
            this.LabelHolderId.BackColor = System.Drawing.Color.White;
            this.LabelHolderId.Font = new System.Drawing.Font("TH Sarabun New", 17.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LabelHolderId.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.LabelHolderId.Location = new System.Drawing.Point(249, 53);
            this.LabelHolderId.Name = "LabelHolderId";
            this.LabelHolderId.Size = new System.Drawing.Size(167, 31);
            this.LabelHolderId.TabIndex = 22;
            this.LabelHolderId.Text = "กรุณากรอก รหัสพนักงาน";
            this.LabelHolderId.Click += new System.EventHandler(this.LabelHolderId_Click);
            // 
            // TextboxIdUser
            // 
            this.TextboxIdUser.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.TextboxIdUser.Location = new System.Drawing.Point(238, 51);
            this.TextboxIdUser.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxIdUser.Name = "TextboxIdUser";
            this.TextboxIdUser.Size = new System.Drawing.Size(270, 37);
            this.TextboxIdUser.TabIndex = 0;
            this.TextboxIdUser.TextChanged += new System.EventHandler(this.TextboxIdUser_TextChanged);
            this.TextboxIdUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxIdUser_KeyPress);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close1;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(381, 156);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(127, 40);
            this.ButtonCancel.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCancel.TabIndex = 3;
            this.ButtonCancel.TextLocation_X = 45;
            this.ButtonCancel.TextLocation_Y = 5;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // ButtonLogin
            // 
            this.ButtonLogin.BackColor = System.Drawing.Color.Transparent;
            this.ButtonLogin.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonLogin.BorderWidth = 1;
            this.ButtonLogin.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonLogin.ButtonText = "ตกลง";
            this.ButtonLogin.CausesValidation = false;
            this.ButtonLogin.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLogin.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonLogin.ForeColor = System.Drawing.Color.Black;
            this.ButtonLogin.GradientAngle = 90;
            this.ButtonLogin.Image = global::MyPayment.Properties.Resources.ok;
            this.ButtonLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonLogin.Location = new System.Drawing.Point(238, 156);
            this.ButtonLogin.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonLogin.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonLogin.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonLogin.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonLogin.Name = "ButtonLogin";
            this.ButtonLogin.ShowButtontext = true;
            this.ButtonLogin.Size = new System.Drawing.Size(127, 40);
            this.ButtonLogin.StartColor = System.Drawing.Color.LightGray;
            this.ButtonLogin.TabIndex = 2;
            this.ButtonLogin.TextLocation_X = 45;
            this.ButtonLogin.TextLocation_Y = 5;
            this.ButtonLogin.Transparent1 = 80;
            this.ButtonLogin.Transparent2 = 120;
            this.ButtonLogin.UseVisualStyleBackColor = true;
            this.ButtonLogin.Click += new System.EventHandler(this.ButtonLogin_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(4, -9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 32);
            this.label1.TabIndex = 3;
            this.label1.Text = "Login";
            // 
            // linkLabelchangePass
            // 
            this.linkLabelchangePass.AutoSize = true;
            this.linkLabelchangePass.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelchangePass.LinkColor = System.Drawing.Color.Blue;
            this.linkLabelchangePass.Location = new System.Drawing.Point(405, 21);
            this.linkLabelchangePass.Name = "linkLabelchangePass";
            this.linkLabelchangePass.Size = new System.Drawing.Size(101, 28);
            this.linkLabelchangePass.TabIndex = 6;
            this.linkLabelchangePass.TabStop = true;
            this.linkLabelchangePass.Text = "แก้ไขรหัสผ่าน";
            this.linkLabelchangePass.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelchangePass_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LabelHolderIPAddess);
            this.groupBox1.Controls.Add(this.TextBoxIPAddess);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.labelIPAddress);
            this.groupBox1.Controls.Add(this.labelClient);
            this.groupBox1.Controls.Add(this.comboBoxClient);
            this.groupBox1.Location = new System.Drawing.Point(8, 271);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(521, 109);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // LabelHolderIPAddess
            // 
            this.LabelHolderIPAddess.AutoSize = true;
            this.LabelHolderIPAddess.BackColor = System.Drawing.Color.White;
            this.LabelHolderIPAddess.Font = new System.Drawing.Font("TH Sarabun New", 17.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LabelHolderIPAddess.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.LabelHolderIPAddess.Location = new System.Drawing.Point(249, 20);
            this.LabelHolderIPAddess.Name = "LabelHolderIPAddess";
            this.LabelHolderIPAddess.Size = new System.Drawing.Size(158, 31);
            this.LabelHolderIPAddess.TabIndex = 14;
            this.LabelHolderIPAddess.Text = "กรุณากรอก IP Address";
            this.LabelHolderIPAddess.Click += new System.EventHandler(this.LabelHolderIPAddess_Click);
            // 
            // TextBoxIPAddess
            // 
            this.TextBoxIPAddess.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.TextBoxIPAddess.Location = new System.Drawing.Point(238, 17);
            this.TextBoxIPAddess.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxIPAddess.Name = "TextBoxIPAddess";
            this.TextBoxIPAddess.Size = new System.Drawing.Size(270, 37);
            this.TextBoxIPAddess.TabIndex = 0;
            this.TextBoxIPAddess.TextChanged += new System.EventHandler(this.TextBoxIPAddess_TextChanged);
            this.TextBoxIPAddess.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxIPAddess_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(4, -10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 32);
            this.label2.TabIndex = 4;
            this.label2.Text = "Setting";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(480, 2);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(13, 13);
            this.lblVersion.TabIndex = 29;
            this.lblVersion.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(392, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Product Version :";
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.BackColor = System.Drawing.Color.Transparent;
            this.lblLanguage.Enabled = false;
            this.lblLanguage.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLanguage.ForeColor = System.Drawing.Color.Red;
            this.lblLanguage.Location = new System.Drawing.Point(1, 217);
            this.lblLanguage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(40, 28);
            this.lblLanguage.TabIndex = 24;
            this.lblLanguage.Text = "END";
            this.lblLanguage.Visible = false;
            // 
            // LoginAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(534, 385);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.GroupBoxLogin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบบรับชำระ";
            this.Load += new System.EventHandler(this.LoginAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.GroupBoxLogin.ResumeLayout(false);
            this.GroupBoxLogin.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelIPAddress;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.ComboBox comboBoxClient;
        private System.Windows.Forms.RadioButton radioButtonSemi;
        private System.Windows.Forms.RadioButton radioButtonOnline;
        private System.Windows.Forms.Label labelSystemConnect;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox GroupBoxLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabelchangePass;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        private Custom_Controls_in_CS.ButtonZ ButtonLogin;
        private System.Windows.Forms.Label LabelHolderPass;
        private System.Windows.Forms.TextBox TextboxPass;
        private System.Windows.Forms.Label LabelHolderId;
        private System.Windows.Forms.TextBox TextboxIdUser;
        private System.Windows.Forms.Label LabelHolderIPAddess;
        private System.Windows.Forms.TextBox TextBoxIPAddess;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLanguage;
    }
}