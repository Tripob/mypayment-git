﻿namespace MyPayment.FormLogin
{
    partial class ConfirmPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmPassword));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelHolderPassNew = new System.Windows.Forms.Label();
            this.TextboxPassNew = new System.Windows.Forms.TextBox();
            this.LabelHolderPassOld = new System.Windows.Forms.Label();
            this.TextboxPassOld = new System.Windows.Forms.TextBox();
            this.LabelHolderId = new System.Windows.Forms.Label();
            this.TextboxUserId = new System.Windows.Forms.TextBox();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonOk = new Custom_Controls_in_CS.ButtonZ();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.LabelHolderPassNew);
            this.groupBox1.Controls.Add(this.TextboxPassNew);
            this.groupBox1.Controls.Add(this.LabelHolderPassOld);
            this.groupBox1.Controls.Add(this.TextboxPassOld);
            this.groupBox1.Controls.Add(this.LabelHolderId);
            this.groupBox1.Controls.Add(this.TextboxUserId);
            this.groupBox1.Controls.Add(this.ButtonCancel);
            this.groupBox1.Controls.Add(this.ButtonOk);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(529, 204);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // LabelHolderPassNew
            // 
            this.LabelHolderPassNew.AutoSize = true;
            this.LabelHolderPassNew.BackColor = System.Drawing.Color.White;
            this.LabelHolderPassNew.Font = new System.Drawing.Font("TH Sarabun New", 17.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LabelHolderPassNew.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.LabelHolderPassNew.Location = new System.Drawing.Point(257, 102);
            this.LabelHolderPassNew.Name = "LabelHolderPassNew";
            this.LabelHolderPassNew.Size = new System.Drawing.Size(165, 31);
            this.LabelHolderPassNew.TabIndex = 7;
            this.LabelHolderPassNew.Text = "กรุณากรอก รหัสผ่านใหม่";
            this.LabelHolderPassNew.Click += new System.EventHandler(this.LabelHolderPassNew_Click);
            // 
            // TextboxPassNew
            // 
            this.TextboxPassNew.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.TextboxPassNew.Location = new System.Drawing.Point(246, 100);
            this.TextboxPassNew.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxPassNew.Name = "TextboxPassNew";
            this.TextboxPassNew.PasswordChar = '*';
            this.TextboxPassNew.Size = new System.Drawing.Size(270, 37);
            this.TextboxPassNew.TabIndex = 2;
            this.TextboxPassNew.TextChanged += new System.EventHandler(this.TextboxPassNew_TextChanged);
            // 
            // LabelHolderPassOld
            // 
            this.LabelHolderPassOld.AutoSize = true;
            this.LabelHolderPassOld.BackColor = System.Drawing.Color.White;
            this.LabelHolderPassOld.Font = new System.Drawing.Font("TH Sarabun New", 17.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LabelHolderPassOld.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.LabelHolderPassOld.Location = new System.Drawing.Point(257, 61);
            this.LabelHolderPassOld.Name = "LabelHolderPassOld";
            this.LabelHolderPassOld.Size = new System.Drawing.Size(161, 31);
            this.LabelHolderPassOld.TabIndex = 6;
            this.LabelHolderPassOld.Text = "กรุณากรอก รหัสผ่านเก่า";
            this.LabelHolderPassOld.Click += new System.EventHandler(this.LabelHolderPassOld_Click);
            // 
            // TextboxPassOld
            // 
            this.TextboxPassOld.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.TextboxPassOld.Location = new System.Drawing.Point(246, 59);
            this.TextboxPassOld.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxPassOld.Name = "TextboxPassOld";
            this.TextboxPassOld.PasswordChar = '*';
            this.TextboxPassOld.Size = new System.Drawing.Size(270, 37);
            this.TextboxPassOld.TabIndex = 1;
            this.TextboxPassOld.TextChanged += new System.EventHandler(this.TextboxPassOld_TextChanged);
            // 
            // LabelHolderId
            // 
            this.LabelHolderId.AutoSize = true;
            this.LabelHolderId.BackColor = System.Drawing.Color.White;
            this.LabelHolderId.Font = new System.Drawing.Font("TH Sarabun New", 17.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LabelHolderId.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.LabelHolderId.Location = new System.Drawing.Point(257, 20);
            this.LabelHolderId.Name = "LabelHolderId";
            this.LabelHolderId.Size = new System.Drawing.Size(167, 31);
            this.LabelHolderId.TabIndex = 5;
            this.LabelHolderId.Text = "กรุณากรอก รหัสพนักงาน";
            this.LabelHolderId.Click += new System.EventHandler(this.LabelHolderId_Click);
            // 
            // TextboxUserId
            // 
            this.TextboxUserId.Font = new System.Drawing.Font("CordiaUPC", 15.75F);
            this.TextboxUserId.Location = new System.Drawing.Point(246, 18);
            this.TextboxUserId.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxUserId.Name = "TextboxUserId";
            this.TextboxUserId.Size = new System.Drawing.Size(270, 37);
            this.TextboxUserId.TabIndex = 0;
            this.TextboxUserId.TextChanged += new System.EventHandler(this.TextboxUserId_TextChanged);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close1;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(389, 153);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(127, 40);
            this.ButtonCancel.StartColor = System.Drawing.Color.LightGray;
            this.ButtonCancel.TabIndex = 4;
            this.ButtonCancel.TextLocation_X = 45;
            this.ButtonCancel.TextLocation_Y = 5;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // ButtonOk
            // 
            this.ButtonOk.BackColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonOk.BorderWidth = 1;
            this.ButtonOk.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonOk.ButtonText = "ตกลง";
            this.ButtonOk.CausesValidation = false;
            this.ButtonOk.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonOk.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ButtonOk.ForeColor = System.Drawing.Color.Black;
            this.ButtonOk.GradientAngle = 90;
            this.ButtonOk.Image = global::MyPayment.Properties.Resources.ok;
            this.ButtonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonOk.Location = new System.Drawing.Point(246, 153);
            this.ButtonOk.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonOk.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonOk.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonOk.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonOk.Name = "ButtonOk";
            this.ButtonOk.ShowButtontext = true;
            this.ButtonOk.Size = new System.Drawing.Size(127, 40);
            this.ButtonOk.StartColor = System.Drawing.Color.LightGray;
            this.ButtonOk.TabIndex = 3;
            this.ButtonOk.TextLocation_X = 45;
            this.ButtonOk.TextLocation_Y = 5;
            this.ButtonOk.Transparent1 = 80;
            this.ButtonOk.Transparent2 = 120;
            this.ButtonOk.UseVisualStyleBackColor = true;
            this.ButtonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MyPayment.Properties.Resources.MEA_new_colorcenter;
            this.pictureBox1.Location = new System.Drawing.Point(13, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(214, 175);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // ConfirmPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(529, 204);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmPassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ยืนยันระหัสผ่านใหม่";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        private Custom_Controls_in_CS.ButtonZ ButtonOk;
        private System.Windows.Forms.Label LabelHolderPassNew;
        private System.Windows.Forms.TextBox TextboxPassNew;
        private System.Windows.Forms.Label LabelHolderPassOld;
        private System.Windows.Forms.TextBox TextboxPassOld;
        private System.Windows.Forms.Label LabelHolderId;
        private System.Windows.Forms.TextBox TextboxUserId;
    }
}