﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.ReportDoc
{
    public partial class FormRemittanceSearch : Form
    {
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ClassLogsService meaLogError = new ClassLogsService("EventLogError", "");
        ResultRemittance resul = new ResultRemittance();
        ArrearsController arr = new ArrearsController();
        public string strTemp;
        public CultureInfo usCulture = new CultureInfo("en-US");
        public CultureInfo _usCultureTh = new CultureInfo("th-TH");
        public FormRemittanceSearch()
        {
            InitializeComponent();
        }
        private void btAdd_Click(object sender, EventArgs e)
        {
            FormRemittance receive = new FormRemittance();
            receive.ShowDialog();
        }
        private void FormRemittanceSearch_Load(object sender, EventArgs e)
        {
            LabelEmployeeCode.Text = GlobalClass.UserId.ToString();
            LabelStationNo.Text = Convert.ToDecimal(GlobalClass.No).ToString("00");
            SetControl();
        }
        public void SetControl()
        {
            GridViewDetail.AutoGenerateColumns = false;
            GridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ArrearsController arr = new ArrearsController();
                string date = DateTimePicker.Value.Date.ToString("dd/MM/yyyy");
                DateTime dueDate = DateTime.Parse(date);
                ClassPayInBalance payIn = new ClassPayInBalance();
                payIn.distId = GlobalClass.DistId;
                payIn.cashierId = GlobalClass.UserId;
                payIn.payInDateStr = dueDate.ToString("yyyy-MM-dd");
                resul = arr.SearchDataRemittance(payIn);
                if (resul.Output != null)
                {
                    SetControl();
                    this.GridViewDetail.DataSource = resul.Output.OrderBy(c => c.orderNo).ToList();
                    meaLog.WriteData("Success :: " + strTemp, method, LogLevel.Debug);
                    AddImage();
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormRemittanceSearch | Function :: ButtonSearch_Click()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
            }
        }
        public void AddImage()
        {
            int i = 0;
            var loopTo = GridViewDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                DataGridViewRow row = GridViewDetail.Rows[i];
                row.Height = 40;
                Image image = Properties.Resources.Recycle;
                GridViewDetail.Rows[i].Cells["ColDelete"].Value = image;
            }
        }
        private void GridViewDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.ColumnIndex == 6)
                {
                    if (MessageBox.Show("คุณต้องการยกเลิกรายการนี้หรือไม่ ", "ยกเลิกเช็ค", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    else
                    {
                        if (GridViewDetail.Rows.Count > 0)
                        {
                            string closingId = GridViewDetail.Rows[e.RowIndex].Cells["colClosingDrawerId"].Value.ToString();
                            string orderNo = GridViewDetail.Rows[e.RowIndex].Cells["colorderNo"].Value.ToString();
                            string MaxOrder = resul.Output.Max(c => c.orderNo).ToString();
                            if(Convert.ToInt32(orderNo) >= Convert.ToInt32(MaxOrder))
                            {
                                ClassClosingDrawer cls = new ClassClosingDrawer();
                                cls.closingDrawerId = Convert.ToInt64(closingId);
                                ResultProcessCancelPayIn rs = arr.CancelRemittance(cls);
                                if (rs.ResultMessage == "SUCCESS")
                                {
                                    MessageBox.Show("ยกเลิกการส่งเงินเข้าคลังลำดับที่ " + rs.closingDrawerIdList[0].ToString() + " เรียบร้อย", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    ButtonSearch_Click(null, null);
                                }
                                else
                                    MessageBox.Show("ไม่สามารถยกเลิกการส่งเงินเข้าคลังลำดับที่ " + orderNo + " ได้", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                                MessageBox.Show("ไม่สามารถยกเลิกการส่งเงินเข้าคลังที่ลำดับน้อยกว่าลำดับครั้งล่าสุดได้", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormRemittanceSearch | Function :: GridViewDetail_CellClick()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
            }
        }
        private void GridViewDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    int i = 0;
                    int countCheck = 0;
                    var loopTo = GridViewDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (GridViewDetail.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            DataGridViewCell cell = GridViewDetail.CurrentCell;
                            string orderNo = GridViewDetail.Rows[i].Cells["colorderNo"].Value.ToString();
                            payInDisplayResponseDtoList payIn = resul.Output.Find(item => item.orderNo.ToString().Equals(orderNo, StringComparison.CurrentCultureIgnoreCase));
                            if (payIn != null)
                            {
                                DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewDetail.Rows[i].Cells["colCheck"];
                                bool newBool = (bool)checkCell.EditedFormattedValue;
                                if (newBool)
                                {
                                    payIn.status = true;
                                    GridViewDetail.Rows[i].Cells[0].Value = true;
                                    countCheck++;
                                    if (countCheck == GridViewDetail.Rows.Count)
                                        CheckBoxSelectAll.Checked = true;
                                }
                                else
                                {
                                    payIn.status = false;
                                    GridViewDetail.Rows[i].Cells[0].Value = false;
                                    CheckBoxSelectAll.Checked = false;
                                    if (countCheck == 0)
                                        countCheck = 0;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormArrearsNew | Function :: DataGridViewDetail_CellContentDoubleClick()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }

        private void CheckBoxSelectAll_Click(object sender, EventArgs e)
        {
            int i = 0;
            var loopTo = GridViewDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                string orderNo = GridViewDetail.Rows[i].Cells["colorderNo"].Value.ToString();
                payInDisplayResponseDtoList payIn = resul.Output.Find(item => item.orderNo.ToString().Equals(orderNo, StringComparison.CurrentCultureIgnoreCase));
                if (payIn != null)
                {
                    if (CheckBoxSelectAll.Checked == true)
                    {
                        payIn.status = true;
                        GridViewDetail.Rows[i].Cells["colCheck"].Value = true;
                    }
                    else
                    {
                        payIn.status = false;
                        GridViewDetail.Rows[i].Cells["colCheck"].Value = false;
                    }
                }
            }
        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            if (!SendDataAddPrintData())
            {
                this.GridViewDetail.DataSource = null;
                this.GridViewDetail.Refresh();
            }
        }
        public bool SendDataAddPrintData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool status = false;
            try
            {
                List<payInDisplayResponseDtoList> _lstPayIn = new List<payInDisplayResponseDtoList>();
                if (resul.Output != null)
                    _lstPayIn = resul.Output.Where(c => c.status == true).ToList();
                if (_lstPayIn.Count > 0)
                {
                    string Path = "";
                    List<payInDisplayResponseDtoList> _lstDisplay = new List<payInDisplayResponseDtoList>();
                    foreach (payInDisplayResponseDtoList item in _lstPayIn)
                    {
                        DateTime dt = DateTime.Now;
                        DateTime dtNow = DateTime.Now;
                        payInDisplayResponseDtoList detail = new payInDisplayResponseDtoList();
                        detail.userName = "ชื่อผู้ส่ง " + GlobalClass.UserName;
                        detail.distName = "การไฟฟ้า  " + GlobalClass.DistName;
                        string str = arr.PrintDate(dt.Month);
                        if (dt.Year < 2500)
                            dtNow = dt.AddYears(543);
                        string Minute = dt.Minute.ToString().Length == 1 ? "0" + dt.Minute.ToString() : dt.Minute.ToString();
                        string Second = dt.Second.ToString().Length == 1 ? "0" + dt.Second.ToString() : dt.Second.ToString();
                        detail.strDate = dt.Day.ToString() + " " + str + " " + dtNow.Year.ToString() + " " + dt.Hour.ToString() + ":" + Minute + ":" + Second;
                        detail.orderNo = item.orderNo;
                        detail.closingDrawerId = item.closingDrawerId;
                        detail.cashierId = item.cashierId;
                        detail.cashierNo = item.cashierNo;
                        detail.createdDtStr = item.createdDtStr;
                        detail.totalCash = item.totalCash;
                        detail.totalCheque = item.totalCheque;
                        detail.totalChequeAmount = item.totalChequeAmount;
                        detail.totalCreditCard = item.totalCreditCard;
                        detail.totalCreditCardAmount = item.totalCreditCardAmount;
                        detail.totalSendAmount = item.totalSendAmount;
                        detail.totalPaymentAmount = item.totalPaymentAmount;
                        detail.diffAmount = item.diffAmount;
                        detail.closingDrawerStatus = item.closingDrawerStatus;
                        detail.totalSendAmountDay = item.totalSendAmountDay;
                        detail.previouseSendAmount = item.previouseSendAmount;
                        detail.unitB1000 = item.unitB1000;
                        detail.unitB500 = item.unitB500;
                        detail.unitB100 = item.unitB100;
                        detail.unitB50 = item.unitB50;
                        detail.unitB20 = item.unitB20;
                        detail.amountB1000 = item.amountB1000;
                        detail.amountB500 = item.amountB500;
                        detail.amountB100 = item.amountB100;
                        detail.amountB50 = item.amountB50;
                        detail.amountB20 = item.amountB20;
                        detail.totalCoin = item.totalCoin;
                        detail.totalBankNote = item.totalBankNote;
                        detail.sumTotalAmount = item.sumTotalAmount;
                        detail.sumTotalCard = item.sumTotalCard;
                        detail.sumTotalCash = item.sumTotalCash;
                        detail.sumTotalCheque = item.sumTotalCheque;
                        detail.status = item.status;
                        _lstDisplay.Add(detail);
                        Path = ReportPath + "\\" + "Remittance_Report_Doc.rpt";
                        ReportDocument cryRpt = new ReportDocument();
                        cryRpt.Load(Path);
                        cryRpt.Database.Tables[0].SetDataSource(_lstDisplay);
                        CrystalReportViewer crystalReportViewer1 = new CrystalReportViewer();
                        crystalReportViewer1.ReportSource = cryRpt;
                        crystalReportViewer1.Refresh();
                        cryRpt.PrintToPrinter(1, true, 0, 0);
                    }
                    status = true;
                }
            }
            catch (Exception ex)
            {
                meaLogError.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
            return status;
        }
    }
}
