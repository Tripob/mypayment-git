﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;

namespace MyPayment.ReportDoc
{
    public partial class FormRemittanceReport : Form
    {
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ClassLogsService meaLogError = new ClassLogsService("EventLogError", "");
        ResultDisplayPayIn resul = new ResultDisplayPayIn();
        ArrearsController arr = new ArrearsController();
        public string strTemp;
        public CultureInfo usCulture = new CultureInfo("en-US");
        public CultureInfo _usCultureTh = new CultureInfo("th-TH");
        public FormRemittanceReport()
        {
            InitializeComponent();
        }
        private void btAdd_Click(object sender, EventArgs e)
        {
            FormRemittance receive = new FormRemittance();
            receive.ShowDialog();
        }
        private void FormRemittanceSearch_Load(object sender, EventArgs e)
        {
            LabelEmployeeCode.Text = GlobalClass.UserId.ToString();
            LabelStationNo.Text = Convert.ToDecimal(GlobalClass.No).ToString("00");
            SetControl();
        }
        public void SetControl()
        {
            GridViewDetail.AutoGenerateColumns = false;
            GridViewDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ArrearsController arr = new ArrearsController();
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                string date = DateTimePicker.Value.Date.ToString("dd/MM/yyyy");
                DateTime dueDate = DateTime.Parse(date);
                ClassPayInBalance payIn = new ClassPayInBalance();
                payIn.distId = GlobalClass.DistId;
                payIn.cashierId = GlobalClass.UserId;
                payIn.payInDateStr = dueDate.ToString("yyyy-MM-dd");
                resul = arr.SearchDataSumRemittance(payIn);
                if (resul.Output != null)
                {
                    SetControl();
                    this.GridViewDetail.DataSource = resul.Output;
                }
            }
            catch (Exception ex)
            {
                meaLog = new ClassLogsService("ErrorLog", "");
                meaLog.WriteData("Form :: FormRemittanceSearch | Function :: ButtonSearch_Click()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
            }
        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            SendDataAddPrintData();
        }
        public void SendDataAddPrintData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (resul.Output != null)
                {
                    string Path = "";
                    foreach (ClassProcessDisplayPayIn item in resul.Output)
                    {
                        List<ClassProcessDisplayPayIn> _lstDisplay = new List<ClassProcessDisplayPayIn>();
                        List<payInCardResponseDtoList> _lstCard = new List<payInCardResponseDtoList>();
                        List<payInChequeResponseDtoList> _lstCheque = new List<payInChequeResponseDtoList>();
                        DateTime dt = DateTime.Now;
                        DateTime dtNow = DateTime.Now;
                        string str = arr.PrintDate(dt.Month);
                        if (dt.Year < 2500)
                            dtNow = dt.AddYears(543);
                        string Minute = dt.Minute.ToString().Length == 1 ? "0" + dt.Minute.ToString() : dt.Minute.ToString();
                        string Second = dt.Second.ToString().Length == 1 ? "0" + dt.Second.ToString() : dt.Second.ToString();
                        string _datePay = dt.Day.ToString() + " " + str + " " + dtNow.Year.ToString() + " " + dt.Hour.ToString() + ":" + Minute + ":" + Second;
                        string _dateNow = dt.Day.ToString() + "/" + dt.Month.ToString() + "/" + dtNow.Year.ToString();
                        ClassProcessDisplayPayIn model = new ClassProcessDisplayPayIn();
                        model.strDate = _dateNow;
                        model.totalAmount = item.totalAmount;
                        model.totalCard = item.totalCard;
                        model.totalCash = item.totalCash;
                        model.totalCheque = item.totalCheque;
                        model.userName = item.userName;
                        model.distName = item.distName;
                        model.strAmount = item.strAmount;
                        model.strAmountNew = item.strAmountNew;
                        model.userId = item.userId;
                        model.datePament = _datePay;
                        _lstDisplay.Add(model);
                        Path = ReportPath + "\\" + "Remittance_Report.rpt";
                        PrintData(Path, _lstDisplay, _lstCheque, _lstCard);
                        #region Cheque
                        if (item.payInChequeResponseDtoList != null)
                        {
                            foreach (var itemCheque in item.payInChequeResponseDtoList)
                            {
                                payInChequeResponseDtoList cheque = new payInChequeResponseDtoList();
                                cheque.chequeId = itemCheque.chequeId;
                                cheque.chequeNo = itemCheque.chequeNo;
                                cheque.chequeOwner = itemCheque.chequeOwner;
                                cheque.chequeDate = itemCheque.chequeDate;
                                cheque.telNo = itemCheque.telNo;
                                cheque.chequeAmount = itemCheque.chequeAmount;
                                cheque.bankDesc = itemCheque.bankDesc;
                                cheque.No = itemCheque.No;
                                _lstCheque.Add(cheque);
                            }
                        }
                        else
                        {
                            payInChequeResponseDtoList cheque = new payInChequeResponseDtoList();
                            cheque.chequeId = 0;
                            cheque.chequeNo = "";
                            cheque.chequeOwner = "";
                            cheque.chequeDate = "";
                            cheque.telNo = "";
                            cheque.chequeAmount = 0;
                            cheque.bankDesc = "";
                            cheque.No = "";
                            _lstCheque.Add(cheque);
                        }
                        Path = ReportPath + "\\" + "Remittance_Cheque_Report.rpt";
                        _lstCard = new List<payInCardResponseDtoList>();
                        PrintData(Path, _lstDisplay, _lstCheque, _lstCard);
                        #endregion
                        #region card
                        if (item.payInCardResponseDtoList != null)
                        {
                            foreach (var itemCard in item.payInCardResponseDtoList)
                            {
                                payInCardResponseDtoList card = new payInCardResponseDtoList();
                                card.cardId = itemCard.cardId;
                                card.cardNo = itemCard.cardNo;
                                card.cardOwner = itemCard.cardOwner;
                                card.cardAmount = itemCard.cardAmount;
                                card.No = itemCard.No;
                                _lstCard.Add(card);
                            }
                        }
                        else
                        {
                            payInCardResponseDtoList card = new payInCardResponseDtoList();
                            card.cardId = 0;
                            card.cardNo = "";
                            card.cardOwner = "";
                            card.cardAmount = 0;
                            card.No = "";
                            _lstCard.Add(card);
                        }
                        Path = ReportPath + "\\" + "Remittance_Card_Report.rpt";
                        _lstCheque = new List<payInChequeResponseDtoList>();
                        PrintData(Path, _lstDisplay, _lstCheque, _lstCard);
                        #endregion
                    }
                }
                else
                    return;
            }
            catch (Exception ex)
            {
                meaLogError.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public void PrintData(string Path, List<ClassProcessDisplayPayIn> _lstData, List<payInChequeResponseDtoList> _lstCheque, List<payInCardResponseDtoList> _lstCard)
        {
            ReportDocument cryRpt = new ReportDocument();
            cryRpt.Load(Path);
            if (_lstData != null && _lstCheque.Count == 0 && _lstCard.Count == 0)
                cryRpt.Database.Tables[0].SetDataSource(_lstData);
            if (_lstCheque.Count > 0)
            {
                cryRpt.Database.Tables[0].SetDataSource(_lstData);
                cryRpt.Database.Tables[1].SetDataSource(_lstCheque);
            }
            if (_lstCard.Count > 0)
            {
                cryRpt.Database.Tables[0].SetDataSource(_lstData);
                cryRpt.Database.Tables[1].SetDataSource(_lstCard);
            }
            CrystalReportViewer crystalReportViewer1 = new CrystalReportViewer();
            crystalReportViewer1.ReportSource = cryRpt;
            crystalReportViewer1.Refresh();
            cryRpt.PrintToPrinter(1, true, 0, 0);
        }    
    }
}
