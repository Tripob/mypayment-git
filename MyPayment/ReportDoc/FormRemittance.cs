﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.ReportDoc
{
    public partial class FormRemittance : Form
    {
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ClassLogsService meaLogError = new ClassLogsService("EventLogError", "");
        ResultRemittance resul = new ResultRemittance();
        ArrearsController arrears = new ArrearsController();
        public CultureInfo usCulture = new CultureInfo("en-US");
        public decimal _cashAmount;
        ArrearsController arr = new ArrearsController();
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        public FormRemittance()
        {
            InitializeComponent();
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FormRemittance_Load(object sender, EventArgs e)
        {
            LoadData();
            SetControl();
        }
        public void LoadData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ResultPayInBalance rs = new ResultPayInBalance();
                labelEmployeeCode.Text = GlobalClass.UserId.ToString();
                LabelStationNo.Text = Convert.ToDecimal(GlobalClass.No).ToString("00");
                DateTime dateNow = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                ClassPayInBalance classPay = new ClassPayInBalance();
                classPay.distId = GlobalClass.DistId;
                classPay.cashierId = GlobalClass.UserId;
                rs = arrears.SelectDataPayin(classPay);
                decimal countAmount = 0;

                if (rs.OutputResponseDto != null)
                {
                    payInResponseDto item = rs.OutputResponseDto.FirstOrDefault();
                    countAmount = item.cashTotalAmount + item.chequeAmount + item.cardAmount;
                    _cashAmount = item.cashTotalAmount;

                    if (item.payInChequeResponseDtoList != null)
                        GridViewDetailCheque.DataSource = item.payInChequeResponseDtoList;
                    if (item.payInCardResponseDtoList != null)
                        GridViewCard.DataSource = item.payInCardResponseDtoList;
                }
                lblAmount.Text = countAmount.ToString("#,###,###,##0.00");

            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void SetControl()
        {
            GridViewDetailCheque.AutoGenerateColumns = false;
            GridViewDetailCheque.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GridViewCard.AutoGenerateColumns = false;
            GridViewCard.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void DateTimePickerCheque_ValueChanged(object sender, EventArgs e)
        {

        }
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ClassPayIn payin = new ClassPayIn();
                payin.distId = GlobalClass.DistId;
                payin.subDistId = GlobalClass.SubDistId;
                payin.cashierId = Convert.ToInt64(labelEmployeeCode.Text);
                payin.cashierNo = LabelStationNo.Text;
                payin.coin025 = (TextAmount25st.Text != "") ? int.Parse(TextAmount25st.Text) : 0;
                payin.coin050 = (TextAmount50st.Text != "") ? int.Parse(TextAmount50st.Text) : 0;
                payin.coin1 = (TextAmount1.Text != "") ? int.Parse(TextAmount1.Text) : 0;
                payin.coin2 = (TextAmount2.Text != "") ? int.Parse(TextAmount2.Text) : 0;
                payin.coin5 = (TextAmount5.Text != "") ? int.Parse(TextAmount5.Text) : 0;
                payin.coin10 = (TextAmount10.Text != "") ? int.Parse(TextAmount10.Text) : 0;
                payin.bankNote20 = (TextAmount20.Text != "") ? int.Parse(TextAmount20.Text) : 0;
                payin.bankNote50 = (TextAmount50.Text != "") ? int.Parse(TextAmount50.Text) : 0;
                payin.bankNote100 = (TextAmount100.Text != "") ? int.Parse(TextAmount100.Text) : 0;
                payin.bankNote500 = (TextAmount500.Text != "") ? int.Parse(TextAmount500.Text) : 0;
                payin.bankNote1000 = (TextAmount1000.Text != "") ? int.Parse(TextAmount1000.Text) : 0;
                payin.totalCash = Convert.ToDecimal(labelAmount.Text);
                payin.totalCheque = Convert.ToDecimal(labelAmountCheq.Text);
                payin.totalCard = Convert.ToDecimal(labelAmountCard.Text);
                payin.chequeQty = CountCheqce();
                payin.cardQty = CountCard();
                if (labelAmountCheq.Text != "0")
                    payin.payInChequeResponseDtoList = AddPainCheque();
                if (labelAmountCard.Text != "0")
                    payin.payInCardResponseDtoList = AddPainCard();

                resul = arrears.InsertRemittance(payin);

                if (resul.ResultMessage == "SUCCESS")
                {
                    SendDataAddPrintData();
                    MessageBox.Show("นำส่งเงิน :: " + resul.ResultMessage, " ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                }
                else
                    MessageBox.Show(resul.ResultMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public List<payInChequeResponseDtoList> AddPainCheque()
        {
            List<payInChequeResponseDtoList> _lstCheque = new List<payInChequeResponseDtoList>();
            int i = 0;
            var loopTo = GridViewDetailCheque.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewDetailCheque.Rows[i].Cells["ColCkCheq"];
                bool newBool = (bool)checkCell.EditedFormattedValue;
                if (newBool)
                {
                    string chequeNo = GridViewDetailCheque.Rows[i].Cells["ColChequeNo"].Value.ToString();
                    string bankDesc = GridViewDetailCheque.Rows[i].Cells["ColbankName"].Value.ToString();
                    string chequeAmount = GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value.ToString();
                    string chequeId = GridViewDetailCheque.Rows[i].Cells["ColchequeId"].Value.ToString();
                    string chequeOwner = GridViewDetailCheque.Rows[i].Cells["ColchequeOwner"].Value.ToString();
                    payInChequeResponseDtoList item = new payInChequeResponseDtoList();
                    item.chequeId = Int64.Parse(chequeId);
                    item.chequeNo = chequeNo;
                    item.bankDesc = bankDesc;
                    item.chequeOwner = chequeOwner;
                    item.chequeAmount = Convert.ToDecimal(chequeAmount);
                    _lstCheque.Add(item);
                }
            }
            return _lstCheque;
        }
        public List<payInCardResponseDtoList> AddPainCard()
        {
            List<payInCardResponseDtoList> _lstCard = new List<payInCardResponseDtoList>();
            int i = 0;
            var loopTo = GridViewCard.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewCard.Rows[i].Cells["ColCkCard"];
                bool newBool = (bool)checkCell.EditedFormattedValue;
                if (newBool)
                {
                    string cardId = GridViewCard.Rows[i].Cells["colcardId"].Value.ToString();
                    string cardNo = GridViewCard.Rows[i].Cells["ColCardNo"].Value.ToString();
                    string cardOwner = GridViewCard.Rows[i].Cells["ColcardOwner"].Value.ToString();
                    string cardAmount = GridViewCard.Rows[i].Cells["ColAmountCard"].Value.ToString();
                    payInCardResponseDtoList item = new payInCardResponseDtoList();
                    item.cardId = Int64.Parse(cardId);
                    item.cardNo = cardNo;
                    item.cardOwner = cardOwner;
                    item.cardAmount = Convert.ToDecimal(cardAmount);
                    _lstCard.Add(item);
                }
            }
            return _lstCard;
        }
        private void TextAmount1000_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount1000.Text == string.Empty)
                labelAmount1000.Text = "0";
            else
            {
                decimal amount = Convert.ToInt32(TextAmount1000.Text) * 1000;
                //if (amount <= _cashAmount)
                labelAmount1000.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount500_TextChanged_1(object sender, EventArgs e)
        {
            if (TextAmount500.Text == string.Empty)
                TextAmount500.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount500.Text) * 500;
                labelAmount500.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount100_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount100.Text == string.Empty)
                TextAmount100.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount100.Text) * 100;
                labelAmount100.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount50_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount50.Text == string.Empty)
                TextAmount50.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount50.Text) * 50;
                labelAmount50.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount20_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount20.Text == string.Empty)
                TextAmount20.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount20.Text) * 20;
                labelAmount20.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount10_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount10.Text == string.Empty)
                TextAmount10.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount10.Text) * 10;
                labelAmount10.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount5_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount5.Text == string.Empty)
                TextAmount5.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount5.Text) * 5;
                labelAmount5.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount2_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount2.Text == string.Empty)
                TextAmount2.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount2.Text) * 2;
                labelAmount2.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount1_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount1.Text == string.Empty)
                TextAmount1.Text = "0";
            else
            {
                double amount = Convert.ToInt32(TextAmount1.Text);
                labelAmount1.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount50st_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount50st.Text == string.Empty)
                TextAmount50st.Text = "0";
            else
            {
                double amount = (Convert.ToInt32(TextAmount50st.Text) * 0.50);
                labelAmount50st.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        private void TextAmount25st_TextChanged(object sender, EventArgs e)
        {
            if (TextAmount25st.Text == string.Empty)
                TextAmount25st.Text = "0";
            else
            {
                double amount = (Convert.ToInt32(TextAmount25st.Text) * 0.25);
                labelAmount25st.Text = amount.ToString("#,###,###,##0.00");
            }
            CountAmount();
            SumAmount();
        }
        public void CountAmount()
        {
            double amount;
            double amount1000 = Convert.ToDouble(labelAmount1000.Text);
            double amount500 = Convert.ToDouble(labelAmount500.Text);
            double amount100 = Convert.ToDouble(labelAmount100.Text);
            double amount50 = Convert.ToDouble(labelAmount50.Text);
            double amount20 = Convert.ToDouble(labelAmount20.Text);
            double amount10 = Convert.ToDouble(labelAmount10.Text);
            double amount5 = Convert.ToDouble(labelAmount5.Text);
            double amount2 = Convert.ToDouble(labelAmount2.Text);
            double amount1 = Convert.ToDouble(labelAmount1.Text);
            double amount50st = Convert.ToDouble(labelAmount50st.Text);
            double amount25st = Convert.ToDouble(labelAmount25st.Text);
            amount = amount1000 + amount500 + amount100 + amount50 + amount20 + amount10 + amount5 + amount2 + amount1 + amount50st + amount25st;
            labelAmount.Text = amount.ToString("#,###,###,##0.00");
        }
        public void SumAmount()
        {
            double sumAmount;
            double amountCheq = Convert.ToDouble(labelAmountCheq.Text);
            double amountCard = Convert.ToDouble(labelAmountCard.Text);
            double amount = Convert.ToDouble(labelAmount.Text);
            sumAmount = amountCheq + amountCard + amount;
            labelTotalAmount.Text = sumAmount.ToString("#,###,###,##0.00");

            double sumAll;
            double TotalAmount = Convert.ToDouble(labelTotalAmount.Text);
            double AmountAll = Convert.ToDouble(lblAmount.Text);
            sumAll = AmountAll - TotalAmount;
            labelSettlement.Text = sumAll.ToString("#,###,###,##0.00");
        }
        public int CountCheqce()
        {
            int i = 0;
            int countCheck = 0;
            var loopTo = GridViewDetailCheque.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (GridViewDetailCheque.CurrentCell is DataGridViewCheckBoxCell)
                {
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewDetailCheque.Rows[i].Cells["ColCkCheq"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                        countCheck++;
                }
            }
            return countCheck;
        }
        public int CountCard()
        {
            int i = 0;
            int countCheck = 0;
            var loopTo = GridViewCard.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (GridViewCard.CurrentCell is DataGridViewCheckBoxCell)
                {
                    DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewCard.Rows[i].Cells["ColCkCard"];
                    bool newBool = (bool)checkCell.EditedFormattedValue;
                    if (newBool)
                        countCheck++;
                }
            }
            return countCheck;
        }
        private void GridViewDetailCheque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    double amount = 0;
                    int i = 0;
                    int countCheck = 0;
                    var loopTo = GridViewDetailCheque.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (GridViewDetailCheque.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewDetailCheque.Rows[i].Cells["ColCkCheq"];
                            bool newBool = (bool)checkCell.EditedFormattedValue;
                            if (newBool)
                            {
                                if (GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value.ToString() != "")
                                    amount += Convert.ToDouble(GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value);
                                countCheck++;
                                if (countCheck == GridViewDetailCheque.Rows.Count)
                                    CkSelectCheqAll.Checked = true;
                            }
                            else
                                CkSelectCheqAll.Checked = false;
                        }
                    }
                    labelAmountCheq.Text = amount.ToString("#,###,###,##0.00");
                    SumAmount();
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void GridViewCard_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    double amount = 0;
                    int i = 0;
                    int countCheck = 0;
                    var loopTo = GridViewCard.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (GridViewCard.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridViewCard.Rows[i].Cells["ColCkCard"];
                            bool newBool = (bool)checkCell.EditedFormattedValue;
                            if (newBool)
                            {
                                if (GridViewCard.Rows[i].Cells["ColAmountCard"].Value.ToString() != "")
                                    amount += Convert.ToDouble(GridViewCard.Rows[i].Cells["ColAmountCard"].Value);
                                countCheck++;
                                if (countCheck == GridViewCard.Rows.Count)
                                    CkSelectCardAll.Checked = true;
                            }
                            else
                                CkSelectCardAll.Checked = false;
                        }
                    }
                    labelAmountCard.Text = amount.ToString("#,###,###,##0.00");
                    SumAmount();
                }
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void CkSelectCheqAll_Click(object sender, EventArgs e)
        {
            SelectCheckboxCheqce();
        }
        public void SelectCheckboxCheqce()
        {
            double amount = 0;
            int i = 0;
            var loopTo = GridViewDetailCheque.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CkSelectCheqAll.Checked == true)
                {
                    GridViewDetailCheque.Rows[i].Cells["ColCkCheq"].Value = true;
                    if (GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value.ToString() != "")
                        amount += Convert.ToDouble(GridViewDetailCheque.Rows[i].Cells["ColchequeAmount"].Value);
                }
                else
                {
                    amount = 0;
                    GridViewDetailCheque.Rows[i].Cells["ColCkCheq"].Value = false;
                }
            }
            labelAmountCheq.Text = amount.ToString("#,###,###,##0.00");
            SumAmount();
        }
        public void SelectCheckboxCard()
        {
            double amount = 0;
            int i = 0;
            var loopTo = GridViewCard.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CkSelectCardAll.Checked == true)
                {
                    GridViewCard.Rows[i].Cells["ColCkCard"].Value = true;
                    if (GridViewCard.Rows[i].Cells["ColAmountCard"].Value.ToString() != "")
                        amount += Convert.ToDouble(GridViewCard.Rows[i].Cells["ColAmountCard"].Value);
                }
                else
                {
                    amount = 0;
                    GridViewCard.Rows[i].Cells["ColCkCard"].Value = false;
                }
            }
            labelAmountCard.Text = amount.ToString("#,###,###,##0.00");
            SumAmount();
        }
        private void CkSelectCardAll_Click(object sender, EventArgs e)
        {
            SelectCheckboxCard();
        }
        private void TextAmount1000_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount500_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount100_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount50_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount20_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount10_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount50st_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void TextAmount25st_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        public void SendDataAddPrintData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                List<payInDisplayResponseDtoList> _lstPayIn = new List<payInDisplayResponseDtoList>();
                if (resul.Output != null)
                    _lstPayIn = resul.Output.Where(c => c.status == true).ToList();
                if (_lstPayIn.Count > 0)
                {
                    string Path = "";
                    List<payInDisplayResponseDtoList> _lstDisplay = new List<payInDisplayResponseDtoList>();
                    foreach (payInDisplayResponseDtoList item in _lstPayIn)
                    {
                        DateTime dt = DateTime.Now;
                        DateTime dtNow = DateTime.Now;
                        payInDisplayResponseDtoList detail = new payInDisplayResponseDtoList();
                        detail.userName = "ชื่อผู้ส่ง " + GlobalClass.UserName;
                        detail.distName = "การไฟฟ้า  " + GlobalClass.DistName;
                        string str = arr.PrintDate(dt.Month);
                        if (dt.Year < 2500)
                            dtNow = dt.AddYears(543);
                        string Minute = dt.Minute.ToString().Length == 1 ? "0" + dt.Minute.ToString() : dt.Minute.ToString();
                        string Second = dt.Second.ToString().Length == 1 ? "0" + dt.Second.ToString() : dt.Second.ToString();
                        detail.strDate = dt.Day.ToString() + " " + str + " " + dtNow.Year.ToString() + " " + dt.Hour.ToString() + ":" + Minute + ":" + Second;
                        detail.orderNo = item.orderNo;
                        detail.closingDrawerId = item.closingDrawerId;
                        detail.cashierId = item.cashierId;
                        detail.cashierNo = item.cashierNo;
                        detail.createdDtStr = item.createdDtStr;
                        detail.totalCash = item.totalCash;
                        detail.totalCheque = item.totalCheque;
                        detail.totalChequeAmount = item.totalChequeAmount;
                        detail.totalCreditCard = item.totalCreditCard;
                        detail.totalCreditCardAmount = item.totalCreditCardAmount;
                        detail.totalSendAmount = item.totalSendAmount;
                        detail.totalPaymentAmount = item.totalPaymentAmount;
                        detail.totalSendAmountDay = item.totalSendAmountDay;
                        detail.previouseSendAmount = item.previouseSendAmount;
                        detail.diffAmount = item.diffAmount;
                        detail.closingDrawerStatus = item.closingDrawerStatus;
                        detail.unitB1000 = item.unitB1000;
                        detail.unitB500 = item.unitB500;
                        detail.unitB100 = item.unitB100;
                        detail.unitB50 = item.unitB50;
                        detail.unitB20 = item.unitB20;
                        detail.amountB1000 = item.amountB1000;
                        detail.amountB500 = item.amountB500;
                        detail.amountB100 = item.amountB100;
                        detail.amountB50 = item.amountB50;
                        detail.amountB20 = item.amountB20;
                        detail.totalCoin = item.totalCoin;
                        detail.totalBankNote = item.totalBankNote;
                        detail.sumTotalCard = item.sumTotalCard;
                        detail.sumTotalCash = item.sumTotalCash;
                        detail.sumTotalAmount = item.sumTotalAmount;
                        detail.sumTotalCheque = item.sumTotalCheque;
                        detail.status = item.status;
                        _lstDisplay.Add(detail);
                        Path = ReportPath + "\\" + "Remittance_Report_Doc.rpt";
                        ReportDocument cryRpt = new ReportDocument();
                        cryRpt.Load(Path);
                        cryRpt.Database.Tables[0].SetDataSource(_lstDisplay);
                        CrystalReportViewer crystalReportViewer1 = new CrystalReportViewer();
                        crystalReportViewer1.ReportSource = cryRpt;
                        crystalReportViewer1.Refresh();
                        cryRpt.PrintToPrinter(1, true, 0, 0);
                    }
                }
                else
                    return;
            }
            catch (Exception ex)
            {
                meaLogError.WriteData("Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
    }
}
