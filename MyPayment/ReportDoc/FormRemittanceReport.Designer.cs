﻿namespace MyPayment.ReportDoc
{
    partial class FormRemittanceReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRemittanceReport));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.LabelStationNo = new System.Windows.Forms.Label();
            this.LabelEmployeeCode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btPrint = new Custom_Controls_in_CS.ButtonZ();
            this.GridViewDetail = new System.Windows.Forms.DataGridView();
            this.colCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colorderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colChequeAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCardAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coltotalCash = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ButtonSearch);
            this.panel1.Controls.Add(this.DateTimePicker);
            this.panel1.Controls.Add(this.LabelStationNo);
            this.panel1.Controls.Add(this.LabelEmployeeCode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 59);
            this.panel1.TabIndex = 0;
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "ค้นหา";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.Silver;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSearch.Image")));
            this.ButtonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSearch.Location = new System.Drawing.Point(881, 14);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(90, 28);
            this.ButtonSearch.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.TabIndex = 1000000043;
            this.ButtonSearch.TextLocation_X = 35;
            this.ButtonSearch.TextLocation_Y = 1;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker.Location = new System.Drawing.Point(54, 15);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(171, 26);
            this.DateTimePicker.TabIndex = 1000000029;
            // 
            // LabelStationNo
            // 
            this.LabelStationNo.BackColor = System.Drawing.Color.White;
            this.LabelStationNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelStationNo.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStationNo.Location = new System.Drawing.Point(734, 14);
            this.LabelStationNo.Name = "LabelStationNo";
            this.LabelStationNo.Size = new System.Drawing.Size(83, 28);
            this.LabelStationNo.TabIndex = 1000000028;
            this.LabelStationNo.Text = "09";
            this.LabelStationNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelEmployeeCode
            // 
            this.LabelEmployeeCode.BackColor = System.Drawing.Color.White;
            this.LabelEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelEmployeeCode.Font = new System.Drawing.Font("TH Sarabun New", 18F);
            this.LabelEmployeeCode.Location = new System.Drawing.Point(329, 14);
            this.LabelEmployeeCode.Name = "LabelEmployeeCode";
            this.LabelEmployeeCode.Size = new System.Drawing.Size(277, 28);
            this.LabelEmployeeCode.TabIndex = 1000000027;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(621, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 28);
            this.label2.TabIndex = 1000000026;
            this.label2.Text = "โต๊ะรับชำระเงิน";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(231, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 28);
            this.label1.TabIndex = 1000000025;
            this.label1.Text = "รหัสพนักงาน";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(12, 14);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(41, 28);
            this.Label3.TabIndex = 1000000024;
            this.Label3.Text = "วันที่";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 59);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1012, 646);
            this.panel2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btPrint);
            this.groupBox1.Controls.Add(this.GridViewDetail);
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1006, 643);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รายละเอียด";
            // 
            // btPrint
            // 
            this.btPrint.BackColor = System.Drawing.Color.Transparent;
            this.btPrint.BorderColor = System.Drawing.Color.Transparent;
            this.btPrint.BorderWidth = 1;
            this.btPrint.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btPrint.ButtonText = "พิมพ์";
            this.btPrint.CausesValidation = false;
            this.btPrint.EndColor = System.Drawing.Color.Silver;
            this.btPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPrint.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btPrint.ForeColor = System.Drawing.Color.Black;
            this.btPrint.GradientAngle = 90;
            this.btPrint.Image = ((System.Drawing.Image)(resources.GetObject("btPrint.Image")));
            this.btPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrint.Location = new System.Drawing.Point(908, 599);
            this.btPrint.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btPrint.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btPrint.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btPrint.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btPrint.Name = "btPrint";
            this.btPrint.ShowButtontext = true;
            this.btPrint.Size = new System.Drawing.Size(95, 37);
            this.btPrint.StartColor = System.Drawing.Color.Gainsboro;
            this.btPrint.TabIndex = 1000000045;
            this.btPrint.TextLocation_X = 45;
            this.btPrint.TextLocation_Y = 8;
            this.btPrint.Transparent1 = 80;
            this.btPrint.Transparent2 = 120;
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // GridViewDetail
            // 
            this.GridViewDetail.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridViewDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GridViewDetail.BackgroundColor = System.Drawing.Color.White;
            this.GridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCheck,
            this.colorderNo,
            this.colChequeAmount,
            this.colCardAmount,
            this.coltotalCash,
            this.colTotalAmount});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridViewDetail.DefaultCellStyle = dataGridViewCellStyle7;
            this.GridViewDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.GridViewDetail.Location = new System.Drawing.Point(3, 29);
            this.GridViewDetail.Name = "GridViewDetail";
            this.GridViewDetail.RowHeadersVisible = false;
            this.GridViewDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridViewDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridViewDetail.Size = new System.Drawing.Size(1000, 564);
            this.GridViewDetail.TabIndex = 3;
            // 
            // colCheck
            // 
            this.colCheck.Frozen = true;
            this.colCheck.HeaderText = "";
            this.colCheck.Name = "colCheck";
            this.colCheck.Width = 30;
            // 
            // colorderNo
            // 
            this.colorderNo.DataPropertyName = "orderNo";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colorderNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.colorderNo.HeaderText = "ครั้ง";
            this.colorderNo.MinimumWidth = 50;
            this.colorderNo.Name = "colorderNo";
            this.colorderNo.ReadOnly = true;
            this.colorderNo.Width = 65;
            // 
            // colChequeAmount
            // 
            this.colChequeAmount.DataPropertyName = "totalCheque";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,###,###,##0.00";
            this.colChequeAmount.DefaultCellStyle = dataGridViewCellStyle3;
            this.colChequeAmount.HeaderText = "จำนวนเงินเช็ค";
            this.colChequeAmount.Name = "colChequeAmount";
            this.colChequeAmount.ReadOnly = true;
            this.colChequeAmount.Width = 220;
            // 
            // colCardAmount
            // 
            this.colCardAmount.DataPropertyName = "totalCard";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,###,###,##0.00";
            this.colCardAmount.DefaultCellStyle = dataGridViewCellStyle4;
            this.colCardAmount.HeaderText = "จำนวนเงินบัตรเครดิต";
            this.colCardAmount.Name = "colCardAmount";
            this.colCardAmount.ReadOnly = true;
            this.colCardAmount.Width = 220;
            // 
            // coltotalCash
            // 
            this.coltotalCash.DataPropertyName = "totalCash";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,###,###,##0.00";
            this.coltotalCash.DefaultCellStyle = dataGridViewCellStyle5;
            this.coltotalCash.HeaderText = "จำนวนเงิน";
            this.coltotalCash.Name = "coltotalCash";
            this.coltotalCash.ReadOnly = true;
            this.coltotalCash.Width = 230;
            // 
            // colTotalAmount
            // 
            this.colTotalAmount.DataPropertyName = "totalAmount";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,###,###,##0.00";
            dataGridViewCellStyle6.NullValue = null;
            this.colTotalAmount.DefaultCellStyle = dataGridViewCellStyle6;
            this.colTotalAmount.HeaderText = "จำนวนเงินรวม";
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.ReadOnly = true;
            this.colTotalAmount.Width = 230;
            // 
            // FormRemittanceReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormRemittanceReport";
            this.Text = "FormRemittanceSearch";
            this.Load += new System.EventHandler(this.FormRemittanceSearch_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        internal System.Windows.Forms.Label LabelStationNo;
        internal System.Windows.Forms.Label LabelEmployeeCode;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label Label3;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private Custom_Controls_in_CS.ButtonZ btPrint;
        internal System.Windows.Forms.DataGridView GridViewDetail;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn colorderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChequeAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCardAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn coltotalCash;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTotalAmount;
    }
}