﻿namespace MyPayment.ReportDoc
{
    partial class FormRemittance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRemittance));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAmount = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.LabelStationNo = new System.Windows.Forms.Label();
            this.labelEmployeeCode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CkSelectCheqAll = new System.Windows.Forms.CheckBox();
            this.labelAmountCheq = new System.Windows.Forms.Label();
            this.GridViewDetailCheque = new System.Windows.Forms.DataGridView();
            this.ColCkCheq = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColChequeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColbankName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColchequeAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColchequeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColchequeOwner = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CkSelectCardAll = new System.Windows.Forms.CheckBox();
            this.labelAmountCard = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.GridViewCard = new System.Windows.Forms.DataGridView();
            this.ColCkCard = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColCardNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAmountCard = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColcardOwner = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colcardId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label45 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.labelSettlement = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.ButtonConfirm = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.label51 = new System.Windows.Forms.Label();
            this.labelTotalAmount = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.labelAmount2 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.TextAmount2 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelAmount1 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.TextAmount1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelAmount500 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TextAmount500 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.labelAmount25st = new System.Windows.Forms.Label();
            this.TextAmount25st = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.labelAmount50st = new System.Windows.Forms.Label();
            this.labelAmount5 = new System.Windows.Forms.Label();
            this.labelAmount10 = new System.Windows.Forms.Label();
            this.labelAmount20 = new System.Windows.Forms.Label();
            this.labelAmount50 = new System.Windows.Forms.Label();
            this.labelAmount100 = new System.Windows.Forms.Label();
            this.labelAmount1000 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.TextAmount50st = new System.Windows.Forms.TextBox();
            this.TextAmount5 = new System.Windows.Forms.TextBox();
            this.TextAmount10 = new System.Windows.Forms.TextBox();
            this.TextAmount20 = new System.Windows.Forms.TextBox();
            this.TextAmount50 = new System.Windows.Forms.TextBox();
            this.TextAmount100 = new System.Windows.Forms.TextBox();
            this.TextAmount1000 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDetailCheque)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewCard)).BeginInit();
            this.panel4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblAmount);
            this.panel1.Controls.Add(this.label52);
            this.panel1.Controls.Add(this.DateTimePicker);
            this.panel1.Controls.Add(this.LabelStationNo);
            this.panel1.Controls.Add(this.labelEmployeeCode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(958, 47);
            this.panel1.TabIndex = 0;
            // 
            // lblAmount
            // 
            this.lblAmount.BackColor = System.Drawing.Color.White;
            this.lblAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAmount.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.Location = new System.Drawing.Point(818, 8);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(126, 28);
            this.lblAmount.TabIndex = 1000000045;
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(736, 8);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(79, 28);
            this.label52.TabIndex = 1000000044;
            this.label52.Text = "จำนวนเงิน";
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker.Location = new System.Drawing.Point(53, 9);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(138, 26);
            this.DateTimePicker.TabIndex = 1000000023;
            this.DateTimePicker.ValueChanged += new System.EventHandler(this.DateTimePickerCheque_ValueChanged);
            // 
            // LabelStationNo
            // 
            this.LabelStationNo.BackColor = System.Drawing.Color.White;
            this.LabelStationNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelStationNo.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStationNo.Location = new System.Drawing.Point(606, 8);
            this.LabelStationNo.Name = "LabelStationNo";
            this.LabelStationNo.Size = new System.Drawing.Size(83, 28);
            this.LabelStationNo.TabIndex = 1000000022;
            this.LabelStationNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelEmployeeCode
            // 
            this.labelEmployeeCode.BackColor = System.Drawing.Color.White;
            this.labelEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelEmployeeCode.Font = new System.Drawing.Font("TH Sarabun New", 18F);
            this.labelEmployeeCode.Location = new System.Drawing.Point(285, 8);
            this.labelEmployeeCode.Name = "labelEmployeeCode";
            this.labelEmployeeCode.Size = new System.Drawing.Size(206, 28);
            this.labelEmployeeCode.TabIndex = 1000000021;
            this.labelEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(493, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 28);
            this.label2.TabIndex = 5;
            this.label2.Text = "โต๊ะรับชำระเงิน";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(194, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 28);
            this.label1.TabIndex = 4;
            this.label1.Text = "รหัสพนักงาน";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(11, 8);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(41, 28);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "วันที่";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CkSelectCheqAll);
            this.groupBox3.Controls.Add(this.labelAmountCheq);
            this.groupBox3.Controls.Add(this.GridViewDetailCheque);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox3.Location = new System.Drawing.Point(41, -1);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox3.Size = new System.Drawing.Size(914, 133);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "เช็ค";
            // 
            // CkSelectCheqAll
            // 
            this.CkSelectCheqAll.AutoSize = true;
            this.CkSelectCheqAll.Location = new System.Drawing.Point(10, 36);
            this.CkSelectCheqAll.Name = "CkSelectCheqAll";
            this.CkSelectCheqAll.Size = new System.Drawing.Size(15, 14);
            this.CkSelectCheqAll.TabIndex = 146;
            this.CkSelectCheqAll.UseVisualStyleBackColor = true;
            this.CkSelectCheqAll.Click += new System.EventHandler(this.CkSelectCheqAll_Click);
            // 
            // labelAmountCheq
            // 
            this.labelAmountCheq.BackColor = System.Drawing.Color.White;
            this.labelAmountCheq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmountCheq.Font = new System.Drawing.Font("TH Sarabun New", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmountCheq.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelAmountCheq.Location = new System.Drawing.Point(700, 22);
            this.labelAmountCheq.Name = "labelAmountCheq";
            this.labelAmountCheq.Size = new System.Drawing.Size(161, 35);
            this.labelAmountCheq.TabIndex = 1000000033;
            this.labelAmountCheq.Text = "0";
            this.labelAmountCheq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GridViewDetailCheque
            // 
            this.GridViewDetailCheque.AllowUserToAddRows = false;
            this.GridViewDetailCheque.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridViewDetailCheque.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GridViewDetailCheque.BackgroundColor = System.Drawing.Color.White;
            this.GridViewDetailCheque.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewDetailCheque.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCkCheq,
            this.ColChequeNo,
            this.ColbankName,
            this.ColchequeAmount,
            this.ColchequeId,
            this.colDate,
            this.colTel,
            this.ColchequeOwner,
            this.colNo});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridViewDetailCheque.DefaultCellStyle = dataGridViewCellStyle5;
            this.GridViewDetailCheque.Location = new System.Drawing.Point(3, 22);
            this.GridViewDetailCheque.Name = "GridViewDetailCheque";
            this.GridViewDetailCheque.RowHeadersVisible = false;
            this.GridViewDetailCheque.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridViewDetailCheque.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridViewDetailCheque.Size = new System.Drawing.Size(643, 105);
            this.GridViewDetailCheque.TabIndex = 2;
            this.GridViewDetailCheque.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridViewDetailCheque_CellContentClick);
            // 
            // ColCkCheq
            // 
            this.ColCkCheq.Frozen = true;
            this.ColCkCheq.HeaderText = "";
            this.ColCkCheq.Name = "ColCkCheq";
            this.ColCkCheq.Width = 25;
            // 
            // ColChequeNo
            // 
            this.ColChequeNo.DataPropertyName = "chequeNo";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColChequeNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColChequeNo.HeaderText = "เลขที่เช็ค";
            this.ColChequeNo.Name = "ColChequeNo";
            this.ColChequeNo.Width = 200;
            // 
            // ColbankName
            // 
            this.ColbankName.DataPropertyName = "bankDesc";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColbankName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColbankName.HeaderText = "ชื่อธนาคาร/สาขา";
            this.ColbankName.Name = "ColbankName";
            this.ColbankName.Width = 240;
            // 
            // ColchequeAmount
            // 
            this.ColchequeAmount.DataPropertyName = "chequeAmount";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,###,###,##0.00";
            this.ColchequeAmount.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColchequeAmount.HeaderText = "จำนวนเงิน";
            this.ColchequeAmount.Name = "ColchequeAmount";
            this.ColchequeAmount.Width = 175;
            // 
            // ColchequeId
            // 
            this.ColchequeId.DataPropertyName = "chequeId";
            this.ColchequeId.HeaderText = "chequeId";
            this.ColchequeId.Name = "ColchequeId";
            this.ColchequeId.Visible = false;
            // 
            // colDate
            // 
            this.colDate.DataPropertyName = "chequeDate";
            this.colDate.HeaderText = "chequeDate";
            this.colDate.Name = "colDate";
            this.colDate.Visible = false;
            // 
            // colTel
            // 
            this.colTel.DataPropertyName = "telNo";
            this.colTel.HeaderText = "telNo";
            this.colTel.Name = "colTel";
            this.colTel.Visible = false;
            // 
            // ColchequeOwner
            // 
            this.ColchequeOwner.DataPropertyName = "chequeOwner";
            this.ColchequeOwner.HeaderText = "chequeOwner";
            this.ColchequeOwner.Name = "ColchequeOwner";
            this.ColchequeOwner.Visible = false;
            // 
            // colNo
            // 
            this.colNo.DataPropertyName = "No";
            this.colNo.HeaderText = "No";
            this.colNo.Name = "colNo";
            this.colNo.Visible = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(864, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(46, 32);
            this.label41.TabIndex = 1000000032;
            this.label41.Text = "บาท";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(653, 23);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(45, 32);
            this.label40.TabIndex = 1000000031;
            this.label40.Text = "รวม";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(958, 137);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 184);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(958, 131);
            this.panel3.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CkSelectCardAll);
            this.groupBox1.Controls.Add(this.labelAmountCard);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.GridViewCard);
            this.groupBox1.Controls.Add(this.label45);
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(41, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(915, 123);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "บัตรเครดิต";
            // 
            // CkSelectCardAll
            // 
            this.CkSelectCardAll.AutoSize = true;
            this.CkSelectCardAll.Location = new System.Drawing.Point(9, 39);
            this.CkSelectCardAll.Name = "CkSelectCardAll";
            this.CkSelectCardAll.Size = new System.Drawing.Size(15, 14);
            this.CkSelectCardAll.TabIndex = 146;
            this.CkSelectCardAll.UseVisualStyleBackColor = true;
            this.CkSelectCardAll.Click += new System.EventHandler(this.CkSelectCardAll_Click);
            // 
            // labelAmountCard
            // 
            this.labelAmountCard.BackColor = System.Drawing.Color.White;
            this.labelAmountCard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmountCard.Font = new System.Drawing.Font("TH Sarabun New", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmountCard.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelAmountCard.Location = new System.Drawing.Point(701, 31);
            this.labelAmountCard.Name = "labelAmountCard";
            this.labelAmountCard.Size = new System.Drawing.Size(161, 35);
            this.labelAmountCard.TabIndex = 1000000036;
            this.labelAmountCard.Text = "0";
            this.labelAmountCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(865, 32);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(46, 32);
            this.label44.TabIndex = 1000000035;
            this.label44.Text = "บาท";
            // 
            // GridViewCard
            // 
            this.GridViewCard.AllowUserToAddRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridViewCard.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.GridViewCard.BackgroundColor = System.Drawing.Color.White;
            this.GridViewCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewCard.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCkCard,
            this.ColCardNo,
            this.ColAmountCard,
            this.ColcardOwner,
            this.colcardId,
            this.collNo});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridViewCard.DefaultCellStyle = dataGridViewCellStyle9;
            this.GridViewCard.Location = new System.Drawing.Point(3, 29);
            this.GridViewCard.Name = "GridViewCard";
            this.GridViewCard.RowHeadersVisible = false;
            this.GridViewCard.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridViewCard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridViewCard.Size = new System.Drawing.Size(643, 90);
            this.GridViewCard.TabIndex = 2;
            this.GridViewCard.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridViewCard_CellContentClick);
            // 
            // ColCkCard
            // 
            this.ColCkCard.Frozen = true;
            this.ColCkCard.HeaderText = "";
            this.ColCkCard.Name = "ColCkCard";
            this.ColCkCard.Width = 25;
            // 
            // ColCardNo
            // 
            this.ColCardNo.DataPropertyName = "cardNo";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColCardNo.DefaultCellStyle = dataGridViewCellStyle7;
            this.ColCardNo.HeaderText = "เลขที่บัตรเครดิต";
            this.ColCardNo.Name = "ColCardNo";
            this.ColCardNo.Width = 340;
            // 
            // ColAmountCard
            // 
            this.ColAmountCard.DataPropertyName = "cardAmount";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "#,###,###,##0.00";
            this.ColAmountCard.DefaultCellStyle = dataGridViewCellStyle8;
            this.ColAmountCard.HeaderText = "จำนวนเงิน";
            this.ColAmountCard.Name = "ColAmountCard";
            this.ColAmountCard.Width = 275;
            // 
            // ColcardOwner
            // 
            this.ColcardOwner.DataPropertyName = "cardOwner";
            this.ColcardOwner.HeaderText = "cardOwner";
            this.ColcardOwner.Name = "ColcardOwner";
            this.ColcardOwner.Visible = false;
            // 
            // colcardId
            // 
            this.colcardId.DataPropertyName = "cardId";
            this.colcardId.HeaderText = "cardId";
            this.colcardId.Name = "colcardId";
            this.colcardId.Visible = false;
            // 
            // collNo
            // 
            this.collNo.DataPropertyName = "No";
            this.collNo.HeaderText = "No";
            this.collNo.Name = "collNo";
            this.collNo.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(653, 32);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(45, 32);
            this.label45.TabIndex = 1000000034;
            this.label45.Text = "รวม";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.labelSettlement);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.ButtonConfirm);
            this.panel4.Controls.Add(this.ButtonCancel);
            this.panel4.Controls.Add(this.label51);
            this.panel4.Controls.Add(this.labelTotalAmount);
            this.panel4.Controls.Add(this.label49);
            this.panel4.Controls.Add(this.groupBox2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 315);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(958, 424);
            this.panel4.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(261, 387);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 32);
            this.label25.TabIndex = 1000000046;
            this.label25.Text = "บาท";
            // 
            // labelSettlement
            // 
            this.labelSettlement.BackColor = System.Drawing.Color.White;
            this.labelSettlement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSettlement.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelSettlement.Location = new System.Drawing.Point(90, 388);
            this.labelSettlement.Name = "labelSettlement";
            this.labelSettlement.Size = new System.Drawing.Size(169, 31);
            this.labelSettlement.TabIndex = 1000000045;
            this.labelSettlement.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(13, 387);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 32);
            this.label23.TabIndex = 1000000044;
            this.label23.Text = "ส่วนต่าง";
            // 
            // ButtonConfirm
            // 
            this.ButtonConfirm.BackColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderWidth = 1;
            this.ButtonConfirm.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonConfirm.ButtonText = "ยืนยัน";
            this.ButtonConfirm.CausesValidation = false;
            this.ButtonConfirm.EndColor = System.Drawing.Color.Silver;
            this.ButtonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonConfirm.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonConfirm.ForeColor = System.Drawing.Color.Black;
            this.ButtonConfirm.GradientAngle = 90;
            this.ButtonConfirm.Image = global::MyPayment.Properties.Resources.ok1;
            this.ButtonConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonConfirm.Location = new System.Drawing.Point(758, 389);
            this.ButtonConfirm.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonConfirm.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonConfirm.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonConfirm.Name = "ButtonConfirm";
            this.ButtonConfirm.ShowButtontext = true;
            this.ButtonConfirm.Size = new System.Drawing.Size(90, 28);
            this.ButtonConfirm.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.TabIndex = 1000000042;
            this.ButtonConfirm.TextLocation_X = 35;
            this.ButtonConfirm.TextLocation_Y = 1;
            this.ButtonConfirm.Transparent1 = 80;
            this.ButtonConfirm.Transparent2 = 120;
            this.ButtonConfirm.UseVisualStyleBackColor = true;
            this.ButtonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.Silver;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close2;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(855, 389);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(90, 28);
            this.ButtonCancel.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.TabIndex = 1000000043;
            this.ButtonCancel.TextLocation_X = 35;
            this.ButtonCancel.TextLocation_Y = 1;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(636, 387);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(46, 32);
            this.label51.TabIndex = 1000000041;
            this.label51.Text = "บาท";
            // 
            // labelTotalAmount
            // 
            this.labelTotalAmount.BackColor = System.Drawing.Color.White;
            this.labelTotalAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTotalAmount.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelTotalAmount.Location = new System.Drawing.Point(467, 388);
            this.labelTotalAmount.Name = "labelTotalAmount";
            this.labelTotalAmount.Size = new System.Drawing.Size(166, 31);
            this.labelTotalAmount.TabIndex = 1000000040;
            this.labelTotalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(342, 387);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(124, 32);
            this.label49.TabIndex = 1000000037;
            this.label49.Text = "รวมเงินทั้งหมด";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.labelAmount2);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.TextAmount2);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.labelAmount1);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.TextAmount1);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.labelAmount500);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.TextAmount500);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.labelAmount25st);
            this.groupBox2.Controls.Add(this.TextAmount25st);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.label47);
            this.groupBox2.Controls.Add(this.labelAmount);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.label48);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.labelAmount50st);
            this.groupBox2.Controls.Add(this.labelAmount5);
            this.groupBox2.Controls.Add(this.labelAmount10);
            this.groupBox2.Controls.Add(this.labelAmount20);
            this.groupBox2.Controls.Add(this.labelAmount50);
            this.groupBox2.Controls.Add(this.labelAmount100);
            this.groupBox2.Controls.Add(this.labelAmount1000);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.TextAmount50st);
            this.groupBox2.Controls.Add(this.TextAmount5);
            this.groupBox2.Controls.Add(this.TextAmount10);
            this.groupBox2.Controls.Add(this.TextAmount20);
            this.groupBox2.Controls.Add(this.TextAmount50);
            this.groupBox2.Controls.Add(this.TextAmount100);
            this.groupBox2.Controls.Add(this.TextAmount1000);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox2.Location = new System.Drawing.Point(41, -1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox2.Size = new System.Drawing.Size(915, 384);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "เงินสด";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(567, 315);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 28);
            this.label22.TabIndex = 1000000057;
            this.label22.Text = "บาท";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(567, 348);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 28);
            this.label24.TabIndex = 1000000056;
            this.label24.Text = "บาท";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(567, 249);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(39, 28);
            this.label29.TabIndex = 1000000055;
            this.label29.Text = "บาท";
            // 
            // labelAmount2
            // 
            this.labelAmount2.BackColor = System.Drawing.Color.White;
            this.labelAmount2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount2.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount2.Location = new System.Drawing.Point(426, 248);
            this.labelAmount2.Name = "labelAmount2";
            this.labelAmount2.Size = new System.Drawing.Size(133, 31);
            this.labelAmount2.TabIndex = 1000000054;
            this.labelAmount2.Text = "0";
            this.labelAmount2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(347, 249);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 28);
            this.label39.TabIndex = 1000000053;
            this.label39.Text = "เหรียญ";
            // 
            // TextAmount2
            // 
            this.TextAmount2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount2.Location = new System.Drawing.Point(211, 248);
            this.TextAmount2.Name = "TextAmount2";
            this.TextAmount2.Size = new System.Drawing.Size(133, 31);
            this.TextAmount2.TabIndex = 7;
            this.TextAmount2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount2.TextChanged += new System.EventHandler(this.TextAmount2_TextChanged);
            this.TextAmount2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount2_KeyPress);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(43, 249);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(104, 28);
            this.label42.TabIndex = 1000000051;
            this.label42.Text = "เหรียญ 2 บาท";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(567, 282);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 28);
            this.label14.TabIndex = 1000000050;
            this.label14.Text = "บาท";
            // 
            // labelAmount1
            // 
            this.labelAmount1.BackColor = System.Drawing.Color.White;
            this.labelAmount1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount1.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount1.Location = new System.Drawing.Point(426, 281);
            this.labelAmount1.Name = "labelAmount1";
            this.labelAmount1.Size = new System.Drawing.Size(133, 31);
            this.labelAmount1.TabIndex = 1000000049;
            this.labelAmount1.Text = "0";
            this.labelAmount1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(347, 282);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(58, 28);
            this.label27.TabIndex = 1000000048;
            this.label27.Text = "เหรียญ";
            // 
            // TextAmount1
            // 
            this.TextAmount1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount1.Location = new System.Drawing.Point(211, 281);
            this.TextAmount1.Name = "TextAmount1";
            this.TextAmount1.Size = new System.Drawing.Size(133, 31);
            this.TextAmount1.TabIndex = 8;
            this.TextAmount1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount1.TextChanged += new System.EventHandler(this.TextAmount1_TextChanged);
            this.TextAmount1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount1_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(43, 282);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(103, 28);
            this.label28.TabIndex = 1000000046;
            this.label28.Text = "เหรียญ 1 บาท";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(567, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 28);
            this.label13.TabIndex = 1000000045;
            this.label13.Text = "บาท";
            // 
            // labelAmount500
            // 
            this.labelAmount500.BackColor = System.Drawing.Color.White;
            this.labelAmount500.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount500.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount500.Location = new System.Drawing.Point(426, 50);
            this.labelAmount500.Name = "labelAmount500";
            this.labelAmount500.Size = new System.Drawing.Size(133, 31);
            this.labelAmount500.TabIndex = 1000000044;
            this.labelAmount500.Text = "0";
            this.labelAmount500.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(347, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 28);
            this.label15.TabIndex = 1000000043;
            this.label15.Text = "ใบ";
            // 
            // TextAmount500
            // 
            this.TextAmount500.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount500.Location = new System.Drawing.Point(211, 50);
            this.TextAmount500.Name = "TextAmount500";
            this.TextAmount500.Size = new System.Drawing.Size(133, 31);
            this.TextAmount500.TabIndex = 1;
            this.TextAmount500.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount500.TextChanged += new System.EventHandler(this.TextAmount500_TextChanged_1);
            this.TextAmount500.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount500_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(43, 51);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(120, 28);
            this.label26.TabIndex = 1000000041;
            this.label26.Text = "ธนบัตร 500 บาท";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(347, 348);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(58, 28);
            this.label38.TabIndex = 1000000040;
            this.label38.Text = "เหรียญ";
            // 
            // labelAmount25st
            // 
            this.labelAmount25st.BackColor = System.Drawing.Color.White;
            this.labelAmount25st.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount25st.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount25st.Location = new System.Drawing.Point(426, 347);
            this.labelAmount25st.Name = "labelAmount25st";
            this.labelAmount25st.Size = new System.Drawing.Size(133, 31);
            this.labelAmount25st.TabIndex = 1000000039;
            this.labelAmount25st.Text = "0";
            this.labelAmount25st.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextAmount25st
            // 
            this.TextAmount25st.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount25st.Location = new System.Drawing.Point(211, 347);
            this.TextAmount25st.Name = "TextAmount25st";
            this.TextAmount25st.Size = new System.Drawing.Size(133, 31);
            this.TextAmount25st.TabIndex = 10;
            this.TextAmount25st.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount25st.TextChanged += new System.EventHandler(this.TextAmount25st_TextChanged);
            this.TextAmount25st.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount25st_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(347, 315);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 28);
            this.label37.TabIndex = 1000000037;
            this.label37.Text = "เหรียญ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(567, 216);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(39, 28);
            this.label36.TabIndex = 1000000036;
            this.label36.Text = "บาท";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(863, 31);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(46, 32);
            this.label47.TabIndex = 1000000035;
            this.label47.Text = "บาท";
            // 
            // labelAmount
            // 
            this.labelAmount.BackColor = System.Drawing.Color.White;
            this.labelAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount.Font = new System.Drawing.Font("TH Sarabun New", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelAmount.Location = new System.Drawing.Point(700, 29);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(161, 35);
            this.labelAmount.TabIndex = 1000000036;
            this.labelAmount.Text = "0";
            this.labelAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(567, 183);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(39, 28);
            this.label35.TabIndex = 1000000035;
            this.label35.Text = "บาท";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(567, 150);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(39, 28);
            this.label34.TabIndex = 1000000034;
            this.label34.Text = "บาท";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(653, 29);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(45, 32);
            this.label48.TabIndex = 1000000034;
            this.label48.Text = "รวม";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(567, 117);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(39, 28);
            this.label32.TabIndex = 1000000032;
            this.label32.Text = "บาท";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(567, 84);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(39, 28);
            this.label31.TabIndex = 1000000031;
            this.label31.Text = "บาท";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(567, 18);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(39, 28);
            this.label30.TabIndex = 1000000030;
            this.label30.Text = "บาท";
            // 
            // labelAmount50st
            // 
            this.labelAmount50st.BackColor = System.Drawing.Color.White;
            this.labelAmount50st.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount50st.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount50st.Location = new System.Drawing.Point(426, 314);
            this.labelAmount50st.Name = "labelAmount50st";
            this.labelAmount50st.Size = new System.Drawing.Size(133, 31);
            this.labelAmount50st.TabIndex = 1000000029;
            this.labelAmount50st.Text = "0";
            this.labelAmount50st.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAmount5
            // 
            this.labelAmount5.BackColor = System.Drawing.Color.White;
            this.labelAmount5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount5.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount5.Location = new System.Drawing.Point(426, 215);
            this.labelAmount5.Name = "labelAmount5";
            this.labelAmount5.Size = new System.Drawing.Size(133, 31);
            this.labelAmount5.TabIndex = 1000000028;
            this.labelAmount5.Text = "0";
            this.labelAmount5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAmount10
            // 
            this.labelAmount10.BackColor = System.Drawing.Color.White;
            this.labelAmount10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount10.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount10.Location = new System.Drawing.Point(426, 182);
            this.labelAmount10.Name = "labelAmount10";
            this.labelAmount10.Size = new System.Drawing.Size(133, 31);
            this.labelAmount10.TabIndex = 1000000027;
            this.labelAmount10.Text = "0";
            this.labelAmount10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAmount20
            // 
            this.labelAmount20.BackColor = System.Drawing.Color.White;
            this.labelAmount20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount20.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount20.Location = new System.Drawing.Point(426, 149);
            this.labelAmount20.Name = "labelAmount20";
            this.labelAmount20.Size = new System.Drawing.Size(133, 31);
            this.labelAmount20.TabIndex = 1000000026;
            this.labelAmount20.Text = "0";
            this.labelAmount20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAmount50
            // 
            this.labelAmount50.BackColor = System.Drawing.Color.White;
            this.labelAmount50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount50.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount50.Location = new System.Drawing.Point(426, 116);
            this.labelAmount50.Name = "labelAmount50";
            this.labelAmount50.Size = new System.Drawing.Size(133, 31);
            this.labelAmount50.TabIndex = 1000000025;
            this.labelAmount50.Text = "0";
            this.labelAmount50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAmount100
            // 
            this.labelAmount100.BackColor = System.Drawing.Color.White;
            this.labelAmount100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount100.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold);
            this.labelAmount100.Location = new System.Drawing.Point(426, 83);
            this.labelAmount100.Name = "labelAmount100";
            this.labelAmount100.Size = new System.Drawing.Size(133, 31);
            this.labelAmount100.TabIndex = 1000000023;
            this.labelAmount100.Text = "0";
            this.labelAmount100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAmount1000
            // 
            this.labelAmount1000.BackColor = System.Drawing.Color.White;
            this.labelAmount1000.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmount1000.Font = new System.Drawing.Font("TH Sarabun New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmount1000.Location = new System.Drawing.Point(426, 17);
            this.labelAmount1000.Name = "labelAmount1000";
            this.labelAmount1000.Size = new System.Drawing.Size(133, 31);
            this.labelAmount1000.TabIndex = 1000000022;
            this.labelAmount1000.Text = "0";
            this.labelAmount1000.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(347, 216);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 28);
            this.label21.TabIndex = 28;
            this.label21.Text = "เหรียญ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(347, 183);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 28);
            this.label20.TabIndex = 27;
            this.label20.Text = "เหรียญ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(347, 150);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 28);
            this.label19.TabIndex = 26;
            this.label19.Text = "ใบ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(347, 117);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 28);
            this.label18.TabIndex = 25;
            this.label18.Text = "ใบ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(347, 84);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 28);
            this.label17.TabIndex = 24;
            this.label17.Text = "ใบ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(347, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 28);
            this.label16.TabIndex = 23;
            this.label16.Text = "ใบ";
            // 
            // TextAmount50st
            // 
            this.TextAmount50st.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount50st.Location = new System.Drawing.Point(211, 314);
            this.TextAmount50st.Name = "TextAmount50st";
            this.TextAmount50st.Size = new System.Drawing.Size(133, 31);
            this.TextAmount50st.TabIndex = 9;
            this.TextAmount50st.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount50st.TextChanged += new System.EventHandler(this.TextAmount50st_TextChanged);
            this.TextAmount50st.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount50st_KeyPress);
            // 
            // TextAmount5
            // 
            this.TextAmount5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount5.Location = new System.Drawing.Point(211, 215);
            this.TextAmount5.Name = "TextAmount5";
            this.TextAmount5.Size = new System.Drawing.Size(133, 31);
            this.TextAmount5.TabIndex = 6;
            this.TextAmount5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount5.TextChanged += new System.EventHandler(this.TextAmount5_TextChanged);
            this.TextAmount5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount5_KeyPress);
            // 
            // TextAmount10
            // 
            this.TextAmount10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount10.Location = new System.Drawing.Point(211, 182);
            this.TextAmount10.Name = "TextAmount10";
            this.TextAmount10.Size = new System.Drawing.Size(133, 31);
            this.TextAmount10.TabIndex = 5;
            this.TextAmount10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount10.TextChanged += new System.EventHandler(this.TextAmount10_TextChanged);
            this.TextAmount10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount10_KeyPress);
            // 
            // TextAmount20
            // 
            this.TextAmount20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount20.Location = new System.Drawing.Point(211, 149);
            this.TextAmount20.Name = "TextAmount20";
            this.TextAmount20.Size = new System.Drawing.Size(133, 31);
            this.TextAmount20.TabIndex = 4;
            this.TextAmount20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount20.TextChanged += new System.EventHandler(this.TextAmount20_TextChanged);
            this.TextAmount20.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount20_KeyPress);
            // 
            // TextAmount50
            // 
            this.TextAmount50.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount50.Location = new System.Drawing.Point(211, 116);
            this.TextAmount50.Name = "TextAmount50";
            this.TextAmount50.Size = new System.Drawing.Size(133, 31);
            this.TextAmount50.TabIndex = 3;
            this.TextAmount50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount50.TextChanged += new System.EventHandler(this.TextAmount50_TextChanged);
            this.TextAmount50.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount50_KeyPress);
            // 
            // TextAmount100
            // 
            this.TextAmount100.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount100.Location = new System.Drawing.Point(211, 83);
            this.TextAmount100.Name = "TextAmount100";
            this.TextAmount100.Size = new System.Drawing.Size(133, 31);
            this.TextAmount100.TabIndex = 2;
            this.TextAmount100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount100.TextChanged += new System.EventHandler(this.TextAmount100_TextChanged);
            this.TextAmount100.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount100_KeyPress);
            // 
            // TextAmount1000
            // 
            this.TextAmount1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount1000.Location = new System.Drawing.Point(211, 17);
            this.TextAmount1000.Name = "TextAmount1000";
            this.TextAmount1000.Size = new System.Drawing.Size(133, 31);
            this.TextAmount1000.TabIndex = 0;
            this.TextAmount1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextAmount1000.TextChanged += new System.EventHandler(this.TextAmount1000_TextChanged);
            this.TextAmount1000.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount1000_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(43, 348);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 28);
            this.label12.TabIndex = 12;
            this.label12.Text = "เหรียญ 25 สตางค์";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(43, 315);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 28);
            this.label11.TabIndex = 11;
            this.label11.Text = "เหรียญ 50 สตางค์";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(43, 216);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 28);
            this.label10.TabIndex = 10;
            this.label10.Text = "เหรียญ 5 บาท";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(43, 183);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 28);
            this.label9.TabIndex = 9;
            this.label9.Text = "เหรียญ 10 บาท";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(43, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 28);
            this.label8.TabIndex = 8;
            this.label8.Text = "ธนบัตร 20 บาท";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(43, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 28);
            this.label7.TabIndex = 7;
            this.label7.Text = "ธนบัตร 50 บาท";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(43, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 28);
            this.label6.TabIndex = 6;
            this.label6.Text = "ธนบัตร 100 บาท";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 28);
            this.label5.TabIndex = 5;
            this.label5.Text = "ธนบัตร 500 บาท";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(43, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 28);
            this.label4.TabIndex = 4;
            this.label4.Text = "ธนบัตร 1000 บาท";
            // 
            // FormRemittance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 740);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormRemittance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ใบนำส่งเงินเข้าคลัง";
            this.Load += new System.EventHandler(this.FormRemittance_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDetailCheque)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewCard)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label Label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label LabelStationNo;
        internal System.Windows.Forms.Label labelEmployeeCode;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        private System.Windows.Forms.DataGridView GridViewDetailCheque;
        private System.Windows.Forms.DataGridView GridViewCard;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label labelAmountCheq;
        internal System.Windows.Forms.Label label41;
        internal System.Windows.Forms.Label label40;
        internal System.Windows.Forms.Label labelAmountCard;
        internal System.Windows.Forms.Label label44;
        internal System.Windows.Forms.Label label45;
        internal System.Windows.Forms.Label label51;
        internal System.Windows.Forms.Label labelTotalAmount;
        internal System.Windows.Forms.Label label49;
        internal System.Windows.Forms.Label labelAmount;
        internal System.Windows.Forms.Label label47;
        internal System.Windows.Forms.Label label48;
        internal System.Windows.Forms.Label label38;
        internal System.Windows.Forms.Label labelAmount25st;
        private System.Windows.Forms.TextBox TextAmount25st;
        internal System.Windows.Forms.Label label37;
        internal System.Windows.Forms.Label label36;
        internal System.Windows.Forms.Label label35;
        internal System.Windows.Forms.Label label34;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.Label label31;
        internal System.Windows.Forms.Label label30;
        internal System.Windows.Forms.Label labelAmount50st;
        internal System.Windows.Forms.Label labelAmount5;
        internal System.Windows.Forms.Label labelAmount10;
        internal System.Windows.Forms.Label labelAmount20;
        internal System.Windows.Forms.Label labelAmount50;
        internal System.Windows.Forms.Label labelAmount100;
        internal System.Windows.Forms.Label labelAmount1000;
        internal System.Windows.Forms.Label label21;
        internal System.Windows.Forms.Label label20;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TextAmount50st;
        private System.Windows.Forms.TextBox TextAmount5;
        private System.Windows.Forms.TextBox TextAmount10;
        private System.Windows.Forms.TextBox TextAmount20;
        private System.Windows.Forms.TextBox TextAmount50;
        private System.Windows.Forms.TextBox TextAmount100;
        private System.Windows.Forms.TextBox TextAmount1000;
        private Custom_Controls_in_CS.ButtonZ ButtonConfirm;
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        internal System.Windows.Forms.Label label52;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label labelSettlement;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label lblAmount;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label labelAmount500;
        internal System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TextAmount500;
        internal System.Windows.Forms.Label label26;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label labelAmount1;
        internal System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox TextAmount1;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label labelAmount2;
        internal System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox TextAmount2;
        internal System.Windows.Forms.Label label42;
        private System.Windows.Forms.CheckBox CkSelectCheqAll;
        private System.Windows.Forms.CheckBox CkSelectCardAll;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColCkCheq;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColChequeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColbankName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColchequeAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColchequeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColchequeOwner;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColCkCard;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCardNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAmountCard;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColcardOwner;
        private System.Windows.Forms.DataGridViewTextBoxColumn colcardId;
        private System.Windows.Forms.DataGridViewTextBoxColumn collNo;
    }
}