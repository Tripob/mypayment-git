﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPayment
{
    public partial class FormTestPrinter : Form
    {
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        public FormTestPrinter()
        {
            InitializeComponent();
        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            SendDataAddPrintData();
        }
        public bool SendDataAddPrintData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool status = false;
            try
            {
                string Path = "";
                Path = ReportPath + "\\" + "ReportTestPrinter.rpt";
                ReportDocument cryRpt = new ReportDocument();
                cryRpt.Load(Path);
                CrystalReportViewer crystalReportViewer1 = new CrystalReportViewer();
                crystalReportViewer1.ReportSource = cryRpt;
                crystalReportViewer1.Refresh();
                cryRpt.PrintToPrinter(1, true, 0, 0);
                this.Hide();
            }
            catch (Exception ex)
            {
            }
            return status;
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
