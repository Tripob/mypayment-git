﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClassDisplayCancelReceipt
    {
        public string result_code { get; set; }
        public string result_message { get; set; }
        public string version_control { get; set; }
        public List<displayCancelReceiptResponseDtoList> displayCancelReceiptResponseDtoList { get; set; }
    }
}
