﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClassRemittanceSlip : ResultList
    {
        public  string EmpId { get; set; }
        public DateTime? Datetime { get; set; }
        public int StationNo { get; set; }
        public double TotalAmountAll { get; set; }
        public double TotalAmountCheqce { get; set; }
        public int? CountCheqce { get; set; }
        public double TotalAmountCard { get; set; }
        public int? CountCard { get; set; }
        public double TotalAmount { get; set; }
        public int? Count1000 { get; set; }
        public int? Count500 { get; set; }
        public int? Count100 { get; set; }
        public int? Count50 { get; set; }
        public int? Count20 { get; set; }
        public int? Count10 { get; set; }
        public int? Count5 { get; set; }
        public int? Count2 { get; set; }
        public int? Count1 { get; set; }
        public int? CountSatang50 { get; set; }
        public int? CountSatang25 { get; set; }
        public double Amount1000 { get; set; }
        public double Amount500 { get; set; }
        public double Amount100 { get; set; }
        public double Amount50 { get; set; }
        public double Amount20 { get; set; }
        public double Amount10 { get; set; }
        public double Amount5 { get; set; }
        public double Amount2 { get; set; }
        public double Amount1 { get; set; }
        public double AmountSatang50 { get; set; }
        public double AmountSatang25 { get; set; }
    }  
}
