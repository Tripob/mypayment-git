﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyPayment.Controllers;

namespace MyPayment.Models
{
    class LogService
    {
        public enum LogLevel
        {
            Debug = 1,
            Release = 2,
            Production = 3
        }
        private string logControlLevel;
        private string logFile;
        private string logName = "log4net";
        private int maxLogFileSize;
        private static Hashtable htNoLogRod = new Hashtable();
        private static Hashtable htLogTemp = new Hashtable();

        public LogService(string logName)
        {
            this.logFile = GeneralService.LogPath + @"\" + this.MakeSubDirectory(logName) + @"\" + logName + "-[" + DateTime.Now.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo) + "].txt";
            this.maxLogFileSize = GeneralService.MaxLogFileSize;
            this.logName = logName;
            if (!Directory.Exists(GeneralService.LogPath + @"\" + this.MakeSubDirectory(logName)))
                Directory.CreateDirectory(GeneralService.LogPath + @"\" + this.MakeSubDirectory(logName));
        }
        public LogService(string logName, string subDirectory)
        {
            this.logName = logName;
            if ((subDirectory.Trim().Length == 0))
            {		
                this.logFile = GeneralService.LogPath + @"\" + logName + "-[" + DateTime.Now.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo) + "].txt";
                this.logControlLevel = GeneralService.LogControlLevel;
                this.maxLogFileSize = GeneralService.MaxLogFileSize;
                if ((!Directory.Exists(GeneralService.LogPath + @"\")))
                    Directory.CreateDirectory(GeneralService.LogPath + @"\");
            }
            else
            {		
                this.logFile = GeneralService.LogPath + @"\" + subDirectory + @"\" + logName + "-[" + DateTime.Now.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo) + "].txt";
                this.logControlLevel = GeneralService.LogControlLevel;
                this.maxLogFileSize = GeneralService.MaxLogFileSize;
                if ((!Directory.Exists(GeneralService.LogPath + @"\" + subDirectory)))
                    Directory.CreateDirectory(GeneralService.LogPath + @"\" + subDirectory);
            }
        }       
        private string GenFormattedLogText(LogLevel logLevel, string logType, MethodBase method, string logText)
        {
            return DateTime.Now.ToString("HH:mm:ss.fff", DateTimeFormatInfo.InvariantInfo) + " | " + AppDomain.GetCurrentThreadId().ToString() + " | " + method.Name + "() | " + logText;
        }
        private void HandleException(Exception exeption, string logText)
        {
            this.WriteEventLogError("Could not write a log to the file '" + this.logFile + @"'.\n\nLog Text : " + logText + @"\n\nCause from the exception : " + exeption.Message);
        }
        private void WriteEventLog(string logText, string eventLogName, string sourceName, EventLogEntryType type)
        {
            EventLog eventLog = new EventLog(eventLogName);
            eventLog.Source = sourceName;
            eventLog.WriteEntry(logText, type);
            eventLog.Close();
        }
        private void WriteLogFile(string logText)
        {
            StreamWriter writer = null;
            try
            {
                writer = File.AppendText(logFile);
                FileInfo fileInfo = new FileInfo(logFile);
                if ((fileInfo.Length > (long)this.maxLogFileSize))
                {
                    writer.Close();
                    string backUpLogFile = logFile.Remove(logFile.Length - 4, 4) + "-[" + DateTime.Now.ToString("HH-mm-ss-fff", DateTimeFormatInfo.InvariantInfo) + "].txt";
                    File.Copy(logFile, backUpLogFile, true);
                    File.Delete(logFile);
                    writer = File.AppendText(logFile);
                }
                Console.WriteLine(logText);
                writer.WriteLine(logText);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if ((!(writer == null)))
                    writer.Close();
            }
        }
        private void WriteLogFileNotNewline(string logText)
        {
            StreamWriter writer = null;
            try
            {
                writer = File.AppendText(logFile);
                FileInfo fileInfo = new FileInfo(logFile);
                
                if ((fileInfo.Length > (long)this.maxLogFileSize))
                {
                    writer.Close();
                    string backUpLogFile = logFile.Remove(logFile.Length - 4, 4) + "-[" + DateTime.Now.ToString("HH-mm-ss-fff", DateTimeFormatInfo.InvariantInfo) + "].txt";
                    File.Copy(logFile, backUpLogFile, true);
                    File.Delete(logFile);
                    writer = File.AppendText(logFile);
                }
                Console.WriteLine(logText);
                writer.Write(logText);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if ((!(writer == null)))
                    writer.Close();
            }
        }
        private void WriteLogFile(string logText, LogLevel logLevel)
        {
            if (LogService.htNoLogRod.ContainsKey(this.logName))
                return;
            if ((this.logControlLevel ?? "") == "None")
                return;
            if ((this.logControlLevel ?? "") == "Production")
            {
                if (((int)logLevel == (int)LogLevel.Debug) | ((int)logLevel == (int)LogLevel.Release))
                    return;
            }
            else if ((this.logControlLevel ?? "") == "Release")
            {
                if ((int)logLevel == (int)LogLevel.Debug)
                    return;
            }
            int k = 0;
            int n = 24;
            while ((k < n))
            {
                k = k + 1;
                try
                {
                    if ((htLogTemp.ContainsKey(this.logName)))
                    {
                        logText = Convert.ToString(htLogTemp[this.logName]) + @"\r\n" + logText;
                        htLogTemp.Remove(this.logName);
                    }
                    this.WriteLogFile(logText);
                    break;
                }
                catch (Exception ex)
                {
                    if ((k < n))
                        continue;
                }
            }
        }
        private void WriteLogFileS(string logText, LogLevel logLevel)
        {
            int k = 0;
            int n = 24;
            while ((k < n))
            {
                k = k + 1;
                try
                {
                    if ((htLogTemp.ContainsKey(this.logName)))
                    {
                        logText = Convert.ToString(htLogTemp[this.logName]) + @"\r\n" + logText;
                        htLogTemp.Remove(this.logName);
                    }
                    this.WriteLogFileNotNewline("," + logText);
                    break;
                }
                catch (Exception ex)
                {
                    if ((k < n))
                        continue;
                }
            }
        }
        private void WriteLogFileSLn(string logText, LogLevel logLevel)
        {
            int k = 0;
            int n = 24;
            while ((k < n))
            {
                k = k + 1;
                try
                {
                    if ((htLogTemp.ContainsKey(this.logName)))
                    {
                        logText = Convert.ToString(htLogTemp[this.logName]) + @"\r\n" + logText;
                        htLogTemp.Remove(this.logName);
                    }
                    this.WriteLogFile(logText);
                    break;
                }
                catch (Exception ex)
                {
                    if ((k < n))
                        continue;
                }
            }
        }

        private string MakeSubDirectory(string logName)
        {
            string output = logName;          
            return output;
        }
        public void WriteData(Hashtable htData, MethodBase method, LogLevel logLevel)
        {
            StringBuilder sb = new StringBuilder();
            //DictionaryEntry item = default(DictionaryEntry);
            foreach (var item in htData)
            {
                //sb.Append(item.Key + "( ");
                //sb.Append(Interaction.IIf(!Operators.ConditionalCompareObjectEqual(item.Value, null, false), item.Value, "NULL"));
                //sb.Append(" ), ");
            }

            if ((sb.Length != 0))
                sb.Remove(sb.Length - 2, 2);

            this.WriteLogFile(this.GenFormattedLogText(logLevel, "Data", method, sb.ToString()), logLevel);
        }
        public void WriteData(string logText, MethodBase method, LogLevel logLevel)
        {
            this.WriteLogFile(this.GenFormattedLogText(logLevel, "Data", method, logText), logLevel);
        }
        public void Write(string logText)
        {
            this.WriteLogFileS(logText, LogLevel.Debug);
        }
        public void Writeln(string logText)
        {
            this.WriteLogFileSLn(logText, LogLevel.Debug);
        }
        public void WriteEventLogError(string logText)
        {
            this.WriteEventLog(logText, "Application", "TSD.PTI.NET", EventLogEntryType.Error);
        }
        public void WriteEventLogInfo(string logText)
        {
            this.WriteEventLog(logText, "Application", "TSD.PTI.NET", EventLogEntryType.Information);
        }
        public void WriteException(Exception exception, MethodBase method, LogLevel logLevel)
        {
            this.WriteLogFile(this.GenFormattedLogText(logLevel, "Exception", method, exception.Message), logLevel);
        }
        public void WriteInfo(string logText, MethodBase method, LogLevel logLevel)
        {
            this.WriteLogFile(this.GenFormattedLogText(logLevel, "Information", method, logText), logLevel);
        }

    }
}
