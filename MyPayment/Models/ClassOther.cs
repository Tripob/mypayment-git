﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MyPayment.Models
{
    public class ClassOther
    {
        public string InvNo { get; set; }
        public string Ca { get; set; }
        public string CustName { get; set; }
        public string Address { get; set; }
        public string BusinessType { get; set; }
        public string Department { get; set; }
        public string profit { get; set; }
        public string TypeItem { get; set; }
        public string CodeId { get; set; }
        public string Unit { get; set; }
        public decimal UnitPrice { get; set; }
        public int CountUnit { get; set; }
        public decimal Amount { get; set; }
        public string AccountCode { get; set; }
        public int PercentVat { get; set; }
        public decimal Vat { get; set; }
        public decimal CountAmount { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public bool Status { get; set; }
        public bool SelectCheck { get; set; }
        public bool StatusOrderBy { get; set; }
        public string ChequeNo { get; set; }
    }
    public class ClassOtherReceipt
    {
        public int index { get; set; }
        public string cashierId { get; set; }
        public string cashierName { get; set; }
        public string receiptRefNo { get; set; }
        public string receiptNo { get; set; }
        public string receiptPayer { get; set; }
        public string receiptPayerAddress { get; set; }
        public string distName { get; set; }
        public string receiptDtlNo { get; set; }
        public int receiptDtlUnit { get; set; }
        
        public string receiptDtlName { get; set; }
        public decimal receiptDtlVat { get; set; }
        public decimal receiptDtlAmount { get; set; }
        public decimal receiptDtlTotalAmount { get; set; }
        public decimal intAmount { get; set; }

        public decimal ITEM_PRICE_WITH_VAT { get; set; }

        public int receiptPercentVat { get; set; }
        public string str1 { get; set; }
        public string str2 { get; set; }
        public string str3 { get; set; }
        public string strAmount { get; set; }

    }
}
