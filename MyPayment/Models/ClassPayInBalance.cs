﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassPayInBalance
    {
        public string payInDateStr { get; set; }
        public int distId { get; set; }
        public int cashierId { get; set; }
    }
}
