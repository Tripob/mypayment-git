﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class paymentList
    {
        public int distId { get; set; }
        public int cashierNo { get; set; }
        public int cashierId { get; set; }
        public double paymentTotalAmount { get; set; }
        public double paymentReceive { get; set; }
        public double paymentChange { get; set; }
        public double paymentRealChange { get; set; }
        public string paymentDate { get; set; }
        public int paymentChannel { get; set; }
        public string payeeFlag { get; set; }
        public int subDistId { get; set; }
        public int userId { get; set; }
        public string qNo { get; set; }
        public string collId { get; set; }
        public string paymentServer { get; set; }
        public List<paymentDetailList> paymentDetailList { get; set; }
        public List<paymentMethodList> paymentMethodList { get; set; }
        public List<receiptList> receiptList { get; set; }
    }
    public class paymentListOTH
    {
        public int distId { get; set; }
        public int cashierNo { get; set; }
        public int cashierId { get; set; }
        public double paymentTotalAmount { get; set; }
        public double paymentReceive { get; set; }
        public double paymentChange { get; set; }
        public double paymentRealChange { get; set; }
        public string paymentDate { get; set; }
        public int paymentChannel { get; set; }
        public string payeeFlag { get; set; }
        public int subDistId { get; set; }
        public int userId { get; set; }
        public string qNo { get; set; }
        public string collId { get; set; }
        public List<paymentDetailListOTH> paymentDetailList { get; set; }
        public List<paymentMethodList> paymentMethodList { get; set; }
        public List<receiptList> receiptList { get; set; }
    }
}
