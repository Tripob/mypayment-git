﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class receiptList
    {
        public int receiptTypeId { get; set; }
        public string receiptDebtType { get; set; }
        public string receiptDate { get; set; }
        public string receiptPayer { get; set; }
        public string receiptPayerAddress { get; set; }
        public string receiptPayerTaxid { get; set; }
        public string receiptPayerBranch { get; set; }
        public string distId { get; set; }
        public string distName { get; set; }
        public string receiptCustId { get; set; }
        public string receiptCustName { get; set; }
        public string receiptCustTaxid { get; set; }
        public string receiptCustBranch { get; set; }
        public string receiptReqAddress { get; set; }
        public string receiptCa { get; set; }
        public string receiptUi { get; set; }
        public string receiptAmount { get; set; }
        public string receiptPercentVat { get; set; }
        public string receiptVat { get; set; }
        public string receiptTotalAmount { get; set; }
        public double receiptIntAmount { get; set; }
        public double receiptIntTotalAmount { get; set; }
        public string receiptPayerFlag { get; set; }
        public string countCa { get; set; }
        public int countInvoice { get; set; }
        public List<receiptDetailDtoList> receiptDetailDtoList { get; set; }
        public string receiptNo { get; set; }
    }
}
