﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
  public  class inquiryInfoBeanList : ResultList
    {
        public string ca { get; set; }
        public string distId { get; set; }
        public string custGroup { get; set; }
        public string custType { get; set; }
        public string custId { get; set; }
        public string distTaxBranch { get; set; }
        public string startDate { get; set; }
        public string ui { get; set; }
        public string endDate { get; set; }
        public string collId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string bpIdNo { get; set; }
        public string bpAddrNo { get; set; }
        public string bpAddress { get; set; }
        public string coAddress { get; set; }
        public string payeeEleAddress { get; set; }
        public string payeeSerAddress { get; set; }
        public string payeeEleName { get; set; }
        public string payeeEleTax20 { get; set; }
        public string payeeEleTaxBranch { get; set; }
        public string payeeEleFlagOtherPerson { get; set; }
        public string payeeSerName { get; set; }
        public string dunningProcedure { get; set; }
        public string payeeSerTax20 { get; set; }
        public string payeeSerTaxBranch { get; set; }
        public string payeeSerFlagOtherPerson { get; set; }
        public string tarifId { get; set; }
        public string EleInvCount { get; set; }
        public decimal EleTotalAmount { get; set; }
        public string OtherTotalAmount { get; set; }
        public decimal EleTotalVat { get; set; }
        public string OtherTotalVat { get; set; }
        public bool SelectCheck { get; set; }
        public string lockBill { get; set; }
        public bool debtType { get; set; }
        public string Defaultpenalty { get; set; }
        public bool Status { get; set; }
        public string payeeFlag { get; set; }
        public decimal TotalAmount { get; set; }
        public bool StatusOrderBy { get; set; }
        public string ChequeNo { get; set; }
        public string Tax20 { get; set; }
        public string TaxBranch { get; set; }
        public double vatBalance { get; set; }
        public double? morRate { get; set; }
        public string type { get; set; }
        public string lockCheque { get; set; }
        public string TypeCheck { get; set; }
        public string collInvNo { get; set; }        
        public List<inquiryGroupDebtBeanList> inquiryGroupDebtBeanList { get; set; }
        public string billVatCode { get; set; }
        public string serviceVatCode { get; set; }
        public string billVatDesc { get; set; }
        public string serviceVatDesc { get; set; }
        public bool statusPartialpayment { get; set; }
        public bool StatuslockCheque { get; set; }
        public bool StatusLockDirectDebit { get; set; }
    }
}
