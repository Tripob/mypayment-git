﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class installmentDtlList
    {
        public string instDtlId { get; set; }
        public string instHdrId { get; set; }
        public string instDtlNo { get; set; }
        public string instDtlAmount { get; set; }
        public string instDtlVat { get; set; }
        public string instDtlTotalAmount { get; set; }
        public string instDtlDueDate { get; set; }
        public string instDtlBalance { get; set; }   
    }
}
