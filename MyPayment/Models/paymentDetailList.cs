﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class paymentDetailList
    {
        public Int64[] debtIdSet { get; set; }
        public decimal amount { get; set; }
        public int percentVat { get; set; }
        public decimal vat { get; set; }
        public decimal totalAmount { get; set; }
        public decimal intAmount { get; set; }
        public int? intDay { get; set; }
        public string meaUnit { get; set; }
        public decimal price { get; set; }
        public string debtType { get; set; }
        public int itemId { get; set; }
        public string invHdrNo { get; set; }
        public string mwaTaxId { get; set; }
        public string customerCode { get; set; }
        public string billDueDate { get; set; }
        public string billNumber { get; set; }
        public string check { get; set; }
        public string custId { get; set; }
        public string ca { get; set; }
        public string ui { get; set; }
        public string accountCode { get; set; }
        public string unitCode { get; set; }
        public string itemDesc1 { get; set; }
        public string itemDesc2 { get; set; }
        public string itemDesc3 { get; set; }
        public string businessTypeId { get; set; }
        public string profitCenter { get; set; }
        public string payeeName { get; set; }
        public string payeeAddress { get; set; }
        public string insDebtInsHdrId { get; set; }
        public List<paymentDetailSubList> paymentDetailSubList { get; set; }          
    }
    public class paymentDetailListOTH : paymentDetailList
    {
        public string payeeName { get; set; }
        public string payeeAddress { get; set; }
    }
   
}
