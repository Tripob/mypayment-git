﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassDirectPaymentPayIn
    {
        public string processType { get; set; }
        public string processStatus { get; set; }
        public string processMsg { get; set; }
        public string parameterIn { get; set; }
        public int closingId { get; set; }
    }
}
