﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassLogin
    {
        public string empId { get; set; }
        public string password { get; set; }
        public string ipAddress { get; set; }
        public string reqDate { get; set; }
        public string system { get; set; }
        public Int64 userId { get; set; }
    }
}
