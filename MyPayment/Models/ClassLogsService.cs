﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using MyPayment.Controllers;

namespace MyPayment.Models
{
    class ClassLogsService
    {
        public enum LogLevel
        {
            Debug = 1,
            Release = 2,
            Production = 3,
            Error = 4
        }
        private string _logFile;
        private string _logControlLevel;
        private string _logName = "log4net";
        private int _maxLogFileSize;
        private static Hashtable _htNoLogRod = new Hashtable();
        private static Hashtable _htLogTemp = new Hashtable();
        public ClassLogsService()
        {

        }
        public ClassLogsService(string logName, string logDetail)
        {
            _logFile = GeneralService.LogPath + @"\" + this.MakeSubDirectory(logName) + @"\" + this.MakeSubDirectory(logDetail) + @"\" + logName +"-"+ logDetail+ "-[" + DateTime.Now.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo) + "].txt";
            this._maxLogFileSize = GeneralService.MaxLogFileSize;
            this._logName = logName;
            if (!Directory.Exists(GeneralService.LogPath + @"\" + this.MakeSubDirectory(logName) + @"\" + this.MakeSubDirectory(logDetail)))
                Directory.CreateDirectory(GeneralService.LogPath + @"\" + this.MakeSubDirectory(logName) + @"\" + this.MakeSubDirectory(logDetail));
        }
        public void WriteData(string logText, MethodBase method, LogLevel logLevel)
        {
            this.WriteLogFile(this.GenFormattedLogText(logLevel, "Data", method, logText), logLevel);
        }
        public void WriteDataEvent(string logText, MethodBase method, LogLevel logLevel)
        {
            this.WriteLogFile(this.GenFormattedLogTextEvent(logLevel, "Data", method, logText), logLevel);
        }
        public void WriteDataError(string logText, MethodBase method, LogLevel logLevel)
        {
            this.WriteLogFile(this.GenFormattedLogTextError(logLevel, "Data", method, logText), logLevel);
        }
        public void WriteDataLogsError(string logText, MethodBase method, LogLevel logLevel)
        {
            this.WriteLogFile(GenFormattedLogTextNew(logLevel, method, logText), logLevel);
        }
        private string GenFormattedLogTextNew(LogLevel logLevel, MethodBase method, string logText)
        {
            return DateTime.Now.ToString("HH:mm:ss.fff", DateTimeFormatInfo.InvariantInfo) + " | " + method + " | " + logText;
        }
        private string GenFormattedLogTextError(LogLevel logLevel, string logType, MethodBase method, string logText)
        {
            return DateTime.Now.ToString("HH:mm:ss.fff", DateTimeFormatInfo.InvariantInfo) + " | " + method.Name + "() | " + logText;
        }
        private string GenFormattedLogTextEvent(LogLevel logLevel, string logType, MethodBase method, string logText)
        {
            return DateTime.Now.ToString("HH:mm:ss.fff", DateTimeFormatInfo.InvariantInfo) + " | " + method.Name + "() | " + logText;
        }
        private string GenFormattedLogText(LogLevel logLevel, string logType, MethodBase method, string logText)
        {
            return DateTime.Now.ToString("HH:mm:ss.fff", DateTimeFormatInfo.InvariantInfo) + " | " + logText;
        }
        private void WriteLogFile(string logText, LogLevel logLevel)
        {
            if (ClassLogsService._htNoLogRod.ContainsKey(this._logName))
                return;
            if ((this._logControlLevel == "None"))
                return;
            if ((this._logControlLevel == "Production"))
            {
                if (logLevel == LogLevel.Debug || logLevel == LogLevel.Release)
                    return;
            }
            else if ((this._logControlLevel == "Release"))
            {
                if ((logLevel == LogLevel.Debug))
                    return;
            }

            int k = 0;
            int n = 24;

            while ((k < n))
            {
                k = k + 1;
                try
                {
                    if (_htLogTemp.ContainsKey(this._logName))
                    {
                        logText = _htLogTemp[_logName].ToString() + @"\r\n" + logText;
                        _htLogTemp.Remove(this._logName);
                    }
                    this.WriteLogFile(logText);
                    break;
                }
                catch (Exception ex)
                {
                    if ((k < n))
                        continue;
                }
            }
        }
        private void WriteLogFile(string logText)
        {
            StreamWriter writer = null;
            try
            {
                writer = File.AppendText(_logFile);
                FileInfo fileInfo = new FileInfo(_logFile);
                if ((fileInfo.Length > this._maxLogFileSize))
                {
                    writer.Close();
                    string backUpLogFile = _logFile.Remove(_logFile.Length - 4, 4) + "-[" + DateTime.Now.ToString("HH-mm-ss-fff", DateTimeFormatInfo.InvariantInfo) + "].txt";
                    File.Copy(_logFile, backUpLogFile, true);
                    File.Delete(_logFile);
                    writer = File.AppendText(_logFile);
                }
                Console.WriteLine(logText);
                writer.WriteLine(logText);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                if ((!(writer == null)))
                    writer.Close();
            }
        }
        private string MakeSubDirectory(string logName)
        {
            string output = logName;
            return output;
        }
        public void WriteLogInformation(string msg, TextWriter w)
        {

            //w.Write(Environment.NewLine);
            w.Write("[{0} | {1}] |", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
            w.Write("\t");
            w.WriteLine(" {0}", msg);

            //w.WriteLine("-----------------------");


        }
    }
}
