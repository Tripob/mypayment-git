﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassUser
    {
        public string result_code { get; set; }
        public Int64 userId { get; set; }
        public string empId { get; set; }
        public string userName { get; set; }
        public int authorizeLevel { get; set; }
        public string result_message { get; set; }
        public string http { get; set; }
        public string password { get; set; }
        public string oldPassword { get; set; }
        public string distId { get; set; }
        public string distName { get; set; }
        public string counterNo { get; set; }
        public string cashDeskIpFlag { get; set; }
        public string ui { get; set; }
        public string ca { get; set; }
        public string custId { get; set; }
        public string invNo { get; set; }
        public string reqNo { get; set; }
        public string receiptBranch { get; set; }
        public string QNo { get; set; }
        public int subDistId { get; set; }
        public List<paymentResponseList> paymentResponseList { get; set; }
        public string Datetime { get; set; }
        public string collId { get; set; }
        public string collInvNo { get; set; }
        //public String resultMap { get; set; }
        public string debtType { get; set; }
        public List<String> successReceiptList { get; set; }
        public List<String> failureReceiptList { get; set; }
        public string qServerIp { get; set; }
    }
}
