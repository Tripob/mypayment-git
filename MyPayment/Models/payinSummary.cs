﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class payInResponseDto
    {
        public Int64 cashierId { get; set; }
        public string cashierNo { get; set; }
        public string createdDtStr { get; set; }
        public decimal cashTotalAmount { get; set; }
        public decimal chequeAmount { get; set; }
        public decimal cardAmount { get; set; }
        public string transferCash { get; set; }
        public string transferCheque { get; set; }
        public string transferCard { get; set; }
        public List<payInChequeResponseDtoList> payInChequeResponseDtoList { get; set; }
        public List<payInCardResponseDtoList> payInCardResponseDtoList { get; set; }
    }
}
