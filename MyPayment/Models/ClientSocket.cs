﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.Models
{
    public class ClientSocket : Component
    {
        //ClassLogsService LogQueueError = new ClassLogsService("QueueClass", "ErrorLog");
        public ClientSocket(IContainer Container) : this()
        {
            Container.Add(this);
        }
        public ClientSocket() : base()
        {
            InitializeComponent();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!(components == null))
                    components.Dispose();
            }
            base.Dispose(disposing);
        }
        private IContainer components;
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new Container();
        }
        private Socket _mySocket;
        private static ArrayList _dummy = new ArrayList();
        private static ArrayList _socketSyncObj = new ArrayList();
        private Int32 _packetSize = 10024;
        private bool _lineMode = true;
        private char _eolChar;
        private ASCIIEncoding _ascii = new ASCIIEncoding();
        private ISynchronizeInvoke _syncObject;
        private delegate void RaiseReceiveEvent(string receiveData);
        private delegate void RaiseConnectedEvent(bool connected);
        private delegate void RaiseExceptionEvent(Exception ex);
        public event ReceiveEventHandler Receive;
        public delegate void ReceiveEventHandler(string receiveData);
        public event ConnectedEventHandler Connected;
        public delegate void ConnectedEventHandler(bool connected);
        public event ExceptionEventHandler Exception;
        public delegate void ExceptionEventHandler(Exception ex);
        public bool Connect(string hostNameOrAddress, Int32 port)
        {
            bool ConnectRet = default(bool);
            ConnectRet = false;
            IPAddress serverAddress;
            try
            {
                serverAddress = Dns.Resolve(hostNameOrAddress).AddressList[0];
            }
            catch (Exception ex)
            {
                throw new Exception("Could not resolve Host name or Address.", ex);
            }
            try
            {
                this.Connect(serverAddress, port);
                ConnectRet = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ConnectRet;
        }
        public void Connect(IPAddress serverAddress, Int32 port)
        {
            IPEndPoint ep = new IPEndPoint(serverAddress, port);
            try
            {
                Connect(ep);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Connect(IPEndPoint endPoint)
        {
            _mySocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                _mySocket.Connect(endPoint);
                if (IsConnected() == true)
                    Connected?.Invoke(true);
            }
            catch (Exception ex)
            {
                throw new Exception(" not connect", ex);
            }
            byte[] bytes = new byte[_packetSize - 1 + 1];
            try
            {
                _mySocket.BeginReceive(bytes, 0, bytes.Length, SocketFlags.None, ReceiveCallBack, bytes);
            }
            catch (Exception ex)
            {
                throw new Exception("Error receiving data", ex);
            }
        }
        public bool IsConnected()
        {
            bool result;
            if (_mySocket == null)
                return false;

            lock (_socketSyncObj)
                result = _mySocket.Poll(1, SelectMode.SelectRead);
            if (_mySocket == null)
                return false;
            Int32 temp;
            lock (_socketSyncObj)
                temp = _mySocket.Available;
            if ((result == true) & (temp == 0))
                return false;
            else
                return true;
        }
        public void Send(string data)
        {
            if (IsConnected() == false)
                Connected?.Invoke(false);
            else
            {
                byte[] bytes = _ascii.GetBytes(data);
                lock (_socketSyncObj)
                    _mySocket.Send(bytes, bytes.Length, SocketFlags.None);
            }
        }
        public void Close()
        {
            if (IsConnected() == true)
            {
                lock (_socketSyncObj)
                {
                    _mySocket.Shutdown(SocketShutdown.Both);
                    _mySocket.Close();
                    _mySocket = null;
                    if (IsConnected() == false)
                        Connected?.Invoke(false);
                }
            }
        }
        protected virtual void OnReceive(string receivedData)
        {
            Receive?.Invoke(receivedData);
        }
        protected virtual void OnConnected(bool connected)
        {
            Connected?.Invoke(connected);
        }
        protected virtual void OnExcpetion(Exception ex)
        {
            Exception?.Invoke(ex);
        }
        private delegate void NoParamsDelegate();
        public void ReceiveCallBack(IAsyncResult ar)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                byte[] bytes = (byte[])ar.AsyncState;
                Int32 numBytes = _mySocket.EndReceive(ar);
                if (numBytes == 0)
                {
                    _mySocket.Shutdown(SocketShutdown.Both);
                    _mySocket.Close();
                }
                if (numBytes > 0)
                {
                    var oldBytes = bytes;
                    bytes = new byte[numBytes - 1 + 1];
                    if (oldBytes != null)
                        Array.Copy(oldBytes, bytes, Math.Min(numBytes - 1 + 1, oldBytes.Length));
                    string received = _ascii.GetString(bytes);

                    object[] args = new object[1];

                    RaiseReceiveEvent d = new RaiseReceiveEvent(OnReceive);

                    if (_lineMode == true)
                    {
                        char[] sep = new[] { EOLChar };
                        string[] lines = received.Split(sep);
                        Int32 i;
                        var loopTo = lines.Length - 1;
                        for (i = 0; i <= loopTo; i++)
                        {
                            if (i == (lines.Length - 1))
                                args[0] = lines[i];
                            else
                                args[0] = lines[i] + EOLChar.ToString();
                            try
                            {
                                _syncObject.Invoke(d, args);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else
                    {
                        args[0] = received;
                        _syncObject.Invoke(d, args);
                    }
                }

                if (IsConnected() == false)
                {
                    object[] args = new object[] { false };
                    RaiseConnectedEvent d = new RaiseConnectedEvent(OnConnected);
                    _syncObject.Invoke(d, args);
                }
                else
                {
                    bytes = new byte[PacketSize - 1 + 1];
                    try
                    {
                        _mySocket.BeginReceive(bytes, 0, bytes.Length, SocketFlags.None, ReceiveCallBack, bytes);
                    }
                    catch (Exception ex)
                    {
                        object[] args = new[] { ex };
                        RaiseExceptionEvent d = new RaiseExceptionEvent(OnExcpetion);
                        _syncObject.Invoke(d, args);

                        if (IsConnected() == false)
                        {
                            args[0] = false;
                            RaiseConnectedEvent dl = new RaiseConnectedEvent(OnConnected);
                            _syncObject.Invoke(dl, args);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //LogQueueError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public char EOLChar
        {
            get
            {
                char c;
                lock (_dummy)
                    c = _eolChar;
                return c;
            }
            set
            {
                lock (_dummy)
                    _eolChar = value;
            }
        }
        public bool LineMode
        {
            get
            {
                bool l;
                lock (_dummy)
                    l = _lineMode;
                return l;
            }
            set
            {
                lock (_dummy)
                    _lineMode = value;
            }
        }
        public Int32 PacketSize
        {
            get
            {
                Int32 pk;
                lock (_dummy)
                    pk = _packetSize;
                return pk;
            }
            set
            {
                lock (_dummy)
                    _packetSize = value;
            }
        }
        [Browsable(false)]
        public ISynchronizeInvoke SynchronizingObject
        {
            get
            {
                if (_syncObject == null & this.DesignMode)
                {
                    IDesignerHost designer = (IDesignerHost)this.GetService(typeof(IDesignerHost));
                    if (designer != null)
                        _syncObject = (ISynchronizeInvoke)designer.RootComponent;
                }
                return _syncObject;
            }
            set
            {
                if (!this.DesignMode)
                {
                    if (!(_syncObject == null) & !(_syncObject == value))
                        throw new Exception("Property ca not be set at run-time");
                    else
                        _syncObject = value;
                }
            }
        }
    }
}
