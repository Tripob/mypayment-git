﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassProcessDisplayPayIn
    {
        public string result_code { get; set; }
        public string result_message { get; set; }
        public string version_control { get; set; }
        public decimal totalCash { get; set; }
        public decimal totalCheque { get; set; }
        public decimal totalCard { get; set; }
        public decimal totalAmount { get; set; }
        public string strAmount { get; set; }
        public string strAmountNew { get; set; }
        public string userName { get; set; }
        public string userId { get; set; }
        public string strDate { get; set; }
        public string datePament { get; set; }
        public string distName { get; set; }
        public List<payInChequeResponseDtoList> payInChequeResponseDtoList { get; set; }
        public List<payInCardResponseDtoList> payInCardResponseDtoList { get; set; }
        public List<payInDisplayResponseDtoList> payInDisplayResponseDtoList { get; set; }
    }
}
