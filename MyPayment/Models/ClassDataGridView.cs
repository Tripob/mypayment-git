﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClassDataGridView
    {
        public bool Status { get; set; }
        public decimal Penalty { get; set; }
        public string IdDebt { get; set; }
        public int No { get; set; }
    }
}
