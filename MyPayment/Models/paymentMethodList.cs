﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
  public  class paymentMethodList
    {
        public int paymentMethodId { get; set; }
        public decimal cashTotalAmount { get; set; }
        public decimal cashReceive { get; set; }
        public string bankBranch { get; set; }
        public string chequeNo { get; set; }
        public string chequeOwner { get; set; }
        public string chequeDate { get; set; }
        public decimal chequeAmount { get; set; }
        public decimal chequeBalance { get; set; }
        public string chequeTel { get; set; }
        public string cashierCheque { get; set; }
        public string chequeApproveBy { get; set; }
        public string cardNo { get; set; }
        public decimal cardAmount { get; set; }
        public string cardOwner { get; set; }
        public string cardStartDate { get; set; }
        public string cardEndDate { get; set; }
    }
}
