﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ItemCodeData
    {
        public bool Selected { get; set; }
        public string TaxId { get; set; }

        public string BillNumber { get; set; }

        public string LastDayMemo { get; set; }
        public string TaxInvoice { get; set; }
        public string ContractAccount { get; set; }
        public string InvoiceNumber { get; set; }
        public string DueDate { get; set; }
        public string PowerUnit { get; set; }
        public string DebtCode { get; set; } // รหัสหนี้ค้างชำระ
        public string DayFine { get; set; }
        public string FineFlakedOut { get; set; }
        public string AmountExVat { get; set; }
        public string Vat { get; set; }
        public string AmountIncVat { get; set; }
        public string StopElectricityAmount { get; set; }
        public string ServiceType { get; set; }

        public string TotalAmountExVat { get; set; }
        public string TotalVat { get; set; }
        public string TotalAmountIncVat { get; set; }
        public string TotalFine { get; set; }
        public string TotalAll { get; set; }

    }

    //public class ItemShortTaxInvoice
    //{
    //    public string ContractAccount { get; set; }
    //    public string LastDayMemo { get; set; }
    //    public string TaxInvoice { get; set; }
    //    public string PowerUnit { get; set; }
    //    public string AmountExVat { get; set; }
    //    public string Vat { get; set; }
    //    public string AmountIncVat { get; set; }
    //    public string DayFine { get; set; }
    //    public string FineFlakedOut { get; set; }
    //    public string FT { get; set; }
    //}

    public class ItemOfflineSumBanknote
    {
        public string Cash { get; set; }
        public string Cheque { get; set; }
    }



    //public class ItemOfflineModel
    //{
    //    public string RowNo { get; set; }
    //    public string PaymentDate { get; set; }
    //    public string Time { get; set; }
    //    public string Branch { get; set; }
    //    public string CashierNo { get; set; }
    //    public string UserId { get; set; }
    //    public string ContractAccount { get; set; }
    //    public string InvoiceNumber { get; set; }
    //    public string TaxInvoice { get; set; }
    //    public string Amount { get; set; }

    //    public string ServiceType { get; set; }

    //    public string Cash { get; set; }
    //    public string CashChange { get; set; }


    //    public string GainLoss { get; set; } 
    //    public string ContactNumber { get; set; }

    //    /******************************************************/



    //    public string BillNo { get; set; }

    //    public string AmountIncVat { get; set; }

    //    public string Interest { get; set; }

    //    public string DayOfInterest { get; set; }

    //    public string DB_OPS { get; set; }

    //    public string Transact { get; set; }


    //}

    public class ItemOfflineLogToServiceModel
    {
        public string PaymentDate { get; set; }
        public string ServiceType { get; set; }


        public string ContractAccount { get; set; }

        public string InvoiceNumber { get; set; }

        public string BillNo { get; set; }

        public string AmountIncVat { get; set; }

        public string Interest { get; set; }

        public string DayOfInterest { get; set; }

        public string DB_OPS { get; set; }

        public string Transact { get; set; }

    }

    public class ItemUserOfflineModel
    {
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int distId { get; set; }
        public int cashierNo { get; set; }
        public string distName { get; set; }
        public string receiptBranch { get; set; }
    }


    public class ItemSummaryPaymentModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public class ItemBillCancelModel
    {
        public bool Selected { get; set; }
        public string UserCode { get; set; }
        public string CA { get; set; }
        public string InvoiceNumber { get; set; }

        public string BillNumber { get; set; }
        public string Status { get; set; }

        public double AmountIncVat { get; set; }

        public string Remark { get; set; }


        public string ReceiptNumber { get; set; }
    }

    public class ItemDataPaymentModel
    {
        public double SumCash { get; set; }
        public double SumChequeChange { get; set; }
        public double SumCheque { get; set; }
    }
    public class ItemtRevertTaxInvoiceModel
    {
        public string UserId { get; set; }
        public string ContractAccount { get; set; }
        public string InvoiceNumber { get; set; }
        public string TaxInvoice { get; set; }
        public string Amount { get; set; }
        public string Remark { get; set; }
    }



    public class ItemtSummaryChequeModel
    {
        public string UserCode { get; set; }

        public string ChequeNumber { get; set; }

        public string BankCode { get; set; }

        public string BankName { get; set; }
        public string ChequeDate { get; set; }

        public string Amount { get; set; }

        public string ChequeChange { get; set; }

        public string ChequeName { get; set; }
        public string CashierCheque { get; set; }

        public string BillNumber { get; set; }

        //public string ContractAccount { get; set; }
        //public string TaxInvoice { get; set; }


    }

    public class ItemSummaryPaymentModel_Display
    {
        public string UserId { get; set; }
        public string PaymentDate { get; set; }
        public string Branch { get; set; }
        public string CashierNo { get; set; }


        public string SumCheque { get; set; }

        public string SumChequeChange { get; set; }
        public string SumCash { get; set; }

        public string SumGainLoss { get; set; }
        public string SumCredit { get; set; }
        public string SumTotal { get; set; }


    }

    public class SummaryPaymentObj
    {
        public List<ItemSummaryPaymentModel_Display> Display { get; set; }

        public List<ItemSummaryPaymentModel> Report { get; set; }

    }


    /********************************************************************/

    public class ItemOfflineModel
    {

        public bool Selected { get; set; }
        public string RowNo { get; set; }

        public string SendFlag { get; set; }

        public string ReceiptNumber { get; set; }
        public string UserCode { get; set; }

        public string BranchCode { get; set; }

        public double TotalAmountExVat { get; set; }

        public double TotalVat { get; set; }

        public double TotalAmountIncVat { get; set; }

        public double TotalFine { get; set; }

        public decimal TotalAll { get; set; }

        public double Cash { get; set; }

        public double SumCheque { get; set; }

        public double ChequeChange { get; set; }
        public string InvoiceNumber { get; set; }

        public double CashReceive { get; set; }

        public string BillNumber { get; set; }
        public string CA { get; set; }

        public string Tel { get; set; }

        public string Status { get; set; }

        public string CreateDate { get; set; }

        public string CreateTime { get; set; }

        public string UpdateDate { get; set; }


    }

    // public class ItemInvoiceModel
    public class ItemInvoiceModel
    {

        public int Index { get; set; }

        public bool Selected { get; set; }
        public string ReceiptNumber { get; set; }
        public string CA { get; set; }

        public string BillNumber { get; set; }
        public string PaymentDate { get; set; }

        public string Time { get; set; }

        public string TaxID { get; set; }

        public double PowerUnit { get; set; }

        public string DebtCode { get; set; }

        public string InvoiceNumber { get; set; }

        public string DueDate { get; set; }

        public int DayFine { get; set; }

        public double AmountExVat { get; set; }

        public double VatPercent { get; set; }

        public double AmountVat { get; set; }

        public double AmountIncVat { get; set; }

        public string ServiceType { get; set; }

        public double StopElectricityAmount { get; set; }

        public string ConnectPower { get; set; }
        

        public double FineFlakedOut { get; set; } //ค่าปรับ

        public double Total { get; set; } //ค่าไฟบวกค่าปรับ


        public string CodeType { get; set; }

        public string strTax { get; set; }

        public string strRefNo1 { get; set; }

        public string strRefNo2 { get; set; }

        public string strAmount { get; set; }


    }

    public class ItemBank
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
    }

    public class ItemChequeModel
    {
        public string ReceiptNumber { get; set; }
        public string ChequeNumber { get; set; }
        public string ChequeName { get; set; }
        public string ChequeDate { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public double ChequeAmount { get; set; }
        public string CashierCheque { get; set; }
        public string ChequeTel { get; set; }
        public string CreateBy { get; set; }
        public string CreateDate { get; set; }

    }

    public class ItemMapCashModel
    {
        public string InvoiceNumber { get; set; }
        public double Cash { get; set; }
        public string CreateBy { get; set; }
        public string CreateDate { get; set; }
    }

    public class ItemChangeModel
    {
        public string ReceiptNumber { get; set; }

        public double CashChange { get; set; }

        public double ChequeChange { get; set; }


        public double RealCashChange { get; set; }

        public double RealChequeChange { get; set; }

    }

    public class ItemBillModel
    {
        public string BillNumber { get; set; }
        public double BillTotalAmountExVat { get; set; }
        public double BillTotalVat { get; set; }
        public double BillTotalAmountIncVat { get; set; }
        public double BillTotalFine { get; set; }
        public double BillTotalAll { get; set; }
        public string BillType { get; set; }
    }

    public class ItemStopElectricModel
    {
        public string ReceiptNumber { get; set; }
        public string CA { get; set; }

        public string BillNumber { get; set; }
        public double AmountExVat { get; set; }
        public double AmountVat { get; set; }
        public double AmountIncVat { get; set; }
    }
    public class ItemMapChequeModel
    {
        public string ChequeNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public double Amount { get; set; }

        public string ChequeName { get; set; }

        public string ChequeDate { get; set; }

        public string CashierCheque { get; set; }

        public string BankCode { get; set; }



    }
    public class ItemShortTaxInvoice
    {
        public string ReceiptNumber { get; set; }
        public string BillNumber { get; set; }
        public string CA { get; set; }
        public string InvoiceNumber { get; set; }
        public double PowerUnit { get; set; }
        public double AmountExVat { get; set; }
        public double VatPercent { get; set; }
        public double AmountIncVat { get; set; }
        public int DayFine { get; set; }
        public double FineFlakedOut { get; set; }
        public double BillTotalAmountExVat { get; set; }
        public double BillTotalVat { get; set; }
        public double BillTotalAmountIncVat { get; set; }
        public double BillTotalFine { get; set; }
        public double BillTotalAll { get; set; }

    }

    public class ItemIP
    {
        public bool Selected { get; set; }
        public string IP { get; set; }
        public string Port { get; set; }
        public string BranchCode { get; set; }
        public string CashierNo { get; set; }

        public string Status { get; set; }

        public string Date { get; set; }

        public string DateDisplay { get; set; }
        // public string Detail { get; set; }

        public string Data { get; set; }

        public string SendToServerDate { get; set; }
        public string SendToServerDateDisplay { get; set; }

        public bool hasData { get; set; }

    }


    public class ItemDataToService
    {

        public string ReceiptNumber { get; set; }
        public string CA { get; set; }
        public string BillNumber { get; set; }
        public string PaymentDate { get; set; }
        public string Time { get; set; }
        public string TaxID { get; set; }
        public double PowerUnit { get; set; }
        public string DebtCode { get; set; }
        public int DayFine { get; set; }
        public string InvoiceNumber { get; set; }
        public string DueDate { get; set; }
        public double AmountExVat { get; set; }
        public double VatPercent { get; set; }
        public double AmountVat { get; set; }
        public double AmountIncVat { get; set; }
        public string ServiceType { get; set; }
        public double StopElectricityAmount { get; set; }
        public double FineFlakedOut { get; set; } //ค่าปรับ


        public string ConnectPower { get; set; }
        

        /******************************************/
        public double TotalAmountExVat { get; set; }
        public double TotalAmountIncVat { get; set; }

        public double TotalVat { get; set; }

        public double TotalAll { get; set; }
        public double CashChange { get; set; }

        public double ChequeChange { get; set; }

        public double RealCashChange { get; set; }

        public double RealChequeChange { get; set; }

        public double CashReceive { get; set; }

        public string CodeType { get; set; }
        public string strTax { get; set; }
        public string strRefNo1 { get; set; }
        public string strRefNo2 { get; set; }
        public string strAmount { get; set; }




    }

    public class ItemSendData
    {
        public string IP { get; set; }
        public string CreateDate { get; set; }
        public string Data { get; set; }
        public string SendToServerDate { get; set; }
        public string Status { get; set; }
        public string Branch { get; set; }
        public string CashierDesk { get; set; }
    }

    public class ItemDataToServer
    {
        public List<ItemOfflineModel> LstOffline { get; set; }
    }





    /*******************************************************/
    public class paymentListOFF : paymentList
    {
        public string paymentServer { get; set; }
    }

    public class paymentDetailListOFF : paymentDetailList
    {
        public string taxField { get; set; }
        public string connectPower { get; set; }


        public string tax { get; set; }

        public string barTax { get; set; }

        public string barRefNo1 { get; set; }

        public string barRefNo2 { get; set; }

        public string barAmount { get; set; }



        public string qrTax { get; set; }

        public string qrRefNo1 { get; set; }

        public string qrRefNo2 { get; set; }

        public string qrAmount { get; set; }

    }

    public class MyPaymentListOFF
    {
        public List<paymentListOFF> paymentList { get; set; }
    }
  
        


}



