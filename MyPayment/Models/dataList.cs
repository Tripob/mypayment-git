﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class dataList
    {
        public string processSet { get; set; }
        public string distId { get; set; }
        public string refInv { get; set; }
        public string itemCode { get; set; }
        public double? priceIncVat { get; set; }
        public double? itemQty { get; set; }
        public double? totalAmount { get; set; }
        public double? amountExcVat { get; set; }
        public double? amountVat { get; set; }
        public int? percentVat { get; set; }
        public Int64? userId { get; set; }
    }
}
