﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class MyPaymentList
    {
        public List<paymentList> paymentList { get; set; }
    }
    public class MyPaymentListOTH
    {
        public List<paymentListOTH> paymentList { get; set; }
    }
}
