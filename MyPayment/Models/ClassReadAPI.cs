﻿using MyPayment.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;

namespace MyPayment.Models
{
    class ClassReadAPI
    {
        static Random rnd = new Random();
        public enum StatusLoadData
        {
            Loging = 1,
            LoginAddmin = 2,
            changePassword = 3,
            LoadData = 4,
            Logout = 5
        }
        public static ClassUser ReadToObject(string json)
        {
            ClassUser deserializedMember = new ClassUser();
            deserializedMember = new JavaScriptSerializer().Deserialize<ClassUser>(json);
            return deserializedMember;
        }
        public static DataTable GetDataTableFromJsonString(string json)
        {
            var jsonLinq = JObject.Parse(json);
            var srcArray = jsonLinq.Descendants().Where(d => d is JArray).First();
            var trgArray = new JArray();
            foreach (JObject row in srcArray.Children<JObject>())
            {
                var cleanRow = new JObject();
                foreach (JProperty column in row.Properties())
                {
                    if (column.Value is JValue)
                        cleanRow.Add(column.Name, column.Value);
                }
                trgArray.Add(cleanRow);
            }
            return JsonConvert.DeserializeObject<DataTable>(trgArray.ToString());
        }
        public static IList<inquiryGroupDebtBeanList> ReadToObjectlistCostTDebt(string json)
        {
            IList<inquiryGroupDebtBeanList> searchResults = new List<inquiryGroupDebtBeanList>();
            ClassUser deserializedMember = new ClassUser();
            deserializedMember = new JavaScriptSerializer().Deserialize<ClassUser>(json);
            if (deserializedMember.result_code == "SUCCESS")
            {
                JObject objSearch = JObject.Parse(json);
                IList<JToken> results = objSearch["result"]["listCostTDebt"].Children().ToList();
                foreach (JToken result in results)
                {
                    inquiryGroupDebtBeanList searchResult = JsonConvert.DeserializeObject<inquiryGroupDebtBeanList>(result.ToString());
                    searchResults.Add(searchResult);
                }
                return searchResults;
            }
            else
            {
                inquiryGroupDebtBeanList results = new inquiryGroupDebtBeanList();
                results.ResultMessage = deserializedMember.result_message;
                searchResults.Add(results);
                return searchResults;
            }
        }
        public static ResultInquiryDebt ReadToObjectInquiry(string json)
        {
            ResultInquiryDebt searchResults = new ResultInquiryDebt();
            ClassInquiryDebt Inquiry = JsonConvert.DeserializeObject<ClassInquiryDebt>(json);
            if (Inquiry.result_code == "SUCCESS")
            {
                if (Inquiry.inquiryBean.inquiryInfoBeanList != null)
                    searchResults.OutputInquiryDebt = Inquiry;
            }
            else
            {
                if (Inquiry.result_code != "EC005")
                    searchResults.OutputInquiryDebt = Inquiry;
                searchResults.ResultMessage = Inquiry.result_message;
                searchResults.ResultCode = Inquiry.result_code;
            }
            return searchResults;
        }
        public static ResultPayInBalance ReadToObjectPayIn(string json)
        {
            ResultPayInBalance searchResults = new ResultPayInBalance();
            ClassDisplayPayIn Display = JsonConvert.DeserializeObject<ClassDisplayPayIn>(json);
            if (Display.result_code == "SUCCESS")
            {
                if (Display.payInResponseDto != null)
                    searchResults.OutputDisplay = Display;
                searchResults.ResultMessage = Display.result_message;
            }
            else
                searchResults.ResultMessage = Display.result_message;
            return searchResults;
        }
        public static ResultRemittance ReadToObjectProcessPayIn(string json)
        {
            ResultRemittance searchResults = new ResultRemittance();
            ClassProcessDisplayPayIn Display = JsonConvert.DeserializeObject<ClassProcessDisplayPayIn>(json);
            if (Display.result_code == "SUCCESS")
            {
                if (Display.payInDisplayResponseDtoList != null)
                    searchResults.OutputDisplay = Display;
                searchResults.ResultMessage = Display.result_message;
            }
            else
                searchResults.ResultMessage = Display.result_message;
            return searchResults;
        }
        public static ResultProcessCancelPayIn ReadToObjectProcessCancelPayIn(string json)
        {
            ResultProcessCancelPayIn searchResults = new ResultProcessCancelPayIn();
            ClassProcessCancelPayIn Display = JsonConvert.DeserializeObject<ClassProcessCancelPayIn>(json);
            if (Display.result_code == "SUCCESS")
            {
                if (Display.closingDrawerIdList != null)
                    searchResults.Output = Display;
                searchResults.ResultMessage = Display.result_message;
            }
            else
                searchResults.ResultMessage = Display.result_message;
            return searchResults;
        }
        public static string GetDataAPINew(string url, string result)
        {
            try
            {
                string json = "";
                string http = GlobalClass.Url + url + "?rnd=" + rnd.Next();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(http);
                request.ContentType = "application/json";
                request.Method = "POST";
                request.ContentLength = 0;
                request.AllowAutoRedirect = false;
                request.KeepAlive = false;
                request.Timeout = 3000000;
                request.ReadWriteTimeout = 3000000;
                request.Accept = "application/json";
                request.ProtocolVersion = HttpVersion.Version11;
                request.Headers.Add("Accept-Language", "de_DE");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                byte[] byteArray = Encoding.UTF8.GetBytes(result);
                request.ContentLength = byteArray.Length;
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                    writer.Flush();
                    writer.Close();
                }
                using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponseAsync().GetAwaiter().GetResult())
                {
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        json = streamReader.ReadToEndAsync().GetAwaiter().GetResult();
                        //streamReader.Close();
                        //httpResponse.Close();
                        return json;
                    }
                }
            }
            catch (WebException ex)
            {
                using (HttpWebResponse httpResponse = (HttpWebResponse)ex.Response)
                {
                    var httpRes = (HttpWebResponse)ex.Response;
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static string PostDataAPI(string url, string result, double? timer = 5)
        {
            try
            {
                string uri;
                uri = GlobalClass.Url + url;
                HttpClient client = new HttpClient();
                var content = new StringContent(result, Encoding.UTF8, "application/json");
                client.Timeout = TimeSpan.FromMinutes(timer.Value);
                client.BaseAddress = new Uri(uri);
                client.DefaultRequestHeaders.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue
                {
                    NoCache = true
                };
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                // List data response.
                HttpResponseMessage response = client.PostAsync(new Uri(uri), content).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    var dataObjects = response.Content.ReadAsStringAsync().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                    client.Dispose();
                    return dataObjects;
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                    //log.Error(response.ReasonPhrase);
                    client.Dispose();
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetDataAPI(string url, double? timer = 10000)
        {
            try
            {
                string uri;
                uri = GlobalClass.Url + url + rnd.Next();
                HttpClient client = new HttpClient();
                client.Timeout = TimeSpan.FromSeconds(timer.Value);
                client.BaseAddress = new Uri(uri);
                client.DefaultRequestHeaders.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue
                {
                    NoCache = true
                };
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync(new Uri(uri)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var dataObjects = response.Content.ReadAsStringAsync().Result;
                    client.Dispose();
                    return dataObjects;
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                    client.Dispose();
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }   
        public static ResultDisplayPaytment ReadToObjectDisplayPaytment(string json)
        {
            ResultDisplayPaytment searchResults = new ResultDisplayPaytment();
            ClassDisplayCancelReceipt Display = JsonConvert.DeserializeObject<ClassDisplayCancelReceipt>(json);
            if (Display.result_code == "SUCCESS")
            {
                if (Display.displayCancelReceiptResponseDtoList != null)
                    searchResults.OutputDisplay = Display;
            }
            else
                searchResults.ResultMessage = Display.result_message;
            return searchResults;
        }
        public static ResultPayment ReadToObjectDisplayRemittance(string json)
        {
            ResultPayment searchResults = new ResultPayment();
            ResultPayment Display = new ResultPayment();
            Display = new JavaScriptSerializer().Deserialize<ResultPayment>(json);
            if (Display.result_code == "SUCCESS")
            {
                searchResults.Output = new List<paymentDisplayTransctionResponseDtoList>();
                JObject objSearch = JObject.Parse(json);
                IList<JToken> results = objSearch["paymentDisplayTransctionResponseDtoList"].Children().ToList();
                foreach (JToken result in results)
                {
                    paymentDisplayTransctionResponseDtoList searchResult = JsonConvert.DeserializeObject<paymentDisplayTransctionResponseDtoList>(result.ToString());
                    searchResults.Output.Add(searchResult);
                }
            }
            return searchResults;
        }
    }
}
