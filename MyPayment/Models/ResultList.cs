﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ResultList
    {
        public string ResultMessage { get; set; }
        public string ResultCode { get; set; }
        public string PayeeFlag { get; set; }
        public Int64[] closingDrawerIdList { get; set; }
    }
    public class ResultInquiryDebt : ResultList
    {
        public List<inquiryInfoBeanList> Output { get; set; }
        public List<listSummaryCostTDebt> OutputSumlist { get; set; }
        public List<inquiryGroupDebtBeanList> OutputTDebt { get; set; }
        public ClassInquiryDebt OutputInquiryDebt { get; set; }
        public List<ClassReturnDIS> OutputDIS { get; set; }
    }
    public class ResultCostTDebt : ResultList
    {
        public List<inquiryGroupDebtBeanList> Output { get; set; }
    }
    public class ResultDisplayPayIn : ResultList
    {
        public List<ClassProcessDisplayPayIn> Output { get; set; }
    }
    public class ResultRemittance : ResultList
    {
        public List<payInDisplayResponseDtoList> Output { get; set; }
        public ClassProcessDisplayPayIn OutputDisplay { get; set; }
    }
    public class ResultProcessCancelPayIn : ResultList
    {
        public ClassProcessCancelPayIn Output { get; set; }
    }    
    public class ResultRemittanceSlip : ResultList
    {
        public List<ClassRemittanceSlip> Output { get; set; }
    }
    public class ResultExpenseItem : ResultList
    {
        public List<listExpenseItem> Output { get; set; }
        public List<listProfitCenter> OutputPro { get; set; }
        public List<costUnit> OutputUnit { get; set; }
    }
    public class ResultDisplayPaytment : ResultList
    {
        public List<displayCancelReceiptResponseDtoList> Output { get; set; }
        public ClassDisplayCancelReceipt OutputDisplay { get; set; }
        public string OutMessage { get; set; }
    }
    public class ResultCancel : ResultList
    {
        public List<ClassCancelContinue> Cancel { get; set; }
    }
    public class ResultCancelElectricity : ResultList
    {
        public List<ClassElectricityPenalty> Cancel { get; set; }
    }
    public class ResultClassCancelProcessing : ResultList
    {
        public List<ClassCancelProcessingFee> Cancel { get; set; }
    }
    public class ResultPayment : ResultList
    {
        public string result_code { get; set; }
        public List<paymentDisplayTransctionResponseDtoList> Output { get; set; }
    }
    public class ResultProcessPayment : ResultList
    {
        public string result_code { get; set; }
        public List<paymentResponseDtoList> Output { get; set; }
        public ClassOutputProcessPayment OutputProcess { get; set; }
    }
    public class ResultPayInBalance : ResultList
    {
        public string result_code { get; set; }
        public List<payInChequeResponseDtoList> OutputCheque { get; set; }
        public List<payInResponseDto> OutputResponseDto { get; set; }
        public List<payInCardResponseDtoList> OutputCredit { get; set; }
        public ClassDisplayPayIn OutputDisplay { get; set; }
    }
}
