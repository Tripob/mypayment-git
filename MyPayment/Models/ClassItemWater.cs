﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClassItemWater
    {
        public string responseMessage { get; set; }
        public string responseCode { get; set; }
        public string mwaTransRef { get; set; }
        public string taxId { get; set; }
        public string invoiceType { get; set; }
        public string mwaThAddress1 { get; set; }
        public string paidAddress { get; set; }
        public string mwaThAddress2 { get; set; }
        public string customerName { get; set; }
        public string totalInvoice { get; set; }
        public string customerCode { get; set; }
        public string taxBranch { get; set; }
        public string branchCode { get; set; }
        public string mwaTaxId { get; set; }
        public string agentTransRef { get; set; }
        public string totalNetAmount { get; set; }
        public string vatType { get; set; }
        public string billDate { get; set; }
        public string billDueDate { get; set; }
        public string meaUnit { get; set; }
        public string billNumber { get; set; }
        public string mwaThName { get; set; }
        public string mwaTaxBranch { get; set; }
        public string customerAddress { get; set; }
        public string paidName { get; set; }
        public string receiveType { get; set; }
        public decimal totalAmount { get; set; }
        public decimal VatAmount { get; set; }
        public string Amount { get; set; }
        public Int64[] debtIdSet { get; set; }
        public string check { get; set; }
        public string dueDate { get; set; }
    }
}
