﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassPaymentCheque
    {
        public decimal Amount { get; set; }
        public string ChequeNo { get; set; }
        public string CardNo { get; set; }
        public decimal AmountBalance { get; set; }
        public string Ca { get; set; }
        public string DebtId { get; set; }
        public decimal Payed { get; set; }
        public decimal AmountOld { get; set; }
        public string ChequeNoOld { get; set; }
        public int MethodId { get; set; }
        public bool Status { get; set; }
        public bool ChStatus { get; set; }
        public bool StatusCheque { get; set; }
        public bool StatusMapCheque { get; set; }
    }
}
