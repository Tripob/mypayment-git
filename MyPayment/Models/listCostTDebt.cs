﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class inquiryGroupDebtBeanList
    {
        public string ResultMessage { get; set; }
        public Int64[] debtIdSet { get; set; }
        public string debtType { get; set; }
        public string distIdNew { get; set; }
        public string distId { get; set; }
        public string reqId { get; set; }
        public string reqNo { get; set; }
        public string invNo { get; set; }
        public string custId { get; set; }
        public string ca { get; set; }
        public string ui { get; set; }
        public string debtDate { get; set; }
        public string quaranteeNo { get; set; }
        public string debtInsHdrId { get; set; }
        public string dueDate { get; set; }
        public decimal amount { get; set; }
        public decimal vat { get; set; }
        public string percentVat { get; set; }
        public string totalAmount { get; set; }
        public string totalInt { get; set; }
        public string totalIntDay { get; set; }
        public decimal debtBalance { get; set; }
        public string vatBalance { get; set; }
        public string debitNoteFlag { get; set; }
        public string debtStatus { get; set; }
        public string paymentStatus { get; set; }
        public string paymentDate { get; set; }
        public string installmentsFlag { get; set; }
        public string dunningProcedure { get; set; }
        public string directDebitFlag { get; set; }
        public string dunningStatus { get; set; }
        public string directDebitDate { get; set; }
        public bool StatusLockDirectDebit { get; set; }
        public string dunningNo { get; set; }
        public string postponeFlag { get; set; }
        public string createdProcess { get; set; }
        public string postponeId { get; set; }
        public string invNewDueDate { get; set; }
        public string intLockFlag { get; set; }
        public string intLockDate { get; set; }
        public string tarifId { get; set; }
        public string custType { get; set; }
        public string debtDesc { get; set; }
        public string ft { get; set; }
        public string totalkWh { get; set; }
        public DateTime? schdate { get; set; }
        public string intFalg { get; set; }
        public string intRate { get; set; }
        public decimal Payment { get; set; }
        public int? countDay { get; set; }
        public decimal amountNew { get; set; }
        public decimal Defaultpenalty { get; set; }
        public decimal TotalAmountOld { get; set; }
        public decimal EleTotalVatOld { get; set; }
        public decimal EleTotalAmountOld { get; set; }
        public string lockBill { get; set; }
        public string ftRate { get; set; }
        public string receiptDetailDesc { get; set; }
        public bool SelectCheck { get; set; }
        public string exceptIntBy { get; set; }
        public string reqDesc { get; set; }
        public string uiTypeDesc { get; set; }
        public string subCodeName { get; set; }
        public string debtInsIntFlag { get; set; }
        public string kwh { get; set; }
        public string orgInvoice { get; set; }
        public string orgAmount { get; set; }
        public string payeeFlag { get; set; }
        public bool status { get; set; }
        public int EleInvCount { get; set; }
        public decimal EleTotalAmount { get; set; }
        public decimal OtherTotalAmount { get; set; }
        public decimal VatEleTotalAmount { get; set; }
        public decimal VatOtherTotalAmount { get; set; }
        public string lockCheq { get; set; }
        public decimal Payed { get; set; }
        public decimal PayedNew { get; set; }
        public string ChequeNo { get; set; }
        public decimal AmountCheque { get; set; }
        public bool StatusCheq { get; set; }
        public bool StatusSelectCheq { get; set; }
        public string AmountBalance { get; set; }
        public string No { get; set; }
        public string paymentId { get; set; }
        public string paymentNo { get; set; }
        public bool StatusOrderBy { get; set; }
        public string[] guaNo { get; set; }
        public int statusAmount { get; set; }
        public bool statusType { get; set; }
        public string cusName { get; set; }
        public bool StatusDefaultpenalty { get; set; }
        public string VatNew { get; set; }
        public int countDayNew { get; set; }
        public decimal DefaultpenaltyNew { get; set; }
        public bool statusCancel { get; set; }
        public string unit { get; set; }
        public decimal price { get; set; }
        public int itemId { get; set; }
        public string invHdrNo { get; set; }
        public decimal totalPayment { get; set; }
        public decimal? calTotalInt { get; set; }
        public int? calTotalIntDay { get; set; }
        public decimal? interest { get; set; }
        public string debtId { get; set; }
        public string insDebtInsHdrId { get; set; }
        public string infoCa { get; set; }
        public string previousDueDate { get; set; }
        public string debtLock { get; set; }
        public string debtRemark { get; set; }      
        #region ค่าน้ำ
        public string mwaTaxId { get; set; }
        public string customerCode { get; set; }
        public string billDueDate { get; set; }
        public string billNumber { get; set; }
        public string check { get; set; }
        #endregion
        public string accountCode { get; set; }
        public string unitCode { get; set; }
        public string itemDesc1 { get; set; }
        public string itemDesc2 { get; set; }
        public string itemDesc3 { get; set; }
        public string businessTypeId { get; set; }
        public string profitCenter { get; set; }
    }
}
