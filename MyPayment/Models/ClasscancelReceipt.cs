﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClasscancelReceipt
    {
        public string[] receiptNoList { get; set; }
        public int userId { get; set; }
        public string remark { get; set; }
        public int approveEmpId { get; set; }
        public string paymentId { get; set; }
    }
}
