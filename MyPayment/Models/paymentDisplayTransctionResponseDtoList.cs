﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class paymentDisplayTransctionResponseDtoList
    {
        public string paymentNo { get; set; }
        public string receiptNo { get; set; }
        public string paymentChannelDesc { get; set; }
        public string distShortDesc { get; set; }
        public string paymentTypeDesc { get; set; } 
        public string paymentDateStr { get; set; }
        public string debtName { get; set; }
        public string receiptDtlName { get; set; }
        public decimal receiptDtlAmount { get; set; }
        public decimal receiptDtlVat { get; set; }
        public decimal receiptDtlTotalAmount { get; set; }
        public string cashierName { get; set; }
        public string cashierNo { get; set; }       
        public string strDate { get; set; }
        public decimal totalAmount { get; set; }
    }
}
