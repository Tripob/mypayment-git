﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class payTReceiptList
    {
        public int? receiptId { get; set; }
        public string receiptNo { get; set; }
        public int? receiptTypeId { get; set; }
        public int? receiptDebtType { get; set; }
        public string receiptDate { get; set; }
        public string receiptRefNo { get; set; }
        public string receiptPayer { get; set; }
        public string receiptPayerAddress { get; set; }
        public string receiptPayerTaxid { get; set; }
        public string receiptPayerBranch { get; set; }
        public int? distId { get; set; }
        public string distName { get; set; }
        public string receiptCustId { get; set; }
        public string receiptCustName { get; set; }
        public string receiptCustTaxid { get; set; }
        public string receiptCustBranch { get; set; }
        public string receiptReqAddress { get; set; }
        public int? receiptCa { get; set; }
        public int? receiptUi { get; set; }
        public decimal? receiptAmount { get; set; }
        public decimal? receiptVat { get; set; }
        public decimal? receiptTotalAmount { get; set; }
        public decimal? receiptIntAmount { get; set; }
        public double? receiptIntTotalAmount { get; set; }
        public string cashierNo { get; set; }
        public string cashierId { get; set; }
        public string cashierName { get; set; }
        public int? receiptPercentVat { get; set; }
        public int? subDistId { get; set; }
        public string countCa { get; set; }
        public string collId { get; set; }
        public string countInv { get; set; }
        public List<responseReceiptDtlBeanList> responseReceiptDtlBeanList { get; set; }
        public string debtType { get; set; }
        public int paymentTypeId { get; set; }
        public string mwaAccountNo { get; set; }
        public string cardNo { get; set; }
        public string installmentsFlag { get; set; }
        public string partialPayment { get; set; }
    }
}
