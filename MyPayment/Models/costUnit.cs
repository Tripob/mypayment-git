﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class costUnit
    {
        public int unitId { get; set; }
        public string unitCode { get; set; }
        public string unitDetail { get; set; }
    }
}
