﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassPayIn
    {
        public int distId { get; set; }
        public int subDistId { get; set; }       
        public Int64 cashierId { get; set; }
        public string cashierNo { get; set; }
        public int bankNote1000 { get; set; }
        public int bankNote500 { get; set; }
        public int bankNote100 { get; set; }
        public int bankNote50 { get; set; }
        public int bankNote20 { get; set; }
        public int coin10 { get; set; }
        public int coin5 { get; set; }
        public int coin2 { get; set; }
        public int coin1 { get; set; }
        public int coin050 { get; set; }
        public int coin025 { get; set; }
        public decimal totalCash { get; set; }
        public decimal totalCheque { get; set; }
        public decimal totalCard { get; set; }
        public int chequeQty { get; set; }
        public int cardQty { get; set; }
        public List<payInChequeResponseDtoList> payInChequeResponseDtoList { get; set; }
        public List<payInCardResponseDtoList> payInCardResponseDtoList { get; set; }
    }
}
