﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassModelsReportDetail
    {
        public string invNo { get; set; }
        public decimal amount { get; set; }
        public string vatBalance { get; set; }
        public string debtBalance { get; set; }
        public string schdate { get; set; }
        public string totalkWh { get; set; }
        public int countDay { get; set; }
        public decimal Defaultpenalty { get; set; }
        public string ftRate { get; set; }
        public string DetailDesc { get; set; }
        public string Branch { get; set; }
        public decimal Price { get; set; }
        public string RefReceiptId { get; set; }
        public string RefReceiptNo { get; set; }
        public decimal RefReceiptAmount { get; set; }
        public decimal RefOldReceiptAmount { get; set; }
        public decimal RefReceiptSumAmount { get; set; }
        public decimal RefReceiptVat { get; set; }
        public decimal RefReceiptTotalAmount { get; set; }
        public string RefRemark { get; set; }
    }
}
