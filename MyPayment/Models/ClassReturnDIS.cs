﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClassReturnDIS
    {
        public string cusName { get; set; }
        public string cusId { get; set; }
        public string ca { get; set; }
        public string ui { get; set; }
        public string debtId { get; set; }
    }
}
