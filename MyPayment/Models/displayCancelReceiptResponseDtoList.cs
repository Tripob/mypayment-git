﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class displayCancelReceiptResponseDtoList
    {
        public string paymentId { get; set; }
        public string paymentNo { get; set; }
        public string receiptNo { get; set; }
        public string payerName { get; set; }
        public string custId { get; set; }
        public string ca { get; set; }
        public string ui { get; set; }
        public string totalReceipt { get; set; }        
    }
}
