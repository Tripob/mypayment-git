﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class payInCardResponseDtoList
    {
        public string No { get; set; }
        public Int64 cardId { get; set; }
        public string cardNo { get; set; }
        public string cardOwner { get; set; }     
        public decimal cardAmount { get; set; }      
    }
}
