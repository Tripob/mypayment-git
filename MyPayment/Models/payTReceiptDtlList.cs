﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class responseReceiptDtlBeanList
    {
        public string receiptDtlNo { get; set; }
        public string receiptDtlName { get; set; }
        public decimal receiptDtlAmount { get; set; }
        public decimal? receiptDtlVat { get; set; }
        public decimal receiptDtlTotalAmount { get; set; }
        public string lastBillingDate { get; set; }
        public int? receiptDtlUnit { get; set; }
        public decimal intAmount { get; set; }
        public int? intDay { get; set; }
        public string ft { get; set; }
        public string mwaInvoiceNo { get; set; }
        public string mwaBranch { get; set; }
        public decimal? itemPriceWithVat { get; set; }
        public string receiptId { get; set; }
        public string refReceiptId { get; set; }
        public string refReceiptNo { get; set; }
        public decimal? refReceiptAmount { get; set; }
        public decimal? refOldReceiptAmount { get; set; }
        public decimal? refReceiptSumAmount { get; set; }
        public decimal? refReceiptVat { get; set; }
        public decimal? refReceiptTotalAmount { get; set; }
        public string refRemark { get; set; }
    }
}
