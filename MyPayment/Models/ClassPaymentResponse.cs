﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassPaymentResponse
    {
        public string PaymentId { get; set; }
        public string ReceiptNo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string CA { get; set; }
        public string UI { get; set; }
        public decimal Amount { get; set; }
        public string PaymentNo { get; set; }
    }
}
