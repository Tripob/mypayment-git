﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClassInformationReport
    {
        public string strDate { get; set; }
        public string debtType { get; set; }
        public string distId { get; set; }
        public string debtDate { get; set; }
        public decimal amount { get; set; }
        public decimal vat { get; set; }
        public int percentVat { get; set; }
        public string debtDesc { get; set; }
        public decimal totalAmount { get; set; }        
    }
}
