﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class payInDisplayResponseDtoList
    {
        public int orderNo { get; set; } 
        public int closingDrawerId { get; set; }
        public Int64 cashierId { get; set; }
        public int cashierNo { get; set; }
        public string createdDtStr { get; set; }
        public decimal totalCash { get; set; }
        public int totalCheque { get; set; }
        public decimal totalChequeAmount { get; set; }
        public int totalCreditCard { get; set; }
        public decimal totalCreditCardAmount { get; set; }
        public decimal totalSendAmount { get; set; }
        public decimal totalPaymentAmount { get; set; }
        public decimal diffAmount { get; set; }
        public decimal totalSendAmountDay { get; set; }
        public decimal previouseSendAmount { get; set; }
        public string closingDrawerStatus { get; set; }
        public int unitB1000 { get; set; }
        public int unitB500 { get; set; }
        public int unitB100 { get; set; }
        public int unitB50 { get; set; }
        public int unitB20 { get; set; }
        public decimal amountB1000 { get; set; }
        public decimal amountB500 { get; set; }
        public decimal amountB100 { get; set; }
        public decimal amountB50 { get; set; }
        public decimal amountB20 { get; set; }
        public decimal totalCoin { get; set; }		
        public bool status { get; set; }
        public string userName { get; set; }
        public string distName { get; set; }
        public string strDate { get; set; }
        public decimal totalBankNote { get; set; }
        public decimal sumTotalCash { get; set; }
        public decimal sumTotalCheque { get; set; }
        public decimal sumTotalCard { get; set; }
        public decimal sumTotalAmount { get; set; }
    }
}
