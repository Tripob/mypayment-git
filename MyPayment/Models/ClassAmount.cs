﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassAmount
    {
        public decimal Amount { get; set; }
        public decimal VatAmount { get; set; }
        public decimal Total { get; set; }
        public decimal PenaltyCharge { get; set; }
        public decimal TotalAmount { get; set; }
        public string str1 { get; set; }
        public string str2 { get; set; }
        public string str3 { get; set; }
        public string str4 { get; set; }
    }
}
