﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class payInChequeResponseDtoList
    {
        public string No { get; set; }
        public Int64 chequeId { get; set; }
        public string chequeNo { get; set; }
        public string bankDesc { get; set; }
        public string chequeOwner { get; set; }
        public string chequeDate { get; set; }
        public string telNo { get; set; }
        public decimal chequeAmount { get; set; }       
    }
}
