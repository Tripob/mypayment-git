﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   static class GlobalClass
    {
        private static bool _status;
        private static int _userId;
        private static Int64 _userIdLogin;
        private static string _userName;
        private static int _authorizeLevel;
        private static string _dist;
        private static int _distId;
        private static string _no;
        private static string _password;
        private static bool _isConnectOnline;
        private static string _url;
        private static string strJson;
        private static List<ClassModelsReport> _lst;
        private static List<inquiryGroupDebtBeanList> _lstCost;
        private static List<inquiryInfoBeanList> _lstInfo;
        private static List<ClassCheque> _lstCheque;
        private static List<ClassPaymentCheque> _lstSub;
        private static List<ClassPaymentResponse> _payMent;
        private static List<ClassCancelContinue> _Cancel;
        private static List<ClassConnectMeter> _ConnectMeter;
        private static List<ClassElectricityPenalty> _CancelElectricity;
        private static List<ClassCancelProcessingFee> _cancelProcessing;
        private static List<ClassOther> _ClassOther;
        private static ClassOthDetail _ClassOthDatail;
        private static List<ClassDataGridView> _clsDataGridView;
        private static string _receiptDistName;
        private static double _fine;
        private static double _vat;
        private static double _total;
        private static double _payAmount;
        private static string _ca;
        private static int _eleInvCount;
        private static double _otherTotal;
        private static string _receiptNo;
        private static string _receiptBranch;
        private static int _subDistId;
        private static int _userIdCancel;    
        private static string _remark;
        private static string _userCode;
        private static int? _statusForm;
        private static string _ipConfig;
        private static int? _statusFormMoney;
        private static string _portQ;
        private static string _ipServerQ;
        private static int? _statusAdvance;
        private static double _timer;
        #region ReportOffLine
        private static string _reportName;
        private static string _branch;
        private static string _branchId;
        private static string _branchName;
        private static string _cashierNo;
        private static string _reportOfflineType;
        private static List<ItemOfflineModel> _lstOfflineModel;
        private static List<ItemSummaryPaymentModel> _lstSummaryPaymentModel;
        private static List<ItemtSummaryChequeModel> _lstSummaryChequeModel;
        private static List<ItemBillCancelModel> _lstRevertTaxInvoiceModel;
        private static List<ItemInvoiceModel> _lstShortTaxInviceSelectedModel;
        private static List<ItemStopElectricModel> _lstStopElectricityModel;
        private static List<ItemInvoiceModel> _lstCASelectedModel;
        private static List<ItemBillModel> _lstBillModel;
        private static string _totalAmount;
        private static string _totalContractAccount;
        private static string _totalTaxInvoice;
        private static int _billIndex;
        private static ItemOfflineModel _offlineModel;
        private static string _receiptNumber;
        private static int _CanceluserId;
        #endregion ReportOffLine
        
        public static int BillIndex { get => _billIndex; set => _billIndex = value; }
        public static int UserId { get => _userId; set => _userId = value; }
        public static string UserName { get => _userName; set => _userName = value; }
        public static int AuthorizeLevel { get => _authorizeLevel; set => _authorizeLevel = value; }
        public static string Dist { get => _dist; set => _dist = value; }
        public static string No { get => _no; set => _no = value; }
        public static string Password { get => _password; set => _password = value; }
        public static bool IsConnectOnline { get => _isConnectOnline; set => _isConnectOnline = value; }
        public static string Url { get => _url; set => _url = value; }
        public static string StrJson { get => strJson; set => strJson = value; }
        public static string DistName { get => _receiptDistName; set => _receiptDistName = value; }
        public static List<inquiryGroupDebtBeanList> LstCost { get => _lstCost; set => _lstCost = value; }
        public static double Fine { get => _fine; set => _fine = value; }
        public static double Vat { get => _vat; set => _vat = value; }
        public static double Total { get => _total; set => _total = value; }
        public static double PayAmount { get => _payAmount; set => _payAmount = value; }
        public static string Ca { get => _ca; set => _ca = value; }
        public static List<inquiryInfoBeanList> LstInfo { get => _lstInfo; set => _lstInfo = value; }
        public static int EleInvCount { get => _eleInvCount; set => _eleInvCount = value; }
        public static double OtherTotal { get => _otherTotal; set => _otherTotal = value; }
        public static string ReceiptNo { get => _receiptNo; set => _receiptNo = value; }
        public static string ReceiptBranch { get => _receiptBranch; set => _receiptBranch = value; }
        public static int DistId { get => _distId; set => _distId = value; }
        public static List<ClassPaymentCheque> LstSub { get => _lstSub; set => _lstSub = value; }
        public static int SubDistId { get => _subDistId; set => _subDistId = value; }
        public static int UserIdCancel { get => _userIdCancel; set => _userIdCancel = value; }
        public static List<ClassPaymentResponse> PayMent { get => _payMent; set => _payMent = value; }
        public static string Remark { get => _remark; set => _remark = value; }
        public static string UserCode { get => _userCode; set => _userCode = value; }
        internal static List<ClassModelsReport> List { get => _lst; set => _lst = value; }
        internal static List<ClassCheque> LstCheque { get => _lstCheque; set => _lstCheque = value; }
        public static List<ClassCancelContinue> Cancel { get => _Cancel; set => _Cancel = value; }
        public static List<ClassElectricityPenalty> CancelElectricity { get => _CancelElectricity; set => _CancelElectricity = value; }
        public static int? StatusForm { get => _statusForm; set => _statusForm = value; }
        public static List<ClassCancelProcessingFee> CancelProcessing { get => _cancelProcessing; set => _cancelProcessing = value; }
        public static int? StatusFormMoney { get => _statusFormMoney; set => _statusFormMoney = value; }
        public static List<ClassOther> ClassOther { get => _ClassOther; set => _ClassOther = value; }
        public static ClassOthDetail ClassOthDatail { get => _ClassOthDatail; set => _ClassOthDatail = value; }
        public static string PortQ { get => _portQ; set => _portQ = value; }
        public static string IpServerQ { get => _ipServerQ; set => _ipServerQ = value; }
        public static int? StatusAdvance { get => _statusAdvance; set => _statusAdvance = value; }
        public static List<ClassDataGridView> ClsDataGridView { get => _clsDataGridView; set => _clsDataGridView = value; }
        public static List<ClassConnectMeter> ConnectMeter { get => _ConnectMeter; set => _ConnectMeter = value; }
        public static double Timer { get => _timer; set => _timer = value; }
        #region ReportOffLine
        public static string ReceiptNumber { get => _receiptNumber; set => _receiptNumber = value; }
        public static List<ItemOfflineModel> LstOfflineModel { get => _lstOfflineModel; set => _lstOfflineModel = value; }
        public static List<ItemSummaryPaymentModel> LstSummaryPaymentModel { get => _lstSummaryPaymentModel; set => _lstSummaryPaymentModel = value; }
        public static List<ItemtSummaryChequeModel> LstSummaryChequeModel { get => _lstSummaryChequeModel; set => _lstSummaryChequeModel = value; }
        public static List<ItemBillCancelModel> LstRevertTaxInvoiceModel { get => _lstRevertTaxInvoiceModel; set => _lstRevertTaxInvoiceModel = value; }
        public static List<ItemInvoiceModel> LstShortTaxInviceSelectedModel { get => _lstShortTaxInviceSelectedModel; set => _lstShortTaxInviceSelectedModel = value; }
        public static List<ItemBillModel> LstBillModel { get => _lstBillModel; set => _lstBillModel = value; }
        public static List<ItemInvoiceModel> LstCASelectedModel { get => _lstCASelectedModel; set => _lstCASelectedModel = value; }
        public static List<ItemStopElectricModel> LstStopElectricityModel { get => _lstStopElectricityModel; set => _lstStopElectricityModel = value; }
        public static string Branch { get => _branch; set => _branch = value; }
        public static string BranchId { get => _branchId; set => _branchId = value; }
        public static string BranchName { get => _branchName; set => _branchName = value; }
        public static string CashierNo { get => _cashierNo; set => _cashierNo = value; }
        public static string ReportOfflineType { get => _reportOfflineType; set => _reportOfflineType = value; }
        public static string TotalAmount { get => _totalAmount; set => _totalAmount = value; }
        public static string TotalContractAccount { get => _totalContractAccount; set => _totalContractAccount = value; }
        public static string TotalTaxInvoice { get => _totalTaxInvoice; set => _totalTaxInvoice = value; }
        public static string ReportName { get => _reportName; set => _reportName = value; }
        public static bool Status { get => _status; set => _status = value; }
        public static ItemOfflineModel OfflineModel { get => _offlineModel; set => _offlineModel = value; }
        public static string IpConfig { get => _ipConfig; set => _ipConfig = value; }
        public static long UserIdLogin { get => _userIdLogin; set => _userIdLogin = value; }
        public static int CanceluserId { get => _CanceluserId; set => _CanceluserId = value; }      
        #endregion
    }
}
