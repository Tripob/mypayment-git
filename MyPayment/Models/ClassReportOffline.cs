﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    public class ClassReportOffline
    {
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string DistId { get; set; }
        public string CashierNo { get; set; }
        public string DistName { get; set; }
        public string ReceiptBranch { get; set; }
        public string TotalAmountCash { get; set; }
        public string DateTime { get; set; }
        public string TotalAmountCheque { get; set; }
        public string CashierCheque { get; set; }        
    }
}
