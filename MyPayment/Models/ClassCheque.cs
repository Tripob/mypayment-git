﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassCheque
    {
        public string UserId { get; set; }
        public string Chno { get;set;}
        public string Bankno { get; set; }
        public string Bankname { get; set; }
        public string Chdate { get; set; }
        public string ChdateNew { get; set; }
        public decimal Chamount { get; set; }
        public string Chamountreal { get; set; }
        public string Chname { get; set; }
        public string Tel { get; set; }
        public bool Chcheck { get; set; }
        public string CONTACTNO { get; set; }
        public decimal Balance { get; set; }
        public bool Status { get; set; }
        public bool ChStatus { get; set; }
        public bool StatusCheque { get; set; }
        public string No { get; set; }

    }
}
