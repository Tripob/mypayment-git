﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
   public class ClassProcessCancelPayIn
    {
        public string result_code { get; set; }
        public string result_message { get; set; }
        public string version_control { get; set; }
        //public  payInDisplayResponseDtoList payInDisplayResponseDtoList { get; set; }
        public Int64[] closingDrawerIdList { get; set; }
    }
}
