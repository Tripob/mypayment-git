﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Models
{
    class ClassModelsReport
    {
        public string No { get; set; }
        public string DistName { get; set; }        
        public string CustomerName { get; set; }
        public string CustomerTaxId { get; set; }
        public string CustomerTaxBranch { get; set; }
        public string CustomerAddress { get; set; }
        public string Name { get; set; }
        public string CoAddress { get; set; }
        public string CA { get; set; }
        public string UI { get; set; }
        public string EmpName { get; set; }
        public string EmpId { get; set; }
        public string PayNo { get; set; }
        public decimal SumAmount { get; set; }
        public decimal SumVatAmount { get; set; }
        public decimal SumTotal { get; set; }
        public decimal SumPenaltyCharge { get; set; }
        public decimal SumTotalAmount { get; set; }
        public string strAmount { get; set; }
        public string CusType { get; set; }
        public string ReceiptBranch { get; set; }
        public string schdate { get; set; }
        public string invNo { get; set; }
        public string totalkWh { get; set; }
        public double amount { get; set; }
        public string vatBalance { get; set; }
        public string debtBalance { get; set; }
        public int countDay { get; set; }
        public decimal Defaultpenalty { get; set; }
        public string ftRate { get; set; }
        public string DetailDesc { get; set; }
        public string strDate { get; set; }
        public string AccountNo { get; set; }
        public string cardNo { get; set; }
        public string countCa { get; set; }
        public string countInv { get; set; }
    }
}
