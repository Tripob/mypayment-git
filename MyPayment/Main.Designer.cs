﻿namespace MyPayment
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.ButtonClos = new System.Windows.Forms.Button();
            this.labelDateTime = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuItemPayment = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemOther = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemCancelContinue = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemCancelReceipt = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemReport = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemRemittance = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemInquiryPayment = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemIsu = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemCountRemittance = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemReport1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemReport2 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemReport3 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemReport4 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemAdminReceipt = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemTestPrinter = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemCloseCard = new System.Windows.Forms.ToolStripMenuItem();
            this.panel13 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblStatua = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 746);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1024, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "Receive No.";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem4.Text = "toolStripMenuItem4";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 746);
            this.splitter1.TabIndex = 7;
            this.splitter1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel13);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1021, 36);
            this.panel3.TabIndex = 13;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button1);
            this.panel4.Controls.Add(this.ButtonClos);
            this.panel4.Controls.Add(this.labelDateTime);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(672, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(349, 36);
            this.panel4.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(263, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(41, 36);
            this.button1.TabIndex = 4;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ButtonClos
            // 
            this.ButtonClos.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonClos.BackgroundImage = global::MyPayment.Properties.Resources.close1;
            this.ButtonClos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonClos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonClos.Location = new System.Drawing.Point(307, 0);
            this.ButtonClos.Name = "ButtonClos";
            this.ButtonClos.Size = new System.Drawing.Size(41, 36);
            this.ButtonClos.TabIndex = 3;
            this.ButtonClos.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ButtonClos.UseVisualStyleBackColor = false;
            this.ButtonClos.Click += new System.EventHandler(this.ButtonClos_Click);
            this.ButtonClos.MouseHover += new System.EventHandler(this.ButtonClos_MouseHover);
            // 
            // labelDateTime
            // 
            this.labelDateTime.BackColor = System.Drawing.SystemColors.Control;
            this.labelDateTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDateTime.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelDateTime.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labelDateTime.ForeColor = System.Drawing.Color.Red;
            this.labelDateTime.Location = new System.Drawing.Point(0, 0);
            this.labelDateTime.Name = "labelDateTime";
            this.labelDateTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelDateTime.Size = new System.Drawing.Size(349, 36);
            this.labelDateTime.TabIndex = 1;
            this.labelDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.menuStrip1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(43, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(623, 36);
            this.panel6.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip1.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemPayment,
            this.MenuItemOther,
            this.MenuItem2,
            this.MenuItemReport,
            this.MenuItem7});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip1.Size = new System.Drawing.Size(623, 36);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MenuItemPayment
            // 
            this.MenuItemPayment.Image = global::MyPayment.Properties.Resources.Icon_29;
            this.MenuItemPayment.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemPayment.Name = "MenuItemPayment";
            this.MenuItemPayment.Size = new System.Drawing.Size(115, 32);
            this.MenuItemPayment.Text = "รับชำระ";
            this.MenuItemPayment.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.MenuItemPayment.Click += new System.EventHandler(this.MenuItemPayment_Click);
            // 
            // MenuItemOther
            // 
            this.MenuItemOther.Image = ((System.Drawing.Image)(resources.GetObject("MenuItemOther.Image")));
            this.MenuItemOther.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.MenuItemOther.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemOther.Name = "MenuItemOther";
            this.MenuItemOther.Size = new System.Drawing.Size(86, 32);
            this.MenuItemOther.Text = "อื่นๆ";
            this.MenuItemOther.Click += new System.EventHandler(this.MenuItemOther_Click);
            // 
            // MenuItem2
            // 
            this.MenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemCancelContinue,
            this.MenuItemCancelReceipt});
            this.MenuItem2.Image = global::MyPayment.Properties.Resources.Icon_40;
            this.MenuItem2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItem2.Name = "MenuItem2";
            this.MenuItem2.Size = new System.Drawing.Size(115, 32);
            this.MenuItem2.Text = "ยกเลิก";
            // 
            // MenuItemCancelContinue
            // 
            this.MenuItemCancelContinue.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemCancelContinue.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemCancelContinue.Name = "MenuItemCancelContinue";
            this.MenuItemCancelContinue.Size = new System.Drawing.Size(312, 36);
            this.MenuItemCancelContinue.Text = "ยกเลิกใบเสร็จรับเงิน";
            this.MenuItemCancelContinue.Click += new System.EventHandler(this.MenuItemCancelContinue_Click);
            // 
            // MenuItemCancelReceipt
            // 
            this.MenuItemCancelReceipt.Image = ((System.Drawing.Image)(resources.GetObject("MenuItemCancelReceipt.Image")));
            this.MenuItemCancelReceipt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemCancelReceipt.Name = "MenuItemCancelReceipt";
            this.MenuItemCancelReceipt.Size = new System.Drawing.Size(312, 36);
            this.MenuItemCancelReceipt.Text = "ยกเลิกค่าดำเนินการงดจ่ายไฟ";
            this.MenuItemCancelReceipt.Click += new System.EventHandler(this.MenuItemCancelReceipt_Click);
            // 
            // MenuItemReport
            // 
            this.MenuItemReport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemRemittance,
            this.MenuItemInquiryPayment,
            this.MenuItemIsu,
            this.MenuItemCountRemittance,
            this.MenuItemReport1,
            this.MenuItemReport2,
            this.MenuItemReport3,
            this.MenuItemReport4,
            this.MenuItemAdminReceipt,
            this.MenuItemTestPrinter});
            this.MenuItemReport.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__3_;
            this.MenuItemReport.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemReport.Name = "MenuItemReport";
            this.MenuItemReport.Size = new System.Drawing.Size(108, 32);
            this.MenuItemReport.Text = "รายงาน";
            // 
            // MenuItemRemittance
            // 
            this.MenuItemRemittance.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemRemittance.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemRemittance.Name = "MenuItemRemittance";
            this.MenuItemRemittance.Size = new System.Drawing.Size(438, 36);
            this.MenuItemRemittance.Text = "นำส่งเงิน";
            this.MenuItemRemittance.Click += new System.EventHandler(this.MenuItemRemittance_Click);
            // 
            // MenuItemInquiryPayment
            // 
            this.MenuItemInquiryPayment.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemInquiryPayment.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemInquiryPayment.Name = "MenuItemInquiryPayment";
            this.MenuItemInquiryPayment.Size = new System.Drawing.Size(438, 36);
            this.MenuItemInquiryPayment.Text = "ตรวจสอบการรับชำระ";
            this.MenuItemInquiryPayment.Click += new System.EventHandler(this.MenuItemInquiryPayment_Click);
            // 
            // MenuItemIsu
            // 
            this.MenuItemIsu.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemIsu.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemIsu.Name = "MenuItemIsu";
            this.MenuItemIsu.Size = new System.Drawing.Size(438, 36);
            this.MenuItemIsu.Text = "ตรวจสอบข้อมูลหนี้";
            this.MenuItemIsu.Click += new System.EventHandler(this.MenuItemIsu_Click);
            // 
            // MenuItemCountRemittance
            // 
            this.MenuItemCountRemittance.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemCountRemittance.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemCountRemittance.Name = "MenuItemCountRemittance";
            this.MenuItemCountRemittance.Size = new System.Drawing.Size(438, 36);
            this.MenuItemCountRemittance.Text = "สรุปยอดเงินส่งเข้าคลัง";
            this.MenuItemCountRemittance.Click += new System.EventHandler(this.MenuItemCountRemittance_Click);
            // 
            // MenuItemReport1
            // 
            this.MenuItemReport1.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemReport1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemReport1.Name = "MenuItemReport1";
            this.MenuItemReport1.Size = new System.Drawing.Size(438, 36);
            this.MenuItemReport1.Text = "รายงานสรุปการชำระเงินของแต่ละโต๊ะรับชำระ";
            this.MenuItemReport1.Click += new System.EventHandler(this.MenuItemReport1_Click);
            // 
            // MenuItemReport2
            // 
            this.MenuItemReport2.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemReport2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemReport2.Name = "MenuItemReport2";
            this.MenuItemReport2.Size = new System.Drawing.Size(438, 36);
            this.MenuItemReport2.Text = "รายงานสรุปใบนำส่งเงิน";
            this.MenuItemReport2.Click += new System.EventHandler(this.MenuItemReport2_Click);
            // 
            // MenuItemReport3
            // 
            this.MenuItemReport3.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemReport3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemReport3.Name = "MenuItemReport3";
            this.MenuItemReport3.Size = new System.Drawing.Size(438, 36);
            this.MenuItemReport3.Text = "รายงานสรุปยกเลิกใบรับเงิน";
            this.MenuItemReport3.Click += new System.EventHandler(this.MenuItemReport3_Click);
            // 
            // MenuItemReport4
            // 
            this.MenuItemReport4.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemReport4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemReport4.Name = "MenuItemReport4";
            this.MenuItemReport4.Size = new System.Drawing.Size(438, 36);
            this.MenuItemReport4.Text = "รายงานสรุปการรับเช็ค";
            this.MenuItemReport4.Click += new System.EventHandler(this.MenuItemReport4_Click);
            // 
            // MenuItemAdminReceipt
            // 
            this.MenuItemAdminReceipt.Image = global::MyPayment.Properties.Resources.Webp_net_resizeimage__2_;
            this.MenuItemAdminReceipt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemAdminReceipt.Name = "MenuItemAdminReceipt";
            this.MenuItemAdminReceipt.Size = new System.Drawing.Size(438, 36);
            this.MenuItemAdminReceipt.Text = "ส่งข้อมูลเพื่อลดหนี้";
            this.MenuItemAdminReceipt.Click += new System.EventHandler(this.MenuItemAdminReceipt_Click);
            // 
            // MenuItemTestPrinter
            // 
            this.MenuItemTestPrinter.Image = global::MyPayment.Properties.Resources.closeCard;
            this.MenuItemTestPrinter.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemTestPrinter.Name = "MenuItemTestPrinter";
            this.MenuItemTestPrinter.Size = new System.Drawing.Size(438, 36);
            this.MenuItemTestPrinter.Text = "ทดสอบพิมพ์";
            this.MenuItemTestPrinter.Click += new System.EventHandler(this.MenuItemTestPrinter_Click);
            // 
            // MenuItem7
            // 
            this.MenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemCloseCard});
            this.MenuItem7.Enabled = false;
            this.MenuItem7.Image = global::MyPayment.Properties.Resources.closeCard;
            this.MenuItem7.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.MenuItem7.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItem7.Name = "MenuItem7";
            this.MenuItem7.Size = new System.Drawing.Size(108, 32);
            this.MenuItem7.Text = "ปิดยอด";
            this.MenuItem7.Visible = false;
            // 
            // MenuItemCloseCard
            // 
            this.MenuItemCloseCard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuItemCloseCard.Name = "MenuItemCloseCard";
            this.MenuItemCloseCard.Size = new System.Drawing.Size(146, 36);
            this.MenuItemCloseCard.Text = "ปิดบัตร";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.pictureBox1);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(43, 36);
            this.panel13.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MyPayment.Properties.Resources.Icon_MEA;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(43, 36);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(956, 751);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(13, 13);
            this.lblVersion.TabIndex = 31;
            this.lblVersion.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(868, 751);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Product Version :";
            // 
            // lblStatua
            // 
            this.lblStatua.AutoSize = true;
            this.lblStatua.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblStatua.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblStatua.Location = new System.Drawing.Point(5, 750);
            this.lblStatua.Name = "lblStatua";
            this.lblStatua.Size = new System.Drawing.Size(53, 13);
            this.lblStatua.TabIndex = 33;
            this.lblStatua.Text = "ONLINE";
            // 
            // timer2
            // 
            this.timer2.Interval = 50000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblStatua);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบบรับชำระหนี้ค้าง";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button ButtonClos;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label labelDateTime;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemPayment;
        private System.Windows.Forms.ToolStripMenuItem MenuItemOther;
        private System.Windows.Forms.ToolStripMenuItem MenuItem2;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCancelContinue;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCancelReceipt;
        private System.Windows.Forms.ToolStripMenuItem MenuItemReport;
        private System.Windows.Forms.ToolStripMenuItem MenuItemRemittance;
        private System.Windows.Forms.ToolStripMenuItem MenuItemReport1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemReport2;
        private System.Windows.Forms.ToolStripMenuItem MenuItemReport3;
        private System.Windows.Forms.ToolStripMenuItem MenuItemReport4;
        private System.Windows.Forms.ToolStripMenuItem MenuItem7;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCloseCard;
        private System.Windows.Forms.ToolStripMenuItem MenuItemInquiryPayment;
        private System.Windows.Forms.ToolStripMenuItem MenuItemIsu;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCountRemittance;
        private System.Windows.Forms.ToolStripMenuItem MenuItemAdminReceipt;
        private System.Windows.Forms.ToolStripMenuItem MenuItemTestPrinter;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStatua;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button button1;
    }
}

