﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;
using System.IO.Compression;
using System.IO;
using MyPayment.Models;
using System.Globalization;
using System.Security.Principal;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;
using aejw.Network;
using System.Security.Cryptography;
using System.Transactions;
using System.Data.SQLite;
using System.Data;
using System.Reflection;
using Newtonsoft.Json;
using static MyPayment.Models.ClassLogsService;
using MyPayment.Controllers;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace MyPayment.Master
{
    class OffLineClass
    {

        // public static string FilePath = @"E:\MEA\TestOffline\";
        public static string FilePath = AppDomain.CurrentDomain.BaseDirectory + @"\Offline\";
        public static string DBPath = @"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + @"\Data\MyPaymentDB.db;Version=3;New=False;Compress=True;";
        //   public static string DBPath = @"Data Source=" + @"D:\MEA" + @"\Data\MyPaymentDB.db;Version=3;New=False;Compress=True;";
        public const string SecurityKey = "MEA-MyPayment";
        // public static string DBPath = AppDomain.CurrentDomain.BaseDirectory + @"Data Source=" + @"\Data\MyPaymentDB.db;Version=3;New=False;Compress=True;";
        //  public static string SERVER_IP = ConfigurationManager.AppSettings["IP-SERVER-ADDRESS"].ToString();
        //  public static int PORT_NO = int.Parse(ConfigurationManager.AppSettings["SERVER-PORT"].ToString());

        GeneralService genService = new GeneralService(true);
        ClassLogsService meaLog = new ClassLogsService("OFFLINE", "DataLog");
        ClassLogsService meaLogEvent = new ClassLogsService("OFFLINE", "EventLog");
        ClassLogsService meaLogError = new ClassLogsService("OFFLINE", "ErrorLog");
        public ItemInvoiceModel ReadQrCode(string strQrCode)
        {
            try
            {
                strQrCode = (strQrCode.IndexOf("|") > -1 ? "" : "|") + strQrCode;
                Random random = new Random();
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string random_decimal = "0123456789";
                string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                ItemInvoiceModel Rec = new ItemInvoiceModel();
                //  strQrCode = strQrCode.Replace(" ", "").Replace("\r\n","#");
                string Suffix = strQrCode.Substring(14, 2);
                string TaxId = "";
                string ContractAccount = "";
                string PowerUnit = "";
                string InvoiceNumber = "";
                string DebtCode = "";
                string DueDate = "";
                string Amount = "";
                string BillNumber = "";
                if (Suffix == "11")
                {
                    TaxId = strQrCode.Substring(1, 13);
                    ContractAccount = strQrCode.Substring(16, 9);
                    InvoiceNumber = strQrCode.Substring(34, 12);
                    DueDate = strQrCode.Substring(46, 6);
                    Amount = strQrCode.Substring(52, strQrCode.Length - 52);
                    Amount = Amount.Insert(Amount.Length - 2, ".");
                    BillNumber = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                    PowerUnit = strQrCode.Substring(25, 8);
                    Rec.Selected = true;
                    //  Rec.BillNumber = BillNumber;
                    Rec.TaxID = TaxId;
                    Rec.CA = ContractAccount;
                    Rec.InvoiceNumber = InvoiceNumber.Substring(1);
                    Rec.DueDate = DueDate.Substring(0, 2) + "/" + DueDate.Substring(2, 2) + "/" + DueDate.Substring(4, 2);
                    Rec.AmountIncVat = double.Parse(Amount);
                    Rec.PowerUnit = double.Parse(PowerUnit);
                    Rec.AmountIncVat = double.Parse(Rec.AmountIncVat.ToString("N2"));
                }
                return Rec;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ItemInvoiceModel ReadStdBarCode(string strQrCode)
        {
            try
            {
                Random random = new Random();
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string random_decimal = "0123456789";
                string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                ItemInvoiceModel Rec = new ItemInvoiceModel();
                strQrCode = "|" + strQrCode.Replace(" ", "").Replace("\r\n", "#").Trim();
                string ServiceCode = strQrCode.Substring(36, 2);
                string TaxId = "";
                string ContractAccount = "";
                string PowerUnit = "";
                string InvoiceNumber = "";
                string DebtCode = "";
                string DueDate = "";
                string Amount = "";
                string BillNumber = "";
                if (ServiceCode == "11")
                {
                    strQrCode = strQrCode.Replace("|", "");
                    TaxId = strQrCode.Substring(0, 13);
                    ContractAccount = strQrCode.Substring(15, 9);
                    InvoiceNumber = strQrCode.Substring(24, 11);
                    DueDate = strQrCode.Substring(37, 6);
                    PowerUnit = strQrCode.Substring(45, 6);
                    BillNumber = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                    Amount = strQrCode.Substring(50, strQrCode.Length - 50);
                    Amount = Amount.Insert(Amount.Length - 2, ".");
                    Rec.Selected = true;
                    //  Rec.BillNumber = BillNumber;
                    Rec.TaxID = TaxId;
                    Rec.CA = ContractAccount;
                    Rec.InvoiceNumber = InvoiceNumber;
                    Rec.DueDate = DueDate.Substring(0, 2) + "/" + DueDate.Substring(2, 2) + "/" + DueDate.Substring(4, 2);
                    Rec.AmountIncVat = double.Parse(Amount);
                    Rec.PowerUnit = double.Parse(PowerUnit);
                    Rec.AmountIncVat = double.Parse(Rec.AmountIncVat.ToString("N2"));
                }
                return Rec;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ItemInvoiceModel TestGenCAInputRec()
        {
            try
            {
                ItemInvoiceModel Rec = new ItemInvoiceModel();
                Random random = new Random();
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string random_decimal = "0123456789";
                string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                DateTime RandomDay()
                {
                    DateTime start = new DateTime(2019, 9, 27);
                    int range = (DateTime.Today.AddDays(5) - start).Days;
                    return start.AddDays(random.Next(range));
                }
                Rec.DueDate = RandomDay().ToString("dd/MM/yyyy");
                Rec.Selected = true;
                Rec.CA = new string(Enumerable.Repeat(random_decimal, 9).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.TaxID = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.InvoiceNumber = new string(Enumerable.Repeat(random_decimal, 12).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.AmountExVat = double.Parse(new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray()));
                Rec.PowerUnit = double.Parse(new string(Enumerable.Repeat(random_decimal, 3).Select(s => s[random.Next(s.Length)]).ToArray()));
                Rec.ServiceType = "A";
                return Rec;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ItemCodeData TestGenOfflineInputRec()
        {
            try
            {
                ItemCodeData Rec = new ItemCodeData();
                Random random = new Random();
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string random_decimal = "0123456789";
                string random_chars_only = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                DateTime RandomDay()
                {
                    DateTime start = new DateTime(2019, 9, 27);
                    int range = (DateTime.Today.AddDays(5) - start).Days;
                    return start.AddDays(random.Next(range));
                }
                Rec.DueDate = RandomDay().ToString("dd/MM/yyyy");
                Rec.Selected = true;
                Rec.BillNumber = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.ContractAccount = new string(Enumerable.Repeat(random_decimal, 9).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.TaxId = new string(Enumerable.Repeat(random_decimal, 13).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.InvoiceNumber = new string(Enumerable.Repeat(random_decimal, 12).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.AmountExVat = new string(Enumerable.Repeat(random_decimal, 4).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.PowerUnit = new string(Enumerable.Repeat(random_decimal, 3).Select(s => s[random.Next(s.Length)]).ToArray());
                Rec.AmountExVat = double.Parse(Rec.AmountExVat).ToString("N2");
                Rec.ServiceType = "A";
                return Rec;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public void ReadOffLineFileFromCSV()
        {
            try
            {
                ItemOfflineModel UserModel = new ItemOfflineModel();
                TextReader readFile = new StreamReader(@"c:\\textwriter.csv");
                UserModel = CsvSerializer.DeserializeFromReader<ItemOfflineModel>(readFile);
            }
            catch (Exception ex)
            {

            }
        }
        public List<ItemOfflineModel> ReadAllOffLineFileFromCSV(string strDate)
        {
            try
            {
                //string strPath = @"E:\MEA\TestOffline\" + strDate;
                string strPath = FilePath + @"\" + strDate;
                List<ItemOfflineModel> Result = new List<ItemOfflineModel>();
                string[] files = System.IO.Directory.GetFiles(strPath, "*.csv").Where(m => m.Contains("OFFLINE")).ToArray();
                List<ItemOfflineModel> LstItemModel = new List<ItemOfflineModel>();
                TextReader readFile;
                int RowNo = 0;
                foreach (var path in files)
                {
                    RowNo++;
                    readFile = new StreamReader(path);
                    LstItemModel = CsvSerializer.DeserializeFromReader<List<ItemOfflineModel>>(readFile);
                    //  ItemModel.RowNo = RowNo.ToString();
                    Result.AddRange(LstItemModel);
                }
                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ItemOfflineLogToServiceModel> ReadOfflineLogToServiceCSV(string strDate)
        {
            try
            {
                string strPath = @"E:\MEA\TestOffline\" + strDate;
                List<ItemOfflineLogToServiceModel> Result = new List<ItemOfflineLogToServiceModel>();
                string[] files = System.IO.Directory.GetFiles(strPath, "*.csv").Where(m => m.Contains("SERVICE")).ToArray();
                List<ItemOfflineLogToServiceModel> ItemModel = new List<ItemOfflineLogToServiceModel>();
                TextReader readFile;
                int RowNo = 0;
                foreach (var path in files)
                {
                    RowNo++;
                    readFile = new StreamReader(path);
                    ItemModel = CsvSerializer.DeserializeFromReader<List<ItemOfflineLogToServiceModel>>(readFile);
                    //ItemModel.RowNo = RowNo.ToString();
                    Result.AddRange(ItemModel);
                }
                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ItemOfflineModel> ReadCancelOffLineFileFromCSV(string strDate)
        {
            try
            {
                string strPath = FilePath + @"\" + strDate;
                List<ItemOfflineModel> Result = new List<ItemOfflineModel>();
                //string[] files = System.IO.Directory.GetFiles(@"\\172.20.14.147\MeaShare", "*.csv");
                string[] files = System.IO.Directory.GetFiles(strPath, "*.csv").Where(m => m.Contains("CANCEL")).ToArray();
                ItemOfflineModel ItemModel = new ItemOfflineModel();
                TextReader readFile;
                foreach (var path in files)
                {
                    readFile = new StreamReader(path);
                    ItemModel = CsvSerializer.DeserializeFromReader<ItemOfflineModel>(readFile);

                    Result.Add(ItemModel);
                }

                return Result;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool isDupInvoiceNumberInDB(string strInvoiceNumber)
        {
            bool Result = false;
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = @"Select * from TB_INVOICE Where InvoiceNumber = '" + strInvoiceNumber + @"' Limit 1";
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                        }
                        /**************************************/
                        if (dt.Rows.Count == 0)
                        {
                            Result = false;
                        }
                        else
                        {
                            Result = true;
                        }
                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        public ItemOfflineSumBanknote GetOfflineSumBanknote(string strDate)
        {
            try
            {
                ItemOfflineSumBanknote Result = new ItemOfflineSumBanknote();
                List<ItemOfflineModel> CashData = GetSummaryCashdeskOfflineReport(strDate);
                List<ItemtSummaryChequeModel> ChequeData = GetSummaryChequeReport(strDate);
                string Cash = "";// CashData.Where(m => m.Cash != null).Sum(m => double.Parse(m.Cash)).ToString("N2");
                string Cheque = ChequeData.Where(m => m.Amount != null).Sum(m => double.Parse(m.Amount)).ToString("N2");
                Result.Cash = Cash;
                Result.Cheque = Cheque;
                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #region WriteFile
        public void WriteOffLineToCSV_Bak(ItemOfflineModel itemModel)
        {
            try
            {
                string strDate = DateTime.Now.ToString("yyyyMMdd");
                string strPath = FilePath + strDate;
                string strFileName = "";//"OFFLINE_" + itemModel.ContractAccount + "_" + itemModel.InvoiceNumber;
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                TextWriter writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");
                CsvSerializer.SerializeToWriter(itemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }
        public void WriteCancelOffLineToCSV(ItemtRevertTaxInvoiceModel itemModel)
        {
            try
            {
                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 543;
                }
                string strDate = year.ToString() + DateTime.Now.ToString("MMdd");
                string strPath = FilePath + strDate;
                string strFileName = "CANCEL_" + itemModel.InvoiceNumber;
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                TextWriter writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");
                CsvSerializer.SerializeToWriter(itemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }
        public void WriteChequeOffLineToCSV(List<ItemtSummaryChequeModel> LstItemModel, string strDateTime)
        {
            try
            {
                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 543;
                }
                string strDate = year.ToString() + DateTime.Now.ToString("MMdd");
                string strPath = FilePath + strDate;
                string strFileName = "CHEQUE_" + strDateTime;
                TextWriter writer;
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");
                CsvSerializer.SerializeToWriter(LstItemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }
        public void WriteOffLineLogToServiceToCSV(List<ItemOfflineLogToServiceModel> LstItemModel)
        {
            try
            {
                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 543;
                }
                string strDate = year.ToString() + DateTime.Now.ToString("MMdd");
                string strPath = FilePath + strDate;
                ItemOfflineLogToServiceModel itemModel = LstItemModel.FirstOrDefault();
                string strFileName = "";
                TextWriter writer;
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                strFileName = "SERVICE_" + itemModel.ContractAccount + "_" + itemModel.InvoiceNumber + "_" + itemModel.Transact;
                writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");
                CsvSerializer.SerializeToWriter(LstItemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }
        public void WriteOffLineToCSV(List<ItemOfflineModel> LstItemModel, string strDateTime)
        {
            try
            {
                int year = DateTime.Now.Year;
                if (year > 2500)
                {
                    year = year - 543;
                }
                string strDate = year.ToString() + DateTime.Now.ToString("MMdd");
                string strPath = FilePath + strDate;
                string strFileName = "OFFLINE_" + strDateTime;
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                TextWriter writer = new StreamWriter(strPath + @"\" + strFileName + ".csv");
                CsvSerializer.SerializeToWriter(LstItemModel, writer);
                writer.Flush();
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {

            }
        }
        public bool WriteOfflineTxtLog()
        {
            try
            {
                List<ItemOfflineLogToServiceModel> LstItemOffline = ReadOfflineLogToServiceCSV("20191002");
                string strItem = "";
                string TransactionType = "";
                string PaymentDate = "";
                string ServiceType = "";
                string ContractAccount = "";
                string RefNumber = "";
                string TaxInvoice = "";
                string Amount = "";
                string Interest = "";
                string DayInterest = "";
                string RefDbOps = "";
                strItem = @"@MEA" + Environment.NewLine;
                foreach (var item in LstItemOffline)
                {
                    strItem = strItem + "|";
                    TransactionType = "CSH";
                    PaymentDate = item.PaymentDate;
                    ServiceType = "A";
                    ContractAccount = item.ContractAccount.PadLeft(9, '0');
                    RefNumber = "XBLNR";
                    TaxInvoice = item.BillNo;
                    Amount = (item.AmountIncVat.IndexOf('.') > -1 ? item.AmountIncVat : item.AmountIncVat + ".00");
                    Interest = "xxxx.xx";
                    DayInterest = "yy";
                    RefDbOps = "zzzz";

                    strItem = strItem + TransactionType + "|" + PaymentDate + "|" + ServiceType + "|" + ContractAccount + "|" + RefNumber + "|" + TaxInvoice + "|" + Amount + "|" + Interest + "|" + DayInterest + "|" + RefDbOps + Environment.NewLine;
                }
                strItem = strItem + "#";
                /****************************************************/
                string strDate = DateTime.Now.ToString("yyyyMMdd");
                string strPath = FilePath + strDate;
                string strFileName = "LOG_" + strDate + ".txt";
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                File.AppendAllText(strPath + "\\" + strFileName, strItem);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string ConvertPaymentDateForService(string strData)
        {
            try
            {
                string strYear = "";
                string strResult = "";
                DateTime myDate = DateTime.ParseExact(strData, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (myDate.Year > 2100)
                {
                    strYear = (myDate.Year - 543).ToString();
                }
                else
                {
                    strYear = myDate.Year.ToString();
                }
                strResult = myDate.ToString("ddMM") + strYear;
                return strResult;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        #endregion
        #region GetReport
        public List<ItemtSummaryChequeModel> GetSummaryChequeReport(string strDate)
        {
            List<ItemtSummaryChequeModel> lstSummaryChequeReport = new List<ItemtSummaryChequeModel>();
            try
            {
                string sSql = @"Select OFL.UserCode , MAP_CHE.ChequeNumber,CHE.BankCode ,CHE.BankName , ( Substr( CHE.ChequeDate ,9,2) || '/' || Substr( CHE.ChequeDate,6,2) || '/' || Substr( CHE.ChequeDate ,0,5)) As ChequeDate , CHE.CashierCheque  , MAP_CHE.Amount ,CHE.ChequeName , INV.BillNumber
                                From TB_MAPCHEQUE MAP_CHE
                                Inner Join TB_CHEQUE CHE On MAP_CHE.ChequeNumber = CHE.ChequeNumber
                                Inner Join TB_INVOICE INV On MAP_CHE.InvoiceNumber = INV.InvoiceNumber
                                Inner Join TB_OFFLINE OFL On INV.ReceiptNumber = OFL.ReceiptNumber
                                Where Substr( OFL.CreateDate ,0,11) = '" + strDate + "'";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            /**************************************/
                            lstSummaryChequeReport = new List<ItemtSummaryChequeModel>();
                            lstSummaryChequeReport = (from DataRow dr in dt.Rows
                                                      select new ItemtSummaryChequeModel()
                                                      {
                                                          UserCode = dr["UserCode"].ToString(),
                                                          ChequeNumber = dr["ChequeNumber"].ToString(),
                                                          ChequeName = dr["ChequeName"].ToString(),
                                                          BankCode = dr["BankCode"].ToString(),
                                                          BankName = dr["BankName"].ToString(),
                                                          ChequeDate = dr["ChequeDate"].ToString(),
                                                          Amount = double.Parse(dr["Amount"].ToString()).ToString("N2"),
                                                          CashierCheque = dr["CashierCheque"].ToString(),
                                                          BillNumber = dr["BillNumber"].ToString(),
                                                      }).ToList();
                            /**************************************/
                        }
                        /**************************************/
                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return lstSummaryChequeReport;
            }
            catch (Exception ex)
            {
                return lstSummaryChequeReport;
            }
        }
        public List<ItemOfflineModel> GetSummaryCashdeskOfflineReport(string strDate)
        {
            List<ItemOfflineModel> LstOfflineAll = new List<ItemOfflineModel>();
            try
            {
                string sSql = @"Select  ( Substr(OFL.CreateDate ,9,2) || '/' || Substr(OFL.CreateDate ,6,2) || '/' || Substr(OFL.CreateDate ,0,5)) As CreateDate  , Substr(OFL.CreateDate ,11,9) As CreateTime , OFL.UserCode , INV.CA , INV.InvoiceNumber , INV.BillNumber , INV.AmountIncVat , OFL.Tel , MCA.Cash , OFL.CashReceive ,
                                (Select Sum( MCH.Amount) From TB_MAPCHEQUE MCH Where INV.InvoiceNumber = MCH.InvoiceNumber)  SumCheque , 0.0 As ChequeChange
                                From TB_INVOICE INV 
                                Inner Join TB_OFFLINE OFL On INV.ReceiptNumber = OFL.ReceiptNumber
                                Left Join TB_MAPCASH MCA On  INV.InvoiceNumber = MCA.InvoiceNumber
                                Where Substr(OFL.CreateDate ,0,11) = '" + strDate + "'";

                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            /**************************************/
                            LstOfflineAll = new List<ItemOfflineModel>();
                            LstOfflineAll = (from DataRow dr in dt.Rows
                                             select new ItemOfflineModel()
                                             {
                                                 CreateDate = dr["CreateDate"].ToString(),
                                                 CreateTime = dr["CreateTime"].ToString(),
                                                 UserCode = dr["UserCode"].ToString(),
                                                 CA = dr["CA"].ToString(),
                                                 InvoiceNumber = dr["InvoiceNumber"].ToString(),
                                                 BillNumber = dr["BillNumber"].ToString(),
                                                 TotalAmountIncVat = dr["AmountIncVat"].ToString() == "" ? 0.0 : double.Parse(dr["AmountIncVat"].ToString()),
                                                 Tel = dr["Tel"].ToString(),
                                                 Cash = dr["Cash"].ToString() == "" ? 0.0 : double.Parse(dr["Cash"].ToString()),
                                                 SumCheque = dr["SumCheque"].ToString() == "" ? 0.0 : double.Parse(dr["SumCheque"].ToString()),
                                                 ChequeChange = dr["ChequeChange"].ToString() == "" ? 0.0 : double.Parse(dr["ChequeChange"].ToString()),
                                                 CashReceive = dr["CashReceive"].ToString() == "" ? 0.0 : double.Parse(dr["CashReceive"].ToString())
                                             }).ToList();
                            /**************************************/
                            int RowNo = 1;
                            foreach (var item in LstOfflineAll)
                            {
                                item.RowNo = RowNo.ToString();
                                RowNo++;
                            }
                        }
                        /**************************************/
                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return LstOfflineAll;
            }
            catch (Exception ex)
            {
                return LstOfflineAll;
            }
        }
        public SummaryPaymentObj GetSummaryPaymentReport(string strDate)
        {
            List<ItemSummaryPaymentModel> LstISPM = new List<ItemSummaryPaymentModel>();
            List<ItemOfflineModel> LstOfflineAll = new List<ItemOfflineModel>();
            ItemSummaryPaymentModel RecModel = new ItemSummaryPaymentModel();
            SummaryPaymentObj ResultObj = new SummaryPaymentObj();
            decimal sumCash = 0;
            decimal sumCashChange = 0;
            decimal sumCredit = 0;
            decimal sumCheque = 0;
            decimal sumChequeChange = 0;
            decimal sumTotal = 0;
            decimal sumGainLoss = 0;
            try
            {
                // LstOfflineAll = ReadAllOffLineFileFromCSV(strDate);
                string Branch = "";// LstOfflineAll.FirstOrDefault().Branch;
                string CashierNo = "";// LstOfflineAll.FirstOrDefault().CashierNo;
                string UserId = "";// LstOfflineAll.FirstOrDefault().UserId;
                                   //  var LstOfflineCheque = GetSummaryChequeReport(strDate);
                ItemDataPaymentModel spmObj = SumPaymentObj(strDate);
                DateTime PaymentDate = DateTime.ParseExact(strDate, "yyyy-MM-dd", null);
                sumCash = (decimal)spmObj.SumCash;
                sumCredit = 0;
                sumCheque = (decimal)spmObj.SumCheque;
                sumChequeChange = (decimal)spmObj.SumChequeChange;
                sumGainLoss = 0;// LstOfflineAll.Where(m => m.GainLoss != null).Sum(m => decimal.Parse(m.GainLoss));
                sumTotal = sumCash + sumCredit + sumCheque;
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "วันที่รับชำระ";
                RecModel.Description = PaymentDate.ToString("dd/MM/yyyy");
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "เขต";
                RecModel.Description = Branch;
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "โต๊ะ";
                RecModel.Description = CashierNo;
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รหัสพนักงาน";
                RecModel.Description = UserId;
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รับเช็ค";
                RecModel.Description = string.Format("{0:n}", sumCheque);
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "เงินทอนเช็ค";
                RecModel.Description = string.Format("{0:n}", sumChequeChange);
                LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รับเงินสด";
                RecModel.Description = string.Format("{0:n}", sumCash);
                LstISPM.Add(RecModel);
                /******************************************************/
                //RecModel = new ItemSummaryPaymentModel();
                //RecModel.Title = "รวม Gain(+)/Loss(-)";
                //RecModel.Description = string.Format("{0:n}", sumGainLoss);
                //LstISPM.Add(RecModel);
                /******************************************************/
                //RecModel = new ItemSummaryPaymentModel();
                //RecModel.Title = "รับเครดิต";
                //RecModel.Description = string.Format("{0:n}", sumCredit);
                //LstISPM.Add(RecModel);
                /******************************************************/
                RecModel = new ItemSummaryPaymentModel();
                RecModel.Title = "รวม";
                RecModel.Description = string.Format("{0:n}", sumTotal);
                LstISPM.Add(RecModel);

                /*****************************************/
                ItemSummaryPaymentModel_Display objDisplay = new ItemSummaryPaymentModel_Display();
                objDisplay.UserId = GlobalClass.UserCode;
                objDisplay.PaymentDate = PaymentDate.ToString("dd/MMyyyy");
                objDisplay.Branch = GlobalClass.BranchName;
                objDisplay.CashierNo = GlobalClass.CashierNo;
                objDisplay.SumCheque = string.Format("{0:n}", sumCheque);
                objDisplay.SumChequeChange = string.Format("{0:n}", sumChequeChange);
                objDisplay.SumCash = string.Format("{0:n}", sumCash);
                objDisplay.SumGainLoss = string.Format("{0:n}", sumGainLoss);
                objDisplay.SumCredit = string.Format("{0:n}", sumCredit);
                objDisplay.SumTotal = string.Format("{0:n}", sumTotal);

                ResultObj.Display = new List<ItemSummaryPaymentModel_Display>();
                ResultObj.Display.Add(objDisplay);
                /*****************************************/
                ResultObj.Report = LstISPM;
                return ResultObj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ItemDataPaymentModel SumPaymentObj(string strDate)
        {
            ItemDataPaymentModel DataPaymentModel = new ItemDataPaymentModel();
            try
            {
                string sSql = @"Select 
                                ( SeLect Sum( MCH.Amount) From TB_MAPCHEQUE MCH
                                  Where MCH.InvoiceNumber In (
                                             Select INV.InvoiceNumber From TB_INVOICE INV
                                             Where INV.ReceiptNumber In (Select OFL.ReceiptNumber
                                                                          From TB_OFFLINE OFL
                                                                          Where Substr( OFL.CreateDate ,0,11) = '" + strDate + @"')
                                           ) ) As SumCheque ,
                                                         (Select Sum( CHA.ChequeChange ) From TB_CHANGE CHA
                                                             Where CHA.ReceiptNumber In (Select OFL.ReceiptNumber
                                                        From TB_OFFLINE OFL
                                              Where Substr( OFL.CreateDate ,0,11) = '" + strDate + @"')
                                            ) As SumChequeChange ,
                                             (SeLect Sum( MCA.Cash) From TB_MAPCASH MCA
                                             Where MCA.InvoiceNumber In (

                                             Select INV.InvoiceNumber From TB_INVOICE INV
                                             Where INV.ReceiptNumber In (Select OFL.ReceiptNumber
                                             From TB_OFFLINE OFL
                                             Where Substr( OFL.CreateDate ,0,11) = '" + strDate + @"')
                               )) As SumCash";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            /**************************************/
                            DataPaymentModel = new ItemDataPaymentModel();
                            DataPaymentModel = (from DataRow dr in dt.Rows
                                                select new ItemDataPaymentModel()
                                                {
                                                    SumCash = dr["SumCash"].ToString() != "" ? double.Parse(dr["SumCash"].ToString()) : 0.0,
                                                    SumChequeChange = dr["SumChequeChange"].ToString() != "" ? double.Parse(dr["SumChequeChange"].ToString()) : 0.0,
                                                    SumCheque = dr["SumCheque"].ToString() != "" ? double.Parse(dr["SumCheque"].ToString()) : 0.0,
                                                }).FirstOrDefault();
                            /**************************************/
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return DataPaymentModel;
            }
            catch (Exception ex)
            {
                return DataPaymentModel;
            }
        }
        public List<ItemBillCancelModel> GetRevertReport(string strDate)
        {
            List<ItemBillCancelModel> LstBillModel = new List<ItemBillCancelModel>();
            try
            {
                string sSql = @"Select INV.*,BIL.Remark From TB_INVOICE INV
                              Inner Join TB_BILL BIL On INV.BillNumber = BIL.BillNumber
                               Where BIL.Status = 'I' And   INV.ReceiptNumber in (Select OFL.ReceiptNumber From TB_OFFLINE OFL Where  Substr(OFL.CreateDate ,0,11) = '" + strDate + "')";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            /**************************************/
                            LstBillModel = new List<ItemBillCancelModel>();
                            LstBillModel = (from DataRow dr in dt.Rows
                                            select new ItemBillCancelModel()
                                            {
                                                UserCode = GlobalClass.UserCode,
                                                CA = dr["CA"].ToString(),
                                                InvoiceNumber = dr["InvoiceNumber"].ToString(),
                                                AmountIncVat = double.Parse(dr["AmountIncVat"].ToString()),
                                                BillNumber = dr["BillNumber"].ToString(),
                                                Remark = dr["Remark"].ToString(),
                                            }).ToList();
                            /**************************************/
                        }
                        /**************************************/
                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return LstBillModel;
            }
            catch (Exception ex)
            {
                return LstBillModel;
            }
        }
        #endregion
        #region Update Program
        public void MapNetworkDrive()
        {
            NetworkDrive oNetDrive = new NetworkDrive();
            try
            {
                //oNetDrive.Force = false;
                //oNetDrive.Persistent = true;
                //oNetDrive.LocalDrive = "Z:";
                //oNetDrive.PromptForCredentials = false;
                //oNetDrive.ShareName = @"\\10.118.6.153\C$";
                //oNetDrive.SaveCredentials = false;
                ////match call to options provided
                //oNetDrive.MapDrive("Administrator", "P@ssw0rd");
                RemoveMapNetworkDrive();
                string DRIVE = ConfigurationManager.AppSettings["DRIVE"].ToString();
                string SHARENAME = ConfigurationManager.AppSettings["SHARENAME"].ToString();
                string USERNAME = ConfigurationManager.AppSettings["USERNAME"].ToString();
                string PASSWORD = ConfigurationManager.AppSettings["PASSWORD"].ToString();
                oNetDrive.Force = false;
                oNetDrive.Persistent = true;
                oNetDrive.LocalDrive = DRIVE;
                oNetDrive.PromptForCredentials = false;
                oNetDrive.ShareName = @SHARENAME;
                oNetDrive.SaveCredentials = false;
                //match call to options provided
                oNetDrive.MapDrive(USERNAME, PASSWORD);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void RemoveMapNetworkDrive()
        {
            NetworkDrive oNetDrive = new NetworkDrive();
            try
            {
                if (System.IO.DriveInfo.GetDrives().Where(m => m.Name == @"Z:\").Any())
                {
                    oNetDrive.Force = false;
                    oNetDrive.LocalDrive = "Z:";
                    oNetDrive.UnMapDrive();
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);
            }
        }
        public bool isVersionUpdate()
        {
            try
            {
                MapNetworkDrive();
                string LOCAL_VERSION = Application.ProductVersion;// ConfigurationManager.AppSettings["VERSION"].ToString();
                string URL_UPDATE = ConfigurationManager.AppSettings["URL_UPDATE"].ToString();
                string FileVersion = URL_UPDATE + @"\Version.txt";
                var LstLine = File.ReadAllLines(FileVersion).ToList();
                string txtVersion = "Version:";
                var ServerVersion = LstLine.Where(m => m.Contains(txtVersion)).FirstOrDefault();
                if (ServerVersion != null && ServerVersion != "")
                {
                    ServerVersion = ServerVersion.Replace(txtVersion, "").Trim();
                    if (String.Compare(ServerVersion, LOCAL_VERSION) > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
        #region Encrypt Data
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            System.Configuration.AppSettingsReader settingsReader =
                                                new AppSettingsReader();
            // Get the key from config file
            //string key = (string)settingsReader.GetValue("SecurityKey",typeof(String));
            string key = SecurityKey;
            //System.Windows.Forms.MessageBox.Show(key);
            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);
            System.Configuration.AppSettingsReader settingsReader =
                                                new AppSettingsReader();
            //Get your key from config file to open the lock!
            //  string key = (string)settingsReader.GetValue("SecurityKey",typeof(String));
            string key = SecurityKey;
            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider
                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        public static string Base64Encode(string strData)
        {
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(strData);
                string base64 = Convert.ToBase64String(bytes);
                return base64;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public static string Base64Decode(string strData)
        {
            try
            {
                byte[] bytes = Convert.FromBase64String(strData);
                string str = Encoding.UTF8.GetString(bytes);
                return str;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public static string EncryptToken(string strData)
        {
            try
            {
                string Token = Encrypt(strData, true);
                Token = Base64Encode(Token);
                return Token;
            }
            catch (Exception ex)
            {
                return "";

            }
        }
        public static string DecryptToken(string Token)
        {
            try
            {
                string strData = Base64Decode(Token);
                strData = Decrypt(strData, true);
                return strData;
            }
            catch (Exception ex)
            {
                return "";

            }
        }
        #endregion
        #region Login Offline
        public bool LoginOffline(string userName, string password)
        {
            try
            {
                ItemUserOfflineModel UserOfflineModel = new ItemUserOfflineModel();
                UserOfflineModel = GetUserOffline(userName.Trim(), password.Trim());
                if (UserOfflineModel != null)
                {
                    GlobalClass.BranchId = UserOfflineModel.distId.ToString();
                    GlobalClass.UserId = int.Parse(UserOfflineModel.UserCode);
                    GlobalClass.UserCode = UserOfflineModel.UserCode;
                    GlobalClass.BranchName = UserOfflineModel.receiptBranch;
                    GlobalClass.UserName = UserOfflineModel.UserName;
                    GlobalClass.CashierNo = UserOfflineModel.cashierNo.ToString();
                    GlobalClass.DistName = UserOfflineModel.distName + " สาขา " + UserOfflineModel.receiptBranch;
                    GlobalClass.Dist = UserOfflineModel.receiptBranch + " - " + UserOfflineModel.distName;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
        #region Database
        public List<ItemBank> GetBankOffline()
        {
            List<ItemBank> LstBank = new List<ItemBank>();
            System.Reflection.MethodBase method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                string sSql = @" Select * from TB_BANK";

                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            /**************************************/
                            LstBank = (from DataRow dr in dt.Rows
                                       select new ItemBank()
                                       {
                                           BankCode = dr["BankCode"].ToString(),
                                           BankName = dr["BankName"].ToString(),

                                       }).ToList();
                            /**************************************/
                        }
                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return LstBank;
            }
            catch (Exception ex)
            {
                return LstBank;
            }
        }
        public int GetMaxBillNumber()
        {
            string sKey = DateTime.Now.ToString("yyMMdd") + GlobalClass.UserCode;
            int BillIndex = 0;
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = @"Select BillNumber From TB_BILL  Where BillNumber Like '%" + sKey + "%' ORDER BY BillNumber Desc LIMIT 1";
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                        }
                        /**************************************/
                        if (dt.Rows.Count == 0)
                        {
                            BillIndex = 1;
                        }
                        else
                        {
                            string strMaxCode = dt.Rows[0][0].ToString();
                            BillIndex = int.Parse(strMaxCode.Substring(strMaxCode.Length - 3));
                            BillIndex++;
                        }

                        /**************************************/

                        tran.Complete();


                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return BillIndex;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public bool isDuplicateChequeNumber(string ChequeNumber, string BankCode)
        {
            bool Result = false;
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = @"Select CHE.ChequeNumber From TB_CHEQUE CHE Where CHE.BankCode = '" + BankCode + "' And CHE.ChequeNumber = '" + ChequeNumber + "' Limit 1";

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                        }

                        /**************************************/

                        if (dt.Rows.Count == 0)
                        {
                            Result = false;
                        }
                        else
                        {
                            Result = true;
                        }

                        /**************************************/

                        tran.Complete();


                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        public string CreateReceiptNumber()
        {
            string strResult = "";
            try
            {

                //int BillIndex = GlobalClass.BillIndex;
                //if (BillIndex == 0)
                //{

                //    using (TransactionScope tran = new TransactionScope())
                //    {
                //        SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                //        try
                //        {


                //            DataTable dt = new DataTable();

                //            m_dbConnection.Open();

                //            using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                //            {
                //                fmd.CommandText = @"Select  ReceiptNumber from TB_OFFLINE ORDER BY ReceiptNumber Desc LIMIT 1";

                //                SQLiteDataReader reader = fmd.ExecuteReader();
                //                dt.Load(reader);
                //                reader.Close();
                //                m_dbConnection.Close();
                //            }

                //            /**************************************/

                //            if (dt.Rows.Count == 0)
                //            {
                //                BillIndex = 1;
                //            }
                //            else
                //            {
                //                string strMaxCode = dt.Rows[0][0].ToString();
                //                BillIndex = int.Parse(strMaxCode.Substring(strMaxCode.Length - 5));
                //                BillIndex++;
                //            }
                //            strResult = "R";
                //            GlobalClass.BillIndex = BillIndex;
                //            strResult = strResult + BillIndex.ToString("D5");

                //            /**************************************/
                //            tran.Complete();
                //        }
                //        catch (Exception ex)
                //        {
                //            m_dbConnection.Close();
                //            tran.Dispose();
                //        }
                //    }
                //}
                //else
                //{
                //    BillIndex++;
                //    strResult = "R" ;
                //    GlobalClass.BillIndex = BillIndex;
                //    strResult = strResult + BillIndex.ToString("D5");
                //}
                int BillIndex = 0;
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = @"Select  ReceiptNumber from TB_OFFLINE ORDER BY ReceiptNumber Desc LIMIT 1";

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                        }

                        /**************************************/

                        if (dt.Rows.Count == 0)
                        {
                            BillIndex = 1;
                        }
                        else
                        {
                            string strMaxCode = dt.Rows[0][0].ToString();
                            BillIndex = int.Parse(strMaxCode.Substring(strMaxCode.Length - 5));
                            BillIndex++;
                        }
                        strResult = "R";
                        GlobalClass.BillIndex = BillIndex;
                        strResult = strResult + BillIndex.ToString("D5");

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }

                return strResult;

            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public void InsertOffline(ItemOfflineModel objData)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {
                        string sSql = "";
                        conn.Open();
                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {
                            string CreateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            sSql = " INSERT INTO TB_OFFLINE (ReceiptNumber ,UserCode ,BranchCode ,TotalAmountExVat ,TotalVat ,TotalAmountIncVat , SendFlag ,CashReceive ";
                            sSql = sSql + " ,TotalFine ,TotalAll , Tel ,Status ,CreateDate ,UpdateDate) VALUES ( ";
                            sSql = sSql + " '" + objData.ReceiptNumber + "' , '" + objData.UserCode + "'  , '" + objData.BranchCode + "' ," + objData.TotalAmountExVat + " ," + objData.TotalVat + " ," + objData.TotalAmountIncVat + " , 'W' ," + objData.CashReceive;
                            sSql = sSql + " ," + objData.TotalFine + " ," + objData.TotalAll + " , '" + objData.Tel + "' ," + "'A' ,'" + CreateDate + "' ,'" + CreateDate + "');";
                            cmd.CommandText = @sSql;
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void InsertCheque(List<ItemChequeModel> lstObjData)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {
                        string sSql = "";
                        conn.Open();
                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {
                            string strChequeDate = "";
                            foreach (var item in lstObjData)
                            {
                                strChequeDate = item.ChequeDate.Substring(6, 4) + "-" + item.ChequeDate.Substring(3, 2) + "-" + item.ChequeDate.Substring(0, 2);
                                sSql = " Insert into TB_CHEQUE(ReceiptNumber,ChequeNumber,ChequeName,ChequeDate,BankCode,BankName,ChequeAmount,CashierCheque,ChequeTel,CreateBy,CreateDate) Values (";
                                sSql = sSql + "'" + item.ReceiptNumber + "' ,'" + item.ChequeNumber + "' ,'" + item.ChequeName + "' ,'" + strChequeDate + "' ,";
                                sSql = sSql + "'" + item.BankCode + "' ,'" + item.BankName + "' ," + item.ChequeAmount + " ,'" + item.CashierCheque + "','" + item.ChequeTel + "','" + item.CreateBy + "','" + item.CreateDate + "' )";
                                cmd.CommandText = @sSql;
                                cmd.ExecuteNonQuery();
                            }
                            conn.Close();
                        }
                        tran.Complete();
                        tran.Dispose();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void InsertCA(List<ItemInvoiceModel> lstCA)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";
                        string strPaymentDate = "";
                        string strDueDate = "";
                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {


                            foreach (var item in lstCA)
                            {
                                strPaymentDate = item.PaymentDate != null ? item.PaymentDate.Substring(6, 4) + "-" + item.PaymentDate.Substring(3, 2) + "-" + item.PaymentDate.Substring(0, 2) : "";

                                if (item.PaymentDate != null && item.DueDate.Length == 8) //มาจากรับจริง barcode
                                {
                                    int year = int.Parse(("25" + item.DueDate.Substring(6, 2))) - 543;
                                    strDueDate = item.DueDate != null ? year.ToString() + "-" + item.DueDate.Substring(3, 2) + "-" + item.DueDate.Substring(0, 2) : "";
                                }
                                else
                                {
                                    strDueDate = "";
                                }



                                //  strDueDate = item.DueDate != null ? item.DueDate.Substring(6, 4) + "-" + item.DueDate.Substring(3, 2) + "-" + item.DueDate.Substring(0, 2) : "";

                                sSql = " INSERT INTO TB_INVOICE(ReceiptNumber ,CA ,BillNumber ,PaymentDate ,Time ,TaxID ,PowerUnit , ";
                                sSql = sSql + " CodeType ,strTax ,strRefNo1 , strRefNo2 , strAmount ,  ";
                                sSql = sSql + " DebtCode ,InvoiceNumber ,DueDate ,DayFine ,AmountExVat ,VatPercent ,";
                                sSql = sSql + " AmountVat ,AmountIncVat ,ServiceType ,StopElectricityAmount ,FineFlakedOut , ConnectPower ) VALUES( ";
                                sSql = sSql + " '" + item.ReceiptNumber + "' , '" + item.CA + "' ,'" + item.BillNumber + "' , '" + strPaymentDate + "' , '" + item.Time + "' , '" + item.TaxID + "', " + item.PowerUnit + " , ";

                                sSql = sSql + " '" + item.CodeType + "' , '" + item.strTax + "','" + item.strRefNo1 + "','" + item.strRefNo2 + "','" + item.strAmount + "', ";

                                sSql = sSql + " '" + item.DebtCode + "' , '" + item.InvoiceNumber + "' ,'" + strDueDate + "', " + item.DayFine + " ," + item.AmountExVat + " , " + item.VatPercent + " ,";
                                sSql = sSql + " " + item.AmountVat + " , " + item.AmountIncVat + " ,'" + item.ServiceType + "', " + item.StopElectricityAmount + " ," + item.FineFlakedOut + " ,'" + item.ConnectPower + "' )";
                                cmd.CommandText = @sSql;

                                cmd.ExecuteNonQuery();
                            }


                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void InsertMapCash(List<ItemMapCashModel> lstMapCash)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {
                        string sSql = "";
                        conn.Open();
                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {
                            foreach (var item in lstMapCash)
                            {
                                sSql = " INSERT INTO TB_MAPCASH (InvoiceNumber,Cash,CreateBy,CreateDate) VALUES (";
                                sSql = sSql + " '" + item.InvoiceNumber + "' , " + item.Cash + " , '" + item.CreateBy + "' , '" + item.CreateDate + "')";
                                cmd.CommandText = @sSql;
                                cmd.ExecuteNonQuery();
                            }
                            conn.Close();
                        }
                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void InsertBill(List<ItemBillModel> lstBill)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {

                            foreach (var item in lstBill)
                            {
                                sSql = " INSERT INTO TB_BILL(BillNumber ,BillTotalAmountExVat ,BillTotalVat ,BillTotalAmountIncVat ,BillTotalFine ,BillTotalAll ,Status ,BillType)  VALUES( ";

                                sSql = sSql + " '" + item.BillNumber + "' , " + item.BillTotalAmountExVat + " , " + item.BillTotalVat + " , " + item.BillTotalAmountIncVat + " , " + item.BillTotalFine + " ," + item.BillTotalAll + " , 'A' , '" + item.BillType + "' )";
                                cmd.CommandText = @sSql;

                                cmd.ExecuteNonQuery();
                            }


                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void InsertStopElectric(List<ItemStopElectricModel> lstStopElectric)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {

                            foreach (var item in lstStopElectric)
                            {
                                sSql = " INSERT INTO TB_STOPELECTRIC(ReceiptNumber ,CA ,BillNumber ,AmountExVat ,AmountVat ,AmountIncVat )  VALUES( ";

                                sSql = sSql + " '" + item.ReceiptNumber + "' , '" + item.CA + "' , '" + item.BillNumber + "' , " + item.AmountExVat + " , " + item.AmountVat + " ," + item.AmountIncVat + "  )";
                                cmd.CommandText = @sSql;

                                cmd.ExecuteNonQuery();
                            }


                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void InsertChange(ItemChangeModel objData)
        {
            try
            {


                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {
                            string CreateDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

                            sSql = " INSERT INTO TB_CHANGE (ReceiptNumber ,CashChange ,ChequeChange ,RealCashChange ,RealChequeChange) VALUES (";
                            sSql = sSql + " '" + objData.ReceiptNumber + "' , " + objData.CashChange + " ," + objData.ChequeChange + " ," + objData.RealCashChange + "," + objData.RealChequeChange + ")";
                            cmd.CommandText = @sSql;

                            cmd.ExecuteNonQuery();



                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void InsertUserOffline(ItemUserOfflineModel objData)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {
                        string sSql = "";
                        conn.Open();
                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {
                            string CreateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            sSql = " INSERT INTO TB_USER (UserCode ,UserName ,Password , distId, cashierNo, distName, receiptBranch ) VALUES (";
                            sSql = sSql + " '" + objData.UserCode + "' , '" + objData.UserName + "' ,'" + objData.Password + "'," + objData.distId + "," + objData.cashierNo + ",'" + objData.distName + "','" + objData.receiptBranch + "' )";
                            cmd.CommandText = @sSql;
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public ItemUserOfflineModel GetUserOffline(string UserCode, string Password)
        {
            ItemUserOfflineModel Result = new ItemUserOfflineModel();
            System.Reflection.MethodBase method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                string sSql = @" Select * from TB_USER Where UserCode = '" + UserCode + "' And Password = '" + Password + "'";

                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/
                            Result = new ItemUserOfflineModel();


                            Result = (from DataRow dr in dt.Rows
                                      select new ItemUserOfflineModel()
                                      {
                                          UserCode = dr["UserCode"].ToString(),
                                          UserName = dr["UserName"].ToString(),
                                          Password = dr["Password"].ToString(),
                                          distId = int.Parse(dr["distId"].ToString()),
                                          cashierNo = int.Parse(dr["cashierNo"].ToString()),
                                          distName = dr["distName"].ToString(),
                                          receiptBranch = dr["receiptBranch"].ToString(),

                                      }).FirstOrDefault();
                            /**************************************/
                        }

                        /**************************************/

                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }


                return Result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return Result;
            }
        }
        public void InsertMapCheque(List<ItemMapChequeModel> lstMapCheque)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {

                            foreach (var item in lstMapCheque)
                            {
                                sSql = " INSERT INTO TB_MAPCHEQUE(ChequeNumber ,InvoiceNumber , Amount)  VALUES( ";

                                sSql = sSql + " '" + item.ChequeNumber + "' , '" + item.InvoiceNumber + "' , " + item.Amount + ")";
                                cmd.CommandText = @sSql;

                                cmd.ExecuteNonQuery();
                            }


                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public List<ItemShortTaxInvoice> GetShortBill(string ReceiptNumber)
        {
            List<ItemShortTaxInvoice> lstShortTaxInvoice = new List<ItemShortTaxInvoice>();
            try
            {
                string sSql = @"Select INV.ReceiptNumber ,INV.BillNumber,INV.CA,INV.InvoiceNumber ,INV.PowerUnit ,INV.AmountExVat ,INV.VatPercent
                               ,INV.AmountIncVat ,INV.DayFine ,INV.FineFlakedOut , BI.BillTotalAmountExVat, BI.BillTotalVat
                               ,BI.BillTotalAmountIncVat , BI.BillTotalFine, BI.BillTotalAll
                               From TB_INVOICE INV
                               Left Join TB_BILL BI On INV.BillNumber = BI.BillNumber
                               Where INV.ReceiptNumber = '" + ReceiptNumber + "' And INV.ServiceType = 'A' ";

                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/
                            lstShortTaxInvoice = new List<ItemShortTaxInvoice>();


                            lstShortTaxInvoice = (from DataRow dr in dt.Rows
                                                  select new ItemShortTaxInvoice()
                                                  {

                                                      ReceiptNumber = dr["ReceiptNumber"].ToString(),

                                                      BillNumber = dr["BillNumber"].ToString(),

                                                      CA = dr["CA"].ToString(),

                                                      InvoiceNumber = dr["InvoiceNumber"].ToString(),

                                                      PowerUnit = double.Parse(dr["PowerUnit"].ToString()),

                                                      AmountExVat = double.Parse(dr["AmountExVat"].ToString()),
                                                      VatPercent = double.Parse(dr["VatPercent"].ToString()),
                                                      AmountIncVat = double.Parse(dr["AmountIncVat"].ToString()),
                                                      DayFine = int.Parse(dr["DayFine"].ToString()),
                                                      FineFlakedOut = double.Parse(dr["FineFlakedOut"].ToString()),
                                                      BillTotalAmountExVat = double.Parse(dr["BillTotalAmountExVat"].ToString()),
                                                      BillTotalVat = double.Parse(dr["BillTotalVat"].ToString()),
                                                      BillTotalAmountIncVat = double.Parse(dr["BillTotalAmountIncVat"].ToString()),
                                                      BillTotalFine = double.Parse(dr["BillTotalFine"].ToString()),
                                                      BillTotalAll = double.Parse(dr["BillTotalAll"].ToString()),

                                                  }).ToList();
                            /**************************************/
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }


                return lstShortTaxInvoice;
            }
            catch (Exception ex)
            {
                return lstShortTaxInvoice;
            }
        }
        public string GenListOfflineToService_Old(string strDate)
        {
            string strResult = "";
            List<paymentList> paymentList = new List<paymentList>();
            paymentList payment = new paymentList();
            List<paymentDetailList> paymentDetailList = new List<paymentDetailList>();
            paymentDetailList paymentDetail = new paymentDetailList();

            List<paymentMethodList> paymentMethodList = new List<paymentMethodList>();
            paymentMethodList paymentMethod = new paymentMethodList();

            List<paymentDetailSubList> paymentDetailSubList = new List<paymentDetailSubList>();
            paymentDetailSubList paymentDetailSub = new paymentDetailSubList();

            List<ItemDataToService> lstDataToService = new List<ItemDataToService>();
            string sSql = @"Select OFL.*,INV.*,CHA.CashChange
                            From TB_OFFLINE OFL 
                            Left Join  TB_INVOICE INV ON  OFL.ReceiptNumber = INV.ReceiptNumber
                            Left Join TB_CHANGE CHA On OFL.ReceiptNumber = CHA.ReceiptNumber
                            Where OFL.SendFlag = 'N' 
                            And  Substr( OFL.CreateDate ,0,11) = '" + strDate + @"'";

            string strInvoice = "";
            try
            {
                /**********************************************************/
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        lstDataToService = new List<ItemDataToService>();
                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/



                            lstDataToService = (from DataRow dr in dt.Rows
                                                select new ItemDataToService()
                                                {
                                                    ReceiptNumber = dr["ReceiptNumber"].ToString(),
                                                    CA = dr["CA"].ToString(),
                                                    BillNumber = dr["BillNumber"].ToString(),
                                                    PaymentDate = dr["PaymentDate"].ToString(),
                                                    Time = dr["Time"].ToString(),
                                                    TaxID = dr["TaxID"].ToString(),
                                                    PowerUnit = double.Parse(dr["PowerUnit"].ToString()),
                                                    DebtCode = dr["DebtCode"].ToString(),
                                                    InvoiceNumber = dr["InvoiceNumber"].ToString(),
                                                    DueDate = dr["DueDate"].ToString(),
                                                    DayFine = int.Parse(dr["DayFine"].ToString()),
                                                    AmountExVat = double.Parse(dr["AmountExVat"].ToString()),
                                                    VatPercent = double.Parse(dr["VatPercent"].ToString()),
                                                    AmountVat = double.Parse(dr["AmountVat"].ToString()),
                                                    AmountIncVat = double.Parse(dr["AmountIncVat"].ToString()),
                                                    ServiceType = dr["ServiceType"].ToString(),
                                                    StopElectricityAmount = double.Parse(dr["StopElectricityAmount"].ToString()),
                                                    FineFlakedOut = double.Parse(dr["FineFlakedOut"].ToString()),

                                                    /********************************************************/

                                                    TotalAmountExVat = double.Parse(dr["TotalAmountExVat"].ToString()),
                                                    TotalAmountIncVat = double.Parse(dr["TotalAmountIncVat"].ToString()),
                                                    TotalVat = double.Parse(dr["TotalVat"].ToString()),
                                                    TotalAll = double.Parse(dr["TotalAll"].ToString()),
                                                    CashChange = double.Parse(dr["CashChange"].ToString()),


                                                }).ToList();
                            /**************************************/
                        }

                        /**************************************/
                        strInvoice = string.Join(",", lstDataToService.Select(m => "'" + m.InvoiceNumber + "'").ToArray());


                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }


                /**********************************************************/
                payment = new paymentList();
                //payment.cashierNo = int.Parse(GlobalClass.CashierNo);
                //payment.cashierId = 
                /**********************************************************/

                var lstMapCheque = GetMapCheque(strInvoice);
                var lstMapCash = GetMapCash(strInvoice);




                /**********************************************************/
                //  var arrReceiptNumber = lstDataToService.Select(m => m.ReceiptNumber).Distinct().ToArray();

                var arrReceiptNumber = (from m in lstDataToService
                                        select new
                                        {
                                            VatPercent = m.VatPercent,
                                            ReceiptNumber = m.ReceiptNumber,
                                            TotalAmountExVat = m.TotalAmountExVat,
                                            TotalAmountIncVat = m.TotalAmountIncVat,
                                            TotalVat = m.TotalVat,
                                            TotalAll = m.TotalAll,
                                            CashChange = m.CashChange,

                                        }).Distinct().ToList();



                payment.distId = int.Parse(GlobalClass.BranchId);
                payment.cashierNo = int.Parse(GlobalClass.CashierNo);
                payment.cashierId = int.Parse(GlobalClass.CashierNo);
                payment.userId = int.Parse(GlobalClass.UserCode);
                foreach (var itemReceiptNumber in arrReceiptNumber)
                {

                    paymentDetail = new paymentDetailList();
                    paymentDetail.amount = (decimal)itemReceiptNumber.TotalAmountExVat;
                    paymentDetail.percentVat = (int)itemReceiptNumber.VatPercent;
                    paymentDetail.vat = (decimal)itemReceiptNumber.TotalVat;
                    paymentDetail.totalAmount = (decimal)itemReceiptNumber.TotalAmountIncVat;

                    foreach (var itemInvoice in lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber.ReceiptNumber))
                    {


                        foreach (var itemCash in lstMapCash.Where(m => m.InvoiceNumber == itemInvoice.InvoiceNumber).ToList())
                        {
                            paymentDetailSub = new paymentDetailSubList();
                            paymentDetailSub.paymentMethodId = 1;
                            paymentDetailSub.totalAmount = (decimal)itemCash.Cash;

                            paymentDetailSubList.Add(paymentDetailSub);


                            /*******************/
                            paymentMethod = new paymentMethodList();
                            paymentMethod.paymentMethodId = 1;
                            paymentMethod.cashTotalAmount = (decimal)itemCash.Cash;
                            paymentMethod.cashReceive = (decimal)0.0; /*  ยังขาดส่วนเงินทอน  *************/

                            paymentMethodList.Add(paymentMethod);

                        }

                        foreach (var itemCheque in lstMapCheque.Where(m => m.InvoiceNumber == itemInvoice.InvoiceNumber).ToList())
                        {
                            paymentDetailSub = new paymentDetailSubList();
                            paymentDetailSub.paymentMethodId = 2;
                            paymentDetailSub.totalAmount = (decimal)itemCheque.Amount;
                            paymentDetailSub.chequeNo = itemCheque.ChequeNumber;

                            paymentDetailSubList.Add(paymentDetailSub);

                            /*******************/
                            paymentMethod = new paymentMethodList();
                            paymentMethod.paymentMethodId = 2;
                            paymentMethod.bankBranch = itemCheque.BankCode;
                            paymentMethod.chequeNo = itemCheque.ChequeNumber;
                            paymentMethod.chequeDate = itemCheque.ChequeDate;
                            paymentMethod.chequeAmount = (decimal)itemCheque.Amount;
                            paymentMethod.cashierCheque = "N";

                            paymentMethodList.Add(paymentMethod);
                        }


                        paymentDetail.paymentDetailSubList = paymentDetailSubList;

                        paymentDetailList.Add(paymentDetail);
                    }
                    /**********************************************************/
                    payment.paymentDetailList = paymentDetailList;

                    paymentList.Add(payment);

                }



                strResult = JsonConvert.SerializeObject(paymentList);

                /**********************************************************/
                return strResult;
                // var xx = paymentDetailList;

            }
            catch (Exception ex)
            {
                return strResult;
            }
        }
        public ResultProcessPayment GenListOfflineToService(string strDate, string strReceiptNumber, bool isSendToserver)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            string strResult = "";
            string strTimeLog = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"); 
            MyPaymentListOFF _mypay = new MyPaymentListOFF();
            List<paymentListOFF> paymentList = new List<paymentListOFF>();
            paymentListOFF payment = new paymentListOFF();
            List<paymentDetailList> paymentDetailList = new List<paymentDetailList>();
            paymentDetailListOFF paymentDetail = new paymentDetailListOFF();
            paymentDetailSubList paymentDetailSub = new paymentDetailSubList();
            List<paymentDetailSubList> paymentDetailSubList = new List<paymentDetailSubList>();
            ResultProcessPayment _Paymentout = new ResultProcessPayment();
            try
            {
                List<ItemDataToService> lstDataToService = new List<ItemDataToService>();
                string sSql = @"Select OFL.*,INV.*,CHA.CashChange,CHA.ChequeChange , CHA.RealCashChange,CHA.RealChequeChange
                            From TB_OFFLINE OFL 
                            Left Join  TB_INVOICE INV ON  OFL.ReceiptNumber = INV.ReceiptNumber
                            Left Join TB_CHANGE CHA On OFL.ReceiptNumber = CHA.ReceiptNumber
                            Where OFL.SendFlag = 'W' 
                            And ( INV.ServiceType = 'A' Or INV.ServiceType ISNULL )
                            And  OFL.ReceiptNumber In (" + strReceiptNumber + @")
                            And  Substr( OFL.CreateDate ,0,11) = '" + strDate + @"'";
                string strInvoice = "";
                string strReceipt = "";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        lstDataToService = new List<ItemDataToService>();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            lstDataToService = (from DataRow dr in dt.Rows
                                                select new ItemDataToService()
                                                {
                                                    ReceiptNumber = dr["ReceiptNumber"].ToString(),
                                                    CA = dr["CA"].ToString(),
                                                    BillNumber = dr["BillNumber"].ToString(),
                                                    PaymentDate = dr["PaymentDate"].ToString(),
                                                    Time = dr["Time"].ToString(),
                                                    TaxID = dr["TaxID"].ToString(),
                                                    PowerUnit = double.Parse(dr["PowerUnit"].ToString()),
                                                    DebtCode = dr["DebtCode"].ToString(),
                                                    InvoiceNumber = dr["InvoiceNumber"].ToString(),
                                                    DueDate = dr["DueDate"].ToString(),
                                                    DayFine = int.Parse(dr["DayFine"].ToString()),
                                                    AmountExVat = double.Parse(dr["AmountExVat"].ToString()),
                                                    VatPercent = double.Parse(dr["VatPercent"].ToString()),
                                                    AmountVat = double.Parse(dr["AmountVat"].ToString()),
                                                    AmountIncVat = double.Parse(dr["AmountIncVat"].ToString()),
                                                    ServiceType = dr["ServiceType"].ToString(),
                                                    StopElectricityAmount = double.Parse(dr["StopElectricityAmount"].ToString()),
                                                    FineFlakedOut = double.Parse(dr["FineFlakedOut"].ToString()),
                                                    ConnectPower = dr["ConnectPower"].ToString(),
                                                    TotalAmountExVat = double.Parse(dr["TotalAmountExVat"].ToString()),
                                                    TotalAmountIncVat = double.Parse(dr["TotalAmountIncVat"].ToString()),
                                                    TotalVat = double.Parse(dr["TotalVat"].ToString()),
                                                    TotalAll = double.Parse(dr["TotalAll"].ToString()),
                                                    CashChange = dr["CashChange"].ToString() == "" ? 0.0 : double.Parse(dr["CashChange"].ToString()),
                                                    ChequeChange = dr["ChequeChange"].ToString() == "" ? 0.0 : double.Parse(dr["ChequeChange"].ToString()),
                                                    RealCashChange = dr["RealCashChange"].ToString() == "" ? 0.0 : double.Parse(dr["RealCashChange"].ToString()),
                                                    RealChequeChange = dr["RealChequeChange"].ToString() == "" ? 0.0 : double.Parse(dr["RealChequeChange"].ToString()),
                                                    CashReceive = double.Parse(dr["CashReceive"].ToString()),
                                                    CodeType = dr["CodeType"].ToString(),
                                                    strTax = dr["strTax"].ToString(),
                                                    strRefNo1 = dr["strRefNo1"].ToString(),
                                                    strRefNo2 = dr["strRefNo2"].ToString(),
                                                    strAmount = dr["strAmount"].ToString(),
                                                }).ToList();
                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                var arrReceiptNumber = lstDataToService.Select(m => m.ReceiptNumber).Distinct().ToArray();
                foreach (var itemReceiptNumber in arrReceiptNumber)
                {
                    strInvoice = string.Join(",", lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber).Select(m => "'" + m.InvoiceNumber + "'").ToArray());
                    strReceipt = "'" + itemReceiptNumber + "'";
                    payment = new paymentListOFF();
                    payment.distId = int.Parse(GlobalClass.BranchId);
                    payment.cashierNo = int.Parse(GlobalClass.CashierNo);
                    payment.cashierId = int.Parse(GlobalClass.UserCode);// int.Parse(GlobalClass.CashierNo);
                    payment.userId = int.Parse(GlobalClass.UserCode);// int.Parse(GlobalClass.UserCode);
                    payment.payeeFlag = "N";
                    payment.paymentChannel = 1;
                    payment.subDistId = int.Parse(GlobalClass.BranchId);// int.Parse(GlobalClass.BranchId);
                    payment.paymentDate = strDate;
                    payment.paymentTotalAmount = lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber).Sum(m => (m.AmountIncVat + m.StopElectricityAmount));
                    payment.paymentServer = "OFFLINE";
                    payment.collId = "";
                    double TotalCashChange = lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber).FirstOrDefault().CashChange;
                    double TotalChequeChange = lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber).FirstOrDefault().ChequeChange;
                    payment.paymentChange = TotalCashChange + TotalChequeChange;
                    TotalCashChange = lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber).FirstOrDefault().RealCashChange;
                    TotalChequeChange = lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber).FirstOrDefault().RealChequeChange;
                    payment.paymentRealChange = TotalCashChange + TotalChequeChange;
                    double TotalCashReceive = lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber).FirstOrDefault().CashReceive;
                    double TotalChequeReceive = GetSumCheque(strReceipt);
                    payment.paymentReceive = TotalCashReceive + TotalChequeReceive;
                    var lstMapCheque = GetMapCheque(strInvoice);
                    var lstMapCash = GetMapCash(strInvoice);
                    int indexDebtIdSet = 0;
                    paymentDetailList = new List<paymentDetailList>();
                    foreach (var itemDataToService in lstDataToService.Where(m => m.ReceiptNumber == itemReceiptNumber))
                    {
                        indexDebtIdSet++;
                        paymentDetail = new paymentDetailListOFF();
                        paymentDetail.debtIdSet = new Int64[] { indexDebtIdSet };
                        paymentDetail.amount = (decimal)itemDataToService.AmountExVat;
                        paymentDetail.percentVat = (int)itemDataToService.VatPercent;
                        paymentDetail.vat = (decimal)itemDataToService.AmountVat;
                        paymentDetail.totalAmount = (decimal)itemDataToService.AmountIncVat;
                        paymentDetail.intDay = 0;
                        paymentDetail.intAmount = 0;
                        paymentDetail.debtType = itemDataToService.ServiceType == "A" ? "ELE" : "DIS";
                        paymentDetail.connectPower = itemDataToService.ConnectPower == "" ? null : itemDataToService.ConnectPower; //แก้ด้วย 
                        paymentDetail.ca = itemDataToService.CA;
                        if (itemDataToService.ServiceType == "D") //เป็น "DIS"
                        {
                            paymentDetail.vat = (decimal)2.62;
                            paymentDetail.amount = (decimal)37.38;
                            paymentDetail.totalAmount = (decimal)40.0;
                        }
                        if (itemDataToService.CodeType == "QRCODE")
                        {
                            paymentDetail.qrTax = itemDataToService.strTax;
                            paymentDetail.qrRefNo1 = itemDataToService.strRefNo1;
                            paymentDetail.qrRefNo2 = itemDataToService.strRefNo2;
                            paymentDetail.qrAmount = itemDataToService.strAmount;
                        }
                        else if (itemDataToService.CodeType == "BARCODE")
                        {
                            paymentDetail.barTax = itemDataToService.strTax;
                            paymentDetail.barRefNo1 = itemDataToService.strRefNo1;
                            paymentDetail.barRefNo2 = itemDataToService.strRefNo2;
                            paymentDetail.barAmount = itemDataToService.strAmount;
                        }
                        paymentDetailSubList = new List<paymentDetailSubList>();
                        foreach (var itemCash in lstMapCash.Where(m => m.InvoiceNumber == itemDataToService.InvoiceNumber).ToList())
                        {
                            paymentDetailSub = new paymentDetailSubList();
                            paymentDetailSub.paymentMethodId = 1;
                            paymentDetailSub.totalAmount = (decimal)itemCash.Cash;
                            paymentDetailSubList.Add(paymentDetailSub);

                        }
                        foreach (var itemCheque in lstMapCheque.Where(m => m.InvoiceNumber == itemDataToService.InvoiceNumber).ToList())
                        {
                            paymentDetailSub = new paymentDetailSubList();
                            paymentDetailSub.paymentMethodId = 2;
                            paymentDetailSub.totalAmount = (decimal)itemCheque.Amount;
                            paymentDetailSub.chequeNo = itemCheque.ChequeNumber;
                            paymentDetailSubList.Add(paymentDetailSub);
                        }
                        paymentDetail.paymentDetailSubList = new List<paymentDetailSubList>();
                        paymentDetail.paymentDetailSubList = paymentDetailSubList;
                        paymentDetailList.Add(paymentDetail);
                    }
                    payment.paymentDetailList = paymentDetailList;
                    List<paymentMethodList> paymentMethodList = new List<paymentMethodList>();
                    paymentMethodList = GetOfflinePaymentMethodList(strDate, itemReceiptNumber);
                    payment.paymentMethodList = paymentMethodList;
                    paymentList.Add(payment);
                }
                _mypay.paymentList = paymentList;
                _Paymentout = new ResultProcessPayment();
                if (isSendToserver)
                {
                    GetDataReportController getdata = new GetDataReportController();
                    strResult = JsonConvert.SerializeObject(_mypay);
                    meaLog.WriteData("OFFLINE " + strTimeLog + " : JSON : " + strResult, method, LogLevel.Debug);
                    _Paymentout = getdata.ReturnProcessPaymentOFF(_mypay);
                    strResult = JsonConvert.SerializeObject(_Paymentout);
                    meaLog.WriteData("OFFLINE " + strTimeLog + " : Result : " + strResult, method, LogLevel.Debug);
                }
                else
                {
                    strResult = JsonConvert.SerializeObject(_mypay);
                    meaLog.WriteData("OFFLINE " + strTimeLog + " : JSON : " + strResult, method, LogLevel.Debug);
                    _Paymentout.OutputProcess = new ClassOutputProcessPayment();
                    _Paymentout.OutputProcess.result_code = "SUCCESS";
                    _Paymentout.OutputProcess.result_message = strResult;
                }
                if (isSendToserver)
                {
                    strReceipt = string.Join(",", lstDataToService.Select(m => "'" + m.ReceiptNumber + "'").Distinct().ToArray());
                    if (_Paymentout.OutputProcess.result_message == "SUCCESS")
                        UpdateSendDataByReceiptNumber(strReceipt);
                }
                return _Paymentout;
            }
            catch (Exception ex)
            {
                strResult = ex.Message + (ex.InnerException != null ? " InnerException : " + ex.InnerException.Message : "");
                meaLog.WriteData("OFFLINE " + strTimeLog + " : Error : " + strResult, method, LogLevel.Debug);
                _Paymentout.OutputProcess.result_code = "ERROR";

                return _Paymentout;
            }
        }
        public List<ItemDataToService> GetDataToService(string strDate)
        {
            List<ItemDataToService> lstDataToService = new List<ItemDataToService>();
            try
            {
                string sSql = @"Select OFL.*,INV.*,CHA.CashChange,CHA.ChequeChange , CHA.RealCashChange,CHA.RealChequeChange
                            From TB_OFFLINE OFL 
                            Left Join  TB_INVOICE INV ON  OFL.ReceiptNumber = INV.ReceiptNumber
                            Left Join TB_CHANGE CHA On OFL.ReceiptNumber = CHA.ReceiptNumber
                            Where OFL.SendFlag = 'N' 
                            And  Substr( OFL.CreateDate ,0,11) = '" + strDate + @"'";


                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        lstDataToService = new List<ItemDataToService>();
                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/



                            lstDataToService = (from DataRow dr in dt.Rows
                                                select new ItemDataToService()
                                                {
                                                    ReceiptNumber = dr["ReceiptNumber"].ToString(),
                                                    CA = dr["CA"].ToString(),
                                                    BillNumber = dr["BillNumber"].ToString(),
                                                    PaymentDate = dr["PaymentDate"].ToString(),
                                                    Time = dr["Time"].ToString(),
                                                    TaxID = dr["TaxID"].ToString(),
                                                    PowerUnit = double.Parse(dr["PowerUnit"].ToString()),
                                                    DebtCode = dr["DebtCode"].ToString(),
                                                    InvoiceNumber = dr["InvoiceNumber"].ToString(),
                                                    DueDate = dr["DueDate"].ToString(),
                                                    DayFine = int.Parse(dr["DayFine"].ToString()),
                                                    AmountExVat = double.Parse(dr["AmountExVat"].ToString()),
                                                    VatPercent = double.Parse(dr["VatPercent"].ToString()),
                                                    AmountVat = double.Parse(dr["AmountVat"].ToString()),
                                                    AmountIncVat = double.Parse(dr["AmountIncVat"].ToString()),
                                                    ServiceType = dr["ServiceType"].ToString(),
                                                    StopElectricityAmount = double.Parse(dr["StopElectricityAmount"].ToString()),
                                                    FineFlakedOut = double.Parse(dr["FineFlakedOut"].ToString()),
                                                    ConnectPower = dr["ConnectPower"].ToString(),
                                                    /********************************************************/

                                                    TotalAmountExVat = double.Parse(dr["TotalAmountExVat"].ToString()),
                                                    TotalAmountIncVat = double.Parse(dr["TotalAmountIncVat"].ToString()),
                                                    TotalVat = double.Parse(dr["TotalVat"].ToString()),
                                                    TotalAll = double.Parse(dr["TotalAll"].ToString()),
                                                    CashChange = dr["CashChange"].ToString() == "" ? 0.0 : double.Parse(dr["CashChange"].ToString()),
                                                    ChequeChange = dr["ChequeChange"].ToString() == "" ? 0.0 : double.Parse(dr["ChequeChange"].ToString()),
                                                    RealCashChange = dr["RealCashChange"].ToString() == "" ? 0.0 : double.Parse(dr["RealCashChange"].ToString()),
                                                    RealChequeChange = dr["RealChequeChange"].ToString() == "" ? 0.0 : double.Parse(dr["RealChequeChange"].ToString()),
                                                    CashReceive = double.Parse(dr["CashReceive"].ToString()),

                                                    CodeType = dr["CodeType"].ToString(),
                                                    strTax = dr["strTax"].ToString(),
                                                    strRefNo1 = dr["strRefNo1"].ToString(),
                                                    strRefNo2 = dr["strRefNo2"].ToString(),
                                                    strAmount = dr["strAmount"].ToString(),


                                                }).ToList();
                            /**************************************/
                        }

                        /**************************************/


                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }

                return lstDataToService;
            }
            catch (Exception ex)
            {
                return lstDataToService;
            }
        }
        public List<paymentMethodList> GetOfflinePaymentMethodList(string strDate, string ReceiptNumber)
        {
            List<ItemOfflineModel> lstItemOfflineModel = new List<ItemOfflineModel>();
            List<paymentMethodList> paymentMethodList = new List<paymentMethodList>();
            paymentMethodList paymentMethod = new paymentMethodList();

            List<ItemChequeModel> lstItemChequeModel = new List<ItemChequeModel>();
            try
            {
                string sSql = @" Select OFL.* From TB_OFFLINE OFL Where OFL.SendFlag = 'W' And  OFL.ReceiptNumber = '" + ReceiptNumber + @"' And Substr( OFL.CreateDate ,0,11) = '" + strDate + @"' ";

                string strReceipt = "";


                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        lstItemOfflineModel = new List<ItemOfflineModel>();
                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/



                            lstItemOfflineModel = (from DataRow dr in dt.Rows
                                                   select new ItemOfflineModel()
                                                   {
                                                       ReceiptNumber = dr["ReceiptNumber"].ToString(),
                                                       CashReceive = double.Parse(dr["CashReceive"].ToString()),
                                                       TotalAll = decimal.Parse(dr["TotalAll"].ToString()),

                                                   }).ToList();
                            /**************************************/
                        }

                        /**************************************/

                        strReceipt = string.Join(",", lstItemOfflineModel.Select(m => "'" + m.ReceiptNumber + "'").Distinct().ToArray());

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }

                ////**************************************************
                ///

                if (lstItemOfflineModel.Count > 0)
                {

                    if (lstItemOfflineModel.FirstOrDefault().CashReceive > 0.0)
                    {
                        paymentMethod = new paymentMethodList();
                        paymentMethod.paymentMethodId = 1;
                        paymentMethod.cashTotalAmount = (decimal)lstItemOfflineModel.Sum(m => m.TotalAll);
                        paymentMethod.cashReceive = (decimal)lstItemOfflineModel.Sum(m => m.CashReceive);
                        paymentMethodList.Add(paymentMethod);
                    }

                }


                ////**************************************************
                ///
                sSql = @"Select CHE.* From TB_CHEQUE CHE Where CHE.ReceiptNumber in (" + strReceipt + @")";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        lstItemOfflineModel = new List<ItemOfflineModel>();
                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/



                            lstItemChequeModel = (from DataRow dr in dt.Rows
                                                  select new ItemChequeModel()
                                                  {
                                                      ReceiptNumber = dr["ReceiptNumber"].ToString(),
                                                      ChequeNumber = dr["ChequeNumber"].ToString(),
                                                      ChequeName = dr["ChequeName"].ToString(),
                                                      ChequeDate = dr["ChequeDate"].ToString(),
                                                      BankCode = dr["BankCode"].ToString(),
                                                      BankName = dr["BankName"].ToString(),
                                                      ChequeAmount = double.Parse(dr["ChequeAmount"].ToString()),
                                                      CashierCheque = dr["CashierCheque"].ToString(),
                                                      ChequeTel = dr["ChequeTel"].ToString(),
                                                  }).ToList();
                            /**************************************/
                        }

                        /**************************************/



                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                ////**************************************************


                foreach (var item in lstItemChequeModel)
                {
                    paymentMethod = new paymentMethodList();
                    paymentMethod.paymentMethodId = 2;
                    paymentMethod.bankBranch = item.BankCode;
                    paymentMethod.chequeNo = item.ChequeNumber;
                    paymentMethod.chequeOwner = item.ChequeName;
                    paymentMethod.chequeDate = item.ChequeDate;
                    paymentMethod.chequeAmount = (decimal)item.ChequeAmount;
                    paymentMethod.chequeTel = item.ChequeTel;

                    paymentMethodList.Add(paymentMethod);
                }

                ////------------------------------------------------------------
                decimal sumCheque = paymentMethodList.Where(m => m.paymentMethodId == 2).Sum(m => m.chequeAmount);
                paymentMethodList.Where(m => m.paymentMethodId == 1).ToList().ForEach(m => m.cashTotalAmount = m.cashTotalAmount - sumCheque);
                ////**************************************************
                return paymentMethodList;

            }
            catch (Exception ex)
            {
                return paymentMethodList;
            }
        }
        public double GetSumCheque(string strReceipt)
        {
            double result = 0.0;
            try
            {
                string sSql = @"Select Sum(CHE.ChequeAmount) TotalCheque  
                             From TB_CHEQUE CHE
                             Where CHE.ReceiptNumber in (" + strReceipt + @")";


                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/



                            var objResult = (from DataRow dr in dt.Rows
                                             select new
                                             {
                                                 TotalCheque = double.Parse(dr["TotalCheque"].ToString()),


                                             }).ToList();


                            result = objResult.FirstOrDefault().TotalCheque;
                            /**************************************/
                        }

                        /**************************************/


                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }


                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public List<ItemMapChequeModel> GetMapCheque(string strInvoiceNumber)
        {
            List<ItemMapChequeModel> lstMapCheque = new List<ItemMapChequeModel>();
            try
            {
                string sSql = @"Select MC.InvoiceNumber,MC.ChequeNumber,MC.Amount , 
                                CH.ChequeName,CH.ChequeDate,CH.CashierCheque,CH.BankCode
                                From TB_MAPCHEQUE MC 
                                Inner Join TB_CHEQUE CH On MC.ChequeNumber = CH.ChequeNumber
                                Where MC.InvoiceNumber In (" + strInvoiceNumber + ")";

                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        lstMapCheque = new List<ItemMapChequeModel>();
                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/



                            lstMapCheque = (from DataRow dr in dt.Rows
                                            select new ItemMapChequeModel()
                                            {


                                                /********************************************************/
                                                InvoiceNumber = dr["InvoiceNumber"].ToString(),
                                                ChequeNumber = dr["ChequeNumber"].ToString(),
                                                Amount = dr["Amount"] == null || dr["Amount"].ToString() == "" ? 0.0 : double.Parse(dr["Amount"].ToString()),

                                                ChequeName = dr["ChequeName"].ToString(),
                                                ChequeDate = dr["ChequeDate"].ToString(),
                                                BankCode = dr["BankCode"].ToString(),
                                                CashierCheque = dr["CashierCheque"].ToString()

                                            }).ToList();
                            /**************************************/
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }

                return lstMapCheque;
            }
            catch (Exception ex)
            {
                return lstMapCheque;
            }
        }
        public List<ItemMapCashModel> GetMapCash(string strInvoiceNumber)
        {
            List<ItemMapCashModel> lstMapCash = new List<ItemMapCashModel>();
            try
            {
                string sSql = @"Select * From TB_MAPCASH Where InvoiceNumber In (" + strInvoiceNumber + ")";

                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        lstMapCash = new List<ItemMapCashModel>();
                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/



                            lstMapCash = (from DataRow dr in dt.Rows
                                          select new ItemMapCashModel()
                                          {


                                              /********************************************************/
                                              InvoiceNumber = dr["InvoiceNumber"].ToString(),

                                              Cash = dr["Cash"] == null || dr["Cash"].ToString() == "" ? 0.0 : double.Parse(dr["Cash"].ToString())


                                          }).ToList();
                            /**************************************/
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }

                }
                return lstMapCash;
            }
            catch (Exception ex)
            {
                return lstMapCash;
            }
        }
        public List<ItemIP> GetLstIP(string strDate, string strDateDisplay)
        {
            List<ItemIP> lstIP = new List<ItemIP>();
            try
            {
                //string sSql = @" Select TIP.*,SED.Data,SED.CreateDate,SED.SendToServerDate,Substr(SED.SendToServerDate,9,2) || '/' || Substr(SED.SendToServerDate,6,2) || '/' || Substr(SED.SendToServerDate,1,4)  as SendToServerDateDisplay  From TB_IP TIP
                //                 Left Join  ( Select * From TB_SENDDATA Where CreateDate = '" + strDate + @"') SED On TIP.IP = SED.IP  ";


                string sSql = @"
                              Select SED.*,Substr(SED.SendToServerDate,9,2) || '/' || Substr(SED.SendToServerDate,6,2) || '/' || Substr(SED.SendToServerDate,1,4)  as SendToServerDateDisplay 
                              From TB_SENDDATA SED Where SED.CreateDate = '" + strDate + @"'";


                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        lstIP = new List<ItemIP>();
                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/



                            lstIP = (from DataRow dr in dt.Rows
                                     select new ItemIP()
                                     {


                                         /********************************************************/
                                         IP = dr["IP"].ToString(),
                                         //  Port = dr["Port"].ToString(),


                                         //  BranchCode = dr["BranchCode"].ToString(),
                                         //  CashierNo = dr["CashierNo"].ToString(),
                                         Date = strDate,
                                         //  DateDisplay = strDateDisplay,
                                         SendToServerDate = dr["SendToServerDate"].ToString(),
                                         SendToServerDateDisplay = (dr["SendToServerDate"].ToString() == "" ? "" : dr["SendToServerDateDisplay"].ToString()),
                                         Data = dr["Data"].ToString(),
                                         //  hasData = dr["SendToServerDate"].ToString() == "" && dr["Data"].ToString() != "" ? true : false
                                     }).ToList();
                            /**************************************/
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }

                return lstIP;
            }
            catch (Exception ex)
            {
                return lstIP;
            }
        }
        public List<ItemInvoiceModel> GetBillCancelDetail(string BillNumber)
        {
            List<ItemInvoiceModel> lstItemInvoice = new List<ItemInvoiceModel>();
            try
            {
                string sSql = @" Select INV.* From TB_INVOICE INV
                                 Where INV.BillNumber = '" + BillNumber + @"'  ";


                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        lstItemInvoice = new List<ItemInvoiceModel>();
                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();

                            /**************************************/



                            lstItemInvoice = (from DataRow dr in dt.Rows
                                              select new ItemInvoiceModel()
                                              {

                                                  /********************************************************/

                                                  CA = dr["CA"].ToString(),
                                                  DueDate = dr["DueDate"].ToString(),
                                                  FineFlakedOut = double.Parse(dr["FineFlakedOut"].ToString()),
                                                  StopElectricityAmount = double.Parse(dr["StopElectricityAmount"].ToString()),
                                                  AmountIncVat = double.Parse(dr["AmountIncVat"].ToString()),
                                                  PowerUnit = double.Parse(dr["PowerUnit"].ToString()),
                                                  InvoiceNumber = dr["InvoiceNumber"].ToString()


                                              }).ToList();
                            /**************************************/
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }

                return lstItemInvoice;

            }
            catch (Exception ex)
            {
                return lstItemInvoice;

            }
        }
        public List<ItemOfflineModel> GetSearchOffline(string strDate)
        {
            List<ItemOfflineModel> lstResult = new List<ItemOfflineModel>();
            try
            {
                string sSql = @"Select OFL.* from TB_OFFLINE OFL  Where Substr( OFL.CreateDate ,0,11) = '" + strDate + @"'";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            lstResult = new List<ItemOfflineModel>();
                            lstResult = (from DataRow dr in dt.Rows
                                         select new ItemOfflineModel()
                                         {
                                             Selected = false,
                                             ReceiptNumber = dr["ReceiptNumber"].ToString(),
                                             TotalAll = decimal.Parse(dr["TotalAll"].ToString()),
                                             SendFlag = dr["SendFlag"].ToString() == "N" ? "ยังไม่ส่ง" : "ส่งแล้ว",
                                             Tel = dr["Tel"].ToString()
                                         }).ToList();
                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return lstResult;
            }
            catch (Exception ex)
            {
                return lstResult;
            }
        }
        public List<ItemOfflineModel> SearchDataOffline(string strDate, string status)
        {
            List<ItemOfflineModel> lstResult = new List<ItemOfflineModel>();
            try
            {
                string sSql = @" Select off.CreateDate as Datetime,Inv.CA ,Inv.InvoiceNumber,Off.ReceiptNumber,off.TotalAll as amount,off.SendFlag as status
                                 From TB_OFFLINE Off INNER JOIN TB_INVOICE Inv ON off.ReceiptNumber = Inv.ReceiptNumber Where Substr( off.CreateDate ,0,11) = '" + strDate + "' AND off.SendFlag = '" + status + "'";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            lstResult = new List<ItemOfflineModel>();
                            foreach (DataRow dr in dt.Rows)
                            {
                                ItemOfflineModel item = new ItemOfflineModel();
                                DateTime dtt = Convert.ToDateTime(dr["Datetime"].ToString());
                                item.CreateDate = dtt.ToString("dd/MM/yyyy");
                                item.CA = dr["CA"].ToString();
                                item.InvoiceNumber = dr["InvoiceNumber"].ToString();
                                item.ReceiptNumber = dr["ReceiptNumber"].ToString();
                                item.TotalAll = decimal.Parse(dr["amount"].ToString());
                                string strStatus = "";
                                if (dr["status"].ToString() == "W")
                                {
                                    strStatus = "รอนำส่ง";
                                    item.Selected = true;
                                }                                   
                                else if(dr["status"].ToString() == "N")
                                {
                                    strStatus = "นำส่งแล้ว";
                                    item.Selected = false;
                                }                                   
                                else
                                {
                                    strStatus = "นำส่งไม่ผ่าน";
                                    item.Selected = true;
                                }
                                item.SendFlag = strStatus;
                                lstResult.Add(item);
                            }                          
                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return lstResult;
            }
            catch (Exception ex)
            {
                return lstResult;
            }
        }
        public List<ItemBillCancelModel> GetAllBill(string strDate)
        {
            List<ItemBillCancelModel> LstBillModel = new List<ItemBillCancelModel>();
            try
            {
                //string sSql = @"Select INV.*,BIL.Remark , BIL.Status From TB_INVOICE INV
                //              Inner Join TB_BILL BIL On INV.BillNumber = BIL.BillNumber
                //              Where INV.ReceiptNumber in (Select OFL.ReceiptNumber From TB_OFFLINE OFL Where Substr(OFL.CreateDate ,0,11) = '" + strDate + "')";

                //string sSql = @" 
                //                 Select BIL.* from TB_BILL BIL 
                //                 Where BIL.BillNumber In (

                //                                         Select INV.BillNumber From TB_INVOICE INV Where INV.ReceiptNumber In (

                //                                               Select OFL.ReceiptNumber From TB_OFFLINE OFL Where Substr(OFL.CreateDate ,0,11) = '" + strDate + @"'
                //                                            )

                //                 )
                //                union 
                //                 Select BIL.* from TB_BILL BIL 
                //                 Where BIL.BillNumber In ( Select STO.BillNumber From TB_STOPELECTRIC STO Where STO.ReceiptNumber In (Select OFL.ReceiptNumber From TB_OFFLINE OFL Where Substr(OFL.CreateDate ,0,11) = '" + strDate + @"')

                //                 )";

                string sSql = @" 
                                 Select BIL.* from TB_BILL BIL 
                                 Where BIL.BillNumber In (

                                                         Select INV.BillNumber From TB_INVOICE INV Where INV.ReceiptNumber In (

                                                               Select OFL.ReceiptNumber From TB_OFFLINE OFL Where Substr(OFL.CreateDate ,0,11) = '" + strDate + @"'
                                                            )

                                 )";

                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {


                        DataTable dt = new DataTable();

                        m_dbConnection.Open();

                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sSql;

                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();


                            /**************************************/
                            LstBillModel = new List<ItemBillCancelModel>();


                            LstBillModel = (from DataRow dr in dt.Rows
                                            select new ItemBillCancelModel()
                                            {
                                                Selected = (dr["Status"].ToString() == "I" ? true : false),
                                                // CA = dr["CA"].ToString(),
                                                // InvoiceNumber = dr["InvoiceNumber"].ToString(),
                                                AmountIncVat = double.Parse(dr["BillTotalAll"].ToString()),
                                                BillNumber = dr["BillNumber"].ToString(),
                                                Remark = dr["Remark"].ToString(),

                                                Status = dr["Status"].ToString(),


                                            }).ToList();
                            /**************************************/
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }

                return LstBillModel;
            }
            catch (Exception ex)
            {
                return LstBillModel;
            }
        }
        public bool UpdateBillCancel(List<ItemBillCancelModel> LstItem)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();
                        string strStatus = "";
                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {


                            foreach (var item in LstItem)
                            {
                                strStatus = item.Selected ? "I" : "A";

                                sSql = "  Update TB_BILL Set Status = '" + strStatus + "' , Remark = '" + item.Remark + "' Where BillNumber = '" + item.BillNumber + "' ;";
                                sSql = sSql + "Update TB_INVOICE Set Status = '" + strStatus + "'   Where BillNumber = '" + item.BillNumber + "' ;";


                                cmd.CommandText = @sSql;

                                cmd.ExecuteNonQuery();
                            }


                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable();
            try
            {
                dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
            catch (Exception ex)
            {
                return dataTable;
            }

        }
        public void AddNewData()
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {

                    try
                    {
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                    }
                }



            }
            catch (Exception ex)
            {

            }
        }
        public double Fraction(double data)
        {
            var arrData = data.ToString().Split('.');
            int number = 0;
            if (arrData.Length > 1)
            {
                number = int.Parse(arrData[1]);
            }


            double Satang = 0;
            if (number <= 12)
                Satang = 0.00;
            else if (number <= 37)
                Satang = 0.25;
            else if (number <= 62)
                Satang = 0.50;
            else if (number <= 87)
                Satang = 0.75;
            else
                Satang = 1;

            data = double.Parse(arrData[0]);
            Satang = data + Satang;
            return Satang;
        }
        #endregion
        #region AdminReceipt
        public void WaitRequest() // For Client
        {
            //string SERVER_IP = ConfigurationManager.AppSettings["IP-SERVER-ADDRESS"].ToString();
            //int PORT_SEND_DATA = int.Parse(ConfigurationManager.AppSettings["SERVER-PORT"].ToString());


            try
            {
                string IP = GetLocalIPAddress();
                //  string IP = "127.0.0.1";
                int PORT_NO = 5000;

                IPAddress localAdd = IPAddress.Parse(IP);
                TcpListener listener = new TcpListener(localAdd, PORT_NO);
                listener.Start();

                while (!listener.Pending())
                {
                    Thread.Sleep(10);
                }


                TcpClient client = listener.AcceptTcpClient();
                NetworkStream nwStream = client.GetStream();

                /***********************************/
                byte[] bufferClient = new byte[client.ReceiveBufferSize];
                int bytesRead = nwStream.Read(bufferClient, 0, client.ReceiveBufferSize);
                string dataReceived = Encoding.Unicode.GetString(bufferClient, 0, bytesRead);
                /***********************************/



                /////////////////////////////////

                var LstSearchOffline = GetSearchOffline(dataReceived);
                string strReceiptNumber = string.Join(",", LstSearchOffline.Select(m => "'" + m.ReceiptNumber + "'").Distinct().ToArray());

                ////////////////////////////////

                var objResult = GenListOfflineToService(dataReceived, strReceiptNumber, false);
                string strData = "";
                //if (objResult.OutputProcess.result_code == "SUCCESS")
                //{
                //    strData = objResult.OutputProcess.result_message;
                //}


                strData = JsonConvert.SerializeObject(objResult);
                //string strData = GenListOfflineToService(dataReceived);
                byte[] bufferSend = Encoding.Unicode.GetBytes(strData);

                nwStream.Write(bufferSend, 0, bufferSend.Length);

                client.Close();
                listener.Stop();


                /*****************************************/
                // this.WaitRequest();




            }
            catch (Exception ex)
            {

            }
        }
        public string SendRequest(string CLIENT_IP, int PORT_NO, string STR_DATE)  // For Server Loop Call 
        {
            string strResponse = "";
            try
            {
                TcpClient client = new TcpClient(CLIENT_IP, PORT_NO);
                NetworkStream nwStream = client.GetStream();
                byte[] bytesToSend = ASCIIEncoding.Unicode.GetBytes(STR_DATE);

                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                byte[] bytesToRead = new byte[client.ReceiveBufferSize];
                int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                strResponse = Encoding.Unicode.GetString(bytesToRead, 0, bytesRead);
                client.Close();
                return strResponse;

            }

            catch (Exception ex)
            {
                return strResponse;
            }
        }
        public async Task<string> Test_SendRequest(string CLIENT_IP, int PORT_NO)
        {


            return await Task.Run(() =>
            {

                string strResponse = "";
                try
                {
                    TcpClient client = new TcpClient(CLIENT_IP, PORT_NO);
                    NetworkStream nwStream = client.GetStream();
                    byte[] bytesToSend = ASCIIEncoding.Unicode.GetBytes("");

                    nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                    byte[] bytesToRead = new byte[client.ReceiveBufferSize];
                    int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                    strResponse = Encoding.Unicode.GetString(bytesToRead, 0, bytesRead);
                    client.Close();
                    return strResponse;

                }
                catch (Exception ex)
                {
                    return strResponse;
                }

            });
        }
        public static string GetLocalIPAddress()
        {

            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }

        }
        public void InsertSendData(ItemSendData objData)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {


                            sSql = " INSERT INTO TB_SENDDATA (IP ,CreateDate ,Data ,SendToServerDate ,Status ,Branch , CashierDesk ) VALUES ( ";

                            sSql = sSql + " '" + objData.IP + "' , '" + objData.CreateDate + "'  , '" + objData.Data + "' , '" + objData.SendToServerDate + "' , '" + objData.Status + "' , '" + objData.Branch + "' , '" + objData.CashierDesk + "') ";

                            cmd.CommandText = @sSql;

                            cmd.ExecuteNonQuery();

                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void UpdateSendDataByReceiptNumber(string strReceiptNumber)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {
                            sSql = " Update TB_OFFLINE Set  SendFlag = '" + "Y" + "' ";
                            sSql = sSql + " Where ReceiptNumber In (" + strReceiptNumber + ")";


                            cmd.CommandText = @sSql;

                            cmd.ExecuteNonQuery();

                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void UpdateSendData(ItemSendData objData)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {
                            sSql = " Update TB_SENDDATA Set  Data = '" + objData.Data + "' ";
                            sSql = sSql + " Where IP = '" + objData.IP + "' And CreateDate = '" + objData.CreateDate + "'";


                            cmd.CommandText = @sSql;

                            cmd.ExecuteNonQuery();

                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void ClearData()
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection conn = new SQLiteConnection(DBPath);
                    try
                    {

                        string sSql = "";

                        conn.Open();

                        using (SQLiteCommand cmd = conn.CreateCommand())
                        {
                            sSql = @" Delete From TB_BANK;
                            Delete From TB_BILL;
                            Delete From TB_CHANGE;
                            Delete From TB_CHEQUE;
                            Delete From TB_INVOICE;
                            Delete From TB_MAPCASH;
                            Delete From TB_MAPCHEQUE;
                            Delete From TB_OFFLINE;
                            Delete From TB_SENDDATA;
                            Delete From TB_STOPELECTRIC; ";


                            cmd.CommandText = @sSql;

                            cmd.ExecuteNonQuery();

                            conn.Close();
                        }

                        /**************************************/

                        /**************************************/
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        tran.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region สำหรับส่งเงิน
        //public string SendDataToServer(string strData)
        //{
        //    string strResult = "";
        //    List<ItemShortTaxInvoice> lstShortTaxInvoice = new List<ItemShortTaxInvoice>();
        //    try
        //    {
        //        string sSql = @"Select INV.ReceiptNumber ,INV.BillNumber,INV.CA,INV.InvoiceNumber ,INV.PowerUnit ,INV.AmountExVat ,INV.VatPercent
        //                       ,INV.AmountIncVat ,INV.DayFine ,INV.FineFlakedOut , BI.BillTotalAmountExVat, BI.BillTotalVat
        //                       ,BI.BillTotalAmountIncVat , BI.BillTotalFine, BI.BillTotalAll
        //                       From TB_INVOICE INV
        //                       Left Join TB_BILL BI On INV.BillNumber = BI.BillNumber";


        //        using (TransactionScope tran = new TransactionScope())
        //        {
        //            SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
        //            try
        //            {


        //                DataTable dt = new DataTable();

        //                m_dbConnection.Open();

        //                using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
        //                {
        //                    fmd.CommandText = sSql;

        //                    SQLiteDataReader reader = fmd.ExecuteReader();
        //                    dt.Load(reader);
        //                    reader.Close();
        //                    m_dbConnection.Close();


        //                    /**************************************/
        //                    lstShortTaxInvoice = new List<ItemShortTaxInvoice>();


        //                    lstShortTaxInvoice = (from DataRow dr in dt.Rows
        //                                          select new ItemShortTaxInvoice()
        //                                          {

        //                                              ReceiptNumber = dr["ReceiptNumber"].ToString(),

        //                                              BillNumber = dr["BillNumber"].ToString(),

        //                                              CA = dr["CA"].ToString(),

        //                                              InvoiceNumber = dr["InvoiceNumber"].ToString(),

        //                                              PowerUnit = double.Parse(dr["PowerUnit"].ToString()),

        //                                              AmountExVat = double.Parse(dr["AmountExVat"].ToString()),
        //                                              VatPercent = double.Parse(dr["VatPercent"].ToString()),
        //                                              AmountIncVat = double.Parse(dr["AmountIncVat"].ToString()),
        //                                              DayFine = int.Parse(dr["DayFine"].ToString()),
        //                                              FineFlakedOut = double.Parse(dr["FineFlakedOut"].ToString()),
        //                                              BillTotalAmountExVat = double.Parse(dr["BillTotalAmountExVat"].ToString()),
        //                                              BillTotalVat = double.Parse(dr["BillTotalVat"].ToString()),
        //                                              BillTotalAmountIncVat = double.Parse(dr["BillTotalAmountIncVat"].ToString()),
        //                                              BillTotalFine = double.Parse(dr["BillTotalFine"].ToString()),
        //                                              BillTotalAll = double.Parse(dr["BillTotalAll"].ToString()),

        //                                          }).ToList();
        //                    /**************************************/
        //                }

        //                /**************************************/

        //                /**************************************/
        //                tran.Complete();
        //            }
        //            catch (Exception ex)
        //            {
        //                m_dbConnection.Close();
        //                tran.Dispose();
        //            }
        //        }

        //         strResult = JsonConvert.SerializeObject(lstShortTaxInvoice);

        //        return strResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        return strResult;
        //    }
        //}
        public void SendDataToService(string strData)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region PrintReport_Remittance
        public List<ItemBillCancelModel> SelectDataReport()
        {
            List<ItemBillCancelModel> lst = new List<ItemBillCancelModel>();

            return lst;
        }
        #endregion
        #region RreportRemittance
        public ClassReportOffline SelectReportOffline(ClassPayInBalance cls)
        {
            ClassReportOffline _result = new ClassReportOffline();
            System.Reflection.MethodBase method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                DataTable dtUser = new DataTable();
                DataTable dtCash = new DataTable();
                DataTable dtCheque = new DataTable();
                #region User
                string usql = @"Select * From TB_USER user Where user.UserCode = '" + cls.cashierId + "'";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = usql;
                            SQLiteDataReader reader1 = fmd.ExecuteReader();
                            dtUser.Load(reader1);
                            reader1.Close();
                            m_dbConnection.Close();
                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                #endregion
                #region Cash
                string cashsql = @" SELECT *,SUM(cash.Cash) as TotalAmountCash FROM TB_MAPCASH cash  WHERE cash.CreateDate = '" + cls.payInDateStr + "' AND cash.CreateBy ='" + cls.cashierId + "'";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = cashsql;
                            SQLiteDataReader reader1 = fmd.ExecuteReader();
                            dtCash.Load(reader1);
                            reader1.Close();
                            m_dbConnection.Close();
                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                #endregion
                #region User
                string chequesql = @"SELECT *,Sum(cheque.ChequeAmount) as TotalAmountCheque FROM TB_CHEQUE cheque WHERE cheque.CreateDate = '" + cls.payInDateStr + "' AND cheque.CreateBy ='" + cls.cashierId + "'";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = chequesql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dtCheque.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                #endregion
                if (dtUser != null)
                {
                    _result.UserCode = dtUser.Rows[0]["UserCode"].ToString();
                    _result.UserName = dtUser.Rows[0]["UserName"].ToString();
                    _result.DistId = dtUser.Rows[0]["distId"].ToString();
                    _result.DistName = dtUser.Rows[0]["distName"].ToString();
                    _result.CashierNo = dtUser.Rows[0]["cashierNo"].ToString();
                    _result.ReceiptBranch = dtUser.Rows[0]["receiptBranch"].ToString();
                    if (dtCash != null)
                        _result.TotalAmountCash = (dtCash.Rows[0]["TotalAmountCash"].ToString()) != "" ? dtCash.Rows[0]["TotalAmountCash"].ToString() : "0";
                    else
                        _result.TotalAmountCash = "0";
                    if (dtCheque != null)
                        _result.TotalAmountCheque = (dtCheque.Rows[0]["TotalAmountCheque"].ToString()) != "" ? dtCheque.Rows[0]["TotalAmountCheque"].ToString() : "0";
                    else
                        _result.TotalAmountCheque = "0";
                }
                return _result;
            }
            catch (Exception ex)
            {
                return _result;
            }
        }
        public List<ItemChequeModel> SelectDataChequeOffline(ClassPayInBalance cls)
        {
            List<ItemChequeModel> _result = new List<ItemChequeModel>();
            System.Reflection.MethodBase method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                string sql = @" SELECT * FROM TB_CHEQUE WHERE CreateDate = '" + cls.payInDateStr + "' AND CreateBy ='" + cls.cashierId + "'";
                using (TransactionScope tran = new TransactionScope())
                {
                    SQLiteConnection m_dbConnection = new SQLiteConnection(DBPath);
                    try
                    {
                        DataTable dt = new DataTable();
                        m_dbConnection.Open();
                        using (SQLiteCommand fmd = m_dbConnection.CreateCommand())
                        {
                            fmd.CommandText = sql;
                            SQLiteDataReader reader = fmd.ExecuteReader();
                            dt.Load(reader);
                            reader.Close();
                            m_dbConnection.Close();
                            _result = new List<ItemChequeModel>();
                            _result = (from DataRow dr in dt.Rows
                                       select new ItemChequeModel()
                                       {
                                           ReceiptNumber = dr["ReceiptNumber"].ToString(),
                                           ChequeNumber = dr["ChequeNumber"].ToString(),
                                           ChequeName = dr["ChequeName"].ToString(),
                                           ChequeDate = dr["ChequeDate"].ToString(),
                                           BankCode = dr["BankCode"].ToString(),
                                           BankName = dr["BankName"].ToString(),
                                           ChequeAmount = double.Parse(dr["ChequeAmount"].ToString()),
                                           CashierCheque = dr["CashierCheque"].ToString(),
                                           ChequeTel = dr["ChequeTel"].ToString(),
                                       }).ToList();


                        }
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        meaLogError.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                        m_dbConnection.Close();
                        tran.Dispose();
                    }
                }
                return _result;
            }
            catch (Exception ex)
            {
                return _result;
            }
        }
        #endregion
    }
}
