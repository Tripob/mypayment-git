﻿using MyPayment.Master;
using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormOffline
{
    public partial class FormPayIn : Form
    {
        ClassLogsService meaLogError = new ClassLogsService("ErrorOffLine", "ErrorLog");
        private static OffLineClass OFC = new OffLineClass();
        public FormPayIn()
        {
            InitializeComponent();
        }
        private void FormPayIn_Load(object sender, EventArgs e)
        {
            SetControl();
        }
        private void btSearch_Click(object sender, EventArgs e)
        {
            SearchData();
        }
        private void SearchData()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
                string strDate = DateTimePicker.Value.ToString("yyyy-MM-dd");
                string status = "W";
                int index = cbDetail.SelectedIndex;
                if (index == 0)
                    status = "W";
                else if (index == 1)
                    status = "N";
                else
                    status = "Y";
                BindingSource bs = new BindingSource();
                List<ItemOfflineModel> result = OFC.SearchDataOffline(strDate, status);
                bs.DataSource = result;
                this.dgvDetail.DataSource = bs;
            }
            catch (Exception ex)
            {
                meaLogError = new ClassLogsService("ErrorLog", "");
                meaLogError.WriteData("Form :: FormPayIn | Function :: SearchData()" + " | Error ::" + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        private void SetControl()
        {
            cbDetail.SelectedIndex = 0;
            dgvDetail.AutoGenerateColumns = false;
            dgvDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void buttonSendData_Click(object sender, EventArgs e)
        {
            SendDataToService();
        }
        private bool SendDataToService()
        {
            bool tf = false;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
                string strDate = DateTimePicker.Value.ToString("yyyy-MM-dd");
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.dgvDetail.DataSource;
                List<ItemOfflineModel> LstItem = new List<ItemOfflineModel>();
                if (bs != null)
                {
                    LstItem = (List<ItemOfflineModel>)bs.DataSource;
                    LstItem = LstItem.Where(m => m.Selected == true).ToList();
                    if (LstItem.Count > 0)
                    {
                        string strReceiptNumber = string.Join(",", LstItem.Select(m => "'" + m.ReceiptNumber + "'").Distinct().ToArray());
                        var objResult = OFC.GenListOfflineToService(strDate, strReceiptNumber, true);
                        MessageBox.Show(objResult.OutputProcess.result_message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        if (objResult.OutputProcess.result_message == "SUCCESS")
                        {
                            bs = new BindingSource();
                            var result = OFC.GetSearchOffline(strDate);
                            bs.DataSource = result;
                            this.dgvDetail.DataSource = bs;
                            this.dgvDetail.Refresh();
                        }
                    }
                }

                //int i = 0;
                //var loopTo = dgvDetail.Rows.Count - 1;
                //for (i = 0; i <= loopTo; i++)
                //{
                //    if (dgvDetail.CurrentCell is DataGridViewCheckBoxCell)
                //    {
                //        string receiptNumber = dgvDetail.Rows[i].Cells["colReceipt"].Value.ToString();
                //        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)dgvDetail.Rows[i].Cells["colCheck"];
                //        bool newBool = (bool)checkCell.EditedFormattedValue;
                //        if (newBool)
                //        {
                //            var objResult = OFC.GenListOfflineToService(strDate, receiptNumber, true);
                //            if (objResult.OutputProcess.result_message == "SUCCESS")
                //                SearchData();
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {

            }
            return tf;
        }
        private void CheckBoxSelect_Click(object sender, EventArgs e)
        {
            if (dgvDetail.Rows.Count > 0)
            {
                int i = 0;
                var loopTo = dgvDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (CheckBoxSelect.Checked == true)
                        dgvDetail.Rows[i].Cells["colCheck"].Value = true;
                    else
                        dgvDetail.Rows[i].Cells["colCheck"].Value = false;
                }
            }
        }
    }
}
