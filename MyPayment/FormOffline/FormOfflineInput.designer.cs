﻿namespace MyPayment.FormOffline
{
    partial class FormOfflineInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PN1 = new System.Windows.Forms.Panel();
            this.txtContractAccount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CheckBoxSelect = new System.Windows.Forms.CheckBox();
            this.GridDetail = new System.Windows.Forms.DataGridView();
            this.ColCheckBoxDetail = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colTaxId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPowerUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDebtCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInvoiceNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDueDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFineFlakedOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStopElectricityAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmountExVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDefaultpenalty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTotalSumAmount = new System.Windows.Forms.Label();
            this.Label37 = new System.Windows.Forms.Label();
            this.Label39 = new System.Windows.Forms.Label();
            this.Label38 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtFine = new System.Windows.Forms.Label();
            this.Label36 = new System.Windows.Forms.Label();
            this.txtTotalAmountIncVat = new System.Windows.Forms.Label();
            this.Label35 = new System.Windows.Forms.Label();
            this.txtTotalVat = new System.Windows.Forms.Label();
            this.txtTotalAmountExVat = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label32 = new System.Windows.Forms.Label();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label29 = new System.Windows.Forms.Label();
            this.txtTotalItem = new System.Windows.Forms.Label();
            this.Label31 = new System.Windows.Forms.Label();
            this.txtItemSelect = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.txtPayAmount = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.PanelEmployee = new System.Windows.Forms.Panel();
            this.groupBoxEmployee = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelStationNo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelEmployeeName = new System.Windows.Forms.Label();
            this.LabelEmployeeCode = new System.Windows.Forms.Label();
            this.LabelFkName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ButtonRec = new Custom_Controls_in_CS.ButtonZ();
            this.PN1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).BeginInit();
            this.Panel4.SuspendLayout();
            this.PanelEmployee.SuspendLayout();
            this.groupBoxEmployee.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN1
            // 
            this.PN1.Controls.Add(this.txtContractAccount);
            this.PN1.Controls.Add(this.label1);
            this.PN1.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN1.Location = new System.Drawing.Point(0, 0);
            this.PN1.Name = "PN1";
            this.PN1.Size = new System.Drawing.Size(1012, 49);
            this.PN1.TabIndex = 0;
            // 
            // txtContractAccount
            // 
            this.txtContractAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtContractAccount.Location = new System.Drawing.Point(130, 13);
            this.txtContractAccount.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtContractAccount.Name = "txtContractAccount";
            this.txtContractAccount.Size = new System.Drawing.Size(561, 29);
            this.txtContractAccount.TabIndex = 1;
            this.txtContractAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtContractAccount_KeyDown);
            this.txtContractAccount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContractAccount_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "บัญชีแสดงสัญญา";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CheckBoxSelect);
            this.groupBox1.Controls.Add(this.GridDetail);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10, 3, 10, 4);
            this.groupBox1.Size = new System.Drawing.Size(1012, 433);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รายละเอียดหนี้ค้างชำระ";
            // 
            // CheckBoxSelect
            // 
            this.CheckBoxSelect.AutoSize = true;
            this.CheckBoxSelect.Location = new System.Drawing.Point(14, 41);
            this.CheckBoxSelect.Name = "CheckBoxSelect";
            this.CheckBoxSelect.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelect.TabIndex = 1000000048;
            this.CheckBoxSelect.UseVisualStyleBackColor = true;
            this.CheckBoxSelect.Click += new System.EventHandler(this.CheckBoxSelect_Click);
            // 
            // GridDetail
            // 
            this.GridDetail.AllowUserToAddRows = false;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle43;
            this.GridDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCheckBoxDetail,
            this.colTaxId,
            this.colCA,
            this.colPowerUnit,
            this.colDebtCode,
            this.colInvoiceNumber,
            this.colDueDate,
            this.colFineFlakedOut,
            this.colStopElectricityAmount,
            this.colVat,
            this.colAmountExVat,
            this.colDefaultpenalty,
            this.colIndex});
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle56.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle56.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle56.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle56.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridDetail.DefaultCellStyle = dataGridViewCellStyle56;
            this.GridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDetail.Location = new System.Drawing.Point(10, 28);
            this.GridDetail.Name = "GridDetail";
            this.GridDetail.RowHeadersVisible = false;
            this.GridDetail.RowHeadersWidth = 10;
            this.GridDetail.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GridDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridDetail.Size = new System.Drawing.Size(992, 401);
            this.GridDetail.TabIndex = 3;
            this.GridDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridDetail_CellClick);
            this.GridDetail.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridDetail_CellDoubleClick);
            this.GridDetail.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.GridDetail_CellPainting);
            // 
            // ColCheckBoxDetail
            // 
            this.ColCheckBoxDetail.DataPropertyName = "Selected";
            this.ColCheckBoxDetail.Frozen = true;
            this.ColCheckBoxDetail.HeaderText = "";
            this.ColCheckBoxDetail.Name = "ColCheckBoxDetail";
            this.ColCheckBoxDetail.Width = 20;
            // 
            // colTaxId
            // 
            this.colTaxId.DataPropertyName = "TaxID";
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle44.Format = "dd/MM/yyyy";
            this.colTaxId.DefaultCellStyle = dataGridViewCellStyle44;
            this.colTaxId.HeaderText = "เลขประจำตัวผู้เสียภาษี";
            this.colTaxId.Name = "colTaxId";
            this.colTaxId.ReadOnly = true;
            this.colTaxId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colTaxId.Width = 180;
            // 
            // colCA
            // 
            this.colCA.DataPropertyName = "CA";
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle45.Format = "dd/MM/yyyy";
            dataGridViewCellStyle45.NullValue = null;
            this.colCA.DefaultCellStyle = dataGridViewCellStyle45;
            this.colCA.HeaderText = "บัญชีแสดงสัญญา";
            this.colCA.Name = "colCA";
            this.colCA.ReadOnly = true;
            this.colCA.Width = 150;
            // 
            // colPowerUnit
            // 
            this.colPowerUnit.DataPropertyName = "PowerUnit";
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colPowerUnit.DefaultCellStyle = dataGridViewCellStyle46;
            this.colPowerUnit.FillWeight = 80F;
            this.colPowerUnit.HeaderText = "หน่วยการใช้ไฟฟ้า";
            this.colPowerUnit.Name = "colPowerUnit";
            this.colPowerUnit.ReadOnly = true;
            this.colPowerUnit.Width = 150;
            // 
            // colDebtCode
            // 
            this.colDebtCode.DataPropertyName = "DebtCode";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle47.Format = "#,###,###,##0.00";
            dataGridViewCellStyle47.NullValue = null;
            this.colDebtCode.DefaultCellStyle = dataGridViewCellStyle47;
            this.colDebtCode.HeaderText = "รหัสหนี้ค้างชำระ";
            this.colDebtCode.Name = "colDebtCode";
            this.colDebtCode.ReadOnly = true;
            this.colDebtCode.Width = 150;
            // 
            // colInvoiceNumber
            // 
            this.colInvoiceNumber.DataPropertyName = "InvoiceNumber";
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colInvoiceNumber.DefaultCellStyle = dataGridViewCellStyle48;
            this.colInvoiceNumber.HeaderText = "เลขที่ใบแจ้งฯ";
            this.colInvoiceNumber.Name = "colInvoiceNumber";
            this.colInvoiceNumber.Width = 150;
            // 
            // colDueDate
            // 
            this.colDueDate.DataPropertyName = "DueDate";
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle49.Format = "#,###,###,##0.00";
            this.colDueDate.DefaultCellStyle = dataGridViewCellStyle49;
            this.colDueDate.HeaderText = "วันครบกำหนดชำระ";
            this.colDueDate.Name = "colDueDate";
            this.colDueDate.ReadOnly = true;
            this.colDueDate.Width = 160;
            // 
            // colFineFlakedOut
            // 
            this.colFineFlakedOut.DataPropertyName = "FineFlakedOut";
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle50.Format = "#,###,###,##0.00";
            this.colFineFlakedOut.DefaultCellStyle = dataGridViewCellStyle50;
            this.colFineFlakedOut.HeaderText = "ค่าปรับ";
            this.colFineFlakedOut.Name = "colFineFlakedOut";
            this.colFineFlakedOut.ReadOnly = true;
            this.colFineFlakedOut.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFineFlakedOut.Width = 140;
            // 
            // colStopElectricityAmount
            // 
            this.colStopElectricityAmount.DataPropertyName = "StopElectricityAmount";
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle51.Format = "#,###,###,##0";
            dataGridViewCellStyle51.NullValue = null;
            this.colStopElectricityAmount.DefaultCellStyle = dataGridViewCellStyle51;
            this.colStopElectricityAmount.HeaderText = "ค่าดำเนินการงดจ่ายไฟ";
            this.colStopElectricityAmount.Name = "colStopElectricityAmount";
            this.colStopElectricityAmount.ReadOnly = true;
            this.colStopElectricityAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colStopElectricityAmount.Width = 180;
            // 
            // colVat
            // 
            this.colVat.DataPropertyName = "AmountVat";
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle52.Format = "#,###,###,##0.00";
            this.colVat.DefaultCellStyle = dataGridViewCellStyle52;
            this.colVat.HeaderText = "ภาษี";
            this.colVat.Name = "colVat";
            // 
            // colAmountExVat
            // 
            this.colAmountExVat.DataPropertyName = "AmountExVat";
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle53.Format = "#,###,###,##0.00";
            this.colAmountExVat.DefaultCellStyle = dataGridViewCellStyle53;
            this.colAmountExVat.HeaderText = "จำนวนเงิน";
            this.colAmountExVat.Name = "colAmountExVat";
            this.colAmountExVat.ReadOnly = true;
            this.colAmountExVat.Width = 180;
            // 
            // colDefaultpenalty
            // 
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle54.Format = "#,###,###,##0.00";
            this.colDefaultpenalty.DefaultCellStyle = dataGridViewCellStyle54;
            this.colDefaultpenalty.HeaderText = "ลบ";
            this.colDefaultpenalty.Name = "colDefaultpenalty";
            this.colDefaultpenalty.ReadOnly = true;
            this.colDefaultpenalty.Width = 80;
            // 
            // colIndex
            // 
            this.colIndex.DataPropertyName = "Index";
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle55.Format = "#,###,###,##0.00";
            dataGridViewCellStyle55.NullValue = null;
            this.colIndex.DefaultCellStyle = dataGridViewCellStyle55;
            this.colIndex.HeaderText = "Index";
            this.colIndex.Name = "colIndex";
            this.colIndex.ReadOnly = true;
            this.colIndex.Visible = false;
            this.colIndex.Width = 80;
            // 
            // txtTotalSumAmount
            // 
            this.txtTotalSumAmount.BackColor = System.Drawing.Color.White;
            this.txtTotalSumAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalSumAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTotalSumAmount.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalSumAmount.Location = new System.Drawing.Point(187, 91);
            this.txtTotalSumAmount.Name = "txtTotalSumAmount";
            this.txtTotalSumAmount.Size = new System.Drawing.Size(394, 39);
            this.txtTotalSumAmount.TabIndex = 142;
            this.txtTotalSumAmount.Text = "0.00";
            this.txtTotalSumAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.Font = new System.Drawing.Font("TH Sarabun New", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label37.ForeColor = System.Drawing.Color.Blue;
            this.Label37.Location = new System.Drawing.Point(14, 82);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(173, 57);
            this.Label37.TabIndex = 141;
            this.Label37.Text = "รวมรับชำระ";
            this.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label39
            // 
            this.Label39.AutoSize = true;
            this.Label39.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label39.Location = new System.Drawing.Point(954, 102);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(36, 26);
            this.Label39.TabIndex = 140;
            this.Label39.Text = "บาท";
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label38.Location = new System.Drawing.Point(954, 69);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(36, 26);
            this.Label38.TabIndex = 139;
            this.Label38.Text = "บาท";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label22.Location = new System.Drawing.Point(953, 36);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(36, 26);
            this.Label22.TabIndex = 138;
            this.Label22.Text = "บาท";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label13.Location = new System.Drawing.Point(953, 3);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(36, 26);
            this.Label13.TabIndex = 137;
            this.Label13.Text = "บาท";
            // 
            // txtFine
            // 
            this.txtFine.BackColor = System.Drawing.Color.White;
            this.txtFine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtFine.Location = new System.Drawing.Point(809, 102);
            this.txtFine.Name = "txtFine";
            this.txtFine.Size = new System.Drawing.Size(143, 25);
            this.txtFine.TabIndex = 136;
            this.txtFine.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label36
            // 
            this.Label36.AutoSize = true;
            this.Label36.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label36.Location = new System.Drawing.Point(651, 102);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(152, 26);
            this.Label36.TabIndex = 135;
            this.Label36.Text = "เบี้ยปรับและค่าเสียหายฯ";
            this.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalAmountIncVat
            // 
            this.txtTotalAmountIncVat.BackColor = System.Drawing.Color.White;
            this.txtTotalAmountIncVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalAmountIncVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalAmountIncVat.Location = new System.Drawing.Point(809, 69);
            this.txtTotalAmountIncVat.Name = "txtTotalAmountIncVat";
            this.txtTotalAmountIncVat.Size = new System.Drawing.Size(143, 25);
            this.txtTotalAmountIncVat.TabIndex = 134;
            this.txtTotalAmountIncVat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label35
            // 
            this.Label35.AutoSize = true;
            this.Label35.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label35.Location = new System.Drawing.Point(766, 69);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(37, 26);
            this.Label35.TabIndex = 133;
            this.Label35.Text = "รวม";
            this.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalVat
            // 
            this.txtTotalVat.BackColor = System.Drawing.Color.White;
            this.txtTotalVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalVat.Location = new System.Drawing.Point(809, 36);
            this.txtTotalVat.Name = "txtTotalVat";
            this.txtTotalVat.Size = new System.Drawing.Size(143, 25);
            this.txtTotalVat.TabIndex = 132;
            this.txtTotalVat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalAmountExVat
            // 
            this.txtTotalAmountExVat.BackColor = System.Drawing.Color.White;
            this.txtTotalAmountExVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalAmountExVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalAmountExVat.Location = new System.Drawing.Point(809, 3);
            this.txtTotalAmountExVat.Name = "txtTotalAmountExVat";
            this.txtTotalAmountExVat.Size = new System.Drawing.Size(143, 25);
            this.txtTotalAmountExVat.TabIndex = 131;
            this.txtTotalAmountExVat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label34
            // 
            this.Label34.AutoSize = true;
            this.Label34.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label34.Location = new System.Drawing.Point(745, 3);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(58, 26);
            this.Label34.TabIndex = 130;
            this.Label34.Text = "รวมเงิน";
            this.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label33
            // 
            this.Label33.AutoSize = true;
            this.Label33.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label33.Location = new System.Drawing.Point(709, 36);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(94, 26);
            this.Label33.TabIndex = 129;
            this.Label33.Text = "ภาษีมูลค่าเพิ่ม";
            this.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel4.Controls.Add(this.Label26);
            this.Panel4.Controls.Add(this.Label32);
            this.Panel4.Controls.Add(this.Label19);
            this.Panel4.Controls.Add(this.Label29);
            this.Panel4.Controls.Add(this.txtTotalItem);
            this.Panel4.Controls.Add(this.Label31);
            this.Panel4.Controls.Add(this.txtItemSelect);
            this.Panel4.Controls.Add(this.Label30);
            this.Panel4.Controls.Add(this.txtPayAmount);
            this.Panel4.Location = new System.Drawing.Point(10, 6);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(571, 65);
            this.Panel4.TabIndex = 128;
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label26.ForeColor = System.Drawing.Color.Blue;
            this.Label26.Location = new System.Drawing.Point(431, 34);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(56, 26);
            this.Label26.TabIndex = 93;
            this.Label26.Text = "รายการ";
            // 
            // Label32
            // 
            this.Label32.AutoSize = true;
            this.Label32.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label32.ForeColor = System.Drawing.Color.Blue;
            this.Label32.Location = new System.Drawing.Point(3, 4);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(44, 26);
            this.Label32.TabIndex = 96;
            this.Label32.Text = "ชำระ";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label19.ForeColor = System.Drawing.Color.Blue;
            this.Label19.Location = new System.Drawing.Point(3, 32);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(196, 26);
            this.Label19.TabIndex = 99;
            this.Label19.Text = "จำนวนบัญชีแสดงสัญญา/สัญญา";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label29.ForeColor = System.Drawing.Color.Blue;
            this.Label29.Location = new System.Drawing.Point(431, 4);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(36, 26);
            this.Label29.TabIndex = 99;
            this.Label29.Text = "บาท";
            // 
            // txtTotalItem
            // 
            this.txtTotalItem.BackColor = System.Drawing.Color.White;
            this.txtTotalItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalItem.Location = new System.Drawing.Point(205, 34);
            this.txtTotalItem.Name = "txtTotalItem";
            this.txtTotalItem.Size = new System.Drawing.Size(224, 25);
            this.txtTotalItem.TabIndex = 100;
            this.txtTotalItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label31
            // 
            this.Label31.AutoSize = true;
            this.Label31.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label31.ForeColor = System.Drawing.Color.Blue;
            this.Label31.Location = new System.Drawing.Point(166, 4);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(56, 26);
            this.Label31.TabIndex = 97;
            this.Label31.Text = "รายการ";
            // 
            // txtItemSelect
            // 
            this.txtItemSelect.BackColor = System.Drawing.Color.White;
            this.txtItemSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtItemSelect.Location = new System.Drawing.Point(53, 5);
            this.txtItemSelect.Name = "txtItemSelect";
            this.txtItemSelect.Size = new System.Drawing.Size(111, 25);
            this.txtItemSelect.TabIndex = 101;
            this.txtItemSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label30.ForeColor = System.Drawing.Color.Blue;
            this.Label30.Location = new System.Drawing.Point(224, 4);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(56, 26);
            this.Label30.TabIndex = 98;
            this.Label30.Text = "เป็นเงิน";
            // 
            // txtPayAmount
            // 
            this.txtPayAmount.BackColor = System.Drawing.Color.White;
            this.txtPayAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtPayAmount.Location = new System.Drawing.Point(286, 5);
            this.txtPayAmount.Name = "txtPayAmount";
            this.txtPayAmount.Size = new System.Drawing.Size(143, 25);
            this.txtPayAmount.TabIndex = 103;
            this.txtPayAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PanelEmployee
            // 
            this.PanelEmployee.Controls.Add(this.groupBoxEmployee);
            this.PanelEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelEmployee.Location = new System.Drawing.Point(0, 49);
            this.PanelEmployee.Name = "PanelEmployee";
            this.PanelEmployee.Size = new System.Drawing.Size(1012, 46);
            this.PanelEmployee.TabIndex = 1;
            // 
            // groupBoxEmployee
            // 
            this.groupBoxEmployee.Controls.Add(this.label3);
            this.groupBoxEmployee.Controls.Add(this.label2);
            this.groupBoxEmployee.Controls.Add(this.LabelStationNo);
            this.groupBoxEmployee.Controls.Add(this.label4);
            this.groupBoxEmployee.Controls.Add(this.LabelEmployeeName);
            this.groupBoxEmployee.Controls.Add(this.LabelEmployeeCode);
            this.groupBoxEmployee.Controls.Add(this.LabelFkName);
            this.groupBoxEmployee.Location = new System.Drawing.Point(4, -4);
            this.groupBoxEmployee.Name = "groupBoxEmployee";
            this.groupBoxEmployee.Size = new System.Drawing.Size(1005, 46);
            this.groupBoxEmployee.TabIndex = 1;
            this.groupBoxEmployee.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(361, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 26);
            this.label3.TabIndex = 1000000009;
            this.label3.Text = "รหัสพนง.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(6, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 26);
            this.label2.TabIndex = 1000000008;
            this.label2.Text = "ฟข.";
            // 
            // LabelStationNo
            // 
            this.LabelStationNo.BackColor = System.Drawing.Color.White;
            this.LabelStationNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelStationNo.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStationNo.Location = new System.Drawing.Point(925, 12);
            this.LabelStationNo.Name = "LabelStationNo";
            this.LabelStationNo.Size = new System.Drawing.Size(74, 25);
            this.LabelStationNo.TabIndex = 1000000020;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(854, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 26);
            this.label4.TabIndex = 1000000010;
            this.label4.Text = "ช่องชำระ";
            // 
            // LabelEmployeeName
            // 
            this.LabelEmployeeName.BackColor = System.Drawing.Color.White;
            this.LabelEmployeeName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelEmployeeName.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEmployeeName.Location = new System.Drawing.Point(542, 12);
            this.LabelEmployeeName.Name = "LabelEmployeeName";
            this.LabelEmployeeName.Size = new System.Drawing.Size(306, 25);
            this.LabelEmployeeName.TabIndex = 1000000019;
            this.LabelEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelEmployeeCode
            // 
            this.LabelEmployeeCode.BackColor = System.Drawing.Color.White;
            this.LabelEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelEmployeeCode.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEmployeeCode.Location = new System.Drawing.Point(429, 12);
            this.LabelEmployeeCode.Name = "LabelEmployeeCode";
            this.LabelEmployeeCode.Size = new System.Drawing.Size(110, 25);
            this.LabelEmployeeCode.TabIndex = 1000000018;
            // 
            // LabelFkName
            // 
            this.LabelFkName.BackColor = System.Drawing.Color.White;
            this.LabelFkName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelFkName.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelFkName.Location = new System.Drawing.Point(47, 12);
            this.LabelFkName.Name = "LabelFkName";
            this.LabelFkName.Size = new System.Drawing.Size(310, 25);
            this.LabelFkName.TabIndex = 1000000017;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 433);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Panel4);
            this.panel2.Controls.Add(this.txtTotalSumAmount);
            this.panel2.Controls.Add(this.Label33);
            this.panel2.Controls.Add(this.Label37);
            this.panel2.Controls.Add(this.Label34);
            this.panel2.Controls.Add(this.Label39);
            this.panel2.Controls.Add(this.txtTotalAmountExVat);
            this.panel2.Controls.Add(this.Label38);
            this.panel2.Controls.Add(this.txtTotalVat);
            this.panel2.Controls.Add(this.Label22);
            this.panel2.Controls.Add(this.Label35);
            this.panel2.Controls.Add(this.Label13);
            this.panel2.Controls.Add(this.txtTotalAmountIncVat);
            this.panel2.Controls.Add(this.txtFine);
            this.panel2.Controls.Add(this.Label36);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 528);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1012, 139);
            this.panel2.TabIndex = 5;
            // 
            // ButtonRec
            // 
            this.ButtonRec.BackColor = System.Drawing.Color.Transparent;
            this.ButtonRec.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonRec.BorderWidth = 1;
            this.ButtonRec.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonRec.ButtonText = "รับเงิน";
            this.ButtonRec.CausesValidation = false;
            this.ButtonRec.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonRec.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonRec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonRec.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonRec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.ButtonRec.GradientAngle = 90;
            this.ButtonRec.Image = global::MyPayment.Properties.Resources.icons8_paper_money_26;
            this.ButtonRec.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonRec.Location = new System.Drawing.Point(901, 667);
            this.ButtonRec.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonRec.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonRec.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonRec.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonRec.Name = "ButtonRec";
            this.ButtonRec.ShowButtontext = true;
            this.ButtonRec.Size = new System.Drawing.Size(83, 35);
            this.ButtonRec.StartColor = System.Drawing.Color.LightGray;
            this.ButtonRec.TabIndex = 1000000041;
            this.ButtonRec.TextLocation_X = 35;
            this.ButtonRec.TextLocation_Y = 6;
            this.ButtonRec.Transparent1 = 80;
            this.ButtonRec.Transparent2 = 120;
            this.ButtonRec.UseVisualStyleBackColor = true;
            this.ButtonRec.Click += new System.EventHandler(this.ButtonRec_Click);
            // 
            // FormOfflineInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.ButtonRec);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PanelEmployee);
            this.Controls.Add(this.PN1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormOfflineInput";
            this.Text = "FormOfflineInput";
            this.Load += new System.EventHandler(this.FormOfflineInput_Load);
            this.PN1.ResumeLayout(false);
            this.PN1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).EndInit();
            this.Panel4.ResumeLayout(false);
            this.Panel4.PerformLayout();
            this.PanelEmployee.ResumeLayout(false);
            this.groupBoxEmployee.ResumeLayout(false);
            this.groupBoxEmployee.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN1;
        private System.Windows.Forms.TextBox txtContractAccount;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label32;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label txtTotalItem;
        internal System.Windows.Forms.Label Label31;
        internal System.Windows.Forms.Label txtItemSelect;
        internal System.Windows.Forms.Label Label30;
        internal System.Windows.Forms.Label txtPayAmount;
        internal System.Windows.Forms.Label Label39;
        internal System.Windows.Forms.Label Label38;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label txtFine;
        internal System.Windows.Forms.Label Label36;
        internal System.Windows.Forms.Label txtTotalAmountIncVat;
        internal System.Windows.Forms.Label Label35;
        internal System.Windows.Forms.Label txtTotalVat;
        internal System.Windows.Forms.Label txtTotalAmountExVat;
        internal System.Windows.Forms.Label Label34;
        internal System.Windows.Forms.Label Label33;
        internal System.Windows.Forms.Label txtTotalSumAmount;
        internal System.Windows.Forms.Label Label37;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView GridDetail;
        private System.Windows.Forms.CheckBox CheckBoxSelect;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColCheckBoxDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTaxId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCA;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPowerUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDebtCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInvoiceNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDueDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFineFlakedOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStopElectricityAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmountExVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDefaultpenalty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIndex;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel PanelEmployee;
        private System.Windows.Forms.GroupBox groupBoxEmployee;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label LabelStationNo;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label LabelEmployeeName;
        internal System.Windows.Forms.Label LabelEmployeeCode;
        internal System.Windows.Forms.Label LabelFkName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Custom_Controls_in_CS.ButtonZ ButtonRec;
    }
}