﻿using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPayment.FormOffline
{
    public partial class FormProcessingFeeElectricity : Form
    {
        public FormProcessingFeeElectricity()
        {
            InitializeComponent();
        }
        private void ButtonNo_Click(object sender, EventArgs e)
        {
            GlobalClass.Status = true;
            this.Close();
        }
        private void ButtonYes_Click(object sender, EventArgs e)
        {
            GlobalClass.Status = false;
            this.Close();
        }
    }
}
