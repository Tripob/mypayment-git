﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using MyPayment.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormOffline
{
    public partial class FormOfflineReceive : Form
    {
        //private static string ReportPath = ConfigurationManager.AppSettings["ReportPath"].ToString();
        public static string ReportPath = AppDomain.CurrentDomain.BaseDirectory + @"\Report\";
        private static double VAT = double.Parse(ConfigurationManager.AppSettings["VAT"].ToString());
        private static OffLineClass OFC = new OffLineClass();
        public static string GB_ReceiptNumber = "";
        public static List<ItemBank> GB_lstBnk = new List<ItemBank>();
        public bool isCancel { get; set; }
        public CultureInfo usCulture = new CultureInfo("en-US");
        public string CustomerName;
        public double Countamount;
        public decimal CountamountAll;
        public bool GB_IsReadBarcode = false;
        public string GB_StrQrCode = "";
        public string GB_Chno = "";
        ResultCostTDebt resul = new ResultCostTDebt();
        ArrearsController arr = new ArrearsController();
        List<ClassCheque> _cheque = new List<ClassCheque>();
        List<ClassListGridview> _lstGrid = new List<ClassListGridview>();
        public byte Key_Error;
        public string qNo;
        ClassLogsService ReceiveMoneyLog = new ClassLogsService("ReceiveMoneyOffline", "DataLog");
        ClassLogsService ReceiveMoneyEventLog = new ClassLogsService("ReceiveMoneyOffline", "EventLog");
        ClassLogsService ReceiveMoneyErrorLog = new ClassLogsService("ReceiveMoneyOffline", "ErrorLog");
        public FormOfflineReceive()
        {
            InitializeComponent();
        }
        private void FormOfflineReceive_Load(object sender, EventArgs e)
        {
            try
            {
                SetControl();
                GB_lstBnk = OFC.GetBankOffline();
                var arrBankCode = GB_lstBnk.OrderBy(m => m.BankCode).Select(m => m.BankCode).ToArray();
                AutoCompleteStringCollection allowedTypes = new AutoCompleteStringCollection();
                allowedTypes.AddRange(arrBankCode);
                txtBankId.AutoCompleteCustomSource = allowedTypes;
                txtBankId.AutoCompleteMode = AutoCompleteMode.Suggest;
                txtBankId.AutoCompleteSource = AutoCompleteSource.CustomSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void SetControl()
        {
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        public void SetAmount(string Amount)
        {
            this.txtAmount.Text = Amount;
        }
        public void SetReceiptNumber(string ReceiptNumber)
        {
            GB_ReceiptNumber = ReceiptNumber;
        }
        private void ClearChequeData()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("th-TH");
            txtCheque.Text = "0.00";
            txtChequeNumber.Text = "";
            dpChequeDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtBankName.Text = "";
            txtChequeName.Text = "";
            txtChequeTel.Text = "";
            txtBankId.Text = "";
            chCashierCheque.Checked = false;
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
            this.btnAdd.Enabled = true;
            this.ButtonUpate.Enabled = false;
        }
        private void ClearDataGrid()
        {
            GridDetail.Columns.Clear();
        }
        private void ClearDataItem()
        {
            txtAmount.Text = "";
            txtCash.Text = "";
            txtChequeNumber.Text = "";
            txtBankId.Text = "";
            txtBankName.Text = "";
            txtCheque.Text = "";
            txtChequeName.Text = "";
            txtChequeTel.Text = "";
            chCashierCheque.Checked = false;
            btnAdd.Enabled = true;
            ButtonUpate.Enabled = false;
            lblAmountCheq.Text = "0.00";
            lblAmountTotalCheq.Text = "0.00";
            lblAmountReceived.Text = "0.00";
            txtTelContact.Text = "";
            lblAmountChange.Text = "0.00";
        }
        private void AddChequeMain()
        {
            try
            {
                if (CheckTextEmpty())
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else if (txtChequeNumber.Text.Trim().Length != 9)
                    MessageBox.Show("กรุณากรอกเลขที่เช็คให้ครบ 9 หลัก", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    if (string.IsNullOrEmpty(this.GB_Chno) && CheckData())
                        MessageBox.Show("ข้อมูลเลขที่เช็คและรหัสธนาคาร/สาขา ไม่สามารถซ้ำกันได้ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    else if (OFC.isDuplicateChequeNumber(this.txtChequeNumber.Text.Trim(), this.txtBankId.Text.Trim()))
                        MessageBox.Show("มีข้อมูลเลขที่เช็คและรหัสธนาคารนี้แล้วในระบบ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    else
                    {
                        bool status = false;
                        double AmountCredit = 0;
                        double TotalAmountCheq = 0;
                        double TotalAmount = 0;
                        if (lblAmountTotalCheq.Text != "")
                            TotalAmountCheq = Convert.ToDouble(lblAmountTotalCheq.Text);
                        if (txtCash.Text != "")
                            TotalAmount = Convert.ToDouble(txtCash.Text);
                        if (this.GridDetail.SelectedRows.Count > 0)
                        {
                            ClassCheque cheque = _cheque.Find(item => item.No.Equals(lblNo.Text.ToString(), StringComparison.CurrentCultureIgnoreCase));
                            if (cheque != null)
                                status = false;
                            else
                            {
                                if (TotalAmount == 0 && AmountCredit == 0 && TotalAmountCheq > 0)
                                {
                                    CountAmountCheq();
                                    double amountSum = (TotalAmountCheq + Convert.ToDouble(txtCheque.Text)) - Convert.ToDouble(txtAmount.Text);
                                    if (amountSum > 10)
                                        status = true;
                                }
                            }
                        }
                        else
                        {
                            if (TotalAmount == 0 && AmountCredit == 0 && Convert.ToDouble(txtCheque.Text) > 0)
                            {
                                double amountSum = Convert.ToDouble(txtCheque.Text) - Convert.ToDouble(txtAmount.Text);
                                if (amountSum > 10)
                                    status = true;
                            }
                        }
                        if (status)
                        {
                            MessageBox.Show("มูลค่าเช็คที่ชำระเกินจากจำนวนเงินที่ต้องชำระมากกว่า 10 บาทไม่สามารถรับได้ ", "ชำระเกิน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(this.GB_Chno))
                            {
                                var rec = _cheque.Where(m => m.Chno == this.GB_Chno).FirstOrDefault();
                                _cheque.Remove(rec);
                                this.GB_Chno = "";
                            }
                            /***********************/
                            AddChequeToList();
                            //ClassCheque cheque = new ClassCheque();
                            //cheque.UserId = GlobalClass.UserCode;
                            //cheque.Chno = txtChequeNumber.Text.Trim();
                            //cheque.Bankno = txtBankId.Text.Trim();
                            //cheque.Bankname = txtBankName.Text.Trim();
                            //cheque.Chdate = dpChequeDate.Text.ToString();
                            //cheque.Chamount = txtCheque.Text.Trim();
                            //cheque.Chname = txtChequeName.Text.Trim();
                            //cheque.Tel = txtChequeTel.Text.Replace("-", "").Trim();
                            //cheque.Chcheck = chCashierCheque.Checked;
                            //cheque.ChdateNew = dpChequeDate.Value.Date.ToString("dd/MM/yyyy");
                            //cheque.No = "1";
                            //_cheque.Add(cheque);
                            //lblNo.Text = "1";
                            var source = new BindingSource(_cheque, null);
                            this.GridDetail.DataSource = source;
                            CountamountAll = _cheque.Sum(m => m.Chamount);
                            CountAmountCheq();
                            SumAmount();
                            ClearChequeData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void AddChequeToList()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (_cheque != null && _cheque.Count > 0)
                {
                    int i = 1;
                    List<ClassCheque> _lst = new List<ClassCheque>();
                    foreach (var item in _cheque)
                    {
                        ClassCheque cheque = new ClassCheque();
                        cheque.Chno = item.UserId;
                        cheque.Chno = item.Chno;
                        cheque.Bankno = item.Bankno;
                        cheque.Bankname = item.Bankname;
                        cheque.Chdate = item.Chdate;
                        cheque.Chamount = item.Chamount;
                        cheque.Chname = item.Chname;
                        cheque.Tel = item.Tel;
                        cheque.Chcheck = item.Chcheck;
                        cheque.Status = item.Status;
                        cheque.ChdateNew = item.ChdateNew;
                        cheque.No = i.ToString();
                        i++;
                        _lst.Add(cheque);
                    }
                    ClassCheque chequeNew = new ClassCheque();
                    chequeNew.UserId = GlobalClass.UserCode;
                    chequeNew.Chno = txtChequeNumber.Text.Trim();
                    chequeNew.Bankno = txtBankId.Text.Trim();
                    chequeNew.Bankname = txtBankName.Text.Trim();
                    chequeNew.Chdate = dpChequeDate.Text.ToString();
                    chequeNew.ChdateNew = dpChequeDate.Value.Date.ToString("dd/MM/yyyy");
                    chequeNew.Chamount = Convert.ToDecimal(txtCheque.Text.Trim());
                    chequeNew.Chname = txtChequeName.Text.Trim();
                    chequeNew.Tel = txtChequeTel.Text.Replace("-", "").Trim();
                    chequeNew.Chcheck = chCashierCheque.Checked;
                    chequeNew.No = i.ToString();
                    _lst.Add(chequeNew);
                    _cheque = _lst;
                }
                else
                {
                    ClassCheque cheque = new ClassCheque();
                    cheque.UserId = GlobalClass.UserCode;
                    cheque.Chno = txtChequeNumber.Text.Trim();
                    cheque.Bankno = txtBankId.Text.Trim();
                    cheque.Bankname = txtBankName.Text.Trim();
                    cheque.Chdate = dpChequeDate.Text.ToString();
                    cheque.ChdateNew = dpChequeDate.Value.Date.ToString("dd/MM/yyyy");
                    cheque.Chamount = Convert.ToDecimal(txtCheque.Text.Trim());
                    cheque.Chname = txtChequeName.Text.Trim();
                    cheque.Tel = txtChequeTel.Text.Replace("-", "").Trim();
                    cheque.ChStatus = true;
                    cheque.No = "1";
                    _cheque.Add(cheque);
                    lblNo.Text = "1";
                }
                this.GridDetail.DataSource = _cheque;
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }
        public void SumAmount()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtAmount.Text.Trim()) && double.Parse(txtAmount.Text.Trim()) >= 0.00)
                {
                    double totalAmount = 0;
                    double amount = 0;
                    double amountCheque = 0;
                    double amountCreditCard = 0;
                    double sumAmount = 0;
                    if (txtCash.Text != "")
                        amount = Convert.ToDouble(txtCash.Text);
                    if (lblAmountTotalCheq.Text != "0.00")
                        amountCheque = Convert.ToDouble(lblAmountTotalCheq.Text);
                    totalAmount = amount + amountCheque + amountCreditCard;
                    lblAmountReceived.Text = totalAmount.ToString("#,###,###,##0.00");
                    if (totalAmount > 0)
                        sumAmount = totalAmount - Convert.ToDouble(txtAmount.Text.Trim());
                    if (sumAmount < 0)
                    {
                        label25.Text = sumAmount.ToString("#,###,###,##0.00");
                        lblAmountChange.Text = "00";
                    }
                    else
                    {
                        lblAmountChange.Text = sumAmount.ToString("#,###,###,##0.00");
                        label25.Text = "00";
                    }
                }
                if (double.Parse(label25.Text.Trim()) == 0)
                    this.btnConfirm.Enabled = true;
                else
                    this.btnConfirm.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void CountAmountCheq()
        {
            double Amount = 0;
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                DataGridViewRow row = GridDetail.Rows[i];
                row.Height = 35;
                Image image = Properties.Resources.Recycle;
                GridDetail.Rows[i].Cells["ColDelete"].Value = image;
                Image Edit = Properties.Resources.icons8_edit_32;
                GridDetail.Rows[i].Cells["ColeditNew"].Value = Edit;
                Amount += Convert.ToDouble(GridDetail.Rows[i].Cells["colAmount"].Value);
            }
            lblAmountTotalCheq.Text = Amount.ToString("#,###,###,##0.00");
            lblAmountCheq.Text = Amount.ToString("#,###,##0.00");
        }
        private bool CheckTextEmpty()
        {
            bool status = false;
            if (txtChequeNumber.Text == string.Empty)
                status = true;
            if (txtBankId.Text == string.Empty)
                status = true;
            if (txtBankName.Text == string.Empty)
                status = true;
            if (dpChequeDate.Text == string.Empty)
                status = true;
            if (txtCheque.Text == string.Empty)
                status = true;
            else
            {
                var Cheque = double.Parse(txtCheque.Text);
                if (Cheque <= 0.0)
                {
                    status = true;
                }
            }
            if (txtChequeName.Text == string.Empty)
                status = true;
            if (txtChequeTel.Text.Replace("-", "").Trim() == string.Empty)
                status = true;
            return status;
        }
        private bool CheckData()
        {
            bool tf;
            var checkData = _cheque.Where(c => c.Chno == txtChequeNumber.Text).FirstOrDefault();
            if (checkData != null)
                tf = true;
            else
                tf = false;
            return tf;
        }
        private void GridDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    string Chno = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                    List<ClassCheque> LstNewCheque = new List<ClassCheque>();
                    LstNewCheque = _cheque.Where(m => m.Chno != Chno).ToList();
                    _cheque = LstNewCheque;
                    this.GridDetail.DataSource = _cheque;
                    GlobalClass.LstCheque = _cheque;
                    foreach (var item in GlobalClass.LstCheque)
                    {
                        CountamountAll +=item.Chamount;
                    }
                    CountAmountCheq();
                    SumAmount();
                    return;
                }
                if (e.ColumnIndex == 1)
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-GB");
                    ClassCheque item = new ClassCheque();
                    string Chno = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                    item = _cheque.Where(m => m.Chno == Chno).FirstOrDefault();
                    txtChequeNumber.Text = item.Chno;
                    txtBankId.Text = item.Bankno;
                    txtBankName.Text = item.Bankname;
                    dpChequeDate.Text = item.ChdateNew.ToString();
                    txtCheque.Text = item.Chamount.ToString();
                    txtChequeName.Text = item.Chname;
                    txtChequeTel.Text = item.Tel;
                    this.GB_Chno = item.Chno;
                    this.ButtonUpate.Enabled = true;
                    this.btnAdd.Enabled = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddChequeMain();
        }
        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                    e.Handled = true;
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                    e.Handled = true;
                SumAmount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void txtCash_TextChanged(object sender, EventArgs e)
        {
            SumAmount();
        }
        private void txtCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                e.Handled = true;
            if (label25.Text == "0.00" && lblAmountReceived.Text != "0.00")
                txtTelContact.Focus();
        }
        private void txtCheque_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                    e.Handled = true;
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                    e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            AddChequeMain();
        }
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
                string strTelContact = this.txtTelContact.Text.Replace("-", "").Trim();
                if (strTelContact == "")
                {
                    MessageBox.Show("รบกวนระบุหมายเลขติดต่อกลับ", "แจ้งเตือน");
                    return;
                }
                ItemOfflineModel OfflineRec = GlobalClass.OfflineModel;
                OfflineRec.Tel = strTelContact;
                List<ItemInvoiceModel> LstCASelectedModel = GlobalClass.LstCASelectedModel;
                List<ItemBillModel> LstBillModel = GlobalClass.LstBillModel;
                List<ItemStopElectricModel> LstStopElectricModel = GlobalClass.LstStopElectricityModel;
                List<ItemChequeModel> LstCheque = new List<ItemChequeModel>();
                List<ItemMapChequeModel> LstMappCheque = new List<ItemMapChequeModel>();
                if (_cheque != null && _cheque.Count > 0)
                {
                    ItemChequeModel obj = new ItemChequeModel();
                    foreach (var item in _cheque)
                    {
                        obj = new ItemChequeModel();
                        obj.ReceiptNumber = GB_ReceiptNumber;
                        obj.ChequeNumber = item.Chno;
                        obj.ChequeName = item.Chname;
                        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-GB");
                        obj.ChequeDate = item.ChdateNew;
                        string dtNow = DateTime.Now.ToString("yyyy-MM-dd", usCulture);
                        obj.CreateDate = dtNow;
                        obj.CreateBy = GlobalClass.UserId.ToString();
                        obj.BankCode = item.Bankno;
                        obj.BankName = item.Bankname;
                        obj.ChequeAmount = Convert.ToDouble(item.Chamount);
                        obj.CashierCheque = "Y";
                        obj.ChequeTel = item.Tel;
                        LstCheque.Add(obj);
                    }
                    OFC.InsertCheque(LstCheque);
                    LstMappCheque = MappCheque(LstCheque, LstCASelectedModel);
                }
                MapCash(LstMappCheque, LstCASelectedModel);
                OFC.InsertCA(LstCASelectedModel);
                if (txtCash.Text != "")
                    OfflineRec.CashReceive = double.Parse(txtCash.Text);
                OFC.InsertOffline(OfflineRec);
                OFC.InsertBill(LstBillModel);
                //   OFC.InsertStopElectric(LstStopElectricModel);
                ItemMapCashModel CashRec = new ItemMapCashModel();
                ItemChangeModel ChangeRec = new ItemChangeModel();
                if (lblAmountChange.Text != "0.00")
                {
                    ChangeRec.ReceiptNumber = GB_ReceiptNumber;
                    ChangeRec.CashChange = double.Parse(lblAmountChange.Text);
                    ChangeRec.RealCashChange = OFC.Fraction(double.Parse(lblAmountChange.Text));
                    ChangeRec.ChequeChange = 0.0;
                    ChangeRec.RealChequeChange = 0.0;
                    OFC.InsertChange(ChangeRec);
                }
                ClearDataItem();
                ClearDataGrid();
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("th-TH");
                string Path = "";
                Path = ReportPath + "\\" + "rptTaxInvoiceOfflineStopElectricity.rpt";
                ReportDocument cryRpt = new ReportDocument();
                CrystalDecisions.Shared.PageMargins objPageMargins;
                objPageMargins = cryRpt.PrintOptions.PageMargins;
                objPageMargins.bottomMargin = 100;
                objPageMargins.leftMargin = 100;
                objPageMargins.rightMargin = 100;
                objPageMargins.topMargin = 100;
                cryRpt.Load(Path);
                if (GlobalClass.LstStopElectricityModel != null && GlobalClass.LstStopElectricityModel.Count > 0)
                {
                    cryRpt.SetDataSource(GlobalClass.LstStopElectricityModel);
                    cryRpt.SetParameterValue("Param_Payee", GlobalClass.UserName);
                    cryRpt.SetParameterValue("Param_BranchName", GlobalClass.BranchName);
                    cryRpt.SetParameterValue("Param_UserId", GlobalClass.UserId);
                    cryRpt.SetParameterValue("Param_ReceiptId", "Param_ReceiptId");
                    cryRpt.SetParameterValue("Param_BranchId", GlobalClass.BranchId);
                    cryRpt.PrintOptions.ApplyPageMargins(objPageMargins);
                    cryRpt.PrintOptions.PrinterName = "Microsoft Print to PDF";
                    cryRpt.PrintToPrinter(1, false, 0, 0);
                }
                Path = ReportPath + "\\" + "rptTaxInvoiceOffline_Test.rpt";
                cryRpt = new ReportDocument();
                cryRpt.Load(Path);
                objPageMargins = cryRpt.PrintOptions.PageMargins;
                objPageMargins.bottomMargin = 100;
                objPageMargins.leftMargin = 100;
                objPageMargins.rightMargin = 100;
                objPageMargins.topMargin = 100;
                var LstShortBill = OFC.GetShortBill(GlobalClass.ReceiptNumber);
                cryRpt.SetDataSource(LstShortBill);
                cryRpt.SetParameterValue("Param_Payee", GlobalClass.UserName);
                cryRpt.SetParameterValue("Param_BranchName", GlobalClass.BranchName);
                cryRpt.SetParameterValue("Param_UserId", GlobalClass.UserId);
                cryRpt.SetParameterValue("Param_ReceiptId", "Param_ReceiptId");
                cryRpt.SetParameterValue("Param_BranchId", GlobalClass.BranchId);
                cryRpt.PrintOptions.ApplyPageMargins(objPageMargins);
                cryRpt.PrintOptions.PrinterName = "Microsoft Print to PDF";
                cryRpt.PrintToPrinter(1, false, 0, 0);
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
                System.Windows.Forms.Timer MyTimer = new System.Windows.Forms.Timer();
                MyTimer.Interval = 500; // 45 mins
                MyTimer.Tick += new EventHandler(Timer_CloseForm_Tick);
                MyTimer.Start();

            }
            catch (Exception ex)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
            }
        }
        public List<ItemMapChequeModel> MappCheque(List<ItemChequeModel> LstCheque, List<ItemInvoiceModel> LstItemInvoiceModel)
        {
            List<ItemMapChequeModel> LstMappCheque = new List<ItemMapChequeModel>();
            try
            {
                ItemMapChequeModel MappChequeTmp = new ItemMapChequeModel();
                ItemChequeModel ChequeTmp = new ItemChequeModel();
                ItemInvoiceModel InvoiceTmp = new ItemInvoiceModel();
                var ArrCheque = LstCheque.ToArray();
                var ArrItemInvoiceModel = LstItemInvoiceModel.ToArray();
                int indexCheque = 0;
                int indexInvoice = 0;
                double ChequeAmountTmp = 0.0;
                double InvoiceAmountTmp = 0.0;
                double AmountTmp = 0.0;
                ItemInvoiceModel InvoiceRec = new ItemInvoiceModel();
                ItemChequeModel ChequeRec = new ItemChequeModel();
                bool inloop = true;
                while (inloop)
                {
                    if (ChequeAmountTmp == 0.0)
                    {
                        if (indexCheque < ArrCheque.Length)
                        {
                            ChequeRec = ArrCheque[indexCheque];
                            ChequeAmountTmp = ChequeRec.ChequeAmount;
                        }
                    }
                    if (InvoiceAmountTmp == 0.0)
                    {
                        if (indexInvoice < ArrItemInvoiceModel.Length)
                        {
                            InvoiceRec = ArrItemInvoiceModel[indexInvoice];
                            InvoiceAmountTmp = InvoiceRec.ServiceType == "A" ? InvoiceRec.AmountIncVat : InvoiceRec.StopElectricityAmount;
                        }
                    }
                    MappChequeTmp = new ItemMapChequeModel();
                    if (ChequeAmountTmp < InvoiceAmountTmp)
                    {
                        AmountTmp = ChequeAmountTmp;
                        InvoiceAmountTmp = InvoiceAmountTmp - ChequeAmountTmp;
                        ChequeAmountTmp = 0.0;
                        indexCheque++;
                    }
                    else if (ChequeAmountTmp == InvoiceAmountTmp)
                    {
                        AmountTmp = ChequeAmountTmp;
                        ChequeAmountTmp = 0.0;
                        InvoiceAmountTmp = 0.0;
                        indexInvoice++;
                        indexCheque++;
                    }
                    else
                    {
                        AmountTmp = InvoiceAmountTmp;
                        ChequeAmountTmp = ChequeAmountTmp - InvoiceAmountTmp;
                        InvoiceAmountTmp = 0.0;
                        indexInvoice++;
                    }
                    MappChequeTmp.InvoiceNumber = InvoiceRec.InvoiceNumber;
                    MappChequeTmp.ChequeNumber = ChequeRec.ChequeNumber;
                    MappChequeTmp.Amount = AmountTmp;
                    LstMappCheque.Add(MappChequeTmp);
                    if (indexCheque == ArrCheque.Length || indexInvoice == ArrItemInvoiceModel.Length)
                        inloop = false;
                }
                OFC.InsertMapCheque(LstMappCheque);
                return LstMappCheque;
            }
            catch (Exception ex)
            {
                return LstMappCheque;
            }
        }
        public void MapCash(List<ItemMapChequeModel> LstMappCheque, List<ItemInvoiceModel> LstItemInvoiceModel)
        {
            List<ItemMapCashModel> LstMapCash = new List<ItemMapCashModel>();
            ItemMapCashModel MapCashModel = new ItemMapCashModel();
            try
            {
                double SumAmountCheque = 0.0;
                foreach (var ItemInvoiceModel in LstItemInvoiceModel)
                {
                    SumAmountCheque = LstMappCheque.Where(m => m.InvoiceNumber == ItemInvoiceModel.InvoiceNumber).Sum(m => m.Amount);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-GB");
                    string dtNow = DateTime.Now.ToString("yyyy-MM-dd", usCulture);
                    if (ItemInvoiceModel.ServiceType == "A")
                    {
                        if (SumAmountCheque < ItemInvoiceModel.AmountIncVat)
                        {
                            MapCashModel = new ItemMapCashModel();
                            MapCashModel.InvoiceNumber = ItemInvoiceModel.InvoiceNumber;
                            MapCashModel.Cash = ItemInvoiceModel.AmountIncVat - SumAmountCheque;
                            MapCashModel.CreateBy = GlobalClass.UserId.ToString();
                            MapCashModel.CreateDate = dtNow;
                            LstMapCash.Add(MapCashModel);
                        }
                    }
                    else
                    {
                        if (SumAmountCheque < ItemInvoiceModel.StopElectricityAmount)
                        {
                            MapCashModel = new ItemMapCashModel();
                            MapCashModel.InvoiceNumber = ItemInvoiceModel.InvoiceNumber;
                            MapCashModel.Cash = ItemInvoiceModel.StopElectricityAmount - SumAmountCheque;
                            MapCashModel.CreateBy = GlobalClass.UserId.ToString();
                            MapCashModel.CreateDate = dtNow;
                            LstMapCash.Add(MapCashModel);
                        }
                    }
                }
                OFC.InsertMapCash(LstMapCash);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.isCancel = true;
                this.Close();
                return;
                OFC.WriteOfflineTxtLog();
                return;
                GlobalClass.ReportName = "rptTaxInvoiceOffline.rpt";
                GlobalClass.ReportOfflineType = "TAXINVOICE";
                FormReport.FormMainViewReport FVR = new FormReport.FormMainViewReport();
                //formm receive = new FormReceiveMoney();
                FVR.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void Timer_CloseForm_Tick(object sender, EventArgs e)
        {
            this.isCancel = false;
            this.Close();
        }
        private void txtBankId_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else
                e.Handled = true;
        }
        private void txtChequeNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else
                e.Handled = true;
        }
        private void txtBankId_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.txtBankId.Text.Trim().Length == 7)
                {
                    var txtBankId = this.txtBankId.Text;
                    var rec = GB_lstBnk.Where(m => m.BankCode == txtBankId).FirstOrDefault();
                    if (rec != null)
                        this.txtBankName.Text = rec.BankName;
                    else
                        this.txtBankName.Text = "";
                }
                else
                    this.txtBankName.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void ButtonUpate_Click(object sender, EventArgs e)
        {
            //AddChequeMain();
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                if (CheckTextEmpty())
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    bool status = false;
                    double AmountCredit = 0;
                    double TotalAmountCheq = 0;
                    double TotalAmount = 0;
                    if (lblAmountTotalCheq.Text != "")
                        TotalAmountCheq = Convert.ToDouble(lblAmountTotalCheq.Text);
                    if (txtCash.Text != "")
                        TotalAmount = Convert.ToDouble(txtCash.Text);
                    if (this.GridDetail.SelectedRows.Count > 0)
                    {
                        ClassCheque cheque = _cheque.Find(item => item.No.Equals(lblNo.Text.ToString(), StringComparison.CurrentCultureIgnoreCase));
                        if (cheque != null)
                            status = false;
                        else
                        {
                            if (TotalAmount == 0 && AmountCredit == 0 && TotalAmountCheq > 0)
                            {
                                CountAmountCheq();
                                double amountSum = (TotalAmountCheq + Convert.ToDouble(txtCheque.Text)) - Convert.ToDouble(txtAmount.Text);
                                if (amountSum > 10)
                                    status = true;
                            }
                        }
                    }
                    else
                    {
                        if (TotalAmount == 0 && AmountCredit == 0 && Convert.ToDouble(txtCheque.Text) > 0)
                        {
                            double amountSum = Convert.ToDouble(txtCheque.Text) - Convert.ToDouble(txtAmount.Text);
                            if (amountSum > 10)
                                status = true;
                        }
                    }
                    if (status)
                    {
                        MessageBox.Show("มูลค่าเช็คที่ชำระเกินจากจำนวนเงินที่ต้องชำระมากกว่า 10 บาทไม่สามารถรับได้ ", "ชำระเกิน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    else
                    {
                        ClassCheque cheque = _cheque.Find(item => item.No.Equals(lblNo.Text.ToString(), StringComparison.CurrentCultureIgnoreCase));
                        if (cheque != null)
                        {
                            cheque.UserId = GlobalClass.UserCode;
                            cheque.Bankno = txtBankId.Text;
                            cheque.Bankname = txtBankName.Text;
                            cheque.Chdate = dpChequeDate.Text;
                            cheque.Chamount = Convert.ToDecimal(txtCheque.Text);
                            cheque.Chname = txtChequeName.Text;
                            cheque.ChdateNew = dpChequeDate.Value.Date.ToString("dd/MM/yyyy");
                            cheque.Tel = txtTelContact.Text.Replace("-", "").Trim();
                            cheque.Chcheck = chCashierCheque.Checked;
                            cheque.Chno = txtChequeNumber.Text;
                        }
                        this.GridDetail.DataSource = null;
                        this.GridDetail.DataSource = _cheque;
                        CountAmountCheq();
                        this.GridDetail.Update();
                        this.GridDetail.Refresh();
                        ButtonUpate.Enabled = false;
                        btnAdd.Enabled = true;
                        SumAmount();
                        ClearChequeData();
                    }
                }
            }
            catch (Exception ex)
            {
                ReceiveMoneyErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
        }

    }
}
