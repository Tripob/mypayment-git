﻿namespace MyPayment.FormOffline
{
    partial class FormOfflineReceive
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOfflineReceive));
            this.PN1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAmount = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.PN2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.PN3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblAmountTotalCheq = new System.Windows.Forms.Label();
            this.lblAmountCheq = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.GridDetail = new System.Windows.Forms.DataGridView();
            this.colDelete = new System.Windows.Forms.DataGridViewImageColumn();
            this.colEditNew = new System.Windows.Forms.DataGridViewImageColumn();
            this.colNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPayeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colchCash = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDateNew = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PN_CHEQUE_DETAIL = new System.Windows.Forms.Panel();
            this.lblNo = new System.Windows.Forms.Label();
            this.ButtonUpate = new Custom_Controls_in_CS.ButtonZ();
            this.txtBankName = new System.Windows.Forms.Label();
            this.txtChequeTel = new System.Windows.Forms.MaskedTextBox();
            this.btnAdd = new Custom_Controls_in_CS.ButtonZ();
            this.chCashierCheque = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtChequeName = new System.Windows.Forms.TextBox();
            this.lblB5 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCheque = new System.Windows.Forms.TextBox();
            this.dpChequeDate = new System.Windows.Forms.DateTimePicker();
            this.lblB2 = new System.Windows.Forms.Label();
            this.txtBankId = new System.Windows.Forms.TextBox();
            this.txtChequeNumber = new System.Windows.Forms.TextBox();
            this.lblB1 = new System.Windows.Forms.Label();
            this.lblB3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCash = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.PN4 = new System.Windows.Forms.Panel();
            this.btnConfirm = new Custom_Controls_in_CS.ButtonZ();
            this.btnCancel = new Custom_Controls_in_CS.ButtonZ();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtTelContact = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblStatusC = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblAmountChange = new System.Windows.Forms.Label();
            this.lblAmountReceived = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Timer_CloseForm = new System.Windows.Forms.Timer(this.components);
            this.PN1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.PN2.SuspendLayout();
            this.PN3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).BeginInit();
            this.PN_CHEQUE_DETAIL.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.PN4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN1
            // 
            this.PN1.Controls.Add(this.groupBox1);
            this.PN1.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN1.Location = new System.Drawing.Point(0, 0);
            this.PN1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.PN1.Name = "PN1";
            this.PN1.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN1.Size = new System.Drawing.Size(1008, 65);
            this.PN1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAmount);
            this.groupBox1.Controls.Add(this.Label8);
            this.groupBox1.Controls.Add(this.Label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(10, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.groupBox1.Size = new System.Drawing.Size(988, 65);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.Color.White;
            this.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.ForeColor = System.Drawing.Color.Blue;
            this.txtAmount.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtAmount.Location = new System.Drawing.Point(146, 23);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(301, 33);
            this.txtAmount.TabIndex = 1000000028;
            this.txtAmount.Text = "0.00";
            this.txtAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(450, 26);
            this.Label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(36, 26);
            this.Label8.TabIndex = 1000000027;
            this.Label8.Text = "บาท";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label3.Location = new System.Drawing.Point(4, 26);
            this.Label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(138, 26);
            this.Label3.TabIndex = 1000000025;
            this.Label3.Text = "จำนวนเงินที่ต้องชำระ";
            // 
            // PN2
            // 
            this.PN2.Controls.Add(this.label2);
            this.PN2.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN2.Location = new System.Drawing.Point(0, 65);
            this.PN2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.PN2.Name = "PN2";
            this.PN2.Size = new System.Drawing.Size(1008, 38);
            this.PN2.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(11, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 22);
            this.label2.TabIndex = 1000000026;
            this.label2.Text = "วิธีการชำระเงิน";
            // 
            // PN3
            // 
            this.PN3.Controls.Add(this.groupBox2);
            this.PN3.Controls.Add(this.groupBox3);
            this.PN3.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN3.Location = new System.Drawing.Point(0, 103);
            this.PN3.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.PN3.Name = "PN3";
            this.PN3.Padding = new System.Windows.Forms.Padding(10, 6, 10, 0);
            this.PN3.Size = new System.Drawing.Size(1008, 410);
            this.PN3.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblAmountTotalCheq);
            this.groupBox2.Controls.Add(this.lblAmountCheq);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.GridDetail);
            this.groupBox2.Controls.Add(this.PN_CHEQUE_DETAIL);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox2.Location = new System.Drawing.Point(10, 71);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(10, 0, 10, 4);
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox2.Size = new System.Drawing.Size(988, 339);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "รายการเช็ค";
            // 
            // lblAmountTotalCheq
            // 
            this.lblAmountTotalCheq.BackColor = System.Drawing.Color.White;
            this.lblAmountTotalCheq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAmountTotalCheq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountTotalCheq.ForeColor = System.Drawing.Color.Blue;
            this.lblAmountTotalCheq.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAmountTotalCheq.Location = new System.Drawing.Point(773, 291);
            this.lblAmountTotalCheq.Name = "lblAmountTotalCheq";
            this.lblAmountTotalCheq.Size = new System.Drawing.Size(204, 35);
            this.lblAmountTotalCheq.TabIndex = 8;
            this.lblAmountTotalCheq.Text = "0.00";
            this.lblAmountTotalCheq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAmountCheq
            // 
            this.lblAmountCheq.BackColor = System.Drawing.Color.White;
            this.lblAmountCheq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAmountCheq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountCheq.ForeColor = System.Drawing.Color.Blue;
            this.lblAmountCheq.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAmountCheq.Location = new System.Drawing.Point(469, 291);
            this.lblAmountCheq.Name = "lblAmountCheq";
            this.lblAmountCheq.Size = new System.Drawing.Size(171, 35);
            this.lblAmountCheq.TabIndex = 7;
            this.lblAmountCheq.Text = "0.00";
            this.lblAmountCheq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(639, 296);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(138, 26);
            this.label12.TabIndex = 9;
            this.label12.Text = "รวมมูลค่าเช็คทั้งหมด";
            // 
            // GridDetail
            // 
            this.GridDetail.AllowUserToAddRows = false;
            this.GridDetail.BackgroundColor = System.Drawing.Color.White;
            this.GridDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDelete,
            this.colEditNew,
            this.colNumber,
            this.colNo,
            this.colName,
            this.colDate,
            this.colAmount,
            this.colTel,
            this.colPayeeName,
            this.colchCash,
            this.ColBalance,
            this.colDateNew});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridDetail.DefaultCellStyle = dataGridViewCellStyle8;
            this.GridDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.GridDetail.Location = new System.Drawing.Point(10, 129);
            this.GridDetail.MultiSelect = false;
            this.GridDetail.Name = "GridDetail";
            this.GridDetail.ReadOnly = true;
            this.GridDetail.RowHeadersWidth = 10;
            this.GridDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridDetail.Size = new System.Drawing.Size(968, 149);
            this.GridDetail.TabIndex = 4;
            this.GridDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridDetail_CellClick);
            // 
            // colDelete
            // 
            this.colDelete.HeaderText = "";
            this.colDelete.Name = "colDelete";
            this.colDelete.ReadOnly = true;
            this.colDelete.ToolTipText = "ลบข้อมูล";
            this.colDelete.Width = 40;
            // 
            // colEditNew
            // 
            this.colEditNew.HeaderText = "";
            this.colEditNew.Name = "colEditNew";
            this.colEditNew.ReadOnly = true;
            this.colEditNew.ToolTipText = "แก้ไขข้อมูล";
            this.colEditNew.Width = 40;
            // 
            // colNumber
            // 
            this.colNumber.DataPropertyName = "Chno";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = null;
            this.colNumber.DefaultCellStyle = dataGridViewCellStyle1;
            this.colNumber.HeaderText = "เลขที่เช็ค";
            this.colNumber.Name = "colNumber";
            this.colNumber.ReadOnly = true;
            // 
            // colNo
            // 
            this.colNo.DataPropertyName = "Bankno";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.colNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.colNo.HeaderText = "รหัสธนาคาร";
            this.colNo.Name = "colNo";
            this.colNo.ReadOnly = true;
            this.colNo.Width = 120;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.colName.DataPropertyName = "Bankname";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.colName.DefaultCellStyle = dataGridViewCellStyle3;
            this.colName.HeaderText = "ชื่อธนาคาร/สาขา";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 138;
            // 
            // colDate
            // 
            this.colDate.DataPropertyName = "Chdate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colDate.DefaultCellStyle = dataGridViewCellStyle4;
            this.colDate.HeaderText = "วันที่เช็ค";
            this.colDate.Name = "colDate";
            this.colDate.ReadOnly = true;
            this.colDate.Width = 120;
            // 
            // colAmount
            // 
            this.colAmount.DataPropertyName = "Chamount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,###,###,##0.00";
            this.colAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.colAmount.HeaderText = "จำนวนเงิน";
            this.colAmount.Name = "colAmount";
            this.colAmount.ReadOnly = true;
            // 
            // colTel
            // 
            this.colTel.DataPropertyName = "Tel";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colTel.DefaultCellStyle = dataGridViewCellStyle6;
            this.colTel.HeaderText = "เบอร์โทร";
            this.colTel.Name = "colTel";
            this.colTel.ReadOnly = true;
            // 
            // colPayeeName
            // 
            this.colPayeeName.DataPropertyName = "Chname";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colPayeeName.DefaultCellStyle = dataGridViewCellStyle7;
            this.colPayeeName.HeaderText = "ชื่อผู้สั่งจ่าย";
            this.colPayeeName.Name = "colPayeeName";
            this.colPayeeName.ReadOnly = true;
            this.colPayeeName.Width = 200;
            // 
            // colchCash
            // 
            this.colchCash.DataPropertyName = "Chcheck";
            this.colchCash.HeaderText = "แคชเชียร์เช็ค";
            this.colchCash.Name = "colchCash";
            this.colchCash.ReadOnly = true;
            this.colchCash.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colchCash.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colchCash.Width = 120;
            // 
            // ColBalance
            // 
            this.ColBalance.DataPropertyName = "Balance";
            this.ColBalance.HeaderText = "คงเหลือ";
            this.ColBalance.Name = "ColBalance";
            this.ColBalance.ReadOnly = true;
            this.ColBalance.Visible = false;
            // 
            // colDateNew
            // 
            this.colDateNew.DataPropertyName = "ChdateNew";
            this.colDateNew.HeaderText = "DateNew";
            this.colDateNew.Name = "colDateNew";
            this.colDateNew.ReadOnly = true;
            this.colDateNew.Visible = false;
            // 
            // PN_CHEQUE_DETAIL
            // 
            this.PN_CHEQUE_DETAIL.Controls.Add(this.lblNo);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.ButtonUpate);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.txtBankName);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.txtChequeTel);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.btnAdd);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.chCashierCheque);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.label9);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.txtChequeName);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.lblB5);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.label5);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.label4);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.txtCheque);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.dpChequeDate);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.lblB2);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.txtBankId);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.txtChequeNumber);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.lblB1);
            this.PN_CHEQUE_DETAIL.Controls.Add(this.lblB3);
            this.PN_CHEQUE_DETAIL.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_CHEQUE_DETAIL.Location = new System.Drawing.Point(10, 26);
            this.PN_CHEQUE_DETAIL.Name = "PN_CHEQUE_DETAIL";
            this.PN_CHEQUE_DETAIL.Size = new System.Drawing.Size(968, 103);
            this.PN_CHEQUE_DETAIL.TabIndex = 0;
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.Enabled = false;
            this.lblNo.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblNo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblNo.Location = new System.Drawing.Point(891, 33);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(29, 26);
            this.lblNo.TabIndex = 1000000041;
            this.lblNo.Text = "No";
            this.lblNo.Visible = false;
            // 
            // ButtonUpate
            // 
            this.ButtonUpate.BackColor = System.Drawing.Color.Transparent;
            this.ButtonUpate.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonUpate.BorderWidth = 1;
            this.ButtonUpate.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonUpate.ButtonText = "อัพเดท";
            this.ButtonUpate.CausesValidation = false;
            this.ButtonUpate.Enabled = false;
            this.ButtonUpate.EndColor = System.Drawing.Color.Silver;
            this.ButtonUpate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonUpate.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonUpate.ForeColor = System.Drawing.Color.Black;
            this.ButtonUpate.GradientAngle = 90;
            this.ButtonUpate.Image = global::MyPayment.Properties.Resources.ok1;
            this.ButtonUpate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonUpate.Location = new System.Drawing.Point(830, 63);
            this.ButtonUpate.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonUpate.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonUpate.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonUpate.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonUpate.Name = "ButtonUpate";
            this.ButtonUpate.ShowButtontext = true;
            this.ButtonUpate.Size = new System.Drawing.Size(90, 28);
            this.ButtonUpate.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonUpate.TabIndex = 1000000040;
            this.ButtonUpate.TextLocation_X = 35;
            this.ButtonUpate.TextLocation_Y = 1;
            this.ButtonUpate.Transparent1 = 80;
            this.ButtonUpate.Transparent2 = 120;
            this.ButtonUpate.UseVisualStyleBackColor = true;
            this.ButtonUpate.Click += new System.EventHandler(this.ButtonUpate_Click);
            // 
            // txtBankName
            // 
            this.txtBankName.BackColor = System.Drawing.Color.White;
            this.txtBankName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBankName.Enabled = false;
            this.txtBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtBankName.ForeColor = System.Drawing.Color.Black;
            this.txtBankName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtBankName.Location = new System.Drawing.Point(557, 1);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(408, 29);
            this.txtBankName.TabIndex = 10;
            this.txtBankName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChequeTel
            // 
            this.txtChequeTel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChequeTel.Location = new System.Drawing.Point(415, 66);
            this.txtChequeTel.Mask = " 000-000-000000";
            this.txtChequeTel.Name = "txtChequeTel";
            this.txtChequeTel.Size = new System.Drawing.Size(199, 29);
            this.txtChequeTel.TabIndex = 5;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd.BorderColor = System.Drawing.Color.Transparent;
            this.btnAdd.BorderWidth = 1;
            this.btnAdd.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btnAdd.ButtonText = "เพิ่ม";
            this.btnAdd.CausesValidation = false;
            this.btnAdd.EndColor = System.Drawing.Color.DarkGray;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.GradientAngle = 90;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(741, 63);
            this.btnAdd.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btnAdd.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnAdd.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btnAdd.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ShowButtontext = true;
            this.btnAdd.Size = new System.Drawing.Size(83, 28);
            this.btnAdd.StartColor = System.Drawing.Color.LightGray;
            this.btnAdd.TabIndex = 7;
            this.btnAdd.TextLocation_X = 32;
            this.btnAdd.TextLocation_Y = 1;
            this.btnAdd.Transparent1 = 80;
            this.btnAdd.Transparent2 = 120;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // chCashierCheque
            // 
            this.chCashierCheque.AutoSize = true;
            this.chCashierCheque.Font = new System.Drawing.Font("TH Sarabun New", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chCashierCheque.ForeColor = System.Drawing.Color.Black;
            this.chCashierCheque.Location = new System.Drawing.Point(625, 65);
            this.chCashierCheque.Name = "chCashierCheque";
            this.chCashierCheque.Size = new System.Drawing.Size(115, 30);
            this.chCashierCheque.TabIndex = 6;
            this.chCashierCheque.Text = "แคชเชียร์เช็ค";
            this.chCashierCheque.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(350, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 26);
            this.label9.TabIndex = 1000000036;
            this.label9.Text = "เบอร์โทร";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChequeName
            // 
            this.txtChequeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtChequeName.Location = new System.Drawing.Point(80, 66);
            this.txtChequeName.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtChequeName.Name = "txtChequeName";
            this.txtChequeName.Size = new System.Drawing.Size(240, 29);
            this.txtChequeName.TabIndex = 4;
            // 
            // lblB5
            // 
            this.lblB5.AutoSize = true;
            this.lblB5.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB5.Location = new System.Drawing.Point(6, 67);
            this.lblB5.Name = "lblB5";
            this.lblB5.Size = new System.Drawing.Size(76, 26);
            this.lblB5.TabIndex = 1000000033;
            this.lblB5.Text = "ชื่อผู้สั่งจ่าย";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(620, 35);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 26);
            this.label5.TabIndex = 1000000032;
            this.label5.Text = "บาท";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(340, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 26);
            this.label4.TabIndex = 1000000031;
            this.label4.Text = "จำนวนเงิน";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCheque
            // 
            this.txtCheque.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCheque.Location = new System.Drawing.Point(415, 32);
            this.txtCheque.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtCheque.Name = "txtCheque";
            this.txtCheque.Size = new System.Drawing.Size(199, 29);
            this.txtCheque.TabIndex = 3;
            this.txtCheque.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCheque_KeyPress);
            // 
            // dpChequeDate
            // 
            this.dpChequeDate.CustomFormat = "dd/MM/yyyy";
            this.dpChequeDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dpChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpChequeDate.Location = new System.Drawing.Point(117, 33);
            this.dpChequeDate.Name = "dpChequeDate";
            this.dpChequeDate.Size = new System.Drawing.Size(203, 29);
            this.dpChequeDate.TabIndex = 2;
            // 
            // lblB2
            // 
            this.lblB2.AutoSize = true;
            this.lblB2.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB2.Location = new System.Drawing.Point(5, 35);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(114, 26);
            this.lblB2.TabIndex = 1000000028;
            this.lblB2.Text = "วันที่ในเช็ค(พ.ศ.)";
            this.lblB2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBankId
            // 
            this.txtBankId.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtBankId.Location = new System.Drawing.Point(415, 1);
            this.txtBankId.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtBankId.MaxLength = 7;
            this.txtBankId.Name = "txtBankId";
            this.txtBankId.Size = new System.Drawing.Size(137, 29);
            this.txtBankId.TabIndex = 1;
            this.txtBankId.TextChanged += new System.EventHandler(this.txtBankId_TextChanged);
            this.txtBankId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBankId_KeyPress);
            // 
            // txtChequeNumber
            // 
            this.txtChequeNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtChequeNumber.Location = new System.Drawing.Point(72, 1);
            this.txtChequeNumber.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtChequeNumber.MaxLength = 9;
            this.txtChequeNumber.Name = "txtChequeNumber";
            this.txtChequeNumber.Size = new System.Drawing.Size(199, 29);
            this.txtChequeNumber.TabIndex = 0;
            this.txtChequeNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtChequeNumber_KeyPress);
            // 
            // lblB1
            // 
            this.lblB1.AutoSize = true;
            this.lblB1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB1.Location = new System.Drawing.Point(5, 4);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(69, 26);
            this.lblB1.TabIndex = 68;
            this.lblB1.Text = "เลขที่เช็ค";
            // 
            // lblB3
            // 
            this.lblB3.AutoSize = true;
            this.lblB3.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB3.Location = new System.Drawing.Point(293, 4);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(120, 26);
            this.lblB3.TabIndex = 69;
            this.lblB3.Text = "รหัสธนาคาร/สาขา";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtCash);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox3.Location = new System.Drawing.Point(10, 6);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox3.Size = new System.Drawing.Size(988, 65);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "เงินสด";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(332, 27);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 26);
            this.label7.TabIndex = 2;
            this.label7.Text = "บาท";
            // 
            // txtCash
            // 
            this.txtCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCash.Location = new System.Drawing.Point(100, 22);
            this.txtCash.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(230, 35);
            this.txtCash.TabIndex = 0;
            this.txtCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCash.TextChanged += new System.EventHandler(this.txtCash_TextChanged);
            this.txtCash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCash_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(23, 27);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 26);
            this.label6.TabIndex = 1;
            this.label6.Text = "จำนวนเงิน";
            // 
            // PN4
            // 
            this.PN4.Controls.Add(this.btnConfirm);
            this.PN4.Controls.Add(this.btnCancel);
            this.PN4.Controls.Add(this.groupBox4);
            this.PN4.Controls.Add(this.groupBox5);
            this.PN4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN4.Location = new System.Drawing.Point(0, 513);
            this.PN4.Name = "PN4";
            this.PN4.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN4.Size = new System.Drawing.Size(1008, 216);
            this.PN4.TabIndex = 5;
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.Transparent;
            this.btnConfirm.BorderColor = System.Drawing.Color.Transparent;
            this.btnConfirm.BorderWidth = 1;
            this.btnConfirm.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btnConfirm.ButtonText = "ยืนยัน";
            this.btnConfirm.CausesValidation = false;
            this.btnConfirm.Enabled = false;
            this.btnConfirm.EndColor = System.Drawing.Color.Silver;
            this.btnConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirm.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnConfirm.ForeColor = System.Drawing.Color.Black;
            this.btnConfirm.GradientAngle = 90;
            this.btnConfirm.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirm.Image")));
            this.btnConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirm.Location = new System.Drawing.Point(799, 180);
            this.btnConfirm.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btnConfirm.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnConfirm.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btnConfirm.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.ShowButtontext = true;
            this.btnConfirm.Size = new System.Drawing.Size(90, 28);
            this.btnConfirm.StartColor = System.Drawing.Color.Gainsboro;
            this.btnConfirm.TabIndex = 5;
            this.btnConfirm.TextLocation_X = 35;
            this.btnConfirm.TextLocation_Y = 1;
            this.btnConfirm.Transparent1 = 80;
            this.btnConfirm.Transparent2 = 120;
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BorderColor = System.Drawing.Color.Transparent;
            this.btnCancel.BorderWidth = 1;
            this.btnCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btnCancel.ButtonText = "ยกเลิก";
            this.btnCancel.CausesValidation = false;
            this.btnCancel.EndColor = System.Drawing.Color.Silver;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCancel.GradientAngle = 90;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(896, 180);
            this.btnCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btnCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btnCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.ShowButtontext = true;
            this.btnCancel.Size = new System.Drawing.Size(90, 28);
            this.btnCancel.StartColor = System.Drawing.Color.Gainsboro;
            this.btnCancel.TabIndex = 6;
            this.btnCancel.TextLocation_X = 35;
            this.btnCancel.TextLocation_Y = 1;
            this.btnCancel.Transparent1 = 80;
            this.btnCancel.Transparent2 = 120;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtTelContact);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.lblStatusC);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.lblAmountChange);
            this.groupBox4.Controls.Add(this.lblAmountReceived);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(10, 60);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(988, 114);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // txtTelContact
            // 
            this.txtTelContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelContact.Location = new System.Drawing.Point(255, 68);
            this.txtTelContact.Mask = " 000-000-0000";
            this.txtTelContact.Name = "txtTelContact";
            this.txtTelContact.Size = new System.Drawing.Size(191, 31);
            this.txtTelContact.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(130, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 26);
            this.label10.TabIndex = 1000000035;
            this.label10.Text = "หมายเลขติดต่อกลับ";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblStatusC
            // 
            this.lblStatusC.BackColor = System.Drawing.Color.LightGray;
            this.lblStatusC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatusC.Font = new System.Drawing.Font("TH Sarabun New", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblStatusC.ForeColor = System.Drawing.Color.Red;
            this.lblStatusC.Location = new System.Drawing.Point(74, 18);
            this.lblStatusC.Name = "lblStatusC";
            this.lblStatusC.Size = new System.Drawing.Size(215, 35);
            this.lblStatusC.TabIndex = 1000000033;
            this.lblStatusC.Text = "***ห้ามรับเช็ค***";
            this.lblStatusC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblStatusC.Visible = false;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.White;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label28.Location = new System.Drawing.Point(371, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(103, 30);
            this.label28.TabIndex = 1000000032;
            this.label28.Text = "0.00";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAmountChange
            // 
            this.lblAmountChange.BackColor = System.Drawing.Color.White;
            this.lblAmountChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAmountChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountChange.ForeColor = System.Drawing.Color.Red;
            this.lblAmountChange.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAmountChange.Location = new System.Drawing.Point(579, 63);
            this.lblAmountChange.Name = "lblAmountChange";
            this.lblAmountChange.Size = new System.Drawing.Size(397, 40);
            this.lblAmountChange.TabIndex = 1000000031;
            this.lblAmountChange.Text = "0.00";
            this.lblAmountChange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAmountReceived
            // 
            this.lblAmountReceived.BackColor = System.Drawing.Color.White;
            this.lblAmountReceived.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAmountReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountReceived.ForeColor = System.Drawing.Color.Blue;
            this.lblAmountReceived.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAmountReceived.Location = new System.Drawing.Point(579, 18);
            this.lblAmountReceived.Name = "lblAmountReceived";
            this.lblAmountReceived.Size = new System.Drawing.Size(397, 40);
            this.lblAmountReceived.TabIndex = 1000000030;
            this.lblAmountReceived.Text = "0.00";
            this.lblAmountReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(480, 27);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(99, 26);
            this.label13.TabIndex = 1000000029;
            this.label13.Text = "จำนวนเงินที่รับ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(520, 70);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 26);
            this.label15.TabIndex = 1000000027;
            this.label15.Text = "เงินทอน";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(295, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 26);
            this.label16.TabIndex = 1000000028;
            this.label16.Text = "การปัดเศษ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.Label14);
            this.groupBox5.Controls.Add(this.Label20);
            this.groupBox5.Controls.Add(this.Label18);
            this.groupBox5.Controls.Add(this.Label17);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(10, 0);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(988, 60);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.White;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label25.Location = new System.Drawing.Point(579, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(397, 35);
            this.label25.TabIndex = 1000000021;
            this.label25.Text = "0.00";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.White;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label24.Location = new System.Drawing.Point(205, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(147, 35);
            this.label24.TabIndex = 1000000021;
            this.label24.Text = "0.00";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label14.Location = new System.Drawing.Point(520, 21);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(58, 26);
            this.Label14.TabIndex = 68;
            this.Label14.Text = "เงินขาด";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label20.Location = new System.Drawing.Point(353, 21);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(27, 26);
            this.Label20.TabIndex = 65;
            this.Label20.Text = "ใบ";
            this.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label18.Location = new System.Drawing.Point(110, 21);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(96, 26);
            this.Label18.TabIndex = 66;
            this.Label18.Text = "จำนวนใบเสร็จ";
            this.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label17.Location = new System.Drawing.Point(386, 21);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(137, 26);
            this.Label17.TabIndex = 67;
            this.Label17.Text = "จำนวนเงินรับล่วงหน้า";
            this.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Label17.Visible = false;
            // 
            // Timer_CloseForm
            // 
            this.Timer_CloseForm.Tick += new System.EventHandler(this.Timer_CloseForm_Tick);
            // 
            // FormOfflineReceive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(4F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.PN4);
            this.Controls.Add(this.PN3);
            this.Controls.Add(this.PN2);
            this.Controls.Add(this.PN1);
            this.Font = new System.Drawing.Font("TH Sarabun New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "FormOfflineReceive";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormOfflineReceive";
            this.Load += new System.EventHandler(this.FormOfflineReceive_Load);
            this.PN1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.PN2.ResumeLayout(false);
            this.PN2.PerformLayout();
            this.PN3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).EndInit();
            this.PN_CHEQUE_DETAIL.ResumeLayout(false);
            this.PN_CHEQUE_DETAIL.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.PN4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN1;
        private System.Windows.Forms.Panel PN2;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel PN3;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCash;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel PN_CHEQUE_DETAIL;
        private System.Windows.Forms.TextBox txtBankId;
        private System.Windows.Forms.TextBox txtChequeNumber;
        internal System.Windows.Forms.Label lblB1;
        internal System.Windows.Forms.Label lblB3;
        internal System.Windows.Forms.Label lblB2;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCheque;
        private System.Windows.Forms.DateTimePicker dpChequeDate;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtChequeName;
        internal System.Windows.Forms.Label lblB5;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.CheckBox chCashierCheque;
        internal System.Windows.Forms.Label lblAmountTotalCheq;
        internal System.Windows.Forms.Label lblAmountCheq;
        internal System.Windows.Forms.Label label12;
        private Custom_Controls_in_CS.ButtonZ btnAdd;
        private System.Windows.Forms.Panel PN4;
        private System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Label Label17;
        private System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label lblStatusC;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label lblAmountChange;
        internal System.Windows.Forms.Label lblAmountReceived;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label16;
        private Custom_Controls_in_CS.ButtonZ btnConfirm;
        private Custom_Controls_in_CS.ButtonZ btnCancel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer Timer_CloseForm;
        private System.Windows.Forms.MaskedTextBox txtChequeTel;
        private System.Windows.Forms.MaskedTextBox txtTelContact;
        internal System.Windows.Forms.Label txtBankName;
        internal System.Windows.Forms.Label txtAmount;
        internal System.Windows.Forms.DataGridView GridDetail;
        private Custom_Controls_in_CS.ButtonZ ButtonUpate;
        internal System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.DataGridViewImageColumn colDelete;
        private System.Windows.Forms.DataGridViewImageColumn colEditNew;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPayeeName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colchCash;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDateNew;
    }
}