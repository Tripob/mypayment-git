﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Transactions;
using System.Data.SQLite;
using MyPayment.Models;
using MyPayment.Master;

namespace MyPayment.FormOffline
{
    public partial class FormBillCancelDetail : Form
    {

        public static string GB_BillNumber = "";
        private static OffLineClass OFC = new OffLineClass();

        public FormBillCancelDetail()
        {
            InitializeComponent();
        }

        private void FormBillCancelDetail_Load(object sender, EventArgs e)
        {
            // MessageBox.Show(GB_BillNumber);
            GetData(GB_BillNumber);
        }
        public void SetBillNumber(string BillNumber)
        {
            try
            {
                GB_BillNumber = BillNumber;
            }
            catch (Exception ex)
            {

            }
        }
        public void GetData(string BillNumber)
        {
            try
            {
                List<ItemInvoiceModel> obj = OFC.GetBillCancelDetail(BillNumber);
                GridBillCancelDetail.AutoGenerateColumns = false;
                GridBillCancelDetail.DataSource = obj;
            }
            catch (Exception ex)
            {

            }
        }

    }
}
