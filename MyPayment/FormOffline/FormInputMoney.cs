﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using MyPayment.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace MyPayment.FormOffline
{
    public partial class FormInputMoney : Form
    {
        public FormInputMoney()
        {
            InitializeComponent();
        }

        public string InputMoney { get; set; }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                //GlobalClass.Data = this.txtMoney.Text.Trim();
                GlobalClass.StatusForm = 1;
                this.InputMoney = this.txtMoney.Text.Trim();
                GlobalClass.Status = this.chFeeElectricity.Checked;
                GlobalClass.Remark = "";
                if (this.chFeeElectricity.Checked)
                {
                    if (this.rdYes.Checked)
                        GlobalClass.Remark = "Y";
                    else
                        GlobalClass.Remark = "N";
                }
                this.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            GlobalClass.StatusForm = 0;
            this.txtMoney.Text = "";
            this.Close();
        }

        private void txtMoney_KeyDown(object sender, KeyEventArgs e)
        {
            //try
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    { 

            //        this.SelectNextControl(this.txtMoney, true, true, true, true);
            //    }
            //}
            //catch (Exception ex)
            //{

            //}
        }

        private void txtMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;

            int cInt = Convert.ToInt32(e.KeyChar);
            if (e.KeyChar.Equals(Keys.Tab) || cInt == 13)
                btnOK.Focus();
        }

        private void chFeeElectricity_CheckedChanged(object sender, EventArgs e)
        {
            if (chFeeElectricity.Checked)
            {
                rdNo.Enabled = true;
                rdYes.Enabled = true;
                rdNo.Checked = true;
            }
            else
            {
                rdNo.Checked = false;
                rdYes.Checked = false;
                rdNo.Enabled = false;
                rdYes.Enabled = false;
            }
        }
    }
}
