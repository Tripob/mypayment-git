﻿namespace MyPayment.FormOffline
{
    partial class FormBillCancel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBillCancel));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PN1 = new System.Windows.Forms.Panel();
            this.PN2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GridBillCancel = new System.Windows.Forms.DataGridView();
            this.PN3 = new System.Windows.Forms.Panel();
            this.CheckBoxSelect = new System.Windows.Forms.CheckBox();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.Label3 = new System.Windows.Forms.Label();
            this.ButtonConfirm = new Custom_Controls_in_CS.ButtonZ();
            this.colCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BillNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DebtCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PN1.SuspendLayout();
            this.PN2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridBillCancel)).BeginInit();
            this.PN3.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN1
            // 
            this.PN1.Controls.Add(this.DateTimePicker);
            this.PN1.Controls.Add(this.Label3);
            this.PN1.Controls.Add(this.ButtonSearch);
            this.PN1.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN1.Location = new System.Drawing.Point(0, 0);
            this.PN1.Name = "PN1";
            this.PN1.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN1.Size = new System.Drawing.Size(1012, 57);
            this.PN1.TabIndex = 0;
            // 
            // PN2
            // 
            this.PN2.Controls.Add(this.groupBox1);
            this.PN2.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN2.Location = new System.Drawing.Point(0, 57);
            this.PN2.Name = "PN2";
            this.PN2.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN2.Size = new System.Drawing.Size(1012, 603);
            this.PN2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CheckBoxSelect);
            this.groupBox1.Controls.Add(this.GridBillCancel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(10, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10, 3, 10, 4);
            this.groupBox1.Size = new System.Drawing.Size(992, 603);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รายการใบเสร็จ";
            // 
            // GridBillCancel
            // 
            this.GridBillCancel.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridBillCancel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridBillCancel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridBillCancel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCheck,
            this.BillNumber,
            this.DebtCode,
            this.InvoiceNumber});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridBillCancel.DefaultCellStyle = dataGridViewCellStyle5;
            this.GridBillCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridBillCancel.Location = new System.Drawing.Point(10, 28);
            this.GridBillCancel.Name = "GridBillCancel";
            this.GridBillCancel.RowHeadersVisible = false;
            this.GridBillCancel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridBillCancel.Size = new System.Drawing.Size(972, 571);
            this.GridBillCancel.TabIndex = 0;
            this.GridBillCancel.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridBillCancel_CellContentClick);
            this.GridBillCancel.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridBillCancel_CellContentDoubleClick);
            // 
            // PN3
            // 
            this.PN3.Controls.Add(this.ButtonConfirm);
            this.PN3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PN3.Location = new System.Drawing.Point(0, 654);
            this.PN3.Name = "PN3";
            this.PN3.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN3.Size = new System.Drawing.Size(1012, 51);
            this.PN3.TabIndex = 2;
            // 
            // CheckBoxSelect
            // 
            this.CheckBoxSelect.AutoSize = true;
            this.CheckBoxSelect.Location = new System.Drawing.Point(20, 39);
            this.CheckBoxSelect.Name = "CheckBoxSelect";
            this.CheckBoxSelect.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelect.TabIndex = 1000000049;
            this.CheckBoxSelect.UseVisualStyleBackColor = true;
            this.CheckBoxSelect.Click += new System.EventHandler(this.CheckBoxSelect_Click);
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "ค้นหา";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.Silver;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSearch.Image")));
            this.ButtonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSearch.Location = new System.Drawing.Point(253, 13);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(90, 28);
            this.ButtonSearch.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.TabIndex = 1000000044;
            this.ButtonSearch.TextLocation_X = 35;
            this.ButtonSearch.TextLocation_Y = 1;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker.Location = new System.Drawing.Point(64, 14);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(171, 26);
            this.DateTimePicker.TabIndex = 1000000046;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(15, 13);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(41, 28);
            this.Label3.TabIndex = 1000000045;
            this.Label3.Text = "วันที่";
            // 
            // ButtonConfirm
            // 
            this.ButtonConfirm.BackColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderWidth = 1;
            this.ButtonConfirm.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonConfirm.ButtonText = "ยืนยัน";
            this.ButtonConfirm.CausesValidation = false;
            this.ButtonConfirm.EndColor = System.Drawing.Color.Silver;
            this.ButtonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonConfirm.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonConfirm.ForeColor = System.Drawing.Color.Black;
            this.ButtonConfirm.GradientAngle = 90;
            this.ButtonConfirm.Image = global::MyPayment.Properties.Resources.ok1;
            this.ButtonConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonConfirm.Location = new System.Drawing.Point(898, 12);
            this.ButtonConfirm.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonConfirm.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonConfirm.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonConfirm.Name = "ButtonConfirm";
            this.ButtonConfirm.ShowButtontext = true;
            this.ButtonConfirm.Size = new System.Drawing.Size(90, 28);
            this.ButtonConfirm.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.TabIndex = 17;
            this.ButtonConfirm.TextLocation_X = 35;
            this.ButtonConfirm.TextLocation_Y = 1;
            this.ButtonConfirm.Transparent1 = 80;
            this.ButtonConfirm.Transparent2 = 120;
            this.ButtonConfirm.UseVisualStyleBackColor = true;
            this.ButtonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // colCheck
            // 
            this.colCheck.DataPropertyName = "Selected";
            this.colCheck.HeaderText = "";
            this.colCheck.Name = "colCheck";
            this.colCheck.Width = 30;
            // 
            // BillNumber
            // 
            this.BillNumber.DataPropertyName = "BillNumber";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BillNumber.DefaultCellStyle = dataGridViewCellStyle2;
            this.BillNumber.HeaderText = "เลขที่ใบเสร็จรับเงิน";
            this.BillNumber.Name = "BillNumber";
            this.BillNumber.Width = 220;
            // 
            // DebtCode
            // 
            this.DebtCode.DataPropertyName = "AmountIncVat";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.DebtCode.DefaultCellStyle = dataGridViewCellStyle3;
            this.DebtCode.HeaderText = "จำนวนเงิน(บาท)";
            this.DebtCode.Name = "DebtCode";
            this.DebtCode.Width = 170;
            // 
            // InvoiceNumber
            // 
            this.InvoiceNumber.DataPropertyName = "Remark";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.InvoiceNumber.DefaultCellStyle = dataGridViewCellStyle4;
            this.InvoiceNumber.HeaderText = "เหตุผลที่ยกเลิก";
            this.InvoiceNumber.Name = "InvoiceNumber";
            this.InvoiceNumber.Width = 550;
            // 
            // FormBillCancel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.PN3);
            this.Controls.Add(this.PN2);
            this.Controls.Add(this.PN1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBillCancel";
            this.Text = "FormBillCancel";
            this.PN1.ResumeLayout(false);
            this.PN1.PerformLayout();
            this.PN2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridBillCancel)).EndInit();
            this.PN3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN1;
        private System.Windows.Forms.Panel PN2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView GridBillCancel;
        private System.Windows.Forms.Panel PN3;
        private System.Windows.Forms.CheckBox CheckBoxSelect;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        internal System.Windows.Forms.Label Label3;
        private Custom_Controls_in_CS.ButtonZ ButtonConfirm;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn DebtCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceNumber;
    }
}