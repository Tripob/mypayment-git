﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using MyPayment.Master;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Transactions;
using System.Data.SQLite;
using MyPayment.Models;
using System.Globalization;
using System.Configuration;
using System.Threading;

namespace MyPayment.FormOffline
{
    public partial class FormBillCancel : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        //  public static string DBPath = @"Data Source=" + @"D:\MEA" + @"\Data\MyPaymentDB.db;Version=3;New=False;Compress=True;";
        public static string DBPath = @"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + @"\Data\MyPaymentDB.db;Version=3;New=False;Compress=True;";
        public FormBillCancel()
        {
            InitializeComponent();
        }
        private void GridBillCancel_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    FormOffline.FormBillCancelDetail detail = new FormOffline.FormBillCancelDetail();
                    string BillNumber = GridBillCancel.CurrentRow.Cells["BillNumber"].Value.ToString();
                    detail.SetBillNumber(BillNumber);
                    detail.ShowDialog();
                    detail.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
            string strDate = DateTimePicker.Value.ToString("yyyy-MM-dd");
            List<ItemBillCancelModel> obj = OFC.GetAllBill(strDate);
            GridBillCancel.AutoGenerateColumns = false;
            GridBillCancel.DataSource = obj;
        }
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (GridBillCancel.Rows.Count > 0)
                {
                    List<ItemBillCancelModel> LstItem = new List<ItemBillCancelModel>();
                    var DataSource = this.GridBillCancel.DataSource;
                    LstItem = (List<ItemBillCancelModel>)DataSource;
                    bool result = OFC.UpdateBillCancel(LstItem);
                    if (result)
                        MessageBox.Show("บันทึกข้อมูลแล้ว", "Information");
                    else
                        MessageBox.Show("ไม่สามารถบันทึกข้อมูล", "Warning");
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void CheckBoxSelect_Click(object sender, EventArgs e)
        {
            if (GridBillCancel.Rows.Count > 0)
            {
                int i = 0;
                var loopTo = GridBillCancel.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (CheckBoxSelect.Checked == true)
                        GridBillCancel.Rows[i].Cells["colCheck"].Value = true;
                    else
                        GridBillCancel.Rows[i].Cells["colCheck"].Value = false;
                }
            }
        }
        private void GridBillCancel_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                int i = 0;
                int countCheck = 0;
                var loopTo = GridBillCancel.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (GridBillCancel.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridBillCancel.Rows[i].Cells["colCheck"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        if (newBool)
                        {
                            GridBillCancel.Rows[i].Cells[0].Value = true;
                            countCheck++;
                            if (countCheck == GridBillCancel.Rows.Count)
                                CheckBoxSelect.Checked = true;
                        }
                        else
                        {
                            GridBillCancel.Rows[i].Cells[0].Value = false;
                            CheckBoxSelect.Checked = false;
                            if (countCheck == 0)
                                countCheck = 0;
                        }
                    }
                }
            }
        }
    }
}
