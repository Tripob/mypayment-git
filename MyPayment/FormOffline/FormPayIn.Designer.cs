﻿namespace MyPayment.FormOffline
{
    partial class FormPayIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPayIn));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbDetail = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btSearch = new Custom_Controls_in_CS.ButtonZ();
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.Label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvDetail = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonSendData = new Custom_Controls_in_CS.ButtonZ();
            this.CheckBoxSelect = new System.Windows.Forms.CheckBox();
            this.colCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.calDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceipt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbDetail);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btSearch);
            this.groupBox1.Controls.Add(this.DateTimePicker);
            this.groupBox1.Controls.Add(this.Label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1012, 56);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // cbDetail
            // 
            this.cbDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDetail.FormattingEnabled = true;
            this.cbDetail.Items.AddRange(new object[] {
            "รอนำส่่ง",
            "นำส่งแล้ว",
            "นำส่งไม่ผ่าน"});
            this.cbDetail.Location = new System.Drawing.Point(297, 12);
            this.cbDetail.Name = "cbDetail";
            this.cbDetail.Size = new System.Drawing.Size(186, 28);
            this.cbDetail.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(236, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 28);
            this.label1.TabIndex = 1000000051;
            this.label1.Text = "สถานะ";
            // 
            // btSearch
            // 
            this.btSearch.BackColor = System.Drawing.Color.Transparent;
            this.btSearch.BorderColor = System.Drawing.Color.Transparent;
            this.btSearch.BorderWidth = 1;
            this.btSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btSearch.ButtonText = "ค้นหา";
            this.btSearch.CausesValidation = false;
            this.btSearch.EndColor = System.Drawing.Color.Silver;
            this.btSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSearch.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.btSearch.ForeColor = System.Drawing.Color.Black;
            this.btSearch.GradientAngle = 90;
            this.btSearch.Image = ((System.Drawing.Image)(resources.GetObject("btSearch.Image")));
            this.btSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSearch.Location = new System.Drawing.Point(504, 14);
            this.btSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btSearch.Name = "btSearch";
            this.btSearch.ShowButtontext = true;
            this.btSearch.Size = new System.Drawing.Size(90, 28);
            this.btSearch.StartColor = System.Drawing.Color.Gainsboro;
            this.btSearch.TabIndex = 2;
            this.btSearch.TextLocation_X = 35;
            this.btSearch.TextLocation_Y = 1;
            this.btSearch.Transparent1 = 80;
            this.btSearch.Transparent2 = 120;
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker.Location = new System.Drawing.Point(59, 16);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(171, 26);
            this.DateTimePicker.TabIndex = 0;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(17, 15);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(41, 28);
            this.Label3.TabIndex = 1000000044;
            this.Label3.Text = "วันที่";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CheckBoxSelect);
            this.groupBox2.Controls.Add(this.dgvDetail);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 56);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1012, 604);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // dgvDetail
            // 
            this.dgvDetail.AllowUserToAddRows = false;
            this.dgvDetail.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCheck,
            this.calDate,
            this.colCa,
            this.colInv,
            this.colReceipt,
            this.colAmount,
            this.colStatus});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDetail.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetail.Location = new System.Drawing.Point(3, 16);
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.RowHeadersVisible = false;
            this.dgvDetail.Size = new System.Drawing.Size(1006, 585);
            this.dgvDetail.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonSendData);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 660);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox3.Size = new System.Drawing.Size(1012, 45);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // buttonSendData
            // 
            this.buttonSendData.BackColor = System.Drawing.Color.Transparent;
            this.buttonSendData.BorderColor = System.Drawing.Color.Transparent;
            this.buttonSendData.BorderWidth = 1;
            this.buttonSendData.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.buttonSendData.ButtonText = "ส่งข้อมูล";
            this.buttonSendData.CausesValidation = false;
            this.buttonSendData.EndColor = System.Drawing.Color.Silver;
            this.buttonSendData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSendData.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.buttonSendData.ForeColor = System.Drawing.Color.Black;
            this.buttonSendData.GradientAngle = 90;
            this.buttonSendData.Image = global::MyPayment.Properties.Resources.Running;
            this.buttonSendData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSendData.Location = new System.Drawing.Point(903, 12);
            this.buttonSendData.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.buttonSendData.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonSendData.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.buttonSendData.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.buttonSendData.Name = "buttonSendData";
            this.buttonSendData.ShowButtontext = true;
            this.buttonSendData.Size = new System.Drawing.Size(96, 28);
            this.buttonSendData.StartColor = System.Drawing.Color.Gainsboro;
            this.buttonSendData.TabIndex = 0;
            this.buttonSendData.TextLocation_X = 30;
            this.buttonSendData.TextLocation_Y = 3;
            this.buttonSendData.Transparent1 = 80;
            this.buttonSendData.Transparent2 = 120;
            this.buttonSendData.UseVisualStyleBackColor = true;
            this.buttonSendData.Click += new System.EventHandler(this.buttonSendData_Click);
            // 
            // CheckBoxSelect
            // 
            this.CheckBoxSelect.AutoSize = true;
            this.CheckBoxSelect.Location = new System.Drawing.Point(16, 26);
            this.CheckBoxSelect.Name = "CheckBoxSelect";
            this.CheckBoxSelect.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelect.TabIndex = 1000000051;
            this.CheckBoxSelect.UseVisualStyleBackColor = true;
            this.CheckBoxSelect.Click += new System.EventHandler(this.CheckBoxSelect_Click);
            // 
            // colCheck
            // 
            this.colCheck.Frozen = true;
            this.colCheck.HeaderText = "";
            this.colCheck.Name = "colCheck";
            this.colCheck.Width = 40;
            // 
            // calDate
            // 
            this.calDate.DataPropertyName = "CreateDate";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.calDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.calDate.HeaderText = "วันที่";
            this.calDate.Name = "calDate";
            this.calDate.Width = 140;
            // 
            // colCa
            // 
            this.colCa.DataPropertyName = "CA";
            this.colCa.HeaderText = "บัญชีแสดงสัญญา";
            this.colCa.Name = "colCa";
            this.colCa.Width = 160;
            // 
            // colInv
            // 
            this.colInv.DataPropertyName = "InvoiceNumber";
            this.colInv.HeaderText = "เลขที่ใบแจ้ง";
            this.colInv.Name = "colInv";
            this.colInv.Width = 180;
            // 
            // colReceipt
            // 
            this.colReceipt.DataPropertyName = "ReceiptNumber";
            this.colReceipt.HeaderText = "เลขที่ใบเสร็จ";
            this.colReceipt.Name = "colReceipt";
            this.colReceipt.Width = 190;
            // 
            // colAmount
            // 
            this.colAmount.DataPropertyName = "TotalAll";
            this.colAmount.HeaderText = "จำนวนเงิน";
            this.colAmount.Name = "colAmount";
            this.colAmount.Width = 190;
            // 
            // colStatus
            // 
            this.colStatus.DataPropertyName = "SendFlag";
            this.colStatus.HeaderText = "สถานะ";
            this.colStatus.Name = "colStatus";
            // 
            // FormPayIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPayIn";
            this.Text = "FormPayIn";
            this.Load += new System.EventHandler(this.FormPayIn_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Custom_Controls_in_CS.ButtonZ btSearch;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        internal System.Windows.Forms.Label Label3;
        private Custom_Controls_in_CS.ButtonZ buttonSendData;
        private System.Windows.Forms.DataGridView dgvDetail;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbDetail;
        private System.Windows.Forms.CheckBox CheckBoxSelect;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn calDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCa;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInv;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceipt;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
    }
}