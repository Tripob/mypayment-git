﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using MyPayment.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
namespace MyPayment.FormOffline
{
    public partial class FormOfflineInput : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        private static double VAT = double.Parse(ConfigurationManager.AppSettings["VAT"].ToString());
        public bool GB_IsReadBarcode = false;
        public static ItemInvoiceModel GB_ItemInvoiceModel;
        public static string GB_ReceiptNumber = "";
        public FormOfflineInput()
        {
            InitializeComponent();
        }
        private void FormOfflineInput_Load(object sender, EventArgs e)
        {
            LabelFkName.Text = GlobalClass.Dist;
            LabelEmployeeCode.Text = GlobalClass.UserId.ToString();
            LabelEmployeeName.Text = GlobalClass.UserName;
            LabelStationNo.Text = Convert.ToDecimal(GlobalClass.CashierNo).ToString("00");
            this.GridDetail.AutoGenerateColumns = false;
            this.GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
        private void CountItem()
        {
            try
            {
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.GridDetail.DataSource;
                List<ItemInvoiceModel> LstItem = new List<ItemInvoiceModel>();
                LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                List<ItemInvoiceModel> LstItemSelected = LstItem.Where(m => m.Selected == true).ToList();
                string TotalAmountExVat = LstItem.Sum(m => m.AmountExVat).ToString("N2");
                string PayAmount = LstItemSelected.Sum(m => m.AmountExVat).ToString("N2");
                string ItemSelect = LstItemSelected.Count.ToString();
                string TotalItem = LstItem.Count.ToString();
                string SumFine = LstItem.Where(m => m.FineFlakedOut != 0.0).Sum(m => m.FineFlakedOut).ToString("N2");
                string SumSelectedFine = LstItemSelected.Where(m => m.FineFlakedOut != 0.0).Sum(m => m.FineFlakedOut).ToString("N2");
                string TotalVat = LstItem.Where(m => m.AmountVat != 0.0).Sum(m => m.AmountVat).ToString("N2");
                string TotalSelectedVat = LstItemSelected.Where(m => m.AmountVat != 0.0).Sum(m => m.AmountVat).ToString("N2");
                string TotalStopElectricity = LstItemSelected.Where(m => m.StopElectricityAmount != 0.0).Sum(m => m.StopElectricityAmount).ToString("N2");
                this.txtPayAmount.Text = PayAmount;
                this.txtTotalAmountExVat.Text = TotalAmountExVat;
                this.txtTotalItem.Text = TotalItem;
                this.txtItemSelect.Text = ItemSelect;
                this.txtFine.Text = SumFine;
                this.txtTotalSumAmount.Text = (double.Parse(PayAmount) + double.Parse(TotalSelectedVat) + double.Parse(SumSelectedFine) + double.Parse(TotalStopElectricity)).ToString("N2");
                this.txtTotalVat.Text = TotalVat;
                this.txtTotalAmountIncVat.Text = (double.Parse(TotalVat) + double.Parse(TotalAmountExVat) + double.Parse(TotalStopElectricity)).ToString("N2");
            }
            catch (Exception ex)
            {

            }
        }
        private bool isDuplicateData(string ContractAccount, string InvoiceNumber, string CodeType, string ServiceType)
        {
            try
            {
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.GridDetail.DataSource;
                List<ItemInvoiceModel> LstItem = new List<ItemInvoiceModel>();
                if (bs == null)
                    return false;
                LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                if (LstItem == null)
                    return false;

                if (LstItem.Where(m => m.CA == ContractAccount && m.ServiceType == ServiceType && m.InvoiceNumber == InvoiceNumber).Any())
                    return true;
                return false;

            }
            catch (Exception ex)
            {
                return true;
            }
        }
        public void BindOfflineModel()
        {
            try
            {
                ItemOfflineModel OfflineRec = new ItemOfflineModel();
                string CreateDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                OfflineRec.ReceiptNumber = GB_ReceiptNumber;
                OfflineRec.UserCode = GlobalClass.UserCode;
                OfflineRec.BranchCode = GlobalClass.Branch;
                OfflineRec.TotalAmountExVat = double.Parse(txtTotalAmountExVat.Text);
                OfflineRec.TotalVat = double.Parse(txtTotalVat.Text);
                OfflineRec.TotalAmountIncVat = double.Parse(txtTotalAmountIncVat.Text);
                OfflineRec.TotalFine = double.Parse(txtFine.Text);
                OfflineRec.TotalAll = decimal.Parse(txtTotalSumAmount.Text);
                OfflineRec.Status = "A";
                OfflineRec.CreateDate = CreateDate;
                OfflineRec.UpdateDate = CreateDate;
                GlobalClass.OfflineModel = OfflineRec;
            }
            catch (Exception ex)
            {

            }
        }
        public void ClearData()
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = null;
            this.GridDetail.DataSource = bs;
            this.txtItemSelect.Text = "";
            this.txtPayAmount.Text = "";
            this.txtTotalItem.Text = "";
            this.txtTotalSumAmount.Text = "";
            this.txtTotalAmountExVat.Text = "";
            this.txtTotalVat.Text = "";
            this.txtTotalAmountIncVat.Text = "";
            this.txtFine.Text = "";
        }
        private void btnSendService_Click(object sender, EventArgs e)
        {
            OFC.WriteOfflineTxtLog();
            return;
        }
        private void AddData(ItemInvoiceModel obj, string CodeType, string ServiceType)
        {
            try
            {
                if (isDuplicateData(obj.CA, obj.InvoiceNumber, CodeType, ServiceType))
                {
                    this.txtContractAccount.Text = "";
                    this.txtContractAccount.Focus();
                    MessageBox.Show("ข้อมูลเลขที่ใบแจ้งหนี้ หมายเลข " + obj.InvoiceNumber + " ซ้ำ", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (OFC.isDupInvoiceNumberInDB(obj.InvoiceNumber))
                {
                    MessageBox.Show("มีข้อมูลเลขที่ใบแจ้งหนี้ หมายเลข " + obj.InvoiceNumber + " แล้วในระบบ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.txtContractAccount.Text = "";
                    this.txtContractAccount.Focus();
                    return;
                }
                /***********************************/
                double tmpAmount = 0;
                double tmpAmountVat = 0;
                double fineResult = 0;
                int diffDay = 0;
                DateTime DueDate;
                DateTime NowDate = DateTime.Now;
                tmpAmount = obj.AmountIncVat;
                if (obj.DueDate != null)
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("th-TH");
                    DueDate = DateTime.ParseExact(obj.DueDate, "dd/MM/yy", CultureInfo.InvariantCulture);
                    if (tmpAmount >= 10000 && NowDate.Date > DueDate.Date)
                    {
                        tmpAmountVat = tmpAmount + ((tmpAmount * VAT)) / 100;
                        diffDay = NowDate.Date.Day - DueDate.Date.Day;
                        fineResult = ((tmpAmountVat - tmpAmount) * diffDay * (diffDay / 100)) / 365;
                        obj.FineFlakedOut = double.Parse(fineResult.ToString());
                    }
                    Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
                    DueDate = DateTime.ParseExact(obj.DueDate, "dd/MM/yy", CultureInfo.InvariantCulture);
                }
                /***********************************/
                double VatAmount = 0;
                double VAT_TEMP = 0.0;
                VAT_TEMP = 1.00 + double.Parse("0.0" + VAT.ToString());
                /************************/
                if (CodeType == "")
                {
                    tmpAmount = obj.AmountIncVat;
                    VatAmount = (tmpAmount / VAT_TEMP);
                    obj.AmountVat = double.Parse((tmpAmount - double.Parse(VatAmount.ToString("N2"))).ToString("N2"));
                    obj.AmountExVat = double.Parse(VatAmount.ToString("N2"));
                    obj.VatPercent = VAT;
                }
                else
                {
                    tmpAmount = obj.AmountIncVat;
                    VatAmount = (tmpAmount / VAT_TEMP);
                    obj.AmountVat = double.Parse((tmpAmount - double.Parse(VatAmount.ToString("N2"))).ToString("N2"));
                    obj.AmountExVat = double.Parse(VatAmount.ToString("N2"));
                    obj.VatPercent = VAT;
                }
                /***********************************/
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.GridDetail.DataSource;
                List<ItemInvoiceModel> LstItem = new List<ItemInvoiceModel>();
                if (bs != null)
                {
                    LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                    if (LstItem == null)
                        LstItem = new List<ItemInvoiceModel>();
                }
                else
                    bs = new BindingSource();
                LstItem.Add(obj);
                bs = new BindingSource();
                int index = 0;
                LstItem.ForEach(m => m.Index = ++index);
                bs.DataSource = LstItem;
                this.GridDetail.DataSource = bs;
                CountItem();
                this.txtContractAccount.Text = "";
                this.txtContractAccount.Focus();
                /***************************************/
                if (LstItem.Where(m => m.Selected == false).Any())
                    this.CheckBoxSelect.Checked = false;
                else
                    this.CheckBoxSelect.Checked = true;
            }
            catch (Exception ex)
            {
                this.txtContractAccount.Text = "";
                this.txtContractAccount.Focus();
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void txtContractAccount_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    string ServiceType = "";
                    string CodeType = "";
                    if (txtContractAccount.Text == "" && GridDetail.Rows.Count > 0)
                        ButtonRec.Focus();
                    else if (this.txtContractAccount.Text == "")
                        MessageBox.Show("กรุณาระบุ Contract Number", "Warnning");
                    else if (this.txtContractAccount.Text.Trim().Length == 9)
                    {
                        timer1.Enabled = false;
                        GlobalClass.StatusForm = 0;
                        FormInputMoney frmInputMoney = new FormInputMoney();
                        frmInputMoney.ShowDialog();
                        frmInputMoney.Dispose();
                        if (GlobalClass.StatusForm != 0)
                        {
                            ServiceType = GlobalClass.Status ? "D" : "A";
                            string strCA = this.txtContractAccount.Text;
                            if (ServiceType == "D")
                            {
                                GB_ItemInvoiceModel = new ItemInvoiceModel();
                                GB_ItemInvoiceModel.Selected = true;
                                GB_ItemInvoiceModel.ReceiptNumber = GB_ReceiptNumber;
                                GB_ItemInvoiceModel.CA = strCA;
                                GB_ItemInvoiceModel.ServiceType = "D";
                                GB_ItemInvoiceModel.StopElectricityAmount = 40.00;
                                GB_ItemInvoiceModel.ConnectPower = GlobalClass.Remark;
                                GB_ItemInvoiceModel.InvoiceNumber = "IND" + DateTime.Now.ToString("yyyyMMddHHmmss");
                                AddData(GB_ItemInvoiceModel, "", ServiceType);
                            }
                            if (!string.IsNullOrEmpty(frmInputMoney.InputMoney))
                            {
                                ServiceType = "A";
                                GB_ItemInvoiceModel = new ItemInvoiceModel();
                                GB_ItemInvoiceModel.AmountIncVat = double.Parse(double.Parse(frmInputMoney.InputMoney).ToString("N2"));
                                GB_ItemInvoiceModel.Selected = true;
                                GB_ItemInvoiceModel.ReceiptNumber = GB_ReceiptNumber;
                                GB_ItemInvoiceModel.CA = strCA;
                                GB_ItemInvoiceModel.ServiceType = ServiceType;
                                GB_ItemInvoiceModel.InvoiceNumber = "INV" + DateTime.Now.ToString("yyyyMMddHHmmss");
                                AddData(GB_ItemInvoiceModel, "", ServiceType);
                            }
                        }
                    }
                    else if (this.txtContractAccount.Text.Trim().Length > 55)
                    {
                        timer1.Enabled = false;
                        string strData = this.txtContractAccount.Text.Trim().Replace(" ", "").Replace("|", "");
                        ItemInvoiceModel ItemCode = new ItemInvoiceModel();
                        ServiceType = "A";
                        if (strData.Substring(13, 2) == "00")
                        {
                            CodeType = "BARCODE";
                            ItemCode = OFC.ReadStdBarCode(this.txtContractAccount.Text);
                            ItemCode.ServiceType = "A";
                            ItemCode.CodeType = CodeType;
                            ItemCode.strTax = strData.Substring(0, 15);
                            ItemCode.strRefNo1 = strData.Substring(15, 20);
                            ItemCode.strRefNo2 = strData.Substring(36, 14);
                            ItemCode.strAmount = strData.Substring(50, strData.Length - 50);
                        }
                        else
                        {
                            CodeType = "QRCODE";
                            ItemCode = OFC.ReadQrCode(this.txtContractAccount.Text);
                            ItemCode.ServiceType = "A";
                            ItemCode.CodeType = CodeType;
                            ItemCode.strTax = strData.Substring(0, 15);
                            ItemCode.strRefNo1 = strData.Substring(15, 18);
                            ItemCode.strRefNo2 = strData.Substring(33, 18);
                            ItemCode.strAmount = strData.Substring(51, strData.Length - 51);
                        }
                        AddData(ItemCode, CodeType, ServiceType);
                    }
                    else
                        timer1.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void txtContractAccount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                e.Handled = true;
        }
        private void GridDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                BindingSource bs = new BindingSource();
                List<ItemInvoiceModel> LstItem = new List<ItemInvoiceModel>();
                if (e.ColumnIndex == 0)
                {
                    bs = new BindingSource();
                    bs = (BindingSource)this.GridDetail.DataSource;
                    LstItem = new List<ItemInvoiceModel>();
                    LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                    string InvoiceNumber = GridDetail.CurrentRow.Cells["colInvoiceNumber"].Value.ToString();
                    bool Selected = GridDetail.CurrentRow.Cells["ColCheckBoxDetail"].Value.ToString() == "True" ? true : false;
                    LstItem.Where(m => m.InvoiceNumber == InvoiceNumber).ToList().ForEach(x => x.Selected = !Selected);
                    this.GridDetail.DataSource = bs;
                    CountItem();
                    if (LstItem.Where(m => m.Selected == false).Any())
                        this.CheckBoxSelect.Checked = false;
                    else
                        this.CheckBoxSelect.Checked = true;
                }
                if (e.ColumnIndex == 11)
                {
                    bs = new BindingSource();
                    bs = (BindingSource)this.GridDetail.DataSource;
                    LstItem = new List<ItemInvoiceModel>();
                    LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                    int Index = int.Parse(GridDetail.CurrentRow.Cells["colIndex"].Value.ToString());
                    LstItem = LstItem.Where(m => m.Index != Index).ToList();
                    bs = new BindingSource();
                    bs.DataSource = LstItem;
                    this.GridDetail.DataSource = bs;
                    CountItem();
                    if (LstItem.Where(m => m.Selected == false).Any())
                        this.CheckBoxSelect.Checked = false;
                    else
                        this.CheckBoxSelect.Checked = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void GridDetail_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            Image deleteImage = Properties.Resources.close3;// Image.FromFile(@"E:\Workspace\MyPayment_2\MyPayment\Resources\close3.png");
            if (e.RowIndex < 0)
                return;
            //I supposed your button column is at index 0
            if (e.ColumnIndex == 11)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                var w = 20;
                var h = 20;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;
                e.Graphics.DrawImage(deleteImage, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }

        private void CheckBoxSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (GridDetail.Rows.Count > 0)
                {
                    bool SelectCheck = this.CheckBoxSelect.Checked;
                    BindingSource bs = new BindingSource();
                    bs = (BindingSource)this.GridDetail.DataSource;
                    List<ItemInvoiceModel> LstItem = new List<ItemInvoiceModel>();
                    LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                    LstItem.ForEach(m => m.Selected = SelectCheck);
                    bs = new BindingSource();
                    bs.DataSource = LstItem;
                    this.GridDetail.DataSource = bs;
                    CountItem();
                }
                else
                    CheckBoxSelect.Checked = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void GridDetail_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                BindingSource bs = new BindingSource();
                List<ItemInvoiceModel> LstItem = new List<ItemInvoiceModel>();
                if (e.ColumnIndex == 0)
                {
                    bs = new BindingSource();
                    bs = (BindingSource)this.GridDetail.DataSource;
                    LstItem = new List<ItemInvoiceModel>();
                    LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                    string InvoiceNumber = GridDetail.CurrentRow.Cells["colInvoiceNumber"].Value.ToString();
                    bool Selected = GridDetail.CurrentRow.Cells["ColCheckBoxDetail"].Value.ToString() == "True" ? true : false;
                    LstItem.Where(m => m.InvoiceNumber == InvoiceNumber).ToList().ForEach(x => x.Selected = !Selected);
                    this.GridDetail.DataSource = bs;
                    CountItem();
                    if (LstItem.Where(m => m.Selected == false).Any())
                        this.CheckBoxSelect.Checked = false;
                    else
                        this.CheckBoxSelect.Checked = true;
                }
                if (e.ColumnIndex == 10)
                {
                    bs = new BindingSource();
                    bs = (BindingSource)this.GridDetail.DataSource;
                    LstItem = new List<ItemInvoiceModel>();
                    LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                    int Index = int.Parse(GridDetail.CurrentRow.Cells["colIndex"].Value.ToString());
                    LstItem = LstItem.Where(m => m.Index != Index).ToList();
                    bs = new BindingSource();
                    bs.DataSource = LstItem;
                    this.GridDetail.DataSource = bs;
                    CountItem();
                    if (LstItem.Where(m => m.Selected == false).Any())
                        this.CheckBoxSelect.Checked = false;
                    else
                        this.CheckBoxSelect.Checked = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            txtContractAccount.Text = string.Empty;
            txtContractAccount.Focus();
            timer1.Enabled = false;
            MessageBox.Show("บัญชีแสดงสัญญาไม่ถูกต้อง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        private void ButtonRec_Click(object sender, EventArgs e)
        {
            try
            {
                GB_ReceiptNumber = OFC.CreateReceiptNumber();
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.GridDetail.DataSource;
                List<ItemInvoiceModel> LstItem = new List<ItemInvoiceModel>();
                LstItem = (List<ItemInvoiceModel>)bs.DataSource;
                List<ItemInvoiceModel> LstItemSelected = LstItem.Where(m => m.Selected == true).ToList();
                LstItemSelected.Select(c => { c.ReceiptNumber = GB_ReceiptNumber; return c; }).ToList();
                var arrCA_ELE = LstItemSelected.Select(m => new { m.CA, m.ServiceType }).Distinct().ToArray();
                List<ItemBillModel> lstBillModel = new List<ItemBillModel>();
                ItemBillModel BillModel = new ItemBillModel();
                double TotalAmountExVat = 0;
                double TotalVat = 0;
                double TotalAmountIncVat = 0;
                double TotalFine = 0;
                double TotalAll = 0;
                int BillNumber = 0;
                BillNumber = OFC.GetMaxBillNumber();
                string strBillNumber = "";
                foreach (var item in arrCA_ELE)
                {

                    strBillNumber = DateTime.Now.ToString("yyMMdd") + GlobalClass.UserCode + BillNumber.ToString("D3");
                    if (item.ServiceType == "A")
                    {
                        TotalAmountExVat = LstItemSelected.Where(m => m.CA == item.CA && m.ServiceType == item.ServiceType).Sum(m => m.AmountExVat);
                        TotalVat = LstItemSelected.Where(m => m.CA == item.CA && m.ServiceType == item.ServiceType).Sum(m => m.AmountVat);
                        TotalAmountIncVat = LstItemSelected.Where(m => m.CA == item.CA && m.ServiceType == item.ServiceType).Sum(m => m.AmountIncVat);
                        TotalFine = LstItemSelected.Where(m => m.CA == item.CA && m.FineFlakedOut != 0.0 && m.ServiceType == item.ServiceType).Sum(m => m.FineFlakedOut);
                        TotalAll = TotalAmountIncVat + TotalFine;

                        BillModel = new ItemBillModel();
                        BillModel.BillNumber = strBillNumber;
                        BillModel.BillTotalAmountExVat = double.Parse(TotalAmountExVat.ToString("N2"));
                        BillModel.BillTotalVat = double.Parse(TotalVat.ToString("N2"));
                        BillModel.BillTotalAmountIncVat = double.Parse(TotalAmountIncVat.ToString("N2"));
                        BillModel.BillTotalFine = double.Parse(TotalFine.ToString("N2"));
                        BillModel.BillTotalAll = (double.Parse(TotalAmountIncVat.ToString("N2")) + double.Parse(TotalFine.ToString("N2")));
                        BillModel.BillType = "ELE";
                    }
                    else
                    {
                        BillModel = new ItemBillModel();
                        BillModel.BillNumber = strBillNumber;
                        BillModel.BillTotalAmountExVat = 37.38;
                        BillModel.BillTotalVat = 2.62;
                        BillModel.BillTotalAmountIncVat = 40.0;
                        BillModel.BillTotalFine = 0;
                        BillModel.BillTotalAll = 40.0;
                        BillModel.BillType = "STOP-CONN";

                    }

                    lstBillModel.Add(BillModel);
                    BillNumber++;
                    LstItemSelected.Where(m => m.CA == item.CA && m.ServiceType == item.ServiceType).Select(c => { c.BillNumber = strBillNumber; return c; }).ToList();
                }
                List<ItemStopElectricModel> LstItemStopElectric = new List<ItemStopElectricModel>();
                ItemStopElectricModel StopElectricModel = new ItemStopElectricModel();
                var arrCA_STOP_ELE = LstItemSelected.Where(m => m.StopElectricityAmount != 0.0).Select(m => m.CA).Distinct().ToArray();
                foreach (var item in arrCA_STOP_ELE)
                {

                    StopElectricModel = new ItemStopElectricModel();
                    StopElectricModel.ReceiptNumber = GB_ReceiptNumber;
                    StopElectricModel.CA = item;
                    StopElectricModel.BillNumber = strBillNumber;
                    StopElectricModel.AmountExVat = 37.38;
                    StopElectricModel.AmountVat = 2.62;
                    StopElectricModel.AmountIncVat = 40.0;
                    LstItemStopElectric.Add(StopElectricModel);
                }
                GlobalClass.LstCASelectedModel = LstItemSelected;
                GlobalClass.LstBillModel = lstBillModel;
                GlobalClass.LstStopElectricityModel = LstItemStopElectric;
                BindOfflineModel();
                FormOfflineReceive OfflineReceive = new FormOfflineReceive();
                GlobalClass.ReceiptNumber = GB_ReceiptNumber;
                OfflineReceive.SetAmount(this.txtTotalSumAmount.Text);
                OfflineReceive.SetReceiptNumber(GB_ReceiptNumber);
                OfflineReceive.ShowDialog();
                OfflineReceive.Dispose();
                if (!OfflineReceive.isCancel)
                    ClearData();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
