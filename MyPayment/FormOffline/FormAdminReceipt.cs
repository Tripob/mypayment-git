﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Transactions;
using System.Data.SQLite;
using MyPayment.Models;
using MyPayment.Master;
using System.Threading;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MyPayment.Controllers;

namespace MyPayment.FormOffline
{
    public partial class FormAdminReceipt : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        private   List<ItemIP> GB_LstItem = new List<ItemIP>();
      
        private static FormOfflineMessage frmOfflineMessage = new FormOfflineMessage();
        public FormAdminReceipt()
        {
            InitializeComponent();
        }
        public void ClientWaitRequest()
        {
            try
            {
                OFC.WaitRequest();
                frmOfflineMessage.Hide();
            }
            catch(Exception ex)
            {

            }
        }
        public static void DataGridViewCellVisibility(DataGridViewCell cell, bool visible)
        {
            cell.Style = visible ?
                  new DataGridViewCellStyle { Padding = new Padding(0, 0, 0, 0) } :
                  new DataGridViewCellStyle { Padding = new Padding(cell.OwningColumn.Width, 0, 0, 0) };
            cell.ReadOnly = !visible;
        }

        private void FormAdminReceipt_Load(object sender, EventArgs e)
        {
            try
            {
                this.GridCashierDesk.AutoGenerateColumns = false;
                this.GridCashierDesk.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.GridCashierDesk.RowTemplate.Height = 30;
                foreach (DataGridViewColumn col in this.GridCashierDesk.Columns)
                {
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;                   
                }
            }
            catch(Exception ex)
            {

            }
        }
        //private void GridCashierDesk_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    try
        //    {
        //        if (e.ColumnIndex == 5)
        //        {
        //            bool hasData = (bool)GridCashierDesk.CurrentRow.Cells["hasData"].Value;
        //            if (hasData)
        //            {
        //                string Data = (string)GridCashierDesk.CurrentRow.Cells["Data"].Value;


        //                //List<paymentListOFF> paymentList = new List<paymentListOFF>();
        //                //ResultProcessPayment _Paymentout = new ResultProcessPayment();
        //                //ResultProcessPayment PaymentResult = JsonConvert.DeserializeObject<ResultProcessPayment>(Data);
        //                //GetDataReportController getdata = new GetDataReportController();

        //                //MyPaymentListOFF _mypay = new MyPaymentListOFF();

        //                //paymentList = JsonConvert.DeserializeObject<MyPaymentListOFF>(PaymentResult.OutputProcess.result_message);

        //                //_Paymentout = getdata.ReturnProcessPaymentOFF(_mypay);


        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        private void btnSendDataToService_Click(object sender, EventArgs e)
        {
            try
            {
                frmOfflineMessage = new FormOfflineMessage();
                Thread thread1 = new Thread(SendToService);
                thread1.Start();
                frmOfflineMessage.ShowDialog();
                frmOfflineMessage.Dispose();
            }
            catch (Exception ex)
            {

            }
        }
        public void SendToService()
        {
            try
            {
                string strDateNow = DateTime.Now.ToString("yyyy-MM-dd");
                List<ItemIP> LstItem = new List<ItemIP>();
                var DataSource = this.GridCashierDesk.DataSource;
                LstItem = (List<ItemIP>)DataSource;
                LstItem = LstItem.Where(m => m.Selected == true).ToList();
                ResultProcessPayment PaymentResult = new ResultProcessPayment();
                MyPaymentListOFF _mypay = new MyPaymentListOFF();
                GetDataReportController getdata = new GetDataReportController();
                ResultProcessPayment _Paymentout = new ResultProcessPayment();
                foreach (var item in LstItem)
                {
                    PaymentResult = JsonConvert.DeserializeObject<ResultProcessPayment>(item.Data);
                    if (PaymentResult.OutputProcess.result_code == "SUCCESS")
                    {
                        _mypay = new MyPaymentListOFF();
                        _mypay = JsonConvert.DeserializeObject<MyPaymentListOFF>(PaymentResult.OutputProcess.result_message);
                        _Paymentout = getdata.ReturnProcessPaymentOFF(_mypay);
                        if (_Paymentout.OutputProcess.result_code != null && _Paymentout.OutputProcess.result_code == "SUCCESS")
                        {
                            item.Status = "SUCCESS";

                            /**************************************/


                            ItemSendData SendRec = new ItemSendData();
                            SendRec.IP = item.IP;
                            SendRec.CreateDate = item.Date;
                            SendRec.Data = item.Data;
                            SendRec.CashierDesk = GlobalClass.CashierNo;
                            SendRec.Branch = GlobalClass.BranchId;
                            SendRec.SendToServerDate = strDateNow;
                            /***************************************/

                            OFC.InsertSendData(SendRec);


                            /**************************************/
                        }

                        else
                            item.Status = "Error " + (_Paymentout.ResultMessage != null ? _Paymentout.ResultMessage : "");
                    }
                }
                GB_LstItem = LstItem;
                AccessControl();
            }
            catch (Exception ex)
            {

            }
        }
        private void AccessControl()
        {
            //if(this.InvokeRequired)
            //    this.Invoke(new MethodInvoker(AccessControl));
            //else
            //    frmOfflineMessage.Hide();
            //    this.GridCashierDesk.DataSource = GB_LstItem;

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(AccessControl));
            }
            else
            {
                frmOfflineMessage.Hide();
                this.GridCashierDesk.DataSource = GB_LstItem;


                foreach (DataGridViewRow row in this.GridCashierDesk.Rows)
                {

                    DataGridViewCellVisibility(this.GridCashierDesk.Rows[row.Index].Cells[0], false);

                }

                this.GridCashierDesk.Refresh();
            }
        }

        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                btnSendDataToService.Visible = false;
                buttonSendData.Visible = true;
                buttonSave.Visible = true;
                string strDate = DateTimePicker.Value.ToString("yyyy-MM-dd");
                string strDateDisplay = DateTimePicker.Value.ToString("dd/MM/yyyy");



                /*******************/
                GetDataReportController getdata = new GetDataReportController();
                JObject json = new JObject();
                json.Add("distId", GlobalClass.BranchId);

                var result = getdata.ProcessGetIpAddressByDistId(json.ToString());
                var LstSendData = OFC.GetLstIP(strDate, strDateDisplay);
                List<ItemIP> LstIP = new List<ItemIP>();
                ItemIP ipRec = new ItemIP();

                /**********************************/
                ipRec = new ItemIP();

                ipRec.IP = "Server";
                ipRec.DateDisplay = strDateDisplay;
                ipRec.Port = "5000";
                ipRec.Date = strDate;
                ipRec.hasData = false;
                LstIP.Add(ipRec);

                /*********************************/
                if (result.OutputProcess != null && result.OutputProcess.result_code == "SUCCESS")
                {

                    foreach (var item in result.OutputProcess.ipAddressList)
                    {
                        ipRec = new ItemIP();

                        ipRec.IP = item;
                        ipRec.DateDisplay = strDateDisplay;
                        ipRec.Port = "5000";
                        ipRec.Date = strDate;
                        var sendDataRec = LstSendData.Where(m => m.IP == ipRec.IP).FirstOrDefault();
                        if (sendDataRec != null)
                        {
                            ipRec.hasData = true;
                            ipRec.SendToServerDateDisplay = sendDataRec.SendToServerDateDisplay;
                        }
                        else
                        {
                            ipRec.hasData = false;
                        }
                        LstIP.Add(ipRec);

                    }
                
                }
                /*******************/

           
                GridCashierDesk.AutoGenerateColumns = false;
                GridCashierDesk.DataSource = LstIP;
                foreach (DataGridViewRow row in GridCashierDesk.Rows)
                {
                    //if (GridCashierDesk.Rows[row.Index].Cells[3].EditedFormattedValue.ToString().Trim().Length > 0)
                    //    DataGridViewCellVisibility(GridCashierDesk.Rows[row.Index].Cells[0], false);
                    //if (GridCashierDesk.Rows[row.Index].Cells["Data"].Value.ToString().Trim().Length == 0
                    //    || GridCashierDesk.Rows[row.Index].Cells["SendToServerDateDisplay"].Value.ToString().Trim().Length > 0)
                    //    DataGridViewCellVisibility(GridCashierDesk.Rows[row.Index].Cells[5], false);

                    //if (GridCashierDesk.Rows[row.Index].Cells["Data"].Value.ToString().Trim().Length != 0)
                    //{
                    //    DataGridViewCellVisibility(GridCashierDesk.Rows[row.Index].Cells[0], false);
                    //}


                    if (GridCashierDesk.Rows[row.Index].Cells["hasData"].Value.ToString().Trim() == "True")
                    {
                        DataGridViewCellVisibility(GridCashierDesk.Rows[row.Index].Cells[0], false);
                    }
                }
                GridCashierDesk.Refresh();
            }
            catch (Exception ex)
            {

            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<ItemIP> LstItem = new List<ItemIP>();
                var DataSource = this.GridCashierDesk.DataSource;
                LstItem = (List<ItemIP>)DataSource;
                LstItem = LstItem.Where(m => m.Selected == true).ToList();
                string strResult = "";
                foreach (ItemIP item in LstItem)
                {
                    if (!item.Selected)
                        break;

                    strResult = "";


                    if (item.IP == "Server")
                    {
                        var LstSearchOffline = OFC.GetSearchOffline(item.Date);
                        string strReceiptNumber = string.Join(",", LstSearchOffline.Select(m => "'" + m.ReceiptNumber + "'").Distinct().ToArray());
                        ResultProcessPayment _Paymentout = new ResultProcessPayment();
                        _Paymentout = OFC.GenListOfflineToService(item.Date, strReceiptNumber, false);

                        if (_Paymentout.OutputProcess != null && _Paymentout.OutputProcess.result_code == "SUCCESS")
                        {
                           // strResult = _Paymentout.OutputProcess.result_message;


                            strResult = JsonConvert.SerializeObject(_Paymentout);
                        }

                    }
                    else
                    {
                        strResult = OFC.SendRequest(item.IP, int.Parse(item.Port), item.Date);
                    }

                   // item.hasData = true;
                    //ItemSendData SendRec = new ItemSendData();
                    //SendRec.IP = item.IP;
                    //SendRec.CreateDate = item.Date;
                    //SendRec.Data = strResult;
                    //SendRec.CashierDesk = GlobalClass.CashierNo;
                    //SendRec.Branch = GlobalClass.BranchId;
                //    OFC.InsertSendData(SendRec);
                    item.Status = "รอนำส่ง";
                    item.Data = strResult;
                }
                GridCashierDesk.AutoGenerateColumns = false;
                GridCashierDesk.DataSource = LstItem;
                foreach (DataGridViewRow row in GridCashierDesk.Rows)
                {
                    if ((bool)GridCashierDesk.Rows[row.Index].Cells[0].Value)
                        DataGridViewCellVisibility(GridCashierDesk.Rows[row.Index].Cells[5], true);
                }
                GridCashierDesk.Refresh();
                btnSendDataToService.Visible = true;
            }
            catch (Exception ex)
            {

            }
        }
        private void buttonSendData_Click(object sender, EventArgs e)
        {
            try
            {
                frmOfflineMessage = new FormOfflineMessage();
                Thread thread1 = new Thread(ClientWaitRequest);
                thread1.Start();
                frmOfflineMessage.ShowDialog();
                frmOfflineMessage.Dispose();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
