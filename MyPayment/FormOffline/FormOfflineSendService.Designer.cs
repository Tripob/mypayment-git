﻿namespace MyPayment.FormOffline
{
    partial class FormOfflineSendService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOfflineSendService));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PN1 = new System.Windows.Forms.Panel();
            this.PN2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GridCashierDesk = new System.Windows.Forms.DataGridView();
            this.PN3 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.Label3 = new System.Windows.Forms.Label();
            this.buttonSave = new Custom_Controls_in_CS.ButtonZ();
            this.buttonSendData = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonSearch = new Custom_Controls_in_CS.ButtonZ();
            this.CheckBoxSelect = new System.Windows.Forms.CheckBox();
            this.colCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ReceiptNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalAll = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SendFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PN1.SuspendLayout();
            this.PN2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCashierDesk)).BeginInit();
            this.PN3.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN1
            // 
            this.PN1.Controls.Add(this.buttonSendData);
            this.PN1.Controls.Add(this.DateTimePicker);
            this.PN1.Controls.Add(this.Label3);
            this.PN1.Controls.Add(this.ButtonSearch);
            this.PN1.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN1.Location = new System.Drawing.Point(0, 0);
            this.PN1.Name = "PN1";
            this.PN1.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN1.Size = new System.Drawing.Size(1012, 50);
            this.PN1.TabIndex = 0;
            // 
            // PN2
            // 
            this.PN2.Controls.Add(this.groupBox1);
            this.PN2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN2.Location = new System.Drawing.Point(0, 50);
            this.PN2.Name = "PN2";
            this.PN2.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN2.Size = new System.Drawing.Size(1012, 655);
            this.PN2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CheckBoxSelect);
            this.groupBox1.Controls.Add(this.GridCashierDesk);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(10, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10, 3, 10, 4);
            this.groupBox1.Size = new System.Drawing.Size(992, 610);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รายการข้อมูล";
            // 
            // GridCashierDesk
            // 
            this.GridCashierDesk.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridCashierDesk.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridCashierDesk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridCashierDesk.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCheck,
            this.ReceiptNumber,
            this.TotalAll,
            this.SendFlag,
            this.Tel});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("TH Sarabun New", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridCashierDesk.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridCashierDesk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCashierDesk.Location = new System.Drawing.Point(10, 28);
            this.GridCashierDesk.Name = "GridCashierDesk";
            this.GridCashierDesk.RowHeadersVisible = false;
            this.GridCashierDesk.Size = new System.Drawing.Size(972, 578);
            this.GridCashierDesk.TabIndex = 0;
            this.GridCashierDesk.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridCashierDesk_CellContentClick);
            // 
            // PN3
            // 
            this.PN3.Controls.Add(this.buttonSave);
            this.PN3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PN3.Location = new System.Drawing.Point(0, 658);
            this.PN3.Name = "PN3";
            this.PN3.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.PN3.Size = new System.Drawing.Size(1012, 47);
            this.PN3.TabIndex = 2;
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker.Location = new System.Drawing.Point(58, 14);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(171, 26);
            this.DateTimePicker.TabIndex = 1000000049;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(9, 13);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(41, 28);
            this.Label3.TabIndex = 1000000048;
            this.Label3.Text = "วันที่";
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.Transparent;
            this.buttonSave.BorderColor = System.Drawing.Color.Transparent;
            this.buttonSave.BorderWidth = 1;
            this.buttonSave.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.buttonSave.ButtonText = "รับข้อมูล";
            this.buttonSave.CausesValidation = false;
            this.buttonSave.EndColor = System.Drawing.Color.Silver;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.buttonSave.ForeColor = System.Drawing.Color.Black;
            this.buttonSave.GradientAngle = 90;
            this.buttonSave.Image = global::MyPayment.Properties.Resources.charts05;
            this.buttonSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSave.Location = new System.Drawing.Point(895, 8);
            this.buttonSave.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.buttonSave.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonSave.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.buttonSave.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.ShowButtontext = true;
            this.buttonSave.Size = new System.Drawing.Size(96, 30);
            this.buttonSave.StartColor = System.Drawing.Color.Gainsboro;
            this.buttonSave.TabIndex = 1000000051;
            this.buttonSave.TextLocation_X = 31;
            this.buttonSave.TextLocation_Y = 5;
            this.buttonSave.Transparent1 = 80;
            this.buttonSave.Transparent2 = 120;
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonSendData
            // 
            this.buttonSendData.BackColor = System.Drawing.Color.Transparent;
            this.buttonSendData.BorderColor = System.Drawing.Color.Transparent;
            this.buttonSendData.BorderWidth = 1;
            this.buttonSendData.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.buttonSendData.ButtonText = "ส่งข้อมูล";
            this.buttonSendData.CausesValidation = false;
            this.buttonSendData.EndColor = System.Drawing.Color.Silver;
            this.buttonSendData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSendData.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.buttonSendData.ForeColor = System.Drawing.Color.Black;
            this.buttonSendData.GradientAngle = 90;
            this.buttonSendData.Image = global::MyPayment.Properties.Resources.Running;
            this.buttonSendData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSendData.Location = new System.Drawing.Point(343, 13);
            this.buttonSendData.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.buttonSendData.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonSendData.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.buttonSendData.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.buttonSendData.Name = "buttonSendData";
            this.buttonSendData.ShowButtontext = true;
            this.buttonSendData.Size = new System.Drawing.Size(96, 28);
            this.buttonSendData.StartColor = System.Drawing.Color.Gainsboro;
            this.buttonSendData.TabIndex = 1000000050;
            this.buttonSendData.TextLocation_X = 30;
            this.buttonSendData.TextLocation_Y = 3;
            this.buttonSendData.Transparent1 = 80;
            this.buttonSendData.Transparent2 = 120;
            this.buttonSendData.UseVisualStyleBackColor = true;
            this.buttonSendData.Click += new System.EventHandler(this.buttonSendData_Click);
            // 
            // ButtonSearch
            // 
            this.ButtonSearch.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonSearch.BorderWidth = 1;
            this.ButtonSearch.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonSearch.ButtonText = "ค้นหา";
            this.ButtonSearch.CausesValidation = false;
            this.ButtonSearch.EndColor = System.Drawing.Color.Silver;
            this.ButtonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSearch.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonSearch.ForeColor = System.Drawing.Color.Black;
            this.ButtonSearch.GradientAngle = 90;
            this.ButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSearch.Image")));
            this.ButtonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSearch.Location = new System.Drawing.Point(247, 13);
            this.ButtonSearch.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonSearch.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonSearch.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonSearch.Name = "ButtonSearch";
            this.ButtonSearch.ShowButtontext = true;
            this.ButtonSearch.Size = new System.Drawing.Size(90, 28);
            this.ButtonSearch.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonSearch.TabIndex = 1000000047;
            this.ButtonSearch.TextLocation_X = 35;
            this.ButtonSearch.TextLocation_Y = 1;
            this.ButtonSearch.Transparent1 = 80;
            this.ButtonSearch.Transparent2 = 120;
            this.ButtonSearch.UseVisualStyleBackColor = true;
            this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // CheckBoxSelect
            // 
            this.CheckBoxSelect.AutoSize = true;
            this.CheckBoxSelect.Location = new System.Drawing.Point(19, 38);
            this.CheckBoxSelect.Name = "CheckBoxSelect";
            this.CheckBoxSelect.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelect.TabIndex = 1000000050;
            this.CheckBoxSelect.UseVisualStyleBackColor = true;
            this.CheckBoxSelect.Click += new System.EventHandler(this.CheckBoxSelect_Click);
            // 
            // colCheck
            // 
            this.colCheck.DataPropertyName = "Selected";
            this.colCheck.HeaderText = "";
            this.colCheck.Name = "colCheck";
            this.colCheck.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colCheck.Width = 30;
            // 
            // ReceiptNumber
            // 
            this.ReceiptNumber.DataPropertyName = "ReceiptNumber";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ReceiptNumber.DefaultCellStyle = dataGridViewCellStyle2;
            this.ReceiptNumber.HeaderText = "เลขที่รับ";
            this.ReceiptNumber.Name = "ReceiptNumber";
            this.ReceiptNumber.Width = 230;
            // 
            // TotalAll
            // 
            this.TotalAll.DataPropertyName = "TotalAll";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.TotalAll.DefaultCellStyle = dataGridViewCellStyle3;
            this.TotalAll.HeaderText = "จำนวนเงิน";
            this.TotalAll.Name = "TotalAll";
            this.TotalAll.Width = 180;
            // 
            // SendFlag
            // 
            this.SendFlag.DataPropertyName = "SendFlag";
            this.SendFlag.HeaderText = "สถานะ";
            this.SendFlag.Name = "SendFlag";
            this.SendFlag.Width = 230;
            // 
            // Tel
            // 
            this.Tel.DataPropertyName = "Tel";
            this.Tel.HeaderText = "เบอร์โทร";
            this.Tel.Name = "Tel";
            this.Tel.Width = 297;
            // 
            // FormOfflineSendService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.PN3);
            this.Controls.Add(this.PN2);
            this.Controls.Add(this.PN1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormOfflineSendService";
            this.Text = "FormBillCancel";
            this.Load += new System.EventHandler(this.FormAdminReceipt_Load);
            this.PN1.ResumeLayout(false);
            this.PN1.PerformLayout();
            this.PN2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCashierDesk)).EndInit();
            this.PN3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN1;
        private System.Windows.Forms.Panel PN2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView GridCashierDesk;
        private System.Windows.Forms.Panel PN3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        internal System.Windows.Forms.Label Label3;
        private Custom_Controls_in_CS.ButtonZ ButtonSearch;
        private Custom_Controls_in_CS.ButtonZ buttonSendData;
        private Custom_Controls_in_CS.ButtonZ buttonSave;
        private System.Windows.Forms.CheckBox CheckBoxSelect;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceiptNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalAll;
        private System.Windows.Forms.DataGridViewTextBoxColumn SendFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tel;
    }
}