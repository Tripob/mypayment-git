﻿namespace MyPayment.FormOffline
{
    partial class FormInputMoney
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chFeeElectricity = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMoney = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new Custom_Controls_in_CS.ButtonZ();
            this.btnOK = new Custom_Controls_in_CS.ButtonZ();
            this.rdYes = new System.Windows.Forms.RadioButton();
            this.rdNo = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdNo);
            this.groupBox1.Controls.Add(this.rdYes);
            this.groupBox1.Controls.Add(this.chFeeElectricity);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtMoney);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(504, 104);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // chFeeElectricity
            // 
            this.chFeeElectricity.AutoSize = true;
            this.chFeeElectricity.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chFeeElectricity.Location = new System.Drawing.Point(6, 65);
            this.chFeeElectricity.Name = "chFeeElectricity";
            this.chFeeElectricity.Size = new System.Drawing.Size(184, 32);
            this.chFeeElectricity.TabIndex = 5;
            this.chFeeElectricity.Text = "มีค่าดำเนินการตัดต่อสาย";
            this.chFeeElectricity.UseVisualStyleBackColor = true;
            this.chFeeElectricity.CheckedChanged += new System.EventHandler(this.chFeeElectricity_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(457, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 28);
            this.label7.TabIndex = 4;
            this.label7.Text = "บาท";
            // 
            // txtMoney
            // 
            this.txtMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoney.Location = new System.Drawing.Point(201, 23);
            this.txtMoney.Name = "txtMoney";
            this.txtMoney.Size = new System.Drawing.Size(250, 31);
            this.txtMoney.TabIndex = 3;
            this.txtMoney.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMoney.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMoney_KeyDown);
            this.txtMoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMoney_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(47, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "จำนวนเงินรับชำระ";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BorderColor = System.Drawing.Color.Transparent;
            this.btnCancel.BorderWidth = 1;
            this.btnCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btnCancel.ButtonText = "ยกเลิก";
            this.btnCancel.CausesValidation = false;
            this.btnCancel.EndColor = System.Drawing.Color.DarkGray;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCancel.GradientAngle = 90;
            this.btnCancel.Image = global::MyPayment.Properties.Resources.close1;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(387, 110);
            this.btnCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btnCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btnCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.ShowButtontext = true;
            this.btnCancel.Size = new System.Drawing.Size(127, 40);
            this.btnCancel.StartColor = System.Drawing.Color.LightGray;
            this.btnCancel.TabIndex = 1000000027;
            this.btnCancel.TextLocation_X = 60;
            this.btnCancel.TextLocation_Y = 5;
            this.btnCancel.Transparent1 = 80;
            this.btnCancel.Transparent2 = 120;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.Transparent;
            this.btnOK.BorderColor = System.Drawing.Color.Transparent;
            this.btnOK.BorderWidth = 1;
            this.btnOK.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.btnOK.ButtonText = "ตกลง";
            this.btnOK.CausesValidation = false;
            this.btnOK.EndColor = System.Drawing.Color.DarkGray;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnOK.ForeColor = System.Drawing.Color.Black;
            this.btnOK.GradientAngle = 90;
            this.btnOK.Image = global::MyPayment.Properties.Resources.ok;
            this.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOK.Location = new System.Drawing.Point(254, 110);
            this.btnOK.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.btnOK.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnOK.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.btnOK.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.btnOK.Name = "btnOK";
            this.btnOK.ShowButtontext = true;
            this.btnOK.Size = new System.Drawing.Size(127, 40);
            this.btnOK.StartColor = System.Drawing.Color.LightGray;
            this.btnOK.TabIndex = 1000000026;
            this.btnOK.TextLocation_X = 60;
            this.btnOK.TextLocation_Y = 5;
            this.btnOK.Transparent1 = 80;
            this.btnOK.Transparent2 = 120;
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // rdYes
            // 
            this.rdYes.AutoSize = true;
            this.rdYes.Enabled = false;
            this.rdYes.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.rdYes.Location = new System.Drawing.Point(201, 65);
            this.rdYes.Name = "rdYes";
            this.rdYes.Size = new System.Drawing.Size(126, 32);
            this.rdYes.TabIndex = 6;
            this.rdYes.TabStop = true;
            this.rdYes.Text = "ต้องการต่อกลับ";
            this.rdYes.UseVisualStyleBackColor = true;
            // 
            // rdNo
            // 
            this.rdNo.AutoSize = true;
            this.rdNo.Enabled = false;
            this.rdNo.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.rdNo.Location = new System.Drawing.Point(333, 64);
            this.rdNo.Name = "rdNo";
            this.rdNo.Size = new System.Drawing.Size(142, 32);
            this.rdNo.TabIndex = 7;
            this.rdNo.TabStop = true;
            this.rdNo.Text = "ไม่ต้องการต่อกลับ";
            this.rdNo.UseVisualStyleBackColor = true;
            // 
            // FormInputMoney
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(4F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 161);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("TH Sarabun New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "FormInputMoney";
            this.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รับเงิน";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private Custom_Controls_in_CS.ButtonZ btnCancel;
        private Custom_Controls_in_CS.ButtonZ btnOK;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMoney;
        private System.Windows.Forms.CheckBox chFeeElectricity;
        private System.Windows.Forms.RadioButton rdNo;
        private System.Windows.Forms.RadioButton rdYes;
    }
}