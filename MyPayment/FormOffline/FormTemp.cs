﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using MyPayment.Controllers;
using MyPayment.Models;
using MyPayment.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPayment.FormOffline
{
   
    public partial class FormTemp : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        public CultureInfo usCulture = new CultureInfo("en-US");
        public string CustomerName;
        public double Countamount;
        public double CountamountAll;
        public bool GB_IsReadBarcode = false;
        public string GB_StrQrCode = "";
        public string GB_Chno = "";
        ResultCostTDebt resul = new ResultCostTDebt();
        ArrearsController arr = new ArrearsController();
        List<ClassCheque> _cheque = new List<ClassCheque>();
        List<ClassListGridview> _lstGrid = new List<ClassListGridview>();
        public byte Key_Error;
        public string qNo;
        public FormTemp()
        {
            InitializeComponent();
        }
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                List<ItemtSummaryChequeModel> LstSummaryCheque = new List<ItemtSummaryChequeModel>();
                ItemOfflineModel OfflineRec = new ItemOfflineModel();
                ItemtSummaryChequeModel SummaryChequeRec = new ItemtSummaryChequeModel();
                if (_cheque != null)
                {
                    foreach (var item in _cheque)
                    {
                        SummaryChequeRec = new ItemtSummaryChequeModel();

                        SummaryChequeRec.UserCode = "";
                        SummaryChequeRec.ChequeNumber = item.Chno;
                        SummaryChequeRec.ChequeName = item.Chname;
                        SummaryChequeRec.BankCode = item.Bankno;
                        SummaryChequeRec.BankName = item.Bankname;
                        SummaryChequeRec.ChequeDate = item.Chdate;
                        SummaryChequeRec.Amount = item.Chamount;
                        LstSummaryCheque.Add(SummaryChequeRec);
                    }


                }
                /********************************************************/
                //OfflineRec = CreateOfflineObj();
                //OfflineRec.UserId = "";
                //OfflineRec.PaymentDate = DateTime.Now.ToString("dd/MM/yyyy");
                //OfflineRec.Time = DateTime.Now.ToString("HH:mm");
                //OfflineRec.Branch = txtBranch.Text;
                //OfflineRec.CashierNo = txtCashierNo.Text;
                //OfflineRec.ContactNumber = txTelContact.Text;
                //OfflineRec.ContractAccount = "001234";
                //OfflineRec.InvoiceNumber = "005678";
                //OfflineRec.TaxInvoice = "";
                //OfflineRec.Amount = txtAmount.Text;
                //OfflineRec.Cash = txtCash.Text;
                //OfflineRec.CashChange = "";

               // OFC.WriteOffLineToCSV(OfflineRec);

            }
            catch (Exception ex)
            {

            }
        }

        public void SelectPayment()
        {
            try
            {
                GetDataReportController getdata = new GetDataReportController();
                MyPaymentList _mypay = new MyPaymentList();
                List<paymentList> _paylst = new List<paymentList>();
                List<paymentDetailList> _paydetaillst = new List<paymentDetailList>();
                List<paymentMethodList> _paymethodlst = new List<paymentMethodList>();
                List<receiptList> receiptlst = new List<receiptList>();
                List<receiptDetailDtoList> receiptdetaillst = new List<receiptDetailDtoList>();
                paymentList pay = new paymentList();

                double AmountCreditTotal = 0;
                double AmountCheqTotal = 0;
                double AmountTotal = 0;
                double AmountCredit = 0;
                double amount = 0;
                double TotalAmountCheq = 0;
                double TotalAmount = 0;
                string chequeNo = "";

                if (labelAmountTotalCheq.Text != "")
                    TotalAmountCheq = Convert.ToDouble(labelAmountTotalCheq.Text);
                if (txtCash.Text != "")
                    TotalAmount = Convert.ToDouble(txtCash.Text);
                int j = 0;

                //paymentList
                DateTime dueDate;
                pay.distId = GlobalClass.DistId;
                pay.cashierNo = Convert.ToInt32(GlobalClass.No);
                pay.cashierId = GlobalClass.UserId;
                pay.paymentTotalAmount = Convert.ToDouble(txtAmount.Text.Trim());
                pay.paymentReceive = Convert.ToDouble(LabelAmountReceived.Text.Trim());
                pay.paymentChange = Convert.ToDouble(LabelAmountChange.Text.Trim());
                dueDate = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                pay.paymentDate = dueDate.ToString("yyyy-MM-dd");
                pay.paymentChennel = 1;//Fix
                pay.payeeFlag = "N";
                pay.subDistId = GlobalClass.SubDistId;
                pay.userId = GlobalClass.UserId;
                pay.qNo = (qNo != null) ? qNo : null;

                var LstCost = GlobalClass.LstCost.Where(c => c.status == true).OrderBy(g => g.ca).OrderByDescending(c => c.StatusCheq).ToList();
                //SetDataListGridview();
                int CountRow = GridDetail.Rows.Count - 1;
                foreach (var itemList in LstCost)
                {
                    bool status = false;
                    double pamentOld = 0;
                    List<paymentDetailSubList> _paySublst = new List<paymentDetailSubList>();
                    //paymentList==>paymentDetailList
                    DateTime dueDateDetail;
                    DateTime dateNow;
                    paymentDetailList paydetail = new paymentDetailList();
                    paydetail.debtId = Convert.ToInt32(itemList.debtId);
                    paydetail.amount = itemList.amount;
                    paydetail.percentVat = Convert.ToInt32(itemList.percentVat);
                    paydetail.vat = Convert.ToDouble(itemList.vatBalance);
                    paydetail.totalAmount = Convert.ToDouble(itemList.totalAmount);
                    int countDay = 0;
                    double Defaultpenalty = 0;
                    #region CountDay
                    if (itemList.debtType == "ELE" && itemList.debtInsHdrId is null)
                    {
                        if (!(itemList.invNewDueDate is null))
                            dueDateDetail = Convert.ToDateTime(itemList.invNewDueDate);
                        else
                            dueDateDetail = Convert.ToDateTime(itemList.dueDate);
                        dateNow = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                        if (itemList.debtType != "GV" && Convert.ToDouble(itemList.debtBalance) > 10000 && DateTime.Now > Convert.ToDateTime(itemList.dueDate))
                        {
                            var dtCountDay = dateNow - dueDateDetail;
                            countDay = dtCountDay.Days;
                            Defaultpenalty = (Convert.ToDouble(itemList.debtBalance) - Convert.ToDouble(itemList.vatBalance)) * dtCountDay.Days * (Convert.ToDouble(itemList.intRate) / 100) / 365;
                        }
                        else
                        {
                            countDay = 0;
                            Defaultpenalty = 0;
                        }
                    }
                    else
                    {
                        countDay = 0;
                        Defaultpenalty = 0;
                    }
                    #endregion
                    paydetail.intAmount = Defaultpenalty;
                    paydetail.intDay = countDay;

                    if (TotalAmountCheq > 0 && TotalAmount >= 0 && AmountCredit >= 0)
                    {
                        if (GlobalClass.LstSub != null)
                        {
                            var lstSub = GlobalClass.LstSub.Where(c => c.DebtId == itemList.debtId).ToList();
                            if (lstSub.Count > 0)
                            {
                                foreach (var item in lstSub)
                                {
                                    paymentDetailSubList paySub = new paymentDetailSubList();
                                    paySub.paymentMethodId = 2;
                                    paySub.totalAmount = item.Payed;
                                    _paySublst.Add(paySub);
                                    TotalAmountCheq = TotalAmountCheq - Convert.ToDouble(itemList.debtBalance);
                                }

                                status = true;
                                AmountCheqTotal += Convert.ToDouble(itemList.debtBalance);
                            }
                        }
                        else
                        {
                            int count = 0;
                            int i = 0;
                            for (i = 0; i <= count; i++)
                            {
                                if (amount == 0)
                                {
                                    DataGridViewTextBoxCell amountChequeCell = (DataGridViewTextBoxCell)GridDetail.Rows[j].Cells["colAmount"];
                                    DataGridViewTextBoxCell ChequeNoCell = (DataGridViewTextBoxCell)GridDetail.Rows[j].Cells["colNumber"];
                                    amount = Convert.ToDouble(amountChequeCell.EditedFormattedValue);
                                    chequeNo = ChequeNoCell.EditedFormattedValue.ToString();
                                }
                                double debtBalance = 0;
                                paymentDetailSubList paySub = new paymentDetailSubList();
                                paySub.paymentMethodId = 2;
                                paySub.chequeNo = chequeNo;

                                double countTotal = 0;
                                if (pamentOld != 0)
                                    countTotal = pamentOld;
                                else
                                    countTotal = Convert.ToDouble(itemList.debtBalance);
                                if (countTotal > amount)
                                {
                                    j++;
                                    debtBalance = amount;
                                    if (pamentOld != 0)
                                        pamentOld = pamentOld - amount;
                                    else
                                        pamentOld = Convert.ToDouble(itemList.debtBalance) - amount;
                                    if (TotalAmountCheq > countTotal)
                                    {
                                        count++;
                                        amount = 0;
                                    }
                                }
                                else
                                {
                                    if (pamentOld != 0)
                                    {
                                        debtBalance = pamentOld;
                                        amount = amount - pamentOld;
                                    }
                                    else
                                    {
                                        debtBalance = Convert.ToDouble(itemList.debtBalance);
                                        amount = amount - Convert.ToDouble(itemList.debtBalance);
                                    }
                                }
                                paySub.totalAmount = Math.Round(debtBalance, 2);
                                _paySublst.Add(paySub);
                                TotalAmountCheq = TotalAmountCheq - debtBalance;
                                status = true;
                                AmountCheqTotal += countTotal;
                            }
                            if (TotalAmountCheq < 0)
                            {
                                paymentDetailSubList paySub = new paymentDetailSubList();
                                paySub.paymentMethodId = 1;
                                paySub.totalAmount = Math.Round(Convert.ToDouble(itemList.debtBalance) - amount, 2);
                                _paySublst.Add(paySub);
                                TotalAmount = TotalAmount - (Convert.ToDouble(itemList.debtBalance) - amount);
                                AmountTotal += Convert.ToDouble(itemList.debtBalance) - amount;
                            }
                        }
                    }
                    if (TotalAmount > 0 && status == false)
                    {
                        paymentDetailSubList paySub = new paymentDetailSubList();
                        paySub.paymentMethodId = 1;
                        paySub.totalAmount = Math.Round(Convert.ToDouble(itemList.debtBalance), 2);
                        _paySublst.Add(paySub);
                        TotalAmount = TotalAmount - Convert.ToDouble(itemList.debtBalance);
                        AmountTotal += Convert.ToDouble(itemList.debtBalance);
                    }
                    #region
                    //if()

                    //paymentDetailSubList paySub = new paymentDetailSubList();
                    //paySub.paymentMethodId = 1;
                    //paySub.totalAmount = Convert.ToDouble(itemList.debtBalance);
                    //_paySublst.Add(paySub);


                    //fo reach (var itemSub in _paySublst)
                    //{
                    //paymentDetailList==>paymentDetailSubList

                    //}
                    //paymentDetailSubList paySub = new paymentDetailSubList();
                    //if (Convert.ToDateTime(itemList.lockCheq) > DateTime.Now)
                    //{
                    //    if (CheckCh.Checked == true)
                    //    {
                    //        paySub.paymentMethodId = 2;
                    //        paySub.chequeNo = itemList.ChequeNo;
                    //        paySub.totalAmount = itemList.Payed;
                    //    }
                    //    else
                    //    {
                    //        paySub.paymentMethodId = 1;
                    //        paySub.totalAmount = Convert.ToDouble(itemList.debtBalance);
                    //    }
                    //    _paySublst.Add(paySub);
                    //}
                    //else
                    //{
                    //    if (CountamountAll> Convert.ToDouble(itemList.debtBalance))
                    //    {
                    //        paySub.paymentMethodId = 2;
                    //        paySub.chequeNo = itemList.ChequeNo;
                    //    }
                    //    else
                    //    {
                    //        paySub.paymentMethodId = 1;
                    //    }
                    //    paySub.totalAmount = Convert.ToDouble(itemList.debtBalance);
                    //    _paySublst.Add(paySub);
                    //    //if (AmountCheque > Convert.ToDouble(itemList.debtBalance))
                    //    //{
                    //    //    paySub.paymentMethodId = 2;
                    //    //    paySub.totalAmount = Convert.ToDouble(itemList.debtBalance);
                    //    //    paySub.chequeNo = itemList.ChequeNo;
                    //    //    _paySublst.Add(paySub);
                    //    //}
                    //    //else
                    //    //{
                    //    //    paySub.paymentMethodId = 2;
                    //    //    paySub.totalAmount = Convert.ToDouble(itemList.debtBalance);
                    //    //    _paySublst.Add(paySub);

                    //    //}
                    //}
                    //paySub.totalAmount = Convert.ToDouble(itemList.debtBalance);
                    //AmountCheque = AmountCheque - Convert.ToDouble(itemList.debtBalance);
                    //_paySublst.Add(paySub);
                    //foreach (var itemSub in _paySublst)
                    //{
                    //    //paymentDetailList==>paymentDetailSubList
                    //    paymentDetailSubList paySub = new paymentDetailSubList();
                    //    paySub.paymentMethodId = itemSub.paymentMethodId;
                    //    paySub.totalAmount = itemSub.totalAmount;
                    //    _paySublst.Add(paySub);
                    //}
                    #endregion
                    paydetail.paymentDetailSubList = _paySublst;
                    _paydetaillst.Add(paydetail);
                }
                pay.paymentDetailList = _paydetaillst;


                if (labelAmountTotalCheq.Text != "")
                    TotalAmountCheq = Convert.ToDouble(labelAmountTotalCheq.Text);
                if (txtCash.Text != "")
                    TotalAmount = Convert.ToDouble(txtCash.Text);


                if (TotalAmountCheq > 0)
                {
                    int i = 0;
                    var loopTo = GridDetail.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        DataGridViewTextBoxCell ChequeNoCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNo"];
                        DataGridViewTextBoxCell ChequeNumberCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNumber"];
                        DataGridViewTextBoxCell PayeeNameCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colPayeeName"];
                        DataGridViewTextBoxCell DateCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colDate"];
                        DataGridViewTextBoxCell AmountCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colAmount"];
                        paymentMethodList paymethod = new paymentMethodList();
                        paymethod.paymentMethodId = 2;
                        paymethod.bankBranch = ChequeNoCell.EditedFormattedValue.ToString();
                        paymethod.chequeNo = ChequeNumberCell.EditedFormattedValue.ToString();
                        paymethod.chequeOwner = PayeeNameCell.EditedFormattedValue.ToString();
                        DateTime dateTime = DateTime.Parse(DateCell.EditedFormattedValue.ToString());
                        DateTime dt = Convert.ToDateTime(dateTime.ToString("yyyy-MM-dd"), usCulture);
                        paymethod.chequeDate = dt.ToString("yyyy-MM-dd");
                        paymethod.chequeAmount = Convert.ToDouble(AmountCell.EditedFormattedValue.ToString());
                        paymethod.chequeBalance = TotalAmountCheq;
                        paymethod.cashierCheque = "N";
                        paymethod.chequeApproveBy = null;
                        _paymethodlst.Add(paymethod);
                    }
                }
                if (TotalAmount > 0)
                {
                    paymentMethodList paymethod = new paymentMethodList();
                    paymethod.paymentMethodId = 1;
                    paymethod.cashTotalAmount = AmountTotal;
                    paymethod.cashReceive = TotalAmount;
                    _paymethodlst.Add(paymethod);
                }
                pay.paymentMethodList = _paymethodlst;
                //receipt
                foreach (var itemRe in GlobalClass.LstInfo)
                {
                    var lst = GlobalClass.LstCost.Where(c => c.ca == itemRe.ca && c.status == true).OrderBy(g => g.ca).ToList();
                    var countType = lst.GroupBy(c => c.debtType).Select(g => new { DebtType = g.FirstOrDefault().debtType, Ca = g.FirstOrDefault().ca }).ToList();
                    foreach (var item in countType)
                    {
                        List<receiptDetailDtoList> _lstreceipt = new List<receiptDetailDtoList>();
                        DateTime dueDateDetail;
                        DateTime dateNow;
                        //paymentList==>receiptList
                        receiptList receipt = new receiptList();
                        receipt.receiptTypeId = 1;
                        receipt.receiptDebtType = item.DebtType;
                        receipt.receiptDate = dueDate.ToString("yyyy-MM-dd");
                        receipt.receiptPayer = itemRe.payeeEleName;
                        receipt.receiptPayerAddress = itemRe.payeeEleAddress;
                        receipt.receiptPayerTaxid = itemRe.payeeSerTax20;
                        receipt.receiptPayerBranch = itemRe.payeeEleTaxBranch;
                        receipt.distId = GlobalClass.DistId.ToString();
                        receipt.distName = GlobalClass.DistName;
                        receipt.receiptCustId = itemRe.custId;
                        receipt.receiptCustName = itemRe.bpTitle + " " + itemRe.bpFirstName + " " + itemRe.bpLastName;
                        receipt.receiptReqAddress = itemRe.coAddress;
                        receipt.receiptCa = itemRe.ca;
                        receipt.receiptUi = itemRe.ui;
                        receipt.receiptAmount = lst.Sum(itemTotal => Convert.ToDouble(itemTotal.debtBalance) - Convert.ToDouble(itemTotal.vatBalance)).ToString();
                        receipt.receiptPercentVat = "7";
                        receipt.receiptVat = lst.Sum(itemTotal => Convert.ToDouble(itemTotal.vatBalance)).ToString();
                        receipt.receiptTotalAmount = lst.Sum(itemTotal => Convert.ToDouble(itemTotal.debtBalance)).ToString();
                        double Defaultpenalty = 0;
                        foreach (var itemDetail in lst)
                        {
                            if (itemDetail.debtType == "ELE" && itemDetail.debtInsHdrId is null)
                            {
                                if (!(itemDetail.invNewDueDate is null))
                                    dueDateDetail = Convert.ToDateTime(itemDetail.invNewDueDate);
                                else
                                    dueDateDetail = Convert.ToDateTime(itemDetail.dueDate);
                                dateNow = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                                if (itemDetail.debtType != "GV" && Convert.ToDouble(itemDetail.debtBalance) > 10000 && DateTime.Now > Convert.ToDateTime(itemDetail.dueDate))
                                {
                                    var dtCountDay = dateNow - dueDateDetail;
                                    Defaultpenalty = (Convert.ToDouble(itemDetail.debtBalance) - Convert.ToDouble(itemDetail.vatBalance)) * dtCountDay.Days * (Convert.ToDouble(itemDetail.intRate) / 100) / 365;
                                }
                                else
                                    Defaultpenalty = 0;
                            }
                            else
                                Defaultpenalty = 0;
                        }
                        receipt.receiptIntAmount = Defaultpenalty;
                        receipt.receiptIntTotalAmount = lst.Sum(itemTotal => Convert.ToDouble(itemTotal.debtBalance)) + Defaultpenalty;
                        receipt.receiptPayerFlag = "N";
                        receipt.countCa = null;

                        var lstCost = lst.Where(c => c.ca == item.Ca && c.debtType == item.DebtType).ToList();

                        receipt.countInvoice = lst.Count();

                        double DtlAmount = 0;
                        double DtlTotalAmount = 0;
                        foreach (var itemRelst in lstCost)
                        {
                            receiptDetailDtoList receiptdetail = new receiptDetailDtoList();
                            //receiptList==>receiptDetailDtoList
                            receiptdetail.receiptDtlNo = itemRelst.debtId;
                            receiptdetail.receiptDtlName = itemRelst.reqDesc;
                            receiptdetail.receiptDtlAmount = Math.Round(Convert.ToDouble(itemRelst.debtBalance) - Convert.ToDouble(itemRelst.vatBalance), 2);
                            receiptdetail.receiptDtlVat = Convert.ToDouble(itemRelst.vatBalance);
                            receiptdetail.receiptDtlTotalAmount = Math.Round(Convert.ToDouble(itemRelst.debtBalance), 2);
                            receiptdetail.lastBillingDate = itemRelst.schdate;
                            receiptdetail.receiptDtlUnit = itemRelst.invNo;
                            receiptdetail.intAmount = itemRelst.totalkWh;
                            int countDay = 0;
                            #region CountDay
                            if (itemRelst.debtType == "ELE" && itemRelst.debtInsHdrId is null)
                            {
                                if (!(itemRelst.invNewDueDate is null))
                                    dueDate = Convert.ToDateTime(itemRelst.invNewDueDate);
                                else
                                    dueDate = Convert.ToDateTime(itemRelst.dueDate);
                                dateNow = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                                if (itemRelst.debtType != "GV" && Convert.ToDouble(itemRelst.debtBalance) > 10000 && DateTime.Now > Convert.ToDateTime(itemRelst.dueDate))
                                {
                                    var dtCountDay = dateNow - dueDate;
                                    countDay = dtCountDay.Days;
                                }
                                else
                                    countDay = 0;
                            }
                            else
                                countDay = 0;
                            #endregion
                            receiptdetail.intDay = countDay;
                            receiptdetail.ft = itemRelst.ft;

                            int[] num = new int[lstCost.Count];
                            for (int i = 0; i < lstCost.Count; i++)
                            {
                                int neki = int.Parse(itemRelst.debtId);
                                num[i] = neki;
                            }
                            receiptdetail.debtIdList = num;
                            _lstreceipt.Add(receiptdetail);
                            //receiptdetaillst.Add(receiptdetail);
                        }

                        receipt.receiptDetailDtoList = _lstreceipt;// receiptdetaillst;
                        receiptlst.Add(receipt);
                    }
                }
                pay.receiptList = receiptlst;
                _paylst.Add(pay);
                _mypay.paymentList = _paylst;
                getdata.GetPayment(_mypay);
            }
            catch (Exception ex)
            {

            }

        }
        private void FormTemp_Load(object sender, EventArgs e)
        {
            txtBranch.Focus();
            // LabelAmount.Text = Countamount.ToString("#,###,###,##0.00");
            SetControl();

        }

        public void SetControl()
        {
            GridDetail.AutoGenerateColumns = false;
            GridDetail.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


           
        }
        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        public void CheckType()
        {
            double typeAmount = 0;
            if (GlobalClass.LstCost.Count > 0)
            {
                foreach (var item in GlobalClass.LstCost)
                {
                    listCostTDebt lst = new listCostTDebt();
                    if (item.debtType == "DIS")
                    {
                        //typeAmount
                    }
                }
            }
        }
        private void TextBoxB1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else
                e.Handled = true;
        }
        private void TextBoxNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else
                e.Handled = true;

            if (cInt == 13)
            {
                string name = "";
                if (TextBoxBankBranch.Text != "")
                    name = arr.GetBankDesc(TextBoxBankBranch.Text);
                TextBoxName.Text = name;
            }
        }
        private void TextBoxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if ((int)e.KeyChar >= 48 && (int)e.KeyChar <= 57 || cInt == 8 || cInt == 46)
                e.Handled = false;
            else if (cInt == Convert.ToChar(Keys.Enter))
                SendKeys.Send("{TAB}");
            else
                e.Handled = true;
        }
        private void ComboBoxContract_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cInt = Convert.ToInt32(e.KeyChar);
            if (cInt == Convert.ToChar(Keys.Enter))
                SendKeys.Send("{TAB}");
        }
     
        private void ClearData()
        {
            try
            {
                TextAmountCheque.Text = "0.00";
                TextBoxChequeNo.Text = "";
                DateTimePickerCheque.Text = DateTime.Now.ToString("dd/MM/yyyy");
                TextBoxName.Text = "";
                TextBoxPayeeName.Text = "";
                TextBoxTel.Text = "";
                TextBoxBankBranch.Text = "";
                CheckCh.Checked = false;


                this.ButtonAdd.Enabled = true;
                this.ButtonUpdate.Enabled = false;
            }
            catch(Exception ex)
            {

            }
          
        }
       
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                OFC.ReadQrCode("");
               // AddChequeMain();
            }
            catch (Exception ex)
            {

            }
           
        }

        private void AddChequeMain()
        {
            try
            {
                if (CheckTextEmpty())
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    if (string.IsNullOrEmpty( this.GB_Chno) && CheckData())
                        MessageBox.Show("ข้อมูลเลขที่เช็คและรหัสธนาคาร/สาขา ไม่สามารถซ้ำกันได้ กรุณาตรวจสอบอีกครั้ง", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    else
                    {
                        bool status = false;
                        double AmountCredit = 0;
                        double TotalAmountCheq = 0;
                        double TotalAmount = 0;

                        if (labelAmountTotalCheq.Text != "")
                            TotalAmountCheq = Convert.ToDouble(labelAmountTotalCheq.Text);
                        if (txtCash.Text != "")
                            TotalAmount = Convert.ToDouble(txtCash.Text);

                        if (this.GridDetail.SelectedRows.Count > 0)
                        {
                            if (TotalAmount == 0 && AmountCredit == 0 && TotalAmountCheq > 0)
                            {
                                CountAmountCheq();
                                double amountSum = (TotalAmountCheq + Convert.ToDouble(TextAmountCheque.Text)) - Convert.ToDouble(txtAmount.Text);
                                if (amountSum > 10)
                                    status = true;
                            }
                        }
                        else
                        {
                            if (TotalAmount == 0 && AmountCredit == 0 && Convert.ToDouble(TextAmountCheque.Text) > 0)
                            {
                                double amountSum = Convert.ToDouble(TextAmountCheque.Text) - Convert.ToDouble(txtAmount.Text);
                                if (amountSum > 10)
                                    status = true;
                            }
                        }
                        if (status)
                        {
                            MessageBox.Show("มูลค่าเช็คที่ชำระเกินจากจำนวนเงินที่ต้องชำระมากกว่า 10 บาทไม่สามารถรับได้ ", "ชำระเกิน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        else
                        {

                            if (!string.IsNullOrEmpty(this.GB_Chno))
                            {
                                var rec = _cheque.Where(m => m.Chno == this.GB_Chno).FirstOrDefault();
                                _cheque.Remove(rec);
                                this.GB_Chno = "";
                            }
                            /***********************/
                            ClassCheque cheque = new ClassCheque();
                            cheque.Chno = TextBoxChequeNo.Text.Trim();
                            cheque.Bankno = TextBoxBankBranch.Text.Trim();
                            cheque.Bankname = TextBoxName.Text.Trim();
                            cheque.Chdate = DateTimePickerCheque.Text.ToString();
                            cheque.Chamount = TextAmountCheque.Text.Trim();
                            cheque.Chname = TextBoxPayeeName.Text.Trim();
                            cheque.Tel = TextBoxTel.Text.Trim();
                            cheque.Chcheck = CheckCh.Checked;
                            _cheque.Add(cheque);


                            var source = new BindingSource(_cheque, null);

                            this.GridDetail.DataSource = source;


                            CountamountAll = _cheque.Sum(m => double.Parse( m.Chamount));
                            //GlobalClass.LstCheque = _cheque;
                            //foreach (var item in GlobalClass.LstCheque)
                            //{
                            //    CountamountAll += Convert.ToDouble(item.Chamount);
                            //}
                            /***********************/


                            CountAmountCheq();
                            Summount();

                            ClearData();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Summount()
        {
            double totalAmount = 0;
            double amount = 0;
            double amountCheque = 0;
            double amountCreditCard = 0;
            double sumAmount = 0;
            if (txtCash.Text != "")
                amount = Convert.ToDouble(txtCash.Text);
            if (labelAmountTotalCheq.Text != "0.00")
                amountCheque = Convert.ToDouble(labelAmountTotalCheq.Text);


            totalAmount = amount + amountCheque + amountCreditCard;
            LabelAmountReceived.Text = totalAmount.ToString("#,###,###,##0.00");
            if (totalAmount > 0)
                sumAmount = totalAmount - Convert.ToDouble(txtAmount.Text.Trim());
            if (sumAmount < 0)
            {
                label25.Text = sumAmount.ToString("#,###,###,##0.00");
                LabelAmountChange.Text = "00";
            }
            else
            {
                LabelAmountChange.Text = sumAmount.ToString("#,###,###,##0.00");
                label25.Text = "00";
            }
        }
        public void CountAmountCheq()
        {
            double Amount = 0;
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                Amount += Convert.ToDouble(GridDetail.Rows[i].Cells["colAmount"].Value);
            }
            labelAmountTotalCheq.Text = Amount.ToString("#,###,###,##0.00");
            labelAmountCheq.Text = Amount.ToString("#,###,##0.00");
        }
        private bool CheckTextEmpty()
        {
            bool status = false;
            if (TextBoxChequeNo.Text == string.Empty)
                status = true;
            if (TextBoxBankBranch.Text == string.Empty)
                status = true;
            if (TextBoxName.Text == string.Empty)
                status = true;
            if (DateTimePickerCheque.Text == string.Empty)
                status = true;
            if (TextAmountCheque.Text == string.Empty)
                status = true;
            if (TextBoxPayeeName.Text == string.Empty)
                status = true;
            if (TextBoxTel.Text == string.Empty)
                status = true;
            return status;
        }
        private bool CheckData()
        {
            bool tf;
            var checkData = _cheque.Where(c => c.Chno == TextBoxChequeNo.Text).FirstOrDefault();
            if (checkData != null)
                tf = true;
            else
                tf = false;
            return tf;
        }
        public void AddChequeToList()
        {
            if (_cheque != null)
            {
                List<ClassCheque> _lst = new List<ClassCheque>();
                foreach (var item in _cheque)
                {
                    ClassCheque cheque = new ClassCheque();
                 
                    cheque.Chno = item.Chno;
                    cheque.Bankno = item.Bankno;
                    cheque.Bankname = item.Bankname;
                    cheque.Chdate = item.Chdate;
                    cheque.Chamount = item.Chamount;
                    cheque.Chname = item.Chname;
                    cheque.Tel = item.Tel;
                    cheque.Chcheck = item.Chcheck;
                    _lst.Add(cheque);
                }
                ClassCheque chequeNew = new ClassCheque();
              
                chequeNew.Chno = TextBoxChequeNo.Text.Trim();
                chequeNew.Bankno = TextBoxBankBranch.Text.Trim();
                chequeNew.Bankname = TextBoxName.Text.Trim();
                chequeNew.Chdate = DateTimePickerCheque.Text.ToString();
                chequeNew.Chamount = TextAmountCheque.Text.Trim();
                chequeNew.Chname = TextBoxPayeeName.Text.Trim();
                chequeNew.Tel = TextBoxTel.Text.Trim();
                chequeNew.Chcheck = CheckCh.Checked;
                _lst.Add(chequeNew);
                _cheque = _lst;
            }
            else
            {
                ClassCheque cheque = new ClassCheque();
                
                cheque.Chno = TextBoxChequeNo.Text.Trim();
                cheque.Bankno = TextBoxBankBranch.Text.Trim();
                cheque.Bankname = TextBoxName.Text.Trim();
                cheque.Chdate = DateTimePickerCheque.Text.ToString();
                cheque.Chamount = TextAmountCheque.Text.Trim();
                cheque.Chname = TextBoxPayeeName.Text.Trim();
                cheque.Tel = TextBoxTel.Text.Trim();
                cheque.Chcheck = CheckCh.Checked;
                _cheque.Add(cheque);
            }


            this.GridDetail.DataSource = _cheque;

            GlobalClass.LstCheque = _cheque;
            foreach (var item in GlobalClass.LstCheque)
            {
                CountamountAll += Convert.ToDouble(item.Chamount);
            }
        }
        private void GridDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    ClassCheque item = new ClassCheque();
                    string Chno = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                    item = _cheque.Where(m => m.Chno == Chno).FirstOrDefault();
                    /***********************/
                    TextBoxChequeNo.Text = item.Chno;
                    TextBoxBankBranch.Text = item.Bankno;
                    TextBoxName.Text = item.Bankname;
                    DateTimePickerCheque.Text = item .Chdate;
                    TextAmountCheque.Text = item.Chamount;
                    TextBoxPayeeName.Text = item.Chname;
                    TextBoxTel.Text = item.Tel;


                    this.GB_Chno = item.Chno;
                    this.ButtonUpdate.Enabled = true;
                    this.ButtonAdd.Enabled = false;
                    return;
                }
                if (e.ColumnIndex == 1)
                {
                    string Chno = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                    List<ClassCheque> LstNewCheque = new List<ClassCheque>();
                    LstNewCheque = _cheque.Where(m => m.Chno != Chno).ToList();
                    _cheque = LstNewCheque;
                    this.GridDetail.DataSource = _cheque;

                    GlobalClass.LstCheque = _cheque;
                    foreach (var item in GlobalClass.LstCheque)
                    {
                        CountamountAll += Convert.ToDouble(item.Chamount);
                    }
                    CountAmountCheq();
                    Summount();
                    return;
                } 

                //if (e.ColumnIndex == 1)
                //{
                //    FormArrears.FormMapCheque detail = new FormArrears.FormMapCheque();
                //    detail.labelNumber.Text = GridDetail.CurrentRow.Cells["colNumber"].Value.ToString();
                //    detail.labelNo.Text = GridDetail.CurrentRow.Cells["ColNo"].Value.ToString();
                //    detail.labelName.Text = GridDetail.CurrentRow.Cells["ColName"].Value.ToString();
                //    detail.labelDate.Text = GridDetail.CurrentRow.Cells["ColDate"].Value.ToString();
                //    detail.labelAmount.Text = GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString();
                //    detail.labelPayeeName.Text = GridDetail.CurrentRow.Cells["ColPayeeName"].Value.ToString();
                //    detail.labelTel.Text = GridDetail.CurrentRow.Cells["ColTel"].Value.ToString();
                //    detail.CheckCh.Checked = Convert.ToBoolean(GridDetail.CurrentRow.Cells["colchCash"].Value.ToString());
                //    detail.labelBalance.Text = (GridDetail.CurrentRow.Cells["ColBalance"].Value.ToString() != "") ? GridDetail.CurrentRow.Cells["ColAmount"].Value.ToString() : GridDetail.CurrentRow.Cells["ColBalance"].Value.ToString();
                //    detail.ShowDialog();
                //    detail.Dispose();
                //    LoadData();
                //}
               
            }
            catch (Exception ex)
            {

            }
           
        }
        public void LoadData()
        {
            if (GlobalClass.LstCheque != null)
                GridDetail.DataSource = GlobalClass.LstCheque;
        }
        private void TextAmount_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtCash.Text, "^[0-9]"))
                Summount();
        }
        public void SetDataListGridview()
        {
            int i = 0;
            var loopTo = GridDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                ClassListGridview grid = new ClassListGridview();
                DataGridViewTextBoxCell amountChequeCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colAmount"];
                DataGridViewTextBoxCell ChequeNoCell = (DataGridViewTextBoxCell)GridDetail.Rows[i].Cells["colNumber"];
                grid.ChNo = ChequeNoCell.EditedFormattedValue.ToString();
                grid.ChAmount = amountChequeCell.EditedFormattedValue.ToString();
                _lstGrid.Add(grid);
            }
        }


        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
               

                AddChequeMain();


            }
            catch(Exception ex)
            {

            }
        }

        private void TestAddCheque()
        {
            try
            {
                ClassCheque cheque = new ClassCheque();

                cheque.Chno = TextBoxChequeNo.Text.Trim();
                cheque.Bankno = TextBoxBankBranch.Text.Trim();
                cheque.Bankname = TextBoxName.Text.Trim();
                cheque.Chdate = DateTimePickerCheque.Text.ToString();
                cheque.Chamount = TextAmountCheque.Text.Trim();
                cheque.Chname = TextBoxPayeeName.Text.Trim();
                cheque.Tel = TextBoxTel.Text.Trim();
                cheque.Chcheck = CheckCh.Checked;
                _cheque.Add(cheque);
            }
            catch(Exception ex)
            {

            }
        }


        private void txtBranch_TextChanged(object sender, EventArgs e)
        {
            try
            {

                
                if(!GB_IsReadBarcode)
                {
                    GB_IsReadBarcode = true;
                    timer1.Interval = 1000;
                    timer1.Enabled = true;
                    timer1.Start();
                }

            }
            catch(Exception ex)
            {

            }
        }

        private void txtBranch_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
               
                //    if (e.KeyValue == 12)
                //{
                //    string strCurrentString = txtBranch.Text.Trim().ToString();
                //    if (strCurrentString != "")
                //    {
                //        //Do something with the barcode entered
                //        txtBranch.Text = "";
                //    }
                //    txtBranch.Focus();
                //}
            }
            catch(Exception ex)
            {

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                GB_IsReadBarcode = false;
                timer1.Stop();
                // MessageBox.Show(txtBranch.Text, "Time Elapsed");

               var ItemCode =   OFC.ReadQrCode(txtBranch.Text);
                txtBranch.Text = "";
                MessageBox.Show(ItemCode.CA + " " + ItemCode.DueDate + " " + ItemCode.InvoiceNumber + " " + ItemCode.AmountExVat, "Time Elapsed");
            }
            catch (Exception ex)
            {
                
            }
          

        }
    }
}
