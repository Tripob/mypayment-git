﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Transactions;
using System.Data.SQLite;
using MyPayment.Models;
using MyPayment.Master;
using System.Threading;
using System.Globalization;

namespace MyPayment.FormOffline
{
    public partial class FormOfflineSendService : Form
    {
        private static OffLineClass OFC = new OffLineClass();
        //  public static string DBPath = @"Data Source=" + @"D:\MEA" + @"\Data\MyPaymentDB.db;Version=3;New=False;Compress=True;";
        //  public static string DBPath = @"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + @"\Data\MyPaymentDB.db;Version=3;New=False;Compress=True;";
        public FormOfflineSendService()
        {
            InitializeComponent();
        }
       
        public static void DataGridViewCellVisibility(DataGridViewCell cell, bool visible)
        {
            cell.Style = visible ?
                  new DataGridViewCellStyle { Padding = new Padding(0, 0, 0, 0) } :
                  new DataGridViewCellStyle { Padding = new Padding(cell.OwningColumn.Width, 0, 0, 0) };
            cell.ReadOnly = !visible;
        }
        private void FormAdminReceipt_Load(object sender, EventArgs e)
        {
            try
            {
                this.GridCashierDesk.AutoGenerateColumns = false;
                this.GridCashierDesk.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.GridCashierDesk.RowTemplate.Height = 30;
                foreach (DataGridViewColumn col in this.GridCashierDesk.Columns)
                {
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                }
            }
            catch (Exception ex)
            {

            }
        }
        private void GridCashierDesk_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    int i = 0;
                    int countCheck = 0;
                    var loopTo = GridCashierDesk.Rows.Count - 1;
                    for (i = 0; i <= loopTo; i++)
                    {
                        if (GridCashierDesk.CurrentCell is DataGridViewCheckBoxCell)
                        {
                            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)GridCashierDesk.Rows[i].Cells["colCheck"];
                            bool newBool = (bool)checkCell.EditedFormattedValue;
                            if (newBool)
                            {
                                GridCashierDesk.Rows[i].Cells[0].Value = true;
                                countCheck++;
                                if (countCheck == GridCashierDesk.Rows.Count)
                                    CheckBoxSelect.Checked = true;
                            }
                            else
                            {
                                GridCashierDesk.Rows[i].Cells[0].Value = false;
                                CheckBoxSelect.Checked = false;
                                if (countCheck == 0)
                                    countCheck = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
                string strDate = DateTimePicker.Value.ToString("yyyy-MM-dd");
                BindingSource bs = new BindingSource();
                var result = OFC.GetSearchOffline(strDate);
                bs.DataSource = result;
                this.GridCashierDesk.DataSource = bs;
            }
            catch (Exception ex)
            {

            }
        }
        private void buttonSendData_Click(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
                string strDate = DateTimePicker.Value.ToString("yyyy-MM-dd");
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.GridCashierDesk.DataSource;
                List<ItemOfflineModel> LstItem = new List<ItemOfflineModel>();
                if (bs != null)
                {
                    LstItem = (List<ItemOfflineModel>)bs.DataSource;
                    LstItem = LstItem.Where(m => m.Selected == true).ToList();
                    if (LstItem.Count > 0)
                    {
                        string strReceiptNumber = string.Join(",", LstItem.Select(m => "'" + m.ReceiptNumber + "'").Distinct().ToArray());
                        var objResult = OFC.GenListOfflineToService(strDate, strReceiptNumber, true);
                        MessageBox.Show(objResult.OutputProcess.result_message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        if (objResult.OutputProcess.result_message == "SUCCESS")
                        {
                            bs = new BindingSource();
                            var result = OFC.GetSearchOffline(strDate);
                            bs.DataSource = result;
                            this.GridCashierDesk.DataSource = bs;
                            this.GridCashierDesk.Refresh();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<ItemIP> LstItem = new List<ItemIP>();
                var DataSource = this.GridCashierDesk.DataSource;
                LstItem = (List<ItemIP>)DataSource;
                string strResult = "";
                foreach (ItemIP item in LstItem)
                {
                    if (!item.Selected)
                        break;
                    strResult = OFC.SendRequest(item.IP, int.Parse(item.Port), item.Date);
                    item.hasData = true;
                    ItemSendData SendRec = new ItemSendData();
                    SendRec.IP = item.IP;
                    SendRec.CreateDate = item.Date;
                    SendRec.Data = strResult;
                    SendRec.CashierDesk = GlobalClass.CashierNo;
                    SendRec.Branch = GlobalClass.BranchId;
                    if (item.Data == "") //กรณีเป็นครั้งแรก ไม่มีข้อมูล
                        OFC.InsertSendData(SendRec);
                    else
                        OFC.UpdateSendData(SendRec);
                    item.Data = strResult;
                }
                GridCashierDesk.AutoGenerateColumns = false;
                GridCashierDesk.DataSource = LstItem;
                foreach (DataGridViewRow row in GridCashierDesk.Rows)
                {
                    if ((bool)GridCashierDesk.Rows[row.Index].Cells[0].Value)
                        DataGridViewCellVisibility(GridCashierDesk.Rows[row.Index].Cells[5], true);
                }
                GridCashierDesk.Refresh();
            }
            catch (Exception ex)
            {

            }
        }

        private void CheckBoxSelect_Click(object sender, EventArgs e)
        {
            if (GridCashierDesk.Rows.Count > 0)
            {
                int i = 0;
                var loopTo = GridCashierDesk.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (CheckBoxSelect.Checked == true)
                        GridCashierDesk.Rows[i].Cells["colCheck"].Value = true;
                    else
                        GridCashierDesk.Rows[i].Cells["colCheck"].Value = false;
                }
            }
        }
    }
}
