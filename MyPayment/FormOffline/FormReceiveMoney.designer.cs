﻿namespace MyPayment.FormOffline
{
    partial class FormReceiveMoney
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReceiveMoney));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelAmount = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.labelAmountTotalCheq = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.GridDetail = new System.Windows.Forms.DataGridView();
            this.ColChMapCheq = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPayeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colchCash = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.CheckCh = new System.Windows.Forms.CheckBox();
            this.ButtonDelete = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonEdit = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonAdd = new Custom_Controls_in_CS.ButtonZ();
            this.TextBoxTel = new System.Windows.Forms.TextBox();
            this.TextBoxPayeeName = new System.Windows.Forms.TextBox();
            this.TextBoxBankBranch = new System.Windows.Forms.TextBox();
            this.lblB1 = new System.Windows.Forms.Label();
            this.lblB2 = new System.Windows.Forms.Label();
            this.DateTimePickerCheque = new System.Windows.Forms.DateTimePicker();
            this.lblB5 = new System.Windows.Forms.Label();
            this.TextAmountCheque = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TextBoxName = new System.Windows.Forms.TextBox();
            this.lblB3 = new System.Windows.Forms.Label();
            this.lblB6 = new System.Windows.Forms.Label();
            this.TextBoxChequeNo = new System.Windows.Forms.TextBox();
            this.labelAmountCheq = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TextAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ButtonConfirm = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonCancel = new Custom_Controls_in_CS.ButtonZ();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblStatusC = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.LabelAmountChange = new System.Windows.Forms.Label();
            this.LabelAmountReceived = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).BeginInit();
            this.panel4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(994, 50);
            this.panel1.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LabelAmount);
            this.groupBox1.Controls.Add(this.Label4);
            this.groupBox1.Controls.Add(this.Label3);
            this.groupBox1.Location = new System.Drawing.Point(7, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(979, 51);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // LabelAmount
            // 
            this.LabelAmount.BackColor = System.Drawing.Color.White;
            this.LabelAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAmount.ForeColor = System.Drawing.Color.Blue;
            this.LabelAmount.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelAmount.Location = new System.Drawing.Point(153, 11);
            this.LabelAmount.Name = "LabelAmount";
            this.LabelAmount.Size = new System.Drawing.Size(301, 33);
            this.LabelAmount.TabIndex = 0;
            this.LabelAmount.Text = "1000.00";
            this.LabelAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(456, 14);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(39, 28);
            this.Label4.TabIndex = 3;
            this.Label4.Text = "บาท";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(2, 14);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(148, 28);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "จำนวนเงินที่ต้องชำระ";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 50);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(994, 473);
            this.panel3.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox2.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox2.Location = new System.Drawing.Point(7, -3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(974, 468);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "วิธีรับชำระเงิน";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.labelAmountTotalCheq);
            this.groupBox6.Controls.Add(this.panel5);
            this.groupBox6.Controls.Add(this.panel4);
            this.groupBox6.Controls.Add(this.labelAmountCheq);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox6.Location = new System.Drawing.Point(3, 82);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox6.Size = new System.Drawing.Size(968, 281);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "รายการเช็ค";
            // 
            // labelAmountTotalCheq
            // 
            this.labelAmountTotalCheq.BackColor = System.Drawing.Color.White;
            this.labelAmountTotalCheq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmountTotalCheq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmountTotalCheq.ForeColor = System.Drawing.Color.Blue;
            this.labelAmountTotalCheq.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelAmountTotalCheq.Location = new System.Drawing.Point(771, 242);
            this.labelAmountTotalCheq.Name = "labelAmountTotalCheq";
            this.labelAmountTotalCheq.Size = new System.Drawing.Size(182, 28);
            this.labelAmountTotalCheq.TabIndex = 1;
            this.labelAmountTotalCheq.Text = "0.00";
            this.labelAmountTotalCheq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.GridDetail);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 126);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(962, 113);
            this.panel5.TabIndex = 1;
            // 
            // GridDetail
            // 
            this.GridDetail.AllowUserToAddRows = false;
            this.GridDetail.BackgroundColor = System.Drawing.Color.White;
            this.GridDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColChMapCheq,
            this.colNumber,
            this.colNo,
            this.colName,
            this.colDate,
            this.colAmount,
            this.colTel,
            this.colPayeeName,
            this.colchCash,
            this.ColBalance});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Firebrick;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridDetail.DefaultCellStyle = dataGridViewCellStyle8;
            this.GridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDetail.Location = new System.Drawing.Point(0, 0);
            this.GridDetail.MultiSelect = false;
            this.GridDetail.Name = "GridDetail";
            this.GridDetail.ReadOnly = true;
            this.GridDetail.RowHeadersVisible = false;
            this.GridDetail.RowHeadersWidth = 10;
            this.GridDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridDetail.Size = new System.Drawing.Size(962, 113);
            this.GridDetail.TabIndex = 2;
            this.GridDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridDetail_CellClick);
            // 
            // ColChMapCheq
            // 
            this.ColChMapCheq.DataPropertyName = "Status";
            this.ColChMapCheq.HeaderText = "";
            this.ColChMapCheq.Name = "ColChMapCheq";
            this.ColChMapCheq.ReadOnly = true;
            this.ColChMapCheq.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColChMapCheq.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColChMapCheq.Width = 30;
            // 
            // colNumber
            // 
            this.colNumber.DataPropertyName = "Chno";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = null;
            this.colNumber.DefaultCellStyle = dataGridViewCellStyle1;
            this.colNumber.HeaderText = "เลขที่เช็ค";
            this.colNumber.Name = "colNumber";
            this.colNumber.ReadOnly = true;
            // 
            // colNo
            // 
            this.colNo.DataPropertyName = "Bankno";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.colNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.colNo.HeaderText = "รหัสธนาคาร";
            this.colNo.Name = "colNo";
            this.colNo.ReadOnly = true;
            this.colNo.Width = 120;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Bankname";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.colName.DefaultCellStyle = dataGridViewCellStyle3;
            this.colName.HeaderText = "ชื่อธนาคาร/สาขา";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colDate
            // 
            this.colDate.DataPropertyName = "Chdate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colDate.DefaultCellStyle = dataGridViewCellStyle4;
            this.colDate.HeaderText = "วันที่เช็ค";
            this.colDate.Name = "colDate";
            this.colDate.ReadOnly = true;
            this.colDate.Width = 120;
            // 
            // colAmount
            // 
            this.colAmount.DataPropertyName = "Chamount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,###,###,##0.00";
            this.colAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.colAmount.HeaderText = "จำนวนเงิน";
            this.colAmount.Name = "colAmount";
            this.colAmount.ReadOnly = true;
            // 
            // colTel
            // 
            this.colTel.DataPropertyName = "Tel";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colTel.DefaultCellStyle = dataGridViewCellStyle6;
            this.colTel.HeaderText = "เบอร์โทร";
            this.colTel.Name = "colTel";
            this.colTel.ReadOnly = true;
            // 
            // colPayeeName
            // 
            this.colPayeeName.DataPropertyName = "Chname";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.colPayeeName.DefaultCellStyle = dataGridViewCellStyle7;
            this.colPayeeName.HeaderText = "ชื่อผู้สั่งจ่าย";
            this.colPayeeName.Name = "colPayeeName";
            this.colPayeeName.ReadOnly = true;
            this.colPayeeName.Width = 200;
            // 
            // colchCash
            // 
            this.colchCash.DataPropertyName = "Chcheck";
            this.colchCash.HeaderText = "แคชเชียร์เช็ค";
            this.colchCash.Name = "colchCash";
            this.colchCash.ReadOnly = true;
            this.colchCash.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colchCash.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colchCash.Width = 120;
            // 
            // ColBalance
            // 
            this.ColBalance.DataPropertyName = "Balance";
            this.ColBalance.HeaderText = "คงเหลือ";
            this.ColBalance.Name = "ColBalance";
            this.ColBalance.ReadOnly = true;
            this.ColBalance.Visible = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.CheckCh);
            this.panel4.Controls.Add(this.ButtonDelete);
            this.panel4.Controls.Add(this.ButtonEdit);
            this.panel4.Controls.Add(this.ButtonAdd);
            this.panel4.Controls.Add(this.TextBoxTel);
            this.panel4.Controls.Add(this.TextBoxPayeeName);
            this.panel4.Controls.Add(this.TextBoxBankBranch);
            this.panel4.Controls.Add(this.lblB1);
            this.panel4.Controls.Add(this.lblB2);
            this.panel4.Controls.Add(this.DateTimePickerCheque);
            this.panel4.Controls.Add(this.lblB5);
            this.panel4.Controls.Add(this.TextAmountCheque);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.TextBoxName);
            this.panel4.Controls.Add(this.lblB3);
            this.panel4.Controls.Add(this.lblB6);
            this.panel4.Controls.Add(this.TextBoxChequeNo);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 29);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(962, 97);
            this.panel4.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(566, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 26);
            this.label1.TabIndex = 66;
            this.label1.Text = "บาท";
            // 
            // CheckCh
            // 
            this.CheckCh.AutoSize = true;
            this.CheckCh.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CheckCh.ForeColor = System.Drawing.Color.Black;
            this.CheckCh.Location = new System.Drawing.Point(568, 65);
            this.CheckCh.Name = "CheckCh";
            this.CheckCh.Size = new System.Drawing.Size(115, 30);
            this.CheckCh.TabIndex = 7;
            this.CheckCh.Text = "แคชเชียร์เช็ค";
            this.CheckCh.UseVisualStyleBackColor = true;
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.BackColor = System.Drawing.Color.Transparent;
            this.ButtonDelete.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonDelete.BorderWidth = 1;
            this.ButtonDelete.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonDelete.ButtonText = "ลบ";
            this.ButtonDelete.CausesValidation = false;
            this.ButtonDelete.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonDelete.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonDelete.ForeColor = System.Drawing.Color.Black;
            this.ButtonDelete.GradientAngle = 90;
            this.ButtonDelete.Image = global::MyPayment.Properties.Resources.Recycle;
            this.ButtonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonDelete.Location = new System.Drawing.Point(866, 64);
            this.ButtonDelete.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonDelete.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonDelete.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonDelete.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.ShowButtontext = true;
            this.ButtonDelete.Size = new System.Drawing.Size(83, 28);
            this.ButtonDelete.StartColor = System.Drawing.Color.LightGray;
            this.ButtonDelete.TabIndex = 10;
            this.ButtonDelete.TextLocation_X = 35;
            this.ButtonDelete.TextLocation_Y = 1;
            this.ButtonDelete.Transparent1 = 80;
            this.ButtonDelete.Transparent2 = 120;
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // ButtonEdit
            // 
            this.ButtonEdit.BackColor = System.Drawing.Color.Transparent;
            this.ButtonEdit.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonEdit.BorderWidth = 1;
            this.ButtonEdit.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonEdit.ButtonText = "แก้ไข";
            this.ButtonEdit.CausesValidation = false;
            this.ButtonEdit.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonEdit.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonEdit.ForeColor = System.Drawing.Color.Black;
            this.ButtonEdit.GradientAngle = 90;
            this.ButtonEdit.Image = global::MyPayment.Properties.Resources.icons8_edit_32;
            this.ButtonEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonEdit.Location = new System.Drawing.Point(775, 64);
            this.ButtonEdit.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonEdit.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonEdit.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonEdit.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonEdit.Name = "ButtonEdit";
            this.ButtonEdit.ShowButtontext = true;
            this.ButtonEdit.Size = new System.Drawing.Size(83, 28);
            this.ButtonEdit.StartColor = System.Drawing.Color.LightGray;
            this.ButtonEdit.TabIndex = 9;
            this.ButtonEdit.TextLocation_X = 32;
            this.ButtonEdit.TextLocation_Y = 1;
            this.ButtonEdit.Transparent1 = 80;
            this.ButtonEdit.Transparent2 = 120;
            this.ButtonEdit.UseVisualStyleBackColor = true;
            this.ButtonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
            // 
            // ButtonAdd
            // 
            this.ButtonAdd.BackColor = System.Drawing.Color.Transparent;
            this.ButtonAdd.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonAdd.BorderWidth = 1;
            this.ButtonAdd.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonAdd.ButtonText = "เพิ่ม";
            this.ButtonAdd.CausesValidation = false;
            this.ButtonAdd.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdd.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonAdd.ForeColor = System.Drawing.Color.Black;
            this.ButtonAdd.GradientAngle = 90;
            this.ButtonAdd.Image = global::MyPayment.Properties.Resources.add;
            this.ButtonAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonAdd.Location = new System.Drawing.Point(684, 64);
            this.ButtonAdd.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonAdd.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonAdd.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonAdd.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonAdd.Name = "ButtonAdd";
            this.ButtonAdd.ShowButtontext = true;
            this.ButtonAdd.Size = new System.Drawing.Size(83, 28);
            this.ButtonAdd.StartColor = System.Drawing.Color.LightGray;
            this.ButtonAdd.TabIndex = 8;
            this.ButtonAdd.TextLocation_X = 32;
            this.ButtonAdd.TextLocation_Y = 1;
            this.ButtonAdd.Transparent1 = 80;
            this.ButtonAdd.Transparent2 = 120;
            this.ButtonAdd.UseVisualStyleBackColor = true;
            this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // TextBoxTel
            // 
            this.TextBoxTel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxTel.Location = new System.Drawing.Point(356, 65);
            this.TextBoxTel.Name = "TextBoxTel";
            this.TextBoxTel.Size = new System.Drawing.Size(208, 26);
            this.TextBoxTel.TabIndex = 6;
            // 
            // TextBoxPayeeName
            // 
            this.TextBoxPayeeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxPayeeName.Location = new System.Drawing.Point(79, 65);
            this.TextBoxPayeeName.Name = "TextBoxPayeeName";
            this.TextBoxPayeeName.Size = new System.Drawing.Size(204, 26);
            this.TextBoxPayeeName.TabIndex = 5;
            // 
            // TextBoxBankBranch
            // 
            this.TextBoxBankBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxBankBranch.Location = new System.Drawing.Point(356, 5);
            this.TextBoxBankBranch.Name = "TextBoxBankBranch";
            this.TextBoxBankBranch.Size = new System.Drawing.Size(133, 26);
            this.TextBoxBankBranch.TabIndex = 1;
            this.TextBoxBankBranch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxNo_KeyPress);
            // 
            // lblB1
            // 
            this.lblB1.AutoSize = true;
            this.lblB1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB1.Location = new System.Drawing.Point(5, 5);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(69, 26);
            this.lblB1.TabIndex = 6;
            this.lblB1.Text = "เลขที่เช็ค";
            // 
            // lblB2
            // 
            this.lblB2.AutoSize = true;
            this.lblB2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB2.Location = new System.Drawing.Point(5, 34);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(114, 26);
            this.lblB2.TabIndex = 61;
            this.lblB2.Text = "วันที่ในเช็ค(พ.ศ.)";
            this.lblB2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DateTimePickerCheque
            // 
            this.DateTimePickerCheque.CustomFormat = "dd/MM/yyyy";
            this.DateTimePickerCheque.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DateTimePickerCheque.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerCheque.Location = new System.Drawing.Point(121, 34);
            this.DateTimePickerCheque.Name = "DateTimePickerCheque";
            this.DateTimePickerCheque.Size = new System.Drawing.Size(162, 26);
            this.DateTimePickerCheque.TabIndex = 3;
            // 
            // lblB5
            // 
            this.lblB5.AutoSize = true;
            this.lblB5.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB5.Location = new System.Drawing.Point(5, 65);
            this.lblB5.Name = "lblB5";
            this.lblB5.Size = new System.Drawing.Size(76, 26);
            this.lblB5.TabIndex = 62;
            this.lblB5.Text = "ชื่อผู้สั่งจ่าย";
            // 
            // TextAmountCheque
            // 
            this.TextAmountCheque.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmountCheque.Location = new System.Drawing.Point(356, 34);
            this.TextAmountCheque.Name = "TextAmountCheque";
            this.TextAmountCheque.Size = new System.Drawing.Size(208, 26);
            this.TextAmountCheque.TabIndex = 4;
            this.TextAmountCheque.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxAmount_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(281, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 26);
            this.label11.TabIndex = 63;
            this.label11.Text = "จำนวนเงิน";
            // 
            // TextBoxName
            // 
            this.TextBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxName.Location = new System.Drawing.Point(495, 5);
            this.TextBoxName.Name = "TextBoxName";
            this.TextBoxName.Size = new System.Drawing.Size(454, 26);
            this.TextBoxName.TabIndex = 2;
            // 
            // lblB3
            // 
            this.lblB3.AutoSize = true;
            this.lblB3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB3.Location = new System.Drawing.Point(234, 5);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(120, 26);
            this.lblB3.TabIndex = 64;
            this.lblB3.Text = "รหัสธนาคาร/สาขา";
            // 
            // lblB6
            // 
            this.lblB6.AutoSize = true;
            this.lblB6.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblB6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblB6.Location = new System.Drawing.Point(289, 66);
            this.lblB6.Name = "lblB6";
            this.lblB6.Size = new System.Drawing.Size(65, 26);
            this.lblB6.TabIndex = 65;
            this.lblB6.Text = "เบอร์โทร";
            // 
            // TextBoxChequeNo
            // 
            this.TextBoxChequeNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxChequeNo.Location = new System.Drawing.Point(74, 5);
            this.TextBoxChequeNo.Name = "TextBoxChequeNo";
            this.TextBoxChequeNo.Size = new System.Drawing.Size(153, 26);
            this.TextBoxChequeNo.TabIndex = 0;
            this.TextBoxChequeNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxB1_KeyPress);
            // 
            // labelAmountCheq
            // 
            this.labelAmountCheq.BackColor = System.Drawing.Color.White;
            this.labelAmountCheq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmountCheq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAmountCheq.ForeColor = System.Drawing.Color.Blue;
            this.labelAmountCheq.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelAmountCheq.Location = new System.Drawing.Point(467, 242);
            this.labelAmountCheq.Name = "labelAmountCheq";
            this.labelAmountCheq.Size = new System.Drawing.Size(147, 28);
            this.labelAmountCheq.TabIndex = 0;
            this.labelAmountCheq.Text = "0.00";
            this.labelAmountCheq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(625, 242);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(144, 28);
            this.label12.TabIndex = 2;
            this.label12.Text = "รวมมูลค่าเช็คทั้งหมด";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.TextAmount);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox3.Location = new System.Drawing.Point(3, 29);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox3.Size = new System.Drawing.Size(968, 53);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "เงินสด";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(375, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 28);
            this.label7.TabIndex = 2;
            this.label7.Text = "บาท";
            // 
            // TextAmount
            // 
            this.TextAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextAmount.Location = new System.Drawing.Point(83, 18);
            this.TextAmount.Name = "TextAmount";
            this.TextAmount.Size = new System.Drawing.Size(290, 31);
            this.TextAmount.TabIndex = 0;
            this.TextAmount.TextChanged += new System.EventHandler(this.TextAmount_TextChanged);
            this.TextAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAmount_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(3, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 28);
            this.label6.TabIndex = 1;
            this.label6.Text = "จำนวนเงิน";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.ButtonConfirm);
            this.panel6.Controls.Add(this.ButtonCancel);
            this.panel6.Controls.Add(this.groupBox5);
            this.panel6.Controls.Add(this.groupBox4);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 523);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(994, 183);
            this.panel6.TabIndex = 1;
            // 
            // ButtonConfirm
            // 
            this.ButtonConfirm.BackColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonConfirm.BorderWidth = 1;
            this.ButtonConfirm.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonConfirm.ButtonText = "ยืนยัน";
            this.ButtonConfirm.CausesValidation = false;
            this.ButtonConfirm.EndColor = System.Drawing.Color.Silver;
            this.ButtonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonConfirm.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonConfirm.ForeColor = System.Drawing.Color.Black;
            this.ButtonConfirm.GradientAngle = 90;
            this.ButtonConfirm.Image = global::MyPayment.Properties.Resources.ok1;
            this.ButtonConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonConfirm.Location = new System.Drawing.Point(774, 151);
            this.ButtonConfirm.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonConfirm.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonConfirm.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonConfirm.Name = "ButtonConfirm";
            this.ButtonConfirm.ShowButtontext = true;
            this.ButtonConfirm.Size = new System.Drawing.Size(90, 28);
            this.ButtonConfirm.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonConfirm.TabIndex = 0;
            this.ButtonConfirm.TextLocation_X = 35;
            this.ButtonConfirm.TextLocation_Y = 1;
            this.ButtonConfirm.Transparent1 = 80;
            this.ButtonConfirm.Transparent2 = 120;
            this.ButtonConfirm.UseVisualStyleBackColor = true;
            this.ButtonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonCancel.BorderWidth = 1;
            this.ButtonCancel.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonCancel.ButtonText = "ยกเลิก";
            this.ButtonCancel.CausesValidation = false;
            this.ButtonCancel.EndColor = System.Drawing.Color.Silver;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCancel.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.ButtonCancel.GradientAngle = 90;
            this.ButtonCancel.Image = global::MyPayment.Properties.Resources.close2;
            this.ButtonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCancel.Location = new System.Drawing.Point(871, 151);
            this.ButtonCancel.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonCancel.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonCancel.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.ShowButtontext = true;
            this.ButtonCancel.Size = new System.Drawing.Size(90, 28);
            this.ButtonCancel.StartColor = System.Drawing.Color.Gainsboro;
            this.ButtonCancel.TabIndex = 1;
            this.ButtonCancel.TextLocation_X = 35;
            this.ButtonCancel.TextLocation_Y = 1;
            this.ButtonCancel.Transparent1 = 80;
            this.ButtonCancel.Transparent2 = 120;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblStatusC);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.LabelAmountChange);
            this.groupBox5.Controls.Add(this.LabelAmountReceived);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(12, 47);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(969, 98);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            // 
            // lblStatusC
            // 
            this.lblStatusC.BackColor = System.Drawing.Color.LightGray;
            this.lblStatusC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatusC.Font = new System.Drawing.Font("TH Sarabun New", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblStatusC.ForeColor = System.Drawing.Color.Red;
            this.lblStatusC.Location = new System.Drawing.Point(38, 44);
            this.lblStatusC.Name = "lblStatusC";
            this.lblStatusC.Size = new System.Drawing.Size(225, 35);
            this.lblStatusC.TabIndex = 1000000024;
            this.lblStatusC.Text = "***ห้ามรับเช็ค***";
            this.lblStatusC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblStatusC.Visible = false;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.White;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label28.Location = new System.Drawing.Point(346, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(103, 28);
            this.label28.TabIndex = 1000000023;
            this.label28.Text = "0.00";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelAmountChange
            // 
            this.LabelAmountChange.BackColor = System.Drawing.Color.White;
            this.LabelAmountChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelAmountChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAmountChange.ForeColor = System.Drawing.Color.Red;
            this.LabelAmountChange.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelAmountChange.Location = new System.Drawing.Point(579, 55);
            this.LabelAmountChange.Name = "LabelAmountChange";
            this.LabelAmountChange.Size = new System.Drawing.Size(370, 40);
            this.LabelAmountChange.TabIndex = 1000000020;
            this.LabelAmountChange.Text = "0.00";
            this.LabelAmountChange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelAmountReceived
            // 
            this.LabelAmountReceived.BackColor = System.Drawing.Color.White;
            this.LabelAmountReceived.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelAmountReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAmountReceived.ForeColor = System.Drawing.Color.Blue;
            this.LabelAmountReceived.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelAmountReceived.Location = new System.Drawing.Point(579, 10);
            this.LabelAmountReceived.Name = "LabelAmountReceived";
            this.LabelAmountReceived.Size = new System.Drawing.Size(370, 40);
            this.LabelAmountReceived.TabIndex = 1000000019;
            this.LabelAmountReceived.Text = "0.00";
            this.LabelAmountReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(469, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 28);
            this.label13.TabIndex = 68;
            this.label13.Text = "จำนวนเงินที่รับ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(514, 61);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 28);
            this.label15.TabIndex = 65;
            this.label15.Text = "เงินทอน";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(259, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 28);
            this.label16.TabIndex = 66;
            this.label16.Text = "การปัดเศษ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.Label14);
            this.groupBox4.Controls.Add(this.Label20);
            this.groupBox4.Controls.Add(this.Label18);
            this.groupBox4.Controls.Add(this.Label17);
            this.groupBox4.Location = new System.Drawing.Point(12, -2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(969, 50);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.White;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label25.Location = new System.Drawing.Point(579, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(370, 35);
            this.label25.TabIndex = 1000000021;
            this.label25.Text = "0.00";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.White;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label24.Location = new System.Drawing.Point(125, 10);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(147, 35);
            this.label24.TabIndex = 1000000021;
            this.label24.Text = "0.00";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label14.Location = new System.Drawing.Point(518, 13);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(59, 28);
            this.Label14.TabIndex = 68;
            this.Label14.Text = "เงินขาด";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label20.Location = new System.Drawing.Point(278, 13);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(29, 28);
            this.Label20.TabIndex = 65;
            this.Label20.Text = "ใบ";
            this.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label18.Location = new System.Drawing.Point(13, 13);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(106, 28);
            this.Label18.TabIndex = 66;
            this.Label18.Text = "จำนวนใบเสร็จ";
            this.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label17.Location = new System.Drawing.Point(319, 13);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(150, 28);
            this.Label17.TabIndex = 67;
            this.Label17.Text = "จำนวนเงินรับล่วงหน้า";
            this.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Label17.Visible = false;
            // 
            // FormReceiveMoney
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 706);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormReceiveMoney";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รับเงิน";
            this.Load += new System.EventHandler(this.FormReceiveMoney_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridDetail)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TextAmount;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Label Label17;
        private System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label LabelAmount;
        internal System.Windows.Forms.Label LabelAmountChange;
        internal System.Windows.Forms.Label LabelAmountReceived;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label lblStatusC;
        private Custom_Controls_in_CS.ButtonZ ButtonConfirm;
        private Custom_Controls_in_CS.ButtonZ ButtonCancel;
        private System.Windows.Forms.GroupBox groupBox6;
        internal System.Windows.Forms.Label labelAmountTotalCheq;
        internal System.Windows.Forms.Label labelAmountCheq;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.DataGridView GridDetail;
        private System.Windows.Forms.Panel panel4;
        private Custom_Controls_in_CS.ButtonZ ButtonDelete;
        private Custom_Controls_in_CS.ButtonZ ButtonEdit;
        private Custom_Controls_in_CS.ButtonZ ButtonAdd;
        private System.Windows.Forms.TextBox TextBoxTel;
        private System.Windows.Forms.TextBox TextBoxPayeeName;
        private System.Windows.Forms.TextBox TextBoxBankBranch;
        internal System.Windows.Forms.Label lblB1;
        internal System.Windows.Forms.Label lblB2;
        private System.Windows.Forms.DateTimePicker DateTimePickerCheque;
        internal System.Windows.Forms.Label lblB5;
        private System.Windows.Forms.TextBox TextAmountCheque;
        internal System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TextBoxName;
        internal System.Windows.Forms.Label lblB3;
        internal System.Windows.Forms.Label lblB6;
        private System.Windows.Forms.TextBox TextBoxChequeNo;
        internal System.Windows.Forms.CheckBox CheckCh;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColChMapCheq;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPayeeName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colchCash;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBalance;
        private System.Windows.Forms.Panel panel5;
    }
}