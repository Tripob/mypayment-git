﻿using MyPayment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.Controllers
{
    class GetDataReportController
    {
        ClassLogsService ReceiptLog = new ClassLogsService("ReceipLog", "DataLog");
        ClassLogsService ReceiptErrorLog = new ClassLogsService("ReceipLog", "ErrorLog");
        ClassLogsService PaymentLog = new ClassLogsService("PaymentLogInput", "DataLog");
        public ResultProcessPayment ReturnProcessPayment(MyPaymentList mylist)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultProcessPayment _payment = new ResultProcessPayment();
            try
            {
                string result = JsonConvert.SerializeObject(mylist);
                PaymentLog.WriteData("Payment Data :: " + result, method, LogLevel.Debug);
                var strJSON = ClassReadAPI.PostDataAPI("processPayment", result);
                ReceiptLog.WriteData("Return Data :: " + strJSON, method, LogLevel.Debug);
                ClassOutputProcessPayment Payment = JsonConvert.DeserializeObject<ClassOutputProcessPayment>(strJSON);
                if (Payment.result_code == "SUCCESS")
                {
                    if (Payment.paymentResponseDtoList != null)
                        _payment.OutputProcess = Payment;
                }
                else
                    _payment.ResultMessage = Payment.result_message;               
            }
            catch (Exception ex)
            {
                ReceiptErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return _payment;
        }
        public ResultProcessPayment ReturnProcessPaymentOFF(MyPaymentListOFF mylist)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultProcessPayment _payment = new ResultProcessPayment();
            try
            {
                string result = JsonConvert.SerializeObject(mylist);
                PaymentLog.WriteData("Payment Data :: " + result, method, LogLevel.Debug);
                var strJSON = ClassReadAPI.PostDataAPI("processPayment", result, GlobalClass.Timer);
                ClassOutputProcessPayment Payment = JsonConvert.DeserializeObject<ClassOutputProcessPayment>(strJSON);
                if (Payment.result_code == "SUCCESS")
                {
                    if (Payment.paymentResponseDtoList != null)
                        _payment.OutputProcess = Payment;
                }
                else
                    _payment.ResultMessage = Payment.result_message;
                ReceiptLog.WriteData("Return Data :: " + strJSON, method, LogLevel.Debug);
            }
            catch (Exception ex)
            {
                ReceiptErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return _payment;
        }
        public ResultProcessPayment ProcessGetIpAddressByDistId(string strParam)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();

            ResultProcessPayment _payment = new ResultProcessPayment();
            try
            {
                var strJSON = ClassReadAPI.PostDataAPI("processGetIpAddressByDistId", strParam, GlobalClass.Timer);
                ClassOutputProcessPayment Payment = JsonConvert.DeserializeObject<ClassOutputProcessPayment>(strJSON);
                if (Payment.result_code == "SUCCESS")
                {
                    if (Payment.ipAddressList != null)
                        _payment.OutputProcess = Payment;
                }
                else
                    _payment.ResultMessage = Payment.result_message;
                ReceiptLog.WriteData("Return Data :: " + strJSON, method, LogLevel.Debug);
                return _payment;
            }
            catch(Exception ex)
            {
                ReceiptErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                return _payment;
            }
        }
        public string GetPaymentOTH(MyPaymentListOTH mylist)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ClassOutputProcessPayment deserializedMember = new ClassOutputProcessPayment();
            string strJSON = "";
            try
            {
                string result = JsonConvert.SerializeObject(mylist);
                // var strJSON = ClassReadAPI.GetDataAPI("directPaymentOth", result);
                strJSON = ClassReadAPI.PostDataAPI("processPayment", result, GlobalClass.Timer);

                // deserializedMember = new JavaScriptSerializer().Deserialize<ClassOutputProcessPayment>(strJSON);
                PaymentLog.WriteData("Payment Data :: " + result, method, LogLevel.Debug);
                ReceiptLog.WriteData("Return Data :: " + strJSON, method, LogLevel.Debug);
            }
            catch (Exception ex)
            {
                strJSON = "Exception : " + ex.Message.ToString();
                ReceiptErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return strJSON;
        }
       
    }
}
