﻿using MyPayment.FormArrears;
using MyPayment.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static MyPayment.Models.ClassLogsService;
using static MyPayment.Models.ClassReadAPI;
using System.Threading;
using MyPayment.Master;

namespace MyPayment.Controllers
{
    class ArrearsController
    {
        ClassLogsService ControlLog = new ClassLogsService("Controllers", "EventLog");
        ClassLogsService ControlEventLog = new ClassLogsService("Controllers", "EventLog");
        ClassLogsService ControlErrorLog = new ClassLogsService("Controllers", "EventLog");
        ClassLogsService LogInquiry = new ClassLogsService("Inquiry", "DataLog");
        ClassLogsService LogEventInquiry = new ClassLogsService("Inquiry", "EventLog");
        ClassLogsService LogErrorInquiry = new ClassLogsService("Inquiry", "ErrorLog");
        ClassLogsService LogRemittance = new ClassLogsService("Remittance", "DataLog");
        ClassLogsService LogEventRemittance = new ClassLogsService("Remittance", "EventLog");
        ClassLogsService LogErrorRemittance = new ClassLogsService("Remittance", "ErrorLog");
        public CultureInfo thCulture = new CultureInfo("th-TH");
        public CultureInfo usCulture = new CultureInfo("en-US");
        private static OffLineClass OFC = new OffLineClass();
        public ResultInquiryDebt SelectProcessInquiry(ClassUser cls, ResultInquiryDebt _r, string barcode, int status)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultInquiryDebt rs = new ResultInquiryDebt();
            try
            {
                string strJSON = null;
                string result = JsonConvert.SerializeObject(cls);
                LogInquiry.WriteData("Inquiry Data In :: " + result, method, LogLevel.Debug);
                strJSON = ClassReadAPI.PostDataAPI("processInquiry", result, GlobalClass.Timer);
                LogInquiry.WriteData("Inquiry Data Out :: " + strJSON, method, LogLevel.Debug);
                ResultInquiryDebt clsData = ClassReadAPI.ReadToObjectInquiry(strJSON);
                rs.Output = new List<inquiryInfoBeanList>();
                if (_r.Output != null && _r.Output.Count != 0)
                {
                    if (clsData.OutputInquiryDebt.inquiryBean != null)
                    {
                        foreach (inquiryInfoBeanList item in clsData.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
                        {
                            List<inquiryInfoBeanList> lstData = _r.Output.Where(c => c.ca == item.ca).ToList();
                            if (lstData.Count == 0)
                                rs = AddDatainquiryDebt(clsData, _r, status);
                            else
                            {
                                rs.Output = _r.Output;
                                rs.ResultMessage = "มีข้อมูล " + item.ca + " อยู่แล้ว";
                                rs.ResultCode = "0000";
                                return rs;
                            }
                        }
                    }
                    if (clsData.ResultMessage != null)
                    {
                        LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                        rs.ResultMessage = clsData.ResultMessage.ToString();
                        rs.ResultCode = clsData.ResultCode.ToString();
                    }
                }
                else
                {
                    if (clsData.OutputInquiryDebt != null && clsData.ResultMessage == null)
                        rs = AddDatainquiry(clsData, status);
                    else
                    {
                        if (clsData.ResultMessage != "SUCCESS" && clsData.OutputInquiryDebt.inquiryBean != null)
                        {
                            LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                            rs.ResultMessage = clsData.ResultMessage.ToString();
                        }
                        if (clsData.OutputInquiryDebt.inquiryBean != null)
                        {
                            foreach (inquiryInfoBeanList item in clsData.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
                            {
                                string name = "";
                                inquiryInfoBeanList models = new inquiryInfoBeanList();
                                if (item.ResultMessage != null)
                                {
                                    models.ResultMessage = item.ResultMessage;
                                    LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                                    return rs;
                                }
                                else
                                {
                                    name = item.Title + " " + item.FirstName + " " + item.LastName;
                                    models.custId = item.custId;
                                    models.ca = item.ca;
                                    models.ui = (item.ui != null) ? item.ui : "";
                                    models.FirstName = name;
                                    models.coAddress = item.coAddress;
                                    models.custType = (item.custType != null) ? item.custType : "";
                                    models.payeeEleName = (item.payeeEleName != null) ? item.payeeEleName : "";
                                    models.payeeEleAddress = (item.payeeEleAddress != null) ? item.payeeEleAddress : "";
                                    models.payeeFlag = item.PayeeFlag;
                                    models.collId = (item.collId != null) ? item.collId : "";
                                    models.Tax20 = (item.Tax20 != null) ? item.Tax20 : "";
                                    models.TaxBranch = (item.TaxBranch != null) ? item.TaxBranch : "";
                                    models.Status = true;
                                    models.SelectCheck = true;
                                    models.billVatCode = item.billVatCode;
                                    rs.Output.Add(models);
                                }
                                LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                                rs.ResultMessage = clsData.ResultMessage.ToString();
                                rs.ResultCode = clsData.ResultCode.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
            }
            return rs;
        }
        public ResultInquiryDebt SelectInquiryDIS(ClassUser cls, string barcode)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultInquiryDebt rs = new ResultInquiryDebt();
            try
            {
                string strJSON = null;
                string result = JsonConvert.SerializeObject(cls);
                strJSON = ClassReadAPI.PostDataAPI("processInquiry", result, GlobalClass.Timer);
                ResultInquiryDebt clsData = ClassReadAPI.ReadToObjectInquiry(strJSON);
                rs.OutputDIS = new List<ClassReturnDIS>();
                if (clsData.OutputInquiryDebt != null && clsData.ResultMessage == null)
                {
                    foreach (var item in clsData.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
                    {
                        if (item.inquiryGroupDebtBeanList.Count > 0)
                        {
                            string name = "";
                            name = item.Title + " " + item.FirstName + " " + item.LastName;
                            foreach (var itemDetail in item.inquiryGroupDebtBeanList)
                            {
                                ClassReturnDIS dis = new ClassReturnDIS();
                                dis.cusName = name;
                                dis.cusId = item.custId;
                                dis.ca = item.ca;
                                dis.ui = item.ui;
                                dis.debtId = itemDetail.debtIdSet[0].ToString();
                                rs.OutputDIS.Add(dis);
                            }
                        }
                    }
                }
                else
                {
                    if (clsData.ResultMessage != "SUCCESS" && clsData.OutputInquiryDebt.inquiryBean != null)
                    {
                        LogEventInquiry.WriteDataEvent("Fail ==> " + barcode + " || Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                        rs.ResultMessage = clsData.ResultMessage.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultInquiryDebt AddDatainquiry(ResultInquiryDebt result, int status)
        {
            ResultInquiryDebt rs = new ResultInquiryDebt();
            rs.Output = new List<inquiryInfoBeanList>();
            List<inquiryInfoBeanList> _lstInfo = new List<inquiryInfoBeanList>();
            if (GlobalClass.LstCost == null)
                GlobalClass.LstCost = new List<inquiryGroupDebtBeanList>();
            foreach (var item in result.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
            {              
                string type = "";
                string name = "";
                decimal amountEle = 0;
                decimal TotalAmount = 0;
                decimal TotalVat = 0;
                decimal TotalDefaultpenalty = 0;
                inquiryInfoBeanList models = new inquiryInfoBeanList();
                name = item.Title + " " + item.FirstName + " " + item.LastName;
                models.custId = item.custId;
                string ca = "";
                if (item.collInvNo != null && item.collInvNo != "")
                    ca = item.collInvNo;
                else
                    ca = item.ca;
                models.ca = ca;
                models.ui = (item.ui != null) ? item.ui : "";
                models.FirstName = name;
                models.coAddress = item.coAddress;
                models.custType = (item.custType != null) ? item.custType : "";
                models.payeeEleName = (item.payeeEleName != null) ? item.payeeEleName : "";
                models.payeeEleAddress = (item.payeeEleAddress != null) ? item.payeeEleAddress : "";
                models.SelectCheck = true;
                models.payeeFlag = item.payeeFlag;
                models.collId = (item.collId != null) ? item.collId : "";
                models.Tax20 = (item.Tax20 != null) ? item.Tax20 : "";
                models.TaxBranch = (item.TaxBranch != null) ? item.TaxBranch : "";
                models.Status = true;
                models.StatusLockDirectDebit = false;
                models.collInvNo = item.collInvNo;
                if (item.lockCheque != null && item.lockCheque != "")
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                    DateTime dt = Convert.ToDateTime(item.lockCheque);
                    if (dt < DateTime.Now)
                        models.StatuslockCheque = false;
                    else
                        models.StatuslockCheque = true;
                    models.lockCheque = (item.lockCheque != null) ? item.lockCheque : "";
                }
                models.billVatCode = item.billVatCode;
                List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
                if (item.inquiryGroupDebtBeanList != null && item.inquiryGroupDebtBeanList.Count != 0)
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                    DateTime dateNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    if (status == 0)
                    {
                        var selectData = item.inquiryGroupDebtBeanList.Where(c => c.debtType == "DIS").FirstOrDefault();
                        if (selectData != null)
                        {
                            DateTime dt = Convert.ToDateTime(selectData.dueDate);
                            if (dt == dateNow)
                            {
                                if (GlobalClass.CancelProcessing == null)
                                    GlobalClass.CancelProcessing = new List<ClassCancelProcessingFee>();
                                ClassCancelProcessingFee Cancel = new ClassCancelProcessingFee();
                                Cancel.userId = GlobalClass.UserIdCancel;
                                Cancel.debtId = selectData.debtIdSet[0].ToString();
                                GlobalClass.CancelProcessing.Add(Cancel);
                            }
                            else
                            {
                                if (selectData.debtType == "DIS")
                                {
                                    FormProcessingFeeElectricity process = new FormProcessingFeeElectricity(selectData.debtIdSet, selectData.ca, selectData.dueDate);
                                    process.ShowDialog();
                                    process.Dispose();
                                }
                            }
                        }
                    }
                    int debtId = 0;
                    foreach (var itemDetail in item.inquiryGroupDebtBeanList.OrderBy(c => c.dueDate))
                    {
                        bool _status = true;
                        inquiryGroupDebtBeanList modelsDetail = new inquiryGroupDebtBeanList();
                        string debtLock = itemDetail.debtLock != null ? itemDetail.debtLock : "";
                        DateTime dt = Convert.ToDateTime(itemDetail.dueDate);
                        modelsDetail.status = true;
                        modelsDetail.SelectCheck = true;
                        if (itemDetail.debtType == "DIS" && dt == dateNow || debtLock == "Y")
                        {
                            modelsDetail.status = false;
                            modelsDetail.SelectCheck = false;
                            _status = false;
                        }                       
                        if (itemDetail.installmentsFlag == "Y")
                        {
                            if(dateNow <= dt)
                            {
                                modelsDetail.status = false;
                                modelsDetail.SelectCheck = false;
                                _status = false;
                            }                       
                        }
                        
                        modelsDetail.debtIdSet = itemDetail.debtIdSet;
                        modelsDetail.debtId = debtId.ToString();//itemDetail.debtIdSet[0].ToString();
                        modelsDetail.debtType = itemDetail.debtType;
                        modelsDetail.distId = itemDetail.distId;
                        modelsDetail.reqNo = itemDetail.reqNo;
                        modelsDetail.invNo = itemDetail.invNo;
                        modelsDetail.custId = itemDetail.custId;
                        modelsDetail.ca = itemDetail.ca;
                        modelsDetail.ui = itemDetail.ui;
                        modelsDetail.debtDate = itemDetail.debtDate;
                        modelsDetail.previousDueDate = itemDetail.previousDueDate;
                        modelsDetail.dueDate = itemDetail.dueDate;
                        modelsDetail.percentVat = itemDetail.percentVat;
                        modelsDetail.amount = itemDetail.amount;
                        modelsDetail.vat = itemDetail.vat;
                        modelsDetail.totalAmount = itemDetail.totalAmount;
                        modelsDetail.debtBalance = itemDetail.debtBalance;
                        modelsDetail.vatBalance = itemDetail.vatBalance;
                        modelsDetail.debtDesc = itemDetail.debtDesc;
                        modelsDetail.reqDesc = itemDetail.reqDesc;
                        modelsDetail.kwh = itemDetail.kwh;
                        modelsDetail.ftRate = itemDetail.ftRate;
                        modelsDetail.schdate = itemDetail.schdate;
                        modelsDetail.tarifId = itemDetail.tarifId;
                        modelsDetail.custType = itemDetail.custType;
                        modelsDetail.debtInsIntFlag = itemDetail.debtInsIntFlag;
                        modelsDetail.intLockFlag = itemDetail.intLockFlag;
                        modelsDetail.intLockDate = itemDetail.intLockDate;
                        modelsDetail.invNewDueDate = itemDetail.invNewDueDate;
                        modelsDetail.installmentsFlag = itemDetail.installmentsFlag;
                        modelsDetail.postponeFlag = itemDetail.postponeFlag;
                        modelsDetail.createdProcess = itemDetail.createdProcess;
                        modelsDetail.totalIntDay = itemDetail.totalIntDay;//จำนวนเบี้ยปรับ
                        modelsDetail.totalInt = itemDetail.totalInt;//เบี้ยปรับ
                        modelsDetail.directDebitFlag = itemDetail.directDebitFlag;
                        modelsDetail.directDebitDate = itemDetail.directDebitDate;
                        if (_status && itemDetail.directDebitFlag == "Y" && itemDetail.debtLock == "Y" || itemDetail.debtLock == null)
                            modelsDetail.StatusLockDirectDebit = true;
                        else
                            modelsDetail.StatusLockDirectDebit = false;
                        modelsDetail.totalPayment = itemDetail.totalPayment;
                        modelsDetail.StatusCheq = false;
                        modelsDetail.AmountBalance = "0";
                        modelsDetail.cusName = name;
                        modelsDetail.StatusDefaultpenalty = false;
                        modelsDetail.statusCancel = true;
                        modelsDetail.calTotalIntDay = itemDetail.calTotalIntDay;//จำนวนเบี้ยปรับ
                        modelsDetail.calTotalInt = itemDetail.calTotalInt;//เบี้ยปรับ
                        modelsDetail.insDebtInsHdrId = itemDetail.insDebtInsHdrId;
                        modelsDetail.payeeFlag = item.payeeFlag;
                        modelsDetail.infoCa = itemDetail.infoCa;
                        modelsDetail.EleTotalAmountOld = itemDetail.debtBalance;
                        modelsDetail.EleTotalVatOld = Convert.ToDecimal(itemDetail.vatBalance);
                        modelsDetail.TotalAmountOld = Convert.ToDecimal(itemDetail.debtBalance);
                        #region                   
                        string amountFine = "0";
                        if (itemDetail.debtType == "CHQ" && itemDetail.debtInsHdrId is null || Convert.ToDouble(itemDetail.totalAmount) > 10000 && itemDetail.debtType == "ELE" || itemDetail.debtType == "ELO" && itemDetail.debtInsHdrId is null)
                        {
                            DateTime dueDate;
                            DateTime dateNowNew;
                            double countFine = 0;
                            if (!(itemDetail.intLockDate is null))
                                dueDate = dueDate = DateTime.Parse(itemDetail.intLockDate);
                            else if (!(itemDetail.paymentDate is null))
                                dueDate = dueDate = DateTime.Parse(itemDetail.paymentDate);
                            else if (!(itemDetail.invNewDueDate is null))
                                dueDate = DateTime.Parse(itemDetail.invNewDueDate);
                            else
                                dueDate = DateTime.Parse(itemDetail.dueDate);
                            dateNowNew = DateTime.Now;
                            string morRate = "0";
                            var dtCountDay = dateNowNew - dueDate;
                            if (itemDetail.debtType == "ELE")
                            {
                                if (item.custType != "GV" && dateNow > dueDate)
                                {
                                    if (itemDetail.interest != null)
                                        morRate = itemDetail.interest.ToString();
                                    countFine += Convert.ToDouble(itemDetail.amount) * Math.Abs(dtCountDay.Days) * (Convert.ToDouble(morRate) / 100) / 365;
                                    modelsDetail.countDay = Math.Abs(dtCountDay.Days);
                                }
                            }
                            else
                            {
                                if (item.custType != "GV" && DateTime.Now > Convert.ToDateTime(itemDetail.dueDate))
                                    countFine += Convert.ToDouble(itemDetail.amount) * Math.Abs(dtCountDay.Days) * (Convert.ToDouble(morRate) / 100) / 365;
                            }
                            amountFine = countFine.ToString("#,###,###,##0.00");
                            modelsDetail.Defaultpenalty = Convert.ToDecimal(amountFine);
                            modelsDetail.countDayNew = Math.Abs(dtCountDay.Days);
                            modelsDetail.DefaultpenaltyNew = Convert.ToDecimal(amountFine);
                        }
                        else
                        {
                            amountFine = "0";
                            modelsDetail.countDay = 0;
                            modelsDetail.Defaultpenalty = 0;
                        }
                        #endregion                        
                        modelsDetail.Defaultpenalty = Convert.ToDecimal(itemDetail.calTotalInt);
                        modelsDetail.countDay = (itemDetail.calTotalIntDay != null) ? itemDetail.calTotalIntDay.Value : 0;
                        modelsDetail.countDayNew = (itemDetail.calTotalIntDay != null) ? itemDetail.calTotalIntDay.Value : 0;
                        modelsDetail.DefaultpenaltyNew = Convert.ToDecimal(itemDetail.calTotalInt);
                        modelsDetail.debtLock = itemDetail.debtLock;
                        modelsDetail.debtRemark = itemDetail.debtRemark;
                        type = itemDetail.debtType;
                        _lstDebt.Add(modelsDetail);
                        if (debtLock != "Y" && _status)
                        {
                            TotalDefaultpenalty += Convert.ToDecimal(itemDetail.calTotalInt);
                            amountEle += Convert.ToDecimal(itemDetail.debtBalance);
                            TotalAmount += Convert.ToDecimal(itemDetail.debtBalance);
                            TotalVat += Convert.ToDecimal(itemDetail.vatBalance);
                        }
                        GlobalClass.LstCost.Add(modelsDetail);
                        debtId++;
                    }
                }
                models.EleTotalAmount = amountEle;
                models.EleTotalVat = TotalVat;
                models.TotalAmount = TotalAmount;
                models.Defaultpenalty = TotalDefaultpenalty.ToString();
                models.payeeEleTax20 = type == "SER" ? item.payeeSerTax20 : item.payeeEleTax20;
                models.payeeEleTaxBranch = type == "SER" ? item.payeeSerTaxBranch : item.payeeEleTaxBranch;
                models.inquiryGroupDebtBeanList = _lstDebt;
                _lstInfo.Add(models);
                rs.Output.Add(models);
            }
            GlobalClass.LstInfo = _lstInfo;
            return rs;
        }
        public ResultInquiryDebt AddDatainquiryDebt(ResultInquiryDebt result, ResultInquiryDebt _r, int status)
        {
            ResultInquiryDebt rs = new ResultInquiryDebt();
            rs.Output = new List<inquiryInfoBeanList>();
            List<inquiryInfoBeanList> _lstInfo = new List<inquiryInfoBeanList>();
            GlobalClass.LstCost = new List<inquiryGroupDebtBeanList>();
            int debtId = 0;
            if (_r.Output != null)
            {
                foreach (var item in _r.Output)
                {
                    List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
                    inquiryInfoBeanList models = new inquiryInfoBeanList();
                    models.custId = item.custId;
                    models.ca = item.ca;
                    models.ui = item.ui;
                    models.FirstName = item.FirstName;
                    models.coAddress = item.coAddress;
                    models.custType = (item.custType != null) ? item.custType : "";
                    models.payeeEleName = (item.payeeEleName != null) ? item.payeeEleName : "";
                    models.payeeEleTax20 = (item.payeeEleTax20 != null) ? item.payeeEleTax20 : "";
                    models.payeeEleAddress = (item.payeeEleAddress != null) ? item.payeeEleAddress : "";
                    models.payeeEleTaxBranch = (item.payeeEleTaxBranch != null) ? item.payeeEleTaxBranch : "";
                    models.SelectCheck = item.Status;
                    models.EleInvCount = (item.EleInvCount != null) ? item.EleInvCount : "";
                    models.EleTotalAmount = (item.EleTotalAmount != 0) ? item.EleTotalAmount : 0;
                    models.OtherTotalAmount = (item.OtherTotalAmount != null) ? item.OtherTotalAmount : "";
                    models.EleTotalVat = (Convert.ToDecimal(item.EleTotalVat) != 0) ? Convert.ToDecimal(item.EleTotalVat) : 0;
                    models.OtherTotalVat = (item.OtherTotalVat != null) ? item.OtherTotalVat : "";
                    models.lockBill = (item.lockBill != null) ? item.lockBill : null;
                    models.debtType = Convert.ToBoolean((item.debtType == true) ? true : false);
                    models.Defaultpenalty = (item.Defaultpenalty != null) ? item.Defaultpenalty : "0";
                    models.TotalAmount = (item.TotalAmount != 0) ? Convert.ToDecimal(item.TotalAmount) : 0;
                    models.collId = (item.collId != null) ? item.collId : "";
                    models.Tax20 = (item.Tax20 != null) ? item.Tax20 : "";
                    models.TaxBranch = (item.TaxBranch != null) ? item.TaxBranch : "";
                    models.Status = item.Status;
                    models.lockCheque = (item.lockCheque != null) ? item.lockCheque : "";
                    models.StatuslockCheque = item.StatuslockCheque;
                    models.StatusLockDirectDebit = item.StatusLockDirectDebit;
                    if (item.inquiryGroupDebtBeanList != null)
                    {
                        foreach (var itemDetail in item.inquiryGroupDebtBeanList)
                        {
                            inquiryGroupDebtBeanList modelsDetail = new inquiryGroupDebtBeanList();
                            modelsDetail.debtIdSet = itemDetail.debtIdSet;
                            modelsDetail.debtId = itemDetail.debtId;
                            modelsDetail.debtType = itemDetail.debtType;
                            modelsDetail.distId = itemDetail.distId;
                            modelsDetail.reqNo = itemDetail.reqNo;
                            modelsDetail.invNo = itemDetail.invNo;
                            modelsDetail.custId = itemDetail.custId;
                            modelsDetail.ca = itemDetail.ca;
                            modelsDetail.ui = itemDetail.ui;
                            modelsDetail.debtDate = itemDetail.debtDate;
                            modelsDetail.previousDueDate = itemDetail.previousDueDate;
                            modelsDetail.dueDate = itemDetail.dueDate;
                            modelsDetail.percentVat = itemDetail.percentVat;
                            modelsDetail.amount = itemDetail.amount;
                            modelsDetail.vat = itemDetail.vat;
                            modelsDetail.totalAmount = itemDetail.totalAmount;
                            modelsDetail.debtBalance = itemDetail.debtBalance;
                            modelsDetail.vatBalance = itemDetail.vatBalance;
                            modelsDetail.debtDesc = itemDetail.debtDesc;
                            modelsDetail.reqDesc = itemDetail.reqDesc;
                            modelsDetail.kwh = itemDetail.kwh;
                            modelsDetail.ftRate = itemDetail.ftRate;
                            modelsDetail.schdate = itemDetail.schdate;
                            modelsDetail.tarifId = itemDetail.tarifId;
                            modelsDetail.custType = itemDetail.custType;
                            modelsDetail.debtInsIntFlag = itemDetail.debtInsIntFlag;
                            modelsDetail.intLockFlag = itemDetail.intLockFlag;
                            modelsDetail.intLockDate = itemDetail.intLockDate;
                            modelsDetail.invNewDueDate = itemDetail.invNewDueDate;
                            modelsDetail.installmentsFlag = itemDetail.installmentsFlag;
                            modelsDetail.postponeFlag = itemDetail.postponeFlag;
                            modelsDetail.createdProcess = itemDetail.createdProcess;
                            modelsDetail.totalIntDay = itemDetail.totalIntDay;//จำนวนเบี้ยปรับ
                            modelsDetail.totalInt = itemDetail.totalInt;//เบี้ยปรับ
                            modelsDetail.directDebitFlag = itemDetail.directDebitFlag;
                            modelsDetail.directDebitDate = itemDetail.directDebitDate;
                            modelsDetail.StatusLockDirectDebit = itemDetail.StatusLockDirectDebit;
                            modelsDetail.totalPayment = itemDetail.totalPayment;
                            modelsDetail.status = itemDetail.status;
                            modelsDetail.StatusCheq = itemDetail.StatusCheq;
                            modelsDetail.AmountBalance = itemDetail.AmountBalance;
                            modelsDetail.cusName = item.FirstName;
                            modelsDetail.StatusDefaultpenalty = itemDetail.StatusDefaultpenalty;
                            modelsDetail.statusCancel = itemDetail.statusCancel;
                            modelsDetail.calTotalIntDay = itemDetail.calTotalIntDay;//จำนวนเบี้ยปรับ
                            modelsDetail.calTotalInt = itemDetail.calTotalInt;//เบี้ยปรับ
                            modelsDetail.insDebtInsHdrId = itemDetail.insDebtInsHdrId;
                            modelsDetail.countDay = (itemDetail.countDay != null) ? itemDetail.countDay : 0;
                            modelsDetail.Defaultpenalty = (itemDetail.Defaultpenalty != 0) ? itemDetail.Defaultpenalty : 0;
                            modelsDetail.infoCa = itemDetail.infoCa;
                            modelsDetail.payeeFlag = itemDetail.payeeFlag;
                            modelsDetail.EleTotalAmountOld = itemDetail.EleTotalAmountOld;
                            modelsDetail.EleTotalVatOld = itemDetail.EleTotalVatOld;
                            modelsDetail.TotalAmountOld = itemDetail.TotalAmountOld;
                            modelsDetail.debtLock = itemDetail.debtLock;
                            modelsDetail.debtRemark = itemDetail.debtRemark;
                            _lstDebt.Add(modelsDetail);
                            debtId = Convert.ToInt32(itemDetail.debtId + 1);
                            GlobalClass.LstCost.Add(modelsDetail);
                        }
                        models.inquiryGroupDebtBeanList = _lstDebt;
                    }
                    _lstInfo.Add(models);
                    rs.Output.Add(models);
                }
            }
            if (result.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList != null)
            {
                foreach (var item in result.OutputInquiryDebt.inquiryBean.inquiryInfoBeanList)
                {
                    List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
                    string type = "";
                    string name = "";
                    decimal amountEle = 0;
                    decimal TotalAmount = 0;
                    decimal TotalVat = 0;
                    decimal TotalDefaultpenalty = 0;
                    inquiryInfoBeanList models = new inquiryInfoBeanList();
                    name = item.Title + " " + item.FirstName + " " + item.LastName;
                    models.custId = item.custId;
                    string ca = "";
                    if (item.collInvNo != null && item.collInvNo != "")
                        ca = item.collInvNo;
                    else
                        ca = item.ca;
                    models.ca = ca;
                    models.ui = (item.ui != null) ? item.ui : "";
                    models.FirstName = name;
                    models.coAddress = item.coAddress;
                    models.custType = (item.custType != null) ? item.custType : "";
                    models.payeeEleName = (item.payeeEleName != null) ? item.payeeEleName : "";
                    models.payeeEleAddress = (item.payeeEleAddress != null) ? item.payeeEleAddress : "";
                    models.SelectCheck = true;
                    models.payeeFlag = item.PayeeFlag;
                    models.collId = (item.collId != null) ? item.collId : "";
                    models.Tax20 = (item.Tax20 != null) ? item.Tax20 : "";
                    models.TaxBranch = (item.TaxBranch != null) ? item.TaxBranch : "";
                    models.Status = true;
                    models.StatusLockDirectDebit = false;
                    models.collInvNo = item.collInvNo;
                    if (item.lockCheque != null && item.lockCheque != "")
                    {
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                        DateTime dt = Convert.ToDateTime(item.lockCheque);
                        if (dt < DateTime.Now)
                            models.StatuslockCheque = false;
                        else
                            models.StatuslockCheque = true;
                        models.lockCheque = (item.lockCheque != null) ? item.lockCheque : "";
                    }
                    if (item.inquiryGroupDebtBeanList != null || item.inquiryGroupDebtBeanList.Count != 0)
                    {
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                        DateTime dateNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                        if (status == 0)
                        {
                            var selectData = item.inquiryGroupDebtBeanList.Where(c => c.debtType == "DIS").FirstOrDefault();
                            if (selectData != null)
                            {
                                DateTime dt = Convert.ToDateTime(selectData.dueDate);
                                if (dt == dateNow)
                                {
                                    if (GlobalClass.CancelProcessing == null)
                                        GlobalClass.CancelProcessing = new List<ClassCancelProcessingFee>();
                                    ClassCancelProcessingFee Cancel = new ClassCancelProcessingFee();
                                    Cancel.userId = GlobalClass.UserIdCancel;
                                    Cancel.debtId = selectData.debtIdSet[0].ToString();
                                    GlobalClass.CancelProcessing.Add(Cancel);
                                }
                                else
                                {
                                    if (selectData.debtType == "DIS")
                                    {
                                        FormProcessingFeeElectricity process = new FormProcessingFeeElectricity(selectData.debtIdSet, selectData.ca, selectData.dueDate);
                                        process.ShowDialog();
                                        process.Dispose();
                                    }
                                }
                            }
                        }
                        foreach (var itemDetail in item.inquiryGroupDebtBeanList)
                        {
                            bool _status = true;
                            inquiryGroupDebtBeanList modelsDetail = new inquiryGroupDebtBeanList();
                            string debtLock = itemDetail.debtLock != null ? itemDetail.debtLock : "";
                            DateTime dt = Convert.ToDateTime(itemDetail.dueDate);
                            modelsDetail.status = true;
                            modelsDetail.SelectCheck = true;
                            if (itemDetail.debtType == "DIS" && dt == dateNow || debtLock == "Y")
                            {
                                modelsDetail.status = false;
                                modelsDetail.SelectCheck = false;
                                _status = false;
                            }
                            if (itemDetail.installmentsFlag == "Y")
                            {
                                if (dateNow <= dt)
                                {
                                    modelsDetail.status = false;
                                    modelsDetail.SelectCheck = false;
                                    _status = false;
                                }
                            }
                            modelsDetail.debtIdSet = itemDetail.debtIdSet;
                            modelsDetail.debtType = itemDetail.debtType;
                            modelsDetail.debtId = debtId.ToString();//itemDetail.debtIdSet[0].ToString();
                            modelsDetail.distId = itemDetail.distId;
                            modelsDetail.reqNo = itemDetail.reqNo;
                            modelsDetail.invNo = itemDetail.invNo;
                            modelsDetail.custId = itemDetail.custId;
                            modelsDetail.ca = itemDetail.ca;
                            modelsDetail.ui = itemDetail.ui;
                            modelsDetail.debtDate = itemDetail.debtDate;
                            modelsDetail.dueDate = itemDetail.dueDate;
                            modelsDetail.previousDueDate = itemDetail.previousDueDate;
                            modelsDetail.percentVat = itemDetail.percentVat;
                            modelsDetail.amount = itemDetail.amount;
                            modelsDetail.vat = itemDetail.vat;
                            modelsDetail.totalAmount = itemDetail.totalAmount;
                            modelsDetail.debtBalance = itemDetail.debtBalance;
                            modelsDetail.vatBalance = itemDetail.vatBalance;
                            modelsDetail.debtDesc = itemDetail.debtDesc;
                            modelsDetail.reqDesc = itemDetail.reqDesc;
                            modelsDetail.kwh = itemDetail.kwh;
                            modelsDetail.ftRate = itemDetail.ftRate;
                            modelsDetail.schdate = itemDetail.schdate;
                            modelsDetail.tarifId = itemDetail.tarifId;
                            modelsDetail.custType = itemDetail.custType;
                            modelsDetail.debtInsIntFlag = itemDetail.debtInsIntFlag;
                            modelsDetail.intLockFlag = itemDetail.intLockFlag;
                            modelsDetail.intLockDate = itemDetail.intLockDate;
                            modelsDetail.invNewDueDate = itemDetail.invNewDueDate;
                            modelsDetail.installmentsFlag = itemDetail.installmentsFlag;
                            modelsDetail.postponeFlag = itemDetail.postponeFlag;
                            modelsDetail.createdProcess = itemDetail.createdProcess;
                            modelsDetail.totalIntDay = itemDetail.totalIntDay;//จำนวนเบี้ยปรับ
                            modelsDetail.totalInt = itemDetail.totalInt;//เบี้ยปรับ
                            modelsDetail.directDebitFlag = itemDetail.directDebitFlag;
                            modelsDetail.directDebitDate = itemDetail.directDebitDate;
                            if (_status && itemDetail.directDebitFlag == "Y" && itemDetail.debtLock == "Y" || itemDetail.debtLock == null)
                                modelsDetail.StatusLockDirectDebit = true;
                            else
                                modelsDetail.StatusLockDirectDebit = false;
                            modelsDetail.totalPayment = itemDetail.totalPayment;
                            modelsDetail.StatusCheq = false;
                            modelsDetail.AmountBalance = "0";
                            modelsDetail.cusName = name;
                            modelsDetail.StatusDefaultpenalty = false;
                            modelsDetail.statusCancel = true;
                            modelsDetail.calTotalIntDay = itemDetail.calTotalIntDay;//จำนวนเบี้ยปรับ
                            modelsDetail.calTotalInt = itemDetail.calTotalInt;//เบี้ยปรับ
                            modelsDetail.insDebtInsHdrId = itemDetail.insDebtInsHdrId;
                            modelsDetail.payeeFlag = item.payeeFlag;
                            modelsDetail.infoCa = itemDetail.infoCa;
                            modelsDetail.EleTotalAmountOld = itemDetail.debtBalance;
                            modelsDetail.EleTotalVatOld = Convert.ToDecimal(itemDetail.vatBalance);
                            modelsDetail.TotalAmountOld = Convert.ToDecimal(itemDetail.debtBalance);
                            #region                   
                            string amountFine = "0";
                            if (Convert.ToDouble(itemDetail.totalAmount) > 10000 && itemDetail.debtType == "ELE" || itemDetail.debtType == "CHQ" || itemDetail.debtType == "ELO" && itemDetail.debtInsHdrId is null)
                            {
                                DateTime dueDate;
                                DateTime dateNowNew;
                                double countFine = 0;
                                if (!(itemDetail.intLockDate is null))
                                    dueDate = Convert.ToDateTime(itemDetail.intLockDate);
                                else if (!(itemDetail.paymentDate is null))
                                    dueDate = Convert.ToDateTime(itemDetail.paymentDate);
                                else if (!(itemDetail.invNewDueDate is null))
                                    dueDate = Convert.ToDateTime(itemDetail.invNewDueDate);
                                else
                                    dueDate = Convert.ToDateTime(itemDetail.dueDate);
                                dateNowNew = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy", usCulture));
                                string morRate = "0";
                                var dtCountDay = dateNowNew - dueDate;
                                if (itemDetail.debtType == "ELE")
                                {
                                    if (item.custType != "GV" && dateNowNew > dueDate)
                                    {
                                        if (itemDetail.interest != null)
                                            morRate = itemDetail.interest.ToString();
                                        countFine += (Convert.ToDouble(itemDetail.debtBalance) - Convert.ToDouble(itemDetail.vatBalance)) * Math.Abs(dtCountDay.Days) * (Convert.ToDouble(morRate) / 100) / 365;
                                        modelsDetail.countDay = Math.Abs(dtCountDay.Days);
                                    }
                                }
                                else
                                {
                                    if (item.custType != "GV" && DateTime.Now > Convert.ToDateTime(itemDetail.dueDate))
                                        countFine += (Convert.ToDouble(itemDetail.debtBalance) - Convert.ToDouble(itemDetail.vatBalance)) * Math.Abs(dtCountDay.Days) * (Convert.ToDouble(morRate) / 100) / 365;
                                }
                                amountFine = countFine.ToString("#,###,###,##0.00");
                                modelsDetail.Defaultpenalty = Convert.ToDecimal(amountFine);
                                modelsDetail.countDayNew = Math.Abs(dtCountDay.Days);
                                modelsDetail.DefaultpenaltyNew = Convert.ToDecimal(amountFine);
                            }
                            else
                            {
                                amountFine = "0";
                                modelsDetail.countDay = 0;
                                modelsDetail.Defaultpenalty = 0;
                            }
                            #endregion
                            modelsDetail.Defaultpenalty = Convert.ToDecimal(itemDetail.calTotalInt);
                            modelsDetail.countDay = (itemDetail.calTotalIntDay != null) ? itemDetail.calTotalIntDay.Value : 0;
                            modelsDetail.countDayNew = (itemDetail.calTotalIntDay != null) ? itemDetail.calTotalIntDay.Value : 0;
                            modelsDetail.DefaultpenaltyNew = Convert.ToDecimal(itemDetail.calTotalInt);
                            modelsDetail.debtLock = itemDetail.debtLock;
                            modelsDetail.debtRemark = itemDetail.debtRemark;
                            type = itemDetail.debtType;
                            _lstDebt.Add(modelsDetail);
                            if (debtLock != "Y"&& _status)
                            {
                                TotalDefaultpenalty += Convert.ToDecimal(itemDetail.calTotalInt);
                                amountEle += Convert.ToDecimal(itemDetail.debtBalance);
                                TotalAmount += Convert.ToDecimal(itemDetail.debtBalance);
                                TotalVat += Convert.ToDecimal(itemDetail.vatBalance);
                            }
                            GlobalClass.LstCost.Add(modelsDetail);
                            debtId++;
                        }
                    }
                    models.EleTotalAmount = amountEle;
                    models.EleTotalVat = TotalVat;
                    models.TotalAmount = TotalAmount;
                    models.Defaultpenalty = TotalDefaultpenalty.ToString();
                    models.payeeEleTax20 = type == "SER" ? item.payeeSerTax20 : item.payeeEleTax20;
                    models.payeeEleTaxBranch = type == "SER" ? item.payeeSerTaxBranch : item.payeeEleTaxBranch;
                    models.inquiryGroupDebtBeanList = _lstDebt;
                    _lstInfo.Add(models);
                    rs.Output.Add(models);
                }
            }
            GlobalClass.LstInfo = _lstInfo;
            return rs;
        }
        public ResultCostTDebt SelectDataInquiryDebtDetail(ResultInquiryDebt result, string ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultCostTDebt rs = new ResultCostTDebt();
            try
            {
                #region
                List<inquiryGroupDebtBeanList> _list = new List<inquiryGroupDebtBeanList>();
                if (GlobalClass.LstInfo.Count > 0)
                {
                    inquiryInfoBeanList lstInfo = GlobalClass.LstInfo.Where(c => c.ca == ca).FirstOrDefault();
                    if (lstInfo.inquiryGroupDebtBeanList != null && lstInfo.inquiryGroupDebtBeanList.Count != 0)
                    {
                        List<inquiryGroupDebtBeanList> lstData = lstInfo.inquiryGroupDebtBeanList.OrderBy(c => c.schdate).ToList();
                        if (lstData.Count > 0)
                        {
                            rs.Output = new List<inquiryGroupDebtBeanList>();
                            int Invcount = 0;
                            foreach (inquiryGroupDebtBeanList item in lstData)
                            {
                                decimal TotalAmount = 0;
                                decimal OtherTotalAmount = 0;
                                decimal VatTotalAmount = 0;
                                decimal VatOtherTotalAmount = 0;
                                inquiryGroupDebtBeanList models = new inquiryGroupDebtBeanList();
                                if (item.ResultMessage != null)
                                {
                                    models.ResultMessage = item.ResultMessage;
                                    return rs;
                                }
                                else
                                {
                                    models.debtIdSet = item.debtIdSet;
                                    models.distIdNew = item.debtIdSet[0].ToString();
                                    models.SelectCheck = item.debtLock == "Y" ? false : item.status;
                                    models.debtId = item.debtId;
                                    models.schdate = item.schdate;//(item.schdate != null) ? Convert.ToDateTime(item.schdate).ToString("dd/MM/yyyy") : "";
                                    models.totalkWh = item.kwh;
                                    models.invNo = item.invNo;
                                    models.reqNo = item.reqNo;
                                    models.debtDate = (item.debtDate != null) ? Convert.ToDateTime(item.debtDate).ToString("dd/MM/yyyy") : "";
                                    models.previousDueDate = (item.previousDueDate != null) ? Convert.ToDateTime(item.previousDueDate).ToString("dd/MM/yyyy") : "";
                                    models.dueDate = (item.dueDate != null) ? Convert.ToDateTime(item.dueDate).ToString("dd/MM/yyyy") : "";
                                    models.amountNew = Convert.ToDecimal(item.TotalAmountOld);
                                    decimal vat = item.vatBalance != null ? Convert.ToDecimal(item.vatBalance) : 0;
                                    models.vatBalance = vat.ToString("#,###,###,##0.00");
                                    models.debtBalance = item.debtBalance;
                                    if (item.debtType == "MWA")
                                        models.Payment = Math.Round(item.Payment, 2);
                                    else
                                        models.Payment = (item.Payment != 0) ? Math.Round(item.Payment, 2) : Math.Round(Convert.ToDecimal(item.debtBalance), 2);
                                    models.debtDesc = item.debtDesc;
                                    models.custId = item.custId;
                                    models.ca = item.ca;
                                    models.invNewDueDate = (item.invNewDueDate != null) ? Convert.ToDateTime(item.invNewDueDate).ToString("dd/MM/yyyy") : "";
                                    models.lockBill = item.lockBill;
                                    models.ftRate = item.ftRate;
                                    models.receiptDetailDesc = item.receiptDetailDesc;
                                    models.debtType = item.debtType;
                                    models.StatusDefaultpenalty = item.StatusDefaultpenalty;
                                    models.countDay = item.countDay != 0 ? item.countDay : 0;
                                    models.Defaultpenalty = item.Defaultpenalty != 0 ? item.Defaultpenalty : 0;
                                    models.DefaultpenaltyNew = item.DefaultpenaltyNew != 0 ? item.DefaultpenaltyNew : 0;
                                    models.debtLock = item.debtLock;
                                    models.debtRemark = item.debtRemark;
                                    models.directDebitFlag = item.directDebitFlag;
                                    models.directDebitDate = item.directDebitDate;
                                    if (item.debtType == "ELE" || item.debtType == "ADV")
                                    {
                                        Invcount++;
                                        TotalAmount += item.debtBalance;
                                        VatTotalAmount += Convert.ToDecimal(item.vatBalance);
                                    }
                                    else
                                    {
                                        OtherTotalAmount += item.debtBalance;
                                        VatOtherTotalAmount += Convert.ToDecimal(item.vatBalance);
                                    }
                                    models.EleInvCount = Invcount;
                                    models.EleTotalAmount = TotalAmount;
                                    models.OtherTotalAmount = OtherTotalAmount;
                                    models.VatEleTotalAmount = VatTotalAmount;
                                    models.VatOtherTotalAmount = VatOtherTotalAmount;
                                    models.insDebtInsHdrId = item.insDebtInsHdrId != null ? item.insDebtInsHdrId : "";
                                    models.installmentsFlag = item.installmentsFlag;
                                    models.StatusLockDirectDebit = item.StatusLockDirectDebit;
                                    rs.Output.Add(models);
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ControlErrorLog = new ClassLogsService("ErrorLog", "");
                ControlErrorLog.WriteData("Controller :: ArrearsController | Function :: SelectDataCustomerDetail()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultCostTDebt LoadDataContinue()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultCostTDebt rs = new ResultCostTDebt();
            try
            {
                #region
                if (GlobalClass.LstCost.Count > 0)
                {
                    List<inquiryGroupDebtBeanList> clsData = GlobalClass.LstCost.Where(c => c.debtType == "").ToList();
                    if (clsData.Count > 0)
                    {
                        rs.Output = new List<inquiryGroupDebtBeanList>();
                        foreach (inquiryGroupDebtBeanList item in clsData)
                        {
                            inquiryGroupDebtBeanList models = new inquiryGroupDebtBeanList();
                            if (item.ResultMessage != null)
                            {
                                models.ResultMessage = item.ResultMessage;
                                return rs;
                            }
                            else
                            {
                                models.cusName = item.cusName;
                                models.debtIdSet = item.debtIdSet;
                                models.custId = item.custId;
                                models.ca = item.ca;
                                models.ui = item.ui != null ? item.ui : item.ui;
                                rs.Output.Add(models);
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public inquiryGroupDebtBeanList UpdateStatus(string Id, bool status, decimal amount, string ca, string hdrId)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            inquiryGroupDebtBeanList _costDebt = new inquiryGroupDebtBeanList();
            try
            {
                inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == ca).FirstOrDefault();
                inquiryGroupDebtBeanList costTDebt = new inquiryGroupDebtBeanList();
                if (hdrId == "")
                    costTDebt = info.inquiryGroupDebtBeanList.Find(item => item.debtId.ToString().Equals(Id, StringComparison.CurrentCultureIgnoreCase));
                else
                    costTDebt = info.inquiryGroupDebtBeanList.Where(c => c.insDebtInsHdrId == hdrId).FirstOrDefault();

                if (costTDebt != null)
                {
                    costTDebt.status = status;
                    if (amount != 0)
                    {
                        string perVat;
                        if (Convert.ToInt16(costTDebt.percentVat) >= 10)
                            perVat = "1." + costTDebt.percentVat;
                        else
                            perVat = "1.0" + costTDebt.percentVat;
                        decimal vat = amount - (amount / Convert.ToDecimal(perVat));
                        costTDebt.amount = costTDebt.amount;
                        costTDebt.Payment = amount;
                        costTDebt.vatBalance = vat.ToString("#,###,###,##0.00");
                        costTDebt.debtBalance = amount;
                        costTDebt.totalAmount = (amount + vat).ToString("#,###,###,##0.00");
                        _costDebt.percentVat = perVat;
                        _costDebt.vatBalance = vat.ToString("#,###,###,##0.00");
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return _costDebt;
        }
        public bool UpdateStatusDefaultpenalty(bool _status, string _id, string _ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == _ca).FirstOrDefault();
                inquiryGroupDebtBeanList costTDebt = info.inquiryGroupDebtBeanList.Find(item => item.debtId.Equals(_id, StringComparison.CurrentCultureIgnoreCase));
                //if (costTDebt == null)
                //{
                //    string debtIdDetail = costTDebt.debtIdSet[0].ToString();
                //    var Datadebt = costTDebt.debtIdSet.Where(c => c.ToString() == _id).FirstOrDefault();
                //    if (Datadebt.ToString() != "")
                //        costTDebt = info.inquiryGroupDebtBeanList.Find(item => item.debtIdSet[0].ToString().Equals(debtIdDetail, StringComparison.CurrentCultureIgnoreCase));
                //}
                if (costTDebt != null)
                {
                    //inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(item => item.ca.Equals(costTDebt.ca, StringComparison.CurrentCultureIgnoreCase));
                    //if (userInfo != null)
                    //{
                    if (_status)
                    {
                        info.Defaultpenalty = (Convert.ToDecimal(info.Defaultpenalty) - costTDebt.Defaultpenalty).ToString();
                        costTDebt.Defaultpenalty = 0;
                        costTDebt.countDay = 0;
                        _status = true;
                    }
                    else
                    {
                        info.Defaultpenalty = (Convert.ToDecimal(info.Defaultpenalty) + costTDebt.DefaultpenaltyNew).ToString();
                        costTDebt.Defaultpenalty = costTDebt.DefaultpenaltyNew;
                        costTDebt.countDay = costTDebt.countDayNew;
                        _status = false;
                    }
                    //}
                    costTDebt.StatusDefaultpenalty = _status;
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                tf = false;
                throw new System.Exception();
            }
            return tf;
        }
        public bool UpdateStatusUserInfo(string Ca, bool status, decimal amount, decimal vat, string defaultpenalty)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(item => item.ca.Equals(Ca, StringComparison.CurrentCultureIgnoreCase));
                if (userInfo != null)
                {
                    userInfo.Status = status;
                    userInfo.SelectCheck = status;
                    if (amount != 0)
                    {
                        userInfo.EleTotalAmount = amount;
                        userInfo.TotalAmount = amount;
                        userInfo.EleTotalVat = vat;
                        userInfo.Defaultpenalty = defaultpenalty;
                        userInfo.statusPartialpayment = status;
                    }
                    else
                    {
                        if (!status)
                        {
                            userInfo.TotalAmount = 0;
                            userInfo.EleTotalVat = 0;
                            userInfo.Defaultpenalty = "0";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                tf = false;
                throw new System.Exception();
            }
            return tf;
        }
        public bool UpdateUserInfoNew()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                if (GlobalClass.LstInfo.Count > 0)
                {
                    foreach (var item in GlobalClass.LstInfo)
                    {
                        decimal amount = 0;
                        decimal Totalamount = 0;
                        decimal TotalVat = 0;
                        decimal TotalDefaultpenalty = 0;
                        foreach (var itemDetail in item.inquiryGroupDebtBeanList)//.Where(c => c.status == true && c.EleTotalAmountOld > c.amount).ToList()
                        {
                            inquiryGroupDebtBeanList costTDebt = item.inquiryGroupDebtBeanList.Find(itemdetail => itemdetail.debtId.ToString().Equals(itemDetail.debtId, StringComparison.CurrentCultureIgnoreCase));
                            costTDebt.debtBalance = itemDetail.EleTotalAmountOld;
                            costTDebt.Payment = itemDetail.EleTotalAmountOld;
                            costTDebt.vatBalance = itemDetail.EleTotalVatOld.ToString();
                            costTDebt.totalAmount = itemDetail.TotalAmountOld.ToString();
                            costTDebt.Defaultpenalty = itemDetail.DefaultpenaltyNew;
                            costTDebt.amount = itemDetail.EleTotalAmountOld;
                            TotalDefaultpenalty += itemDetail.DefaultpenaltyNew;
                            amount += itemDetail.EleTotalAmountOld;
                            Totalamount += itemDetail.TotalAmountOld;
                            TotalVat += itemDetail.EleTotalVatOld;
                        }
                        inquiryInfoBeanList userInfo = GlobalClass.LstInfo.Find(iteminfo => iteminfo.ca.Equals(item.ca, StringComparison.CurrentCultureIgnoreCase));
                        userInfo.EleTotalAmount = amount;
                        userInfo.EleTotalVat = TotalVat;
                        userInfo.TotalAmount = Totalamount;
                        userInfo.Defaultpenalty = TotalDefaultpenalty.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                tf = false;
                throw new System.Exception();
            }
            return tf;
        }
        public string MyXor(string strConvert)
        {
            string strReturn = "";
            int intStr = 0;
            var loopTo = strConvert.Length - 1;
            for (int x = 0; x <= loopTo; x++)
                intStr = intStr ^ strConvert[x];
            strReturn = intStr.ToString();
            return strReturn;
        }
        public string GetIPAddress()
        {
            string ipAddresses = "";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    ipAddresses = ip.ToString();
            }
            return ipAddresses;
        }
        public string ThaiBaht(decimal number)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            string bahtTxt, n, bahtTH = "";
            try
            {
                decimal amount;
                try { amount = Convert.ToDecimal(number); }
                catch { amount = 0; }
                bahtTxt = amount.ToString("####.00");
                string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
                string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
                string[] temp = bahtTxt.Split('.');
                string intVal = temp[0];
                string decVal = temp[1];
                if (Convert.ToDouble(bahtTxt) == 0)
                    bahtTH = "ศูนย์บาทถ้วน";
                else
                {
                    for (int i = 0; i < intVal.Length; i++)
                    {
                        n = intVal.Substring(i, 1);
                        if (n != "0")
                        {
                            if ((i == (intVal.Length - 1)) && (n == "1"))
                                bahtTH += "เอ็ด";
                            else if ((i == (intVal.Length - 2)) && (n == "2"))
                                bahtTH += "ยี่";
                            else if ((i == (intVal.Length - 2)) && (n == "1"))
                                bahtTH += "";
                            else
                                bahtTH += num[Convert.ToInt32(n)];
                            bahtTH += rank[(intVal.Length - i) - 1];
                        }
                    }
                    bahtTH += "บาท";
                    if (decVal == "00")
                        bahtTH += "ถ้วน";
                    else
                    {
                        for (int i = 0; i < decVal.Length; i++)
                        {
                            n = decVal.Substring(i, 1);
                            if (n != "0")
                            {
                                if ((i == decVal.Length - 1) && (n == "1"))
                                    bahtTH += "เอ็ด";
                                else if ((i == (decVal.Length - 2)) && (n == "2"))
                                    bahtTH += "ยี่";
                                else if ((i == (decVal.Length - 2)) && (n == "1"))
                                    bahtTH += "";
                                else
                                    bahtTH += num[Convert.ToInt32(n)];
                                bahtTH += rank[(decVal.Length - i) - 1];
                            }
                        }
                        bahtTH += "สตางค์";
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return bahtTH;
        }
        public string Fraction(int number)
        {
            string Satang = "0";
            if (number <= 12)
                Satang = "0";
            else if (number <= 37)
                Satang = "0.25";
            else if (number <= 62)
                Satang = "0.50";
            else if (number <= 87)
                Satang = "0.75";
            else
                Satang = "1";
            return Satang;
        }
        public string GetBankDesc(string code)
        {
            string url = "getBankBranch?bankBranch=" + code + "&rnd=";
            string jsonData = ClassReadAPI.GetDataAPI(url);
            dynamic data = JObject.Parse(jsonData);
            return data.bankDesc;
        }
        public ResultExpenseItem GetExpenseItem(int status)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultExpenseItem rs = new ResultExpenseItem();
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                string url = "inquiryProfitCenterAndExpenseItemList" + "?rnd=";
                var jsonData = ClassReadAPI.GetDataAPI(url);
                ClassItemCode expenseItem = new ClassItemCode();
                expenseItem = new JavaScriptSerializer().Deserialize<ClassItemCode>(jsonData);
                rs.Output = new List<listExpenseItem>();
                rs.OutputPro = new List<listProfitCenter>();
                rs.OutputUnit = new List<costUnit>();
                if (status == 0)
                {
                    foreach (var item in expenseItem.listProfitCenter)
                    {
                        if (item != null)
                        {
                            listProfitCenter itemPro = new listProfitCenter();
                            itemPro.profitCenter = item.profitCenter;
                            itemPro.distShortDesc = item.distShortDesc;
                            rs.OutputPro.Add(itemPro);
                        }
                    }
                }
                else if (status == 1)
                {
                    foreach (var item in expenseItem.listExpenseItem)
                    {
                        if (item != null)
                        {
                            listExpenseItem itemEx = new listExpenseItem();
                            itemEx.itemCode = item.itemCode;
                            itemEx.itemAccountCode = item.itemAccountCode;
                            itemEx.itemDescEn = item.itemDescEn;
                            itemEx.itemDescTh = item.itemCode + " " + item.itemDescTh;
                            itemEx.itemId = item.itemId;
                            itemEx.vatCode = item.vatCode;
                            itemEx.percentVat = item.percentVat;
                            rs.Output.Add(itemEx);
                        }
                    }
                }
                else
                {
                    foreach (var item in expenseItem.costUnit)
                    {
                        if (item != null)
                        {
                            costUnit itemIUnit = new costUnit();
                            itemIUnit.unitId = item.unitId;
                            itemIUnit.unitCode = item.unitCode;
                            itemIUnit.unitDetail = item.unitDetail;
                            rs.OutputUnit.Add(itemIUnit);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultDisplayPaytment GetDisplayPaytment(ClassDisplayPaytment cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultDisplayPaytment rs = new ResultDisplayPaytment();
            try
            {
                string strJSON = null;
                string result = JsonConvert.SerializeObject(cls);
                strJSON = ClassReadAPI.PostDataAPI("processDisplayCancelReceipt", result, GlobalClass.Timer);
                ResultDisplayPaytment clsData = ClassReadAPI.ReadToObjectDisplayPaytment(strJSON);
                rs.Output = new List<displayCancelReceiptResponseDtoList>();
                if (clsData.OutputDisplay != null)
                {
                    foreach (displayCancelReceiptResponseDtoList item in clsData.OutputDisplay.displayCancelReceiptResponseDtoList)
                    {
                        displayCancelReceiptResponseDtoList model = new displayCancelReceiptResponseDtoList();
                        model.paymentId = item.paymentId;
                        model.paymentNo = item.paymentNo;
                        model.payerName = item.payerName;
                        model.receiptNo = item.receiptNo;
                        model.custId = item.custId;
                        model.ca = item.ca;
                        model.ui = item.ui;
                        model.totalReceipt = item.totalReceipt;
                        rs.Output.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultRemittance SearchDataRemittance(ClassPayInBalance cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultRemittance rs = new ResultRemittance();
            try
            {
                string result = JsonConvert.SerializeObject(cls);
                var strJSON = ClassReadAPI.PostDataAPI("processDisplayPayIn", result, GlobalClass.Timer);
                ResultRemittance clsData = ClassReadAPI.ReadToObjectProcessPayIn(strJSON);
                rs.Output = new List<payInDisplayResponseDtoList>();
                if (clsData.ResultMessage == "SUCCESS")
                {
                    if (clsData.OutputDisplay != null)
                    {
                        foreach (payInDisplayResponseDtoList item in clsData.OutputDisplay.payInDisplayResponseDtoList)
                        {
                            payInDisplayResponseDtoList models = new payInDisplayResponseDtoList();
                            models.orderNo = item.orderNo;
                            models.closingDrawerId = item.closingDrawerId;
                            models.cashierId = item.cashierId;
                            models.cashierNo = item.cashierNo;
                            models.createdDtStr = item.createdDtStr;
                            models.totalCash = item.totalCash;
                            models.totalCheque = item.totalCheque;
                            models.totalChequeAmount = item.totalChequeAmount;
                            models.totalCreditCard = item.totalCreditCard;
                            models.totalCreditCardAmount = item.totalCreditCardAmount;
                            models.totalSendAmount = item.totalSendAmount;
                            models.totalPaymentAmount = item.totalPaymentAmount;
                            models.diffAmount = item.diffAmount;
                            models.totalSendAmountDay = item.totalSendAmountDay;
                            models.previouseSendAmount = item.previouseSendAmount;
                            models.closingDrawerStatus = item.closingDrawerStatus;
                            models.unitB1000 = item.unitB1000;
                            models.unitB500 = item.unitB500;
                            models.unitB100 = item.unitB100;
                            models.unitB50 = item.unitB50;
                            models.unitB20 = item.unitB20;
                            models.amountB1000 = item.amountB1000;
                            models.amountB500 = item.amountB500;
                            models.amountB100 = item.amountB100;
                            models.amountB50 = item.amountB50;
                            models.amountB20 = item.amountB20;
                            models.totalCoin = item.totalCoin;
                            models.totalBankNote = item.totalBankNote;
                            models.sumTotalCash = item.sumTotalCash;
                            models.sumTotalCheque = item.sumTotalCheque;
                            models.sumTotalCard = item.sumTotalCard;
                            models.sumTotalAmount = item.sumTotalAmount;
                            models.status = true;
                            rs.Output.Add(models);
                        }
                    }
                }
                else
                {
                    LogEventInquiry.WriteDataEvent("Fail ==> Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                    rs.ResultMessage = clsData.ResultMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultDisplayPayIn SearchDataSumRemittance(ClassPayInBalance cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultDisplayPayIn rs = new ResultDisplayPayIn();
            try
            {
                string result = JsonConvert.SerializeObject(cls);
                var strJSON = ClassReadAPI.PostDataAPI("processSummaryPayIn", result, GlobalClass.Timer);
                ClassProcessDisplayPayIn Display = JsonConvert.DeserializeObject<ClassProcessDisplayPayIn>(strJSON);
                rs.Output = new List<ClassProcessDisplayPayIn>();
                if (Display.result_code == "SUCCESS")
                {
                    string[] disName = GlobalClass.Dist.Split('-');
                    string userName = GlobalClass.UserId + " " + GlobalClass.UserName;
                    ClassProcessDisplayPayIn display = new ClassProcessDisplayPayIn();
                    display.totalAmount = Display.totalAmount;
                    display.totalCard = Display.totalCard;
                    display.totalCash = Display.totalCash;
                    display.totalCheque = Display.totalCheque;
                    display.strAmount = "( " + ThaiBaht(Display.totalAmount) + " )";
                    display.strAmountNew = ThaiBaht(Display.totalAmount);
                    display.userName = GlobalClass.UserName;
                    display.userId = userName;
                    display.distName = disName[1].ToString();
                    if (Display.payInChequeResponseDtoList != null && Display.payInChequeResponseDtoList.Count > 0)
                    {
                        int i = 1;
                        display.payInChequeResponseDtoList = new List<payInChequeResponseDtoList>();
                        foreach (var item in Display.payInChequeResponseDtoList)
                        {
                            payInChequeResponseDtoList cheque = new payInChequeResponseDtoList();
                            cheque.No = i.ToString();
                            cheque.chequeId = item.chequeId;
                            cheque.chequeNo = item.chequeNo;
                            cheque.bankDesc = item.bankDesc;
                            cheque.chequeOwner = item.chequeOwner;
                            cheque.telNo = item.telNo;
                            cheque.chequeAmount = item.chequeAmount;
                            cheque.chequeDate = item.chequeDate;
                            display.payInChequeResponseDtoList.Add(cheque);
                            i++;
                        }
                    }
                    if (Display.payInCardResponseDtoList != null && Display.payInCardResponseDtoList.Count > 0)
                    {
                        int i = 1;
                        display.payInCardResponseDtoList = new List<payInCardResponseDtoList>();
                        foreach (var item in Display.payInCardResponseDtoList)
                        {
                            payInCardResponseDtoList card = new payInCardResponseDtoList();
                            card.No = i.ToString();
                            card.cardId = item.cardId;
                            card.cardNo = item.cardNo;
                            card.cardOwner = item.cardOwner;
                            card.cardAmount = item.cardAmount;
                            display.payInCardResponseDtoList.Add(card);
                            i++;
                        }
                    }
                    rs.Output.Add(display);
                }
                else
                {
                    LogEventInquiry.WriteDataEvent("Fail ==> Message :: " + Display.result_code.ToString(), method, LogLevel.Debug);
                    rs.ResultMessage = Display.result_code.ToString();
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultDisplayPayIn SearchDataSumRemittanceOffline(ClassPayInBalance cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultDisplayPayIn rs = new ResultDisplayPayIn();
            try
            {
                ClassReportOffline _lst = OFC.SelectReportOffline(cls);
                List<ItemChequeModel> _lstCheque = OFC.SelectDataChequeOffline(cls);
                rs.Output = new List<ClassProcessDisplayPayIn>();
                if (_lst != null)
                {
                    decimal totalAmount = Convert.ToDecimal(_lst.TotalAmountCash) + Convert.ToDecimal(_lst.TotalAmountCheque);
                    ClassProcessDisplayPayIn display = new ClassProcessDisplayPayIn();
                    display.totalAmount = totalAmount;
                    display.totalCard = 0;
                    display.totalCash = Convert.ToDecimal(_lst.TotalAmountCash);
                    display.totalCheque = Convert.ToDecimal(_lst.TotalAmountCheque);
                    display.strAmount = "( " + ThaiBaht(totalAmount) + " )";
                    display.strAmountNew = ThaiBaht(totalAmount);
                    display.userName = GlobalClass.UserName;
                    display.userId = _lst.UserCode + " " + _lst.UserName;
                    display.distName = _lst.DistName;
                    if (_lstCheque.Count > 0)
                    {
                        int i = 1;
                        display.payInChequeResponseDtoList = new List<payInChequeResponseDtoList>();
                        foreach (var item in _lstCheque)
                        {
                            payInChequeResponseDtoList cheque = new payInChequeResponseDtoList();
                            cheque.No = i.ToString();
                            cheque.chequeId = 0;
                            cheque.chequeNo = item.ChequeNumber;
                            cheque.bankDesc = item.BankName;
                            cheque.chequeOwner = "";
                            cheque.telNo = item.ChequeTel;
                            cheque.chequeAmount = Convert.ToDecimal(item.ChequeAmount);
                            cheque.chequeDate = item.ChequeDate;
                            display.payInChequeResponseDtoList.Add(cheque);
                            i++;
                        }
                    }
                    rs.Output.Add(display);
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultRemittance InsertRemittance(ClassPayIn mylist)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultRemittance rs = new ResultRemittance();
            try
            {
                string result = JsonConvert.SerializeObject(mylist);
                LogRemittance.WriteData("Remittance Data :: " + result, method, LogLevel.Debug);
                var strJSON = ClassReadAPI.PostDataAPI("processPayIn", result, GlobalClass.Timer);
                ResultRemittance clsData = ClassReadAPI.ReadToObjectProcessPayIn(strJSON);
                rs.Output = new List<payInDisplayResponseDtoList>();
                if (clsData.ResultMessage == "SUCCESS")
                {
                    if (clsData.OutputDisplay != null)
                    {
                        foreach (payInDisplayResponseDtoList item in clsData.OutputDisplay.payInDisplayResponseDtoList)
                        {
                            payInDisplayResponseDtoList models = new payInDisplayResponseDtoList();
                            models.orderNo = item.orderNo;
                            models.closingDrawerId = item.closingDrawerId;
                            models.cashierId = item.cashierId;
                            models.cashierNo = item.cashierNo;
                            models.createdDtStr = item.createdDtStr;
                            models.totalCash = item.totalCash;
                            models.totalCheque = item.totalCheque;
                            models.totalChequeAmount = item.totalChequeAmount;
                            models.totalCreditCard = item.totalCreditCard;
                            models.totalCreditCardAmount = item.totalCreditCardAmount;
                            models.totalSendAmount = item.totalSendAmount;
                            models.totalPaymentAmount = item.totalPaymentAmount;
                            models.diffAmount = item.diffAmount;
                            models.totalSendAmountDay = item.totalSendAmountDay;
                            models.previouseSendAmount = item.previouseSendAmount;
                            models.closingDrawerStatus = item.closingDrawerStatus;
                            models.unitB1000 = item.unitB1000;
                            models.unitB500 = item.unitB500;
                            models.unitB100 = item.unitB100;
                            models.unitB50 = item.unitB50;
                            models.unitB20 = item.unitB20;
                            models.amountB1000 = item.amountB1000;
                            models.amountB500 = item.amountB500;
                            models.amountB100 = item.amountB100;
                            models.amountB50 = item.amountB50;
                            models.amountB20 = item.amountB20;
                            models.totalCoin = item.totalCoin;
                            models.totalBankNote = item.totalBankNote;
                            models.sumTotalCash = item.sumTotalCash;
                            models.sumTotalCheque = item.sumTotalCheque;
                            models.sumTotalCard = item.sumTotalCard;
                            models.sumTotalAmount = item.sumTotalAmount;
                            models.status = true;
                            rs.Output.Add(models);
                        }
                    }
                    rs.ResultMessage = clsData.ResultMessage;
                }
                else
                    rs.ResultMessage = clsData.ResultMessage.ToString();
                LogEventRemittance.WriteData("Remittance Message :: " + strJSON, method, LogLevel.Debug);
            }
            catch (Exception ex)
            {
                LogErrorRemittance.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ClassUser loadDataLogin(ClassLogin cls, string url)
        {
            string strJSON = null;
            string result = JsonConvert.SerializeObject(cls);
            strJSON = ClassReadAPI.PostDataAPI(url, result, GlobalClass.Timer);
            ClassUser clsLogin = ClassReadAPI.ReadToObject(strJSON);
            return clsLogin;
        }
        public ClassUser ChangePassword(ClassUser cls, string url)
        {
            string strJSON = null;
            string result = JsonConvert.SerializeObject(cls);
            strJSON = ClassReadAPI.PostDataAPI(url, result, GlobalClass.Timer);
            ClassUser clsLogin = ClassReadAPI.ReadToObject(strJSON);
            return clsLogin;
        }
        public bool UpdateCostTDebt(List<inquiryInfoBeanList> lstInfo, int type, string count)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            bool tf = false;
            try
            {
                foreach (var item in lstInfo)
                {
                    bool status = false;
                    decimal vat = 0;
                    decimal payment = 0;
                    decimal fine = 0;
                    if (type == 1)
                    {
                        int i = 0;
                        inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == item.ca).FirstOrDefault();
                        foreach (var itemDetail in info.inquiryGroupDebtBeanList.OrderBy(c => c.dueDate).ToList())
                        {
                            if (i < Convert.ToInt32(count))
                            {
                                itemDetail.status = true;
                                fine += itemDetail.Defaultpenalty;
                                if (itemDetail.vatBalance != "")
                                    vat += Convert.ToDecimal(itemDetail.vatBalance);
                                payment += Convert.ToDecimal(itemDetail.debtBalance);
                                status = true;
                            }
                            else
                                itemDetail.status = false;
                            i++;
                        }
                        UpdateStatusUserInfo(info.ca, status, payment, vat, fine.ToString());
                    }
                    else
                    {
                        inquiryInfoBeanList info = GlobalClass.LstInfo.Where(c => c.ca == item.ca).FirstOrDefault();
                        foreach (var itemDetail in info.inquiryGroupDebtBeanList.Where(c => c.debtType == "GUA").ToList())
                        {
                            itemDetail.status = false;
                            payment += itemDetail.Payment;
                            fine += itemDetail.Defaultpenalty;
                            if (itemDetail.vatBalance != "")
                                vat += Convert.ToDecimal(itemDetail.vatBalance);
                            UpdateStatusUserInfo(info.ca, false, payment, vat, fine.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                tf = false;
                throw new System.Exception();
            }
            return tf;
        }
        public string PrintDate(int mouth)
        {
            string str = "";
            if (mouth == 1)
                str = "มกราคม";
            else if (mouth == 2)
                str = "กุมภาพันธ์";
            else if (mouth == 3)
                str = "มีนาคม";
            else if (mouth == 4)
                str = "เมษายน";
            else if (mouth == 5)
                str = "พฤษภาคม";
            else if (mouth == 6)
                str = "มิถุนายน";
            else if (mouth == 7)
                str = "กรกฏาคม";
            else if (mouth == 8)
                str = "สิงหาคม";
            else if (mouth == 9)
                str = "กันยายน";
            else if (mouth == 10)
                str = "ตุลาคม";
            else if (mouth == 11)
                str = "พฤษจิกายน";
            else
                str = "ธันวาคม";
            return str;
        }
        public ResultInquiryDebt addDataWater(ClassItemWater _water)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultInquiryDebt rs = new ResultInquiryDebt();
            try
            {
                List<inquiryInfoBeanList> _lstInfo = new List<inquiryInfoBeanList>();
                List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
                rs.Output = new List<inquiryInfoBeanList>();
                if (GlobalClass.LstInfo == null)
                    GlobalClass.LstInfo = new List<inquiryInfoBeanList>();

                inquiryInfoBeanList model = new inquiryInfoBeanList();
                model.ca = _water.customerName;
                model.EleTotalAmount = _water.totalAmount;
                model.EleTotalVat = _water.VatAmount;
                model.TotalAmount = _water.totalAmount;
                model.SelectCheck = true;
                model.lockCheque = "";
                model.Status = true;
                model.OtherTotalAmount = "0";
                model.OtherTotalVat = "0";
                model.Defaultpenalty = "0";
                #region Detail
                inquiryGroupDebtBeanList modelDetail = new inquiryGroupDebtBeanList();
                modelDetail.ca = _water.customerName;
                modelDetail.amount = Convert.ToDecimal(_water.Amount);
                modelDetail.percentVat = _water.vatType;
                modelDetail.vatBalance = _water.VatAmount.ToString("#,###,###,##0.00");
                modelDetail.totalAmount = _water.totalAmount.ToString("#,###,###,##0.00");
                modelDetail.debtBalance = _water.totalAmount;
                modelDetail.Payment = Math.Round(_water.totalAmount, 2);
                modelDetail.dueDate = Substring(Convert.ToString(_water.dueDate));
                modelDetail.unit = _water.meaUnit;
                modelDetail.debtType = "MWA";
                modelDetail.mwaTaxId = _water.mwaTaxId;
                modelDetail.customerCode = _water.customerCode;
                modelDetail.billDueDate = _water.billDueDate;
                modelDetail.billNumber = _water.billNumber;
                modelDetail.check = _water.check;
                modelDetail.status = true;
                modelDetail.debtIdSet = _water.debtIdSet;
                modelDetail.infoCa = _water.customerName;
                _lstDebt.Add(modelDetail);
                model.inquiryGroupDebtBeanList = _lstDebt;
                _lstInfo.Add(model);
                #endregion
                rs.Output.Add(model);
                GlobalClass.LstInfo.Add(model);
                if (GlobalClass.LstCost == null)
                    GlobalClass.LstCost = new List<inquiryGroupDebtBeanList>();
                GlobalClass.LstCost.Add(modelDetail);
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public void AddReceiveinadvance(decimal amount, ResultInquiryDebt result, string ca)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultInquiryDebt rs = new ResultInquiryDebt();
            try
            {
                rs.Output = new List<inquiryInfoBeanList>();
                if (GlobalClass.LstCost == null)
                    GlobalClass.LstCost = new List<inquiryGroupDebtBeanList>();
                if (result.Output != null)
                {
                    inquiryInfoBeanList inquiry = result.Output.Where(c => c.ca == ca).FirstOrDefault();
                    inquiryGroupDebtBeanList inquiryDetail = new inquiryGroupDebtBeanList();
                    string perVat;
                    if (Convert.ToInt16(inquiry.billVatCode) >= 10)
                        perVat = "1." + inquiry.billVatCode;
                    else
                        perVat = "1.0" + inquiry.billVatCode;
                    decimal vat = amount - (amount / Convert.ToDecimal(perVat));
                    inquiryDetail.debtIdSet = new Int64[1] { 1 };
                    inquiryDetail.debtId = "1";
                    inquiryDetail.amount = Convert.ToDecimal(amount) - vat;
                    inquiryDetail.percentVat = inquiry.billVatCode;
                    inquiryDetail.vatBalance = Math.Round(vat, 2).ToString();
                    inquiryDetail.totalAmount = amount.ToString();
                    inquiryDetail.debtBalance = amount;
                    inquiryDetail.Payment = Convert.ToDecimal(amount);
                    inquiryDetail.debtType = "ADV";
                    inquiryDetail.custId = inquiry.custId;
                    inquiryDetail.ca = inquiry.ca;
                    inquiryDetail.ui = inquiry.ui;
                    inquiryDetail.status = true;
                    inquiryDetail.debtDesc = "เงินล่วงหน้า";
                    inquiryDetail.EleTotalAmountOld = Convert.ToDecimal(amount) - vat;
                    inquiryDetail.EleTotalVatOld = vat;
                    inquiryDetail.TotalAmountOld = Convert.ToDecimal(amount);
                    inquiryDetail.infoCa = inquiry.ca;
                    inquiryDetail.debtLock = "N";
                    inquiryDetail.SelectCheck = true;
                    if (inquiry.inquiryGroupDebtBeanList == null)
                    {
                        inquiry.inquiryGroupDebtBeanList = new List<inquiryGroupDebtBeanList>();
                        inquiry.EleTotalAmount = Convert.ToDecimal(amount);
                        inquiry.EleTotalVat = Math.Round(vat, 2);
                        inquiry.TotalAmount = Convert.ToDecimal(amount);
                        inquiry.SelectCheck = true;
                        inquiry.Status = true;
                    }
                    else
                    {
                        inquiryGroupDebtBeanList debtBeanlist = inquiry.inquiryGroupDebtBeanList.Where(c => c.status == true).FirstOrDefault();
                        if (debtBeanlist != null)
                        {
                            inquiry.EleTotalAmount += Convert.ToDecimal(amount);
                            inquiry.EleTotalVat += vat;
                            inquiry.TotalAmount += Convert.ToDecimal(amount);
                        }
                        else
                        {
                            inquiry.EleTotalAmount = Convert.ToDecimal(amount);
                            inquiry.EleTotalVat = vat;
                            inquiry.TotalAmount = Convert.ToDecimal(amount);
                        }
                        inquiry.SelectCheck = true;
                        inquiry.Status = true;
                    }
                    inquiry.inquiryGroupDebtBeanList.Add(inquiryDetail);
                    GlobalClass.LstCost.Add(inquiryDetail);
                }
                GlobalClass.LstInfo = result.Output;
            }
            catch (Exception ex)
            {
                ControlErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
        }
        public string Substring(string date)
        {
            string _date = "";
            string day = date.Substring(0, 2);
            string month = date.Substring(2, 2);
            string year = date.Substring(4, 2);
            _date = day + "/" + month + "/" + ConvertDate() + year;
            return _date;
        }
        public string ConvertDate()
        {
            string date = "";
            DateTime dt = DateTime.Now;
            DateTime _date;
            if (dt.Year < 2500)
                _date = dt.AddYears(543);
            else
                _date = dt;
            var strDate = _date.Year;
            date = strDate.ToString().Substring(0, 2);
            return date;
        }
        public ResultPayment SelectDataRemittance(inquiryInfoBeanList cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultPayment rs = new ResultPayment();
            try
            {
                #region
                string strJSON = null;
                string result = JsonConvert.SerializeObject(cls);
                strJSON = ClassReadAPI.PostDataAPI("processPaymentDisplayTransction", result, GlobalClass.Timer);
                ResultPayment clsData = ClassReadAPI.ReadToObjectDisplayRemittance(strJSON);
                rs.Output = new List<paymentDisplayTransctionResponseDtoList>();
                List<paymentDisplayTransctionResponseDtoList> _list = new List<paymentDisplayTransctionResponseDtoList>();
                if (clsData.Output != null)
                {
                    foreach (paymentDisplayTransctionResponseDtoList item in clsData.Output)
                    {
                        paymentDisplayTransctionResponseDtoList models = new paymentDisplayTransctionResponseDtoList();
                        models.paymentNo = item.paymentNo;
                        models.receiptNo = item.receiptNo;
                        models.paymentChannelDesc = item.paymentChannelDesc;
                        models.distShortDesc = item.distShortDesc;
                        models.paymentTypeDesc = item.paymentTypeDesc;
                        DateTime dt = Convert.ToDateTime(item.paymentDateStr);
                        models.paymentDateStr = dt.ToString("dd/MM/yyyy");
                        models.debtName = item.debtName;
                        models.receiptDtlName = item.receiptDtlName;
                        models.receiptDtlAmount = item.receiptDtlAmount;
                        models.receiptDtlVat = item.receiptDtlVat;
                        models.receiptDtlTotalAmount = item.receiptDtlTotalAmount;
                        models.cashierName = item.cashierName;
                        models.cashierNo = item.cashierNo;
                        rs.Output.Add(models);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ControlErrorLog = new ClassLogsService("ErrorLog", "");
                ControlErrorLog.WriteData("Controller :: ArrearsController | Function :: SelectDataRemittance()" + " | Error ::" + ex.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultPayInBalance SelectDataPayin(ClassPayInBalance cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultPayInBalance rs = new ResultPayInBalance();
            try
            {
                string strJSON = null;
                string result = JsonConvert.SerializeObject(cls);
                strJSON = ClassReadAPI.PostDataAPI("processGetPayIn", result, GlobalClass.Timer);
                ResultPayInBalance clsData = ClassReadAPI.ReadToObjectPayIn(strJSON);
                rs.OutputResponseDto = new List<payInResponseDto>();
                if (clsData.ResultMessage == "SUCCESS")
                {
                    if (clsData.OutputDisplay != null)
                    {
                        payInResponseDto item = clsData.OutputDisplay.payInResponseDto;
                        payInResponseDto models = new payInResponseDto();
                        models.cashierId = item.cashierId;
                        models.cashierNo = item.cashierNo;
                        models.createdDtStr = item.createdDtStr;
                        models.cashTotalAmount = Math.Round(item.cashTotalAmount, 2);
                        models.chequeAmount = Math.Round(item.chequeAmount, 2);
                        models.cardAmount = Math.Round(item.cardAmount, 2);
                        models.transferCash = item.transferCash;
                        models.transferCheque = item.transferCheque;
                        models.transferCard = item.transferCard;
                        if (item.payInChequeResponseDtoList != null || item.payInChequeResponseDtoList.Count != 0)
                        {
                            models.payInChequeResponseDtoList = new List<payInChequeResponseDtoList>();
                            foreach (payInChequeResponseDtoList itemCheque in item.payInChequeResponseDtoList)
                            {
                                payInChequeResponseDtoList modelsCheque = new payInChequeResponseDtoList();
                                modelsCheque.chequeId = itemCheque.chequeId;
                                modelsCheque.chequeNo = itemCheque.chequeNo;
                                modelsCheque.bankDesc = itemCheque.bankDesc;
                                modelsCheque.chequeOwner = itemCheque.chequeOwner;
                                modelsCheque.chequeAmount = Math.Round(itemCheque.chequeAmount, 2);
                                models.payInChequeResponseDtoList.Add(modelsCheque);
                            }
                        }
                        if (item.payInCardResponseDtoList != null && item.payInCardResponseDtoList.Count != 0)
                        {
                            models.payInCardResponseDtoList = new List<payInCardResponseDtoList>();
                            foreach (payInCardResponseDtoList itemCard in item.payInCardResponseDtoList)
                            {
                                payInCardResponseDtoList modelsCard = new payInCardResponseDtoList();
                                modelsCard.cardNo = itemCard.cardNo;
                                modelsCard.cardOwner = itemCard.cardOwner;
                                modelsCard.cardId = itemCard.cardId;
                                modelsCard.cardAmount = Math.Round(itemCard.cardAmount, 2);
                                models.payInCardResponseDtoList.Add(modelsCard);
                            }
                        }
                        rs.OutputResponseDto.Add(models);
                    }
                }
                else
                {
                    LogEventInquiry.WriteDataEvent("Fail ==> Message :: " + clsData.ResultMessage.ToString(), method, LogLevel.Debug);
                    rs.ResultMessage = clsData.ResultMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
        public ResultProcessCancelPayIn CancelRemittance(ClassClosingDrawer cls)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            ResultProcessCancelPayIn rs = new ResultProcessCancelPayIn();
            try
            {
                string result = JsonConvert.SerializeObject(cls);
                var strJSON = ClassReadAPI.PostDataAPI("processCancelPayIn", result, GlobalClass.Timer);
                ResultProcessCancelPayIn clsData = ClassReadAPI.ReadToObjectProcessCancelPayIn(strJSON);
                rs.Output = new ClassProcessCancelPayIn();
                rs.ResultMessage = clsData.ResultMessage;
                if (clsData.ResultMessage == "SUCCESS")
                    rs.closingDrawerIdList = clsData.Output.closingDrawerIdList;
            }
            catch (Exception ex)
            {
                LogErrorInquiry.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                throw new System.Exception();
            }
            return rs;
        }
    }
}
