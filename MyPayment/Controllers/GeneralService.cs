﻿using MyPayment.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPayment.Controllers
{
    class GeneralService
    {
        private static string _logControlLevel;
        private static string _logPath;
        private static int _maxLogFileSize;
        private static string _ptiClearSetConnectionString;
        private static string _ptiRegDepConnectionString;
        private static string _ptiACAConnectionString;
        private static Hashtable _htXmlSchemaCollection = new Hashtable();
        private static string _xmlSchemaPath;
        public static string LogPath
        {
            get
            {
                if ((GeneralService._logPath == null))
                    throw new Exception("Could not find the LogPath parameter in the application configuration file.");
                return GeneralService._logPath;
            }
        }
        public static string LogControlLevel
        {
            get
            {
                if ((GeneralService._logControlLevel == null))
                    throw new Exception("Could not find the LogControlLevel parameter in the application configuration file.");
                return GeneralService._logControlLevel;
            }
        }
        public static int MaxLogFileSize
        {
            get
            {
                if ((GeneralService._maxLogFileSize == 0))
                    throw new Exception("Could not find the MaxLogFileSize parameter in the application configuration file.");

                return GeneralService._maxLogFileSize;
            }
        }
        public GeneralService(bool status)
        {
            if (status)
                _logPath = @"C:\XMLData\LogOnline"; 
            else
                _logPath = @"C:\XMLData\LogSemi";
            _logControlLevel = ""; 
            _maxLogFileSize = 800000;
            _xmlSchemaPath = "";
        }
      
    }
}
