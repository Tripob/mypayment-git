﻿using MyPayment.Controllers;
using MyPayment.FormArrears;
using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormOtherPayments
{
    public partial class FormOtherNew : Form
    {
        ClassLogsService meaLog = new ClassLogsService("EventLog", "");
        ArrearsController arr = new ArrearsController();
        ClassLogsService OtherLog = new ClassLogsService("Other", "DataLog");
        ClassLogsService OtherEventLog = new ClassLogsService("Other", "EventLog");
        ClassLogsService OtherErrorLog = new ClassLogsService("Other", "ErrorLog");
        public int _status = 0;
        public FormOtherNew()
        {
            InitializeComponent();
        }
        private void FormOtherNew_Load(object sender, EventArgs e)
        {
            GlobalClass.ClassOther = new List<ClassOther>();
            GlobalClass.LstCost = new List<inquiryGroupDebtBeanList>();
            LabelFkName.Text = GlobalClass.Dist;
            LabelEmployeeCode.Text = GlobalClass.UserId.ToString();
            LabelEmployeeName.Text = GlobalClass.UserName;
            LabelStationNo.Text = Convert.ToDecimal(GlobalClass.No).ToString("00");
            ComboBoxBussType.SelectedIndex = 0;
            LoadProfitCenter();
        }
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (!CheckDataText())
            {
                AddDataForm();
                FormDetail frmDetail = new FormDetail();
                frmDetail.ShowDialog();
                frmDetail.Dispose();
                this.dgvDetail.AutoGenerateColumns = false;
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.dgvDetail.DataSource;
                List<ClassOther> LstItem = new List<ClassOther>();
                if (bs != null)
                {
                    LstItem = (List<ClassOther>)bs.DataSource;
                    if (LstItem == null)
                        LstItem = new List<ClassOther>();
                }
                else
                    bs = new BindingSource();
                LstItem.AddRange(GlobalClass.ClassOther);
                //int index = 0;
                //LstItem.ForEach(x => x.InvNo = (++index).ToString()); // update index
                bs = new BindingSource();
                bs.DataSource = LstItem;
                this.dgvDetail.DataSource = bs;
                CheckBoxSelectAll.Checked = true;
                GlobalClass.ClassOther = LstItem;
                SumItem();
            }
            else
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public void AddDataForm()
        {
            ClassOthDetail model = new ClassOthDetail();
            model.InvNo = TextBoxRef.Text;
            model.UI = TextBoxID.Text;
            model.CustName = TextBoxCustName.Text;
            model.Address = TextBoxCustAddr.Text;
            model.BusinessType = ComboBoxBussType.Text;
            model.Department = txtDepartment.Text;
            string[] profit = ComboBoxPrc.Text.Split(' ');
            model.profit = profit[0].ToString();
            GlobalClass.ClassOthDatail = model;
        }
        public bool CheckDataText()
        {
            bool tf = false;
            if (TextBoxRef.Text == string.Empty)
            {
                tf = true;
                TextBoxRef.Focus();
            }
            if (TextBoxCustName.Text == string.Empty)
            {
                tf = true;
                TextBoxCustName.Focus();
            }
            if (TextBoxCustAddr.Text == string.Empty)
            {
                tf = true;
                TextBoxCustAddr.Focus();
            }
            if (ComboBoxBussType.SelectedIndex == 0)
            {
                tf = true;
                ComboBoxBussType.Focus();
            }
            return tf;
        }
        public void LoadProfitCenter()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ResultExpenseItem result = arr.GetExpenseItem(0);
                List<listProfitCenter> _lstInfo = new List<listProfitCenter>();
                int i = 0;
                foreach (var item in result.OutputPro)
                {
                    if (i == 0)
                    {
                        listProfitCenter exItemNew = new listProfitCenter();
                        exItemNew.distShortDesc = "";
                        exItemNew.profitCenter = " == กรุณาเลือกข้อมูล == ";
                        _lstInfo.Add(exItemNew);
                    }
                    listProfitCenter exItem = new listProfitCenter();
                    exItem.distShortDesc = item.distShortDesc;
                    exItem.profitCenter = item.profitCenter + " " + item.distShortDesc;
                    _lstInfo.Add(exItem);
                    i++;
                }
                ComboBoxPrc.DataSource = _lstInfo;
                ComboBoxPrc.ValueMember = "distShortDesc";
                ComboBoxPrc.DisplayMember = "profitCenter";
                ComboBoxPrc.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                OtherErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("เกิดข้อผิดพลาดในการโหลดข้อมูลของ ศูนย์กำไร", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonClear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        public void ClearData()
        {
            TextBoxRef.Text = string.Empty;
            TextBoxID.Text = string.Empty;
            TextBoxCustName.Text = string.Empty;
            TextBoxCustAddr.Text = string.Empty;
            ComboBoxBussType.SelectedIndex = 0;
            txtDepartment.Text = string.Empty;
            ComboBoxPrc.SelectedIndex = 0;
            labelItem.Text = "0";
            txtAmount.Text = string.Empty;
            txtVat.Text = string.Empty;
            txtTotalSum.Text = "0.00";
            BindingSource bs = new BindingSource();
            this.dgvDetail.DataSource = bs;
        }
        private void ButtonRec_Click(object sender, EventArgs e)
        {
            if (labelItem.Text.Trim() != "0")
            {
                AddDataTolstInfo();
                string cusName = "";
                int i = 0;
                List<ClassModelsReport> _list = new List<ClassModelsReport>();
                foreach (DataGridViewRow row in dgvDetail.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["ColCheckBox"].EditedFormattedValue))
                    {
                        if (i == 0)
                            cusName = row.Cells["ColCustName"].Value.ToString();

                        ClassModelsReport report = new ClassModelsReport();
                        report.DistName = GlobalClass.DistName;
                        report.ReceiptBranch = GlobalClass.ReceiptBranch;
                        report.CustomerName = row.Cells["ColCustName"].Value.ToString();
                        _list.Add(report);
                        i++;
                    }
                }
                GlobalClass.List = _list;
                FormReceiveMoney receive = new FormReceiveMoney(2);
                receive.Countamount = (txtTotalSum.Text.Trim() != "") ? Convert.ToDouble(txtTotalSum.Text.Trim()) : 0;
                receive.CustomerName = cusName;
                receive.ShowDialog();
                receive.Dispose();
                if (GlobalClass.StatusFormMoney == 1)
                {
                    GlobalClass.PayMent = null;
                    GlobalClass.LstInfo = new List<inquiryInfoBeanList>();
                    ClearData();
                }
                GlobalClass.StatusFormMoney = null;
            }
        }
        public void AddDataTolstInfo()
        {
            int index = 0;
            if (GlobalClass.ClassOther.Count > 0)
            {
                List<inquiryGroupDebtBeanList> _lstDebt = new List<inquiryGroupDebtBeanList>();
                if (GlobalClass.LstInfo == null || GlobalClass.LstInfo.Count == 0)
                    GlobalClass.LstInfo = new List<inquiryInfoBeanList>();
                if (GlobalClass.LstCost == null || GlobalClass.LstCost.Count == 0)
                    GlobalClass.LstCost = new List<inquiryGroupDebtBeanList>();

                int i = 1;
                inquiryInfoBeanList info = new inquiryInfoBeanList();
                foreach (var item in GlobalClass.ClassOther)
                {
                    if (item.SelectCheck == true)
                    {
                        info.SelectCheck = true;
                        info.ca = i.ToString();
                        info.ui = TextBoxID.Text;
                        info.payeeEleName = item.CustName;
                        info.payeeEleAddress = item.Address;
                        info.payeeEleTaxBranch = "0000";
                        info.Status = true;
                        info.EleTotalAmount += item.Amount;
                        info.EleTotalVat += item.Vat;
                        info.TotalAmount += item.Amount + item.Vat;

                        #region Detail
                        inquiryGroupDebtBeanList detail = new inquiryGroupDebtBeanList();
                        detail.SelectCheck = true;
                        detail.debtType = "OTH";
                        detail.invNo = item.InvNo;
                        detail.ca = i.ToString();
                        detail.infoCa = i.ToString();
                        detail.reqDesc = item.CustName;
                        detail.price = item.UnitPrice;
                        detail.unit = item.CountUnit.ToString();
                        detail.itemId = int.Parse(item.CodeId);
                        detail.invHdrNo = item.InvNo;
                        detail.amount = item.Amount;
                        detail.vatBalance = item.Vat.ToString();
                        detail.ui = TextBoxID.Text;
                        detail.percentVat = item.PercentVat.ToString();
                        detail.totalAmount = item.CountAmount.ToString("N2");
                        detail.debtBalance = Convert.ToDecimal(item.CountAmount.ToString("N2"));
                        detail.status = true;
                        detail.unitCode = item.Unit;
                        detail.accountCode = item.AccountCode;
                        detail.itemDesc1 = item.Remark1;
                        detail.itemDesc2 = item.Remark2;
                        detail.itemDesc3 = item.Remark3;
                        detail.profitCenter = item.profit;
                        detail.businessTypeId = item.BusinessType;
                        index++;
                        detail.debtIdSet = new Int64[] { index };
                        #endregion
                        _lstDebt.Add(detail);
                        info.inquiryGroupDebtBeanList = _lstDebt;
                        GlobalClass.LstCost.Add(detail);
                        //i++;
                    }
                }
                GlobalClass.LstInfo.Add(info);
            }
        }
        private void ComboBoxBussType_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void ComboBoxPrc_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDepartment.Text = ComboBoxPrc.SelectedValue.ToString();
        }
        private void dgvDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                UpdateStatus((e.RowIndex + 1).ToString());
                SumItem();

                //if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                //{
                //    double vat = 0;
                //    double amount = 0;
                //    int i = 0;
                //    int countCheck = 0;
                //    var loopTo = dgvDetail.Rows.Count - 1;
                //    for (i = 0; i <= loopTo; i++)
                //    {
                //        if (dgvDetail.CurrentCell is DataGridViewCheckBoxCell)
                //        {
                //            string Inv = dgvDetail.Rows[i].Cells["ColInvNo"].Value.ToString();
                //            DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)dgvDetail.Rows[i].Cells["ColCheckBox"];
                //            bool newBool = (bool)checkCell.EditedFormattedValue;
                //            if (newBool)
                //            {
                //                if (dgvDetail.Rows[i].Cells["ColAmount"].Value.ToString() != "")
                //                    amount += Convert.ToDouble(dgvDetail.Rows[i].Cells["ColAmount"].Value);
                //                if (dgvDetail.Rows[i].Cells["ColVat"].Value.ToString() != "")
                //                    vat += Convert.ToDouble(dgvDetail.Rows[i].Cells["ColVat"].Value);
                //                countCheck++;
                //                if (countCheck == dgvDetail.Rows.Count)
                //                    CheckBoxSelectAll.Checked = true;
                //            }
                //            else
                //            {
                //                countCheck--;
                //                CheckBoxSelectAll.Checked = false;
                //            }
                //           // UpdateStatus(Inv, newBool);
                //        }
                //        labelItem.Text = countCheck.ToString();
                //    }

                //    UpdateStatus((e.RowIndex + 1).ToString(), true);

                //    txtAmount.Text = amount.ToString("#,###,###,##0.00");
                //    txtVat.Text = vat.ToString("#,###,###,##0.00");
                //    txtTotalSum.Text = (amount + vat).ToString("#,###,###,##0.00");
                //}
            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void SumItem()
        {
            try
            {
                //BindingSource bs = new BindingSource();
                //bs = (BindingSource)this.dgvDetail.DataSource;
                //List<ClassOther> LstItem = new List<ClassOther>();
                //LstItem = (List<ClassOther>)bs.DataSource;


                //LstItem = LstItem.Where(m => m.SelectCheck == true).ToList();

                /*********************************/


                double vat = 0;
                double amount = 0;
                int i = 0;
                int countCheck = 0;
                var loopTo = dgvDetail.Rows.Count - 1;
                for (i = 0; i <= loopTo; i++)
                {
                    if (dgvDetail.CurrentCell is DataGridViewCheckBoxCell)
                    {
                        string Inv = dgvDetail.Rows[i].Cells["ColInvNo"].Value.ToString();
                        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)dgvDetail.Rows[i].Cells["ColCheckBox"];
                        bool newBool = (bool)checkCell.EditedFormattedValue;
                        if (newBool)
                        {
                            if (dgvDetail.Rows[i].Cells["ColAmount"].Value.ToString() != "")
                                amount += Convert.ToDouble(dgvDetail.Rows[i].Cells["ColAmount"].Value);
                            if (dgvDetail.Rows[i].Cells["ColVat"].Value.ToString() != "")
                                vat += Convert.ToDouble(dgvDetail.Rows[i].Cells["ColVat"].Value);
                            countCheck++;
                            // if (countCheck == dgvDetail.Rows.Count)
                            //  CheckBoxSelectAll.Checked = true;
                        }
                        else
                        {
                            // countCheck--;
                            // CheckBoxSelectAll.Checked = false;
                        }
                        // UpdateStatus(Inv, newBool);
                    }
                    labelItem.Text = countCheck.ToString();
                }

                txtAmount.Text = amount.ToString("#,###,###,##0.00");
                txtVat.Text = vat.ToString("#,###,###,##0.00");
                txtTotalSum.Text = (amount + vat).ToString("#,###,###,##0.00");
                /*********************************/
                //this.dgvDetail.DataSource = bs;
            }
            catch (Exception ex)
            {

            }
        }
        public void UpdateStatus(string inv)
        {
            //ClassOther oth = GlobalClass.ClassOther.Find(item => item.InvNo.Equals(inv, StringComparison.CurrentCultureIgnoreCase));
            //if (oth != null)
            //    oth.SelectCheck = status;


            try
            {
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.dgvDetail.DataSource;
                List<ClassOther> LstItem = new List<ClassOther>();
                LstItem = (List<ClassOther>)bs.DataSource;


                //string InvoiceNumber = GridInputData.CurrentRow.Cells["InvoiceNumber"].Value.ToString();
                //bool Selected = GridInputData.CurrentRow.Cells["Selected"].Value.ToString() == "True" ? true : false;

                LstItem.Where(m => m.InvNo == inv).ToList().ForEach(x => x.SelectCheck = !x.SelectCheck);

                if (LstItem.Where(x => x.SelectCheck == false).Any())
                {
                    CheckBoxSelectAll.Checked = false;
                }
                else
                {
                    CheckBoxSelectAll.Checked = true;
                }

                this.dgvDetail.DataSource = bs;
            }
            catch (Exception ex)
            {

            }
        }
        //private void CheckBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    //SelectCheckbox();
        //    SumItem();
        //}
        public void SelectCheckbox()
        {
            double Amount = 0;
            double TotalVat = 0;
            int i = 0;
            int countCheck = 0;
            var loopTo = dgvDetail.Rows.Count - 1;
            for (i = 0; i <= loopTo; i++)
            {
                if (CheckBoxSelectAll.Checked == true)
                {
                    if (dgvDetail.Rows[i].Cells["ColAmount"].Value.ToString() != "")
                        Amount += Convert.ToDouble(dgvDetail.Rows[i].Cells["ColAmount"].Value);
                    if (dgvDetail.Rows[i].Cells["ColVat"].Value.ToString() != "")
                        TotalVat += Convert.ToDouble(dgvDetail.Rows[i].Cells["ColVat"].Value);
                    dgvDetail.Rows[i].Cells[0].Value = true;
                    countCheck++;
                }
                else
                {
                    dgvDetail.Rows[i].Cells[0].Value = false;
                    countCheck = 0;
                }
            }
            labelItem.Text = countCheck.ToString();
            txtAmount.Text = Amount.ToString("#,###,###,##0.00");
            txtVat.Text = TotalVat.ToString("#,###,###,##0.00");
            txtTotalSum.Text = (Amount + TotalVat).ToString("#,###,###,##0.00");
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {



                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.dgvDetail.DataSource;
                List<ClassOther> LstItem = new List<ClassOther>();
                LstItem = (List<ClassOther>)bs.DataSource;

                LstItem = LstItem.Where(m => m.SelectCheck == false).ToList();


                bs = new BindingSource();
                bs.DataSource = LstItem;
                this.dgvDetail.DataSource = bs;
                GlobalClass.ClassOther = LstItem;
                SumItem();

                CheckBoxSelectAll.Checked = false;

                //int i = 0;
                //var loopTo = dgvDetail.Rows.Count - 1;
                //for (i = 0; i <= loopTo; i++)
                //{
                //    if (dgvDetail.CurrentCell is DataGridViewCheckBoxCell)
                //    {
                //        DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)dgvDetail.Rows[i].Cells["ColCheckBox"];
                //        bool newBool = (bool)checkCell.EditedFormattedValue;
                //        if (newBool)
                //        {
                //            string inv = dgvDetail.Rows[i].Cells["ColInvNo"].Value.ToString();
                //            var itemToRemove = GlobalClass.ClassOther.SingleOrDefault(r => r.InvNo == inv);
                //            if (itemToRemove != null)
                //            {
                //                GlobalClass.ClassOther.Remove(itemToRemove);
                //                meaLog.WriteData("DeleteOther :: Success :: Inv : " + inv, method, LogLevel.Debug);
                //            }
                //        }
                //    }
                //}
                //if (GlobalClass.ClassOther.Count <= 0)
                //    ClearData();
                //else
                //    this.dgvDetail.DataSource = GlobalClass.ClassOther;


            }
            catch (Exception ex)
            {
                meaLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.dgvDetail.DataSource;
                List<ClassOther> LstItem = new List<ClassOther>();
                LstItem = (List<ClassOther>)bs.DataSource;

                ClassOther rec = LstItem.Where(m => m.SelectCheck == true).FirstOrDefault();

                FormDetail frmDetail = new FormDetail();
                frmDetail.SetToEdit(rec);
                frmDetail.ShowDialog();
                frmDetail.Dispose();

                /*************************************************/


                LstItem = LstItem.Where(m => m.InvNo != rec.InvNo).ToList();
                ClassOther newRec = GlobalClass.ClassOther.FirstOrDefault(); // เลือกอันที่ Edit
                newRec.InvNo = rec.InvNo;
                LstItem.Add(newRec);

                bs = new BindingSource();
                bs.DataSource = LstItem;
                this.dgvDetail.DataSource = bs;
                GlobalClass.ClassOther = LstItem;

                SumItem();

            }
            catch (Exception ex)
            {

            }
        }
        private void dgvDetail_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                _status = 1;
                ClassOther model = new ClassOther();
                TextBoxRef.Text = dgvDetail.CurrentRow.Cells["ColInvNo"].Value.ToString();
                TextBoxID.Text = dgvDetail.CurrentRow.Cells["ColCa"].Value.ToString();
                TextBoxCustName.Text = dgvDetail.CurrentRow.Cells["ColCustName"].Value.ToString();
                TextBoxCustAddr.Text = dgvDetail.CurrentRow.Cells["ColAddress"].Value.ToString();
                ComboBoxBussType.Text = dgvDetail.CurrentRow.Cells["ColBusinessType"].Value.ToString();
                txtDepartment.Text = dgvDetail.CurrentRow.Cells["ColDepartment"].Value.ToString();
                ComboBoxPrc.Text = dgvDetail.CurrentRow.Cells["Colprofit"].Value.ToString();

                model.TypeItem = dgvDetail.CurrentRow.Cells["ColTypeItem"].Value.ToString();
                model.CodeId = dgvDetail.CurrentRow.Cells["ColCodeId"].Value.ToString();
                model.Unit = dgvDetail.CurrentRow.Cells["ColUnitPrice"].Value.ToString();
                model.UnitPrice = Convert.ToDecimal(dgvDetail.CurrentRow.Cells["ColCountUnit"].Value.ToString());
                model.CountUnit = int.Parse(dgvDetail.CurrentRow.Cells["ColUnit"].Value.ToString());
                model.Amount = Convert.ToDecimal(dgvDetail.CurrentRow.Cells["ColAmount"].Value.ToString());
                model.Vat = Convert.ToDecimal(dgvDetail.CurrentRow.Cells["ColVat"].Value.ToString());
                model.CountAmount = Convert.ToDecimal(dgvDetail.CurrentRow.Cells["ColCountAmount"].Value.ToString());
                model.Remark1 = dgvDetail.CurrentRow.Cells["ColRemark1"].Value.ToString();
                model.Remark2 = dgvDetail.CurrentRow.Cells["ColRemark2"].Value.ToString();
                model.Remark3 = dgvDetail.CurrentRow.Cells["ColRemark3"].Value.ToString();

                //GlobalClass.ClassOther = model;
                AddDataForm();
            }
            catch (Exception ex)
            {

            }


        }

        private void CheckBoxSelectAll_Click(object sender, EventArgs e)
        {
            if (dgvDetail.Rows.Count > 0)
            {
                BindingSource bs = new BindingSource();
                bs = (BindingSource)this.dgvDetail.DataSource;
                List<ClassOther> LstItem = new List<ClassOther>();
                LstItem = (List<ClassOther>)bs.DataSource;
                if (this.CheckBoxSelectAll.Checked)
                    LstItem.ForEach(x => x.SelectCheck = true);
                else
                    LstItem.ForEach(x => x.SelectCheck = false);
                bs = new BindingSource();
                bs.DataSource = LstItem;
                this.dgvDetail.DataSource = bs;
                SumItem();
            }
        }
    }
}
