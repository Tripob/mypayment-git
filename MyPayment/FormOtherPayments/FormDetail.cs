﻿using MyPayment.Controllers;
using MyPayment.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyPayment.Models.ClassLogsService;

namespace MyPayment.FormOtherPayments
{
    public partial class FormDetail : Form
    {
        ClassLogsService OtherLog = new ClassLogsService("Other", "DataLog");
        ClassLogsService OtherEventLog = new ClassLogsService("Other", "EventLog");
        ClassLogsService OtherErrorLog = new ClassLogsService("Other", "ErrorLog");
        ArrearsController arr = new ArrearsController();
        private listExpenseItem GB_ItemSelect = new listExpenseItem();
        private string GB_MODE = "";
        public ClassOther GB_DataEdit = null;

        public FormDetail()
        {
            InitializeComponent();
        }
        private void FormDetail_Load(object sender, EventArgs e)
        {
            GlobalClass.ClassOther = new List<ClassOther>();
            LoadDataType();
            LoadDataUnit();
            GlobalClass.LstInfo = new List<inquiryInfoBeanList>();
            if (GB_DataEdit != null)
                BindDataEdit(GB_DataEdit);
        }
        public void BindDataEdit(ClassOther objData)
        {
            try
            {
                cboTypeList.Text = objData.TypeItem;
                comboBoxUnit.Text = objData.Unit;
                textBoxIdCode.Text = objData.CodeId;
                textBoxCountUnit.Text = objData.UnitPrice.ToString();
                textBoxUnit.Text = objData.CountUnit.ToString();
                textBoxAmount.Text = objData.Amount.ToString("N2");
                textBoxVat.Text = objData.Vat.ToString("N2");
                textBoxCountAmount.Text = objData.CountAmount.ToString("N2");
                textBoxRemark1.Text = objData.Remark1;
                textBoxRemark2.Text = objData.Remark2;
                textBoxRemark3.Text = objData.Remark3;
            }
            catch (Exception ex)
            {

            }
        }
        public void SetToEdit(ClassOther objData)
        {
            try
            {
                GB_MODE = "EDIT";
                GB_DataEdit = new ClassOther();
                GB_DataEdit = objData;
                /***************************************/
                //ClassOther models = new ClassOther();
                //models.SelectCheck = true;
                //models.TypeItem = cboTypeList.Text;
                //models.CodeId = textBoxIdCode.Text;
                //models.AccountCode = GB_ItemSelect.itemAccountCode.ToString();
                //models.Unit = comboBoxUnit.Text;
                //models.UnitPrice = Convert.ToDouble(textBoxCountUnit.Text);
                //models.CountUnit = int.Parse(textBoxUnit.Text);
                //models.Amount = Convert.ToDouble(textBoxAmount.Text);
                //models.Vat = Convert.ToDouble(textBoxVat.Text);
                //models.PercentVat = Convert.ToInt32(GB_ItemSelect.percentVat);
                //models.CountAmount = Convert.ToDouble(textBoxCountAmount.Text);
                //models.Remark1 = textBoxRemark1.Text;
                //models.Remark2 = textBoxRemark2.Text;
                //models.Remark3 = textBoxRemark3.Text;
                //models.InvNo = GlobalClass.ClassOthDatail.InvNo;
                //models.Ca = GlobalClass.ClassOthDatail.UI;
                //models.CustName = GlobalClass.ClassOthDatail.CustName;
                //models.Address = GlobalClass.ClassOthDatail.Address;
                //models.BusinessType = GlobalClass.ClassOthDatail.BusinessType;
                //models.Department = GlobalClass.ClassOthDatail.Department;
                //models.profit = GlobalClass.ClassOthDatail.profit;
                //GlobalClass.ClassOther.Add(models);

                //textBoxIdCode.Text = objData.CodeId;
                //textBoxCountUnit.Text = objData.UnitPrice.ToString();
                //textBoxUnit.Text = objData.CountUnit.ToString();
                //textBoxAmount.Text = objData.Amount.ToString("N2");
                //textBoxVat.Text = objData.Vat.ToString("N2");
                //textBoxCountAmount.Text = objData.CountAmount.ToString("N2");
                //textBoxRemark1.Text = objData.Remark1;
                //textBoxRemark2.Text = objData.Remark2;
                //textBoxRemark3.Text = objData.Remark3;
                //cboTypeList.Text = objData.TypeItem;
                //comboBoxUnit.Text = objData.Unit;
            }
            catch (Exception ex)
            {

            }
        }
        public void LoadDataType()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ResultExpenseItem result = arr.GetExpenseItem(1);
                List<listExpenseItem> _lstInfo = new List<listExpenseItem>();
                int i = 0;
                foreach (var item in result.Output)
                {
                    if (i == 0)
                    {
                        listExpenseItem exItemNew = new listExpenseItem();
                        exItemNew.itemDescTh = " == กรุณาเลือกข้อมูล == ";
                        exItemNew.itemId = 0;
                        _lstInfo.Add(exItemNew);
                    }
                    listExpenseItem exItem = new listExpenseItem();
                    exItem.itemDescTh = item.itemDescTh;
                    exItem.itemId = item.itemId;
                    exItem.vatCode = item.vatCode;
                    exItem.percentVat = item.percentVat;
                    exItem.itemAccountCode = item.itemAccountCode;
                    _lstInfo.Add(exItem);
                    i++;
                }
                cboTypeList.DataSource = _lstInfo;
                cboTypeList.ValueMember = "itemId";
                cboTypeList.DisplayMember = "itemDescTh";
                cboTypeList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                OtherErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("เกิดข้อผิดพลาดในการโหลดข้อมูลของ ประเภทรายการ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void LoadDataUnit()
        {
            System.Reflection.MethodBase method;
            method = System.Reflection.MethodBase.GetCurrentMethod();
            try
            {
                ResultExpenseItem result = arr.GetExpenseItem(2);
                List<costUnit> _lstInfo = new List<costUnit>();
                int i = 0;
                foreach (var item in result.OutputUnit)
                {
                    if (i == 0)
                    {
                        costUnit exItemNew = new costUnit();
                        exItemNew.unitDetail = " == กรุณาเลือกข้อมูล == ";
                        exItemNew.unitId = 0;
                        _lstInfo.Add(exItemNew);
                    }
                    costUnit exItem = new costUnit();
                    exItem.unitDetail = item.unitDetail;
                    exItem.unitId = item.unitId;
                    _lstInfo.Add(exItem);
                    i++;
                }
                comboBoxUnit.DataSource = _lstInfo;
                comboBoxUnit.ValueMember = "unitId";
                comboBoxUnit.DisplayMember = "unitDetail";
                comboBoxUnit.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                OtherErrorLog.WriteDataError("Error :: " + ex.Message.ToString(), method, LogLevel.Error);
                MessageBox.Show("เกิดข้อผิดพลาดในการโหลดข้อมูลของ Unit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (!CheckDataText())
            {
                AddDataToreceiptList();
                ResetNewItem();
                this.Hide();
            }
            else
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        private void cboTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                GB_ItemSelect = new listExpenseItem();
                GB_ItemSelect = (listExpenseItem)cboTypeList.SelectedItem;
                if (GB_ItemSelect.itemId != 0)
                {
                    textBoxIdCode.Text = GB_ItemSelect.itemId.ToString();
                    textBoxCountUnit.Text = "";
                    textBoxUnit.Text = "";
                    textBoxAmount.Text = "";
                    textBoxVat.Text = "";
                    textBoxCountAmount.Text = "";
                }
                else
                {
                    comboBoxUnit.SelectedIndex = 0;
                    textBoxIdCode.Text = "";
                    textBoxCountUnit.Text = "";
                    textBoxUnit.Text = "";
                    textBoxAmount.Text = "";
                    textBoxVat.Text = "";
                    textBoxCountAmount.Text = "";
                    textBoxRemark1.Text = "";
                    textBoxRemark2.Text = "";
                    textBoxRemark3.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void textBoxCountUnit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;
        }
        private void textBoxUnit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }
        private void CountAmount()
        {
            try
            {
                double resultBoxCountAmount = 0.0;
                double resultVat = 0.0;
                if (textBoxCountUnit.Text != "" && textBoxUnit.Text != "")
                {
                    resultBoxCountAmount = double.Parse(textBoxCountUnit.Text) * double.Parse(textBoxUnit.Text);
                    textBoxAmount.Text = resultBoxCountAmount.ToString("N2");
                }
                if (GB_ItemSelect != null && GB_ItemSelect.vatCode != "N")
                {
                    double vat = 0;
                    if (GB_ItemSelect.percentVat.ToString().Length > 1)
                        vat = Convert.ToDouble("1." + GB_ItemSelect.percentVat.ToString());
                    else
                        vat = Convert.ToDouble("1.0" + GB_ItemSelect.percentVat.ToString());              
                    resultVat = (resultBoxCountAmount / vat);
                    textBoxVat.Text = resultVat.ToString("N2");
                    textBoxAmount.Text = (resultBoxCountAmount - resultVat).ToString("N2");
                    textBoxCountAmount.Text = resultBoxCountAmount.ToString("N2");
                }
                else
                {
                    textBoxVat.Text = "0.00";
                    textBoxCountAmount.Text = (resultBoxCountAmount).ToString("N2");
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void AddDataToreceiptList()
        {
            try
            {
                ClassOther models = new ClassOther();
                models.SelectCheck = true;
                models.TypeItem = cboTypeList.Text;
                models.CodeId = textBoxIdCode.Text;
                models.AccountCode = GB_ItemSelect.itemAccountCode.ToString();
                models.Unit = comboBoxUnit.Text;
                models.UnitPrice = Convert.ToDecimal(textBoxCountUnit.Text);
                models.CountUnit = int.Parse(textBoxUnit.Text);
                models.Amount = Convert.ToDecimal(textBoxAmount.Text);
                models.Vat = Convert.ToDecimal(textBoxVat.Text);
                models.PercentVat = Convert.ToInt32(GB_ItemSelect.percentVat);
                models.CountAmount = Convert.ToDecimal(textBoxCountAmount.Text);
                models.Remark1 = textBoxRemark1.Text;
                models.Remark2 = textBoxRemark2.Text;
                models.Remark3 = textBoxRemark3.Text;
                models.InvNo = GlobalClass.ClassOthDatail.InvNo;
                models.Ca = GlobalClass.ClassOthDatail.UI;
                models.CustName = GlobalClass.ClassOthDatail.CustName;
                models.Address = GlobalClass.ClassOthDatail.Address;
                models.BusinessType = GlobalClass.ClassOthDatail.BusinessType;
                models.Department = GlobalClass.ClassOthDatail.Department;
                models.profit = GlobalClass.ClassOthDatail.profit;
                GlobalClass.ClassOther.Add(models);
            }
            catch (Exception ex)
            {

            }

        }
        public bool CheckDataText()
        {
            bool tf = false;
            if (cboTypeList.SelectedIndex == 0)
                tf = true;
            if (comboBoxUnit.SelectedIndex == 0)
                tf = true;
            if (textBoxCountUnit.Text == "")
            {
                tf = true;
                textBoxCountUnit.Focus();
            }
            if (textBoxUnit.Text == "")
            {
                tf = true;
                textBoxUnit.Focus();
            }
            if (textBoxRemark1.Text == "")
            {
                tf = true;
                textBoxRemark1.Focus();
            }
            return tf;
        }
        private void textBoxCountUnit_KeyUp(object sender, KeyEventArgs e)
        {
            CountAmount();
        }

        private void textBoxUnit_KeyUp(object sender, KeyEventArgs e)
        {
            CountAmount();
        }
        private void ResetNewItem()
        {
            cboTypeList.SelectedIndex = 0;
            comboBoxUnit.SelectedIndex = 0;
            textBoxIdCode.Text = "";
            textBoxCountUnit.Text = "0";
            textBoxUnit.Text = "0";
            textBoxAmount.Text = "0";
            textBoxVat.Text = "0";
            textBoxCountAmount.Text = "0";
            textBoxRemark1.Text = "";
            textBoxRemark2.Text = "";
            textBoxRemark3.Text = "";
        }

        private void textBoxCountUnit_TextChanged(object sender, EventArgs e)
        {
            TextBox T = (TextBox)sender;
            decimal amount = Convert.ToDecimal(textBoxCountUnit.Text);
            textBoxCountUnit.Text = amount.ToString("#,###,###,##0.00");
            int len = textBoxCountUnit.Text.ToString().Length;
            T.SelectionStart = len - 3;
        }

        private void textBoxUnit_TextChanged(object sender, EventArgs e)
        {
            //TextBox T = (TextBox)sender;
            //decimal amount = Convert.ToDecimal(textBoxUnit.Text);
            //textBoxUnit.Text = amount.ToString("#,###,###,##0.00");
            //int len = textBoxUnit.Text.ToString().Length;
            //T.SelectionStart = len - 3;
        }
    }
}
