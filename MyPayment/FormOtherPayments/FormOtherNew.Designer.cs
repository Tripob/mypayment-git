﻿namespace MyPayment.FormOtherPayments
{
    partial class FormOtherNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOtherNew));
            this.panelMain = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ButtonRec = new Custom_Controls_in_CS.ButtonZ();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtVat = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.Label();
            this.txtTotalSum = new System.Windows.Forms.Label();
            this.Label37 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelItem = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.PanelGridview = new System.Windows.Forms.Panel();
            this.CheckBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.dgvDetail = new System.Windows.Forms.DataGridView();
            this.ColCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColTypeItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCodeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCountUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRemark1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRemark2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRemark3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColInvNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCustName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColBusinessType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDepartment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Colprofit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtDepartment = new System.Windows.Forms.TextBox();
            this.ButtonClear = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonDelete = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonEdit = new Custom_Controls_in_CS.ButtonZ();
            this.ButtonAdd = new Custom_Controls_in_CS.ButtonZ();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ComboBoxPrc = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ComboBoxBussType = new System.Windows.Forms.ComboBox();
            this.TextBoxCustAddr = new System.Windows.Forms.TextBox();
            this.TextBoxCustName = new System.Windows.Forms.TextBox();
            this.TextBoxID = new System.Windows.Forms.TextBox();
            this.TextBoxRef = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PanelEmployee = new System.Windows.Forms.Panel();
            this.groupBoxEmployee = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelStationNo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelEmployeeName = new System.Windows.Forms.Label();
            this.LabelEmployeeCode = new System.Windows.Forms.Label();
            this.LabelFkName = new System.Windows.Forms.Label();
            this.panelMain.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.PanelGridview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            this.panel1.SuspendLayout();
            this.PanelEmployee.SuspendLayout();
            this.groupBoxEmployee.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.panel3);
            this.panelMain.Controls.Add(this.panel2);
            this.panelMain.Controls.Add(this.panel1);
            this.panelMain.Controls.Add(this.PanelEmployee);
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1012, 693);
            this.panelMain.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ButtonRec);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 650);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1012, 43);
            this.panel3.TabIndex = 8;
            // 
            // ButtonRec
            // 
            this.ButtonRec.BackColor = System.Drawing.Color.Transparent;
            this.ButtonRec.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonRec.BorderWidth = 1;
            this.ButtonRec.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonRec.ButtonText = "รับเงิน";
            this.ButtonRec.CausesValidation = false;
            this.ButtonRec.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonRec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonRec.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonRec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.ButtonRec.GradientAngle = 90;
            this.ButtonRec.Image = global::MyPayment.Properties.Resources.icons8_paper_money_26;
            this.ButtonRec.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonRec.Location = new System.Drawing.Point(924, 6);
            this.ButtonRec.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonRec.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonRec.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonRec.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonRec.Name = "ButtonRec";
            this.ButtonRec.ShowButtontext = true;
            this.ButtonRec.Size = new System.Drawing.Size(83, 35);
            this.ButtonRec.StartColor = System.Drawing.Color.LightGray;
            this.ButtonRec.TabIndex = 1000000039;
            this.ButtonRec.TextLocation_X = 35;
            this.ButtonRec.TextLocation_Y = 6;
            this.ButtonRec.Transparent1 = 80;
            this.ButtonRec.Transparent2 = 120;
            this.ButtonRec.UseVisualStyleBackColor = true;
            this.ButtonRec.Click += new System.EventHandler(this.ButtonRec_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtVat);
            this.panel2.Controls.Add(this.txtAmount);
            this.panel2.Controls.Add(this.txtTotalSum);
            this.panel2.Controls.Add(this.Label37);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.labelItem);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.PanelGridview);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 269);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1012, 381);
            this.panel2.TabIndex = 7;
            // 
            // txtVat
            // 
            this.txtVat.BackColor = System.Drawing.Color.White;
            this.txtVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtVat.Location = new System.Drawing.Point(753, 295);
            this.txtVat.Name = "txtVat";
            this.txtVat.Size = new System.Drawing.Size(190, 25);
            this.txtVat.TabIndex = 1000000029;
            this.txtVat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.Color.White;
            this.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtAmount.Location = new System.Drawing.Point(753, 262);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(190, 25);
            this.txtAmount.TabIndex = 1000000028;
            this.txtAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalSum
            // 
            this.txtTotalSum.BackColor = System.Drawing.Color.White;
            this.txtTotalSum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTotalSum.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalSum.Location = new System.Drawing.Point(549, 331);
            this.txtTotalSum.Name = "txtTotalSum";
            this.txtTotalSum.Size = new System.Drawing.Size(394, 39);
            this.txtTotalSum.TabIndex = 1000000027;
            this.txtTotalSum.Text = "0.00";
            this.txtTotalSum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.Font = new System.Drawing.Font("TH Sarabun New", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label37.ForeColor = System.Drawing.Color.Blue;
            this.Label37.Location = new System.Drawing.Point(371, 322);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(173, 57);
            this.Label37.TabIndex = 1000000026;
            this.Label37.Text = "รวมรับชำระ";
            this.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("TH Sarabun New", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(948, 331);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(59, 43);
            this.label25.TabIndex = 1000000025;
            this.label25.Text = "บาท";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelItem
            // 
            this.labelItem.BackColor = System.Drawing.Color.White;
            this.labelItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelItem.ForeColor = System.Drawing.Color.Black;
            this.labelItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelItem.Location = new System.Drawing.Point(90, 261);
            this.labelItem.Name = "labelItem";
            this.labelItem.Size = new System.Drawing.Size(123, 28);
            this.labelItem.TabIndex = 1000000020;
            this.labelItem.Text = "0";
            this.labelItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label21.Location = new System.Drawing.Point(948, 298);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 26);
            this.label21.TabIndex = 1000000015;
            this.label21.Text = "บาท";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label20.Location = new System.Drawing.Point(948, 262);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 26);
            this.label20.TabIndex = 1000000014;
            this.label20.Text = "บาท";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label19.Location = new System.Drawing.Point(648, 298);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 26);
            this.label19.TabIndex = 1000000013;
            this.label19.Text = "ภาษีมูลค่าเพิ่ม";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label18.Location = new System.Drawing.Point(684, 262);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 26);
            this.label18.TabIndex = 1000000012;
            this.label18.Text = "รวมเงิน";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label17.Location = new System.Drawing.Point(219, 262);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 26);
            this.label17.TabIndex = 1000000011;
            this.label17.Text = "รายการ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(22, 262);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 26);
            this.label16.TabIndex = 1000000010;
            this.label16.Text = "รวมทั้งสิน";
            // 
            // PanelGridview
            // 
            this.PanelGridview.Controls.Add(this.CheckBoxSelectAll);
            this.PanelGridview.Controls.Add(this.dgvDetail);
            this.PanelGridview.Location = new System.Drawing.Point(12, 0);
            this.PanelGridview.Name = "PanelGridview";
            this.PanelGridview.Size = new System.Drawing.Size(997, 257);
            this.PanelGridview.TabIndex = 1;
            // 
            // CheckBoxSelectAll
            // 
            this.CheckBoxSelectAll.AutoSize = true;
            this.CheckBoxSelectAll.Location = new System.Drawing.Point(8, 11);
            this.CheckBoxSelectAll.Name = "CheckBoxSelectAll";
            this.CheckBoxSelectAll.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxSelectAll.TabIndex = 1;
            this.CheckBoxSelectAll.UseVisualStyleBackColor = true;
            this.CheckBoxSelectAll.Click += new System.EventHandler(this.CheckBoxSelectAll_Click);
            // 
            // dgvDetail
            // 
            this.dgvDetail.AllowUserToAddRows = false;
            this.dgvDetail.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCheckBox,
            this.ColTypeItem,
            this.ColCodeId,
            this.ColUnitPrice,
            this.ColCountUnit,
            this.ColUnit,
            this.ColAmount,
            this.ColVat,
            this.ColCountAmount,
            this.ColRemark1,
            this.ColRemark2,
            this.ColRemark3,
            this.ColInvNo,
            this.ColCa,
            this.ColCustName,
            this.ColAddress,
            this.ColBusinessType,
            this.ColDepartment,
            this.Colprofit});
            this.dgvDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetail.Location = new System.Drawing.Point(0, 0);
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.ReadOnly = true;
            this.dgvDetail.RowHeadersVisible = false;
            this.dgvDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetail.Size = new System.Drawing.Size(997, 257);
            this.dgvDetail.TabIndex = 0;
            this.dgvDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetail_CellContentClick);
            this.dgvDetail.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetail_CellDoubleClick);
            // 
            // ColCheckBox
            // 
            this.ColCheckBox.DataPropertyName = "SelectCheck";
            this.ColCheckBox.Frozen = true;
            this.ColCheckBox.HeaderText = "";
            this.ColCheckBox.Name = "ColCheckBox";
            this.ColCheckBox.ReadOnly = true;
            this.ColCheckBox.Width = 30;
            // 
            // ColTypeItem
            // 
            this.ColTypeItem.DataPropertyName = "TypeItem";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColTypeItem.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColTypeItem.HeaderText = "ประเภทรายการ";
            this.ColTypeItem.Name = "ColTypeItem";
            this.ColTypeItem.ReadOnly = true;
            this.ColTypeItem.Width = 180;
            // 
            // ColCodeId
            // 
            this.ColCodeId.DataPropertyName = "CodeId";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColCodeId.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColCodeId.HeaderText = "รหัสบัญชี";
            this.ColCodeId.Name = "ColCodeId";
            this.ColCodeId.ReadOnly = true;
            this.ColCodeId.Width = 150;
            // 
            // ColUnitPrice
            // 
            this.ColUnitPrice.DataPropertyName = "UnitPrice";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColUnitPrice.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColUnitPrice.HeaderText = "ราคาต่อหน่วย";
            this.ColUnitPrice.Name = "ColUnitPrice";
            this.ColUnitPrice.ReadOnly = true;
            this.ColUnitPrice.Width = 150;
            // 
            // ColCountUnit
            // 
            this.ColCountUnit.DataPropertyName = "CountUnit";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColCountUnit.DefaultCellStyle = dataGridViewCellStyle5;
            this.ColCountUnit.HeaderText = "จำนวนหน่วย";
            this.ColCountUnit.Name = "ColCountUnit";
            this.ColCountUnit.ReadOnly = true;
            this.ColCountUnit.Width = 150;
            // 
            // ColUnit
            // 
            this.ColUnit.DataPropertyName = "Unit";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColUnit.DefaultCellStyle = dataGridViewCellStyle6;
            this.ColUnit.HeaderText = "หน่วย";
            this.ColUnit.Name = "ColUnit";
            this.ColUnit.ReadOnly = true;
            // 
            // ColAmount
            // 
            this.ColAmount.DataPropertyName = "Amount";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColAmount.DefaultCellStyle = dataGridViewCellStyle7;
            this.ColAmount.HeaderText = "จำนวนเงิน";
            this.ColAmount.Name = "ColAmount";
            this.ColAmount.ReadOnly = true;
            // 
            // ColVat
            // 
            this.ColVat.DataPropertyName = "Vat";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColVat.DefaultCellStyle = dataGridViewCellStyle8;
            this.ColVat.HeaderText = "ภาษี";
            this.ColVat.Name = "ColVat";
            this.ColVat.ReadOnly = true;
            // 
            // ColCountAmount
            // 
            this.ColCountAmount.DataPropertyName = "CountAmount";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColCountAmount.DefaultCellStyle = dataGridViewCellStyle9;
            this.ColCountAmount.HeaderText = "จำนวนเงินรวม";
            this.ColCountAmount.Name = "ColCountAmount";
            this.ColCountAmount.ReadOnly = true;
            this.ColCountAmount.Width = 150;
            // 
            // ColRemark1
            // 
            this.ColRemark1.DataPropertyName = "Remark1";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColRemark1.DefaultCellStyle = dataGridViewCellStyle10;
            this.ColRemark1.HeaderText = "คำอธิบายรายการ1";
            this.ColRemark1.Name = "ColRemark1";
            this.ColRemark1.ReadOnly = true;
            this.ColRemark1.Width = 180;
            // 
            // ColRemark2
            // 
            this.ColRemark2.DataPropertyName = "Remark2";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColRemark2.DefaultCellStyle = dataGridViewCellStyle11;
            this.ColRemark2.HeaderText = "คำอธิบายรายการ2";
            this.ColRemark2.Name = "ColRemark2";
            this.ColRemark2.ReadOnly = true;
            this.ColRemark2.Width = 180;
            // 
            // ColRemark3
            // 
            this.ColRemark3.DataPropertyName = "Remark3";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColRemark3.DefaultCellStyle = dataGridViewCellStyle12;
            this.ColRemark3.HeaderText = "คำอธิบายรายการ3";
            this.ColRemark3.Name = "ColRemark3";
            this.ColRemark3.ReadOnly = true;
            this.ColRemark3.Width = 180;
            // 
            // ColInvNo
            // 
            this.ColInvNo.DataPropertyName = "InvNo";
            this.ColInvNo.HeaderText = "ColInvNo";
            this.ColInvNo.Name = "ColInvNo";
            this.ColInvNo.ReadOnly = true;
            this.ColInvNo.Visible = false;
            // 
            // ColCa
            // 
            this.ColCa.DataPropertyName = "Ca";
            this.ColCa.HeaderText = "Ca";
            this.ColCa.Name = "ColCa";
            this.ColCa.ReadOnly = true;
            this.ColCa.Visible = false;
            // 
            // ColCustName
            // 
            this.ColCustName.DataPropertyName = "CustName";
            this.ColCustName.HeaderText = "CustName";
            this.ColCustName.Name = "ColCustName";
            this.ColCustName.ReadOnly = true;
            this.ColCustName.Visible = false;
            // 
            // ColAddress
            // 
            this.ColAddress.DataPropertyName = "Address";
            this.ColAddress.HeaderText = "Address";
            this.ColAddress.Name = "ColAddress";
            this.ColAddress.ReadOnly = true;
            this.ColAddress.Visible = false;
            // 
            // ColBusinessType
            // 
            this.ColBusinessType.DataPropertyName = "BusinessType";
            this.ColBusinessType.HeaderText = "BusinessType";
            this.ColBusinessType.Name = "ColBusinessType";
            this.ColBusinessType.ReadOnly = true;
            this.ColBusinessType.Visible = false;
            // 
            // ColDepartment
            // 
            this.ColDepartment.DataPropertyName = "Department";
            this.ColDepartment.HeaderText = "Department";
            this.ColDepartment.Name = "ColDepartment";
            this.ColDepartment.ReadOnly = true;
            this.ColDepartment.Visible = false;
            // 
            // Colprofit
            // 
            this.Colprofit.DataPropertyName = "profit";
            this.Colprofit.HeaderText = "profit";
            this.Colprofit.Name = "Colprofit";
            this.Colprofit.ReadOnly = true;
            this.Colprofit.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtDepartment);
            this.panel1.Controls.Add(this.ButtonClear);
            this.panel1.Controls.Add(this.ButtonDelete);
            this.panel1.Controls.Add(this.ButtonEdit);
            this.panel1.Controls.Add(this.ButtonAdd);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.ComboBoxPrc);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.ComboBoxBussType);
            this.panel1.Controls.Add(this.TextBoxCustAddr);
            this.panel1.Controls.Add(this.TextBoxCustName);
            this.panel1.Controls.Add(this.TextBoxID);
            this.panel1.Controls.Add(this.TextBoxRef);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 212);
            this.panel1.TabIndex = 6;
            // 
            // txtDepartment
            // 
            this.txtDepartment.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepartment.Location = new System.Drawing.Point(642, 144);
            this.txtDepartment.Name = "txtDepartment";
            this.txtDepartment.Size = new System.Drawing.Size(367, 33);
            this.txtDepartment.TabIndex = 1000000040;
            // 
            // ButtonClear
            // 
            this.ButtonClear.BackColor = System.Drawing.Color.Transparent;
            this.ButtonClear.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonClear.BorderWidth = 1;
            this.ButtonClear.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonClear.ButtonText = "เคลียร์ค่า";
            this.ButtonClear.CausesValidation = false;
            this.ButtonClear.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonClear.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonClear.ForeColor = System.Drawing.Color.Black;
            this.ButtonClear.GradientAngle = 90;
            this.ButtonClear.Image = global::MyPayment.Properties.Resources.Cycle;
            this.ButtonClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonClear.Location = new System.Drawing.Point(870, 179);
            this.ButtonClear.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonClear.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonClear.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonClear.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.ShowButtontext = true;
            this.ButtonClear.Size = new System.Drawing.Size(139, 28);
            this.ButtonClear.StartColor = System.Drawing.Color.LightGray;
            this.ButtonClear.TabIndex = 1000000039;
            this.ButtonClear.TextLocation_X = 45;
            this.ButtonClear.TextLocation_Y = 3;
            this.ButtonClear.Transparent1 = 80;
            this.ButtonClear.Transparent2 = 120;
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.BackColor = System.Drawing.Color.Transparent;
            this.ButtonDelete.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonDelete.BorderWidth = 1;
            this.ButtonDelete.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonDelete.ButtonText = "ลบ";
            this.ButtonDelete.CausesValidation = false;
            this.ButtonDelete.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonDelete.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonDelete.ForeColor = System.Drawing.Color.Black;
            this.ButtonDelete.GradientAngle = 90;
            this.ButtonDelete.Image = global::MyPayment.Properties.Resources.Recycle;
            this.ButtonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonDelete.Location = new System.Drawing.Point(194, 179);
            this.ButtonDelete.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonDelete.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonDelete.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonDelete.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.ShowButtontext = true;
            this.ButtonDelete.Size = new System.Drawing.Size(83, 28);
            this.ButtonDelete.StartColor = System.Drawing.Color.LightGray;
            this.ButtonDelete.TabIndex = 1000000038;
            this.ButtonDelete.TextLocation_X = 35;
            this.ButtonDelete.TextLocation_Y = 1;
            this.ButtonDelete.Transparent1 = 80;
            this.ButtonDelete.Transparent2 = 120;
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // ButtonEdit
            // 
            this.ButtonEdit.BackColor = System.Drawing.Color.Transparent;
            this.ButtonEdit.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonEdit.BorderWidth = 1;
            this.ButtonEdit.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonEdit.ButtonText = "แก้ไข";
            this.ButtonEdit.CausesValidation = false;
            this.ButtonEdit.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonEdit.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonEdit.ForeColor = System.Drawing.Color.Black;
            this.ButtonEdit.GradientAngle = 90;
            this.ButtonEdit.Image = global::MyPayment.Properties.Resources.icons8_edit_32;
            this.ButtonEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonEdit.Location = new System.Drawing.Point(103, 179);
            this.ButtonEdit.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonEdit.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonEdit.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonEdit.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonEdit.Name = "ButtonEdit";
            this.ButtonEdit.ShowButtontext = true;
            this.ButtonEdit.Size = new System.Drawing.Size(83, 28);
            this.ButtonEdit.StartColor = System.Drawing.Color.LightGray;
            this.ButtonEdit.TabIndex = 1000000037;
            this.ButtonEdit.TextLocation_X = 32;
            this.ButtonEdit.TextLocation_Y = 1;
            this.ButtonEdit.Transparent1 = 80;
            this.ButtonEdit.Transparent2 = 120;
            this.ButtonEdit.UseVisualStyleBackColor = true;
            this.ButtonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
            // 
            // ButtonAdd
            // 
            this.ButtonAdd.BackColor = System.Drawing.Color.Transparent;
            this.ButtonAdd.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonAdd.BorderWidth = 1;
            this.ButtonAdd.ButtonShape = Custom_Controls_in_CS.ButtonZ.ButtonsShapes.Rect;
            this.ButtonAdd.ButtonText = "เพิ่ม";
            this.ButtonAdd.CausesValidation = false;
            this.ButtonAdd.EndColor = System.Drawing.Color.DarkGray;
            this.ButtonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdd.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold);
            this.ButtonAdd.ForeColor = System.Drawing.Color.Black;
            this.ButtonAdd.GradientAngle = 90;
            this.ButtonAdd.Image = global::MyPayment.Properties.Resources.add;
            this.ButtonAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonAdd.Location = new System.Drawing.Point(12, 179);
            this.ButtonAdd.MouseClickColor1 = System.Drawing.Color.DarkOrange;
            this.ButtonAdd.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ButtonAdd.MouseHoverColor1 = System.Drawing.Color.Gainsboro;
            this.ButtonAdd.MouseHoverColor2 = System.Drawing.Color.Orange;
            this.ButtonAdd.Name = "ButtonAdd";
            this.ButtonAdd.ShowButtontext = true;
            this.ButtonAdd.Size = new System.Drawing.Size(83, 28);
            this.ButtonAdd.StartColor = System.Drawing.Color.LightGray;
            this.ButtonAdd.TabIndex = 1000000036;
            this.ButtonAdd.TextLocation_X = 32;
            this.ButtonAdd.TextLocation_Y = 1;
            this.ButtonAdd.Transparent1 = 80;
            this.ButtonAdd.Transparent2 = 120;
            this.ButtonAdd.UseVisualStyleBackColor = true;
            this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(153, 114);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(18, 21);
            this.label14.TabIndex = 1000000029;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(153, 78);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 21);
            this.label13.TabIndex = 1000000028;
            this.label13.Text = "*";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(153, 43);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 21);
            this.label12.TabIndex = 1000000027;
            this.label12.Text = "*";
            // 
            // ComboBoxPrc
            // 
            this.ComboBoxPrc.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ComboBoxPrc.FormattingEnabled = true;
            this.ComboBoxPrc.Location = new System.Drawing.Point(172, 143);
            this.ComboBoxPrc.Name = "ComboBoxPrc";
            this.ComboBoxPrc.Size = new System.Drawing.Size(382, 34);
            this.ComboBoxPrc.TabIndex = 1000000025;
            this.ComboBoxPrc.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPrc_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(153, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 21);
            this.label11.TabIndex = 1000000021;
            this.label11.Text = "*";
            // 
            // ComboBoxBussType
            // 
            this.ComboBoxBussType.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ComboBoxBussType.FormattingEnabled = true;
            this.ComboBoxBussType.Items.AddRange(new object[] {
            " == กรุณาเลือกข้อมูล ==",
            "1001",
            "2001"});
            this.ComboBoxBussType.Location = new System.Drawing.Point(172, 107);
            this.ComboBoxBussType.Name = "ComboBoxBussType";
            this.ComboBoxBussType.Size = new System.Drawing.Size(382, 34);
            this.ComboBoxBussType.TabIndex = 1000000020;
            this.ComboBoxBussType.SelectedIndexChanged += new System.EventHandler(this.ComboBoxBussType_SelectedIndexChanged);
            // 
            // TextBoxCustAddr
            // 
            this.TextBoxCustAddr.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxCustAddr.Location = new System.Drawing.Point(172, 72);
            this.TextBoxCustAddr.Name = "TextBoxCustAddr";
            this.TextBoxCustAddr.Size = new System.Drawing.Size(837, 33);
            this.TextBoxCustAddr.TabIndex = 1000000019;
            // 
            // TextBoxCustName
            // 
            this.TextBoxCustName.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxCustName.Location = new System.Drawing.Point(172, 37);
            this.TextBoxCustName.Name = "TextBoxCustName";
            this.TextBoxCustName.Size = new System.Drawing.Size(837, 33);
            this.TextBoxCustName.TabIndex = 1000000018;
            // 
            // TextBoxID
            // 
            this.TextBoxID.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxID.Location = new System.Drawing.Point(642, 2);
            this.TextBoxID.Name = "TextBoxID";
            this.TextBoxID.Size = new System.Drawing.Size(167, 33);
            this.TextBoxID.TabIndex = 1000000017;
            // 
            // TextBoxRef
            // 
            this.TextBoxRef.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxRef.Location = new System.Drawing.Point(172, 2);
            this.TextBoxRef.Name = "TextBoxRef";
            this.TextBoxRef.Size = new System.Drawing.Size(382, 33);
            this.TextBoxRef.TabIndex = 1000000016;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(36, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 26);
            this.label10.TabIndex = 1000000015;
            this.label10.Text = "เลขที่เอกสารอ้างอิง";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(551, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 26);
            this.label9.TabIndex = 1000000014;
            this.label9.Text = "รหัสเครื่องวัด";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(85, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 26);
            this.label8.TabIndex = 1000000013;
            this.label8.Text = "ผู้ชำระเงิน";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(60, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 26);
            this.label7.TabIndex = 1000000012;
            this.label7.Text = "ที่อยู่ผู้ชำระเงิน";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(37, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 26);
            this.label6.TabIndex = 1000000011;
            this.label6.Text = "เลือกประเภทธุรกิจ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(575, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 26);
            this.label5.TabIndex = 1000000010;
            this.label5.Text = "หน่วยงาน";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(92, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 26);
            this.label1.TabIndex = 1000000009;
            this.label1.Text = "ศูนย์กำไร";
            // 
            // PanelEmployee
            // 
            this.PanelEmployee.Controls.Add(this.groupBoxEmployee);
            this.PanelEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelEmployee.Location = new System.Drawing.Point(0, 0);
            this.PanelEmployee.Name = "PanelEmployee";
            this.PanelEmployee.Size = new System.Drawing.Size(1012, 57);
            this.PanelEmployee.TabIndex = 5;
            // 
            // groupBoxEmployee
            // 
            this.groupBoxEmployee.Controls.Add(this.label3);
            this.groupBoxEmployee.Controls.Add(this.label2);
            this.groupBoxEmployee.Controls.Add(this.LabelStationNo);
            this.groupBoxEmployee.Controls.Add(this.label4);
            this.groupBoxEmployee.Controls.Add(this.LabelEmployeeName);
            this.groupBoxEmployee.Controls.Add(this.LabelEmployeeCode);
            this.groupBoxEmployee.Controls.Add(this.LabelFkName);
            this.groupBoxEmployee.Location = new System.Drawing.Point(15, 2);
            this.groupBoxEmployee.Name = "groupBoxEmployee";
            this.groupBoxEmployee.Size = new System.Drawing.Size(994, 48);
            this.groupBoxEmployee.TabIndex = 1000000022;
            this.groupBoxEmployee.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(353, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 26);
            this.label3.TabIndex = 1000000009;
            this.label3.Text = "รหัสพนง.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(6, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 26);
            this.label2.TabIndex = 1000000008;
            this.label2.Text = "ฟข.";
            // 
            // LabelStationNo
            // 
            this.LabelStationNo.BackColor = System.Drawing.Color.White;
            this.LabelStationNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelStationNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStationNo.Location = new System.Drawing.Point(889, 16);
            this.LabelStationNo.Name = "LabelStationNo";
            this.LabelStationNo.Size = new System.Drawing.Size(94, 25);
            this.LabelStationNo.TabIndex = 1000000020;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(817, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 26);
            this.label4.TabIndex = 1000000010;
            this.label4.Text = "ช่องชำระ";
            // 
            // LabelEmployeeName
            // 
            this.LabelEmployeeName.BackColor = System.Drawing.Color.White;
            this.LabelEmployeeName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelEmployeeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEmployeeName.Location = new System.Drawing.Point(534, 16);
            this.LabelEmployeeName.Name = "LabelEmployeeName";
            this.LabelEmployeeName.Size = new System.Drawing.Size(277, 25);
            this.LabelEmployeeName.TabIndex = 1000000019;
            this.LabelEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelEmployeeCode
            // 
            this.LabelEmployeeCode.BackColor = System.Drawing.Color.White;
            this.LabelEmployeeCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelEmployeeCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEmployeeCode.Location = new System.Drawing.Point(421, 16);
            this.LabelEmployeeCode.Name = "LabelEmployeeCode";
            this.LabelEmployeeCode.Size = new System.Drawing.Size(110, 25);
            this.LabelEmployeeCode.TabIndex = 1000000018;
            // 
            // LabelFkName
            // 
            this.LabelFkName.BackColor = System.Drawing.Color.White;
            this.LabelFkName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelFkName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelFkName.Location = new System.Drawing.Point(47, 16);
            this.LabelFkName.Name = "LabelFkName";
            this.LabelFkName.Size = new System.Drawing.Size(302, 25);
            this.LabelFkName.TabIndex = 1000000017;
            // 
            // FormOtherNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1012, 705);
            this.Controls.Add(this.panelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormOtherNew";
            this.Text = "FormOtherNew";
            this.Load += new System.EventHandler(this.FormOtherNew_Load);
            this.panelMain.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.PanelGridview.ResumeLayout(false);
            this.PanelGridview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PanelEmployee.ResumeLayout(false);
            this.groupBoxEmployee.ResumeLayout(false);
            this.groupBoxEmployee.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel PanelEmployee;
        private System.Windows.Forms.GroupBox groupBoxEmployee;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label LabelStationNo;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label LabelEmployeeName;
        internal System.Windows.Forms.Label LabelEmployeeCode;
        internal System.Windows.Forms.Label LabelFkName;
        private System.Windows.Forms.Panel panel1;
        private Custom_Controls_in_CS.ButtonZ ButtonClear;
        private Custom_Controls_in_CS.ButtonZ ButtonDelete;
        private Custom_Controls_in_CS.ButtonZ ButtonEdit;
        private Custom_Controls_in_CS.ButtonZ ButtonAdd;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox ComboBoxPrc;
        internal System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ComboBoxBussType;
        private System.Windows.Forms.TextBox TextBoxCustAddr;
        private System.Windows.Forms.TextBox TextBoxCustName;
        private System.Windows.Forms.TextBox TextBoxID;
        private System.Windows.Forms.TextBox TextBoxRef;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label labelItem;
        internal System.Windows.Forms.Label label21;
        internal System.Windows.Forms.Label label20;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel PanelGridview;
        private System.Windows.Forms.DataGridView dgvDetail;
        private System.Windows.Forms.Panel panel3;
        private Custom_Controls_in_CS.ButtonZ ButtonRec;
        internal System.Windows.Forms.Label txtTotalSum;
        internal System.Windows.Forms.Label Label37;
        private System.Windows.Forms.CheckBox CheckBoxSelectAll;
        internal System.Windows.Forms.Label txtVat;
        internal System.Windows.Forms.Label txtAmount;
        private System.Windows.Forms.TextBox txtDepartment;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTypeItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCodeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUnitPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCountUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCountAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRemark1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRemark2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRemark3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColInvNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCustName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBusinessType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDepartment;
        private System.Windows.Forms.DataGridViewTextBoxColumn Colprofit;
    }
}